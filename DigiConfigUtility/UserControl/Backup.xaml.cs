﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using System.IO;
using System.Configuration;
//using DigiPhoto.DataLayer.Model;
using System.Collections;
using System.ServiceProcess;
using System.ComponentModel;
using FrameworkHelper.Common;
using DigiPhoto;
using DigiConfigUtility.Utility;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Backup.xaml
    /// </summary>
    public partial class Backup : System.Windows.Controls.UserControl
    {
        #region Declaration
        enum IntervalType { Day = 1, Week = 2, Month = 3 };
        //Filter setting
        //"Red", "1" "Green", "2" "Blue", "3" "Magenta", "4" "Lime", "4" "Orange", "5" "Yellow", "6" "Underwater", "7" //"Warning", "8"
        System.ComponentModel.BackgroundWorker BackupWorker = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        static string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        static string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];
        string _databasename = "";
        string _username = "";
        string _pass = "";
        string _server = "";
        string _destinationPath = "";
        string _imgSource = "";
        string _imgDestination = "";
        string _heavyTables = "";
        int _cleanupdaysBackup;
        int _oldCleanupdaysBackup;
        int _FailledOnlineOrderCleanupdays;
        string _destinationPathDaily = string.Empty;
        string _filePathDate = string.Empty;
        string _thumbnailFilePathDate = string.Empty;
        string _bigThumbnailFilePathDate = string.Empty;
        string _destinationPathDailySite = string.Empty;
        string _filePathDateSource = string.Empty;
        string _thumbnailFilePathDateSource = string.Empty;
        string _bigThumbnailFilePathDateSource = string.Empty;
        //  DigiPhotoDataServices _objDataLayer;
        DateTime fromDate = DateTime.Now;
        DateTime toDate = DateTime.Now;
        string hotFolderPath = string.Empty;
        string cleanupTablesBase = string.Empty;
        List<string> lstSelectedTables = null;
        public class Backupdata
        {
            private DateTime? Scheduledate;


            public DateTime? ScheduleBackupdate
            {
                get { return Scheduledate; }
                set { Scheduledate = value; }
            }
            private string status;
            public string Backupstatus
            {
                get { return status; }
                set { status = value; }
            }
            private string Id;
            public string Backupid
            {
                get { return Id; }
                set { Id = value; }
            }


            private DateTime? EndDate;
            public DateTime? BackupEndDate
            {
                get { return EndDate; }
                set { EndDate = value; }
            }

            private DateTime? StartDate;
            public DateTime? BackupStartDate
            {
                get { return StartDate; }
                set { StartDate = value; }
            }
            private string Substore;
            public string BackupSubstore
            {
                get { return Substore; }
                set { Substore = value; }
            }
            private string Errormessage;
            public string BackupErrormessage
            {
                get { return Errormessage; }
                set { Errormessage = value; }
            }
            private int cleanupStatus;
            public int CleanupStatus
            {
                get { return cleanupStatus; }
                set { cleanupStatus = value; }
            }
            //-----------Code Added by Anis 18-Jan-19-------
            public string SetVisibility { get; set; }
        }


        #endregion
        #region Constructor
        public Backup()
        {

            InitializeComponent();
            GetConfigData();
            SetDailyBackUpPath(false);
            BackupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(BackupWorker_DoWork);
            BackupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(BackupWorker_RunWorkerCompleted);
            //MyWorker.RunWorkerCompleted += MyWorker_RunWorkerCompleted;
            if (txtTables.Text == "")
            {
                txtTables.Text = cleanupTablesBase;// "DG_Activity,DG_Photos,DG_Refund,DG_RefundDetails,DG_Orders,DG_Orders_Details,DG_PrinterQueue,DG_PhotoGroup";
            }

            fillRecursiveOptions();
            GetBackupConfigData();
            fromDate = DateTime.Now.Date.Add(new TimeSpan(-7, 0, 0, 0));
            toDate = DateTime.Now.Date.Add(new TimeSpan(7, 0, 0, 0));
            GetBackupistoryData(fromDate, toDate);
        }
        #endregion

        private void btnbrwsfolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                Button _objbtn = (Button)sender;
                switch (_objbtn.Name)
                {
                    case "btnBrowseDBbackupPath":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtDbBackupPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBrowseDfbackupPath":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtHfBackupPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBrowseDBbackupPathDaily":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtDbBackupPathDaily.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                }
            }

            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void chkSchBackupDaily_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetDailyBackUpPath(((CheckBox)sender).IsChecked);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void SetDailyBackUpPath(bool? IsDefaultSet)
        {
            if (IsDefaultSet == true)
            {
                txtDbBackupPathDaily.IsEnabled = true;
                btnBrowseDBbackupPathDaily.IsEnabled = true;
            }
            else
            {
                txtDbBackupPathDaily.Text = "";
                txtDbBackupPathDaily.IsEnabled = false;
                btnBrowseDBbackupPathDaily.IsEnabled = false;
            }
        }

        private void BtnRefresh_click(object sender, RoutedEventArgs e)
        {
            if (dtFrom.Text != null && dtTo.Text != null)
            {
                if ((dtFrom.Value.ToDateTime() <= dtTo.Value.ToDateTime()))
                {
                    fromDate = (DateTime)dtFrom.Value;
                    toDate = (DateTime)dtTo.Value;
                    GetBackupistoryData(fromDate, toDate);
                }
                else
                {
                    MessageBox.Show("'From Date' should always be grater then or equal to 'To Date'", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Select From date and To date.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void GetBackupistoryData(DateTime fromDate, DateTime toDate)
        {

            dtFrom.Value = fromDate;
            dtTo.Value = toDate;

            List<Backupdata> _objBackuphistorydetail = new List<Backupdata>();
            // DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            BackupHistoryBusniess bacBiz = new BackupHistoryBusniess();

            foreach (var item in bacBiz.GetBackupHistoryData(Config.SubStoreId, fromDate, toDate))
            {
                Backupdata _objnew = new Backupdata();
                _objnew.ScheduleBackupdate = item.ScheduleDate;
                //-----------Code Added by Anis 18-Jan-19-------
                _objnew.SetVisibility = item.SetVisibility;

                switch (item.Status)
                {
                    case 1:
                        _objnew.Backupstatus = "Completed";
                        break;
                    case 2:
                        _objnew.Backupstatus = "Failed";
                        break;
                    case 3:
                        _objnew.Backupstatus = "Scheduled";
                        break;
                    case 4:
                        _objnew.Backupstatus = "Inprogress ";
                        break;
                    default:
                        break;

                }
                switch (item.CleanupStatus)
                {
                    case 0:
                        _objnew.CleanupStatus = 0;
                        break;
                    case 1:
                        _objnew.CleanupStatus = 1;
                        break;
                    case 2:
                        _objnew.CleanupStatus = 2;
                        break;
                    case 3:
                        _objnew.CleanupStatus = 3;
                        break;
                    case 4:
                        _objnew.CleanupStatus = 4;
                        break;
                    default:
                        break;

                }
                _objnew.BackupStartDate = item.StartDate;
                _objnew.BackupEndDate = item.EndDate;
                _objnew.BackupErrormessage = item.ErrorMessage;
                if (_objnew.BackupErrormessage != null)
                {
                    _objnew.Backupid = item.BackupId.ToString();

                }

                else
                {
                    _objnew.Backupid = string.Empty;

                }
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                List<string> objList = stoBiz.GetBackupsubstorename(Config.SubStoreId);
                foreach (string s in objList)
                {
                    _objnew.BackupSubstore = s;
                }
                _objBackuphistorydetail.Add(_objnew);
            }

            _objBackuphistorydetail = _objBackuphistorydetail.OrderByDescending(t => t.ScheduleBackupdate.HasValue).OrderByDescending(t => t.ScheduleBackupdate).ToList();
            DGbackuphistory.ItemsSource = _objBackuphistorydetail;

        }

        private void GetBackupConfigData()
        {
            try
            {
                DateTime dtsch = DateTime.Now;
                // _objDataLayer = new DigiPhotoDataServices();
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> _objdata = conBiz.GetNewConfigValues(Config.SubStoreId);

                if (_objdata != null && _objdata.Count > 0)
                {
                    foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                    {
                        switch (imixConfigValue.IMIXConfigurationMasterId)
                        {
                            case 9:
                                txtDbBackupPath.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case 10:
                                txtTables.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case 11:
                                txtHfBackupPath.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case 12:
                                if (!string.IsNullOrEmpty(imixConfigValue.ConfigurationValue))
                                {
                                    dtsch = Convert.ToDateTime(imixConfigValue.ConfigurationValue, CultureInfo.InvariantCulture);
                                }
                                break;
                            case 13:
                                chkSchBackup.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                                break;
                            case 14:
                                chkRecursive.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                                break;
                            case 15:
                                cmbSelectRecursiveCount.SelectedItem = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case 16:
                                cmbSelectRecursiveTime.SelectedItem = (IntervalType)imixConfigValue.ConfigurationValue.ToInt32();
                                break;
                            case 17:
                                txtCleanupdays.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case (int)ConfigParams.CleanUpDaysOldBackup:
                                txtBackupCleanupdays.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case (int)ConfigParams.BackUpEmailAddress:
                                txtBackupEmailAddress.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                                break;
                            case (int)ConfigParams.ScheduleBackupDaily:
                                {
                                    if (!string.IsNullOrEmpty(imixConfigValue.ConfigurationValue))
                                    {
                                        chkSchBackupDaily.IsChecked = true;
                                        txtDbBackupPathDaily.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                                        SetDailyBackUpPath(true);
                                    }
                                    else
                                    {
                                        chkSchBackupDaily.IsChecked = false;
                                        SetDailyBackUpPath(false);
                                    }
                                }
                                break;
                            case (int)ConfigParams.ChangeTrackingAppObjectIDs:
                                txtApplicationObjectIDs.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                                break;
                            case (int)ConfigParams.FailedOnlineOrderCleanupDays:
                                txtFailedOnlineOrderCleanupdays.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case (int)ConfigParams.ArchiveCleanupDays:  //Added by Manoj for setting of ArchiveCleanupDays
                                txtDBArchiveCleanUpDays.Text = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            //case (int)ConfigParams.BackUpEmailSubject:
                            //    txtBackupEmailSubject.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            //    break;
                            default:
                                break;
                        }
                    }

                    //Get selected table names
                    string[] valuesArray = txtTables.Text.Split(',');
                    List<string> names = valuesArray.ToList();

                    //Get all table names
                    CommonBusiness comBiz = new CommonBusiness();
                    List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                    List<Action> abc = new List<Action>();
                    lstSelectedTables = new List<string>();
                    if (_objTabledata != null)
                        foreach (var datas in _objTabledata)
                        {
                            Action data = new Action();
                            data.IsSelected = false;
                            data.Name = datas.name;

                            abc.Add(data);


                        }

                    foreach (var datas in abc)
                    {

                        datas.IsSelected = true;
                        lstSelectedTables.Add(datas.Name);

                    }
                    listtables.ItemsSource = abc;

                    if (string.IsNullOrEmpty(hotFolderPath) == false)
                    {
                        hiddenHFPath.Text = hotFolderPath;
                    }
                    if (chkSchBackup.IsChecked == true)
                    {
                        if (!string.IsNullOrEmpty(dtsch.ToString()))
                            ccBackupDateTime.Value = dtsch;
                    }
                    else
                    {
                        ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                    }
                }
                else
                {
                    if (txtTables.Text == "")
                    {
                        txtTables.Text = cleanupTablesBase;// "DG_Activity,DG_Photos,DG_Refund,DG_RefundDetails,DG_Orders,DG_Orders_Details,DG_PrinterQueue,DG_PhotoGroup";
                    }
                    string[] valuesArray = txtTables.Text.Split(',');
                    List<string> names = valuesArray.ToList();

                    //Get all table names
                    CommonBusiness comBiz = new CommonBusiness();
                    List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                    List<Action> abc = new List<Action>();
                    if (_objTabledata != null)
                        foreach (var datas in _objTabledata)
                        {
                            Action data = new Action();
                            data.IsSelected = false;
                            data.Name = datas.name;
                            abc.Add(data);
                        }

                    listtables.ItemsSource = abc;

                    lstSelectedTables = new List<string>();
                    foreach (var datas in abc)
                    {
                        datas.IsSelected = true;
                        lstSelectedTables.Add(datas.Name);
                    }
                    txtDbBackupPath.Text = String.Empty;
                    txtTables.Text = String.Empty;
                    txtCleanupdays.Text = string.Empty;
                    txtHfBackupPath.Text = String.Empty;
                    chkSchBackup.IsChecked = false;
                    ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        void GetConfigData()
        {
            var configItem = new ConfigBusiness().GetConfigurationData(Config.SubStoreId);
            if (configItem != null)
            {
                hotFolderPath = configItem.DG_Hot_Folder_Path;
                cleanupTablesBase = configItem.DG_CleanupTables;
            }
        }
        private void SetBackupConfigData()
        {

            int substoreId = Config.SubStoreId;
            string dbBackupPath = txtDbBackupPath.Text;
            int DaysForCleanupBackup = txtCleanupdays.Text.ToInt32();
            #region Archived days deletion parameter
            int archiveCleanupDays = Convert.ToInt16(txtDBArchiveCleanUpDays.Text);
            #endregion
            string cleanupTables = string.Empty;

            List<int> selectedItemIndexes = (from object o in lstSelectedTables select listtables.Items.IndexOf(o)).ToList();

            for (int j = 0; j < lstSelectedTables.Count(); j++)
            {
                listtables.SelectedItems.Add(listtables.Items.GetItemAt(j));
            }
            int i;

            for (i = 0; i < listtables.SelectedItems.Count; i++)
            {
                cleanupTables = cleanupTables + ((Action)listtables.SelectedItems[i]).Name + ",";
            }

            cleanupTables = cleanupTables.Length > 0 ? cleanupTables.TrimEnd(cleanupTables[cleanupTables.Length - 1]) : string.Empty;
            string hfBackupPath = txtHfBackupPath.Text;
            bool isScheduled = false;
            if (chkSchBackup.IsChecked != null)
            {
                isScheduled = chkSchBackup.IsChecked.Value;
            }
            else
            {
                isScheduled = false;
            }


            string backupDateTime = ccBackupDateTime.Value.ToString();
            // _objDataLayer = new DigiPhotoDataServices();

            bool IsRecursive = false;
            int IntervalCount = 0;
            int IntervalType = 0;
            if (chkRecursive.IsChecked == true)
            {

                if (chkSchBackup.IsChecked != true)
                {
                    MessageBox.Show("Check out schedule to start recursively.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (string.IsNullOrEmpty(backupDateTime))
                {
                    MessageBox.Show("Select date and time to start recursively.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (cmbSelectRecursiveCount.SelectedItem == null)
                {
                    MessageBox.Show("Select recursive interval days.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (cmbSelectRecursiveTime.SelectedItem == null)
                {
                    MessageBox.Show("Select recursive type.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                IsRecursive = true;
                IntervalCount = int.Parse(cmbSelectRecursiveCount.SelectedItem.ToString());
                IntervalType = (int)((IntervalType)cmbSelectRecursiveTime.SelectedItem);
            }

            if (!cleanupTablesBase.Contains("DG_Photos"))
            {
                MessageBox.Show("please Select The DG_Photos from Table List");
            }
            else
            {
                List<iMIXConfigurationInfo> configEntityList = new List<iMIXConfigurationInfo>();

                iMIXConfigurationInfo configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DbBackupPath);
                configEntity.ConfigurationValue = dbBackupPath.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.CleanUpTable);
                configEntity.ConfigurationValue = cleanupTables;
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.HfBackupPath);
                configEntity.ConfigurationValue = hfBackupPath;
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsBackupScheduled);
                configEntity.ConfigurationValue = isScheduled.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ScheduleBackup);
                configEntity.ConfigurationValue = backupDateTime.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsRecursive);
                configEntity.ConfigurationValue = IsRecursive.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IntervalCount);
                configEntity.ConfigurationValue = IntervalCount.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IntervalType);
                configEntity.ConfigurationValue = IntervalType.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.CleanUpDaysBackup);
                configEntity.ConfigurationValue = DaysForCleanupBackup.ToString();
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.CleanUpDaysOldBackup);
                configEntity.ConfigurationValue = txtBackupCleanupdays.Text;
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.BackUpEmailAddress);
                configEntity.ConfigurationValue = txtBackupEmailAddress.Text;
                configEntityList.Add(configEntity);

                //if (chkSchBackupDaily.IsChecked == true && !string.IsNullOrEmpty(txtDbBackupPathDaily.Text.Trim()))
                //{
                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ScheduleBackupDaily);
                configEntity.ConfigurationValue = txtDbBackupPathDaily.Text;
                configEntityList.Add(configEntity);

                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ChangeTrackingAppObjectIDs);
                configEntity.ConfigurationValue = txtApplicationObjectIDs.Text.Trim().Equals("") ? "16" : txtApplicationObjectIDs.Text.Trim();
                configEntityList.Add(configEntity);
                //}
                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.FailedOnlineOrderCleanupDays);
                configEntity.ConfigurationValue = txtFailedOnlineOrderCleanupdays.Text;
                configEntityList.Add(configEntity);

                //--23 Mar 2018---2018-----Nilesh----Set the Dont do DB Clean Up Process----------Start---------- 
                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DontDoDBCleanUp);
                configEntity.ConfigurationValue = chkCleanUpReq.IsChecked.Value.ToString();
                configEntityList.Add(configEntity);
                //--23 Mar 2018---2018-----Nilesh----Set the Dont do DB Clean Up Process------------End-----------

                // ******************* Ajay start 29-May-2018*******************************
                configEntity = new iMIXConfigurationInfo();
                configEntity.SubstoreId = Config.SubStoreId;
                configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ArchiveCleanupDays);
                configEntity.ConfigurationValue = archiveCleanupDays.ToString();
                configEntityList.Add(configEntity);
                // ******************* Ajay End 29-May-2018*******************************


                //_objDataLayer = new DigiPhotoDataServices();
                ConfigBusiness conBiz = new ConfigBusiness();
                bool IsSaved = conBiz.SaveUpdateNewConfig(configEntityList);
                if (IsSaved)
                {

                    if (chkSchBackup.IsChecked == true)
                    {
                        try
                        {
                            int status = Convert.ToInt32(Digibackup.scheduled);
                            BackupHistoryBusniess bacBiz = new BackupHistoryBusniess();

                            bacBiz.SaveScheduledConfig(substoreId, backupDateTime.ToDateTime(), status);
                            //Start backup service
                            ServiceController myService = new ServiceController();
                            myService.ServiceName = "DigiBackupService";
                            string svcStatus = myService.Status.ToString();
                            if (svcStatus == "Stopped")
                            {
                                myService.Start();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("There was some error starting the backup service! Error: " + ex.Message, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }

                    MessageBox.Show("Backup configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnBackupNow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Config.SubStoreId <= 0)
                {
                    MessageBox.Show("Please configure your SubStore.");
                    return;
                }
                if (string.IsNullOrEmpty(txtDbBackupPath.Text.Trim()))
                {
                    MessageBox.Show("Select database backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (string.IsNullOrEmpty(txtHfBackupPath.Text.Trim()))
                {
                    MessageBox.Show("Select hot folder backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (string.IsNullOrEmpty(txtCleanupdays.Text.Trim()))
                {
                    MessageBox.Show("Enter No of days for Cleanup Backup.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (chkSchBackupDaily.IsChecked == true && string.IsNullOrEmpty(txtDbBackupPathDaily.Text.Trim()))
                {
                    MessageBox.Show("Select Nas storage path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                bs.Show();
                string appConString = "";
                appConString = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                string[] strVar = appConString.Split(';');                
                Hashtable htCon = new Hashtable();
                if (strVar.Length != 0)
                {
                    for (int k = 0; k <= strVar.Length - 1; k++)
                    {
                        string[] tempK = strVar[k].Split('=');
                        //if (tempK.Length != 0)//Commmnted by VINS..Gaylord site had issue of last semicolon on cnn string_8Dec2019
                        if (tempK.Length >= 2)
                        {
                            if (!htCon.ContainsKey(tempK[0]))
                            {
                                htCon.Add(tempK[0], tempK[1]);
                            }
                        }
                    }
                }
                _databasename = htCon.ContainsKey("Initial Catalog") ? htCon["Initial Catalog"].ToString() : ConfigurationManager.AppSettings["DatabaseName"];
                _username = htCon.ContainsKey("User ID") ? htCon["User ID"].ToString() : ConfigurationManager.AppSettings["UserName"];
                _pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : ConfigurationManager.AppSettings["UserPass"];
                _server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : ConfigurationManager.AppSettings["UserPass"];
                _destinationPath = txtDbBackupPath.Text;
                _imgSource = hiddenHFPath.Text;
                _imgDestination = txtHfBackupPath.Text;
                CommonBusiness comBiz = new CommonBusiness();
                List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                listtables.DataContext = _objTabledata;
                _heavyTables = txtTables.Text;
                _cleanupdaysBackup = txtCleanupdays.Text.ToInt32();
                _oldCleanupdaysBackup = txtBackupCleanupdays.Text.Trim().ToInt32();
                _destinationPathDaily = txtDbBackupPathDaily.Text.Trim();
                _FailledOnlineOrderCleanupdays = txtFailedOnlineOrderCleanupdays.Text.Trim().ToInt32();
                BackupWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                SaveEmailDetails(ex.Message);
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
        }
        private bool SaveEmailDetails(string exceptionMsg)
        {
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, Config.SubStoreId);
            string ToAddress = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.BackUpEmailAddress).FirstOrDefault().Value;
            if (ToAddress != null)
            {
                bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP FAILED", Config.SubStoreId);
            }
            return true;
        }
        private void BackupWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string errorMsg = "Error Message: There is not enough space in the disk.";
            try
            {
                int errorId = 0;
                bool isBackupFailed = false;
                if (clsBackup.IsMemoryAvailable(_imgSource, _imgDestination, Config.SubStoreId))
                {
                    DoBackupNow(ref errorId);
                    if (errorId == (int)BackupErrorMessage.DestinationDiskFull)
                        isBackupFailed = true;
                }
                else
                {
                    isBackupFailed = true;
                }
                if (isBackupFailed)
                {
                    MessageBox.Show(errorMsg);
                    LogConfigurator.log.Error(errorMsg);
                    SaveEmailDetails(errorMsg);

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error(errorMsg);
            }
        }
        private void BackupWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            GetBackupConfigData();
        }

        private static void DoBackupNowDaily(Int32 subStoreId, string _imgSource, string destinationId, int excludeBackUpDays)
        {
            try
            {
                WriteToFile("DoBackupNowDaily 1: " + _imgSource + " excludeBackUpDays : " + excludeBackUpDays);
                string _destinationPathDaily = string.Empty;
                string _filePathDate = string.Empty;
                string _thumbnailFilePathDate = string.Empty;
                string _bigThumbnailFilePathDate = string.Empty;
                string _destinationPathDailySite = string.Empty;
                string _filePathDateSource = string.Empty;
                string _thumbnailFilePathDateSource = string.Empty;
                string _bigThumbnailFilePathDateSource = string.Empty;
                string _VideoFilePathDate = string.Empty;
                string _ProcessedVideoFilePathDate = string.Empty;
                string _VideoFilePathDateSource = string.Empty;
                string _ProcessedVideoFilePathDateSource = string.Empty;
                iMIXConfigurationInfo _objdata = (new ConfigBusiness()).GetNewConfigValues(subStoreId)
                .Where(o => o.IMIXConfigurationMasterId == 95).FirstOrDefault();
                if (_objdata != null && !string.IsNullOrEmpty(_objdata.ConfigurationValue))
                {
                    _destinationPathDaily = _objdata.ConfigurationValue.ToString();
                    _destinationPathDaily = FrameworkHelper.Common.clsBackup.CreateBackupFolderExits(_destinationPathDaily);
                    DailyImageBackupInNAS(_imgSource, _destinationPathDaily, excludeBackUpDays);
                }
                if (!String.IsNullOrEmpty(destinationId) && !String.IsNullOrWhiteSpace(destinationId))
                {

                    if (Directory.Exists(destinationId))
                    {
                        if (!String.IsNullOrEmpty(_imgSource))
                        {
                            if (Directory.Exists(_imgSource))
                            {
                                _filePathDate = System.IO.Path.Combine(destinationId, DateTime.Now.AddDays(-excludeBackUpDays).ToString("yyyyMMdd"));
                                _VideoFilePathDate = System.IO.Path.Combine(destinationId, "Videos", (DateTime.Now.AddDays(-excludeBackUpDays)).ToString("yyyyMMdd"));
                                _ProcessedVideoFilePathDate = System.IO.Path.Combine(destinationId, "ProcessedVideos", (DateTime.Now.AddDays(-excludeBackUpDays)).ToString("yyyyMMdd"));
                                _VideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "Videos", (DateTime.Now.AddDays(-excludeBackUpDays)).ToString("yyyyMMdd"));
                                _ProcessedVideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "ProcessedVideos", (DateTime.Now.AddDays(-excludeBackUpDays)).ToString("yyyyMMdd"));
                                _filePathDateSource = System.IO.Path.Combine(_imgSource, DateTime.Now.AddDays(-excludeBackUpDays).ToString("yyyyMMdd"));
                                WriteToFile("DoBackupNowDaily 2: " + _filePathDate + " _filePathDateSource : " + _filePathDateSource);
                                if (Directory.Exists(_filePathDateSource))
                                {
                                    if (!Directory.Exists(_filePathDate))
                                    {
                                        Directory.CreateDirectory(_filePathDate);
                                    }
                                    CopyAllFile(new DirectoryInfo(_filePathDateSource), new DirectoryInfo(_filePathDate));
                                }

                                if (Directory.Exists(_VideoFilePathDateSource))
                                {
                                    if (!Directory.Exists(_VideoFilePathDate))
                                    {
                                        Directory.CreateDirectory(_VideoFilePathDate);
                                    }
                                    CopyAllFile(new DirectoryInfo(_VideoFilePathDateSource), new DirectoryInfo(_VideoFilePathDate));
                                }
                                if (Directory.Exists(_ProcessedVideoFilePathDateSource))
                                {
                                    if (!Directory.Exists(_ProcessedVideoFilePathDate))
                                    {
                                        Directory.CreateDirectory(_ProcessedVideoFilePathDate);
                                    }
                                    CopyAllFile(new DirectoryInfo(_ProcessedVideoFilePathDateSource), new DirectoryInfo(_ProcessedVideoFilePathDate));
                                }
                            }
                        }
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                WriteToFile("DoBackupNowDaily 1:catch : ");

            }
        }
        private static void DailyImageBackupInNAS(string _imgSource, string _destinationPathDaily, int excludeBackUpDays)
        {
            string _filePathDate = string.Empty;
            string _filePathDateSource = string.Empty;
            string _VideoFilePathDate = string.Empty;
            string _ProcessedVideoFilePathDate = string.Empty;
            string _VideoFilePathDateSource = string.Empty;
            string _ProcessedVideoFilePathDateSource = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(_destinationPathDaily) && !String.IsNullOrWhiteSpace(_destinationPathDaily))
                {

                    if (Directory.Exists(_destinationPathDaily))
                    {
                        if (!String.IsNullOrEmpty(_imgSource))
                        {
                            if (Directory.Exists(_imgSource))
                            {
                                _filePathDate = System.IO.Path.Combine(_destinationPathDaily, DateTime.Now.ToString("yyyyMMdd"));
                                _filePathDateSource = System.IO.Path.Combine(_imgSource, DateTime.Now.ToString("yyyyMMdd"));
                                CopyFiles(_filePathDateSource, _filePathDate);
                                _VideoFilePathDate = System.IO.Path.Combine(_destinationPathDaily, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                                _VideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                                CopyFiles(_VideoFilePathDateSource, _VideoFilePathDate);
                                _ProcessedVideoFilePathDate = System.IO.Path.Combine(_destinationPathDaily, "ProcessedVideos", DateTime.Now.ToString("yyyyMMdd"));
                                _ProcessedVideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "ProcessedVideos", DateTime.Now.ToString("yyyyMMdd"));
                                CopyFiles(_ProcessedVideoFilePathDateSource, _ProcessedVideoFilePathDate);
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        protected static void WriteToFile(string text)
        {
            string filePath = @"C:\";  // folder location
            string fileName = "ServiceError" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            filePath = filePath + fileName;
            if (!Directory.Exists(filePath))
            {
                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + text + "<br/>" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "---------------------------------------------------" + Environment.NewLine);
                }
            }
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }
        }
        private static void CopyFiles(string source, string destination)
        {
            if (Directory.Exists(source))
            {
                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }
                CopyAllFile(new DirectoryInfo(source), new DirectoryInfo(destination));
            }
        }
        public static void CopyAllFile(DirectoryInfo source, DirectoryInfo target)
        {
            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                if (!File.Exists(target + @"\" + fi.Name))
                    fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name));

            }
        }
        private void DoBackupNow(ref int errorID)
        {
            bool isBackupDBSuccess; bool isTableCleaned; bool isImagesCompressed; string strMsg = "";
            try
            {
                if (!String.IsNullOrEmpty(_destinationPath) && !String.IsNullOrWhiteSpace(_destinationPath))
                {
                    if (Directory.Exists(_destinationPath))
                    {
                        //int errorId = 0;
                        isBackupDBSuccess = clsBackup.BackupDatabase(_databasename, _username, _pass, _server, _destinationPath, ref errorID);
                        if (!isBackupDBSuccess && errorID == (int)BackupErrorMessage.DestinationDiskFull)
                        {
                            strMsg += "ERROR : There is not enough space in the disk.";
                            return;
                        }
                        if (isBackupDBSuccess)
                        {
                            //MessageBox.Show("Database backup complete!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            strMsg += "- Database backup complete! \n\r\n\r";
                        }
                        else
                        {
                            throw new Exception("DB Backup operation could not be executed");
                        }
                        //MessageBox.Show("Database backup complete!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);                       
                        if (!String.IsNullOrEmpty(_heavyTables))
                        {
                            if (!_heavyTables.EndsWith(","))
                            {
                                _heavyTables = _heavyTables + ",";
                            }
                            isTableCleaned = clsBackup.EmptyTables(_heavyTables, _cleanupdaysBackup, _oldCleanupdaysBackup, Config.SubStoreId, _FailledOnlineOrderCleanupdays);
                            if (isTableCleaned)
                            {
                                strMsg += "- Specified tables have been emptied! \n\r\n\r";
                                //MessageBox.Show("Specified tables have been emptied!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                                if (!String.IsNullOrEmpty(_imgDestination) && !String.IsNullOrWhiteSpace(_imgDestination))
                                {
                                    if (Directory.Exists(_imgDestination))
                                    {
                                        if (!String.IsNullOrEmpty(_imgSource))
                                        {
                                            //isImagesCompressed = true;
                                            //isImageCompressed = clsBackup.BackupCompress(_imgSource, _imgDestination);
                                            string destinationFld = clsBackup.CreateBackupFolderExits(_imgSource, _imgDestination);
                                            //if (isImagesCompressed)
                                            //{
                                            int delImgCount = 0;

                                            if (!String.IsNullOrEmpty(_imgSource))
                                            {
                                                int tableCount = 1;//for the first time

                                                tableCount = clsBackup.GetPhotoArchived();
                                                if (tableCount > 0)
                                                {
                                                    DoBackupNowDaily(Config.SubStoreId, _imgSource, _imgDestination, _cleanupdaysBackup);
                                                    delImgCount += clsBackup.CopyDirectoryFiles(destinationFld, _imgSource);
                                                    bool statusUpdated = clsBackup.UpdateArchivedPhotoDetails();


                                                }

                                                strMsg += "- " + delImgCount.ToString() + " images deleted! \n\r\n\r";
                                            }
                                            //}
                                            //else
                                            //{
                                            //    throw new Exception("Images not Compressed");
                                            //}
                                        }
                                        else
                                        {
                                            MessageBox.Show("Hot folder path has not been set!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Drive Not Exists");
                                        return;

                                    }
                                }
                                else
                                {

                                    MessageBox.Show("Image Destination Path is Null");
                                    return;
                                }
                            }
                            else
                            {

                                MessageBox.Show("specified Tables are not Cleaned");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("No tables specified!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Drive Not Exists");
                        return;

                    }
                }
                else
                {
                    MessageBox.Show("Destination Path is not correct");
                    return;

                }
                MessageBox.Show("Backup Complete\n\r\n\r" + strMsg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was some error in backup! Error: " + ex.Message, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                SaveEmailDetails("There was some error in backup! Error: " + ex.Message + strMsg);
            }
        }

        /// <summary>
        /// delete directory recursivly
        /// </summary>
        /// <param name="baseDir"></param>
        public static void RecursiveDelete(DirectoryInfo baseDir)
        {
            if (!baseDir.Exists)
                return;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
                RecursiveDelete(dir);
            }
            baseDir.Delete(true);
        }

        private void btnSaveBackupSettings_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^[1-9][0-9]{0,2}$"); //regex that allows numeric input only
            Regex regexEmail = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

            if (Config.SubStoreId <= 0)
            {
                MessageBox.Show("Please configure your SubStore.");
                return;
            }
            if (string.IsNullOrEmpty(txtBackupEmailAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter email address.");
                return;
            }
            DateTime dtProvided = DateTime.Parse(ccBackupDateTime.Value.ToString());
            if (string.IsNullOrEmpty(txtDbBackupPath.Text.Trim()))
            {
                MessageBox.Show("Select database backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtHfBackupPath.Text.Trim()))
            {
                MessageBox.Show("Select hot folder backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Enter No of days for Cleanup Backup.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtBackupCleanupdays.Text.Trim()) || !regex.IsMatch(txtBackupCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Enter days for Cleanup old Backups between 1 and 999.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtFailedOnlineOrderCleanupdays.Text.Trim()) || !regex.IsMatch(txtFailedOnlineOrderCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Enter No of days for failed online orders Backup.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if (Convert.ToInt32(txtCleanupdays.Text.Trim()) > Convert.ToInt32(txtFailedOnlineOrderCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Failed online orders cleanup days should be greater than backup cleanup days!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (chkSchBackupDaily.IsChecked == true && string.IsNullOrEmpty(txtDbBackupPathDaily.Text.Trim()))
            {
                MessageBox.Show("Select Nas storage path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Convert.ToInt32(txtDBArchiveCleanUpDays.Text.Trim()) <= 0)
            {
                MessageBox.Show("Please enter Archive Cleanup Days greater than zero!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dtProvided < DateTime.Now)
            //if ((chkSchBackup.IsChecked == true) && (dtProvided < DateTime.Now))
            {
                MessageBox.Show("Backup cannot be scheduled for previous date or time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetBackupConfigData();
                BtnRefresh_click(sender, e);
            }
        }

        private void fillRecursiveOptions()
        {
            cmbSelectRecursiveCount.Items.Clear();
            for (int d = 1; d < 30; d++)
            {
                cmbSelectRecursiveCount.Items.Add(d.ToString());
            }

            cmbSelectRecursiveTime.Items.Clear();
            cmbSelectRecursiveTime.Items.Add(IntervalType.Day);
            cmbSelectRecursiveTime.Items.Add(IntervalType.Week);
            cmbSelectRecursiveTime.Items.Add(IntervalType.Month);
        }

        private void ccBackupDateTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            DateTime schdlbackup = new DateTime();
            schdlbackup = ccBackupDateTime.Value.ToDateTime();

            if (schdlbackup.TimeOfDay.ToString() == "00:00:00")
            {
                ccBackupDateTime.Value = schdlbackup.AddHours(11);

            }
        }
        /// <summary>
        /// In DataGrid delete button click- scheduled gets deleted on this button click- added by manoj at 12-Sept-2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
            object item = DGbackuphistory.SelectedItem;
            if (item != null)
            {
                int backupId = Convert.ToInt32(((DigiConfigUtility.Backup.Backupdata)item).Backupid);
                MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this scheduled backup ? ", "Digiphoto", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result == MessageBoxResult.OK)
                {
                    int isDeleted = stoBiz.DeleteBackupHistory(backupId);
                    BtnRefresh_click(sender, e);
                }
            }
        }
    }

    public class Action : INotifyPropertyChanged
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
                }
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class CleanupStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string CleanupStatus = string.Empty;
            switch (value.ToString())
            {
                case "0":
                    CleanupStatus = "NA";
                    break;
                case "1":
                    CleanupStatus = "Scheduled";
                    break;
                case "2":
                    CleanupStatus = "Failed";
                    break;
                case "3":
                    CleanupStatus = "Completed";
                    break;
                case "4":
                    CleanupStatus = "In-Progress";
                    break;
            }
            return CleanupStatus;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
