﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using DigiConfigUtility.Utility;
using System.IO;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System.Data;
using System.Text;
using DigiPhoto;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Linq;
using System.ServiceProcess;
using System.Diagnostics;

using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Trigger.xaml
    /// </summary>
    public partial class Trigger : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        public List<SyncTriggerStatusInfo> SyncTableList;
        public List<SyncPriority> SyncPriorityList;
        enum IntervalType { Hour = 0, Day = 1 };
        int prevSyncSI = 0, prevSyncDD = 0, prevReSyncDD = 0;
        public Trigger()
        {
            InitializeComponent();
            fillRecursiveOptions();
            ccResyncDateTime.Value = DateTime.Now;
            SyncTableList = GetTriggerStatus();
            SyncPriorityList = GetSyncPriority();
            listTables.ItemsSource = SyncTableList;
            SyncPrioritylistDisplay.ItemsSource = SyncPriorityList;
            
        }
        private List<SyncTriggerStatusInfo> GetTriggerStatus()
        {
            List<SyncTriggerStatusInfo> TableList = new List<SyncTriggerStatusInfo>();
            ConfigBusiness objBuss = new ConfigBusiness();
            TableList = objBuss.GetAllSyncTriggerTables();
            if (TableList.Where(o => o.IsSyncTriggerEnable == false).ToList().Count > 0)
            {
                chkSelectAll.IsChecked = false;
            }
            GetSyncConfigDetails();
            return TableList;
        }
        #region Sync Priority Changes Ashirwad
        private List<SyncPriority> GetSyncPriority()
        {
            List<SyncPriority> TableList = new List<SyncPriority>();
            ConfigBusiness objBuss = new ConfigBusiness();
            TableList = objBuss.GetAllSyncPriority();        
            return TableList;
        }
        #endregion
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Config.SubStoreId <= 0)
                {
                    System.Windows.MessageBox.Show("Please configure your SubStore", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    if (validateInput())
                    {
                        ConfigBusiness objBuss = new ConfigBusiness();
                        bool SyncRetryActive = chkReSyncFailedOnlineOrder.IsChecked == true ? true : false;
                        string ResyncDateTime = string.Empty; int MaxRetryCount = 0; int IntervalCount = 0;
                        int IntervalType = 0;
                        if (chkReSyncFailedOnlineOrder.IsChecked == true)
                        {
                            ResyncDateTime = ccResyncDateTime.Value.ToString();

                            if (string.IsNullOrEmpty(ResyncDateTime))
                            {
                                System.Windows.MessageBox.Show("Select date and time.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            if (string.IsNullOrEmpty(txtReSyncMaxCount.Text))
                            {
                                System.Windows.MessageBox.Show("Enter max retry count.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            if (cmbSelectRecursiveCount.SelectedItem == null)
                            {
                                System.Windows.MessageBox.Show("Select recursive interval days.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            if (cmbSelectRecursiveTime.SelectedItem == null)
                            {
                                System.Windows.MessageBox.Show("Select recursive type.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            if (Convert.ToDateTime(ResyncDateTime) < DateTime.Now)
                            //if ((chkSchBackup.IsChecked == true) && (dtProvided < DateTime.Now))
                            {
                                System.Windows.MessageBox.Show("Resync Configuration Start From cannot be scheduled for previous date or time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            MaxRetryCount = Convert.ToInt32(txtReSyncMaxCount.Text);
                            IntervalCount = int.Parse(cmbSelectRecursiveCount.SelectedItem.ToString());
                            IntervalType = (int)((IntervalType)cmbSelectRecursiveTime.SelectedItem);
                        }


                        SaveConfigValue(txtSyncServiceInterval.Text, txtSyncDataDelay.Text, txtReSyncDataDelay.Text, SyncRetryActive, ResyncDateTime, IntervalType, IntervalCount, MaxRetryCount);
                        if (chkReSyncFailedOnlineOrder.IsChecked == true)
                        {
                            SaveResyncHistory(Convert.ToDateTime(ResyncDateTime));
                        }
                        if (objBuss.UpdateTriggerStatus(SyncTableList) && objBuss.UpdateSyncPriority(SyncPriorityList))
                        {
                            RestartSyncService();
                            System.Windows.MessageBox.Show("Sync settings saved successfully.", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("There is some error occurred!", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void SaveResyncHistory(DateTime dateTime)
        {
            try
            {
                SyncStatusBusiness buss = new DigiPhoto.IMIX.Business.SyncStatusBusiness();
                bool issaved = buss.SetResyncHistory(dateTime, 0, 1);
            }
            catch { }
        }
        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            SyncTableList.ForEach(t => t.IsSyncTriggerEnable = (bool)chkSelectAll.IsChecked);
            listTables.ItemsSource = null;
            listTables.ItemsSource = SyncTableList;
        }

        private void chkitems_Click(object sender, RoutedEventArgs e)
        {
            if (SyncTableList.Where(o => o.IsSyncTriggerEnable == false).ToList().Count > 0)
            {
                chkSelectAll.IsChecked = false;
            }
            else if (SyncTableList.Where(o => o.IsSyncTriggerEnable == false).ToList().Count == 0)
            {
                chkSelectAll.IsChecked = true;
            }
        }

        private void GetSyncConfigDetails()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> _objdata = conBiz.GetNewConfigValues(Config.SubStoreId);
                if (_objdata != null && _objdata.Count > 0)
                {
                    foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                    {
                        switch (imixConfigValue.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.SyncServiceInterval:
                                txtSyncServiceInterval.Text = imixConfigValue.ConfigurationValue;
                                prevSyncSI = imixConfigValue.ConfigurationValue.ToInt32();
                                break;
                            case (int)ConfigParams.SyncDataDelay:
                                txtSyncDataDelay.Text = imixConfigValue.ConfigurationValue;
                                prevSyncDD = imixConfigValue.ConfigurationValue.ToInt32();
                                break;
                            case (int)ConfigParams.ReSyncDataDelay:
                                txtReSyncDataDelay.Text = imixConfigValue.ConfigurationValue;
                                prevReSyncDD = imixConfigValue.ConfigurationValue.ToInt32();
                                break;
                            case (int)ConfigParams.IsResyncActive:
                                chkReSyncFailedOnlineOrder.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsResyncActive) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsResyncActive] : false.ToString());
                                break;
                            case (int)ConfigParams.InitialResyncDateTime:
                                string date = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.InitialResyncDateTime) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.InitialResyncDateTime] : DateTime.Now.ToString();
                                if (!string.IsNullOrEmpty(date))
                                {
                                    ccResyncDateTime.Value = Convert.ToDateTime(date);
                                }
                                break;
                            case (int)ConfigParams.ResyncMaxRetryCount:
                                txtReSyncMaxCount.Text = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResyncMaxRetryCount) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.ResyncMaxRetryCount] : "5";
                                break;
                            case (int)ConfigParams.ResyncIntervalCount:
                                cmbSelectRecursiveCount.SelectedItem = imixConfigValue.ConfigurationValue.ToString();
                                break;
                            case (int)ConfigParams.ResyncIntervalType:
                                cmbSelectRecursiveCount.SelectedItem = imixConfigValue.ConfigurationValue.ToString();
                                break;
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }
        private bool SaveConfigValue(string SyncServiceInterval, string SyncDataDelay, string ReSyncDataDelay, bool SyncRetryActive, string ResyncDateTime, int IntervalType, int IntervalCount, int MaxRetryCount)
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                //_objDataLayer = new DigiPhotoDataServices();
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = SyncServiceInterval;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.SyncServiceInterval;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = SyncDataDelay;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.SyncDataDelay;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = ReSyncDataDelay;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ReSyncDataDelay;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = SyncRetryActive.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsResyncActive;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                if (SyncRetryActive)
                {
                    ConfigValue = new iMIXConfigurationInfo();
                    ConfigValue.ConfigurationValue = ResyncDateTime;
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.InitialResyncDateTime;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                    ConfigValue = new iMIXConfigurationInfo();
                    ConfigValue.ConfigurationValue = IntervalType.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ResyncIntervalType;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                    ConfigValue = new iMIXConfigurationInfo();
                    ConfigValue.ConfigurationValue = IntervalCount.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ResyncIntervalCount;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                    ConfigValue = new iMIXConfigurationInfo();
                    ConfigValue.ConfigurationValue = MaxRetryCount.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ResyncMaxRetryCount;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                }

                ConfigBusiness conBiz = new ConfigBusiness();
                bool IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);


                return true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;

            }
        }
        private bool validateInput()
        {
            if (string.IsNullOrEmpty(txtSyncServiceInterval.Text))
            {
                System.Windows.MessageBox.Show("Please enter Sync Service Interval!", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (string.IsNullOrEmpty(txtSyncDataDelay.Text))
            {
                System.Windows.MessageBox.Show("Please enter Sync Data Delay!", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void RestartSyncService()
        {
            if (prevSyncSI != txtSyncServiceInterval.Text.ToInt32() || prevSyncDD != txtSyncDataDelay.Text.ToInt32())
            {
                //Process[] processlist;
                //processlist = Process.GetProcessesByName("DataSyncWinService");//project exe name is different than service name
                ServiceController sc = new ServiceController("DigiphotoDataSyncService");
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        RestartService("DigiphotoDataSyncService", 30000);
                        break;
                }
            }
        }
        public void RestartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        #region Sync Priority Changes Ashirwad
        private void SyncPrioritylistDisplay_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (SyncPrioritylistDisplay.SelectedIndex <= 3)
            {
                btnUp.IsEnabled = false;
            }
            else
            {
                btnUp.IsEnabled = true;
            }
            if (SyncPrioritylistDisplay.SelectedIndex <= 2 || SyncPrioritylistDisplay.SelectedIndex >= (SyncPriorityList.Count - 1))
            {
                btnDown.IsEnabled = false;
            }
            else
            {
                btnDown.IsEnabled = true;
            }

        }
        #endregion
        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = this.SyncPrioritylistDisplay.SelectedIndex;

            if (selectedIndex > 3)
            {
                SyncPriority itemToMoveUp = (SyncPriority)this.SyncPrioritylistDisplay.Items[selectedIndex];                
                this.SyncPriorityList.RemoveAt(selectedIndex);
                this.SyncPriorityList.Insert(selectedIndex - 1, itemToMoveUp);                
                this.SyncPrioritylistDisplay.SelectedIndex = selectedIndex - 1;
                SyncPriorityList[selectedIndex - 1].OnlineSyncPriority = SyncPriorityList[selectedIndex - 1].OnlineSyncPriority - 1;
                SyncPriorityList[selectedIndex].OnlineSyncPriority = SyncPriorityList[selectedIndex - 1].OnlineSyncPriority + 1;
            }
            
            SyncPrioritylistDisplay.ItemsSource = SyncPriorityList;
            SyncPrioritylistDisplay.Items.Refresh();
        }

        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = this.SyncPrioritylistDisplay.SelectedIndex;

            if (selectedIndex > 2 && selectedIndex <(SyncPriorityList.Count-1))
            {
                if (selectedIndex + 1 < this.SyncPrioritylistDisplay.Items.Count)
                {
                    var itemToMoveDown = this.SyncPriorityList[selectedIndex];                    
                    this.SyncPriorityList.RemoveAt(selectedIndex);
                    this.SyncPriorityList.Insert(selectedIndex + 1, itemToMoveDown);
                    this.SyncPrioritylistDisplay.SelectedIndex = selectedIndex + 1;
                }
                SyncPriorityList[selectedIndex + 1].OnlineSyncPriority = SyncPriorityList[selectedIndex + 1].OnlineSyncPriority + 1;
                SyncPriorityList[selectedIndex].OnlineSyncPriority = SyncPriorityList[selectedIndex + 1].OnlineSyncPriority - 1;
            }
            
            SyncPrioritylistDisplay.ItemsSource = SyncPriorityList;
            SyncPrioritylistDisplay.Items.Refresh();
        }

        private void fillRecursiveOptions()
        {
            cmbSelectRecursiveCount.Items.Clear();
            for (int d = 1; d < 30; d++)
            {
                cmbSelectRecursiveCount.Items.Add(d.ToString());
            }

            cmbSelectRecursiveTime.Items.Clear();
            cmbSelectRecursiveTime.Items.Add(IntervalType.Hour);
            cmbSelectRecursiveTime.Items.Add(IntervalType.Day);
            cmbSelectRecursiveCount.SelectedIndex = 0;
            cmbSelectRecursiveTime.SelectedIndex = 0;
        }
        private void ccBackupDateTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            DateTime schdlbackup = new DateTime();
            schdlbackup = ccResyncDateTime.Value.ToDateTime();

            if (schdlbackup.TimeOfDay.ToString() == "00:00:00")
            {
                ccResyncDateTime.Value = schdlbackup.AddHours(11);
            }
        }
    }
}
