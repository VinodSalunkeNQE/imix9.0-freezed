﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Text.RegularExpressions;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Others.xaml
    /// </summary>
    public partial class Others : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = null;
        PhotoBusiness photoBusiness = new PhotoBusiness();
        public Others()
        {
            InitializeComponent();
            FillProductCombo();
            SetDefaultBrightness();
            GetBriConData();
            GetFileWatcherConfig();
        }



        private void FillProductCombo()
        {
            Dictionary<string, string> lstProductList;
            lstProductList = new Dictionary<string, string>();
            try
            {
                // DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                ProductBusiness proBiz = new ProductBusiness();
                foreach (var item in proBiz.GetProductType().Where(t => t.DG_IsAccessory == false))
                {
                    if (item.DG_Orders_ProductType_pkey == 1 || item.DG_Orders_ProductType_pkey == 2 || item.DG_Orders_ProductType_pkey == 98 || item.DG_Orders_ProductType_pkey == 30 || item.DG_Orders_ProductType_pkey == 103)
                    {

                        lstProductList.Add(item.DG_Orders_ProductType_Name, item.DG_Orders_ProductType_pkey.ToString());
                    }
                }
                CmbProductType.ItemsSource = lstProductList;
                CmbProductType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }
        private void GetBriConData()
        {
            try
            {
                ProductBusiness proBiz = new ProductBusiness();
                // _objDataLayer = new DigiPhotoDataServices();
                foreach (var item in proBiz.GetProductType())
                {
                    if (item.DG_Orders_ProductType_pkey == 4 && item.DG_IsBorder == true)
                        chkUniq4.IsChecked = item.DG_IsBorder;


                    if (item.DG_Orders_ProductType_pkey == 5 && item.DG_IsBorder == true)
                        chk4small.IsChecked = item.DG_IsBorder;


                    if (item.DG_Orders_ProductType_pkey == 3 && item.DG_IsBorder == true)
                        chk4Large.IsChecked = item.DG_IsBorder;

                    if (item.DG_Orders_ProductType_pkey == 98 && item.DG_IsBorder == true)
                        chk3by3.IsChecked = item.DG_IsBorder;
                    if (item.DG_Orders_ProductType_pkey == 101 && item.DG_IsBorder == true)
                        chkUniq4SW.IsChecked = item.DG_IsBorder;
                }
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(Config.SubStoreId);
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.Contrast:
                                cVal.Value = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToDouble() : 1;
                                break;
                            case (int)ConfigParams.Brightness:
                                bVal.Value = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToDouble() : 0;
                                break;
                            case (int)ConfigParams.IsDeleteFromUSB:
                                chkDeleteUSBImageDown.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;
                            case (int)ConfigParams.MktImgPath:
                                txtMktImgPath.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "";
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                txtMktImgTime.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : 10.ToString();
                                break;
                            //FTP Detail
                            case (int)ConfigParams.FtpFolder:
                                txtFtpFolder.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.FtpIP:
                                txtFtpIP.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.FtpPwd:
                                txtFtpPwd.Password = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.FtpUid:
                                txtFtpUid.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.ProductPreview:
                                CmbProductType.SelectedValue = string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) == false ? ConfigValuesList[i].ConfigurationValue.ToString() : "0";
                                break;
                            case (int)ConfigParams.EnablePaymentScreenOnClientView:
                                chkenablepaymentscreen.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;

                            case (int)ConfigParams.Card:
                                rdbcard.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;

                            case (int)ConfigParams.Cash:
                                rdbcash.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;

                            case (int)ConfigParams.IsOrderwiseLoadBalancingActive:
                                chkOrderwisePrinting.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;
                            case (int)ConfigParams.CalenderPrintMargin:
                                txtCalenderMargin.Text = ConfigValuesList[i].ConfigurationValue;// != null ? ConfigValuesList[i].ConfigurationValue : "0";
                                break;
                            case (int)ConfigParams.MarginValue4X6_2:
                                txtMarginValue4X6_2.Text = ConfigValuesList[i].ConfigurationValue;// != null ? ConfigValuesList[i].ConfigurationValue : "0";
                                break;
                            case (int)ConfigParams.MarginValueUnique4X6_2:
                                txtMarginValueUnique4X6_2.Text = ConfigValuesList[i].ConfigurationValue;// != null ? ConfigValuesList[i].ConfigurationValue : "0";
                                break;
                            // Added by Suraj For Set 3 Flag Value
                            case (int)ConfigParams.IsSet3Flag:
                                chkSet3EnabledStatus.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;
                                // End Changes
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void GetFileWatcherConfig()
        {
            try
            {
                string ConfigValue = photoBusiness.GetFileWatcherConfigValues(Config.SubStoreId);
                if (!String.IsNullOrEmpty(ConfigValue))
                {
                    if (ConfigValue == "True")
                    {
                        rdbParallel.IsChecked = true;
                        rdbSequential.IsChecked = false;
                    }
                    else
                    {
                        rdbParallel.IsChecked = false;
                        rdbSequential.IsChecked = true;
                    }
                }
            }
            catch(Exception)
            {

            }
        }

        private void btnResetBriCon_Click(object sender, RoutedEventArgs e)
        {
            SetDefaultBrightness();
        }

        private void SetDefaultBrightness()
        {
            cVal.Value = 1;
            bVal.Value = 0;
        }

        private void btnSaveBriCon_Click(object sender, RoutedEventArgs e)
        {
            string mktImgTime = txtMktImgTime.Text;
            string sPattern = "^[0-9]{0,3}$";
            if (Config.SubStoreId <= 0)
            {
                MessageBox.Show("Please configure your SubStore.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (!string.IsNullOrEmpty(txtCalenderMargin.Text))
            {
                Regex regex = new Regex(@"^[0-9]{0,4}$");
                if (!regex.IsMatch(txtCalenderMargin.Text.Trim()))
                {
                    MessageBox.Show("Invalid value for Calendar print margin!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            if (!string.IsNullOrEmpty(txtMarginValue4X6_2.Text))
            {
                Regex regex = new Regex(@"^[0-9]{0,4}$");
                if (!regex.IsMatch(txtMarginValue4X6_2.Text.Trim()))
                {
                    MessageBox.Show("Invalid value for 4*6(2) product print margin!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            if (!string.IsNullOrEmpty(txtMarginValueUnique4X6_2.Text))
            {
                Regex regex = new Regex(@"^[0-9]{0,4}$");
                if (!regex.IsMatch(txtMarginValueUnique4X6_2.Text.Trim()))
                {
                    MessageBox.Show("Invalid value for Unique 4*6(2) product print margin!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            if (System.Text.RegularExpressions.Regex.IsMatch(mktImgTime, sPattern))
            {
                SetBriConData();
            }
            else
            {
                MessageBox.Show("Only Integers allowed in Time.");
                txtMktImgTime.Text = "";
            }
            SaveFileWatcherConfig();
        }

        private void SetBriConData()
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                int substoreId = Config.SubStoreId;
                double configBrightness = bVal.Value;
                double configContrast = cVal.Value;
                // _objDataLayer = new DigiPhotoDataServices();
                if (txtMktImgTime.Text == "")
                    txtMktImgTime.Text = "0";
                //Brighteness
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = configBrightness.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.Brightness;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                //configContrast
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = configContrast.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.Contrast;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //chkDeleteUSBImageDown
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = chkDeleteUSBImageDown.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsDeleteFromUSB;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //MktImgPath
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtMktImgPath.Text.Trim();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.MktImgPath;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //MktImgTimeInSec
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtMktImgTime.Text.Trim().ToInt32().ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.MktImgTimeInSec;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //FtpFolder
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtFtpFolder.Text.Trim();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.FtpFolder;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //FtpIP
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtFtpIP.Text.Trim();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.FtpIP;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //FtpPwd
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtFtpPwd.Password.Trim();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.FtpPwd;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //FtpUid
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtFtpUid.Text.Trim();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.FtpUid;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //Product Preview
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = CmbProductType.SelectedValue.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ProductPreview;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //Enable Payment Screen on client view

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = chkenablepaymentscreen.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.EnablePaymentScreenOnClientView;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                // Payment mode (Card/cash)

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = rdbcard.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.Card;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = rdbcash.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.Cash;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = chkOrderwisePrinting.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsOrderwiseLoadBalancingActive;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtCalenderMargin.Text;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.CalenderPrintMargin;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);//

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtMarginValue4X6_2.Text;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.MarginValue4X6_2;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = txtMarginValueUnique4X6_2.Text;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.MarginValueUnique4X6_2;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //Save set3 enabled configs_by VINS_18Jun2020
                ConfigBusiness conBiz = new ConfigBusiness();
                ConfigValue = new iMIXConfigurationInfo();
                //ConfigValue.ConfigurationValue = Convert.ToString(chkSet3EnabledStatus.IsChecked);
                ConfigValue.ConfigurationValue = "True";
                ConfigValue.IMIXConfigurationMasterId = (int)conBiz.GETSet3EnabledID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = Convert.ToString(rdbDay.IsChecked);
                ConfigValue.IMIXConfigurationMasterId = (int)conBiz.GETSet3DayWiseID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = Convert.ToString(rdbHours.IsChecked);
                ConfigValue.IMIXConfigurationMasterId = (int)conBiz.GETSet3HourWiseID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                //Save set3 enabled configs_by VINS

                //ConfigBusiness conBiz = new ConfigBusiness();
                bool IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);

                bool ApplyBorderFor4Large = (bool)chk4Large.IsChecked;// chklogo.IsChecked == null ? false : (bool)chk4Large.IsChecked;
                bool ApplyBorderForUniq4 = (bool)chkUniq4.IsChecked;// chklogo.IsChecked == null ? false : (bool)chkUniq4.IsChecked;
                bool ApplyBorderFor4small = (bool)chk4small.IsChecked;// chklogo.IsChecked == null ? false : (bool)chk4small.IsChecked;
                bool ApplyBorderFor3by3 = (bool)chk3by3.IsChecked;
                bool ApplyBorderForUniq4SW = (bool)chkUniq4SW.IsChecked;
                ProductBusiness proBiz = new ProductBusiness();
                bool IsborderApply = proBiz.SaveBorderFor4Images(ApplyBorderFor4Large, ApplyBorderForUniq4, ApplyBorderFor4small, ApplyBorderFor3by3, ApplyBorderForUniq4SW);

                if (IsSaved)
                {
                    MessageBox.Show("Others configuration detailed updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    GetBriConData();
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void SaveFileWatcherConfig()
        {
            try
            {
                string IsFileConfig = string.Empty;
                if ((bool)rdbParallel.IsChecked)
                {
                    IsFileConfig = "True";
                }
                else if ((bool)rdbSequential.IsChecked)
                {
                    IsFileConfig = "False";
                }
                else
                {
                    IsFileConfig = "False";
                }
                photoBusiness.SaveFileWatcherConfig(IsFileConfig, Config.SubStoreId);
            }
            catch(Exception)
            {

            }
        }
        /// <summary>
        /// Handles the Click event of the btnhotfolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnhotfolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                var result = fbDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtMktImgPath.Text = fbDialog.SelectedPath + "\\";
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
    }
}

namespace CustomPixelRender
{
    public class BrightContrastEffect : ShaderEffect
    {
        private static string executableLocation = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static string xslLocation = System.IO.Path.Combine(executableLocation, "Shader\\bricon.ps");
        private static PixelShader m_shader =
            new PixelShader()
            {
                UriSource = new Uri(xslLocation)
            };

        public BrightContrastEffect()
        {
            PixelShader = m_shader;
            UpdateShaderValue(InputProperty);
            UpdateShaderValue(BrightnessProperty);
            UpdateShaderValue(ContrastProperty);
        }

        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(BrightContrastEffect), 0);

        public float Brightness
        {
            get { return (float)GetValue(BrightnessProperty); }
            set { SetValue(BrightnessProperty, value); }
        }

        public static readonly DependencyProperty BrightnessProperty = DependencyProperty.Register("Brightness", typeof(double), typeof(BrightContrastEffect), new UIPropertyMetadata(0.0, PixelShaderConstantCallback(0)));

        public float Contrast
        {
            get { return (float)GetValue(ContrastProperty); }
            set { SetValue(ContrastProperty, value); }
        }

        public static readonly DependencyProperty ContrastProperty = DependencyProperty.Register("Contrast", typeof(double), typeof(BrightContrastEffect), new UIPropertyMetadata(0.0, PixelShaderConstantCallback(1)));



    }

}
