﻿using DigiConfigUtility.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Types;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for GeneralEffects.xaml
    /// </summary>
    public partial class GeneralEffectsControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public GeneralEffectsControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;   
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
       
        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            return _result;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }
        private void UnsubscribeEvents()
        {
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
        }
        private void chkPreview_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void radioStandardPreview_Checked(object sender, RoutedEventArgs e)
        {
           
        }
         private void radioTemplatePreview_Checked(object sender, RoutedEventArgs e)
        {
           
        }
        private void PlayFile(string fileName)
        {
           
        }
      
        private void tbLightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
          
        }
        private void tbSaturation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           
        }
        private void tbContrast_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           
        }  
        private void tbDarkness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
        }
        private void chkGrayScale_Checked(object sender, RoutedEventArgs e)
        {
            
        }
        private void chkInvert_Checked(object sender, RoutedEventArgs e)
        {
            
        }

    }
}
