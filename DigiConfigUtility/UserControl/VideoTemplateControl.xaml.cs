﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class VideoTemplateControl : UserControl
    {
        private UIElement _parent;
        private UIElement _parentBorder;
        private bool _result = false;
        public List<SlotProperty> videoSlotlist = new List<SlotProperty>();
        int uniqueID = 1;
        public List<VideoTemplateInfo.VideoSlot> slotList;
        List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
        public int videoExpLen = 0;
        int EditVideoID = 0;
        bool IsVideoEdited = false;

        public VideoTemplateControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }

        public void SetParent(UIElement parent, UIElement parentBorder)
        {
            _parent = (UIElement)parent;
            _parentBorder = (UIElement)parentBorder;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
            //((VideoConfig)_parent).SetVideoLength(videoSlotlist);
        }
        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
                _parentBorder.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }            
        }
        public void ControlListAutoFill()
        {
            DGManageVideo.ItemsSource = videoSlotlist;
            DGManageVideo.Items.Refresh();
        }
        public bool ShowHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Visible;
                _parentBorder.IsEnabled = false;
                //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
                ConfigBusiness configBusiness = new ConfigBusiness();
                TemplateListItems tmpList;
                /// <summary>
                /// Loads the VideoTemplate.
                /// </summary>
                List<VideoTemplateInfo> objVideoTemplatesList = configBusiness.GetVideoTemplate();
                objTemplateList.Clear();
                for (int i = 0; i < objVideoTemplatesList.Count; i++)
                {
                    if (objVideoTemplatesList[i].IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.FilePath = System.IO.Path.GetFileName(objVideoTemplatesList[i].Name); ;
                        tmpList.Item_ID = objVideoTemplatesList[i].VideoTemplateId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = objVideoTemplatesList[i].DisplayName;
                        tmpList.MediaType = 604;
                        tmpList.Length = objVideoTemplatesList[i].VideoLength;
                        tmpList.StartTime = 0;
                        tmpList.EndTime = (int)tmpList.Length;
                        tmpList.InsertTime = 0;
                        tmpList.Name = LoginUser.DigiFolderAudioPath + System.IO.Path.GetFileName(objVideoTemplatesList[i].Name);
                        tmpList.Tooltip = "Video Template\n" + "Name : " + tmpList.DisplayName + "\n" + "Length: " + tmpList.Length.ToString() + " sec";
                        objTemplateList.Add(tmpList);
                    }
                }
                cmbVideo.ItemsSource = null;
                cmbVideo.ItemsSource = objTemplateList; //((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608).ToList();
                UpdateLayout();
                if (objTemplateList.Count > 0)
                {
                    cmbVideo.SelectedItem = objTemplateList[0];
                    videoExpLen = Convert.ToInt32(objTemplateList[0].Length);
                    txtStopTime.Text = videoExpLen.ToString();
                    if (videoExpLen > 0)
                    {
                        sldRange.Maximum = videoExpLen;
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return _result;
            }
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteSlots_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                long SlotId = long.Parse(btnSender.Tag.ToString());
                MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    SlotProperty brdlst = videoSlotlist.Where(o => o.ID == SlotId).FirstOrDefault();
                    long borderid = (videoSlotlist.Where(o => o.ID == SlotId).FirstOrDefault()).ItemID;
                    videoSlotlist.RemoveAll(o => o.ID == SlotId);
                    if (videoSlotlist.Where(o => o.ItemID == borderid).Count() == 0)
                    {
                        TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == borderid).FirstOrDefault();
                        tmp.IsChecked = false;
                    }
                    MessageBox.Show("Item deleted successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    DGManageVideo.ItemsSource = videoSlotlist;
                    DGManageVideo.Items.Refresh();
                    if (videoSlotlist.Count == 0)
                    {
                        uniqueID = 151;
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string strtemp = string.Empty;
                if (string.IsNullOrEmpty(((TextBox)sender).Text))
                    strtemp = "";
                else
                {
                    int num = 0;
                    bool success = int.TryParse(((TextBox)sender).Text, out num);
                    if (success & num >= 0)
                    {
                        ((TextBox)sender).Text.Trim();
                        strtemp = ((TextBox)sender).Text;
                    }
                    else
                    {
                        ((TextBox)sender).Text = strtemp;
                        ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                    }
                }
            } 
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtStartTime.Text.Trim()) || string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtStartTime.Text.Trim()) >= Convert.ToInt32(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time can not be less then or equal to the End time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UnsubscribeEvents()
        {
            // txtStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            // // txtStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            // btnAddAudio.Click -= new RoutedEventHandler(btnAddAudio_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            btnClearList.Click -= new RoutedEventHandler(btnClearList_Click);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //UnsubscribeEvents();
        }

        private void btnAddAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbVideo.Items.Count <= 0)
                {
                    MessageBox.Show("Please upload video template/s.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                SlotProperty videoSlot = new SlotProperty();
                int stopTime = 0;
                int startTime = 0;
                bool added = false;
                if (!string.IsNullOrEmpty(txtStartTime.Text))
                {
                    startTime = Convert.ToInt32(txtStartTime.Text);
                }
                if (!string.IsNullOrEmpty(txtStopTime.Text))
                {
                    stopTime = Convert.ToInt32(txtStopTime.Text);
                }
                long id = ((TemplateListItems)(cmbVideo.SelectionBoxItem)).Item_ID;

                //if (chkApplyVideoTemplate.IsChecked == true)
                //{
                if (CheckSlotValidations())
                {
                    if (IsVideoEdited)
                    {
                        IsVideoEdited = false;
                        MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (response == MessageBoxResult.Yes)
                        {
                            videoSlot = videoSlotlist.Where(o => o.ID == EditVideoID).FirstOrDefault();
                            videoSlotlist.Remove(videoSlot);
                            if (SlotValidation.IsValidSlot(videoSlotlist, startTime, stopTime))
                            {
                                videoSlot.Name = ((TemplateListItems)(cmbVideo.SelectionBoxItem)).DisplayName;
                                videoSlot.ItemID = id;
                                videoSlot.FilePath = ((TemplateListItems)(cmbVideo.SelectionBoxItem)).FilePath;
                                videoSlot.StartTime = Convert.ToInt32(txtStartTime.Text);
                                videoSlot.StopTime = Convert.ToInt32(txtStopTime.Text);
                                videoSlotlist.Add(videoSlot);
                            }
                            else
                            {
                                videoSlotlist.Add(videoSlot);
                            }
                        }
                        else
                        {
                            txtStartTime.Text = "0";
                            txtStopTime.Text = "0";
                            cmbVideo.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        if (SlotValidation.IsValidSlot(videoSlotlist, startTime, stopTime))
                        {
                            videoSlot.ID = uniqueID;
                            videoSlot.Name = ((TemplateListItems)(cmbVideo.SelectionBoxItem)).DisplayName;
                            videoSlot.ItemID = id;
                            videoSlot.FilePath = ((TemplateListItems)(cmbVideo.SelectionBoxItem)).FilePath;
                            videoSlot.StartTime = Convert.ToInt32(txtStartTime.Text);
                            videoSlot.StopTime = Convert.ToInt32(txtStopTime.Text);
                            videoSlotlist.Add(videoSlot);
                            uniqueID++;
                            added = true;
                        }
                    }
                    if (added)
                    {
                        TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == id).FirstOrDefault();
                        tmp.IsChecked = true;
                    }
                    DGManageVideo.ItemsSource = videoSlotlist.OrderBy(o => o.StartTime);
                    DGManageVideo.Items.Refresh();

                }
                //}
                //else
                //{
                //    MessageBox.Show("Please check Apply Audio.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                //}
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (videoSlotlist != null)
            {
                videoSlotlist.Clear();
                DGManageVideo.ItemsSource = videoSlotlist;
                DGManageVideo.Items.Refresh();
                objTemplateList.Where(o => o.MediaType == 604 && o.IsChecked == true).ToList().ForEach(t => t.IsChecked = false);
            }
        }

        private void cmbVideo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var cmbselectedItem = cmbVideo.SelectedItem;
                if (cmbselectedItem != null)
                {
                    SlotProperty audioProp = new SlotProperty();
                    long length = ((TemplateListItems)(cmbselectedItem)).Length;
                    sldRange.Maximum = length;
                    txtStopTime.Text = length.ToString();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private bool IsValidSlot(List<SlotProperty> slotList, int startTime, int stopTime)
        {
            try
            {
                SlotProperty firstItem = slotList.OrderBy(o => o.StartTime).FirstOrDefault();
                SlotProperty LastItem = slotList.OrderBy(o => o.StartTime).LastOrDefault();
                if (firstItem != null)
                {
                    if (stopTime <= firstItem.StartTime)
                    {
                        return true;
                    }
                    else if (startTime >= LastItem.StopTime)
                    {
                        return true;
                    }
                    else
                    {
                        List<SlotProperty> templst = slotList.OrderBy(o => o.StartTime).ToList();
                        SlotProperty item = templst.Where(o => o.StopTime <= startTime).LastOrDefault();
                        SlotProperty item2 = templst[templst.IndexOf(item) + 1];
                        if (item2 != null)
                        {
                            if (item2.StartTime >= stopTime)
                            {
                                return true;
                            }
                            else
                            {
                                MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            } 
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return true;
            }
        }

        private void btnEditVideoSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditVideoID = Convert.ToInt32(btnSender.Tag.ToString());
            SlotProperty videoSlot = videoSlotlist.Where(o => o.ID == EditVideoID).FirstOrDefault();
            TemplateListItems video = objTemplateList.Where(o => o.Item_ID == videoSlot.ItemID).FirstOrDefault();
            cmbVideo.SelectedItem = video;
            txtStartTime.Text = videoSlot.StartTime.ToString();
            txtStopTime.Text = videoSlot.StopTime.ToString();
            IsVideoEdited = true;
        }
        private void txtStopTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStopTime.Text.Trim()) > sldRange.Maximum)
                    txtStopTime.Text = sldRange.Maximum.ToString();
            }
        }

        private void txtStartTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStartTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStartTime.Text.Trim()) > sldRange.Maximum)
                    txtStartTime.Text = "0";
            }
        }    

    }

}
