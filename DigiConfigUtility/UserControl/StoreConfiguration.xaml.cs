﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.ServiceProcess;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for StoreConfiguration.xaml
    /// </summary>
    public partial class StoreConfiguration : System.Windows.Controls.UserControl
    {
        public StoreConfiguration()
        {
            InitializeComponent();
            //GetStoreConfigValue();
            LoadFontStyle();
            LoadTextAlign();
            GetStoreConfigData();
            GetAllSubstoreConfigdata();
        }
        iMIXStoreConfigurationInfo ConfigValue;
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //string errorMessage = ValidateInput();
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<iMIXStoreConfigurationInfo> lstImixStoreConfiguration = new List<iMIXStoreConfigurationInfo>();

                ConfigValue = new iMIXStoreConfigurationInfo();
                ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.DynamicBGFunctionalityActive;
                ConfigValue.ConfigurationValue = ChkDynamicBGFunctionalityActive.IsChecked == true ? "true" : "false";
                lstImixStoreConfiguration.Add(ConfigValue);

                ConfigValue = new iMIXStoreConfigurationInfo();
                ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.TripCameraPropertyPassword;
                ConfigValue.ConfigurationValue = txtTripPassword.Text;
                lstImixStoreConfiguration.Add(ConfigValue);

                ConfigValue = new iMIXStoreConfigurationInfo();
                ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.SmallWalletWith4ImageCutter;
                ConfigValue.ConfigurationValue = chkSmallWalletWith4ImgCutter.IsChecked == true ? "true" : "false";
                lstImixStoreConfiguration.Add(ConfigValue);

                //string logoSourcePath = txtLogoPath.Text.Trim();
                //string logoDestPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo" + System.IO.Path.GetExtension(logoSourcePath));
                ConfigValue = new iMIXStoreConfigurationInfo();
                ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.ReceiptLogoPath;
                ConfigValue.ConfigurationValue = System.IO.Path.GetFileName(txtLogoPath.Text.Trim());
                lstImixStoreConfiguration.Add(ConfigValue);

                bool result = configBusiness.SaveUpdateNewStoreConfig(lstImixStoreConfiguration);
                if (result == true)
                    MessageBox.Show("Record saved successfully");
                GetStoreConfigData();
            }


            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void SaveLogo(string logoSourcePath, string logoDestPath)
        {
            try
            {
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo")))
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo"));
                }
                if (System.IO.File.Exists(txtLogoPath.Text.Trim()))
                {
                    System.IO.File.Copy(logoSourcePath, logoDestPath, true);
                    //CopyToAllSubstore(lstConfigurationInfo, logoSourcePath);
                }
            }
            catch { }
        }
        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();

            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.DynamicBGFunctionalityActive:
                        ChkDynamicBGFunctionalityActive.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                        break;
                    case (int)ConfigParams.TripCameraPropertyPassword:
                        txtTripPassword.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : string.Empty;
                        break;
                    case (int)ConfigParams.QrCodeOnlineProductText:
                        FillValue(ConfigValuesList[i].ConfigurationValue);
                        break;
                    case (int)ConfigParams.QrCodeOnlineProductURL:
                        FillQrCodeValue(ConfigValuesList[i].ConfigurationValue);
                        //txtQrCode.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : string.Empty;                      
                        break;
                    case (int)ConfigParams.SmallWalletWith4ImageCutter:
                        chkSmallWalletWith4ImgCutter.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                        break;
                    case (int)ConfigParams.ReceiptLogoPath:
                        txtLogoPath.Text = String.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) ? string.Empty : System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo", ConfigValuesList[i].ConfigurationValue);
                        if (string.IsNullOrEmpty(txtLogoPath.Text))
                        {
                            imgLogo.Source = null;
                            imgLogo.UpdateLayout();
                        }
                        if (!string.IsNullOrEmpty(txtLogoPath.Text.Trim()) && System.IO.File.Exists(txtLogoPath.Text.Trim()))
                            SaveImg(txtLogoPath.Text.Trim());
                        break;

                }
            }

        }

        private string ValidateInput()
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(txtTripPassword.Text))
                return errorMessage = "Please set password.";

            return null;
        }

        private string ValidateQrCodeInput()
        {
            if (string.IsNullOrWhiteSpace(txtTopMsg.Text))
            {
                return "Please fill description for Top Message";
            }
            if (string.IsNullOrWhiteSpace(txtMidMsg.Text))
            {
                return "Please fill description for Mid Message(Right) ";
            }
            if (string.IsNullOrWhiteSpace(txtBotMsg.Text))
            {
                return "Please fill description for Mid Message(Left) ";
            }
            if (string.IsNullOrWhiteSpace(txtQrCode.Text))
            {
                return "Please fill description for Bottom Message ";
            }

            if (txtFontSize.Text == "0" || string.IsNullOrEmpty(txtFontSize.Text))
            {
                return "Font size should be greater than zero (0) for Top Message";
            }
            if (txtFontSize2.Text == "0" || string.IsNullOrEmpty(txtFontSize2.Text))
            {
                return "Font size should be greater than zero (0) for Mid Message(Right)";
            }
            if (txtFontSize3.Text == "0" || string.IsNullOrEmpty(txtFontSize3.Text))
            {
                return "Font size should be greater than zero (0) for Mid Message(Left)";
            }
            if (txtFontSize4.Text == "0" || string.IsNullOrEmpty(txtFontSize4.Text))
            {
                return "Font size should be greater than zero (0) for Bottom Message";
            }


            if (!IsNumber(txtFontSize.Text))
            {
                return "Please enter only numeric value in fontsize for Top Message";
            }
            if (!IsNumber(txtFontSize2.Text))
            {
                return "Please enter only numeric value in fontsize for Mid Message(Right)";
            }
            if (!IsNumber(txtFontSize3.Text))
            {
                return "Please enter only numeric value in fontsize for Mid Message(Left)";
            }
            if (!IsNumber(txtFontSize4.Text))
            {
                return "Please enter only numeric value in fontsize for Bottom Message";
            }

            if (Convert.ToInt16(txtFontSize.Text) > 12)
            {
                return "Font size will not be greater than 12 for Top Message";
            }
            if (Convert.ToInt16(txtFontSize2.Text) > 12)
            {
                return "Font size will not be greater than 12 for Mid Message(Right)";
            }
            if (Convert.ToInt16(txtFontSize3.Text) > 12)
            {
                return "Font size will not be greater than 12 for Mid Message(Left)";
            }
            if (Convert.ToInt16(txtFontSize4.Text) > 12)
            {
                return "Font size will not be greater than 12 for Bottom Message";
            }

            if ((CmbFontFamily.Text == "Arial") && Convert.ToInt16(txtFontSize.Text) > 9)
            {
                return "For Arial font family font size can not be greater than 9 for Top Message";
            }

            if ((CmbFontFamily2.Text == "Arial") && Convert.ToInt16(txtFontSize2.Text) > 9)
            {
                return "For Arial font family font size can not be greater than 9 for Mid Message(Right)";
            }

            if ((CmbFontFamily3.Text == "Arial") && Convert.ToInt16(txtFontSize3.Text) > 9)
            {
                return "For Arial font family font size can not be greater than 9 for Mid Message(Left)";
            }

            if ((CmbFontFamily4.Text == "Arial") && Convert.ToInt16(txtFontSize4.Text) > 9)
            {
                return "For Arial font family font size can not be greater than 9 for Bottom Message";
            }




            if ((CmbFontFamily.Text == "Times New Roman") && Convert.ToInt16(txtFontSize.Text) > 9)
            {
                return "For Times New Roman font family font size can not be greater than 9 for Top Message";
            }

            if ((CmbFontFamily2.Text == "Times New Roman") && Convert.ToInt16(txtFontSize2.Text) > 9)
            {
                return "For Times New Roman font family font size can not be greater than 9 for Mid Message(Right)";
            }

            if ((CmbFontFamily3.Text == "Times New Roman") && Convert.ToInt16(txtFontSize3.Text) > 9)
            {
                return "For Times New Roman font family font size can not be greater than 9 for Mid Message(Left)";
            }

            if ((CmbFontFamily4.Text == "Times New Roman") && Convert.ToInt16(txtFontSize4.Text) > 9)
            {
                return "For Times New Roman font family font size can not be greater than 9 for Bottom Message";
            }

            if ((CmbFontFamily.Text == "Calibri") && Convert.ToInt16(txtFontSize.Text) > 10)
            {
                return "For Calibri font family font size can not be greater than 10 for Top Message";
            }

            if ((CmbFontFamily2.Text == "Calibri") && Convert.ToInt16(txtFontSize2.Text) > 10)
            {
                return "For Calibri font family font size can not be greater than 10 for Mid Message(Right)";
            }

            if ((CmbFontFamily3.Text == "Calibri") && Convert.ToInt16(txtFontSize3.Text) > 10)
            {
                return "For Calibri font family font size can not be greater than 10 for Mid Message(Left)";
            }

            if ((CmbFontFamily4.Text == "Calibri") && Convert.ToInt16(txtFontSize4.Text) > 10)
            {
                return "For Calibri font family font size can not be greater than 10 for Bottom Message";
            }


            return null;
        }
        private void LoadFontStyle()
        {


            List<string> fonts = new List<string>();
            System.Drawing.Text.InstalledFontCollection col = new System.Drawing.Text.InstalledFontCollection();
            foreach (System.Drawing.FontFamily family in col.Families)
            {
                if (family.Name == "Calibri" || family.Name == "Times New Roman" || family.Name == "Arial")
                    fonts.Add(family.Name);
            }
            CmbFontFamily.ItemsSource = fonts;
            CmbFontFamily2.ItemsSource = fonts;
            CmbFontFamily3.ItemsSource = fonts;
            CmbFontFamily4.ItemsSource = fonts;
            CmbFontFamily.Text = "Calibri";
            CmbFontFamily2.Text = "Calibri";
            CmbFontFamily3.Text = "Calibri";
            CmbFontFamily4.Text = "Calibri";
        }

        private void LoadTextAlign()
        {



            List<string> textalign = new List<string>();

            textalign.Add("Center");
            textalign.Add("Right");
            textalign.Add("Left");
            textalign.Add("Justify");

            CmbTextAlign.ItemsSource = textalign;
            CmbTextAlign2.ItemsSource = textalign;
            CmbTextAlign3.ItemsSource = textalign;
            CmbTextAlign4.ItemsSource = textalign;
            CmbTextAlign.Text = "Center";
            CmbTextAlign2.Text = "Center";
            CmbTextAlign3.Text = "Center";
            CmbTextAlign4.Text = "Center";
        }

        private void btnTextSave_Click(object sender, RoutedEventArgs e)
        {

            string errorMsg = ValidateQrCodeInput();
            if (errorMsg == null)
            {
                string Text1 = txtTopMsg.Text + "@@@@" + cmbSelectColorBackground.SelectedColor.ToString() + "@@@@" + CmbFontFamily.SelectedValue
                                                 + "@@@@" + CmbFontStyle.SelectionBoxItem + "@@@@" + chkIsUnderline.IsChecked.ToString() + "@@@@" +
                                                 cmbSelectForeColor.SelectedColor.ToString() + "@@@@" + txtFontSize.Text + "@@@@" + chkIsBold.IsChecked.ToString()
                                                 + "@@@@" + CmbTextAlign.SelectionBoxItem;
                string Text2 = txtMidMsg.Text + "@@@@" + cmbSelectColorBackground2.SelectedColor.ToString() + "@@@@" + CmbFontFamily2.SelectedValue
                                                 + "@@@@" + CmbFontStyle2.SelectionBoxItem + "@@@@" + chkIsUnderline2.IsChecked.ToString() + "@@@@" +
                                                 cmbSelectForeColor2.SelectedColor.ToString() + "@@@@" + txtFontSize2.Text + "@@@@" + chkIsBold2.IsChecked.ToString()
                                                 + "@@@@" + CmbTextAlign2.SelectionBoxItem;
                string Text3 = txtBotMsg.Text + "@@@@" + cmbSelectColorBackground3.SelectedColor.ToString() + "@@@@" + CmbFontFamily3.SelectedValue
                                                 + "@@@@" + CmbFontStyle3.SelectionBoxItem + "@@@@" + chkIsUnderline3.IsChecked.ToString() + "@@@@" +
                                                 cmbSelectForeColor3.SelectedColor.ToString() + "@@@@" + txtFontSize3.Text + "@@@@" + chkIsBold3.IsChecked.ToString()
                                                 + "@@@@" + CmbTextAlign3.SelectionBoxItem;

                string TexturlText = txtQrCode.Text + "@@@@" + cmbSelectColorBackground4.SelectedColor.ToString() + "@@@@" + CmbFontFamily4.SelectedValue
                                                + "@@@@" + CmbFontStyle4.SelectionBoxItem + "@@@@" + chkIsUnderline4.IsChecked.ToString() + "@@@@" +
                                                cmbSelectForeColor4.SelectedColor.ToString() + "@@@@" + txtFontSize4.Text + "@@@@" + chkIsBold4.IsChecked.ToString()
                                                + "@@@@" + CmbTextAlign4.SelectionBoxItem;

                string TxtShowHideLine = chkshowHideLine.IsChecked.ToString();

                string FinalText = Text1 + "@@##" + Text2 + "@@##" + Text3 + "@@##" + TxtShowHideLine;

                try
                {
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    List<iMIXStoreConfigurationInfo> lstImixStoreConfiguration = new List<iMIXStoreConfigurationInfo>();

                    ConfigValue = new iMIXStoreConfigurationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.QrCodeOnlineProductText;
                    ConfigValue.ConfigurationValue = FinalText;
                    lstImixStoreConfiguration.Add(ConfigValue);

                    ConfigValue = new iMIXStoreConfigurationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (long)ConfigParams.QrCodeOnlineProductURL;
                    ConfigValue.ConfigurationValue = TexturlText;
                    lstImixStoreConfiguration.Add(ConfigValue);

                    bool result = configBusiness.SaveUpdateNewStoreConfig(lstImixStoreConfiguration);
                    if (result == true)
                    {
                        // MessageBox.Show(errorMsg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                        MessageBox.Show("QR Print & Online Product Setting saved successfully", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        //ResetControls();
                        GetStoreConfigData();
                    }
                }
                catch (Exception ex)
                {
                    LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                }
            }
            else
            {
                MessageBox.Show(errorMsg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        //private void ResetControls()
        //{
        //    txtTopMsg.Text = "";
        //    txtMidMsg.Text = "";
        //    txtBotMsg.Text = "";
        //    txtQrCode.Text = "";
        //    System.Windows.Media.Color color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#00FFFFFF"));
        //    cmbSelectColorBackground.SelectedColor = color;
        //    cmbSelectColorBackground2.SelectedColor = color;
        //    cmbSelectColorBackground3.SelectedColor = color;

        //    CmbFontFamily.SelectedValue = "Times New Roman";
        //    CmbFontFamily2.SelectedValue = "Times New Roman";
        //    CmbFontFamily3.SelectedValue = "Times New Roman";

        //    CmbFontStyle.SelectedValue = "Normal";
        //    CmbFontStyle2.SelectedValue = "Normal";
        //    CmbFontStyle3.SelectedValue = "Normal";

        //    chkIsUnderline.IsChecked = false;
        //    chkIsUnderline2.IsChecked = false;
        //    chkIsUnderline3.IsChecked = false;

        //    cmbSelectForeColor.SelectedColor = color;
        //    cmbSelectForeColor2.SelectedColor = color;
        //    cmbSelectForeColor3.SelectedColor = color;

        //    txtFontSize.Text = "5";
        //    txtFontSize2.Text = "5";
        //    txtFontSize3.Text = "5";
        //}

        private void LoadTextPropertiesAtFirstTime()
        {
            System.Windows.Media.Color color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#00FFFFFF"));
            cmbSelectColorBackground.SelectedColor = color;
            cmbSelectColorBackground2.SelectedColor = color;
            cmbSelectColorBackground3.SelectedColor = color;
            cmbSelectForeColor.SelectedColor = color;
            cmbSelectForeColor2.SelectedColor = color;
            cmbSelectForeColor3.SelectedColor = color;
        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);
        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }
        private bool IsNumber(string text)
        {
            int number;

            //Allowing only numbers
            if (!(int.TryParse(text, out number)))
            {
                return false;
            }
            return true;
        }
        //string textValue = string.Empty;
        private void FillValue(string textValue)
        {
            try
            {
                if (!string.IsNullOrEmpty(textValue))
                {
                    string[] TextSeparators = { "@@##" };
                    string[] strTextArr = textValue.Split(TextSeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] PropertySeparators = { "@@@@" };
                    string[] strPropertyArr1 = strTextArr[0].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPropertyArr2 = strTextArr[1].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPropertyArr3 = strTextArr[2].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);

                    chkshowHideLine.IsChecked = Convert.ToBoolean(strTextArr[3]);


                    txtTopMsg.Text = strPropertyArr1[0];
                    System.Windows.Media.Color color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr1[1]));
                    cmbSelectColorBackground.SelectedColor = color;
                    CmbFontFamily.SelectedValue = strPropertyArr1[2];
                    CmbFontStyle.Text = strPropertyArr1[3];
                    chkIsUnderline.IsChecked = Convert.ToBoolean(strPropertyArr1[4]);
                    System.Windows.Media.Color fcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr1[5]));
                    cmbSelectForeColor.SelectedColor = fcolor;
                    txtFontSize.Text = strPropertyArr1[6];
                    chkIsBold.IsChecked = Convert.ToBoolean(strPropertyArr1[7]);
                    CmbTextAlign.SelectedValue = strPropertyArr1[8];


                    txtMidMsg.Text = strPropertyArr2[0];
                    System.Windows.Media.Color color1 = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr2[1]));
                    cmbSelectColorBackground2.SelectedColor = color1;
                    CmbFontFamily2.SelectedValue = strPropertyArr2[2];
                    CmbFontStyle2.Text = strPropertyArr2[3];
                    chkIsUnderline2.IsChecked = Convert.ToBoolean(strPropertyArr2[4]);
                    System.Windows.Media.Color fcolor1 = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr2[5]));
                    cmbSelectForeColor2.SelectedColor = fcolor1;
                    txtFontSize2.Text = strPropertyArr2[6];
                    chkIsBold2.IsChecked = Convert.ToBoolean(strPropertyArr2[7]);
                    CmbTextAlign2.SelectedValue = strPropertyArr2[8];

                    txtBotMsg.Text = strPropertyArr3[0];
                    System.Windows.Media.Color color2 = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr3[1]));
                    cmbSelectColorBackground3.SelectedColor = color2;
                    CmbFontFamily3.SelectedValue = strPropertyArr3[2];
                    CmbFontStyle3.Text = strPropertyArr3[3];
                    chkIsUnderline3.IsChecked = Convert.ToBoolean(strPropertyArr3[4]);
                    System.Windows.Media.Color fcolor2 = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr3[5]));
                    cmbSelectForeColor3.SelectedColor = fcolor2;
                    txtFontSize3.Text = strPropertyArr3[6];
                    chkIsBold3.IsChecked = Convert.ToBoolean(strPropertyArr3[7]);
                    CmbTextAlign3.SelectedValue = strPropertyArr3[8];

                    //if (Convert.ToBoolean(strPropertyArr1[7]))
                    //    txtGetValue.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");


                    //txtGetValue2.Text = strPropertyArr2[0];
                    //txtGetValue2.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr2[1]);
                    //txtGetValue2.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strPropertyArr2[2]);
                    //txtGetValue2.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strPropertyArr2[3]);
                    //if (Convert.ToBoolean(strPropertyArr2[4]))
                    //    txtGetValue2.TextDecorations = TextDecorations.Underline;
                    //txtGetValue2.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr2[5]);
                    //txtGetValue2.FontSize = Convert.ToInt16(strPropertyArr2[6]);
                    //if (Convert.ToBoolean(strPropertyArr2[7]))
                    //    txtGetValue2.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");

                    //txtGetValue3.Text = strPropertyArr3[0];
                    //txtGetValue3.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr3[1]);
                    //txtGetValue3.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strPropertyArr3[2]);
                    //txtGetValue3.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strPropertyArr3[3]);
                    //if (Convert.ToBoolean(strPropertyArr3[4]))
                    //    txtGetValue3.TextDecorations = TextDecorations.Underline;
                    //txtGetValue3.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr3[5]);
                    //txtGetValue3.FontSize = Convert.ToInt16(strPropertyArr3[6]);
                    //if (Convert.ToBoolean(strPropertyArr3[7]))
                    //    txtGetValue3.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void FillQrCodeValue(string textValue)
        {
            try
            {
                if (!string.IsNullOrEmpty(textValue))
                {

                    string[] PropertySeparators = { "@@@@" };
                    string[] strPropertyArr1 = textValue.Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);

                    txtQrCode.Text = strPropertyArr1[0];
                    System.Windows.Media.Color color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr1[1]));
                    cmbSelectColorBackground4.SelectedColor = color;
                    CmbFontFamily4.SelectedValue = strPropertyArr1[2];
                    CmbFontStyle4.Text = strPropertyArr1[3];
                    chkIsUnderline4.IsChecked = Convert.ToBoolean(strPropertyArr1[4]);
                    System.Windows.Media.Color fcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(strPropertyArr1[5]));
                    cmbSelectForeColor4.SelectedColor = fcolor;
                    txtFontSize4.Text = strPropertyArr1[6];
                    chkIsBold4.IsChecked = Convert.ToBoolean(strPropertyArr1[7]);
                    CmbTextAlign4.SelectedValue = strPropertyArr1[8];

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnReceiptLogo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".*";// ".png";
                dlg.Filter = "Image Files(*.jpg,*.Jpeg)|*.jpeg;*.jpg";
                //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|All Files(*.jpg,*.png,*.Jpeg)|*.png;*.jpeg;*.jpg";
                Nullable<bool> result = dlg.ShowDialog();
                // Get the selected file name and display in a TextBox 
                if (result == true)
                {
                    // Open document 
                    string filename = dlg.FileName;
                    txtLogoPath.Text = filename;
                    string logoHotFolderPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo");
                    if (!System.IO.Directory.Exists(logoHotFolderPath))
                    {
                        System.IO.Directory.CreateDirectory(logoHotFolderPath);
                    }
                    if (System.IO.File.Exists(filename))
                    {
                        string hotFolderFilePath = System.IO.Path.Combine(logoHotFolderPath, "ImgLogo" + System.IO.Path.GetExtension(filename));
                        System.IO.File.Copy(filename, hotFolderFilePath, true);
                        CopyToAllSubstore(lstConfigurationInfo, hotFolderFilePath);
                        txtLogoPath.Text = hotFolderFilePath;
                        SaveImg(hotFolderFilePath);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void SaveImg(string hotFolderFilePath)
        {
            using (System.IO.FileStream fileStream = System.IO.File.OpenRead(hotFolderFilePath))
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                fileStream.CopyTo(ms);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = ms;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
                fileStream.Close();
                imgLogo.Source = bitmap;
                ms.Dispose();
            }
        }
        List<ConfigurationInfo> lstConfigurationInfo = new List<ConfigurationInfo>();
        private void GetAllSubstoreConfigdata()
        {
            try
            {

                lstConfigurationInfo = (new ConfigBusiness()).GetAllSubstoreConfigdata();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void CopyToAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string targetDir = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "ReceiptLogo");
                        if (!System.IO.Directory.Exists(targetDir))
                        {
                            System.IO.Directory.CreateDirectory(targetDir);
                        }
                        System.IO.File.Copy(Sourcefile, System.IO.Path.Combine(targetDir, "ImgLogo" + System.IO.Path.GetExtension(Sourcefile)), true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }



    }
}
