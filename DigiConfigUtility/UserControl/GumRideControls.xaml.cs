﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Win32;
using System.Configuration;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for GumRideControls.xaml
    /// </summary>
    public partial class GumRideControls : UserControl
    {
        public GumRideControls()
        {
            InitializeComponent();
            IsSpecActive = SpecActiveStatus();
            FillLocationCombo();
            SetGumRideMasterId();
            GetRideInfo(Config.SubStoreId);
            LoadFontStyle();
        }

        #region Properties

        Dictionary<int, string> lstLocationList;
        ConfigBusiness configBusiness = null;
        List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
        List<long> GumRideList;
        private string availableLocations { get; set; }
        private decimal fontSize { get; set; }
        private string fontColor { get; set; }
        private string fontWeight { get; set; }
        private string fontStyle { get; set; }
        private string fontFamily { get; set; }
        private string backgroundColor { get; set; }
        private string margin { get; set; }
        private string filePath { get; set; }
        private string photoPath { get; set; }
        private string isGumbleRideActive { get; set; }
        private string position { get; set; }
        private string IsSpecGumRide { get; set; }
        private string timeOut { get; set; }
        private string isZeroScoreVisibleOnImage { get; set; }
        private string isZeroScoreVisibleOnPreviewWall { get; set; }
        private bool IsSpecActive { get; set; }

        private string IsPrefixActiveFlow { get; set; }
        private string PrefixPhotoName { get; set; }
        private string PrefixScoreFileName { get; set; }
        private string GumballScoreSeperater { get; set; }

        //-------Start-------Nilesh------Add Gumball Bg image dynamicaly-------24th Feb 2018--
        private string GumballBGImgPath { get; set; }
        private string GumballBGImgHeigthWidth { get; set; }
        private string GumballBGImgPoistion { get; set; }
        //-------Start-------Nilesh------Add Gumball Bg image dynamicaly-------24th Feb 2018--

        #endregion

        #region Methods
        private void SetGumRideMasterId()
        {
            GumRideList = new List<long>();
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideAvailableLocations));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontSize));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontWeight));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideBackgrondColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRidePosition));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideMargin));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFilePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideInputPhotoPath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumRideActive));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumPlayerScoreVisible));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontStyle));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontFamily));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideTextFileTimeOut));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScore));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScoreOnImage));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideInputPhotoPath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumballScoreSeperater));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsPrefixActiveFlow));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixPhotoName));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixScoreFileName));

            //----------Start------Nilesh------Added Dynamic Gumball Image-------24th Feb 2018
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImagePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgHeightWidth));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgPosition));
            //----------End------Nilesh------Added Dynamic Gumball Image-------24th Feb 2018
        }
        private string Validate()
        {
            if (cmbLocation.SelectedIndex <= 0)
                return "Please select location";
            if (string.IsNullOrEmpty(txtFontSize.Text))
                return "Please enter font size(ranges between 1-150)";
            else if (int.Parse(txtFontSize.Text) > 150 || int.Parse(txtFontSize.Text) < 1)
                return "Please enter font size between 1 to 150";
            if (string.IsNullOrEmpty(txtFilePath.Text))
                return "Please browse text file path!";
            if (string.IsNullOrEmpty(txtInputImage.Text))
                return "Please browse input photo path!";
            string err = ValidateMargins();
            if (!String.IsNullOrEmpty(err))
                return err;
            PreparPlayerList();
            if (ValidateDistinctPlayerPositions())
                return "Duplicate values for multiple players is not allowed!";
            if (chkIsPrefixActiveFlow.IsChecked == true)
            {
                if (string.IsNullOrEmpty(txtPrefixPhotoName.Text))
                {
                    return "Please enter Image File Name";
                }
                if (string.IsNullOrEmpty(txtPrefixScoreFileName.Text))
                {
                    return "Please enter Ride Score File Name";
                }
                if (string.IsNullOrEmpty(txtGumBallScoreSeparater.Text))
                {
                    return "Please enter Ride Score Separator";
                }
            }
            return null;
        }
        private void txtGumBallScoreSeparater_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text.Trim());
        }
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        List<string> Player1 = new List<string>();
        List<string> Player2 = new List<string>();
        List<string> Player3 = new List<string>();
        List<string> Player4 = new List<string>();
        List<string> Player5 = new List<string>();
        List<string> Player6 = new List<string>();
        private void PreparPlayerList()
        {
            Player1 = new List<string>();
            Player2 = new List<string>();
            Player3 = new List<string>();
            Player4 = new List<string>();
            Player5 = new List<string>();
            Player6 = new List<string>();

            //Add values for Player 1
            Player1.Add(cmbPosition1.SelectedIndex.ToString());
            Player1.Add(txtLeftMargin1.Text);
            Player1.Add(txtTopMargin1.Text);
            Player1.Add(txtRightMargin1.Text);
            Player1.Add(txtBottomMargin1.Text);

            //Add values for Player 2
            Player2.Add(cmbPosition2.SelectedIndex.ToString());
            Player2.Add(txtLeftMargin2.Text);
            Player2.Add(txtTopMargin2.Text);
            Player2.Add(txtRightMargin2.Text);
            Player2.Add(txtBottomMargin2.Text);

            //Add values for Player 3
            Player3.Add(cmbPosition3.SelectedIndex.ToString());
            Player3.Add(txtLeftMargin3.Text);
            Player3.Add(txtTopMargin3.Text);
            Player3.Add(txtRightMargin3.Text);
            Player3.Add(txtBottomMargin3.Text);

            //Add values for Player 4
            Player4.Add(cmbPosition4.SelectedIndex.ToString());
            Player4.Add(txtLeftMargin4.Text);
            Player4.Add(txtTopMargin4.Text);
            Player4.Add(txtRightMargin4.Text);
            Player4.Add(txtBottomMargin4.Text);

            //Add values for Player 5
            Player5.Add(cmbPosition5.SelectedIndex.ToString());
            Player5.Add(txtLeftMargin5.Text);
            Player5.Add(txtTopMargin5.Text);
            Player5.Add(txtRightMargin5.Text);
            Player5.Add(txtBottomMargin5.Text);

            //Add values for Player 6
            Player6.Add(cmbPosition6.SelectedIndex.ToString());
            Player6.Add(txtLeftMargin6.Text);
            Player6.Add(txtTopMargin6.Text);
            Player6.Add(txtRightMargin6.Text);
            Player6.Add(txtBottomMargin6.Text);
        }
        private bool ValidateDistinctPlayerPositions()
        {
            bool retval = false;
            List<List<String>> CommonList = new List<List<String>>();
            CommonList.Add(Player1);
            CommonList.Add(Player2);
            CommonList.Add(Player3);
            CommonList.Add(Player4);
            CommonList.Add(Player5);
            CommonList.Add(Player6);

            for (int i = 0; i < CommonList.Count; i++)
            {
                List<string> CustomList = new List<string>();
                foreach (var item in CommonList[i])
                {
                    CustomList.Add(item);
                }
                if (i == 0)
                {
                    if ((CustomList.SequenceEqual(Player2) || CustomList.SequenceEqual(Player3) || CustomList.SequenceEqual(Player4) || CustomList.SequenceEqual(Player5) || CustomList.SequenceEqual(Player6)))
                    {
                        retval = true;
                        break;
                    }
                }
                else if (i == 1)
                {
                    if ((CustomList.SequenceEqual(Player1) || CustomList.SequenceEqual(Player3) || CustomList.SequenceEqual(Player4) || CustomList.SequenceEqual(Player5) || CustomList.SequenceEqual(Player6)))
                    {
                        retval = true;
                        break;
                    }
                }
                else if (i == 2)
                {
                    if ((CustomList.SequenceEqual(Player1) || CustomList.SequenceEqual(Player2) || CustomList.SequenceEqual(Player4) || CustomList.SequenceEqual(Player5) || CustomList.SequenceEqual(Player6)))
                    {
                        retval = true;
                        break;
                    }
                }
                else if (i == 3)
                {
                    if ((CustomList.SequenceEqual(Player1) || CustomList.SequenceEqual(Player2) || CustomList.SequenceEqual(Player3) || CustomList.SequenceEqual(Player5) || CustomList.SequenceEqual(Player6)))
                    {
                        retval = true;
                        break;
                    }
                }
                else if (i == 4)
                {
                    if ((CustomList.SequenceEqual(Player1) || CustomList.SequenceEqual(Player2) || CustomList.SequenceEqual(Player3) || CustomList.SequenceEqual(Player4) || CustomList.SequenceEqual(Player6)))
                    {
                        retval = true;
                        break;
                    }
                }
                else if (i == 5)
                {
                    if ((CustomList.SequenceEqual(Player1) || CustomList.SequenceEqual(Player2) || CustomList.SequenceEqual(Player3) || CustomList.SequenceEqual(Player4) || CustomList.SequenceEqual(Player5)))
                    {
                        retval = true;
                        break;
                    }
                }
            }
            return retval;
        }

        private string ValidateMargins()
        {
            string errMessage = string.Empty;
            var controls = new[] {
                txtLeftMargin1, txtLeftMargin2, txtLeftMargin3, txtLeftMargin4, txtLeftMargin5, txtLeftMargin6,
                txtTopMargin1, txtTopMargin2, txtTopMargin3, txtTopMargin4, txtTopMargin5, txtTopMargin6,
                txtRightMargin1, txtRightMargin2, txtRightMargin3, txtRightMargin4, txtRightMargin5, txtRightMargin6,
                txtBottomMargin1, txtBottomMargin2, txtBottomMargin3, txtBottomMargin4, txtBottomMargin5, txtBottomMargin6
            };
            foreach (var control in controls)
            {
                if (control.IsEnabled)
                {
                    if (!string.IsNullOrEmpty(control.Text))
                    {
                        if (int.Parse(control.Text) < 0)
                        {
                            errMessage += "Margin should be greater than or equals to 0 for " + GetPlayerNumerAndMargin(control.Name) + "\n";
                        }
                    }
                    else
                    {
                        errMessage += "Margin should be 0 atleast " + GetPlayerNumerAndMargin(control.Name) + "\n";
                    }
                }
            }
            return errMessage;
        }
        private string GetPlayerNumerAndMargin(string ctrlName)
        {
            string retVal = string.Empty;
            string temp = ctrlName.Replace("txt", "");
            string playerNum = ctrlName.Substring(ctrlName.Length - 1);
            string tempMarginName = ctrlName.Replace("txt", "").Replace("Margin", "");
            tempMarginName = tempMarginName.Substring(0, tempMarginName.Length - 1);
            retVal = "Player " + playerNum + " " + tempMarginName + " Margin";
            return retVal;
        }
        private void LoadFontStyle()
        {
            List<string> fonts = new List<string>();
            System.Drawing.Text.InstalledFontCollection col = new System.Drawing.Text.InstalledFontCollection();
            foreach (System.Drawing.FontFamily family in col.Families)
            {
                fonts.Add(family.Name);
            }
            cmbFontFamily.ItemsSource = fonts;
            cmbFontFamily.Text = "Times New Roman";
        }
        private void SetValueToProperties()
        {
            try
            {
                string playerPos = GetAllPlayerScorePosition();
                string playerMargin = GetAllPlayerScoreMargins();
                availableLocations = cmbLocation.SelectedValue.ToString();
                fontSize = Convert.ToDecimal(txtFontSize.Text);
                fontWeight = cmbFontWeight.Text;
                fontStyle = cmbFontStyle.Text;
                fontFamily = cmbFontFamily.Text;
                margin = playerMargin;
                backgroundColor = cmb1SelectColorBG.SelectedColor.ToString() + ',' + cmb2SelectColorBG.SelectedColor.ToString() + ',' + cmb3SelectColorBG.SelectedColor.ToString() + ',' + cmb4SelectColorBG.SelectedColor.ToString() + ',' + cmb5SelectColorBG.SelectedColor.ToString() + ',' + cmb6SelectColorBG.SelectedColor.ToString();
                isGumbleRideActive = chkIsGumRideActive.IsChecked == true ? "true" : "false";
                filePath = txtFilePath.Text;
                photoPath = txtInputImage.Text.Trim();
                position = playerPos;
                IsSpecGumRide = chkIsGumPlayerScoreVisible.IsChecked == true ? "true" : "false";
                timeOut = txtTimeOut.Text;
                fontColor = cmb1SelectColorFont.SelectedColor.ToString() + ',' + cmb2SelectColorFont.SelectedColor.ToString() + ',' + cmb3SelectColorFont.SelectedColor.ToString() + ',' + cmb4SelectColorFont.SelectedColor.ToString() + ',' + cmb5SelectColorFont.SelectedColor.ToString() + ',' + cmb6SelectColorFont.SelectedColor.ToString();
                isZeroScoreVisibleOnImage = chkIsZeroScoreVisibleOnImage.IsChecked == true ? "true" : "false";
                isZeroScoreVisibleOnPreviewWall = chkIsZeroScoreVisibleOnPreviewWall.IsChecked == true ? "true" : "false";
                IsPrefixActiveFlow = chkIsPrefixActiveFlow.IsChecked == true ? "true" : "false";
                PrefixPhotoName = txtPrefixPhotoName.Text.Trim();
                PrefixScoreFileName = txtPrefixScoreFileName.Text.Trim();
                GumballScoreSeperater = txtGumBallScoreSeparater.Text.Trim();
                //----Start------Nilesh-----Add Gumball image dynamicaly--------24th Feb 2018---
                // GumballBGImgPath = "D:\\Gumball\\BGDiamondImg\\11.pnj | D:\\Gumball\\BGDiamondImg\\22.pnj | D:\\Gumball\\BGDiamondImg\\33.pnj | D:\\Gumball\\BGDiamondImg\\44.pnj | D:\\Gumball\\BGDiamondImg\\55.pnj";//GetAllPlayerBGImgPath();
                GumballBGImgPath = GetAllPlayerBGImgPath();
                GumballBGImgHeigthWidth = GetAllPlayerBGImgHeightWidth();
                GumballBGImgPoistion = GetAllPlayerBGImgPosition();
                //----End------Nilesh-----Add Gumball image dynamicaly--------24th Feb 2018---
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void EnableRequiredMargins(int player, string position)
        {
            if (player == 1)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin1.IsEnabled = true;
                        txtLeftMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.Text = "20";
                        txtRightMargin1.IsEnabled = true;
                        txtBottomMargin1.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.IsEnabled = true;
                        txtRightMargin1.IsEnabled = true;
                        txtBottomMargin1.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.IsEnabled = true;
                        txtRightMargin1.IsEnabled = true;
                        txtRightMargin1.Text = "20";
                        txtBottomMargin1.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin1.IsEnabled = true;
                        txtLeftMargin1.IsEnabled = true;
                        txtLeftMargin1.Text = "20";
                        txtRightMargin1.IsEnabled = true;
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin1.IsEnabled = true;
                        txtLeftMargin1.IsEnabled = true;
                        txtRightMargin1.IsEnabled = true;
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin1.IsEnabled = true;
                        txtLeftMargin1.IsEnabled = true;
                        txtRightMargin1.IsEnabled = true;
                        txtRightMargin1.Text = "20";
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                }
                txtImgHeightWidth1.Text = txtImgHeightWidth1.Text.Trim() == "0" ? "150:150" : txtImgHeightWidth1.Text; //By Vinod
                txtScorePos1.Text = txtScorePos1.Text.Trim() == "0" ? "50:50" : txtScorePos1.Text; //By Vinod
                //txtImgHeightWidth1.Text = "220:220"; //By Nilesh
                //txtScorePos1.Text = "100:50"; //By Nilesh
            }
            else if (player == 2)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin2.IsEnabled = true;
                        txtLeftMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.Text = "20";
                        txtRightMargin2.IsEnabled = true;
                        txtBottomMargin2.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.IsEnabled = true;
                        txtRightMargin2.IsEnabled = true;
                        txtBottomMargin2.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.IsEnabled = true;
                        txtRightMargin2.IsEnabled = true;
                        txtRightMargin2.Text = "20";
                        txtBottomMargin2.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin2.IsEnabled = true;
                        txtLeftMargin2.IsEnabled = true;
                        txtLeftMargin2.Text = "20";
                        txtRightMargin2.IsEnabled = true;
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin2.IsEnabled = true;
                        txtLeftMargin2.IsEnabled = true;
                        txtRightMargin2.IsEnabled = true;
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin2.IsEnabled = true;
                        txtLeftMargin2.IsEnabled = true;
                        txtRightMargin2.IsEnabled = true;
                        txtRightMargin2.Text = "20";
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                }
                txtImgHeightWidth2.Text = txtImgHeightWidth2.Text.Trim() == "0" ? "150:150" : txtImgHeightWidth2.Text; //By Vinod
                txtScorePos2.Text = txtScorePos2.Text.Trim() == "0" ? "50:50" : txtScorePos2.Text; //By Vinod
                //txtImgHeightWidth2.Text = "220:220"; //By Nilesh
                //txtScorePos2.Text = "100:50"; //By Nilesh
            }
            else if (player == 3)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin3.IsEnabled = true;
                        txtLeftMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.Text = "20";
                        txtRightMargin3.IsEnabled = true;
                        txtBottomMargin3.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.IsEnabled = true;
                        txtRightMargin3.IsEnabled = true;
                        txtBottomMargin3.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.IsEnabled = true;
                        txtRightMargin3.IsEnabled = true;
                        if (txtScorePos3.Text.Trim() == "0")
                        {
                            txtRightMargin3.Text = "200";
                        }
                        else
                        {
                            txtRightMargin3.Text = "20";
                        }

                        txtBottomMargin3.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin3.IsEnabled = true;
                        txtLeftMargin3.IsEnabled = true;
                        txtLeftMargin3.Text = "20";
                        txtRightMargin3.IsEnabled = true;
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin3.IsEnabled = true;
                        txtLeftMargin3.IsEnabled = true;
                        txtRightMargin3.IsEnabled = true;
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin3.IsEnabled = true;
                        txtLeftMargin3.IsEnabled = true;
                        txtRightMargin3.IsEnabled = true;
                        txtRightMargin3.Text = "20";
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                }
                txtImgHeightWidth3.Text = txtImgHeightWidth3.Text.Trim() == "0" ? "150:150" : txtImgHeightWidth3.Text; //By Vinod
                txtScorePos3.Text = txtScorePos3.Text.Trim() == "0" ? "50:50" : txtScorePos3.Text; //By Vinod
                //txtImgHeightWidth2.Text = "220:220"; //By Nilesh
                //txtScorePos2.Text = "100:50"; //By Nilesh
            }
            else if (player == 4)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin4.IsEnabled = true;
                        txtLeftMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.Text = "20";
                        txtRightMargin4.IsEnabled = true;
                        txtBottomMargin4.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.IsEnabled = true;
                        txtRightMargin4.IsEnabled = true;
                        txtBottomMargin4.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.IsEnabled = true;
                        txtRightMargin4.IsEnabled = true;
                        txtRightMargin4.Text = "20";
                        txtBottomMargin4.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin4.IsEnabled = true;
                        txtLeftMargin4.IsEnabled = true;
                        txtLeftMargin4.Text = "20";
                        txtRightMargin4.IsEnabled = true;
                        txtBottomMargin4.IsEnabled = true;
                        if (txtScorePos4.Text.Trim() == "0")
                        {
                            txtBottomMargin4.Text = "200";
                        }
                        else
                        {
                            txtBottomMargin4.Text = "20";
                        }
                        //txtBottomMargin4.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin4.IsEnabled = true;
                        txtLeftMargin4.IsEnabled = true;
                        txtRightMargin4.IsEnabled = true;
                        txtBottomMargin4.IsEnabled = true;
                        txtBottomMargin4.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin4.IsEnabled = true;
                        txtLeftMargin4.IsEnabled = true;
                        txtRightMargin4.IsEnabled = true;
                        txtRightMargin4.Text = "20";
                        txtBottomMargin4.IsEnabled = true;
                        txtBottomMargin4.Text = "20";
                        break;
                }
                txtImgHeightWidth4.Text = txtImgHeightWidth4.Text.Trim() == "0" ? "" : txtImgHeightWidth4.Text; //By Vinod
                txtScorePos4.Text = txtScorePos4.Text.Trim() == "0" ? "" : txtScorePos4.Text; //By Vinod
                //txtImgHeightWidth4.Text = "220:220"; //By Nilesh
                //txtScorePos4.Text = "100:50"; //By Nilesh
            }
            else if (player == 5)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin5.IsEnabled = true;
                        txtLeftMargin5.IsEnabled = true;
                        txtTopMargin5.Text = "20";
                        txtLeftMargin5.Text = "20";
                        txtRightMargin5.IsEnabled = true;
                        txtBottomMargin5.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin5.IsEnabled = true;
                        txtTopMargin5.Text = "20";
                        txtLeftMargin5.IsEnabled = true;
                        txtRightMargin5.IsEnabled = true;
                        txtBottomMargin5.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin5.IsEnabled = true;
                        txtTopMargin5.Text = "20";
                        txtLeftMargin5.IsEnabled = true;
                        txtRightMargin5.IsEnabled = true;
                        txtRightMargin5.Text = "20";
                        txtBottomMargin5.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin5.IsEnabled = true;
                        txtLeftMargin5.IsEnabled = true;
                        txtLeftMargin5.Text = "20";
                        txtRightMargin5.IsEnabled = true;
                        txtBottomMargin5.IsEnabled = true;
                        txtBottomMargin5.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin5.IsEnabled = true;
                        txtLeftMargin5.IsEnabled = true;
                        txtRightMargin5.IsEnabled = true;
                        txtBottomMargin5.IsEnabled = true;
                        if (txtScorePos5.Text.Trim() == "0")
                        {
                            txtBottomMargin5.Text = "220";
                        }
                        else
                        {
                            txtBottomMargin5.Text = "20";
                        }

                        //txtBottomMargin5.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin5.IsEnabled = true;
                        txtLeftMargin5.IsEnabled = true;
                        txtRightMargin5.IsEnabled = true;
                        txtRightMargin5.Text = "20";
                        txtBottomMargin5.IsEnabled = true;
                        txtBottomMargin5.Text = "20";
                        break;
                }
                txtImgHeightWidth5.Text = txtImgHeightWidth5.Text.Trim() == "0" ? "" : txtImgHeightWidth5.Text; //By Vinod
                txtScorePos5.Text = txtScorePos5.Text.Trim() == "0" ? "" : txtScorePos5.Text; //By Vinod
                //txtImgHeightWidth5.Text = "220:220"; //By Nilesh
                //txtScorePos5.Text = "100:50"; //By Nilesh
            }
            else if (player == 6)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin6.IsEnabled = true;
                        txtLeftMargin6.IsEnabled = true;
                        txtTopMargin6.Text = "20";
                        txtLeftMargin6.Text = "20";
                        txtRightMargin6.IsEnabled = true;
                        txtBottomMargin6.IsEnabled = true;
                        break;
                    case "Top-Center":
                        txtTopMargin6.IsEnabled = true;
                        txtTopMargin6.Text = "20";
                        txtLeftMargin6.IsEnabled = true;
                        txtRightMargin6.IsEnabled = true;
                        txtBottomMargin6.IsEnabled = true;
                        break;
                    case "Top-Right":
                        txtTopMargin6.IsEnabled = true;
                        txtTopMargin6.Text = "20";
                        txtLeftMargin6.IsEnabled = true;
                        txtRightMargin6.IsEnabled = true;
                        txtRightMargin6.Text = "20";
                        txtBottomMargin6.IsEnabled = true;
                        break;
                    case "Bottom-Left":
                        txtTopMargin6.IsEnabled = true;
                        txtLeftMargin6.IsEnabled = true;
                        txtLeftMargin6.Text = "20";
                        txtRightMargin6.IsEnabled = true;
                        txtBottomMargin6.IsEnabled = true;
                        txtBottomMargin6.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin6.IsEnabled = true;
                        txtLeftMargin6.IsEnabled = true;
                        txtRightMargin6.IsEnabled = true;
                        txtBottomMargin6.IsEnabled = true;
                        txtBottomMargin6.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin6.IsEnabled = true;
                        txtLeftMargin6.IsEnabled = true;
                        txtRightMargin6.IsEnabled = true;
                        //txtRightMargin6.Text = "20";
                        txtBottomMargin6.IsEnabled = true;
                        if (txtScorePos6.Text.Trim() == "0")
                        {
                            txtRightMargin6.Text = "200";
                            txtBottomMargin6.Text = "200";
                        }
                        else
                        {
                            txtRightMargin6.Text = "20";
                            txtBottomMargin6.Text = "20";
                        }

                        //txtBottomMargin6.Text = "20";
                        break;
                }
                txtImgHeightWidth6.Text = txtImgHeightWidth6.Text.Trim() == "0" ? "" : txtImgHeightWidth6.Text; //By Vinod
                txtScorePos6.Text = txtScorePos6.Text.Trim() == "0" ? "" : txtScorePos6.Text; //By Vinod
                //txtImgHeightWidth6.Text = "220:220"; //By Nilesh
                //txtScorePos6.Text = "100:50"; //By Nilesh
            }
        }
        private void ResetMargin(int player)
        {
            switch (player)
            {
                case 1:
                    txtTopMargin1.Text = "0";
                    txtLeftMargin1.Text = "0";
                    txtRightMargin1.Text = "0";
                    txtBottomMargin1.Text = "0";
                    break;
                case 2:
                    txtTopMargin2.Text = "0";
                    txtLeftMargin2.Text = "0";
                    txtRightMargin2.Text = "0";
                    txtBottomMargin2.Text = "0";
                    break;
                case 3:
                    txtTopMargin3.Text = "0";
                    txtLeftMargin3.Text = "0";
                    txtRightMargin3.Text = "0";
                    txtBottomMargin3.Text = "0";
                    break;
                case 4:
                    txtTopMargin4.Text = "0";
                    txtLeftMargin4.Text = "0";
                    txtRightMargin4.Text = "0";
                    txtBottomMargin4.Text = "0";
                    break;
                case 5:
                    txtTopMargin5.Text = "0";
                    txtLeftMargin5.Text = "0";
                    txtRightMargin5.Text = "0";
                    txtBottomMargin5.Text = "0";
                    break;
                case 6:
                    txtTopMargin6.Text = "0";
                    txtLeftMargin6.Text = "0";
                    txtRightMargin6.Text = "0";
                    txtBottomMargin6.Text = "0";
                    break;
            }
        }
        private void ResetPositions()
        {
            cmbPosition1.SelectedIndex = 3;
            cmbPosition2.SelectedIndex = 4;
            cmbPosition3.SelectedIndex = 5;
            cmbPosition4.SelectedIndex = 0;
            cmbPosition5.SelectedIndex = 1;
            cmbPosition6.SelectedIndex = 2;
        }
        private void SetPropertiesToLocationList()
        {
            List<iMixConfigurationLocationInfo> locationConfigurationInfo = new List<iMixConfigurationLocationInfo>();
            int locatioId = (int)cmbLocation.SelectedValue;
            iMixConfigurationLocationInfo obj = new iMixConfigurationLocationInfo();
            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideAvailableLocations,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = availableLocations,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFontSize,
                SubstoreId = Config.SubStoreId,
                LocationId = locatioId,
                ConfigurationValue = fontSize.ToString(),
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFontColor,
                SubstoreId = Config.SubStoreId,
                LocationId = locatioId,
                ConfigurationValue = fontColor,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFontWeight,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = fontWeight,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRidePosition,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = position,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideMargin,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = margin.ToString(),
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFilePath,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = filePath,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);
            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideInputPhotoPath,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = photoPath,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);
            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.IsGumRideActive,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = isGumbleRideActive,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {

                IMIXConfigurationMasterId = (int)ConfigParams.GumRideBackgrondColor,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = backgroundColor,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.IsGumPlayerScoreVisible,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = IsSpecGumRide,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFontStyle,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = fontStyle,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideFontFamily,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = fontFamily,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumRideTextFileTimeOut,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = timeOut,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.IsVisibleGumBallZeroScore,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = isZeroScoreVisibleOnPreviewWall,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.IsVisibleGumBallZeroScoreOnImage,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = isZeroScoreVisibleOnImage,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumballScoreSeperater,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = GumballScoreSeperater,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.IsPrefixActiveFlow,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = IsPrefixActiveFlow,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.PrefixPhotoName,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = PrefixPhotoName,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.PrefixScoreFileName,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = PrefixScoreFileName,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            ///---Start-----Nilesh-------Added Dynamic Img in Gumball------24th Feb 18-------
            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumBGImagePath,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = GumballBGImgPath,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumBGImgHeightWidth,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = GumballBGImgHeigthWidth,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);

            obj = new iMixConfigurationLocationInfo
            {
                IMIXConfigurationMasterId = (int)ConfigParams.GumBGImgPosition,
                SubstoreId = Config.SubStoreId,
                ConfigurationValue = GumballBGImgPoistion,
                LocationId = locatioId,
            };
            locationConfigurationInfo.Add(obj);
            ///---End-----Nilesh-------Added Dynamic Img in Gumball------24th Feb 18-------
            configBusiness = new ConfigBusiness();
            try
            {
                List<SemiOrderSettings> lstDG_SemiOrder_Settings;
                bool isSpecSettingAvailable = false;
                bool IsSaved = configBusiness.SaveUpdateConfigLocation(locationConfigurationInfo);
                if (IsSaved)
                {
                    MessageBox.Show("Ride Integration Settings Saved Successfully !", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    ResetControls();
                    cmbLocation.SelectedIndex = 0;
                    configBusiness = new ConfigBusiness();
                    lstDG_SemiOrder_Settings = (new SemiOrderBusiness()).GetSemiOrderSetting(Config.SubStoreId, locatioId);
                    if (lstDG_SemiOrder_Settings != null)
                    {
                        isSpecSettingAvailable = lstDG_SemiOrder_Settings.Any(x => x.DG_LocationId > 0);
                        if (!isSpecSettingAvailable || !IsSpecActive)
                            System.Windows.Forms.MessageBox.Show("Spec settings are either not active or not saved for this location", "Alert Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                StoreSubStoreDataBusniess substoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = substoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                lstLocationList.Add(0, "-Select Location-");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name);
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void ResetControls()
        {
            cmbFontWeight.SelectedIndex = 4;
            cmbFontFamily.SelectedValue = "Times New Roman";
            chkIsGumRideActive.IsChecked = true;
            chkIsGumPlayerScoreVisible.IsChecked = false;
            chkIsZeroScoreVisibleOnImage.IsChecked = false;
            chkIsZeroScoreVisibleOnPreviewWall.IsChecked = false;
            txtFilePath.Clear();
            txtInputImage.Clear();
            txtFontSize.Clear();
            cmb1SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb2SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb3SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb4SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb5SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb6SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb1SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFBA55D3"));
            cmb2SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFF0000"));
            cmb3SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00BFFF"));
            cmb4SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF3CB371"));
            cmb5SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFF69B4"));
            cmb6SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFD600"));
            txtFontSize.Text = "20";
            txtTimeOut.Text = "120";
            //Reset Properties
            availableLocations = string.Empty;
            fontSize = 0;
            fontColor = string.Empty;
            fontWeight = string.Empty;
            fontStyle = string.Empty;
            fontFamily = string.Empty;
            backgroundColor = string.Empty;
            margin = string.Empty;
            filePath = string.Empty;
            isGumbleRideActive = string.Empty;
            position = string.Empty;
            IsSpecGumRide = string.Empty;
            if (string.IsNullOrEmpty(txtFilePath.Text))
            {
                ToolTipService.SetIsEnabled(txtFilePath, false);
            }
            ResetPositions();
            for (int i = 1; i < 7; i++)
            {
                ResetMargin(i);
            }
            EnableRequiredMargins(1, cmbPosition1.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(2, cmbPosition2.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(3, cmbPosition3.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(4, cmbPosition4.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(5, cmbPosition5.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(6, cmbPosition6.SelectedValue.ToString().Substring(38));
            chkIsPrefixActiveFlow.IsChecked = false;
            txtPrefixPhotoName.Text = string.Empty;
            txtPrefixScoreFileName.Text = string.Empty;
            txtGumBallScoreSeparater.Text = string.Empty;
            txtBgImgPath1.Text = string.Empty;
            txtBgImgPath2.Text = string.Empty;
            txtBgImgPath3.Text = string.Empty;
            txtBgImgPath4.Text = string.Empty;
            txtBgImgPath5.Text = string.Empty;
            txtBgImgPath6.Text = string.Empty;
            //txtImgHeightWidth1.Text = string.Empty;
            //txtScorePos1.Text = string.Empty;
            //txtImgHeightWidth2.Text = string.Empty;
            //txtScorePos2.Text = string.Empty;
            //txtImgHeightWidth3.Text = string.Empty;
            //txtScorePos3.Text = string.Empty;
            //txtImgHeightWidth4.Text = string.Empty;
            //txtScorePos4.Text = string.Empty;
            //txtImgHeightWidth5.Text = string.Empty;
            //txtScorePos5.Text = string.Empty;
            //txtImgHeightWidth6.Text = string.Empty;
            //txtScorePos6.Text = string.Empty;

        }
        private void GetRideInfo(int SubStoreId)
        {
            configBusiness = new ConfigBusiness();
            lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(SubStoreId);

            List<GumRide> lstGrid = new List<GumRide>();
            foreach (var locInfo in lstLocationList)
            {
                GumRide lstPW = new GumRide();
                if (locInfo.Value.Equals("-Select Location-"))
                    continue;
                List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == locInfo.Key && GumRideList.Contains(n.IMIXConfigurationMasterId)).ToList();

                if (lstLocation != null || lstLocation.Count > 0)
                {
                    foreach (var loc in lstLocation)
                    {
                        lstPW.LocationName = locInfo.Value;
                        lstPW.Locationid = locInfo.Key;
                        switch (loc.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.GumRideAvailableLocations:
                                lstPW.Locationid = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.GumRideFontSize:
                                lstPW.FontSize = Convert.ToDecimal(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.GumRideFontColor:
                                lstPW.FontColor = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRideFontWeight:
                                lstPW.FontWeight = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRideBackgrondColor:
                                lstPW.BackgroundColor = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRidePosition:
                                lstPW.Position = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRideMargin:
                                lstPW.Margin = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRideFilePath:
                                lstPW.FilePath = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.GumRideInputPhotoPath:
                                lstPW.PhotoPath = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsGumRideActive:
                                lstPW.IsGumbleRideActive = loc.ConfigurationValue == "true" ? "Active" : "InActive";
                                break;
                            case (int)ConfigParams.IsGumPlayerScoreVisible:
                                lstPW.IsSpecGumbRide = loc.ConfigurationValue == "true" ? "Active" : "InActive";
                                break;

                            case (int)ConfigParams.GumRideFontStyle:
                                lstPW.FontStyle = loc.ConfigurationValue;
                                break;

                            case (int)ConfigParams.GumRideFontFamily:
                                lstPW.FontFamily = loc.ConfigurationValue;
                                break;

                            case (int)ConfigParams.GumRideTextFileTimeOut:
                                lstPW.TimeOut = loc.ConfigurationValue;
                                break;

                            case (int)ConfigParams.PrefixScoreFileName:
                                lstPW.IsPrefixActiveFlow = loc.ConfigurationValue == "true" ? "Active" : "InActive";
                                break;

                            case (int)ConfigParams.IsPrefixActiveFlow:
                                lstPW.PrefixPhotoName = loc.ConfigurationValue;
                                break;

                            case (int)ConfigParams.PrefixPhotoName:
                                lstPW.PrefixScoreFileName = loc.ConfigurationValue;
                                break;

                            case (int)ConfigParams.GumballScoreSeperater:
                                lstPW.GumballScoreSeperater = loc.ConfigurationValue;
                                break;
                        }
                    }
                    if (lstLocation.Count > 0)
                        lstGrid.Add(lstPW);
                }
            }
            grdGumControls.ItemsSource = lstGrid;
        }
        private bool IsNumber(string text)
        {
            int number;
            //Allowing only numbers
            if (!(int.TryParse(text, out number)))
            {
                return false;
            }
            return true;
        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }
        private bool SpecActiveStatus()
        {
            var configItem = new ConfigBusiness().GetConfigurationData(Config.SubStoreId);
            bool SpecActive = false;
            if (configItem != null)
            {
                SpecActive = configItem.DG_SemiOrderMain == null ? false : Convert.ToBoolean(configItem.DG_SemiOrderMain);
            }
            return SpecActive;
        }
        #endregion

        #region Events
        private void btnDeleteGumRideSettings_Click(object sender, RoutedEventArgs e)
        {
            string locid = ((Button)sender).Tag.ToString();

            if (MessageBox.Show("Do you want to delete this Config?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (!string.IsNullOrWhiteSpace(locid))
                {
                    configBusiness = new ConfigBusiness();
                    configBusiness.DeleteLocationWiseConfigParamsGumbleRide(Convert.ToInt32(locid));
                    MessageBox.Show("Config deleted successfully", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbLocation.SelectedIndex = 0;
                    GetRideInfo(Config.SubStoreId);
                }
            }
        }
        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue) && GumRideList.Contains(n.IMIXConfigurationMasterId)).ToList();
            if (lstLocation.Count > 0)
            {
                foreach (var loc in lstLocation)
                {
                    switch (loc.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.GumRideFontSize:
                            txtFontSize.Text = loc.ConfigurationValue;
                            fontSize = Convert.ToDecimal(loc.ConfigurationValue);
                            break;

                        case (int)ConfigParams.IsGumRideActive:
                            if (loc.ConfigurationValue == "true")
                                chkIsGumRideActive.IsChecked = true;
                            else
                                chkIsGumRideActive.IsChecked = false;
                            isGumbleRideActive = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.IsGumPlayerScoreVisible:
                            if (loc.ConfigurationValue == "true")
                                chkIsGumPlayerScoreVisible.IsChecked = true;
                            else
                                chkIsGumPlayerScoreVisible.IsChecked = false;
                            IsSpecGumRide = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRidePosition:
                            if (!String.IsNullOrEmpty(loc.ConfigurationValue))
                                SetAllPlayerScorePosition(loc.ConfigurationValue);
                            position = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideFontWeight:
                            cmbFontWeight.Text = loc.ConfigurationValue;
                            fontWeight = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideFilePath:
                            txtFilePath.Text = loc.ConfigurationValue;
                            filePath = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.GumRideInputPhotoPath:
                            txtInputImage.Text = loc.ConfigurationValue;
                            photoPath = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.GumRideFontStyle:
                            cmbFontStyle.Text = loc.ConfigurationValue;
                            fontStyle = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideFontFamily:
                            cmbFontFamily.Text = loc.ConfigurationValue;
                            fontFamily = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideMargin:
                            if (!String.IsNullOrEmpty(loc.ConfigurationValue))
                                SetAllPlayerScoreMargins(loc.ConfigurationValue);
                            margin = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideTextFileTimeOut:
                            txtTimeOut.Text = loc.ConfigurationValue;
                            timeOut = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumRideFontColor:
                            string fontcolor = loc.ConfigurationValue;
                            string[] fontcolorArray = fontcolor.Split(',');
                            System.Windows.Media.Color color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[0]));
                            cmb1SelectColorFont.SelectedColor = color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[1]));
                            cmb2SelectColorFont.SelectedColor = color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[2]));
                            cmb3SelectColorFont.SelectedColor = color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[3]));
                            cmb4SelectColorFont.SelectedColor = color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[4]));
                            cmb5SelectColorFont.SelectedColor = color;
                            color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(fontcolorArray[5]));
                            cmb6SelectColorFont.SelectedColor = color;
                            fontColor = fontcolor;
                            break;

                        case (int)ConfigParams.GumRideBackgrondColor:
                            string BGcolor = loc.ConfigurationValue;
                            string[] BGcolorArray = BGcolor.Split(',');
                            System.Windows.Media.Color Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[0]));
                            cmb1SelectColorBG.SelectedColor = Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[1]));
                            cmb2SelectColorBG.SelectedColor = Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[2]));
                            cmb3SelectColorBG.SelectedColor = Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[3]));
                            cmb4SelectColorBG.SelectedColor = Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[4]));
                            cmb5SelectColorBG.SelectedColor = Bcolor;
                            Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BGcolorArray[5]));
                            cmb6SelectColorBG.SelectedColor = Bcolor;
                            backgroundColor = BGcolor;
                            break;

                        case (int)ConfigParams.IsVisibleGumBallZeroScore:
                            if (loc.ConfigurationValue == "true")
                                chkIsZeroScoreVisibleOnPreviewWall.IsChecked = true;
                            else
                                chkIsZeroScoreVisibleOnPreviewWall.IsChecked = false;
                            isZeroScoreVisibleOnPreviewWall = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.IsVisibleGumBallZeroScoreOnImage:
                            if (loc.ConfigurationValue == "true")
                                chkIsZeroScoreVisibleOnImage.IsChecked = true;
                            else
                                chkIsZeroScoreVisibleOnImage.IsChecked = false;
                            isZeroScoreVisibleOnImage = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.IsPrefixActiveFlow:
                            if (loc.ConfigurationValue == "true")
                                chkIsPrefixActiveFlow.IsChecked = true;
                            else
                                chkIsPrefixActiveFlow.IsChecked = false;
                            IsPrefixActiveFlow = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.GumballScoreSeperater:
                            txtGumBallScoreSeparater.Text = loc.ConfigurationValue;
                            GumballScoreSeperater = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.PrefixPhotoName:
                            txtPrefixPhotoName.Text = loc.ConfigurationValue;
                            PrefixPhotoName = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.PrefixScoreFileName:
                            txtPrefixScoreFileName.Text = loc.ConfigurationValue;
                            PrefixScoreFileName = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.GumBGImagePath:
                            if (!string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                string[] bgPathLst = loc.ConfigurationValue.Split(',');
                                for (int i = 0; i < bgPathLst.Length; i++)
                                {
                                    if (i == 0)
                                        txtBgImgPath1.Text = bgPathLst[0];
                                    if (i == 1)
                                        txtBgImgPath2.Text = bgPathLst[1];
                                    if (i == 2)
                                        txtBgImgPath3.Text = bgPathLst[2];
                                    if (i == 3)
                                        txtBgImgPath4.Text = bgPathLst[3];
                                    if (i == 4)
                                        txtBgImgPath5.Text = bgPathLst[4];
                                    if (i == 5)
                                        txtBgImgPath6.Text = bgPathLst[5];
                                }
                            }
                            break;
                        case (int)ConfigParams.GumBGImgHeightWidth:
                            if (!string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                string[] gbImgHeightWidth = loc.ConfigurationValue.Split(',');
                                for (int i = 0; i < gbImgHeightWidth.Length; i++)
                                {
                                    if (i == 0)
                                        txtImgHeightWidth1.Text = gbImgHeightWidth[0];
                                    if (i == 1)
                                        txtImgHeightWidth2.Text = gbImgHeightWidth[1];
                                    if (i == 2)
                                        txtImgHeightWidth3.Text = gbImgHeightWidth[2];
                                    if (i == 3)
                                        txtImgHeightWidth4.Text = gbImgHeightWidth[3];
                                    if (i == 4)
                                        txtImgHeightWidth5.Text = gbImgHeightWidth[4];
                                    if (i == 5)
                                        txtImgHeightWidth6.Text = gbImgHeightWidth[5];
                                }
                            }
                            break;
                        case (int)ConfigParams.GumBGImgPosition:
                            if (!string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                string[] gbImgPosition = loc.ConfigurationValue.Split(',');
                                for (int i = 0; i < gbImgPosition.Length; i++)
                                {
                                    if (i == 0)
                                        txtScorePos1.Text = gbImgPosition[0];
                                    if (i == 1)
                                        txtScorePos2.Text = gbImgPosition[1];
                                    if (i == 2)
                                        txtScorePos3.Text = gbImgPosition[2];
                                    if (i == 3)
                                        txtScorePos4.Text = gbImgPosition[3];
                                    if (i == 4)
                                        txtScorePos5.Text = gbImgPosition[4];
                                    if (i == 5)
                                        txtScorePos6.Text = gbImgPosition[5];
                                }
                            }
                            break;
                    }
                }
            }
            else
                ResetControls();
            if (cmbLocation.SelectedIndex == 0)
            {
                chkIsGumPlayerScoreVisible.IsChecked = false;
            }
        }
        private void bntBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtFilePath.Text = dialog.SelectedPath;
            }
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (!IsNumber(txtFontSize.Text))
                {
                    MessageBox.Show("Please enter only numeric value in font size!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!IsNumber(txtTimeOut.Text))
                {
                    MessageBox.Show("Please enter only numeric value in Score File Time Out!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                string errorMessage = Validate();
                if (errorMessage == null)
                {
                    SetValueToProperties();
                    SetPropertiesToLocationList();
                    GetRideInfo(Config.SubStoreId);
                }
                else
                    MessageBox.Show(errorMessage, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private string GetAllPlayerScorePosition()
        {
            string pos = string.Empty;
            pos = cmbPosition1.Text + "," + cmbPosition2.Text + "," + cmbPosition3.Text + "," + cmbPosition4.Text + "," + cmbPosition5.Text + "," + cmbPosition6.Text;
            return pos;
        }
        private string GetPlayerScoreMargins(int player)
        {
            string margin = string.Empty;
            switch (player)
            {
                case 1:
                    margin = txtLeftMargin1.Text.ToString() + "," + txtTopMargin1.Text.ToString() + "," + txtRightMargin1.Text.ToString() + "," + txtBottomMargin1.Text.ToString();
                    break;
                case 2:
                    margin = txtLeftMargin2.Text.ToString() + "," + txtTopMargin2.Text.ToString() + "," + txtRightMargin2.Text.ToString() + "," + txtBottomMargin2.Text.ToString();
                    break;
                case 3:
                    margin = txtLeftMargin3.Text.ToString() + "," + txtTopMargin3.Text.ToString() + "," + txtRightMargin3.Text.ToString() + "," + txtBottomMargin3.Text.ToString();
                    break;
                case 4:
                    margin = txtLeftMargin4.Text.ToString() + "," + txtTopMargin4.Text.ToString() + "," + txtRightMargin4.Text.ToString() + "," + txtBottomMargin4.Text.ToString();
                    break;
                case 5:
                    margin = txtLeftMargin5.Text.ToString() + "," + txtTopMargin5.Text.ToString() + "," + txtRightMargin5.Text.ToString() + "," + txtBottomMargin5.Text.ToString();
                    break;
                case 6:
                    margin = txtLeftMargin6.Text.ToString() + "," + txtTopMargin6.Text.ToString() + "," + txtRightMargin6.Text.ToString() + "," + txtBottomMargin6.Text.ToString();
                    break;
                default:
                    break;
            }
            return margin;
        }
        private string GetAllPlayerScoreMargins()
        {
            string margins = string.Empty;
            for (int i = 1; i < 7; i++)
            {
                margins += GetPlayerScoreMargins(i) + ":";
            }
            if (!string.IsNullOrEmpty(margins) && margins.Length > 0)
            {
                margins = margins.Substring(0, margins.Length - 1);
            }
            return margins;
        }
        private void SetAllPlayerScorePosition(string AllPostitions)
        {
            string[] singlePosition = AllPostitions.Split(',');
            if (singlePosition.Length > 0)
            {
                for (int i = 0; i < singlePosition.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            cmbPosition1.Text = Convert.ToString(singlePosition[i]);
                            break;
                        case 1:
                            cmbPosition2.Text = Convert.ToString(singlePosition[i]);
                            break;
                        case 2:
                            cmbPosition3.Text = Convert.ToString(singlePosition[i]);
                            break;
                        case 3:
                            cmbPosition4.Text = Convert.ToString(singlePosition[i]);
                            break;
                        case 4:
                            cmbPosition5.Text = Convert.ToString(singlePosition[i]);
                            break;
                        case 5:
                            cmbPosition6.Text = Convert.ToString(singlePosition[i]);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void SetAllPlayerScoreMargins(string AllMargins)
        {
            string[] singleMargin;
            string scoreMargin = string.Empty;
            try
            {
                string[] mrg = AllMargins.Split(':');
                for (int i = 0; i < mrg.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin1.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin1.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin1.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin1.Text = Convert.ToString(singleMargin[3]);
                            break;
                        case 1:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin2.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin2.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin2.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin2.Text = Convert.ToString(singleMargin[3]);
                            break;
                        case 2:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin3.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin3.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin3.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin3.Text = Convert.ToString(singleMargin[3]);
                            break;
                        case 3:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin4.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin4.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin4.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin4.Text = Convert.ToString(singleMargin[3]);
                            break;
                        case 4:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin5.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin5.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin5.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin5.Text = Convert.ToString(singleMargin[3]);
                            break;
                        case 5:
                            singleMargin = mrg[i].ToString().Split(',');
                            txtLeftMargin6.Text = Convert.ToString(singleMargin[0]);
                            txtTopMargin6.Text = Convert.ToString(singleMargin[1]);
                            txtRightMargin6.Text = Convert.ToString(singleMargin[2]);
                            txtBottomMargin6.Text = Convert.ToString(singleMargin[3]);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            ResetControls();
            // cmbLocation.SelectedIndex = 0;
        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);
        }
        private void txtFilePath_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                ToolTipService.SetIsEnabled(txtFilePath, true);
                txtFilePath.ToolTip = txtFilePath.Text;
            }
        }
        private void txtInputImage_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtInputImage.Text))
            {
                ToolTipService.SetIsEnabled(txtInputImage, true);
                txtInputImage.ToolTip = txtInputImage.Text;
            }
        }
        private void bntBrowseInputImage_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtInputImage.Text = dialog.SelectedPath;
            }
        }
        #endregion

        private void cmbPosition1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition1.Text))
            {
                ResetMargin(1);
                EnableRequiredMargins(1, cmbPosition1.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition2.Text))
            {
                ResetMargin(2);
                EnableRequiredMargins(2, cmbPosition2.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition3.Text))
            {
                ResetMargin(3);
                EnableRequiredMargins(3, cmbPosition3.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition4.Text))
            {
                ResetMargin(4);
                EnableRequiredMargins(4, cmbPosition4.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition5.Text))
            {
                ResetMargin(5);
                EnableRequiredMargins(5, cmbPosition5.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition6_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition6.Text))
            {
                ResetMargin(6);
                EnableRequiredMargins(6, cmbPosition6.SelectedValue.ToString().Substring(38));
            }
        }

        //-----Start--------Nilesh--------Added Gumball score dynamically------24th Feb 2018--
        private void bntBrowseImg_Click(object sender, RoutedEventArgs e)
        {
            // System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();            
            //System.Windows.Forms.DialogResult result = dialog.ShowDialog;

            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();

            if (result == true)
            {
                txtBgImgPath1.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath1, true);
                txtBgImgPath1.ToolTip = txtBgImgPath1.Text;
            }
        }
        private void bntBrowseImg2_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                txtBgImgPath2.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBgImgPath2.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath2, true);
                txtBgImgPath2.ToolTip = txtBgImgPath2.Text;
            }
        }

        private void bntBrowseImg3_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                txtBgImgPath3.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBgImgPath3.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath3, true);
                txtBgImgPath3.ToolTip = txtBgImgPath3.Text;
            }
        }

        private void bntBrowseImg4_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                txtBgImgPath4.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath4_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBgImgPath4.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath4, true);
                txtBgImgPath4.ToolTip = txtBgImgPath4.Text;
            }
        }

        private void bntBrowseImg5_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                txtBgImgPath5.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath5_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBgImgPath5.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath5, true);
                txtBgImgPath5.ToolTip = txtBgImgPath5.Text;
            }
        }

        private void bntBrowseImg6_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                txtBgImgPath6.Text = dialog.FileName;
            }
        }

        private void txtBgImgPath6_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBgImgPath6.Text))
            {
                ToolTipService.SetIsEnabled(txtBgImgPath6, true);
                txtBgImgPath6.ToolTip = txtBgImgPath5.Text;
            }
        }

        private string GetAllPlayerBGImgPath()
        {
            string bgImgPath = string.Empty;
            string destBGImgPath = string.Empty;
            bgImgPath = txtBgImgPath1.Text + "," + txtBgImgPath2.Text + "," + txtBgImgPath3.Text + "," + txtBgImgPath4.Text + "," + txtBgImgPath5.Text + "," + txtBgImgPath6.Text;
            //-------Create Folder to save BG image------

            if (!string.IsNullOrEmpty(txtBgImgPath1.Text))
            {
                destBGImgPath = CreateFolderForGumBall(txtBgImgPath1.Text);
            }
            if (!string.IsNullOrEmpty(txtBgImgPath2.Text))
            {
                destBGImgPath += ",";
                destBGImgPath += CreateFolderForGumBall(txtBgImgPath2.Text);
            }
            if (!string.IsNullOrEmpty(txtBgImgPath3.Text))
            {
                destBGImgPath += ",";
                destBGImgPath += CreateFolderForGumBall(txtBgImgPath3.Text);
            }
            if (!string.IsNullOrEmpty(txtBgImgPath4.Text))
            {
                destBGImgPath += ",";
                destBGImgPath += CreateFolderForGumBall(txtBgImgPath4.Text);
            }
            if (!string.IsNullOrEmpty(txtBgImgPath5.Text))
            {
                destBGImgPath += ",";
                destBGImgPath += CreateFolderForGumBall(txtBgImgPath5.Text);
            }
            if (!string.IsNullOrEmpty(txtBgImgPath6.Text))
            {
                destBGImgPath += ",";
                destBGImgPath += CreateFolderForGumBall(txtBgImgPath6.Text);
            }
            //CreateFolderForGumBall(bgImgPath);
            //  return bgImgPath;
            return destBGImgPath;
        }

        private string GetAllPlayerBGImgHeightWidth()
        {
            string bgImgHeightWidth = string.Empty;
            bgImgHeightWidth = txtImgHeightWidth1.Text + "," + txtImgHeightWidth2.Text + "," + txtImgHeightWidth3.Text + "," + txtImgHeightWidth4.Text + "," + txtImgHeightWidth5.Text + "," + txtImgHeightWidth6.Text;
            return bgImgHeightWidth;
        }

        private string GetAllPlayerBGImgPosition()
        {
            string scorePos = string.Empty;
            scorePos = txtScorePos1.Text + "," + txtScorePos2.Text + "," + txtScorePos3.Text + "," + txtScorePos4.Text + "," + txtScorePos5.Text + "," + txtScorePos6.Text;
            return scorePos;
        }

        private string CreateFolderForGumBall(string sourceImgFileName)
        {
            string hotfolderPath = ConfigManager.DigiFolderPath;
            string gumballFolderName = ConfigurationManager.AppSettings["GumballBGImgFolderName"];
            string destpathString = System.IO.Path.Combine(hotfolderPath, gumballFolderName);
            string locationpathString = System.IO.Path.Combine(hotfolderPath, gumballFolderName);
            string destFile = string.Empty;
            try
            {
                if (!Directory.Exists(destpathString))
                {
                    Directory.CreateDirectory(destpathString);
                }
                string DeslocationpathString = System.IO.Path.Combine(destpathString, cmbLocation.Text.ToString());
                if (!Directory.Exists(DeslocationpathString))
                {
                    Directory.CreateDirectory(DeslocationpathString);
                }

                string desLocationDateFolder = System.IO.Path.Combine(DeslocationpathString, DateTime.Today.ToShortDateString());
                string sourceFile = string.Empty;
                if (sourceImgFileName.Contains(".png"))
                {
                    sourceFile = sourceImgFileName;
                }
                else
                {
                    sourceFile = System.IO.Path.Combine(sourceImgFileName, System.IO.Path.GetFileName(sourceImgFileName));
                }
                //string sourceFile = System.IO.Path.Combine(sourceImgFileName, System.IO.Path.GetFileName(sourceImgFileName));

                destFile = System.IO.Path.Combine(DeslocationpathString, System.IO.Path.GetFileName(sourceImgFileName));

                System.IO.File.Copy(sourceFile, destFile, true);
            }
            catch (Exception ex)
            {
                return destFile;
            }
            return destFile;
        }
        //-----End--------Nilesh--------Added Gumball score dynamically------24th Feb 2018--
    }

}
