﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for PosSubStoreMapping.xaml
    /// </summary>
    public partial class PosSubStoreMapping : UserControl
    {
        public long _substoreId;
        public Int64 iMixPOSDetailID = 0;
        public bool isEdit = false;
        ImixPOSDetail imixPosDettail;
        ServicePosInfoBusiness servicePosInfoBusiness;
        public PosSubStoreMapping()
        {
            InitializeComponent();
            GetSubstoreDetails();
            //GetPosDetails();
            MappedPosDetail();
        }
        private bool IsValidate()
        {
            bool _flag = true;
            if (CmbSubStore.SelectedIndex == 0)
            {
                MessageBox.Show("Please Select Site ", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(txtSystem.Text.Trim()))
            {
                MessageBox.Show("Please Enter System Name ", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(txtMACAddress.Text.Trim()))
            {
                MessageBox.Show("Please Enter MAC Address ", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(txtIPAddress.Text.Trim()))
            {
                MessageBox.Show("Please Enter IP Address ", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            imixPosDettail = new ImixPOSDetail();
            servicePosInfoBusiness = new ServicePosInfoBusiness();
            imixPosDettail.MacAddress = txtMACAddress.Text.Trim();
            imixPosDettail.SystemName = txtSystem.Text.Trim();
            imixPosDettail.IPAddress = txtIPAddress.Text.Trim();
            int count = servicePosInfoBusiness.ValidatePosDetails(imixPosDettail);
            if (count > 0 && !isEdit)
            {
                MessageBox.Show("POS with given details alreday exists", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return _flag;
        }
        private void btnUpdatePosMapping_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (IsValidate())
                {
                    int Count = 0;
                    int runningServiceByPosId;
                    long imixId = 0;
                    string SyncCode = FrameworkHelper.CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.PosDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    //bool flag = false;
                    imixPosDettail = new ImixPOSDetail();

                    if (CmbSubStore.SelectedItem == null)
                    {
                        MessageBox.Show("Please Select Site ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    else
                    {
                        imixPosDettail.SubStoreID = ((DigiPhoto.IMIX.Model.SubStore)(CmbSubStore.SelectedItem)).DG_SubStore_pkey;
                    }
                    servicePosInfoBusiness = new ServicePosInfoBusiness();

                    runningServiceByPosId = servicePosInfoBusiness.GetAnyRunningServiceByPosIdBusiness(imixId);
                    if (runningServiceByPosId > 0)
                    {
                        MessageBox.Show("Some Service(s) Running on  " + txtSystem.Text + ". Stop all the services and try again",
                            "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        if (isEdit)
                            imixPosDettail.ImixPOSDetailID = iMixPOSDetailID;
                        else
                            imixPosDettail.ImixPOSDetailID = 0;
                        imixPosDettail.CreatedBy = "webusers";
                        imixPosDettail.IsStart = false;
                        imixPosDettail.SyncCode = SyncCode;
                        imixPosDettail.MacAddress = txtMACAddress.Text.Trim();
                        imixPosDettail.SystemName = txtSystem.Text.Trim();
                        imixPosDettail.IPAddress = txtIPAddress.Text.Trim();
                        Count = servicePosInfoBusiness.InsertImixPosBusiness(imixPosDettail);
                        if (isEdit && Count > 0)
                            MessageBox.Show("Record updated successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        else if (Count > 0)
                            MessageBox.Show("Record added Successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        else
                            MessageBox.Show("Some error occured! Please check error log for more details.", "Information", MessageBoxButton.OK, MessageBoxImage.Error);
                        
                        ResetControl();
                        MappedPosDetail();

                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                string code = btnSender.Tag.ToString();
                var runIDs = code;
                string[] separators = { "$$" };
                var listSplit = runIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                iMixPOSDetailID = Int64.Parse(listSplit[0]);
                string SubStoreName = listSplit[2];
                long SubStoreId = long.Parse(listSplit[3]);
                _substoreId = SubStoreId;
                CmbSubStore.SelectedValue = SubStoreId.ToString();
                txtSystem.Text = listSplit[1];
                txtMACAddress.Text = listSplit[4];
                txtIPAddress.Text = listSplit[5];
                isEdit = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int Count = 0;
                if (MessageBox.Show("Do you want to delete this Pos detail?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Button btnSender = (Button)sender;
                    string code = btnSender.Tag.ToString();
                    var runIDs = code;
                    string[] separators = { "$$" };
                    var listSplit = runIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    iMixPOSDetailID = Int64.Parse(listSplit[0]);
                    servicePosInfoBusiness = new ServicePosInfoBusiness();
                    Count = servicePosInfoBusiness.DeletePosDetailBasedonID(iMixPOSDetailID);
                    if (Count != 0)
                    {
                        MessageBox.Show("Record deleted successfully.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        ResetControl();
                        MappedPosDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResetControl();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnStopDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("This Operation will forcefully stop all the running service(s) on the selected system. please confirm ?", "Delete Fom DB", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    Button btnSender = (Button)sender;
                    // GetSubstoreDetails();
                    Int32 ImixID = Convert.ToInt32(btnSender.Tag);
                    ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
                    servicePosBusiness.StartStopServiceByPosIdBusiness(0, 0, ImixID, false, "Webusers");
                    MessageBox.Show("Data Updated Successfully");
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                MessageBox.Show("Some error occured");
            }

        }
        public void ResetControl()
        {
            CmbSubStore.Text = "";
            txtSystem.Text = "";
            txtIPAddress.Text = "";
            txtMACAddress.Text = "";
            iMixPOSDetailID = 0;
            isEdit = false;
            CmbSubStore.SelectedIndex = 0;
        }
        public void GetSubstoreDetails()
        {
            List<SubStore> substore = new List<SubStore>();
            ServicePosInfoBusiness servicePosBusines = new ServicePosInfoBusiness();
            substore = servicePosBusines.GetSubstoreDetailsBusiness();
            CmbSubStore.ItemsSource = substore;
            substore.Insert(0, new SubStore()
            {
                DG_SubStore_Name = "--Select--",
                DG_SubStore_pkey = 0
            });
            CmbSubStore.Text = substore[0].DG_SubStore_Name;
            CmbSubStore.DisplayMemberPath = "DG_SubStore_Name";
            CmbSubStore.SelectedValuePath = "DG_SubStore_pkey";
            CmbSubStore.SelectedIndex = 0;

        }
        public void GetPosDetails()
        {
            List<ImixPOSDetail> imixPosDetail = new List<ImixPOSDetail>();
            ServicePosInfoBusiness servicePosBusines = new ServicePosInfoBusiness();
            imixPosDetail = servicePosBusines.GetPosDetailsBusiness();
            if (imixPosDetail.Count > 0)
            {
                CmbPosDetail.ItemsSource = imixPosDetail;
                CmbPosDetail.Text = imixPosDetail[0].SystemName;
                CmbPosDetail.DisplayMemberPath = "SystemName";
                CmbPosDetail.SelectedValuePath = "ImixPOSDetailID";
            }
        }
        public void MappedPosDetail()
        {
            List<MappedPos> mappedPosDetail = new List<MappedPos>();
            ServicePosInfoBusiness servicePosBusines = new ServicePosInfoBusiness();
            mappedPosDetail = servicePosBusines.MappedPosDetailBusiness();
            grdMappedPosDetail.ItemsSource = mappedPosDetail;
        }

    }
}
