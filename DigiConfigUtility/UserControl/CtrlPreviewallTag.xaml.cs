﻿using DigiConfigUtility.Utility;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for CtrlPreviewallTag.xaml
    /// </summary>
    public partial class CtrlPreviewallTag : UserControl
    {
        #region Variable
        Int64 iMIXStoreConfigurationValueId = 0;
        #endregion
        public CtrlPreviewallTag()
        {
            InitializeComponent();
            GetDummyTagDetail();
        }
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                iMIXStoreConfigurationValueId = ((iMIXStoreConfigurationInfo)(grdTagDetail.SelectedItem)).iMIXStoreConfigurationValueId;
                txtTag.Text = ((iMIXStoreConfigurationInfo)(grdTagDetail.SelectedItem)).ConfigurationValue;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ConfigBusiness conBiz = new ConfigBusiness();
                Boolean IsDeleted = conBiz.DeletePreviewDummyTag(((iMIXStoreConfigurationInfo)(grdTagDetail.SelectedItem)).iMIXStoreConfigurationValueId);

                if (IsDeleted)
                {
                    MessageBox.Show("Tag deleted successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                GetDummyTagDetail();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public void GetDummyTagDetail()
        {
            try
            {
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXStoreConfigurationInfo> ConfigValuesList = (conBiz.GetStoreConfigData()).Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.PreviewWallDummyTag)).ToList();
                if (ConfigValuesList != null && ConfigValuesList.Count > 0)
                {
                    grdTagDetail.ItemsSource = ConfigValuesList;
                    grdTagDetail.Visibility = Visibility.Visible;
                }
                else
                    grdTagDetail.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSaveTag_Click(object sender, RoutedEventArgs e)
        {

            //write code here to add tags 
            try
            {
                iMIXStoreConfigurationInfo ConfigValue = new iMIXStoreConfigurationInfo();

                if (!string.IsNullOrEmpty(txtTag.Text))
                {
                    ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.PreviewWallDummyTag);
                    ConfigValue.ConfigurationValue = txtTag.Text.Trim();
                    ConfigValue.iMIXStoreConfigurationValueId = iMIXStoreConfigurationValueId;
                }

                ConfigBusiness conBiz = new ConfigBusiness();
                Boolean IsSaved = conBiz.SaveUpdatePreviewDummyTag(ConfigValue);

                if (IsSaved)
                {
                    txtTag.Text = "";
                    if(iMIXStoreConfigurationValueId == 0)
                        MessageBox.Show("Tag added successfully !", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    else
                        MessageBox.Show("Tag updated successfully !", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    iMIXStoreConfigurationValueId = 0;
                }
                else
                {
                    MessageBox.Show("The Tag already exists !", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                GetDummyTagDetail();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


    }
}
