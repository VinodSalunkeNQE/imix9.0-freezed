﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Data;
using System.Windows.Controls.Primitives;


namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for ServiceInfo.xaml
    /// </summary>

    public partial class Services : System.Windows.Controls.UserControl
    {
        public Services()
        {
            int[] array = new int[10];
            InitializeComponent();
            //services popup 
            servicesbox.SetParent(this);
            managebox.SetParent(this);
            SelectList();
        }
        string id = string.Empty;
        string SBStoreName = "";
        int ColorRow = 0;
        public void SelectList()
        {
            List<ServicePosInfo> servicePosInfo = new List<ServicePosInfo>();
            ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
            servicePosInfo = servicePosInfoBusiness.GetRunningServices();
            grdService.ItemsSource = null;
            grdService.ItemsSource = servicePosInfo;
            grdService.IsEnabled = true;
                   
        }
        public void BindSubstore()
        {

            List<SubStore> SubStore = new List<SubStore>();
            ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
            SubStore = servicePosInfoBusiness.GetSubstoreDetailsBusiness();
            //Grdmain.ItemsSource = null;
            //Grdmain.ItemsSource = SubStore;
            //Grdmain.IsEnabled = true;
        }
        private void btnStatusClick(object sender, RoutedEventArgs e)
        {
           
            Button btnSender = (Button)sender;
            id = btnSender.Tag.ToString();
            var runIDs = id;
             string[] separators = {"$$"};
             var listSplit = runIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            int serviceId = int.Parse(listSplit[0]);
            int imixId = int.Parse(listSplit[1]);
            int substoreId = int.Parse(listSplit[2]);
            string ServiceName = listSplit[3];
            string SubStoreName = listSplit[4];
            int RunLevel = int.Parse(listSplit[5]);
            bool isService = bool.Parse(listSplit[6]);
            string ServicePath = listSplit[7];
         //   servicesbox.se
          //  ServicesPopup servicePopup = new ServicesPopup(serviceId,imixId,substoreId);


            //servicesbox.ServiceId = serviceId;
            //servicesbox.ImixPOSDetailID = imixId;
            //servicesbox.SubStoreID = substoreId;
            servicesbox.GetPOSDetail(substoreId, RunLevel);//for sytem details 
            servicesbox.GetServiceInfo(serviceId, imixId, substoreId, ServiceName, SubStoreName, isService, ServicePath);
            servicesbox.ShowPanHandlerDialog();
            grdService.IsEnabled = false;
           // servicePopup.GetServiceInfo(serviceId, imixId, substoreId);
            
        }

        private void btnManage_Click(object sender, RoutedEventArgs e)
        {
             Button btnSender = (Button)sender;
            id = btnSender.Tag.ToString();
            var runIDs = id;
            string[] separators = { "$$" };
            var listSplit = runIDs.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            int serviceId = int.Parse(listSplit[0]);
            int imixId = int.Parse(listSplit[1]);
            int substoreId = int.Parse(listSplit[2]);
            string ServiceName = listSplit[3];
            string SubStoreName = listSplit[4];
            int RunLevel = int.Parse(listSplit[5]);
            bool isService = bool.Parse(listSplit[6]);
            string ServicePath = listSplit[7];
            managebox.ShowManageLevel(serviceId, ServiceName, imixId, RunLevel, isService);
            managebox.ShowPanHandlerDialog();
            grdService.IsEnabled = false;

        }

        private void grdService_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            DataGridRow row = e.Row;
            DataRowView rView = row.Item as DataRowView;
            if (SBStoreName == "")
            {
                SBStoreName = ((DigiPhoto.IMIX.Model.ServicePosInfo)(row.Item)).SubStoremName;
               //ColorRow = 0;
            }
            else if (SBStoreName != ((DigiPhoto.IMIX.Model.ServicePosInfo)(row.Item)).SubStoremName)
            {
              
                SBStoreName = ((DigiPhoto.IMIX.Model.ServicePosInfo)(row.Item)).SubStoremName;
                if (ColorRow == 0)
                {
                    ColorRow = 1;
                }
                else
                {
                    ColorRow = 0;
                }
                
            }


            //if (ColorRow==0)
            //{
            //    e.Row.Background = Brushes.LightSkyBlue;
            //}
            //else
            //{
            //    e.Row.Background = Brushes.WhiteSmoke;
            //}

         
            if (!string.IsNullOrEmpty(((DigiPhoto.IMIX.Model.ServicePosInfo)(row.Item)).SystemName))
            {                
                e.Row.Style = null;
                e.Row.Background = Brushes.LightGreen;
            }
        }
        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            SelectList();
        }

        //public static T GetVisualChild<T>(Visual parent) where T : Visual
        //{
        //    T child = default(T);
        //    int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
        //    for (int i = 0; i < numVisuals; i++)
        //    {
        //        Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
        //        child = v as T;
        //        if (child == null)
        //        {
        //            child = GetVisualChild<T>
        //            (v);
        //        }
        //        if (child != null)
        //        {
        //            break;
        //        }
        //    }
        //    return child;
        //}
        //public DataGridCell GetCell(int row, int column)
        //{
        //    DataGridRow rowContainer = GetRow(row);
        //    if (rowContainer != null)
        //    {
        //        DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
        //        if (presenter == null)
        //        {
        //            grdService.ScrollIntoView(rowContainer, grdService.Columns[column]);
        //            presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
        //        }
        //        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
        //        return cell;
        //    }
        //    return null;
        //}
        //public DataGridRow GetRow(int index)
        //{
        //    DataGridRow row = (DataGridRow)grdService.ItemContainerGenerator.ContainerFromIndex(index);
        //    if (row == null)
        //    {
        //        grdService.UpdateLayout();
        //        grdService.ScrollIntoView(grdService.Items[index]);
        //        row = (DataGridRow)grdService.ItemContainerGenerator.ContainerFromIndex(index);
        //    }
        //    return row;
        //}

        //public static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        //{
        //    // Search immediate children
        //    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);

        //        if (child is childItem)
        //            return (childItem)child;

        //        else
        //        {
        //            childItem childOfChild = FindVisualChild<childItem>(child);
        //            if (childOfChild != null)
        //                return childOfChild;
        //        }
        //    }
        //    return null;
        //}

       
    }
    //public class RowColorConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        int index = (int)value;
    //        if (index == 0) return Brushes.Aquamarine;
    //        else return Brushes.Violet;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
      

}
