﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Email.xaml
    /// </summary>
    public partial class Email : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = null;
        public Email()
        {
            InitializeComponent();
            GetRecordsForEmailSettings();
            GetScreenSaverSettings();
        }

        /// <summary>
        /// Handles the Click event of the btnsaveEmail control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnsaveEmail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                if (Config.SubStoreId <= 0)
                {
                    MessageBox.Show("Please configure your SubStore");
                    return;
                }
                if (IsEmailSettingsValid())
                {
                    
                    int displaydur = txtEKDisplayDuration.Text.ToString().ToInt32();
                    int ssDelay = txtEKStartTime.Text.ToString().ToInt32();
                    EmailBusniess emaBiz = new EmailBusniess();
                    emaBiz.TruncateEmailSettingsTable();
                    string body = @"Hi,<br>
                    <p>{Message}</p><br><br>" + TBEmailBody.Text + "<br>Thanks,<br>{Sendername}";
                    SaveScreenSaverData(txtEKSamplePath.Text, displaydur.ToString(), ssDelay.ToString(), ((bool)ChkScreenSaverActive.IsChecked).ToString());
                    bool issaved = emaBiz.SetEmailSettings(TBEmailFrom.Text, TBEmailSubject.Text, body, TBEmailServerName.Text, EmailServerPort.Text, (bool)ChkDefaultCredentials.IsChecked, TBEmailUserName.Text, TBEmailPassword.Password, (bool)ChkEmailEnableSSL.IsChecked, TBEmailBcc.Text, Config.SubStoreId);

                    if (issaved)
                    {
                        MessageBox.Show("Email settings saved successfully", "iMix", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        GetRecordsForEmailSettings();
                        GetScreenSaverSettings();
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private bool SaveScreenSaverData(string SamplePath, string displaydur, string ssDelay, string ScreenSaverActive)
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
               // _objDataLayer = new DigiPhotoDataServices();
                //Sample path
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = SamplePath;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.SampleImagePath;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                //displaydur
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = displaydur;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.DisplayDuration;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //ssDelay
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = ssDelay;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ScreenStartTime;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                //ScreenSaverActive
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = ScreenSaverActive;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsScreenSaverActive;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                ConfigBusiness conBiz = new ConfigBusiness();
                bool IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);
                return IsSaved;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;

            }
        }


        /// <summary>
        /// Determines whether [is email settings valid].
        /// </summary>
        /// <returns></returns>
        private bool IsEmailSettingsValid()
        {
            if (TBEmailFrom.Text == "")
            {
                MessageBox.Show("Please enter the Email From");
                return false;

            }
            else if (TBEmailServerName.Text == "")
            {
                MessageBox.Show("Please enter the Server Name");
                return false;
            }
            else if (EmailServerPort.Text == "")
            {
                MessageBox.Show("Please enter the Server Port no");
                return false;
            }
            else if (TBEmailUserName.Text == "")
            {
                MessageBox.Show("Please enter the Server User Name");
                return false;
            }
            else if (TBEmailPassword.Password == "")
            {
                MessageBox.Show("Please enter the Server Password");
                return false;
            }

            else if (ChkScreenSaverActive.IsChecked == true)
            {
                if (txtEKSamplePath.Text == "")
                {
                    MessageBox.Show("Please enter sample images path");
                    return false;
                }
                else if (String.IsNullOrEmpty(txtEKStartTime.Text.Trim()))
                {
                    MessageBox.Show("Please enter starts after delay (in seconds)");
                    return false;
                }
                else if (String.IsNullOrEmpty(txtEKDisplayDuration.Text.Trim()))
                {
                    MessageBox.Show("Please enter display duration (in seconds)");
                    return false;
                }
                else if (txtEKStartTime.Text.Trim().ToInt32() < 1)
                {

                    MessageBox.Show("Starts after delay value should be greater than 0");
                    return false;

                }
                else if (txtEKDisplayDuration.Text.Trim().ToInt32() < 1)
                {

                    MessageBox.Show("Display duration should be greater than 0");
                    return false;

                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Gets the records for email settings.
        /// </summary>
        private void GetRecordsForEmailSettings()
        {
            try
            {
               // _objDataLayer = new DigiPhotoDataServices();
                EmailBusniess emaBiz = new EmailBusniess();
                var item = emaBiz.GetEmailSettingsDetails();
                if (item != null)
                {
                    TBEmailBcc.Text = item.DG_MailBCC;
                    TBEmailUserName.Text = item.DG_SmtpServerUsername;
                    TBEmailSubject.Text = item.DG_MailSubject;
                    TBEmailBody.Text = item.DG_MailBody.Replace(@"Hi,<br>
                    <p>{Message}</p><br><br>","").Replace(@"<br>Thanks,<br>{Sendername}", "");
                    TBEmailServerName.Text = item.DG_SmtpServername;
                    TBEmailPassword.Password = item.DG_SmtpServerPassword;
                    TBEmailFrom.Text = item.DG_MailSendFrom;
                    EmailServerPort.Text = item.DG_SmtpServerport;
                    ChkEmailEnableSSL.IsChecked = item.DG_SmtpServerEnableSSL;
                    ChkDefaultCredentials.IsChecked = item.DG_SmtpUserDefaultCredentials;

                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private void GetScreenSaverSettings()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(Config.SubStoreId);
                if (ConfigValuesList != null && ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            //30
                            case (int)ConfigParams.SampleImagePath:
                                txtEKSamplePath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.DisplayDuration:
                                txtEKDisplayDuration.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "10";
                                break;
                            case (int)ConfigParams.ScreenStartTime:
                                txtEKStartTime.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "10";
                                break;
                            case (int)ConfigParams.IsScreenSaverActive:
                                ChkScreenSaverActive.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnreset control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnreset_Click(object sender, RoutedEventArgs e)
        {
            TBEmailBcc.Text = "";
            TBEmailUserName.Text = "";
            TBEmailSubject.Text = "";
            TBEmailBody.Text = "";
            TBEmailServerName.Text = "";
            TBEmailPassword.Password = "";
            TBEmailFrom.Text = "";
            EmailServerPort.Text = "";
            ChkEmailEnableSSL.IsChecked = false;
            ChkDefaultCredentials.IsChecked = false;
        }
        /// <summary>
        /// Handles the Click event of the btnBrowseForHTMLFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBrowseForHTMLFile_Click(object sender, RoutedEventArgs e)
        {
            Stream checkstream = null;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "HTML Files | *.html*";
            ofd.ShowDialog();
            try
            {
                if ((checkstream = ofd.OpenFile()) != null)
                {
                    System.IO.StreamReader oStream = new System.IO.StreamReader(ofd.FileName.ToString());
                    TBEmailBody.Text = oStream.ReadToEnd();
                    oStream.Close();
                    oStream.Dispose();
                    oStream = null;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        /// <summary>
        /// Handles the Checked event of the Chkhtmlfile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Chkhtmlfile_Checked(object sender, RoutedEventArgs e)
        {
            TBEmailBody.IsReadOnly = true;
            btnBrowseForHTMLFile.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Handles the Unchecked event of the Chkhtmlfile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Chkhtmlfile_Unchecked(object sender, RoutedEventArgs e)
        {
            TBEmailBody.IsReadOnly = false;
            btnBrowseForHTMLFile.Visibility = Visibility.Collapsed;
            GetRecordsForEmailSettings();
        }

        /// <summary>
        /// Handles the Click event of the btnhotfolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnhotfolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                var result = fbDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtEKSamplePath.Text = fbDialog.SelectedPath + "\\";
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

    }
}
