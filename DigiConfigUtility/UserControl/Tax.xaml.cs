﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Tax.xaml
    /// </summary>
    public partial class Tax : UserControl, INotifyPropertyChanged
    {

        # region Property

        public int inActiveTaxId = 0;
        protected decimal taxpercentage;
        public decimal TaxPercentage
        {
            get { return taxpercentage; }
            set
            {
                if (value != null)
                {
                    taxpercentage = value;
                    NotifyPropertyChanged("TaxPercentage");
                }
            }
        }

        protected bool status;
        public bool Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Status"));
                }
            }
        }

        protected int venueId;
        public int VenueId
        {
            get { return venueId; }

            set
            {
                if (value != null)
                {
                    venueId = value;
                    NotifyPropertyChanged("VenueId");
                }
                ;
            }
        }

        private int taxId;
        public int TaxId
        {
            get { return taxId; }
            set
            {
                if (value != null)
                {
                    taxId = value;
                    NotifyPropertyChanged("TaxId");
                }
                ;
            }
        }


        private ObservableCollection<TaxDetailInfo> taxDetails;
        public ObservableCollection<TaxDetailInfo> TaxDetails
        {
            get { return taxDetails; }
            set { taxDetails = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Contructor
        public Tax()
        {
            InitializeComponent();
            DataContext = this;
            GetTaxMasterDetails();
            // int selectedTaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)(cmbTaxType.SelectionBoxItem)).TaxId;
            GetVenueTaxValueDetails(0);
        }
        #endregion

        #region Private Methods

        private void GetTaxMasterDetails()
        {
            try
            {
                List<TaxDetailInfo> Tax = new List<TaxDetailInfo>();
                TaxBusiness TaxData = new TaxBusiness();
                TaxDetails = TaxData.GetTaxDetail();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }

        }


        //private void GetVenueTaxValueDetails()
        //{
        //    try
        //    {
        //        List<VenueTaxValueModel> VenueTax = new List<VenueTaxValueModel>();
        //        VenueTaxValueBusiness VenueTaxData = new VenueTaxValueBusiness();
        //        var Data = VenueTaxData.GetVenueTaxValue();
        //        foreach (var item in Data)
        //        {
        //            TaxPercentage = item.TaxPercentage;
        //            Status = item.IsActive;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
        //    }
        //}
        private void GetVenueTaxValueDetails(int TaxId)
        {
            try
            {
                if (cmbTaxType.ItemsSource != null)
                {
                    int selectedTaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)(cmbTaxType.SelectedItem)).TaxId;
                    List<VenueTaxValueModel> VenueTax = new List<VenueTaxValueModel>();
                    VenueTaxValueBusiness VenueTaxData = new VenueTaxValueBusiness();
                    var Data = VenueTaxData.GetVenueTaxValue(TaxId);
                    /*   Code Changes Made By ABM For VAT and GST Tax Enabling Issue    */
                    foreach (var item in Data)
                    {
                        TaxPercentage = item.TaxPercentage;
                        Status = item.IsActive;
                        var tax = string.Format("{0:0.000}", TaxPercentage);
                        txtTaxPercentage.Text = tax.ToString();
                    }
                    if (Data.Count == 0)
                    {
                        Status = false;
                        txtTaxPercentage.Text = "0.0";
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }
        }

        private void GetStoreDetails()
        {
            try
            {
                List<StoreInfo> store = new List<StoreInfo>();
                StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                var list = StoreData.GetStore();
                VenueId = list.DG_Store_pkey;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private bool SaveTaxDetails()
        {
            bool success = false;
            try
            {
                List<TaxDetailInfo> TaxDetails = new List<TaxDetailInfo>();
                TaxBusiness TaxData = new TaxBusiness();
                DateTime modifiedate = DateTime.Now;
                TaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)(cmbTaxType.SelectionBoxItem)).TaxId;
                if (!status)
                {
                    inActiveTaxId = 0;
                }
                if (taxId == inActiveTaxId)
                {
                    inActiveTaxId = 0;
                }
                if(!Status)
                {
                    TaxPercentage = 0.00m;
                }
                success = TaxData.SaveTaxDetails(VenueId, TaxId, inActiveTaxId, TaxPercentage, Status, modifiedate);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }
            return success;
        }

        private bool IsNumberKey(Key inputkey)
        {
            if (inputkey == Key.Decimal || inputkey == Key.OemPeriod)
                return true;
            if (inputkey < Key.D0 || inputkey > Key.D9)
            {
                if (inputkey < Key.NumPad0 || inputkey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Validation

        private bool Validation()
        {
            if (cmbTaxType.SelectedItem == null)
            {
                MessageBox.Show("Please Select Site", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            if (String.IsNullOrWhiteSpace(txtTaxPercentage.Text))
            {
                MessageBox.Show("Please enter valid Tax Percentage", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false; 
            }
            //-------Start-----------Nilesh----------Added Condition to check the active---------------
            //if (!chkActive.IsChecked.Value && txtTaxPercentage.Text != "0.000")
            //{
            //    MessageBox.Show("Please select active or remove Tax percentage", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            //-------End-----------Nilesh----------Added Condition to check the active--------------- 
            if (txtTaxPercentage.Text.Split('.').Count() > 2)
            {
                MessageBox.Show("Please enter valid Tax Percentage", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //-------------Commented as per the requiement from the Suhail from Singapore site-----------
            //if (txtTaxPercentage.Text.Split('.')[0] == "0")
            //{
            //    MessageBox.Show("Please enter valid Tax Percentage", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}
            Regex r = new Regex(@"[~`!@#$%^&*()-+=|\{}':;,<>/?]");
            if (r.IsMatch(txtTaxPercentage.Text))
            {
                MessageBox.Show("Please enter valid Tax Percentage", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (Convert.ToDecimal(txtTaxPercentage.Text) > 100 || Convert.ToDecimal(txtTaxPercentage.Text) < 0)
            {
                MessageBox.Show("Please enter valid Tax Percentage(ranges between 0 and 100)", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;

        }

        #endregion

        #region Event
        /*   Code Changes Made By ABM For VAT and GST Tax Enabling Issue    */
        private void btnSaveTax_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool valid;
                bool success = false;
                valid = Validation();
                if (valid.Equals(false))
                    return;
                GetStoreDetails();
                if (CheckTaxIsChecked())
                {
                    success = SaveTaxDetails();
                }
                //if (CheckTaxIsChecked())
                //{
                //    success = SaveTaxDetails();
                //}
                if (success.Equals(true))
                    MessageBox.Show("Successfully Saved", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                //GetTaxMasterDetails();
                int selectedTaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)(cmbTaxType.SelectionBoxItem)).TaxId;
                GetVenueTaxValueDetails(selectedTaxId);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }
        }
        /*   Code Changes Made By ABM For VAT and GST Tax Enabling Issue    */
        public bool CheckTaxIsChecked()
        {
            /*   Code Changes Made By ABM For VAT and GST Tax Enabling Issue    */
            VenueTaxValueModel objActiveTax = new VenueTaxValueModel();
            objActiveTax = GetActiveTaxValueDetails();
            string TaxName = ((DigiPhoto.IMIX.Model.TaxDetailInfo)cmbTaxType.SelectedValue).TaxName;
            inActiveTaxId = objActiveTax.TaxId;
            if (objActiveTax.TaxId != ((DigiPhoto.IMIX.Model.TaxDetailInfo)cmbTaxType.SelectedValue).TaxId && objActiveTax.IsActive && chkActive.IsChecked == true)

            {
                MessageBoxResult result = MessageBox.Show(objActiveTax.TaxName + " is Active, still you want to make  " + TaxName + "  Active? ", "Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        private void txtTaxPercentage_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }

        private void txtTaxPercentage_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Paste) // e.Command == ApplicationCommands.Copy // || e.Command == ApplicationCommands.Cut ||
            {
                e.Handled = true;
            }
        }

        private void chkStatus_Checked(object sender, RoutedEventArgs e)
        {
            if (chkActive.IsChecked == true)
                Status = true;
            else
                Status = false;
        }
        #endregion

        private void cmbTaxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int TaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)cmbTaxType.SelectedValue).TaxId;
            GetVenueTaxValueDetails(TaxId);

        }

        private VenueTaxValueModel GetActiveTaxValueDetails()
        {
            VenueTaxValueModel objActiveTaxDetails = new VenueTaxValueModel();
            try
            {
                VenueTaxValueBusiness VenueTaxData = new VenueTaxValueBusiness();
                objActiveTaxDetails = VenueTaxData.GetActiveTaxDetails();

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }

            return objActiveTaxDetails;
        }

    }
}

