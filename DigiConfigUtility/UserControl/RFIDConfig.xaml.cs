﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.ServiceProcess;
using DigiPhoto.Common;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for RFIDConfig.xaml
    /// </summary>
    public partial class RFIDConfig : System.Windows.Controls.UserControl
    {
        System.ComponentModel.BackgroundWorker FlushWorker = new System.ComponentModel.BackgroundWorker();
        // DigiPhotoDataServices _objDataLayer = null;
        ConfigBusiness configBusiness = null;
        Dictionary<int, string> lstLocationList;
        DateTime fromDate = DateTime.Now;
        DateTime toDate = DateTime.Now;
        Regex regex = new Regex(@"^[0-9]{0,4}$"); //regex that allows numeric input only
        BusyWindow bs = new BusyWindow();
        int locId = 0;
        int NoOfExcludeDays = 5;
        List<RfidInfo> lstRfidInfo = null;
        public RFIDConfig()
        {
            InitializeComponent();
            FlushWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(FlushWorker_DoWork);
            FlushWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(FlushWorker_RunWorkerCompleted);
            GetRfidInfo(); //Created By Gaurav
            FillLocationCombo();
            fromDate = DateTime.Now.Date.Add(new TimeSpan(-7, 0, 0, 0));
            toDate = DateTime.Now.Date.Add(new TimeSpan(7, 0, 0, 0));
            rbGBWithSeperatorPreScan.IsChecked = true;
            rbHourWiseFlush.IsChecked = true;
            GetFlushHistoryData(fromDate, toDate);
				////added by latika for table workflow
            LoadFontStyle();
            GetTableInfo(Convert.ToInt32(cmbLocation.SelectedValue));

            EnableRequiredMargins(1, cmbPosition1.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(2, cmbPosition2.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(3, cmbPosition3.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(4, cmbPosition4.SelectedValue.ToString().Substring(38));
            ////end
        }
			////added by latika for table workflow
        private void LoadFontStyle()
        {
            List<string> fonts = new List<string>();
            System.Drawing.Text.InstalledFontCollection col = new System.Drawing.Text.InstalledFontCollection();
            foreach (System.Drawing.FontFamily family in col.Families)
            {
                fonts.Add(family.Name);
            }
            cmbFontFamily.ItemsSource = fonts;
            cmbFontFamily.Text = "Times New Roman";
        }
        private void btnDeleteGumRideSettings_Click(object sender, RoutedEventArgs e)
        {
            string locid = ((Button)sender).Tag.ToString();

            if (MessageBox.Show("Do you want to delete this Config?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (!string.IsNullOrWhiteSpace(locid))
                {
                    configBusiness = new ConfigBusiness();
                    configBusiness.DeleteLocationWiseConfigParamsGumbleRide(Convert.ToInt32(locid));
                    MessageBox.Show("Config deleted successfully", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbLocation.SelectedIndex = 0;
                   // GetRideInfo(Config.SubStoreId);
                }
            }
        }
		///end
        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                StoreSubStoreDataBusniess stoBiz = new DigiPhoto.IMIX.Business.StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = stoBiz.GetLocationSubstoreWise(Config.SubStoreId);
               // lstLocationList.Add(0, "All Locations");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name.ToString());
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSaveRFIDConfig_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^[0-9]{0,4}$"); //regex that allows numeric input only
            Regex regexEmail = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (Config.SubStoreId <= 0)
            {
                MessageBox.Show("Please configure your SubStore.");
                return;
            }
            if (cmbLocation.SelectedValue == null)
            {
                MessageBox.Show("Select Location.");
                return;
            }
            if (string.IsNullOrEmpty(txtFlusDays.Text.Trim()))
            {
                txtFlusDays.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFlusHours.Text.Trim()))
            {
                txtFlusHours.Text = "0";
            }

            if (chkIsEnabledRFID.IsChecked == true && (!regex.IsMatch(txtFlusHours.Text.Trim()) || !regex.IsMatch(txtFlusDays.Text.Trim())))
            {
                MessageBox.Show("Invalid value for days or hours.");
                return;
            }
            int noOfHours = (txtFlusDays.Text.Trim().ToInt32() * 24) + txtFlusHours.Text.Trim().ToInt32();

            if (chkIsEnabledRFID.IsChecked == false && chkIsEnabledMobileRfid.IsChecked == false)
            {
                //veerendra
                //rbPreScan.IsChecked = true;
                txtFlusHours.Text = "0";
                txtFlusDays.Text = "0";
            }
            if (rbDayWiseFlush.IsChecked == true)
            {
                if (ccFlushDateTime.Value == null)
                {
                    MessageBox.Show("Select start date time for day wise flush.");
                    return;
                }
                if (chkIsRecursiveFlush.IsChecked == true && cmbSelectRecursiveCount.SelectedIndex <= 0)
                {
                    MessageBox.Show("Select recursive days.");
                    return;
                }
                if (!string.IsNullOrEmpty(txtFlushEmailAddress.Text.Trim()) && !regexEmail.IsMatch(txtFlushEmailAddress.Text.Trim()))
                {
                    MessageBox.Show("Enter a correct receiver email!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DateTime dtProvided = DateTime.Parse(ccFlushDateTime.Value.ToString());
                if (dtProvided < DateTime.Now)
                {
                    MessageBox.Show("Flush cannot be scheduled for previous date or time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (string.IsNullOrEmpty(txtExcludeDays.Text.Trim()) || regex.IsMatch(txtExcludeDays.Text.Trim()) == false)
                {
                    MessageBox.Show("Enter exclude days.");
                    return;
                }

            }

            SaveRFIDSettingsLoc(noOfHours);
                GetRfidInfo();
                GetRfidInfoLocationWise();
           
        }

        private void chkIsEnabledRFID_Checked(object sender, RoutedEventArgs e)
        {
            HideShow(Visibility.Visible);
        }

        private void chkIsEnabledRFID_Unchecked(object sender, RoutedEventArgs e)
        {
            HideShow(Visibility.Collapsed);
        }

        private void HideShow(Visibility visibility, Visibility grpVisibility = Visibility.Visible, Visibility stckMobilepanel = Visibility.Collapsed, Visibility DeleteVisible = Visibility.Collapsed)
        {
            stkRFIDMain.Visibility = visibility;
            grpRFIDConfig.Visibility = grpVisibility;
            stkMobileConfig.Visibility = stckMobilepanel;
            btnRFIDConfigDelete.Visibility = DeleteVisible;
        }

        private void GetConfigData()
        {
            try
            {
                // _objDataLayer = new DigiPhotoDataServices();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.IsEnabledRFID);
                filterValues.Add((long)ConfigParams.RFIDScanType);
                filterValues.Add((long)ConfigParams.RFIDFlushInHour);
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledRFID:
                                chkIsEnabledRFID.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.RFIDScanType:
                                //if (ConfigValuesList[i].ConfigurationValue == null)
                                //    rbPreScan.IsChecked = true;
                                //else if (ConfigValuesList[i].ConfigurationValue.ToInt32() == 0)
                                //    rbPreScan.IsChecked = true;
                                //else if (ConfigValuesList[i].ConfigurationValue.ToInt32() == 1)
                                //    rbPostScan.IsChecked = true;
                                break;
                            case (int)ConfigParams.RFIDFlushInHour:
                                int Hours = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "0");
                                txtFlusHours.Text = (Hours % 24).ToString();
                                txtFlusDays.Text = (Hours / 24).ToString();
                                break;
                        }
                    }
                }
                if (chkIsEnabledRFID.IsChecked == false)
                {
                    txtFlusDays.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void GetConfigLocationData()
        {
            try
            {
                configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.IsEnabledRFID);
                filterValues.Add((long)ConfigParams.RFIDScanType);
                filterValues.Add((long)ConfigParams.RFIDFlushInHour);
                filterValues.Add((long)ConfigParams.RfidFlushType);
                filterValues.Add((long)ConfigParams.RfidFlushDateTime);
                filterValues.Add((long)ConfigParams.IsRecursiveRfid);
                filterValues.Add((long)ConfigParams.RfidRecursiveDays);
                filterValues.Add((long)ConfigParams.RfidFlushReceiverEmail);
                filterValues.Add((long)ConfigParams.RfidRecursiveExcludeDays);
                filterValues.Add((long)ConfigParams.RFIDAssociationType);
                filterValues.Add((long)ConfigParams.RFIDAssociationTimeFrame);
                filterValues.Add((long)ConfigParams.IsMobileRfidEnabled);
                filterValues.Add((long)ConfigParams.NoOfGroupsToAssociate);

                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigLocation(cmbLocation.SelectedValue.ToInt32(), Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledRFID:
                                chkIsEnabledRFID.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsMobileRfidEnabled:
                                chkIsEnabledMobileRfid.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.RFIDScanType:
                                //veerendra
                                //if (ConfigValuesList[i].ConfigurationValue == null || ConfigValuesList[i].ConfigurationValue == "")
                                //    rbPreScan.IsChecked = true;
                                if (ConfigValuesList[i].ConfigurationValue.ToInt32() == (int)ScanType.PreScan)
                                {
                                    rbPreScan.IsChecked = true;
                                    rbPreScanGB.IsChecked = true;
                                    rbGBWithSeperatorPreScan.IsChecked = true;
                                }
                                else if (ConfigValuesList[i].ConfigurationValue.ToInt32() == (int)ScanType.PostScan)
                                {
                                    rbPostScan.IsChecked = true;
                                    rbPostScanGB.IsChecked = true;
                                    rbGBSeperatorPostScanGB.IsChecked = true;
                                }
                                break;
                            case (int)ConfigParams.RFIDFlushInHour:
                                int Hours = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue : "0");
                                txtFlusHours.Text = (Hours % 24).ToString();
                                txtFlusDays.Text = (Hours / 24).ToString();
                                break;
                            case (int)ConfigParams.RfidFlushType:
                                int FlushType = !string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) ? Convert.ToInt32(ConfigValuesList[i].ConfigurationValue) : 0;
                                //rbHourWiseFlush.IsChecked = (FlushType == (Int32)RfidFlushType.HourWise || FlushType == 0) ? true : false;
                                rbHourWiseFlush.IsChecked = FlushType == (Int32)RfidFlushType.HourWise ? true : false;
                                rbDayWiseFlush.IsChecked = FlushType == (Int32)RfidFlushType.DayWise ? true : false;
                                break;
                            case (int)ConfigParams.RfidFlushDateTime:
                                ccFlushDateTime.Value = !string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) ? Convert.ToDateTime(ConfigValuesList[i].ConfigurationValue) : DateTime.Now;
                                break;
                            case (int)ConfigParams.IsRecursiveRfid:
                                chkIsRecursiveFlush.IsChecked = !string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.RfidRecursiveDays:
                                if (!string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue))
                                    cmbSelectRecursiveCount.Text = ConfigValuesList[i].ConfigurationValue;
                                else
                                    cmbSelectRecursiveCount.SelectedIndex = 0;
                                break;
                            case (int)ConfigParams.RfidFlushReceiverEmail:
                                txtFlushEmailAddress.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : string.Empty;
                                break;
                            case (int)ConfigParams.RfidRecursiveExcludeDays:
                                txtExcludeDays.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : string.Empty;
                                break;
                            case (int)ConfigParams.RFIDAssociationType:

                                stkPrePostscanType.Visibility = Visibility.Collapsed;
                                stkTimeBased.Visibility = Visibility.Collapsed;
                                if (Convert.ToInt32((ConfigValuesList[i].ConfigurationValue == "" ? "0" : ConfigValuesList[i].ConfigurationValue)) == (int)RFIDAssociationType.GroupBasedAssociation)
                                {
                                    rbGroupBasedAssociation.IsChecked = true;
                                    stkGroupBased.Visibility = Visibility.Visible;
                                    stkPrePostscanType.Visibility = Visibility.Collapsed;
                                    stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
                                }
                                else if (Convert.ToInt32((ConfigValuesList[i].ConfigurationValue == "" ? "0" : ConfigValuesList[i].ConfigurationValue)) == (int)RFIDAssociationType.TimeBasedAssociation)
                                {
                                    rbTimeBasedAssociation.IsChecked = true;
                                    stkTimeBased.Visibility = Visibility.Visible;
                                    stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
                                }
                                else if (Convert.ToInt32((ConfigValuesList[i].ConfigurationValue == "" ? "0" : ConfigValuesList[i].ConfigurationValue)) == (int)RFIDAssociationType.GroupBasedAssociationWithSeperator)
                                {
                                    rbGroupBasedAssociationWthSeperator.IsChecked = true;
                                    stkPrePostscanType.Visibility = Visibility.Collapsed;
                                    stkGroupBased.Visibility = Visibility.Collapsed;
                                    stkTimeBased.Visibility = Visibility.Collapsed;
                                    stkGroupBasedwithSeperator.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    rbPrePostAssociation.IsChecked = true;
                                    stkPrePostscanType.Visibility = Visibility.Visible;
                                    stkGroupBased.Visibility = Visibility.Collapsed;
                                    stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
                                }
                                break;
                            case (int)ConfigParams.RFIDAssociationTimeFrame:
                                txtTimeFrameSec.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : string.Empty;
                                break;
                            case (int)ConfigParams.NoOfGroupsToAssociate:
                                txtNoOfGroupsToAssociate.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : string.Empty;
                                break;
                        }
                    }
                }
                if (chkIsEnabledRFID.IsChecked == false)
                {
                    txtFlusDays.Text = "0";
                }
                if (rbDayWiseFlush.IsChecked != true)
                {
                    //ResetDaywiseFlush();
                }
                if (chkIsRecursiveFlush.IsChecked != true)
                {
                    cmbSelectRecursiveCount.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        void ResetDaywiseFlush()
        {
            chkIsRecursiveFlush.IsChecked = false;
            //ccFlushDateTime.Value = DateTime.Now;
            DateTime schdlFlush = new DateTime();
            schdlFlush = Convert.ToDateTime(ccFlushDateTime.Value);
            if (schdlFlush.TimeOfDay.ToString() == "00:00:00")
            {
                ccFlushDateTime.Value = schdlFlush.AddHours(11);
            }
            txtExcludeDays.Text = txtFlushEmailAddress.Text = string.Empty;
        }

        private void SaveRFIDSettings(int NoOfHours)
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                int substoreId = Config.SubStoreId;
                //_objDataLayer = new DigiPhotoDataServices();

                //Is Enabled
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = chkIsEnabledRFID.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledRFID;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                if (chkIsEnabledRFID.IsChecked == true)
                {
                    //ConfigValue = new iMIXConfigurationInfo();
                    //ConfigValue.ConfigurationValue = rbPostScan.IsChecked == true ? "1" : "0";
                    //ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDScanType;
                    //ConfigValue.SubstoreId = Config.SubStoreId;
                    //objConfigList.Add(ConfigValue);

                    ConfigValue = new iMIXConfigurationInfo();
                    ConfigValue.ConfigurationValue = NoOfHours.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDFlushInHour;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                }
                ConfigBusiness conBiz = new ConfigBusiness();
                bool IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);
                if (IsSaved)
                {
                    MessageBox.Show("RFID configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void SaveRFIDSettingsLoc(int NoOfHours)
        {
            try
            {
                List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
                iMixConfigurationLocationInfo ConfigValue = null;
                int substoreId = Config.SubStoreId;
                int locationId = cmbLocation.SelectedValue.ToInt32();
                configBusiness = new ConfigBusiness();

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = (rbDayWiseFlush.IsChecked == true ? (Int32)RfidFlushType.DayWise : (Int32)RfidFlushType.HourWise).ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RfidFlushType;
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);

                //Is Enabled
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkIsEnabledRFID.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledRFID;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkIsEnabledMobileRfid.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsMobileRfidEnabled;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }



                if (chkIsEnabledRFID.IsChecked == true || chkIsEnabledMobileRfid.IsChecked == true)
                {
                    if (rbHourWiseFlush.IsChecked == true)
                    {
                        if (chkIsEnabledRFID.IsChecked == true || chkIsEnabledMobileRfid.IsChecked == true)
                        {
                            ConfigValue = new iMixConfigurationLocationInfo();
                            ConfigValue.ConfigurationValue = NoOfHours.ToString();
                            ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDFlushInHour;
                            ConfigValue.SubstoreId = Config.SubStoreId;
                            ConfigValue.LocationId = locationId;
                            objConfigList.Add(ConfigValue);
                        }
                    }
                    else if (rbDayWiseFlush.IsChecked == true)
                    {
                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = ccFlushDateTime.Value.ToString();
                        ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RfidFlushDateTime;
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);

                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = txtExcludeDays.Text.Trim();
                        ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RfidRecursiveExcludeDays;
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);

                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = chkIsRecursiveFlush.IsChecked.ToString();
                        ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsRecursiveRfid;
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);

                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = txtFlushEmailAddress.Text.Trim();
                        ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RfidFlushReceiverEmail;
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);

                        if (chkIsRecursiveFlush.IsChecked == true)
                        {
                            ConfigValue = new iMixConfigurationLocationInfo();
                            ConfigValue.ConfigurationValue = cmbSelectRecursiveCount.Text.ToString();
                            ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RfidRecursiveDays;
                            ConfigValue.SubstoreId = Config.SubStoreId;
                            ConfigValue.LocationId = locationId;
                            objConfigList.Add(ConfigValue);
                        }
                    }
                }
                bool IsSaved = configBusiness.SaveUpdateConfigLocation(objConfigList);
                if (IsSaved)
                {
                    MessageBox.Show("RFID configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    if (!String.IsNullOrWhiteSpace(dtFrom.Text) && !String.IsNullOrWhiteSpace(dtTo.Text))
                    {
                        if ((Convert.ToDateTime(dtFrom.Value) <= Convert.ToDateTime(dtTo.Value)))
                        {
                            fromDate = (DateTime)dtFrom.Value;
                            toDate = (DateTime)dtTo.Value;
                            GetFlushHistoryData(fromDate, toDate);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Select the Start/End date", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    if (rbDayWiseFlush.IsChecked == true && rbDayWiseFlush.IsChecked == true && ccFlushDateTime.Value != null)
                        SetFlushConfigData(substoreId, Convert.ToDateTime(ccFlushDateTime.Value), locationId);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rbGBWithSeperatorPreScan.IsChecked = true;
            GetConfigLocationData();
            //GetConfigData();
            if (chkIsEnabledRFID.IsChecked == true)
                HideShow(Visibility.Visible, Visibility.Visible, Visibility.Collapsed, Visibility.Visible);
            else if (chkIsEnabledMobileRfid.IsChecked == true)
                HideShow(Visibility.Visible, Visibility.Collapsed, Visibility.Visible, Visibility.Visible);
            else
                HideShow(Visibility.Collapsed, Visibility.Collapsed, Visibility.Collapsed, Visibility.Collapsed);
            GetRfidInfoLocationWise();
            //HideShow(Visibility.Collapsed,Visibility.Collapsed,Visibility.Visible );
            //if (cmbLocation.SelectedIndex > 0)
            //{
            //    GetRfidInfoLocationWise();
            //}
            //else if (cmbLocation.SelectedIndex == 0)
            //{
            //    List<RfidInfo> _listRfidInfo = new List<RfidInfo>();
            //    if (lstRfidInfo != null)
            //    {
            //        _listRfidInfo = lstRfidInfo.Where(x => (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsEnabledRFID))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushType)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDFlushInHour))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsMobileRfidEnabled))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushReceiverEmail)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveExcludeDays))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushDateTime)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid))
            //     || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveDays)))).ToList();
            //        GetFilterisedRfidValue(_listRfidInfo);
            //    }
				////added by latika for table workflow
   GetTableInfo(Convert.ToInt32(cmbLocation.SelectedValue));
   ///end
            //}
            if (string.IsNullOrEmpty(txtNoOfGroupsToAssociate.Text.Trim()))
                txtNoOfGroupsToAssociate.Text = "2";
        }

        private void rbHourWiseFlush_Checked(object sender, RoutedEventArgs e)
        {
            stFlustHrs.Visibility = Visibility.Visible;
            stFlushDayWise.Visibility = Visibility.Collapsed;
            stkGrid.Visibility = Visibility.Collapsed;
            btnFlushNow.Visibility = Visibility.Collapsed;
        }

        private void rbDayWiseFlush_Checked(object sender, RoutedEventArgs e)
        {
            stFlushDayWise.Visibility = Visibility.Visible;
            stkGrid.Visibility = Visibility.Visible;
            stFlustHrs.Visibility = Visibility.Collapsed;
            btnFlushNow.Visibility = Visibility.Visible;
            //ResetDaywiseFlush();
        }
        private void ccFlushDateTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            ResetDaywiseFlush();
        }

        private void chkIsRecursiveFlush_Checked(object sender, RoutedEventArgs e)
        {
            stkRecursiveDays.Visibility = Visibility.Visible;
        }

        private void chkIsRecursiveFlush_Unchecked(object sender, RoutedEventArgs e)
        {
            stkRecursiveDays.Visibility = Visibility.Collapsed;
        }

        private void BtnRefresh_click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(dtFrom.Text) && !String.IsNullOrWhiteSpace(dtTo.Text))
            {
                if ((Convert.ToDateTime(dtFrom.Value) <= Convert.ToDateTime(dtTo.Value)))
                {
                    fromDate = (DateTime)dtFrom.Value;
                    toDate = (DateTime)dtTo.Value;
                    GetFlushHistoryData(fromDate, toDate);
                }
                else
                {
                    MessageBox.Show("'From Date' should always be grater then or equal to 'To Date'", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Select From date and To date.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void GetFlushHistoryData(DateTime fromDate, DateTime toDate)
        {
            dtFrom.Value = fromDate;
            dtTo.Value = toDate;
            RfidBusiness rfidBuss = new RfidBusiness();
            List<RfidFlushHistotyInfo> _objFlushHistorydetail = rfidBuss.GetFlushHistoryData(Config.SubStoreId, fromDate, toDate);
            if (_objFlushHistorydetail != null)
            {
                foreach (var item in _objFlushHistorydetail)
                {
                    item.ErrorMessage = string.IsNullOrEmpty(item.ErrorMessage) ? string.Empty : item.FlushId.ToString();
                    switch (item.Status)
                    {
                        case 1:
                            item.StrStatus = "Completed";
                            break;
                        case 2:
                            item.StrStatus = "Failed";
                            break;
                        case 3:
                            item.StrStatus = "Scheduled";
                            break;
                        case 4:
                            item.StrStatus = "Inprogress ";
                            break;
                        default:
                            break;
                    }
                }
                //_objFlushHistorydetail = _objFlushHistorydetail.OrderByDescending(t => t.ScheduleFlushdate.HasValue).OrderByDescending(t => t.ScheduleFlushdate).ToList();
            }
            DGFlushHistory.ItemsSource = _objFlushHistorydetail;

        }

        private void btnFlushNow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Convert.ToInt16(865464564564565656);
                if (cmbLocation.SelectedValue == null)
                {
                    MessageBox.Show("Select Location.");
                    return;
                }
                if (Config.SubStoreId <= 0)
                {
                    MessageBox.Show("Please configure your SubStore.");
                    return;
                }
                if (rbDayWiseFlush.IsChecked == true)
                {
                    //if (ccFlushDateTime.Value == null)
                    //{
                    //    MessageBox.Show("Select start date time for day wise flush.");
                    //    return;
                    //}
                    if (string.IsNullOrEmpty(txtExcludeDays.Text.Trim()) || regex.IsMatch(txtExcludeDays.Text.Trim()) == false)
                    {
                        MessageBox.Show("Enter exclude days.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Select day wise flush.");
                    return;
                }
                locId = (int)cmbLocation.SelectedValue;
                NoOfExcludeDays = Convert.ToInt32(txtExcludeDays.Text.Trim());
                bs.Show();
                FlushWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                SaveEmailDetails(ex.Message);
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
        }

        private bool SaveEmailDetails(string exceptionMsg)
        {
            exceptionMsg += ", Sub Store: " + (new StoreSubStoreDataBusniess()).GetSubstoreData(Config.SubStoreId).DG_SubStore_Name;
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, Config.SubStoreId);
            string ToAddress = txtFlushEmailAddress.Text.Trim();
            if (!string.IsNullOrEmpty(ToAddress))
            {
                bool success = buss.SaveEmailDetails(ToAddress, null, null, exceptionMsg, "RFID FLUSH");
            }
            return true;
        }
        private void FlushWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DoFlushNow();
        }
        private void DoFlushNow()
        {
            try
            {

                //string strMsg = string.Empty;
                RfidBusiness rfidBuss = new RfidBusiness();
                rfidBuss.RfidFlushNow(Config.SubStoreId, locId, NoOfExcludeDays);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was some error in flush! Error: " + ex.Message, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void FlushWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            MessageBox.Show("Flush Complete\n\r\n\r", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            GetConfigLocationData();
        }

        private void SetFlushConfigData(int substoreId, DateTime flushDateTime, int locationId)
        {
            try
            {
                RfidFlushHistotyInfo rfidFlush = new RfidFlushHistotyInfo();
                RfidBusiness rfidBuss = new RfidBusiness();

                if (locationId == 0)
                {
                    for (int i = 1; i < cmbLocation.Items.Count; i++)
                    {
                        locationId = ((KeyValuePair<int, string>)cmbLocation.Items[i]).Key;
                        rfidBuss = new RfidBusiness();
                        rfidFlush.SubStoreId = substoreId;
                        rfidFlush.LocationId = locationId;
                        rfidFlush.ScheduleDate = flushDateTime;
                        rfidFlush.Status = Convert.ToInt32(Digibackup.scheduled);
                        rfidBuss.SaveRfidFlushHistory(rfidFlush);
                    }
                }
                else
                {
                    rfidBuss = new RfidBusiness();
                    rfidFlush.SubStoreId = substoreId;
                    rfidFlush.LocationId = locationId;
                    rfidFlush.ScheduleDate = flushDateTime;
                    rfidFlush.Status = Convert.ToInt32(Digibackup.scheduled);
                    rfidBuss.SaveRfidFlushHistory(rfidFlush);
                }
                //Start flush service
                ServiceController myService = new ServiceController();
                myService.ServiceName = "DigiRfidService";
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Stopped")
                {
                    myService.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was some error starting the RFID service! Error: " + ex.Message, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void rbPrePostAssociation_Click(object sender, RoutedEventArgs e)
        {
            if (rbPrePostAssociation.IsChecked.HasValue)
            {
                stkPrePostscanType.Visibility = Visibility.Visible;
                stkTimeBased.Visibility = Visibility.Collapsed;
                stkGroupBased.Visibility = Visibility.Collapsed;
                stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
            }

        }
        private void rbTimeBasedAssociation_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)rbPrePostAssociation.IsChecked.HasValue)
            {
                stkTimeBased.Visibility = Visibility.Visible;
                stkPrePostscanType.Visibility = Visibility.Collapsed;
                stkGroupBased.Visibility = Visibility.Collapsed;
                stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
            }
        }

        private void rbGroupBasedAssociation_Click(object sender, RoutedEventArgs e)
        {
            stkTimeBased.Visibility = Visibility.Collapsed;
            stkPrePostscanType.Visibility = Visibility.Collapsed;
            stkGroupBased.Visibility = Visibility.Visible;
            stkGroupBasedwithSeperator.Visibility = Visibility.Collapsed;
        }

        private void btnRFIDConfigSave_Click(object sender, RoutedEventArgs e)
        {
            SaveRFID();
            
                GetRfidInfo();
                GetRfidInfoLocationWise();
           
        }

        private void chkIsEnabledMobileRfid_Checked(object sender, RoutedEventArgs e)
        {
            HideShow(Visibility.Visible, Visibility.Collapsed, Visibility.Visible, Visibility.Visible);
        }

        private void chkIsEnabledMobileRfid_Unchecked(object sender, RoutedEventArgs e)
        {
            HideShow(Visibility.Visible, Visibility.Collapsed, Visibility.Visible, Visibility.Visible);
        }
        private void btnMobileRFIDConfigSave_Click(object sender, RoutedEventArgs e)
        {
            SaveMobileRFID();
            
                GetRfidInfo();
                GetRfidInfoLocationWise();
          
        }

        private void SaveRFID()
        {
            if ((bool)rbTimeBasedAssociation.IsChecked && string.IsNullOrWhiteSpace(txtTimeFrameSec.Text))
            {
                MessageBox.Show("Please enter time frame", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if ((bool)rbGroupBasedAssociationWthSeperator.IsChecked)
            {
                if (string.IsNullOrWhiteSpace(txtNoOfGroupsToAssociate.Text))
                {
                    MessageBox.Show("Please select no of groups to associate", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else if (Convert.ToInt32(txtNoOfGroupsToAssociate.Text) == 0)
                {
                    MessageBox.Show("No of groups to associate should be greater than zero", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
            iMixConfigurationLocationInfo ConfigValue = null;
            int substoreId = Config.SubStoreId;
            int locationId = cmbLocation.SelectedValue.ToInt32();
            configBusiness = new ConfigBusiness();

            if (chkIsEnabledRFID.IsChecked == true)
            {
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledRFID;
                ConfigValue.ConfigurationValue = ((bool)chkIsEnabledRFID.IsChecked).ToString();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsMobileRfidEnabled;
                ConfigValue.ConfigurationValue = ((string)chkIsEnabledMobileRfid.IsChecked.ToString());
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);

                if ((bool)rbPrePostAssociation.IsChecked)
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDAssociationType;
                    ConfigValue.ConfigurationValue = ((int)RFIDAssociationType.PrePostBasedAssociation).ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    if ((bool)rbPreScan.IsChecked)
                        ConfigValue.ConfigurationValue = ((int)ScanType.PreScan).ToString();
                    else
                        ConfigValue.ConfigurationValue = ((int)ScanType.PostScan).ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDScanType;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }
                else if ((bool)rbGroupBasedAssociationWthSeperator.IsChecked)
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDAssociationType;
                    ConfigValue.ConfigurationValue = ((int)RFIDAssociationType.GroupBasedAssociationWithSeperator).ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.NoOfGroupsToAssociate;
                    ConfigValue.ConfigurationValue = txtNoOfGroupsToAssociate.Text.ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    if ((bool)rbGBWithSeperatorPreScan.IsChecked)
                        ConfigValue.ConfigurationValue = ((int)ScanType.PreScan).ToString();
                    else
                        ConfigValue.ConfigurationValue = ((int)ScanType.PostScan).ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDScanType;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }
                else if ((bool)rbTimeBasedAssociation.IsChecked)
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDAssociationType;
                    ConfigValue.ConfigurationValue = ((int)RFIDAssociationType.TimeBasedAssociation).ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDAssociationTimeFrame;
                    ConfigValue.ConfigurationValue = txtTimeFrameSec.Text.ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }
                else
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDAssociationType;
                    ConfigValue.ConfigurationValue = ((int)RFIDAssociationType.GroupBasedAssociation).ToString();
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    if ((bool)rbPreScanGB.IsChecked)
                        ConfigValue.ConfigurationValue = ((int)ScanType.PreScan).ToString();
                    else
                        ConfigValue.ConfigurationValue = ((int)ScanType.PostScan).ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RFIDScanType;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }
            }



            bool IsSaved = configBusiness.SaveUpdateConfigLocation(objConfigList);
            if (IsSaved)
            {
                MessageBox.Show("RFID configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                btnRFIDConfigDelete.Visibility = Visibility.Visible;
            }
            else
                MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void DeleteRFID()
        {
            List<int> ValuesToBeDeleted = new List<int>();
            int substoreId = Config.SubStoreId;
            int locationId = cmbLocation.SelectedValue.ToInt32();
            configBusiness = new ConfigBusiness();
                ValuesToBeDeleted.Add((int)ConfigParams.IsEnabledRFID);
                ValuesToBeDeleted.Add((int)ConfigParams.RFIDScanType);
                ValuesToBeDeleted.Add((int)ConfigParams.RFIDFlushInHour);
                ValuesToBeDeleted.Add((int)ConfigParams.RfidFlushDateTime);
                ValuesToBeDeleted.Add((int)ConfigParams.IsRecursiveRfid);
                ValuesToBeDeleted.Add((int)ConfigParams.RfidRecursiveDays);
                ValuesToBeDeleted.Add((int)ConfigParams.RfidFlushReceiverEmail);
                ValuesToBeDeleted.Add((int)ConfigParams.RfidRecursiveExcludeDays);
                ValuesToBeDeleted.Add((int)ConfigParams.RfidFlushType);
                ValuesToBeDeleted.Add((int)ConfigParams.RFIDAssociationType);
                ValuesToBeDeleted.Add((int)ConfigParams.RFIDAssociationTimeFrame);
                ValuesToBeDeleted.Add((int)ConfigParams.IsMobileRfidEnabled);
                ValuesToBeDeleted.Add((int)ConfigParams.NoOfGroupsToAssociate);

            bool IsDeleted = configBusiness.DeleteConfigLocation(ValuesToBeDeleted, substoreId, locationId);
            if (IsDeleted)
            {
                MessageBox.Show("RFID configuration deleted successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                btnRFIDConfigDelete.Visibility = Visibility.Collapsed;
                if (!String.IsNullOrWhiteSpace(dtFrom.Text) && !String.IsNullOrWhiteSpace(dtTo.Text))
                {
                    if ((Convert.ToDateTime(dtFrom.Value) <= Convert.ToDateTime(dtTo.Value)))
                    {
                        fromDate = (DateTime)dtFrom.Value;
                        toDate = (DateTime)dtTo.Value;
                        GetFlushHistoryData(fromDate, toDate);
                    }
                }
                else
                {
                    MessageBox.Show("Please Select the Start/End date", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
                MessageBox.Show("There was some error deleting the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void SaveMobileRFID()
        {
            List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
            iMixConfigurationLocationInfo ConfigValue = null;
            int substoreId = Config.SubStoreId;
            int locationId = cmbLocation.SelectedValue.ToInt32();
            configBusiness = new ConfigBusiness();

            {
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsMobileRfidEnabled;
                ConfigValue.ConfigurationValue = ((string)chkIsEnabledMobileRfid.IsChecked.ToString());
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
            }
            {
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledRFID;
                ConfigValue.ConfigurationValue = ((string)chkIsEnabledRFID.IsChecked.ToString());
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
            }



            bool IsSaved = configBusiness.SaveUpdateConfigLocation(objConfigList);
            if (IsSaved)
                MessageBox.Show("Mobile RFID configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void GetRfidInfo()
        {
            List<RfidInfo> _lstRfidInfo = new List<RfidInfo>();
            lstRfidInfo = new List<RfidInfo>();
            StoreSubStoreDataBusniess storeBiz = new StoreSubStoreDataBusniess();
            lstRfidInfo = storeBiz.GetRfidInfoBussines(Config.SubStoreId);
            foreach (var _rfidInfo in lstRfidInfo)
            {
                if (_rfidInfo.ImixConfigMasterID == Convert.ToInt64(ConfigParams.RfidFlushType))
                {
                    if (_rfidInfo.ConfigurationValue.Equals("1"))
                    {
                        _rfidInfo.ConfigurationValue = "hour-Wise";
                    }
                    else
                    {
                        _rfidInfo.ConfigurationValue = "Day-Wise";
                    }
                }
            }
            _lstRfidInfo = lstRfidInfo.Where(x => (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsEnabledRFID)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushType)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDFlushInHour))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsMobileRfidEnabled))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushReceiverEmail)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveExcludeDays))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushDateTime)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveDays)))).ToList();
            GetFilterisedRfidValue(_lstRfidInfo);
        }

        private void GetRfidInfoLocationWise()
        {
            List<RfidInfo> _lstRfidInfo = new List<RfidInfo>();
            int _locationId = Convert.ToInt32(cmbLocation.SelectedValue);
            _lstRfidInfo = lstRfidInfo.Where(x => x.LocationId == _locationId).ToList();
            _lstRfidInfo = _lstRfidInfo.Where(x => (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsEnabledRFID))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDFlushInHour)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushType))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsMobileRfidEnabled))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushReceiverEmail)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveExcludeDays))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushDateTime)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid))
                || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveDays)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate)))).ToList();
            GetFilterisedRfidValue(_lstRfidInfo);
        }
        private void GetFilterisedRfidValue(List<RfidInfo> _lstRfidInfo)
        {
            var listLocationIdListMobileRfid = _lstRfidInfo.Where(x => (x.ConfigurationValue.Equals("Active") &&
                              x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsMobileRfidEnabled)))).ToList();
            foreach (var item in listLocationIdListMobileRfid)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && ((x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType)))
                    || (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame))
                    || (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsEnabledRFID)))));
            }
            var listLocationIdListIsEnableRfid = _lstRfidInfo.Where(x => (x.ConfigurationValue.Equals("Active") &&
                x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsEnabledRFID)))).ToList();
            foreach (var item in listLocationIdListIsEnableRfid)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsMobileRfidEnabled))));
            }
            var RfidScanTypeLocationIDList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("PrePost Association") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType))).ToList();
            foreach (var item in RfidScanTypeLocationIDList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate))));
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame))));
            }
            var timeWiseLocationIDList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("TimeBased Association") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType))).ToList();
            foreach (var item in timeWiseLocationIDList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate))));
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))));
            }
            var groupwiseLocationIDList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("GroupBased Association") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType))).ToList();
            foreach (var item in groupwiseLocationIDList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate))));
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame))));
            }
            var groupwiseWithSeperatorLocationIDList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("GroupBased Association with Seperator") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationType))).ToList();
            foreach (var item in groupwiseWithSeperatorLocationIDList)
            {
                //_lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.NoOfGroupsToAssociate))));
                //_lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDScanType))));
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDAssociationTimeFrame))));
            }
            var HourWiseLoCationIdList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("hour-Wise") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushType))).ToList();
            foreach (var item in HourWiseLoCationIdList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveExcludeDays))
                    || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushDateTime)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushReceiverEmail))
                    || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid)) || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveDays))));
            }
            var dayWiseLoCationIdList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("Day-Wise") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidFlushType))).ToList();
            foreach (var item in dayWiseLoCationIdList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RFIDFlushInHour))));
            }
            var IsRecursiveRfidLoCationIdList = _lstRfidInfo.Where(x => x.ConfigurationValue.Equals("InActive") && x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid))).ToList();
            foreach (var item in IsRecursiveRfidLoCationIdList)
            {
                _lstRfidInfo.RemoveAll(x => x.LocationId == item.LocationId && (x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.IsRecursiveRfid))
                    || x.ImixConfigMasterID.Equals(Convert.ToInt64(ConfigParams.RfidRecursiveDays))));
            }

            DGRfidInfo.ItemsSource = null;
            DGRfidInfo.ItemsSource = _lstRfidInfo.OrderBy(x => x.LocationId);
        }
        private void btnRFIDConfigDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeleteRFID();
                GetRfidInfo();
                GetRfidInfoLocationWise();
            }
            catch(Exception en)
            {
                LogConfigurator.log.Error("Error Message: " + en.Message + " Error Stack Trace: " + en.StackTrace);
            }
        }

        private void rbGroupBasedAssociationWthSeperator_Click(object sender, RoutedEventArgs e)
        {
            stkTimeBased.Visibility = Visibility.Collapsed;
            stkPrePostscanType.Visibility = Visibility.Collapsed;
            stkGroupBased.Visibility = Visibility.Collapsed;
            stkGroupBasedwithSeperator.Visibility = Visibility.Visible;
        }

        string str;
        private void txtNoofGroupsToAssociate_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                str = "";
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    str = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = str;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }

 /// <summary>
        /// Table WorkFlow Start by latika 10-Sept-2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void cmbPosition1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition1.Text))
            {
                ResetMargin(1);
                EnableRequiredMargins(1, cmbPosition1.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition2.Text))
            {
                ResetMargin(2);
                EnableRequiredMargins(2, cmbPosition2.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition3.Text))
            {
                ResetMargin(3);
                EnableRequiredMargins(3, cmbPosition3.SelectedValue.ToString().Substring(38));
            }
        }
        private void cmbPosition4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbPosition4.Text))
            {
                ResetMargin(4);
                EnableRequiredMargins(4, cmbPosition4.SelectedValue.ToString().Substring(38));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        /// <param name="position"></param>
        private void EnableRequiredMargins(int player, string position)
        {
            if (player == 1)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin1.IsEnabled = true;
                        txtLeftMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.Text = "20";
                        txtRightMargin1.IsEnabled = false;
                        txtBottomMargin1.IsEnabled = false;
                        break;
                    case "Top-Center":
                        txtTopMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.IsEnabled = false;
                        txtRightMargin1.IsEnabled = false;
                        txtBottomMargin1.IsEnabled = false;
                        break;
                    case "Top-Right":
                        txtTopMargin1.IsEnabled = true;
                        txtTopMargin1.Text = "20";
                        txtLeftMargin1.IsEnabled = false;
                        txtRightMargin1.IsEnabled = true;
                        txtRightMargin1.Text = "20";
                        txtBottomMargin1.IsEnabled = false;
                        break;
                    case "Bottom-Left":
                        txtTopMargin1.IsEnabled = false;
                        txtLeftMargin1.IsEnabled = true;
                        txtLeftMargin1.Text = "20";
                        txtRightMargin1.IsEnabled = false;
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin1.IsEnabled = false;
                        txtLeftMargin1.IsEnabled = false;
                        txtRightMargin1.IsEnabled = false;
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin1.IsEnabled = false;
                        txtLeftMargin1.IsEnabled = false;
                        txtRightMargin1.IsEnabled = true;
                        txtRightMargin1.Text = "20";
                        txtBottomMargin1.IsEnabled = true;
                        txtBottomMargin1.Text = "20";
                        break;
                }
            }
            else if (player == 2)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin2.IsEnabled = true;
                        txtLeftMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.Text = "20";
                        txtRightMargin2.IsEnabled = false;
                        txtBottomMargin2.IsEnabled = false;
                        break;
                    case "Top-Center":
                        txtTopMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.IsEnabled = false;
                        txtRightMargin2.IsEnabled = false;
                        txtBottomMargin2.IsEnabled = false;
                        break;
                    case "Top-Right":
                        txtTopMargin2.IsEnabled = true;
                        txtTopMargin2.Text = "20";
                        txtLeftMargin2.IsEnabled = false;
                        txtRightMargin2.IsEnabled = true;
                        txtRightMargin2.Text = "20";
                        txtBottomMargin2.IsEnabled = false;
                        break;
                    case "Bottom-Left":
                        txtTopMargin2.IsEnabled = false;
                        txtLeftMargin2.IsEnabled = true;
                        txtLeftMargin2.Text = "20";
                        txtRightMargin2.IsEnabled = false;
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin2.IsEnabled = false;
                        txtLeftMargin2.IsEnabled = false;
                        txtRightMargin2.IsEnabled = false;
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin2.IsEnabled = false;
                        txtLeftMargin2.IsEnabled = false;
                        txtRightMargin2.IsEnabled = true;
                        txtRightMargin2.Text = "20";
                        txtBottomMargin2.IsEnabled = true;
                        txtBottomMargin2.Text = "20";
                        break;
                }
            }
            else if (player == 3)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin3.IsEnabled = true;
                        txtLeftMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.Text = "20";
                        txtRightMargin3.IsEnabled = false;
                        txtBottomMargin3.IsEnabled = false;
                        break;
                    case "Top-Center":
                        txtTopMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.IsEnabled = false;
                        txtRightMargin3.IsEnabled = false;
                        txtBottomMargin3.IsEnabled = false;
                        break;
                    case "Top-Right":
                        txtTopMargin3.IsEnabled = true;
                        txtTopMargin3.Text = "20";
                        txtLeftMargin3.IsEnabled = false;
                        txtRightMargin3.IsEnabled = true;
                        txtRightMargin3.Text = "20";
                        txtBottomMargin3.IsEnabled = false;
                        break;
                    case "Bottom-Left":
                        txtTopMargin3.IsEnabled = false;
                        txtLeftMargin3.IsEnabled = true;
                        txtLeftMargin3.Text = "20";
                        txtRightMargin3.IsEnabled = false;
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin3.IsEnabled = false;
                        txtLeftMargin3.IsEnabled = false;
                        txtRightMargin3.IsEnabled = false;
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin3.IsEnabled = false;
                        txtLeftMargin3.IsEnabled = false;
                        txtRightMargin3.IsEnabled = true;
                        txtRightMargin3.Text = "20";
                        txtBottomMargin3.IsEnabled = true;
                        txtBottomMargin3.Text = "20";
                        break;
                }
            }
            else if (player == 4)
            {
                switch (position)
                {
                    case "Top-Left":
                        txtTopMargin4.IsEnabled = true;
                        txtLeftMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.Text = "20";
                        txtRightMargin4.IsEnabled = false;
                        txtBottomMargin4.IsEnabled = false;
                        break;
                    case "Top-Center":
                        txtTopMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.IsEnabled = false;
                        txtRightMargin4.IsEnabled = false;
                        txtBottomMargin4.IsEnabled = false;
                        break;
                    case "Top-Right":
                        txtTopMargin4.IsEnabled = true;
                        txtTopMargin4.Text = "20";
                        txtLeftMargin4.IsEnabled = false;
                        txtRightMargin4.IsEnabled = true;
                        txtRightMargin4.Text = "20";
                        txtBottomMargin4.IsEnabled = false;
                        break;
                    case "Bottom-Left":
                        txtTopMargin4.IsEnabled = false;
                        txtLeftMargin4.IsEnabled = true;
                        txtLeftMargin4.Text = "20";
                        txtRightMargin4.IsEnabled = false;
                        txtBottomMargin4.IsEnabled = true;
                        txtBottomMargin4.Text = "20";
                        break;
                    case "Bottom-Center":
                        txtTopMargin4.IsEnabled = false;
                        txtLeftMargin4.IsEnabled = false;
                        txtRightMargin4.IsEnabled = false;
                        txtBottomMargin4.IsEnabled = true;
                        txtBottomMargin4.Text = "20";
                        break;
                    case "Bottom-Right":
                        txtTopMargin4.IsEnabled = false;
                        txtLeftMargin4.IsEnabled = false;
                        txtRightMargin4.IsEnabled = true;
                        txtRightMargin4.Text = "20";
                        txtBottomMargin4.IsEnabled = true;
                        txtBottomMargin4.Text = "20";
                        break;
                }
            }


        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);
        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }
        private void ResetMargin(int player)
        {
            switch (player)
            {
                case 1:
                    txtTopMargin1.Text = "0";
                    txtLeftMargin1.Text = "0";
                    txtRightMargin1.Text = "0";
                    txtBottomMargin1.Text = "0";
                    break;
                case 2:
                    txtTopMargin2.Text = "0";
                    txtLeftMargin2.Text = "0";
                    txtRightMargin2.Text = "0";
                    txtBottomMargin2.Text = "0";
                    break;
                case 3:
                    txtTopMargin3.Text = "0";
                    txtLeftMargin3.Text = "0";
                    txtRightMargin3.Text = "0";
                    txtBottomMargin3.Text = "0";
                    break;
                case 4:
                    txtTopMargin4.Text = "0";
                    txtLeftMargin4.Text = "0";
                    txtRightMargin4.Text = "0";
                    txtBottomMargin4.Text = "0";
                    break;
               
            }
        }
        private void ResetPositions()
        {
            cmbPosition1.SelectedIndex = 3;
            cmbPosition2.SelectedIndex = 4;
            cmbPosition3.SelectedIndex = 5;
            cmbPosition4.SelectedIndex = 0;
        
        }
        private bool IsNumber(string text)
        {
            int number;
            //Allowing only numbers
            if (!(int.TryParse(text, out number)))
            {
                return false;
            }
            return true;
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                iMIXRFIDTableWorkflowInfo objRfidFlow = new iMIXRFIDTableWorkflowInfo();
                string errorMessage = null;
                objRfidFlow.SubStoreID = Config.SubStoreId;
                objRfidFlow.LocationID = cmbLocation.SelectedValue.ToInt32();
               
                    if (chkIsTableWorlFlowActive.IsChecked == true)
                {
                    if (!IsNumber(txtFontSize.Text))
                    {
                        MessageBox.Show("Please enter only numeric value in font size!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    
                    objRfidFlow.FontSize = Convert.ToInt32(txtFontSize.Text);
                    objRfidFlow.FontStyle = Convert.ToString(cmbFontStyle.Text);
                    objRfidFlow.FontFamily = Convert.ToString(cmbFontFamily.Text);
                    objRfidFlow.FontWeight = Convert.ToString(cmbFontWeight.Text);
                    objRfidFlow.Settings = setSetting();
                   
                    objRfidFlow.IsActive = Convert.ToBoolean(chkIsTableWorlFlowActive.IsChecked);


                     errorMessage = Validate();
                }
                if (errorMessage == null)
                {
                    configBusiness = new ConfigBusiness();
                    int IsSaved = configBusiness.SaveUpdateRFIDTableWorkflow(objRfidFlow);
                    if (IsSaved>0)
                    {
                        MessageBox.Show("RFID configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        btnRFIDConfigDelete.Visibility = Visibility.Visible;
                        GetTableInfo(objRfidFlow.LocationID);
                    }
                    else
                        MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);

                    //SetValueToProperties();
                    //SetPropertiesToLocationList();
                    //GetRideInfo(Config.SubStoreId);
                }
                else
                    MessageBox.Show(errorMessage, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            ResetControls();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string Validate()
        {
            //if (cmbLocation.Text <= 0)
            //    return "Please select location";
            //if (string.IsNullOrEmpty(txtFontSize.Text))
            //    return "Please enter font size(ranges between 7-10)";
            //else if (int.Parse(txtFontSize.Text) > 10 || int.Parse(txtFontSize.Text) < 7)
            //    return "Please enter font size between 7 to 10";
           // else
            if (chketable.IsChecked == true) {
                if (String.IsNullOrEmpty(cmbOrientation1.Text))
                {
                    return "Please select the Table Orientation.";
                }
                if (CalMargin(1)<10)
                {
                    return "Please select table marging more then 10.";
                }

            }
            else if (chkName.IsChecked == true)
            {
                if (String.IsNullOrEmpty(cmbOrientation2.Text))
                {
                    return "Please select the Table Orientation.";
                }
                if (CalMargin(2) < 10)
                {
                    return "Please select Name marging more then 10.";
                }

            }
            else if (chkEmail.IsChecked == true)
            {
                if (String.IsNullOrEmpty(cmbOrientation3.Text))
                {
                    return "Please select the Table Orientation.";
                }
                if (CalMargin(3) < 10)
                {
                    return "Please select Email marging more then 10.";
                }

            }
            else if (chkContact.IsChecked == true)
            {
                if (String.IsNullOrEmpty(cmbOrientation1.Text))
                {
                    return "Please select the Table Orientation.";
                }
                if (CalMargin(4) < 10)
                {
                    return "Please select Contact No marging more then 10.";
                }

            }

            return null;
        }
        public int CalMargin(int settingtype)
        {
            int sreturn = 0;
            if (settingtype == 1) {
                sreturn = ConvertToInt(txtLeftMargin1.Text) + ConvertToInt(txtRightMargin1.Text) + ConvertToInt(txtTopMargin1.Text) + ConvertToInt(txtBottomMargin1.Text);
            }
           else if (settingtype == 2)
            {
                sreturn = ConvertToInt(txtLeftMargin2.Text) + ConvertToInt(txtRightMargin2.Text) + ConvertToInt(txtTopMargin2.Text) + ConvertToInt(txtBottomMargin2.Text);
            }
            else if (settingtype == 3)
            {
                sreturn = ConvertToInt(txtLeftMargin3.Text) + ConvertToInt(txtRightMargin3.Text) + ConvertToInt(txtTopMargin3.Text) + ConvertToInt(txtBottomMargin3.Text);
            }
            else if (settingtype ==4)
            {
                sreturn = ConvertToInt(txtLeftMargin4.Text) + ConvertToInt(txtRightMargin4.Text) + ConvertToInt(txtTopMargin4.Text) + ConvertToInt(txtBottomMargin4.Text);
            }
            return sreturn;
        }
        private int ConvertToInt(string Texts)
        {
            int sreturn = 0;
            try { sreturn = Convert.ToInt32(Texts); }
            catch { sreturn = 0; }

            return sreturn;
        }
       
        private string GetPlayerNumerAndMargin(string ctrlName)
        {
            string retVal = string.Empty;
            string temp = ctrlName.Replace("txt", "");
            string playerNum = ctrlName.Substring(ctrlName.Length - 1);
            string tempMarginName = ctrlName.Replace("txt", "").Replace("Margin", "");
            tempMarginName = tempMarginName.Substring(0, tempMarginName.Length - 1);
            retVal = "Player " + playerNum + " " + tempMarginName + " Margin";
            return retVal;
        
        }
        private void setValues()
        {
            string retVal = string.Empty;
        }
        private string setSetting()
        {
            StringBuilder iSetting = new StringBuilder();
            string displayText = string.Empty;
            if (chketable.IsChecked == true)
            {
                iSetting.Append("Table," + cmbPosition1.Text + "," + cmb1SelectColorBG.SelectedColorText + "," + cmb1SelectColorFont.SelectedColorText);
                iSetting.Append("," + txtLeftMargin1.Text + "," + txtTopMargin1.Text + "," + txtRightMargin1.Text + "," + txtBottomMargin1.Text + "," + cmbOrientation1.Text);
            }
            if (chkName.IsChecked == true)
            {
                 displayText = "~Name,";
                if (iSetting.Length < 10) { displayText = "Name,"; }

                iSetting.Append(displayText + cmbPosition2.Text + "," + cmb2SelectColorBG.SelectedColorText + "," + cmb2SelectColorFont.SelectedColorText);
                iSetting.Append("," + txtLeftMargin2.Text + "," + txtTopMargin2.Text + "," + txtRightMargin2.Text + "," + txtBottomMargin2.Text + "," + cmbOrientation2.Text);
            }
            if (chkEmail.IsChecked == true)
            {
                displayText = "~Email,";
                if (iSetting.Length < 10) { displayText = "Email,"; }
                iSetting.Append(displayText +  cmbPosition3.Text + "," + cmb3SelectColorBG.SelectedColorText + "," + cmb3SelectColorFont.SelectedColorText);
                iSetting.Append("," + txtLeftMargin3.Text + "," + txtTopMargin3.Text + "," + txtRightMargin3.Text + "," + txtBottomMargin3.Text + "," + cmbOrientation3.Text);
            }
            if (chkEmail.IsChecked == true)
            {
                displayText = "~Contact,";
                if (iSetting.Length < 10) { displayText = "Email,"; }
                iSetting.Append(displayText + cmbPosition4.Text + "," + cmb4SelectColorBG.SelectedColorText + "," + cmb4SelectColorFont.SelectedColorText);
                iSetting.Append("," + txtLeftMargin4.Text + "," + txtTopMargin4.Text + "," + txtRightMargin4.Text + "," + txtBottomMargin4.Text + "," + cmbOrientation4.Text);
            }
            return iSetting.ToString();
        }
        private void GetAllRecords()
        {
            iMIXRFIDTableWorkflowInfo objRfidFlow = new iMIXRFIDTableWorkflowInfo();
            configBusiness = new ConfigBusiness();
            List<iMIXRFIDTableWorkflowInfo> _objTableWorkflowInfo = configBusiness.GetAllTableWorkFlow();
            if (_objTableWorkflowInfo != null)
            { DGFlushHistory.ItemsSource = _objTableWorkflowInfo; }
        }
        private void GetTableInfo(int Locationid)
        {
            ResetControls();
            if (Locationid > 0)
            {
               
                //iMIXRFIDTableWorkflowInfo objRfidFlow = new iMIXRFIDTableWorkflowInfo();
                configBusiness = new ConfigBusiness();
                List<iMIXRFIDTableWorkflowInfo> objRfidFlowlist = configBusiness.GetByIDTableWorkFlow(Locationid);
                if (objRfidFlowlist != null)
                {
                    

                List<iMIXRFIDTableWorkflowInfo> rfidTableFlow = objRfidFlowlist.Where(n => n.TypesName == "Table").ToList();
                    grdControls.ItemsSource = rfidTableFlow;
                foreach (var objRfidFlow in objRfidFlowlist)
                    {
                        
                        System.Windows.Media.Color colorF;
                        System.Windows.Media.Color colorB;
                        chkIsTableWorlFlowActive.IsChecked = objRfidFlow.IsActive;
                        if (chkIsTableWorlFlowActive.IsChecked == true) { SetAllControl(true, 0); }
                        else { SetAllControl(false, 0); }
                        txtFontSize.Text = objRfidFlow.FontSize.ToString();
                        cmbFontStyle.Text = objRfidFlow.FontStyle;
                        cmbFontFamily.Text = objRfidFlow.FontFamily;
                        cmbFontWeight.Text = objRfidFlow.FontWeight;
                        cmbFontStyle.Text = objRfidFlow.FontStyle;
                        if (objRfidFlow.TypesName == "Table")
                        {

                            chketable.IsChecked = true;
                            SetAllControl(true, 1);
                            cmbOrientation1.Text = objRfidFlow.Orientation;
                            cmbPosition1.Text = objRfidFlow.Position;
                            colorB = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.BackColor));
                            colorF = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.Font));
                            cmb1SelectColorBG.SelectedColor = colorB;
                            cmb1SelectColorFont.SelectedColor = colorF;
                            txtLeftMargin1.Text =Convert.ToString(objRfidFlow.MarginLeft);
                            txtTopMargin1.Text = Convert.ToString(objRfidFlow.MarginTop);
                            txtRightMargin1.Text = Convert.ToString(objRfidFlow.MarginRight);
                            txtBottomMargin1.Text = Convert.ToString(objRfidFlow.MarginBottom);


                        }
                       else if (objRfidFlow.TypesName == "Name")
                        {
                            SetAllControl(true, 2);
                            chkName.IsChecked = true;
                            cmbOrientation2.Text = objRfidFlow.Orientation;
                            colorB = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.BackColor));
                            colorF = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.Font));
                            cmb2SelectColorBG.SelectedColor = colorB;
                            cmb2SelectColorFont.SelectedColor = colorF;
                            cmbPosition2.Text = objRfidFlow.Position;
                            txtLeftMargin2.Text = Convert.ToString(objRfidFlow.MarginLeft);
                            txtTopMargin2.Text = Convert.ToString(objRfidFlow.MarginTop);
                            txtRightMargin2.Text = Convert.ToString(objRfidFlow.MarginRight);
                            txtBottomMargin2.Text = Convert.ToString(objRfidFlow.MarginBottom);


                        }
                        else if (objRfidFlow.TypesName == "Email")
                        {
                            SetAllControl(true, 3);
                            chkEmail.IsChecked = true;
                            cmbOrientation3.Text = objRfidFlow.Orientation;
                            cmbPosition3.Text = objRfidFlow.Position;
                            colorB = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.BackColor));
                            colorF = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.Font));
                            cmb3SelectColorBG.SelectedColor = colorB;
                            cmb3SelectColorFont.SelectedColor = colorF;
                            txtLeftMargin3.Text = Convert.ToString(objRfidFlow.MarginLeft);
                            txtTopMargin3.Text = Convert.ToString(objRfidFlow.MarginTop);
                            txtRightMargin3.Text = Convert.ToString(objRfidFlow.MarginRight);
                            txtBottomMargin3.Text = Convert.ToString(objRfidFlow.MarginBottom);


                        }
                        else if (objRfidFlow.TypesName == "Contact")
                        {
                            SetAllControl(true, 4);
                            chkContact.IsChecked = true;
                            cmbOrientation4.Text = objRfidFlow.Orientation;
                            cmbPosition4.Text = objRfidFlow.Position;
                            colorB = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.BackColor));
                            colorF = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objRfidFlow.Font));
                            cmb4SelectColorBG.SelectedColor = colorB;
                            cmb4SelectColorFont.SelectedColor = colorF;
                            txtLeftMargin4.Text = Convert.ToString(objRfidFlow.MarginLeft);
                            txtTopMargin4.Text = Convert.ToString(objRfidFlow.MarginTop);
                            txtRightMargin4.Text = Convert.ToString(objRfidFlow.MarginRight);
                            txtBottomMargin4.Text = Convert.ToString(objRfidFlow.MarginBottom);

                        }
                        
                        
                    }
                   
                }
               
            }
           
        }
        private void ResetControls()
        {
            chkContact.IsChecked=false;
            chkName.IsChecked = false;
            chkEmail.IsChecked = false;
            chketable.IsChecked = false;

            cmbFontWeight.SelectedIndex = 4;
            cmbFontFamily.SelectedValue = "Times New Roman";
            txtFontSize.Clear();
            cmb1SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb2SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb3SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
            cmb4SelectColorFont.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#000000"));
         
            cmb1SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFBA55D3"));
            cmb2SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFF0000"));
            cmb3SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00BFFF"));
            cmb4SelectColorBG.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF3CB371"));
            
            txtFontSize.Text = "8";
            cmbPosition4.SelectedItem = "Top - Center";

            ResetPositions();

            for (int i = 1; i < 7; i++)
            {
                ResetMargin(i);
            }
            EnableRequiredMargins(1, cmbPosition1.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(2, cmbPosition2.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(3, cmbPosition3.SelectedValue.ToString().Substring(38));
            EnableRequiredMargins(4, cmbPosition4.SelectedValue.ToString().Substring(38));

            chkIsTableWorlFlowActive.IsChecked = false;
            grdControls.ItemsSource = null;
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

            string locid = ((Button)sender).Tag.ToString();

            if (MessageBox.Show("Do you want to delete Table work Flow settings from this Location ?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (!string.IsNullOrWhiteSpace(locid))
                {
                    configBusiness = new ConfigBusiness();
                    configBusiness.DeleteTableFlowByID(Convert.ToInt32(locid));
                    MessageBox.Show("Deleted successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    cmbLocation.SelectedIndex = 0;
                   GetTableInfo(Convert.ToInt32(locid));
                }
            }

        }

        
        private void SetAllControl(bool iflag,int setBySetting)
        {
            

            if (setBySetting == 1 || (iflag == false && setBySetting == 0))
            {
                cmbPosition1.IsEnabled = iflag;
                cmb1SelectColorBG.IsEnabled = iflag;
                cmb1SelectColorFont.IsEnabled = iflag;
                txtLeftMargin1.IsEnabled = iflag;
                txtTopMargin1.IsEnabled = iflag;
                txtRightMargin1.IsEnabled = iflag;
                txtBottomMargin1.IsEnabled = iflag;
                cmbOrientation1.IsEnabled = iflag;
            }
           if (setBySetting == 2 || (iflag == false && setBySetting == 0))
            {
                cmbPosition2.IsEnabled = iflag;
                cmb2SelectColorBG.IsEnabled = iflag;
                cmb2SelectColorFont.IsEnabled = iflag;
                txtLeftMargin2.IsEnabled = iflag;
                txtTopMargin2.IsEnabled = iflag;
                txtRightMargin2.IsEnabled = iflag;
                txtBottomMargin2.IsEnabled = iflag;
                cmbOrientation2.IsEnabled = iflag;
            }
             if (setBySetting == 3 || (iflag == false && setBySetting == 0))
            {
                cmbPosition3.IsEnabled = iflag;
                cmb3SelectColorBG.IsEnabled = iflag;
                cmb3SelectColorFont.IsEnabled = iflag;
                txtLeftMargin3.IsEnabled = iflag;
                txtTopMargin3.IsEnabled = iflag;
                txtRightMargin3.IsEnabled = iflag;
                txtBottomMargin3.IsEnabled = iflag;
                cmbOrientation3.IsEnabled = iflag;
            }
             if (setBySetting == 4 || (iflag == false && setBySetting == 0))
            {
                cmbPosition4.IsEnabled = iflag;
                cmb4SelectColorBG.IsEnabled = iflag;
                cmb4SelectColorFont.IsEnabled = iflag;
                txtLeftMargin4.IsEnabled = iflag;
                txtTopMargin4.IsEnabled = iflag;
                txtRightMargin4.IsEnabled = iflag;
                txtBottomMargin4.IsEnabled = iflag;
                cmbOrientation4.IsEnabled = iflag;
            }

            if (setBySetting == 0)
            {
                txtFontSize.IsEnabled = iflag;
                txtFontSize.IsEnabled = iflag;
                cmbFontStyle.IsEnabled = iflag;
                cmbFontFamily.IsEnabled = iflag;
                cmbFontWeight.IsEnabled = iflag;
                cmbFontStyle.IsEnabled = iflag;
              //  btnSave.IsEnabled = iflag;
                btnReset.IsEnabled = iflag;
                chkContact.IsEnabled = iflag;
                chkName.IsEnabled = iflag;
                chkEmail.IsEnabled = iflag;
                chketable.IsEnabled = iflag;

            }
           
        }

        private void chkIsTableWorlFlowActive_Checked(object sender, RoutedEventArgs e)
        {
            SetAllControl(true, 0);
        }

        private void chkIsTableWorlFlowActive_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAllControl(false, 0);
        }

        private void chketable_Checked(object sender, RoutedEventArgs e)
        {
            SetAllControl(true, 1);
        }

        private void chketable_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAllControl(false, 1);
        }

        private void chkName_Checked(object sender, RoutedEventArgs e)
        {
            SetAllControl(true, 2);
        }

        private void chkName_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAllControl(false, 2);
        }

        private void chkEmail_Checked(object sender, RoutedEventArgs e)
        {
            SetAllControl(true, 3);
        }

        private void chkEmail_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAllControl(false, 3);
        }

        private void chkContact_Checked(object sender, RoutedEventArgs e)
        {
            SetAllControl(true, 4);
        }

        private void chkContact_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAllControl(false, 4);
        }

    }
}
