﻿using DigiConfigUtility.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for DecryptUsernamePassword.xaml
    /// </summary>
    public partial class DecryptUsernamePassword : System.Windows.Controls.UserControl
    {
        private string FilePath = string.Empty;
        string xmlData = string.Empty;
        bool IsConfigUtityChanged = false;
        int totalExeCount = 19;
        int selectedExeCount = 0;
        public DecryptUsernamePassword()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

                folderBrowserDialog.SelectedPath = Environment.CurrentDirectory;
                folderBrowserDialog.ShowNewFolderButton = false;

                DialogResult dgResult = folderBrowserDialog.ShowDialog();

                if (dgResult == System.Windows.Forms.DialogResult.OK)
                {
                    textBox1.Text = folderBrowserDialog.SelectedPath;
                    //load data from xml digiphoto

                    FilePath = folderBrowserDialog.SelectedPath;
                    LoadExeFromSelectedFolder(FilePath);
                    if (File.Exists(FilePath + "\\DigiPhoto.exe") && File.Exists(FilePath + "\\DigiPhoto.exe.config"))
                    {
                        //decrypt(FilePath + "\\DigiPhoto.exe");
                        //ShowXmlData(string.Concat(folderBrowserDialog.SelectedPath, "\\DigiPhoto.exe.config"));
                        //encrypt(FilePath + "\\DigiPhoto.exe");
                        chkSelectAll.IsEnabled = true;
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("DigiPhoto.exe file not found.please provide input for working with other exes.", "Digi");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Please select an exe folder", "Digi");
            }
        }

        private void ChkSelectAll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxDigiPhoto_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxReportExportService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxSyncService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxPhotoSyncService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckAzurebolbupload_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxEmailService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxEmailApp_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxBackupService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxCleanupService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckDigiWhatsAppShareService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckDigiEvoucherService_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckDigiFacialServiceMT_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBoxDigiUpdates_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool saved = false;

                if (string.IsNullOrEmpty(FilePath))
                {
                    System.Windows.MessageBox.Show("Select the working folder.", "Digi");
                    return;
                }

                if (checkBoxDigiPhoto.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "ApiKey,ApiSecret,UserName,UserPass";
                    SaveService(FilePath, "DigiPhoto.exe", KeyNames);
                    saved = true;
                }

                if (checkBoxSyncService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "ApiKey,ApiSecret";
                    SaveService(FilePath, "DataSyncWinService.exe", KeyNames);
                    saved = true;
                }

                if (checkBoxPhotoSyncService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "ApiKey,ApiSecret";
                    SaveService(FilePath, "DigiOnlineSyncServiceStatus.exe", KeyNames);
                    saved = true;
                }

                //if (checkBoxEmailApp.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                //{
                //    string KeyNames = "ApiKey,ApiSecret,UserName,UserPass";
                //    SaveService(FilePath, "EmailKiosk.exe", KeyNames);
                //    saved = true;
                //}

                if (checkBoxEmailService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "SmtpServerUsername,SmtpServerPassword";
                    SaveService(FilePath, "EmailService.exe", KeyNames);
                    saved = true;
                }
                if (checkBoxBackupService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "UserName,UserPass";
                    SaveService(FilePath, "DigiBackupService.exe", KeyNames);
                    saved = true;
                }

                if (checkBoxCleanupService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "UserName,UserPass";
                    SaveService(FilePath, "DigiDbCleanupService.exe", KeyNames);
                    saved = true;
                }

                if (checkBoxDigiUpdates.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "UserId,Password";
                    SaveService(FilePath, "DigiUpdates.exe", KeyNames);
                    saved = true;
                }

                if (checkBoxReportExportService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "SmtpServerUsername, SmtpServerPassword";
                    SaveService(FilePath, "DGReportExportService.exe", KeyNames);
                    saved = true;
                }

                if (checkAzurebolbupload.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "AZURE_CLIENT_ID,AZURE_CLIENT_SECRET,AZURE_KEY_VAULT_URL,AZURE_TENANT_ID,AZURE_SUBSCRIPTION_ID,Authorization";
                    SaveService(FilePath, "DigiAzureUploadService.exe", KeyNames);
                    saved = true;
                }
                if (checkDigiWhatsAppShareService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "ApiKey, ApiSecret";
                    SaveService(FilePath, "DigiWhatsAppShareService.exe", KeyNames);
                    saved = true;
                }
                //if (checkDigiEvoucherService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                //{
                //    string KeyNames = "ApiKey,ApiSecret,UserName,UserPass";
                //    SaveService(FilePath, "DigiEvoucherService.exe", KeyNames);
                //    saved = true;
                //}
                if (checkDigiFacialServiceMT.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    string KeyNames = "ClientID,SecretKey,username,password";
                    SaveService(FilePath, "DigiFacialServiceMT.exe", KeyNames);
                    saved = true;
                }
                if (saved)
                {
                    System.Windows.MessageBox.Show("Changes saved successfully", "Digi");
                    if (IsConfigUtityChanged)
                        RestartApplication();
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void checkBoxPhoto_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void LoadExeFromSelectedFolder(string fileName)
        {
            //Enable-Disable check box.
            if (File.Exists(fileName + "\\DigiPhoto.exe"))
            {
                checkBoxDigiPhoto.IsEnabled = true;
            }
            else
            {
                checkBoxDigiPhoto.IsChecked = checkBoxDigiPhoto.IsEnabled = false;

            }

            if (File.Exists(fileName + "\\DataSyncWinService.exe"))
            {
                checkBoxSyncService.IsEnabled = true;
            }
            else
            {
                checkBoxSyncService.IsChecked = checkBoxSyncService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiOnlineSyncServiceStatus.exe"))
            {
                checkBoxPhotoSyncService.IsEnabled = true;
            }
            else
            {
                checkBoxPhotoSyncService.IsChecked = checkBoxPhotoSyncService.IsEnabled = false;
            }

            if (File.Exists(fileName + "\\EmailKiosk.exe"))
            {
                checkBoxEmailApp.IsEnabled = true;
            }
            else
            {
                checkBoxEmailApp.IsChecked = checkBoxEmailApp.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\EmailService.exe"))
            {
                checkBoxEmailService.IsEnabled = true;
            }
            else
            {
                checkBoxEmailService.IsChecked = checkBoxEmailService.IsEnabled = false;
            }

            //Backup setting
            if (File.Exists(fileName + "\\DigiBackupService.exe"))
            {
                checkBoxBackupService.IsEnabled = true;
            }
            else
            {
                checkBoxBackupService.IsChecked = checkBoxBackupService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiDbCleanupService.exe"))
            {
                checkBoxCleanupService.IsEnabled = true;
            }
            else
            {
                checkBoxCleanupService.IsChecked = checkBoxCleanupService.IsEnabled = false;
            }

            if (File.Exists(fileName + "\\DigiUpdates.exe"))
            {
                checkBoxDigiUpdates.IsEnabled = true;
            }
            else
            {
                checkBoxDigiUpdates.IsChecked = checkBoxDigiUpdates.IsEnabled = false;
            }

            if (File.Exists(fileName + "\\DGReportExportService.exe"))
            {
                checkBoxReportExportService.IsEnabled = true;
            }
            else
            {
                checkBoxReportExportService.IsChecked = checkBoxReportExportService.IsEnabled = false;
            }
            // Added by Suraj  -- For IMPS Demaond
            if (File.Exists(fileName + "\\DigiAzureUploadService.exe"))
            {
                checkAzurebolbupload.IsEnabled = true;
            }
            else
            {
                checkAzurebolbupload.IsChecked = checkAzurebolbupload.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiWhatsAppShareService.exe"))
            {
                checkDigiWhatsAppShareService.IsEnabled = true;
            }
            else
            {
                checkDigiWhatsAppShareService.IsChecked = checkDigiWhatsAppShareService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiEvoucherService.exe"))
            {
                checkDigiEvoucherService.IsEnabled = true;
            }
            else
            {
                checkDigiEvoucherService.IsChecked = checkDigiEvoucherService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiFacialServiceMT.exe"))
            {
                checkDigiFacialServiceMT.IsEnabled = true;
            }
            else
            {
                checkDigiFacialServiceMT.IsChecked = checkDigiFacialServiceMT.IsEnabled = false;
            }
            // End Changes
        }

        private void SaveService(string fileName, string serviceName, string KeyNames)
        {
            string KeyFiledArray = string.Empty;
            KeyFiledArray = KeyNames.Trim();
            fileName += "//" + serviceName + ".config";
            if (!File.Exists(fileName))
                return;//message

            WriteXmlData(FilePath + "\\" + serviceName, KeyFiledArray);

        }
        private void RestartApplication()
        {
            try
            {
                string msg = "To reflect the changed Config Utility ConnectionString, Please re-start the application.";
                System.Windows.MessageBox.Show(msg);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        #region Encription logic
        private static void WriteXmlData(string fileName, string KeyArray)
        {
            try
            {
                fileName = fileName + ".config";
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("appSettings");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {
                        bool flag = false;
                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "key")
                            {
                                string EncryptValue = KeyArray;  // Pass Encrption Filed Name Array 
                                string[] EncryptKeys = EncryptValue.Split(',');
                                if (EncryptKeys.Contains(attrColection.Value))
                                {
                                    flag = true;
                                }
                                else
                                {
                                    flag = false;
                                 }
                            }
                            if (attrColection.Name == "value")
                            {
                                string stringData = attrColection.Value;
                                if (flag)
                                {
                                    string encypt = Encrypt(stringData);
                                    StringBuilder strColNew = new StringBuilder(stringData);
                                    strColNew.Replace(stringData, encypt);
                                    attrColection.Value = strColNew.ToString();
                                }
                            }
                        }
                    }
                }

                doc.Save(fileName);
            }
            catch (Exception ex)
            {

            }
        }

        public static string Encrypt(string Encrypt)
        {
            try
            {
                string textToEncrypt = Encrypt;
                string ToReturn = "";
                string publickey = "deiimix1";
                string secretkey = "deiimix1";
                byte[] secretkeyByte = { };
                secretkeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = System.Text.Encoding.UTF8.GetBytes(textToEncrypt);
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateEncryptor(publickeybyte, secretkeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    ToReturn = Convert.ToBase64String(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion

    }
}
