﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for WaterMarkConfig.xaml
    /// </summary>
    public partial class WaterMarkConfig : UserControl
    {
        #region Constructor
        public WaterMarkConfig()
        {
            InitializeComponent();

            FillLocationCombo();
            FillHourMinuteCombo();
            FillPackageCombo();
            GetWaterMarkConfigList();
            GetWaterMarkConfiguration(Config.SubStoreId);
        }

        private void GetWaterMarkConfigList()
        {
            WaterMarkConfigList = new List<long>();
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkEnable));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkLocation));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkProduct));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkScheduledOn));
        }

        #endregion

        #region Properties
        Dictionary<int, string> dctLocationList;
        Dictionary<int, string> lstProductList;
        List<long> WaterMarkConfigList;
        #endregion

        #region Members Function
        private void GetWaterMarkConfiguration(int SubStoreId)
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(SubStoreId);

            List<WaterMark> lstGrid = new List<WaterMark>();
            foreach (var locInfo in dctLocationList)
            {
                WaterMark lstPW = new WaterMark();

                List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == locInfo.Key && WaterMarkConfigList.Contains(n.IMIXConfigurationMasterId)).ToList();

                if (lstLocation != null || lstLocation.Count > 0)
                {
                    foreach (var loc in lstLocation)
                    {
                        lstPW.LocationName = locInfo.Value;
                        lstPW.LocationId = locInfo.Key;
                        switch (loc.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.WaterMarkLocation:
                                lstPW.LocationId = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.WaterMarkEnable:
                                lstPW.EnableWaterMark = loc.ConfigurationValue == "True" ? "Active" : "InActive";
                                break;
                            case (int)ConfigParams.WaterMarkProduct:
                                lstPW.PackageName = GetPackageName(Convert.ToInt32(loc.ConfigurationValue));
                                break;
                            case (int)ConfigParams.WaterMarkScheduledOn:
                                lstPW.ScheduleOn = loc.ConfigurationValue;
                                break;

                        }
                    }
                    if (lstLocation.Count > 0)
                        lstGrid.Add(lstPW);
                }
            }
            grdWaterMarkDetails.ItemsSource = lstGrid;
        }
        private string GetPackageName(int packageid)
        {
            var packageName = lstProductList.Where(x => x.Key == packageid).FirstOrDefault();
            return packageName.Value;
        }
        private void FillLocationCombo()
        {
            try
            {
                StoreSubStoreDataBusniess substoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = substoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                dctLocationList = new Dictionary<int, string>();


                List<Actions> lstLocation = new List<Actions>();
                foreach (var item in locSubstoreBased)
                {
                    Actions actions = new Actions();

                    actions.IsSelected = false;
                    actions.Name = item.DG_Location_Name;
                    actions.LocationId = item.DG_Location_pkey;

                    lstLocation.Add(actions);

                    dctLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name);

                }
                listLocations.ItemsSource = lstLocation;

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void FillPackageCombo()
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<ProductTypeInfo> objProductInfo = configBusiness.GetWaterMarkProduct();
            lstProductList = new Dictionary<int, string>();
            lstProductList.Add(0, "-Select Product-");
            foreach (var item in objProductInfo)
            {
                lstProductList.Add(item.DG_Orders_ProductType_pkey, item.DG_Orders_ProductType_Name);
            }
            cmbPackages.ItemsSource = lstProductList;
            cmbPackages.SelectedIndex = 0;
        }
        private void FillHourMinuteCombo()
        {
            List<int> Hours = new List<int>();
            List<int> Minutes = new List<int>();

            for (int i = 0; i <= 23; i++)
            { Hours.Add(i); }

            for (int j = 0; j <= 60; j++)
            { Minutes.Add(j); }

            cmbHours.ItemsSource = Hours;
            cmbMinutes.ItemsSource = Minutes;
        }
        private string Validate()
        {
            if (listLocations.SelectedItems.Count <= 0)
                return "Please select location.";

            if (cmbHours.SelectedValue == null)
                return "Please select hour.";

            if (cmbMinutes.SelectedValue == null)
                return "Please select minute.";

            if (cmbPackages.SelectedIndex <= 0)
                return "Please select package.";

            return "";
        }
        private bool SaveUpdateWaterMarkConfig()
        {
            bool isSaved = false;
            List<iMixConfigurationLocationInfo> locationConfigurationInfo = new List<iMixConfigurationLocationInfo>();
            foreach (Actions item in listLocations.SelectedItems)
            {
                int locatioId = item.LocationId;
                iMixConfigurationLocationInfo obj = new iMixConfigurationLocationInfo();
                obj = new iMixConfigurationLocationInfo
                {
                    IMIXConfigurationMasterId = (int)ConfigParams.WaterMarkEnable,
                    SubstoreId = Config.SubStoreId,
                    ConfigurationValue = chkEnableWaterMark.IsChecked.Value.ToString(),
                    LocationId = locatioId,
                };
                locationConfigurationInfo.Add(obj);

                obj = new iMixConfigurationLocationInfo
                {
                    IMIXConfigurationMasterId = (int)ConfigParams.WaterMarkProduct,
                    SubstoreId = Config.SubStoreId,
                    LocationId = locatioId,
                    ConfigurationValue = cmbPackages.SelectedValue.ToString(),
                };
                locationConfigurationInfo.Add(obj);

                obj = new iMixConfigurationLocationInfo
                {
                    IMIXConfigurationMasterId = (int)ConfigParams.WaterMarkLocation,
                    SubstoreId = Config.SubStoreId,
                    LocationId = locatioId,
                    ConfigurationValue = locatioId.ToString(),
                };
                locationConfigurationInfo.Add(obj);

                obj = new iMixConfigurationLocationInfo
                {
                    IMIXConfigurationMasterId = (int)ConfigParams.WaterMarkScheduledOn,
                    SubstoreId = Config.SubStoreId,
                    ConfigurationValue = cmbHours.SelectedValue.ToString() + ":" + cmbMinutes.SelectedValue.ToString(),
                    LocationId = locatioId,
                };
                locationConfigurationInfo.Add(obj);
            }

            ConfigBusiness configBusiness = new ConfigBusiness();
            try
            {
                isSaved = configBusiness.SaveUpdateConfigLocation(locationConfigurationInfo);
                if (isSaved)
                {
                    GetWaterMarkConfiguration(Config.SubStoreId);
                    RestartSevice();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

            return isSaved;
        }

        private void RestartSevice()
        {
            ServiceController serviceController = new ServiceController();
            serviceController.ServiceName = "DigiWatermarkService";
            if (serviceController.Status == ServiceControllerStatus.Running)
            {
                serviceController.Stop();
                Thread.Sleep(TimeSpan.FromSeconds(1));
                serviceController.Start();
            }
            else if(serviceController.Status == ServiceControllerStatus.Stopped)
            {
                serviceController.Start();
            }
           
        }

        #endregion

        #region Events
        private void btnSaveWaterMarkConfig_Click(object sender, RoutedEventArgs e)
        {
            string validateMessage = Validate();
            if (String.IsNullOrEmpty(validateMessage))
            {
                if (SaveUpdateWaterMarkConfig())
                    MessageBox.Show("Configuration saved successfully.", "Digiphoto");
                else
                    MessageBox.Show("Something happens wrong.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
                MessageBox.Show(validateMessage, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);

        }
        private void btnResetWaterMarkConfig_Click(object sender, RoutedEventArgs e)
        {
            listLocations.UnselectAll();
            cmbPackages.SelectedIndex = 0;
            cmbHours.SelectedValue = null;
            cmbMinutes.SelectedValue = null;
            chkEnableWaterMark.IsChecked = false;
        }
        private void btnGetWaterMarkSetting_Click(object sender, RoutedEventArgs e)
        {
            int locationId = Convert.ToInt32(((Button)sender).Tag);
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(Config.SubStoreId);
            lstLocationWiseConfigParams = lstLocationWiseConfigParams.Where(x => x.LocationId == locationId && WaterMarkConfigList.Contains(x.IMIXConfigurationMasterId)).ToList();

            foreach (var item in lstLocationWiseConfigParams)
            {
                switch (item.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.WaterMarkLocation:
                        listLocations.SelectedValue = Convert.ToInt32(item.ConfigurationValue);
                        break;
                  
                    case (int)ConfigParams.WaterMarkEnable:
                        if (item.ConfigurationValue == "True")
                            chkEnableWaterMark.IsChecked = true;
                        else
                            chkEnableWaterMark.IsChecked = false;
                        break;
                  
                    case (int)ConfigParams.WaterMarkProduct:
                        cmbPackages.SelectedValue = Convert.ToInt32(item.ConfigurationValue);
                        break;
                
                    case (int)ConfigParams.WaterMarkScheduledOn:
                        string time = item.ConfigurationValue;
                        cmbHours.SelectedValue = Convert.ToInt32(time.Split(':')[0]);
                        cmbMinutes.SelectedValue = Convert.ToInt32(time.Split(':')[1]);
                        break;

                }
            }

        }

        #endregion
    }

    public class Actions : INotifyPropertyChanged
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
                }
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private int _locationId;
        public int LocationId
        {
            get
            {
                return _locationId;
            }
            set
            {
                _locationId = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("LocationId"));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class WaterMark
    {
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public string PackageName { get; set; }
        public string EnableWaterMark { get; set; }
        public string ScheduleOn { get; set; }


    }
}
