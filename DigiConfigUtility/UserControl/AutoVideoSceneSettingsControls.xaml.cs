﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MControls;
//using MPLATFORMLib;
using FrameworkHelper;
using DigiPhoto.Common;
using System.IO;
using System.Runtime.InteropServices;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Threading;
using DigiConfigUtility.Interop;
using FrameworkHelper.Common;
using DigiPhoto;
using System.Xml;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;
using DigiConfigUtility.Utility;
using MPLATFORMLib;
using MCOLORSLib;
//using MCOLORSLib;
//using MCOLORSLib;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for MLLiveCapture.xaml
    /// </summary>
    public partial class AutoVideoSceneSettingsControls : UserControl
    {
        #region Declaration
        public UIElement _parent;
        private bool _result = false;
        IMConfig m_pConfigRoot;
        private IMPersist m_pMPersist;
        // Called if user change playlist selection
        //public event EventHandler OnLoad;
        IMStreams m_pMixerStreams;
        //MPreviewControl mPreviewControl = new MPreviewControl();
        MFormatControl mFormatControl1 = new MFormatControl();
        MMixerClass m_objMixer;

        string outputFormat = "mp4";
        BusyWindow bs = new BusyWindow();
        string processVideoTemp = Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        List<VideoScene> lstVideoScene;
        VideoSceneBusiness business;
        ConfigBusiness objConfigBL;
        CGConfigSettings objCGConfig;
        Dictionary<int, string> dicConfig;
        public MLCHARGENLib.CoMLCharGen m_objCharGen;// = new MLCHARGENLib.CoMLCharGen();
        CGEditor_WinForms.MainWindow cgEditor = new CGEditor_WinForms.MainWindow();
        string m_strItemID = "";
        private CoMColorsClass m_objColors;
        System.ComponentModel.BackgroundWorker bw_CopySettings = new System.ComponentModel.BackgroundWorker();
        System.ComponentModel.BackgroundWorker bw_DeleteSettings = new System.ComponentModel.BackgroundWorker();
        List<ConfigurationInfo> lstConfigurationInfo = new List<ConfigurationInfo>();
        string deleteFileName = String.Empty;
        List<TemplateListItems> lstBorders;
        List<TemplateListItems> objTemplateList;
        public int CgConfigId = 0;
        public string SceneName;
        public int SceneID;
        public bool IsVerticalVideo = false;
        #endregion

        public AutoVideoSceneSettingsControls()
        {
            try
            {
                InitializeComponent();
                this.Loaded += new RoutedEventHandler(MainWindow_Loaded);

                bw_CopySettings.DoWork += bw_CopySettings_DoWork;
                bw_DeleteSettings.DoWork += bw_DeleteSettings_DoWork;
                bw_DeleteSettings.RunWorkerCompleted += bw_DeleteSettings_RunWorkerCompleted;

                if (!Directory.Exists(processVideoTemp))
                {
                    Directory.CreateDirectory(processVideoTemp);
                }
                try
                {
                    m_objMixer = new MMixerClass();
                    m_objColors = new CoMColorsClass();
                }
                catch (Exception exception)
                {
                    return;
                }
                AudioSettingsControlsBox.SetParent(mainGrd);
                AudioSettingsControlsBox.ExecuteMethod += AudioSettingsControlsBox_ExecuteMethod;
                mMixerList1.SetControlledObject(m_objMixer);
                mElementsTree1.SetControlledObject(m_objMixer);
                mMixerControl.SetControlledObject(m_objMixer);
                mFormatControl1.SetControlledObject(m_objMixer);
                mPersistControl1.SetControlledObject(m_objMixer);

                mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
                mPersistControl1.OnLoad += new EventHandler(mPersistControl1_OnLoad);

                //// Fill Senders

                mAttributesList1.ElementDescriptors = MHelpers.MComposerElementDescriptors;

                mElementsTree1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
                m_objMixer.ObjectStart(null);
                MPersistSetControlledObject(m_objMixer);


                if (mMixerControl.m_pPreview != null)
                    mMixerControl.m_pPreview.PreviewEnable("", 0, 1);


                m_objMixer.PluginsAdd(m_objColors, 0);
                GetAllSubstoreConfigdata();
                ShowBorders();
                ShowGraphic();

                LoadImageBackground();
                FillPositionDropDown();
                BindCGConfigCombo();
                getSubstoreID();

                ShowVideoOverlay();

            }
            catch (UnauthorizedAccessException ex)
            {
                if (ex.Source.Equals("Interop.MPLATFORMLib"))
                    MainWindow.isLicExp = true;

                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private void ReloadMixerObject()
        {
            mMixerList1.SetControlledObject(m_objMixer);
            mElementsTree1.SetControlledObject(m_objMixer);
            mMixerControl.SetControlledObject(m_objMixer);
            mFormatControl1.SetControlledObject(m_objMixer);
            mPersistControl1.SetControlledObject(m_objMixer);
            m_objMixer.ObjectStart(null);
            //if (m_objColors == null)
            //{
            //    m_objColors = new CoMColorsClass();
            m_objMixer.PluginsAdd(m_objColors, 0);
            // }
            // public MLCHARGENLib.CoMLCharGen m_objCharGen;// = new MLCHARGENLib.CoMLCharGen();
            CGEditor_WinForms.MainWindow cgEditor = new CGEditor_WinForms.MainWindow();
        }
        private void AudioSettingsControlsBox_ExecuteMethod(object sender, EventArgs e)
        {
            winMPreviewComposer.Visibility = Visibility.Visible;
        }
        private void BindGuestNodeProps()
        {
            string strType, strXML;
            GuestElement.ElementGet(out strType, out strXML);
            GetElementInformation(strXML);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            EnableDisableControlsOnLoad(true);
        }
        private void EnableDisableControlsOnLoad(bool value)
        {
            if (value == false)
                stkQuickSettings.Visibility = Visibility.Collapsed;
            else
                stkQuickSettings.Visibility = Visibility.Visible;
        }
        void mPersistControl1_OnLoad(object sender, EventArgs e)
        {
            mMixerList1.UpdateList(true, 1);
            mElementsTree1.UpdateTree(false);
        }
        public Object MPersistSetControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pMPersist;
            try
            {
                m_pMPersist = (IMPersist)pObject;
            }
            catch (System.Exception) { }

            return pOld;
        }
        private void LoadChromaAndCGConfig()
        {
            try
            {
                string streamID = "stream-000";
                ConfigBusiness objConfigBL = new ConfigBusiness();
                VideoScene objVideoScene = new VideoScene();
                CGConfigSettings objCGConfig = null;
                business = new VideoSceneBusiness();
                objVideoScene = business.GetVideoSceneByCriteria(0, SceneID).FirstOrDefault();

                ///Color Setting 
                VideoColorEffects colorEffects = FrameworkHelper.Common.MLHelpers.ReadColorXml(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", objVideoScene.Name, "Color.xml"));
                if (colorEffects != null)
                    LoadValuestoColorControls(colorEffects);

                m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                m_objMixer.PluginsAdd(m_objCharGen, 0);
                LoadWriterSettings(objVideoScene.Settings);
                if (objVideoScene != null && objVideoScene.CG_ConfigID > 0)
                {
                    objCGConfig = objConfigBL.GetCGConfigSettngs(objVideoScene.CG_ConfigID).FirstOrDefault();
                    cmbLoadGraphicsProfile.SelectedValue = objCGConfig.ID;
                }
                Thread.Sleep(1000);
                if (objCGConfig != null)
                {
                    MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                    //Load editor configuraion file
                    string cgConfigPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension);
                    if (File.Exists(cgConfigPath))
                        pXMLPersist.LoadFromXML(cgConfigPath, -1);
                }
                string chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", objVideoScene.Name, objVideoScene.Name + "_Chroma.xml");
                if (File.Exists(chromaPath))
                    LoadChromaFromFile(streamID, chromaPath);
                if (!string.IsNullOrEmpty(OverlayStream) && IsOverlayApplied)
                {
                    string overlayChromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", objVideoScene.Name, objVideoScene.Name + "_OverlayChroma.xml");
                    if (File.Exists(overlayChromaPath))
                        LoadChromaFromFile(OverlayStream, overlayChromaPath);
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void LoadChromaFromFile(string streamID, string chromaPath)
        {
            MItem pItem; int myIndex;
            m_objMixer.StreamsGet(streamID, out myIndex, out pItem);
            //
            //Load ChromaPlugin For MItem
            LoadChromaPlugin(true, pItem);
            Thread.Sleep(500);
            MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pItem);
            try
            {
                ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
            }
            catch { }
            Thread.Sleep(500);
            //Apply chroma
            if (File.Exists(chromaPath))
                (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
            Thread.Sleep(500);
        }
        public bool ShowHandlerDialog()
        {
            try
            {
                cmbLoadVideoOverlay.SelectedIndex = 0;
                cmbLoadBackGround.SelectedIndex = 0;
                cmbLoadBorders.SelectedIndex = 0;
                cmbLoadGraphicsProfile.SelectedIndex = 0;
                if (m_objMixer == null)
                {
                    m_objMixer = new MMixerClass();
                    ReloadMixerObject();
                }
                Visibility = Visibility.Visible;
                

                string FilePath = string.Empty;
                string directoryPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles");
                if (File.Exists(System.IO.Path.Combine(directoryPath, SceneName, SceneName + ".xml")))
                    FilePath = System.IO.Path.Combine(directoryPath, SceneName, SceneName + ".xml");
                else
                    FilePath = System.IO.Path.Combine(directoryPath, "DefaultProfiles", "StandardScene_overlay.xml");
                if (File.Exists(FilePath))
                {
                    m_pMPersist = (IMPersist)m_objMixer;
                    m_pMPersist.PersistLoad("", FilePath, "");
                    mMixerList1.UpdateList(true, 1);
                    mElementsTree1.UpdateTree(false);
                    GetGuestElement();
                    CheckTemplateItems();
                    LoadChromaAndCGConfig();
                    //Implement vertical design changes here
                    if (IsVerticalVideo)
                    {
                        MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                        props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                        props.dblRate = 29;
                        props.nAspectX = 9;
                        props.nAspectY = 16;
                        props.nHeight = 1920;
                        props.nWidth = 1080;
                        m_objMixer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                    }
                    LoadVideoObjectList();
                    BindGuestNodeProps();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
            //_parent.IsEnabled = false;
            return _result;
        }
        public void ShowBorders()
        {
            try
            {
                TemplateListItems tmpList = new TemplateListItems();
                List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
                tmpList.isActive = true;
                tmpList.FilePath = string.Empty;
                tmpList.Item_ID = 0;
                tmpList.IsChecked = false;
                tmpList.DisplayName = "-- Select/None --";
                tmpList.MediaType = 608;
                tmpList.Name = "--Select/None--";
                objTemplateList.Add(tmpList);
                var objBorders = (new BorderBusiness()).GetBorderDetails().Where(t => t.DG_IsActive == true && t.DG_ProductTypeID == 95);
                //tmpList.DisplayName = "--Select Border--";
                //objTemplateList.Add(tmpList);
                foreach (var item in objBorders)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Frames", item.DG_Border);
                    tmpList.Item_ID = item.DG_Borders_pkey;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DG_Border;
                    tmpList.MediaType = 608;
                    tmpList.Name = item.DG_Border;
                    tmpList.Tooltip = "Borders";
                    objTemplateList.Add(tmpList);
                }
                cmbLoadBorders.ItemsSource = objTemplateList;
                lstBorders = objTemplateList;
                cmbLoadBorders.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        public void ShowGraphic()
        {
            try
            {
                TemplateListItems tmpList;
                objTemplateList = new List<TemplateListItems>();
                var objGraphics = (new GraphicsBusiness()).GetGraphicsDetails().Where(t => t.DG_Graphics_IsActive == true);
                foreach (var item in objGraphics)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = ConfigManager.DigiFolderPath + "Graphics\\" + item.DG_Graphics_Name;
                    tmpList.Item_ID = item.DG_Graphics_pkey;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DG_Graphics_Displayname;
                    tmpList.MediaType = 606;
                    tmpList.Name = item.DG_Graphics_Name;
                    tmpList.Tooltip = "Graphics";
                    objTemplateList.Add(tmpList);
                }
                //List<TemplateListItems> lst = objTemplateList.Where(o => o.MediaType == 606 && o.IsChecked == true).ToList();
                lstGraphic.ItemsSource = objTemplateList;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }


        }

        bool IsOverlayApplied = false;
        private void CheckTemplateItems()
        {
            try
            {
                //Background
                MItem myItem;
                string name = string.Empty;
                m_objMixer.StreamsBackgroundGet(out name, out myItem);
                if (!string.IsNullOrEmpty(name))
                {
                    string name1 = System.IO.Path.GetFileName(name);
                    TemplateListItems item = lstBackground.Where(o => o.MediaType == 605 && o.Name.Equals(name1)).FirstOrDefault();
                    if (item != null)
                    {
                        cmbLoadBackGround.SelectedItem = item;
                        //cmbLoadBorders.SelectedItem = item;
                        //item.IsChecked = true;
                    }
                }

                //Overlay element
                int ind; myItem = null;

                OverlayElement.AttributesStringGet("stream_id", out OverlayStream);
                if (!string.IsNullOrEmpty(OverlayStream))
                {
                    m_objMixer.StreamsGet(OverlayStream, out ind, out myItem);
                    if (myItem != null)
                    {
                        string Overlayrname = string.Empty;
                        myItem.FileNameGet(out Overlayrname);
                        Overlayrname = System.IO.Path.GetFileName(Overlayrname);
                        TemplateListItems item = lstVideoOverlay.Where(o => o.MediaType == 609 && o.Name.Contains(Overlayrname)).FirstOrDefault();
                        if (item != null)
                        {
                            int have = 0;
                            string value;
                            OverlayElement.AttributesHave("show", out have, out value);
                            if (value == "true")
                            {
                                cmbLoadVideoOverlay.SelectedItem = item;
                                OverlayElement.ElementMultipleSet("h=1 w=1 x=0 y=0 audio_gain=-100 show=true", 0.0);
                                OverlayElement.ElementReorder(500);

                                IsOverlayApplied = true;
                            }
                        }
                    }
                }
                //Border element
                BorderElement.AttributesStringGet("stream_id", out name);
                ind = 0; myItem = null;
                m_objMixer.StreamsGet(name, out ind, out myItem);
                if (myItem != null)
                {
                    string bordername = string.Empty;
                    myItem.FileNameGet(out bordername);
                    bordername = System.IO.Path.GetFileName(bordername);
                    TemplateListItems item = lstBorders.Where(o => o.MediaType == 608 && o.FilePath.Contains(bordername)).FirstOrDefault();
                    if (item != null)
                    {
                        int have = 0;
                        string value;
                        BorderElement.AttributesHave("show", out have, out value);

                        if (value == "true")
                        {
                            cmbLoadBorders.SelectedItem = item;
                            BorderElement.ElementReorder(1000);
                            //objTemplateList.Where(o => o.Item_ID == item.Item_ID).FirstOrDefault().IsChecked = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        void BindCGConfigCombo()
        {
            try
            {
                List<CGConfigSettings> objCGConfigList = new List<CGConfigSettings>();
                objConfigBL = new ConfigBusiness();
                objCGConfigList = objConfigBL.GetCGConfigSettngs(0);
                dicConfig = new Dictionary<int, string>();
                dicConfig.Add(0, "-Select Graphics Profile-");
                foreach (var item in objCGConfigList)
                {
                    dicConfig.Add(item.ID, item.DisplayName);
                }
                cmbLoadGraphicsProfile.ItemsSource = dicConfig;
                var selectedItem = dicConfig.Where(x => x.Key == CgConfigId).FirstOrDefault();
                if (CgConfigId > 0)
                    cmbLoadGraphicsProfile.SelectedItem = selectedItem;
                else
                    cmbLoadGraphicsProfile.SelectedIndex = 0;
            }
            catch (Exception ex)
            {

            }
        }
        List<TemplateListItems> lstBackground;//=new List<TemplateListItems>();
        public void LoadImageBackground()
        {
            TemplateListItems tmpList = new TemplateListItems();
            List<TemplateListItems> objTemplateList = new List<TemplateListItems>();

            try
            {
                objTemplateList.Clear();
                ConfigBusiness configBusiness = new ConfigBusiness();
                tmpList.isActive = true;
                tmpList.FilePath = string.Empty;
                tmpList.Item_ID = 0;
                tmpList.IsChecked = false;
                tmpList.DisplayName = "--  Select --";
                tmpList.MediaType = 605;
                tmpList.Name = "--Select--";
                objTemplateList.Add(tmpList);
                List<VideoBackgroundInfo> objTemplatesList = configBusiness.GetVideoBackgrounds();
                foreach (var item in objTemplatesList)
                {
                    if (item.IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.Name = item.Name;
                        if (!tmpList.Name.Contains(".jpg") && !(tmpList.Name.Contains(".png")))
                            tmpList.FilePath = @"/DigiConfigUtility;component/images/vidico.png";
                        else
                            tmpList.FilePath = ConfigManager.DigiFolderPath + "VideoBackGround" + "\\" + item.Name;
                        tmpList.Item_ID = item.VideoBackgroundId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = item.DisplayName;
                        tmpList.MediaType = 605;
                        objTemplateList.Add(tmpList);
                    }
                }
                cmbLoadBackGround.ItemsSource = objTemplateList;
                lstBackground = objTemplateList;
                cmbLoadBackGround.SelectedIndex = 0;
                //cmbLoadBackGround.SelectedItem=((DigiConfigUtility.TemplateListItems)(cmbLoadBackGround.SelectedItem)).DisplayName;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void HideHandlerDialog()
        {
            try
            {
                if (m_objMixer != null)
                {
                    RemovePlugins();
                    RemoveColorPlugins();
                    m_objMixer.FilePlayStop(0);
                    Thread.Sleep(500);

                    m_objMixer.ObjectClose();
                    m_objMixer = null;
                    OnExecuteMethod();
                    // cgEditor.Close();
                }
            }
            catch (Exception ex)
            { LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace); }
            finally
            {
                //Application.Current.Shutdown();
                Visibility = Visibility.Collapsed;
                ((VideoConfig)_parent).OuterBorder.IsEnabled = true;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //#if !DEBUG
            //            EnableTaskbar();
            //#endif
            HideHandlerDialog();
        }
        private void EnableTaskbar()
        {
            //ReleaseKeyboardHook();
            //EnableDisableTaskManager(true);
            //AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.MinimizeToWindows, "Minimized to Windows on :- ");
            //  Taskbar.Show();
        }
        public Object SetWriterControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pConfigRoot;
            try
            {
                m_pConfigRoot = (IMConfig)pObject;
            }
            catch (System.Exception) { }
            return pOld;
        }
        void mScenesCombo1_OnActiveSceneChange(object sender, EventArgs e)
        {
            mElementsTree1.UpdateTree(false);
        }
        MElement SelectedTreeElement;
        private void mElementsTree1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            try
            {
                MElement pElement = (MElement)e.Node.Tag;
                SelectedTreeElement = pElement;
                mAttributesList1.SetControlledObject(pElement);
                string strType, strXML;
                pElement.ElementGet(out strType, out strXML);
                GetElementInformation(strXML);
                bool bDefElement = false;
                foreach (string strDefElement in MHelpers.strDefaultElements)
                {
                    if (strType.Contains(strDefElement))
                    {
                        // mPreviewComposerControl.SetEditElement(pElement);
                        bDefElement = true;
                        break;
                    }
                }
                if (!bDefElement)
                {
                    //mPreviewComposerControl.SetEditElement(null);
                }

            }
            catch (System.Exception) { }
        }
        private void InitializeAllInputFiles()
        {
            try
            {
                MItem m_pFile; string streamId = string.Empty;
                int nCount = 0;
                m_objMixer.StreamsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    m_objMixer.StreamsGetByIndex(i, out streamId, out m_pFile);
                    try
                    {
                        if (m_pFile != null)
                        {
                            m_pFile.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                        }
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                //throw;
            }
        }
        private void buttonChromaProps_Click(object sender, RoutedEventArgs e)
        {
            LoadChromaScreen("stream-000");
        }

        private void LoadChromaScreen(string streamId)
        {
            try
            {
                int ind; MItem pFile;
                m_objMixer.StreamsGet(streamId, out ind, out pFile);
                if (pFile != null)
                {
                    LoadChromaPlugin(true, pFile);
                    Thread.Sleep(500);
                    MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pFile);
                    try
                    {
                        ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                    }
                    catch { }

                    Thread.Sleep(500);
                    if (objChromaKey != null)
                    {
                        buttonChromaProps.IsEnabled = false;
                        FormChromaKey formCK = new FormChromaKey(objChromaKey);
                        formCK.ShowDialog();
                        buttonChromaProps.IsEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView listView = (System.Windows.Forms.ListView)sender;
            if (listView.SelectedItems.Count > 0)
            {
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(listView.SelectedItems[0].ToString());
                //checkBoxCK.IsChecked = objChromaKey == null ? false : true;
                buttonChromaProps.IsEnabled = true; ;
            }
            else
            {
                //checkBoxCK.IsChecked = false; ;
                buttonChromaProps.IsEnabled = false;
            }


        }
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            if (source != null)
            {
                IMPlugins pPlugins = source as IMPlugins;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((isCK == false) || (onloadChroma && isCK == false))
                {
                    pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                }
                buttonChromaProps.IsEnabled = true;
            }
        }
        private void LoadCGPlugin(bool onloadCG, object source)
        {
            if (source != null)
            {
                IMPlugins pPlugins = source as IMPlugins;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((isCK == false) || (onloadCG && isCK == false))
                {
                    pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                }
                buttonChromaProps.IsEnabled = true;
            }
        }
        private void loadSceneCombo()
        {
            business = new VideoSceneBusiness();
            lstVideoScene = new List<VideoScene>();
            lstVideoScene = business.GetVideoScene(0, subStoreId);
            Dictionary<int, string> scenes = new Dictionary<int, string>();
            scenes.Clear();
            scenes.Add(0, "--Select--");
            foreach (VideoScene item in lstVideoScene.Where(o => o.IsActive == true))
            {
                scenes.Add(item.SceneId, item.Name);
            }
            //cmbScene.ItemsSource = scenes;
            //if (scenes.Count > 0)
            //    cmbScene.SelectedIndex = 1;
        }

        private void LoadVideoObjectList()
        {
            List<VideoSceneObject> VideoSceneObj = new List<VideoSceneObject>();
            VideoSceneObject _videosceneObject;
            List<string> VideoSteam = new List<string>();
            MItem mitem;
            int streamCount;
            m_objMixer.StreamsGetCount(out streamCount);
            for (int i = 0; i < streamCount; i++)
            {
                _videosceneObject = new VideoSceneObject();
                string stream;
                m_objMixer.StreamsGetByIndex(i, out stream, out mitem);
                _videosceneObject.VideoObjectId = stream;
                VideoSceneObj.Add(_videosceneObject);
            }
            foreach (var item in VideoSceneObj)
            {
                int indx = 0;
                m_objMixer.StreamsGet(item.VideoObjectId, out indx, out mitem);
                string name;
                mitem.FileNameGet(out name);
                item.FileName = System.IO.Path.GetFileName(name);
            }
        }

        int subStoreId = 0;
        public void getSubstoreID()
        {
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                        if (subID.Contains(','))
                        {
                            subStoreId = Convert.ToInt32((subID.Split(',')[0]));
                        }
                        else
                        {
                            subStoreId = Convert.ToInt32(subID);
                        }
                    }
                }
                else
                {
                    LogConfigurator.log.Error("Please select substore from Configuration Section in Imix for this machine.");
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }

        Hashtable htVideoObjects = new Hashtable();
        #region Apply Route





        //To get the level of the tree element
        private MElement GetMElement(string vidObject)
        {

            MElement elemRoute = null; MElement myElementRoute = null;
            try
            {
                string entry = string.Empty;
                m_objMixer.ElementsGetByIndex(3, out myElementRoute);
                ((IMElements)myElementRoute).ElementsGetByID(vidObject, out elemRoute);
                return elemRoute;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
                return elemRoute = null;
            }
            finally
            {
                //Marshal.ReleaseComObject(elemRoute);
                //Marshal.ReleaseComObject(myElementRoute);
            }
        }

        //private void bwRoute_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        if (bwRoute.CancellationPending)
        //        {
        //            Thread.Sleep(1200);
        //            e.Cancel = true;
        //        }
        //        else
        //        {
        //            foreach (var item in htVideoObjects.Keys)
        //            {
        //                MElement elemRoute = GetMElement(item.ToString());
        //                ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item]);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}

        #endregion
        #region writer settings

        string VideoSettings = string.Empty;
        private void LoadWriterSettings(string settings)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(settings);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OpFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText.Trim();
                        string Audiocodec = node.ChildNodes[5].InnerText.Trim();
                        mFormatControl1.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl1.comboBoxAudio.SelectedIndex = Audioid;
                    }
                }
            }
            catch
            {

            }
        }
        #endregion

       
        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GenrateColorXML();
                SaveChroma();

                SaveScene();
                business = new VideoSceneBusiness();
                if (cmbLoadGraphicsProfile.SelectedIndex > 0)
                {
                    int _graphicProfileID = Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue);
                    business.UpdateSceneGraphicProfile(SceneID, _graphicProfileID);
                }
                else
                {
                    business.UpdateSceneGraphicProfile(SceneID, 0);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private void GenrateColorXML()
        {

            try
            {
                string xmlPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", SceneName);
                if (!Directory.Exists(xmlPath))
                    Directory.CreateDirectory(xmlPath);
                XmlTextWriter xmlWriter = new XmlTextWriter(xmlPath + "\\Color.xml", System.Text.Encoding.UTF8);
                xmlWriter.WriteStartDocument(true);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.Indentation = 2;
                xmlWriter.WriteStartElement("Color");

                xmlWriter.WriteStartElement("ColorEffects");
                xmlWriter.WriteStartElement("ULevel");
                xmlWriter.WriteString(sldULevelSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("UVGain");
                xmlWriter.WriteString(sldUVGainSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("VLevel");
                xmlWriter.WriteString(sldVLevelSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("YGain");
                xmlWriter.WriteString(sldYGainSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("YLevel");
                xmlWriter.WriteString(sldYLevelSlider.Value.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("BrightnessAndContrast");
                xmlWriter.WriteStartElement("BlackLevel");
                xmlWriter.WriteString(sldBlackSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Brightness");
                xmlWriter.WriteString(sldBrightnessSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ColorGain");
                xmlWriter.WriteString(sldColorSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("Contrast");
                xmlWriter.WriteString(sldContrastSlider.Value.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("WhiteLevel");
                xmlWriter.WriteString(sldWhiteSlider.Value.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("PresetEffects");
                xmlWriter.WriteString(presetEffects);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndDocument();
                xmlWriter.Close();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void SaveChroma()
        {

            if (m_objMixer != null)
            {
                //Guest Image
                SaveChromaStreamWise("stream-000", false);
                //Video overlay
                // SaveChromaStreamWise(OverlayStream,true);
            }
        }

        private void SaveChromaStreamWise(string streamId, bool IsOverlay)
        {
            int ind; MItem pFile;
            m_pMixerStreams = (IMStreams)m_objMixer;
            m_pMixerStreams.StreamsGet(streamId, out ind, out pFile);
            if (pFile != null)
            {
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pFile);
                if (objChromaKey != null)
                {
                    //MCHROMAKEYLib.MChromaKey m_pChromaKey;
                    IMPersist m_persist;
                    MCHROMAKEYLib.MKey m_pKey;
                    //m_pChromaKey = (MCHROMAKEYLib.MChromaKey)objChromaKey;
                    //m_pChromaKey.KeyGet(null, out m_pKey, "");
                    objChromaKey.KeyGet(null, out m_pKey, "");
                    m_persist = (IMPersist)objChromaKey;
                    if (m_pKey != null && objChromaKey != null)
                    {
                        if (!string.IsNullOrEmpty(SceneName))
                        {
                            string _chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", SceneName);
                            if (!Directory.Exists(_chromaPath))
                                Directory.CreateDirectory(_chromaPath);
                            string _chromaSavePath = string.Empty;

                            if (!IsOverlay)
                                _chromaSavePath = System.IO.Path.Combine(_chromaPath, SceneName + "_Chroma.xml");
                            else
                                _chromaSavePath = System.IO.Path.Combine(_chromaPath, SceneName + "_OverlayChroma.xml");

                            m_persist.PersistSaveToFile("", _chromaSavePath, "");
                            CopyChromaToAll(lstConfigurationInfo, _chromaSavePath);
                        }
                    }
                }
            }
        }


        #region HideShow Settings Panel
        Boolean ShowHideSet = false;
        private void btnShowHideSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!ShowHideSet)
                ShowHideSet = true;
            else
                ShowHideSet = false;

            ShowHideSettings(ShowHideSet);
        }

        private void ShowHideSettings(bool flag)
        {
            if (flag)
            {
                stkSettingsRightPanel.Visibility = Visibility.Visible;
                stkSettingsBottomPanel.Visibility = Visibility.Visible;
            }
            else
            {
                stkSettingsRightPanel.Visibility = Visibility.Collapsed;
                stkSettingsBottomPanel.Visibility = Visibility.Collapsed;
            }
        }
        #endregion HideShow Settings Panel

        #region Expanders
        private void CollapseOthers(Expander exp)
        {
            if (exp != ExpSceneSettings)
            {
                ExpSceneSettings.IsExpanded = false;
            }

            if (exp != ExpGraphicsSettings)
            {
                ExpGraphicsSettings.IsExpanded = false;
            }
            if (exp != ExpBorderSettings)
            {
                ExpBorderSettings.IsExpanded = false;
            }
            if (exp != ExpBGSettings)
            {
                ExpBGSettings.IsExpanded = false;
            }
            if (exp != ExpEnableAudio)
            {
                ExpEnableAudio.IsExpanded = false;
            }
            if (exp != ExpQuickSettings)
            {
                ExpQuickSettings.IsExpanded = false;
            }
            if (exp != ExpVideoOverlay)
            {
                ExpVideoOverlay.IsExpanded = false;
            }
            if (exp != ExpColorSettings)
            {
                ExpColorSettings.IsExpanded = false;
            }
        }
        private void ExpRecordSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void ExpSceneSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpPreview_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpRecordProgress_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        #endregion Expanders

        private string GetVideoName()
        {
            const string chars = "0123456789";
            var random = new Random();
            return "live" + new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
            //byte[] buffer = Guid.NewGuid().ToByteArray();
            //return "live-" + BitConverter.ToUInt32(buffer, 6).ToString();
        }

        private void InvalidatePropertyChange(Slider sdr, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (sdr == sldXAxis)
                    ApplyPropertyChange(GuestElement, "x", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldYAxis)
                    ApplyPropertyChange(GuestElement, "y", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldHeight)
                    ApplyPropertyChange(GuestElement, "h", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldWidth)
                    ApplyPropertyChange(GuestElement, "w", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldRotation)
                    ApplyPropertyChange(GuestElement, "r", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropX)
                    ApplyPropertyChange(GuestElement, "sx", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropY)
                    ApplyPropertyChange(GuestElement, "sy", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropH)
                    ApplyPropertyChange(GuestElement, "sh", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropW)
                    ApplyPropertyChange(GuestElement, "sw", Convert.ToString(sdr.Value), timeForChange);
            }
        }
        private void InvalidatePropertyChange(TextBox textB, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (textB == txtAudioGain)
                    ApplyPropertyChange(GuestElement, "audio_gain", Convert.ToString(txtAudioGain.Text), timeForChange);
                if (textB == txtStreamId)
                    ApplyPropertyChange(GuestElement, "stream_id", Convert.ToString(txtStreamId.Text), timeForChange);
            }
        }
        private void InvalidatePropertyChange(ComboBox cmb, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (cmb == drpPosition)
                    ApplyPropertyChange(GuestElement, "pos", Convert.ToString(drpPosition.SelectedValue), timeForChange);
                if (cmb == drpCropP)
                    ApplyPropertyChange(GuestElement, "spos", Convert.ToString(drpCropP.SelectedValue), timeForChange);
            }
        }
        private void ApplyPropertyChange(MElement SelectedElem, string attribute, string value, double timeForChange)
        {
            SelectedElem.ElementMultipleSet(attribute + "=" + value, timeForChange);
        }

        bool isRightPnlVisible = false;
        public void SetVisibility(bool IsVisible)
        {
            if (IsVisible)
            {
                winMPreviewComposer.Visibility = Visibility.Visible;
                winMStreamsList.Visibility = Visibility.Visible;
                //winMConfigListl.Visibility = Visibility.Visible;
                //winMFormatControl.Visibility = Visibility.Visible;
                //winMPreviewGreenScreen.Visibility = Visibility.Visible;
                if (isRightPnlVisible == true)
                {
                    stkSettingsRightPanel.Visibility = Visibility.Visible;
                    isRightPnlVisible = false;
                }

            }
            else
            {
                winMPreviewComposer.Visibility = Visibility.Hidden;
                winMStreamsList.Visibility = Visibility.Hidden;
                //winMConfigListl.Visibility = Visibility.Hidden;
                //winMFormatControl.Visibility = Visibility.Hidden;
                //winMPreviewGreenScreen.Visibility = Visibility.Hidden;
                if (stkSettingsRightPanel.Visibility == Visibility.Visible)
                {
                    isRightPnlVisible = true;
                    stkSettingsRightPanel.Visibility = Visibility.Hidden;
                }

            }
        }
        public void EnableORDisableControls(bool IsEnabled)
        {

            winMPreviewComposer.IsEnabled = IsEnabled;
            winMStreamsList.IsEnabled = IsEnabled;
            //winMConfigListl.IsEnabled = IsEnabled;
            //winMFormatControl.IsEnabled = IsEnabled;
            //cmbScene.IsEnabled = IsEnabled;
            ExpSceneSettings.IsEnabled = IsEnabled;
            btnClose.IsEnabled = IsEnabled;
            buttonChromaProps.IsEnabled = IsEnabled;
            btnSaveFile.IsEnabled = IsEnabled;
        }
        private void GetElementInformation(string strAttributes)
        {
            try
            {
                Hashtable htAttrib = new Hashtable();
                XmlDocument sdoc = new XmlDocument();
                sdoc.LoadXml(strAttributes);
                XmlNode xnode = sdoc.FirstChild;
                for (int i = 0; i < xnode.Attributes.Count; i++)
                {
                    string name = Convert.ToString(xnode.Attributes[i].Name);
                    string value = Convert.ToString(xnode.Attributes[i].Value);
                    if (!htAttrib.ContainsKey(name))
                    {
                        htAttrib.Add(name, value);
                    }
                }
                if (htAttrib.Count > 0)
                {
                    PopulateProperties(htAttrib);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }
        private void PopulateProperties(Hashtable htAttrib)
        {
            txtXAxis.Text = htAttrib.ContainsKey("x") ? htAttrib["x"].ToString() : "0.0";
            txtYAxis.Text = htAttrib.ContainsKey("y") ? htAttrib["y"].ToString() : "0.0";
            txtHeight.Text = htAttrib.ContainsKey("h") ? htAttrib["h"].ToString() : "0.0";
            txtWidth.Text = htAttrib.ContainsKey("w") ? htAttrib["w"].ToString() : "0.0";
            txtRotation.Text = htAttrib.ContainsKey("r") ? htAttrib["r"].ToString() : "0.0";
            txtCropX.Text = htAttrib.ContainsKey("sx") ? htAttrib["sx"].ToString() : "0.0";
            txtCropY.Text = htAttrib.ContainsKey("sy") ? htAttrib["sy"].ToString() : "0.0";
            txtCropH.Text = htAttrib.ContainsKey("sh") ? htAttrib["sh"].ToString() : "1.0";
            txtCropW.Text = htAttrib.ContainsKey("sw") ? htAttrib["sw"].ToString() : "1.0";
            txtAudioGain.Text = htAttrib.ContainsKey("audio_gain") ? htAttrib["audio_gain"].ToString() : "+0.0";
            txtStreamId.Text = htAttrib.ContainsKey("stream_id") ? htAttrib["stream_id"].ToString() : "";

            drpPosition.SelectedValue = htAttrib.ContainsKey("pos") ? htAttrib["pos"].ToString() : "bottom-left";
            drpCropP.SelectedValue = htAttrib.ContainsKey("spos") ? htAttrib["spos"].ToString() : "bottom-left";

        }
        private List<string> FillPositionList()
        {
            List<string> Positions = new List<string>();
            Positions.Add("center");
            Positions.Add("right");
            Positions.Add("left");
            Positions.Add("top");
            Positions.Add("bottom");
            Positions.Add("bottom-right");
            Positions.Add("bottom-left");
            Positions.Add("top-right");
            Positions.Add("top-left");
            return Positions;
        }
        private void FillPositionDropDown()
        {
            try
            {

                List<string> Positions = FillPositionList();
                drpPosition.Items.Clear();
                drpPosition.ItemsSource = Positions;
                drpCropP.Items.Clear();
                drpCropP.ItemsSource = Positions;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }
        private void sldXAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldYAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropW_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            sldWidth.Value = sldCropW.Value;
        }

        private void sldCropH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            sldHeight.Value = sldCropH.Value;
        }

        private void txtAudioGain_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void txtStreamId_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void drpPosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }
        private void drpCropP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }

        #region Quick Settings
        private void btnFullScreen_Checked(object sender, RoutedEventArgs e)
        {
            ///This code works fine
            if (mMixerControl.m_pPreview != null)
            {
                // Enable full screen (use -1 for auto-select monitor)
                if ((Boolean)btnFullScreen.IsChecked)
                {
                    mMixerControl.m_pPreview.PreviewFullScreen("", 1, 1);
                    btnFullScreen.IsChecked = false;
                }
                else
                    mMixerControl.m_pPreview.PreviewFullScreen("", 0, 1);
            }
        }
        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Volume in dB
            // 0 - full volume, -100 silence
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            if (mMixerControl.m_pPreview != null)
                mMixerControl.m_pPreview.PreviewAudioVolumeSet("", -1, -30 * (1 - dblPos));

        }
        private void btnAspectRatio_Click(object sender, RoutedEventArgs e)
        {
            if (mMixerControl.m_pPreview != null)
                ((IMProps)mMixerControl.m_pPreview).PropsSet("maintain_ar", (Boolean)btnAspectRatio.IsChecked ? "none" : "letter-box");
        }
        private void btnAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mMixerControl.m_pPreview != null)
                    mMixerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
            }
            catch
            {

            }
        }
        private void btnVideoPreview_Click(object sender, RoutedEventArgs e)
        {
            if (mMixerControl.m_pPreview != null)
                mMixerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
        }
        #endregion Quick Settings
        private void ExpEnableAudio_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void chkEnableAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox chkBox = (CheckBox)sender;
                int pitem = 0;
                MItem mitem;
                IMElements element = (IMElements)m_objMixer;
                m_objMixer.StreamsGet(chkBox.Tag.ToString(), out pitem, out mitem);

                IMProps m_pProps = (IMProps)mitem;
                if (mitem != null)
                {
                    if (chkBox.IsChecked == true)
                    {
                        m_pProps.PropsSet("object::audio_gain", "1");
                    }
                    else
                    {
                        m_pProps.PropsSet("object::audio_gain", "-100");
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }

        private void ExpQuickSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void ClearAllTempVideos()
        {
            DirectoryInfo dir = new DirectoryInfo(processVideoTemp);
            try
            {
                foreach (var item in dir.GetFiles())
                {
                    try
                    {
                        item.Delete();
                    }
                    catch { }
                }
            }
            catch
            { }
        }

        //#region Common Methods For Locking
        ///// <summary>
        ///// Enables the disable task manager.
        ///// </summary>
        ///// <param name="enable">if set to <c>true</c> [enable].</param>
        //private static void EnableDisableTaskManager(bool enable)
        //{
        //    Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
        //    Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
        //    key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        //}

        ///// <summary>
        ///// Lows the level keyboard proc.
        ///// </summary>
        ///// <param name="nCode">The asynchronous code.</param>
        ///// <param name="wParam">The forward parameter.</param>
        ///// <param name="lParam">The calculate parameter.</param>
        ///// <returns></returns>
        //private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        //{
        //    bool blnEat = false;
        //    switch (wParam)
        //    {
        //        case 256:
        //        case 257:
        //        case 260:
        //        case 261:
        //            //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
        //            if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
        //            ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
        //            27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
        //            == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
        //            (lParam.flags == 32)))
        //            {
        //                blnEat = true;
        //            }
        //            break;
        //    }
        //    try
        //    {
        //        if (blnEat)
        //            return 1;
        //        else
        //        {
        //            try
        //            {
        //                int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
        //                return Hook;
        //            }
        //            catch (Exception)
        //            {
        //                return 1;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return 1;
        //    }

        //}
        //static LowLevelKeyboardProcDelegate mHookProc;
        //private void KeyboardHook(object sender, EventArgs e)
        //{
        //    // NOTE: ensure delegate can't be garbage collected
        //    mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
        //    // Get handle to main .exe
        //    IntPtr hModule = GetModuleHandle(IntPtr.Zero);
        //    // Hook
        //    intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
        //    if (intLLKey == IntPtr.Zero)
        //    {
        //        MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

        //    }

        //}

        ///// <summary>
        ///// Releases the keyboard hook.
        ///// </summary>
        //private void ReleaseKeyboardHook()
        //{
        //    intLLKey = UnhookWindowsHookEx(intLLKey);
        //}
        //#endregion

        private void ExpBGSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpGraphicsSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpBorderSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        //private void rdbDynamic_Click(object sender, RoutedEventArgs e)
        //{
        //    //cmbLoadBackGround.ItemsSource = null;
        //    //LoadVideoTemplate();
        //}
        //private void rdbStatic_Click(object sender, RoutedEventArgs e)
        //{
        //    cmbLoadBackGround.ItemsSource = null;
        //    LoadImageBackground();
        //}

        private void ChkSelected_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (!string.IsNullOrEmpty(chk.Uid) && objTemplateList != null)
            {
                long id = Convert.ToInt64(chk.Uid);
                objTemplateList.Where(o => o.Item_ID == id).FirstOrDefault().IsChecked = (bool)chk.IsChecked;
            }

        }


        #region Editor Functions

        private void SaveCGConfig(object sender, EventArgs e)
        {
            SaveCGConfigDb();
            BindCGConfigCombo();
            ResetGraphicsControl();
            if (!cgEditor.IsFleNameExists)
            {
                cgEditor.Close();
            }

        }

        string destpath = string.Empty;
        string _filename = string.Empty;
        void SaveCGConfigDb()
        {
            if (dicConfig.ContainsValue(cgEditor.ConfigFileName) && cgEditor.IsUpdateConfig == false)
            {
                cgEditor.IsFleNameExists = true;
                return;
            }
            else
                cgEditor.IsFleNameExists = false;
            objConfigBL = new ConfigBusiness();
            objCGConfig = new CGConfigSettings();
            string extension = ".ml-cgc";

            objCGConfig.Extension = extension;
            objCGConfig.ConfigFileName = cgEditor.ConfigFileName;
            objCGConfig.IsActive = true;
            try
            {
                if (cgEditor.IsUpdateConfig)
                    CgConfigId = Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue);
                if (!cgEditor.IsUpdateConfig)
                    CgConfigId = objConfigBL.SaveCGConfigSetting(objCGConfig);
                if (CgConfigId > 0)
                    cgEditor.IsSaved = true;
                MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)cgEditor.Editor.CGObject;
                string fileName = cgEditor.ConfigFileName + "_" + CgConfigId + extension;
                string directoryPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                pXMLPersist.SaveToXMLFile(System.IO.Path.Combine(directoryPath, fileName), -1);
                destpath = directoryPath;
                _filename = fileName;
                bw_CopySettings.RunWorkerAsync();

            }
            catch (Exception ex)
            {

            }

        }
        private void LoadGraphicEditor()
        {
            try
            {
                List<TemplateListItems> lstGraphics = objTemplateList.Where(o => o.IsChecked == true).ToList();
                if (lstGraphics.Count > 0 || (chkTextLogo.IsChecked == true && !string.IsNullOrEmpty(txtTextLogo.Text)) || cmbLoadGraphicsProfile.SelectedIndex > 0)
                {
                    var lstGraphicss = lstGraphic.SelectedItems;// = objTemplateList.Where(o => o.MediaType == 606 && o.IsChecked == true).ToList();
                    string _logoText = string.Empty;
                    if (m_objCharGen == null)
                        m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                    m_objMixer.PluginsAdd(m_objCharGen, 0);
                    LoadCGPlugin(true, m_objMixer);
                    cgEditor = new CGEditor_WinForms.MainWindow();
                    cgEditor.CGConfig += SaveCGConfig;
                    cgEditor.FormClosed += cgEditor_FormClosed;
                    //cgEditor.SetSourceObject(null, null);
                    cgEditor.SetSourceObject(m_objMixer, m_objCharGen);
                    if (cmbLoadGraphicsProfile.SelectedIndex > 0)
                    {
                        CGConfigSettings objCGConfig = GetCGConfigSettings(Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue));
                        if (objCGConfig == null)
                            return;
                        cgEditor.UpdateFileName(objCGConfig.DisplayName);
                        cgEditor.ConfigFileName = objCGConfig.DisplayName;
                        MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                        string graphicFilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension);
                        if (File.Exists(graphicFilePath))
                            pXMLPersist.LoadFromXML(graphicFilePath, -1);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtTextLogo.Text.Trim()))
                        {
                            _logoText = txtTextLogo.Text.Trim();
                            m_objCharGen.AddNewItem(_logoText, 0.1, 0.1, 1, 1, ref m_strItemID);
                            m_objCharGen.SetItemProperties(m_strItemID, "", "", "", 0);
                            m_objCharGen.ShowItem(m_strItemID, 1, 1000);
                            m_strItemID = string.Empty;
                        }
                        foreach (var item in lstGraphics)
                        {
                            m_objCharGen.AddNewItem(item.FilePath, 0, 0, 0, 1, ref m_strItemID);
                            m_strItemID = string.Empty;
                        }
                    }
                    cgEditor.Editor.UpdateItemsList();
                    cgEditor.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Please select graphics profile or graphics.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void cgEditor_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            if (CgConfigId == 0 && string.IsNullOrEmpty(cgEditor.ConfigFileName))
            {
                RemovePlugins();
            }
            cgEditor.CGConfig -= SaveCGConfig;
            cgEditor.FormClosed -= cgEditor_FormClosed;

        }
        MLCHARGENLib.IMLXMLPersist pXMLPersist;
        private void LoadCGConfigSettingFromProfile()
        {
            try
            {
                if (m_objMixer != null)
                {
                    RemovePlugins();
                    //cgEditor.Editor.UpdateItemsList();
                    m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                    //m_objMixer.PluginsRemove(m_objCharGen);
                    m_objMixer.PluginsAdd(m_objCharGen, 0);
                    string abc = string.Empty;
                    if (cgEditor == null)
                        cgEditor = new CGEditor_WinForms.MainWindow();
                    cgEditor.SetSourceObject(m_objMixer, m_objCharGen);
                    //cgEditor.SetSourceObject(m_objMixer, m_objCharGen);
                    Thread.Sleep(1000);
                    if (cmbLoadGraphicsProfile.SelectedIndex > 0)
                    {

                        CGConfigSettings objCGConfig = GetCGConfigSettings(Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue));
                        if (objCGConfig == null)
                            return;
                        cgEditor.UpdateFileName(objCGConfig.DisplayName);
                        cgEditor.ConfigFileName = objCGConfig.DisplayName;
                        pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                        pXMLPersist.LoadFromXML(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension), -1);
                        cgEditor.Editor.UpdateItemsList();

                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void RemovePlugins()
        {
            try
            {
                if (m_objCharGen != null)
                {
                    m_objCharGen.RemoveItem("", 1000);
                    try
                    {
                        m_objCharGen.RemoveItem(m_strItemID, 1000);

                    }
                    catch (System.Exception ex)
                    { }
               
                m_objMixer.PluginsRemove(m_objCharGen);

                Marshal.ReleaseComObject(m_objCharGen);
                }
                m_strItemID = "";
                m_objCharGen = null;
            }
            catch (System.Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }


        }
        private void RemoveColorPlugins()
        {
            try
            {
                m_objMixer.PluginsRemove(m_objColors);
                //  Marshal.ReleaseComObject(m_objColors);
            }
            catch (System.Exception ex)
            { }
        }
        private CGConfigSettings GetCGConfigSettings(int configID)
        {
            try
            {
                objConfigBL = new ConfigBusiness();
                objCGConfig = new CGConfigSettings();
                objCGConfig = objConfigBL.GetCGConfigSettngs(Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
            return objCGConfig;
        }
        #endregion

        private void btnAddGraphics_Click(object sender, RoutedEventArgs e)
        {
            RemovePlugins();
            LoadGraphicEditor();
        }

        void SaveScene()
        {

            if (m_objMixer != null)
            {
                string _scenePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", SceneName);
                if (!Directory.Exists(_scenePath))
                    Directory.CreateDirectory(_scenePath);
                if (OverlayElement != null)
                {
                    if (cmbLoadVideoOverlay.SelectedIndex != 0)
                    {
                        OverlayElement.ElementMultipleSet("h=1 w=1 x=0 y=0 audio_gain=-100 show=true", 0.0);
                        OverlayElement.ElementReorder(500);
                        SaveChromaStreamWise(OverlayStream, true);
                    }
                    else
                    {
                        string _overlaychromaSavePath = System.IO.Path.Combine(_scenePath + "_OverlayChroma.xml");
                        if (File.Exists(_overlaychromaSavePath))
                        {
                            File.Delete(_overlaychromaSavePath);
                        }
                    }
                }
                if (BorderElement != null)
                {
                    if (cmbLoadBorders.SelectedIndex != 0)
                    {
                        BorderElement.ElementMultipleSet("h=1 w=1 x=0 y=0 audio_gain=-100 show=true", 0.0);
                        BorderElement.ElementReorder(1000);
                    }
                }

                IMPersist m_imPersist = (IMPersist)m_objMixer;
                string _saveSceneFilePath = System.IO.Path.Combine(_scenePath, SceneName + ".xml");
                m_imPersist.PersistSaveToFile("", _saveSceneFilePath, "");
                MessageBox.Show("Scene saved successfully.");
                CopyProfileToAll(lstConfigurationInfo, _saveSceneFilePath);
            }

            //m_objMixer.PluginsAdd("",);
        }



        private void cmbLoadBackGround_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbLoadBackGround.SelectedIndex > 0)
            {
                TemplateListItems objBackGround = (TemplateListItems)cmbLoadBackGround.SelectedItem;
                if (objBackGround != null)
                {
                    MItem mItem;
                    //m_objMixer.StreamsBackgroundSet("", objBackGround.FilePath, "", out mItem, 0);
                    if (!objBackGround.Name.Contains(".jpg") && !objBackGround.Name.Contains(".png"))
                        m_objMixer.StreamsBackgroundSet("", System.IO.Path.Combine(ConfigManager.DigiFolderPath, "VideoBackGround", objBackGround.Name), "", out mItem, 0);
                    else
                        m_objMixer.StreamsBackgroundSet("", objBackGround.FilePath, "", out mItem, 0);
                    Thread.Sleep(500);
                    Marshal.ReleaseComObject(mItem);
                }
            }
        }
        MElement BorderElement;
        MElement GuestElement;
        MElement OverlayElement;
        private void GetGuestElement()
        {
            MElement myElementRoute1 = null;
            string entry = string.Empty;
            m_objMixer.ElementsGetByIndex(0, out myElementRoute1);
            ((IMElements)myElementRoute1).ElementsGetByID("Guest_Video", out GuestElement);
            //((IMElements)myElementRoute1).ElementsGetByID("Buffer_Video", out BufferElement);
            ((IMElements)myElementRoute1).ElementsGetByID("Overlay_Video", out OverlayElement);
            ((IMElements)myElementRoute1).ElementsGetByID("Border_Video", out BorderElement);
        }
        string borderStream = string.Empty;
        string OverlayStream = string.Empty;

        private void cmbLoadBorders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TemplateListItems objBackGround = (TemplateListItems)cmbLoadBorders.SelectedItem;
                if (objBackGround != null && cmbLoadBorders.SelectedIndex > 0)
                {
                    MItem pFile;
                    m_pMixerStreams = (IMStreams)m_objMixer;
                    m_pMixerStreams.StreamsAdd(borderStream, null, objBackGround.FilePath, "loop=false", out pFile, 0);
                    if (string.IsNullOrEmpty(borderStream))
                    {
                        int count; string streamName = string.Empty;
                        m_pMixerStreams.StreamsGetCount(out count);
                        m_pMixerStreams.StreamsGetByIndex(count - 1, out streamName, out pFile);
                        borderStream = streamName;
                    }
                    if (BorderElement != null)
                        BorderElement.ElementMultipleSet("stream_id=" + borderStream + " h=1 w=1 x=0 y=0 audio_gain=-100 show=true", 0.0);
                }
                if (cmbLoadBorders.SelectedIndex == 0)
                {
                    if (BorderElement != null)
                        BorderElement.ElementMultipleSet("show=false", 0.0);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseGuestImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
                //  fileDialog.Filter = "Image Files | *.jpg";
                fileDialog.Filter = "Select File|*.jpg;*.png;*.mp4;*.wmv;*.avi;|" + "JPG|*.jpg;*.JPG|PNG|*.png;*.PNG|WMV|*.wmv;*.WMV|MP4|*.mp4;*.MP4|AVI|*.avi;*.AVI;";
                fileDialog.ShowDialog();
                string FileName = fileDialog.FileName;
                if (!string.IsNullOrEmpty(FileName))
                {
                    MItem pFile; string streamID = string.Empty;
                    m_pMixerStreams = (IMStreams)m_objMixer;
                    m_pMixerStreams.StreamsAdd("stream-000", null, FileName, "loop=false", out pFile, 0);
                    Thread.Sleep(500);
                    try
                    {
                        if (pFile != null)
                            mMixerList1.SelectFile(pFile);
                    }
                    catch { }
                    try
                    {
                        mMixerList1.UpdateList(true, 1);
                    }
                    catch { }
                    Marshal.ReleaseComObject(pFile);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void cmbLoadGraphicsProfile_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadCGConfigSettingFromProfile();
        }
        private void bw_DeleteSettings_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
        }

        private void bw_DeleteSettings_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DeleteFromAllSubstore(lstConfigurationInfo, deleteFileName);
        }

        private void bw_CopySettings_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            MessageBox.Show("Please enter scene name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void bw_CopySettings_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            CopyToAllSubstore(lstConfigurationInfo, destpath, _filename);
        }

        private void GetAllSubstoreConfigdata()
        {
            try
            {
                lstConfigurationInfo = (new ConfigBusiness()).GetAllSubstoreConfigdata();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void CopyChromaToAll(List<ConfigurationInfo> _objConfigInfo, string Sourcefilepath)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string destpath = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "Profiles", SceneName);
                        if (!System.IO.Directory.Exists(destpath))
                            Directory.CreateDirectory(destpath);
                        System.IO.File.Copy(Sourcefilepath, System.IO.Path.Combine(destpath, SceneName + "_chroma.xml"));
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }

        private void CopyProfileToAll(List<ConfigurationInfo> _objConfigInfo, string Sourcefilepath)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string destpath = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "Profiles", SceneName);
                        if (!System.IO.Directory.Exists(destpath))
                            Directory.CreateDirectory(destpath);
                        System.IO.File.Copy(Sourcefilepath, System.IO.Path.Combine(destpath, SceneName + ".xml"));
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void CopyToAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile, string _filename)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string destpath = string.Empty;
                        string Sourcepath = string.Empty;
                        Sourcepath = System.IO.Path.Combine(Sourcefile, _filename);
                        string fileName = _filename;
                        string target = string.Empty;
                        //  strPath = item.DG_Hot_Folder_Path + "Profiles\\" + _sceneName;
                        destpath = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "CGSettings");
                        if (!System.IO.Directory.Exists(destpath))
                            System.IO.Directory.CreateDirectory(destpath);
                        System.IO.File.Copy(Sourcepath, System.IO.Path.Combine(destpath, fileName), true);

                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void DeleteFromAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string strPath = string.Empty;

                        strPath = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "CGSettings", Sourcefile);

                        string destFile = strPath;
                        if (System.IO.File.Exists(destFile))
                        {
                            File.Delete(destFile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void btnAudioSettings_Click(object sender, RoutedEventArgs e)
        {
            winMPreviewComposer.Visibility = Visibility.Collapsed;
            AudioSettingsControlsBox.AudioFileName = SceneName;
            AudioSettingsControlsBox.ShowHandlerDialog();
        }
        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteGraphicProfile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                objConfigBL = new ConfigBusiness();
                if (cmbLoadGraphicsProfile.SelectedIndex > 0)
                {
                    int configID = Convert.ToInt32(cmbLoadGraphicsProfile.SelectedValue);
                    CGConfigSettings objCGConfig = GetCGConfigSettings(configID);
                    if (objCGConfig != null)
                    {
                        File.Delete(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension));
                        bool isDeleted = objConfigBL.DeleteCGConfigSettngs(configID);
                        if (isDeleted)
                        {
                            MessageBox.Show("Graphics profile deleted successfully");
                            deleteFileName = objCGConfig.ConfigFileName + objCGConfig.Extension;
                            CgConfigId = 0;
                        }
                        bs.Show();
                        bw_DeleteSettings.RunWorkerAsync();
                        BindCGConfigCombo();
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void ResetGraphicsControl()
        {
            if (objTemplateList != null)
            {
                foreach (TemplateListItems item in objTemplateList)
                {
                    item.IsChecked = false;
                }
                lstGraphic.ItemsSource = null;
                lstGraphic.ItemsSource = objTemplateList;
            }
            chkTextLogo.IsChecked = false;
            txtTextLogo.Text = "";
        }

        /// <summary>
        /// Function to zoom in and out the guest image
        /// </summary>
        /// <param name="blGrow"></param>
        private void ZoomInOut(bool blGrow)
        {
            double h;
            double w;
            double zoom = 0.05;

            GuestElement.AttributesDoubleGet("w", out w);
            GuestElement.AttributesDoubleGet("h", out h);
            if (w == 0 & h == 0)
            {
                string _strw;
                string _strh;
                GuestElement.AttributesInfoGet("w", MPLATFORMLib.eMInfoType.eMIT_Default, out _strw);
                GuestElement.AttributesInfoGet("h", MPLATFORMLib.eMInfoType.eMIT_Default, out _strh);
                w = Convert.ToDouble(_strw);
                h = Convert.ToDouble(_strh);
            }

            w = blGrow ? w + zoom : w - zoom;
            h = blGrow ? h + zoom : h - zoom;
            string strZoomValue = "w=" + w + " " + "h=" + h;

            if (!(h <= 0.01 || w <= 0.01))
            {
                if ((blGrow && (h <= 4.0 || w <= 4.0)) || !blGrow && (h >= 0.25 || w >= 0.25))
                {
                    GuestElement.AttributesMultipleSet(strZoomValue, eMUpdateType.eMUT_Update);
                    if ((w <= 1 && w >= 0) && (h <= 1 && h >= 0))
                    {
                        txtHeight.Text = Convert.ToString(h);
                        txtWidth.Text = Convert.ToString(w);
                    }
                }
            }
        }
        private void buttonZoomIn_Click(object sender, RoutedEventArgs e)
        {
            ZoomInOut(true);
        }
        private void buttonZoomOut_Click(object sender, RoutedEventArgs e)
        {
            ZoomInOut(false);
        }

        #region Overlay
        private void ExpVideoOverlay_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void cmbLoadVideoOverlay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TemplateListItems objBackGround = (TemplateListItems)cmbLoadVideoOverlay.SelectedItem;
                if (objBackGround != null && cmbLoadVideoOverlay.SelectedIndex > 0)
                {
                    MItem pFile;
                    m_pMixerStreams = (IMStreams)m_objMixer;
                    m_pMixerStreams.StreamsAdd(OverlayStream, null, System.IO.Path.Combine(ConfigManager.DigiFolderPath, "VideoOverlay", objBackGround.Name), "loop=false", out pFile, 0);
                    if (string.IsNullOrEmpty(OverlayStream))
                    {
                        int count; string streamName = string.Empty;
                        m_pMixerStreams.StreamsGetCount(out count);
                        m_pMixerStreams.StreamsGetByIndex(count - 1, out streamName, out pFile);
                        OverlayStream = streamName;
                    }
                    if (OverlayElement != null)
                    {
                        OverlayElement.ElementMultipleSet("stream_id=" + OverlayStream + " h=1 w=1 x=0 y=0 audio_gain=-100 show=true", 0.0);
                        OverlayElement.ElementReorder(500);
                    }
                }
                if (cmbLoadVideoOverlay.SelectedIndex == 0)
                {
                    if (OverlayElement != null)
                    {
                        OverlayElement.ElementMultipleSet("show=false", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        List<TemplateListItems> lstVideoOverlay;
        private void ShowVideoOverlay()
        {
            try
            {
                TemplateListItems tmpList = new TemplateListItems();
                List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
                tmpList.isActive = true;
                tmpList.FilePath = string.Empty;
                tmpList.Item_ID = 0;
                tmpList.IsChecked = false;
                tmpList.DisplayName = "-- Select Video Overlay --";
                tmpList.MediaType = 2;
                tmpList.Name = "--Select Video Overlay--";
                objTemplateList.Add(tmpList);
                var objVideoOverlay = (new BorderBusiness()).GetVideoOverlays().Where(t => t.IsActive == true);
                foreach (var item in objVideoOverlay)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = tmpList.FilePath = @"/DigiConfigUtility;component/images/vidico.png";
                    tmpList.Item_ID = item.VideoOverlayId;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DisplayName;
                    tmpList.MediaType = 609;
                    tmpList.Name = item.Name;
                    tmpList.Tooltip = "Video Overlays";
                    objTemplateList.Add(tmpList);
                }
                cmbLoadVideoOverlay.ItemsSource = objTemplateList;
                lstVideoOverlay = objTemplateList;
                cmbLoadVideoOverlay.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private string WriteColorEffectsXML(List<FrameClass> lstframes)
        {
            string strXML = string.Empty;
            strXML = "<ColorEffects>" + "\n";
            foreach (var items in lstframes)
            {
                strXML += "<frame no='" + items.Count.Trim().Replace(". ", "") + "'>" + items.Position.Trim() + "</frame>" + "\n";
            }
            strXML += "</ColorEffects>" + "\n";
            return strXML;
        }
        private void btnChromaOverlay_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(OverlayStream))
                LoadChromaScreen(OverlayStream);
            else
                MessageBox.Show("Please select a video overlay!");
        }
        #endregion Overlay
        #region Color effects
        string presetEffects = "None";
        private void SetColorConversion(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetColorConversion();
        }
        private void SetBrightnessContrast(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetBrightnessContrast();
        }
        private void ExpColorSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void rbColorEffect_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            VideoColorEffects colorEffects = new VideoColorEffects();
            if (rb.IsChecked == true && (rb.Name == "rbGrayScale"))
            {
                presetEffects = "GrayScale";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(1);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbSepia"))
            {
                presetEffects = "Sepia";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(2);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbNone"))
            {
                presetEffects = "None";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(3);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbInvert"))
            {
                presetEffects = "Invert";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(4);
            }
            UpdateColorSliderValues(colorEffects);
        }
        private void UpdateColorSliderValues(VideoColorEffects colorEffects)
        {
            sldULevelSlider.Value = colorEffects.ULevel;
            sldUVGainSlider.Value = colorEffects.UVGain;
            sldVLevelSlider.Value = colorEffects.VLevel;
            sldYGainSlider.Value = colorEffects.YGain;
            sldYLevelSlider.Value = colorEffects.YLevel;
            sldBlackSlider.Value = colorEffects.BlackLevel;
            sldBrightnessSlider.Value = colorEffects.Brightness;
            sldColorSlider.Value = colorEffects.ColorGain;
            sldContrastSlider.Value = colorEffects.Contrast;
            sldWhiteSlider.Value = colorEffects.WhiteLevel;
        }
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            VideoColorEffects colorEffects = new VideoColorEffects();
            colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(3);
            rbNone.IsChecked = true;
            UpdateColorSliderValues(colorEffects);
        }
        private void SetColorConversion()
        {
            if (m_objColors != null)
            {
                MG_COLOR_PARAM colorParam = new MG_COLOR_PARAM
                {
                    dblUlevel = (double)sldULevelSlider.Value / 100,
                    dblUVGain = (double)sldUVGainSlider.Value / 100,
                    dblVlevel = (double)sldVLevelSlider.Value / 100,
                    dblYGain = (double)sldYGainSlider.Value / 100,
                    dblYlevel = (double)sldYLevelSlider.Value / 100
                };
                m_objColors.SetColorParam(ref colorParam, 1, 0);
            }
        }
        private void SetBrightnessContrast()
        {
            if (m_objColors != null)
            {
                MG_BRIGHT_CONT_PARAM brightContParam = new MG_BRIGHT_CONT_PARAM
                {
                    dblWhiteLevel = (double)sldWhiteSlider.Value / 100,
                    dblContrast = (double)sldContrastSlider.Value / 100,
                    dblColorGain = (double)sldColorSlider.Value / 100,
                    dblBrightness = (double)sldBrightnessSlider.Value / 100,
                    dblBlackLevel = (double)sldBlackSlider.Value / 100
                };
                m_objColors.SetBrightContParam(ref brightContParam, 1, 0);
            }
        }
        private void LoadValuestoColorControls(VideoColorEffects colorEffects)
        {
            string presetEffects = colorEffects.PresetEffects;
            if (presetEffects == "Sepia")
                rbSepia.IsChecked = true;
            else if (presetEffects == "GrayScale")
                rbGrayScale.IsChecked = true;
            else
                rbNone.IsChecked = true;

            UpdateColorSliderValues(colorEffects);
            SetColorConversion();
        }

        #endregion Color effects
    }
}