﻿using DigiConfigUtility.Utility;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for ManageLevel.xaml
    /// </summary>
    public partial class ManageLevel : System.Windows.Controls.UserControl
    {
        private bool _result = false;
        int _serviceId;
        int _imixId;
        private UIElement _parent;

        public ManageLevel()
        {
            InitializeComponent();
            CmbRunlevel.Text = string.Empty;
            CmbIsService.Text = string.Empty;
            
        }

        public void ShowManageLevel(int serviceId, string ServiceName, int imixId, int RunLevel, bool isService)
        {
            _serviceId = serviceId;
            _imixId = imixId;
            txtSvcName.Text = ServiceName;
            if (RunLevel == 1)
            {
                CmbRunlevel.Text = "Store";
            }
            else
            {
                CmbRunlevel.Text = "Substore";
              }
            if (isService == false)
            {
                CmbIsService.Text = "Exe";
            }
            else
            {
                CmbIsService.Text = "Service";
            }      
        }

        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            //  _parent.IsEnabled = false;
            return _result;
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        private void BtnSaveManage_Click(object sender, RoutedEventArgs e)
        {
            int count = 0;
            int RunLevel=0;
            bool isService=false;

            if (string.IsNullOrEmpty(CmbRunlevel.Text))
            {
                MessageBox.Show("Select RunLevel", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (CmbRunlevel.Text == "Store")
                {
                    RunLevel = 1;
                }
                else
                    RunLevel = 2;
            }

            if(string.IsNullOrEmpty(CmbIsService.Text))
            {
                MessageBox.Show("Select Type", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (CmbIsService.Text == "Exe")
                {
                    isService = false;
                }
                else
                {
                    isService = true;
                }
            }
            if (_imixId > 0)
            {
                MessageBox.Show("This service or exe is started please stop and try", "Information", MessageBoxButton.OK, MessageBoxImage.Information);              
            }
            else
            {

                try
                {
                    ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
                    count = svcPosInfoBusiness.UpdateServiceRunLevelBusiness(_serviceId, RunLevel, isService);
                    if (count > 0)
                    {
                        MessageBox.Show("Record updated successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }


                }
                catch (Exception ex)
                {
                    LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                   
                }
            }
        }
        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
                //   _parent.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        public event EventHandler ExecuteMethod;

        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            
            HideHandlerDialog();
            OnExecuteMethod();
     ((Services)_parent).SelectList();

        }
    }
}
