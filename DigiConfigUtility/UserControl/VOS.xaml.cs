﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for VOS.xaml
    /// </summary>
    public partial class VOS : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = null;
        ConfigBusiness configBusiness = null;
        Dictionary<int, string> lstLocationList;
        /////changed by latika for locationwise settings
        LocationWiseSettingBusiness objLocatinlst = null;
        public long _SubStoreID;
        public int _PosID;
        int _Level;
        ////end
        int SetALLForPreSoldSetting = 0;///changed by latika for presold settings
        public VOS()
        {
            InitializeComponent();
            FillLocationCombo();
            GetConfigData();
            GetStoreConfigData();
            /////////////////modified by latika for locationwise setting image process and video process engine
            BindLocationWisetype(0);
            GetSystemName(0, 0);
            GetLocationSettings();
            BindLocationForSystem();
            ///////////////////////end by latika
            if (chkEnableSingleScreenPreview.IsChecked.ToBoolean())
            {
                SingleScreenAngle.Visibility = Visibility.Visible;
            }
            else
            { SingleScreenAngle.Visibility = Visibility.Hidden; }

            if ((bool)chkIsEnablePartialEditedImage.IsChecked)
                EnableDisableSyncRadio(true);
            else
                EnableDisableSyncRadio(false);
        }

        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = stoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                lstLocationList.Add(0, "All Locations");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name.ToString());
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSaveVOSConfig_Click(object sender, RoutedEventArgs e)
        {

            Regex regex = new Regex(@"^[0-9]{0,4}$"); //regex that allows numeric input only
            if (Config.SubStoreId <= 0)
            {
                MessageBox.Show("Please configure your SubStore.");
                return;
            }
            if (chkAutoColorCorrect.IsChecked == true && chkContrastCorrection.IsChecked == false &&
                chkGammaCorrection.IsChecked == false && chkNoiseReduction.IsChecked == false)
            {
                MessageBox.Show("Please select Contrast and/or Gamma and/or Noise Reduction correction");
                return;
            }

            if (chkIsEnabledStartEndSelect.IsChecked == false)
            {
                chkEnd.IsChecked = false;
            }
            /////change by latika for all location validation

            if (chkisDelayPreSold.IsChecked == true)
            {
                if (string.IsNullOrEmpty(txtDelayTimePresold.Text))
                {
                    MessageBox.Show("Please mention delay tim.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

            }
            if (Convert.ToInt32(cmbLocation.SelectedValue) == 0)
            {
                System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Do you want to set the settings of Is Prepaid Auto-Purchase Active in all the locations.", "DigiConfig", System.Windows.Forms.MessageBoxButtons.YesNo);
                SetALLForPreSoldSetting = 0;
                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    SetALLForPreSoldSetting = 1;
                }
            }
            ///end

            //Show Images from substore
            bool ShowImg = (bool)chkShowSubstorePhotos.IsChecked;
            //----Start--------Nilesh---Zero Search Functionality---27th Dec 2018--------
            bool showSubStoreImg = (bool)chkShowCheckedSubstorePhotos.IsChecked;
            //----End--------Nilesh---Zero Search Functionality---27th Dec 2018--------
            //Enable single screen photo preview
            bool isEnableSingleScreen = (bool)chkEnableSingleScreenPreview.IsChecked;
            bool ischkReceiptPrintSetting = (bool)chkReceiptPrintSetting.IsChecked;
            //******************Added by Manoj at 13-Dec-2018 for print Delivery Note Start**************************

            bool ischkDeliveryNotePrintSetting = chkDeliveryNote.IsChecked == true ? true : false;
            //******************Added by Manoj at 13-Dec-2018 for print Delivery Note End**************************
            Int32 Angle = Convert.ToInt32(cmbSingleScreenAngle.SelectedValue);
            //----Start--------Nilesh---Zero Search Functionality---27th Dec 2018--------
            SetShowImagesFromSubStore(ShowImg, isEnableSingleScreen, ischkReceiptPrintSetting, ischkDeliveryNotePrintSetting, Angle, showSubStoreImg);

            SaveVosSettingsLoc();
        }

        private void chkIsEnabledStartEndSelect_Checked(object sender, RoutedEventArgs e)
        {
            HideShow(Visibility.Visible);
        }

        private void chkIsEnabledStartEndSelect_Unchecked(object sender, RoutedEventArgs e)
        {
            chkEnd.IsChecked = false;
            HideShow(Visibility.Collapsed);
        }

        private void HideShow(Visibility visibility)
        {
            stSelectType.Visibility = visibility;
        }

        /*
        private void GetConfigData()
        {
            try
            {
                _objDataLayer = new DigiPhotoDataServices();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.IsEnabledStartEndSelect);
                filterValues.Add((long)ConfigParams.IsEnabledSelectEnd);

                List<iMIXConfigurationValue> ConfigValuesList = _objDataLayer.GetNewConfigValues(Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledStartEndSelect:
                                chkIsEnabledStartEndSelect.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.IsEnabledSelectEnd:
                                if (ConfigValuesList[i].ConfigurationValue == null || ConfigValuesList[i].ConfigurationValue == "")
                                    chkEnd.IsChecked = false;
                                else
                                    chkEnd.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
                if (chkIsEnabledStartEndSelect.IsChecked == false)
                {
                    chkEnd.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        */

        private void GetConfigLocationData()
        {
            try
            {
                configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.IsEnabledSessionSelect);
                filterValues.Add((long)ConfigParams.IsEnabledSelectEnd);
                filterValues.Add((long)ConfigParams.IsAutoColorCorrection);
                filterValues.Add((long)ConfigParams.IsCorrectAtDownloadActive);
                filterValues.Add((long)ConfigParams.IsContrastCorrection);
                filterValues.Add((long)ConfigParams.IsGammaCorrection);
                filterValues.Add((long)ConfigParams.IsNoiseReduction);
                filterValues.Add((long)ConfigParams.IsEnablePartialEditedImage);
                filterValues.Add((long)ConfigParams.SyncImageType);

                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigLocation(cmbLocation.SelectedValue.ToInt32(), Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledSessionSelect:
                                chkIsEnabledStartEndSelect.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.ShowAllSubstorePhotos://for Show all photos from substore
                                chkShowSubstorePhotos.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsEnabledSelectEnd:
                                if (ConfigValuesList[i].ConfigurationValue == null || ConfigValuesList[i].ConfigurationValue == "")
                                    chkEnd.IsChecked = false;
                                else
                                    chkEnd.IsChecked = ConfigValuesList[i].ConfigurationValue != null && ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsAutoColorCorrection://for auto color correction in images
                                chkAutoColorCorrect.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsCorrectAtDownloadActive://for auto color correction in images
                                bool isCorrectionAtDownload = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                if (isCorrectionAtDownload)
                                    chkCorrectionDownload.IsChecked = true;
                                else
                                    chkCorrectionPrinting.IsChecked = true;
                                break;
                            case (int)ConfigParams.IsContrastCorrection://for auto contrast correction in images
                                chkContrastCorrection.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsGammaCorrection://for auto Gamma correction in images
                                chkGammaCorrection.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsNoiseReduction://for auto Noise Reduction in images
                                chkNoiseReduction.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;

                            case (int)ConfigParams.IsEnablePartialEditedImage:
                                chkIsEnablePartialEditedImage.IsChecked = string.IsNullOrWhiteSpace(ConfigValuesList[i].ConfigurationValue) ? false : Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;

                            case (int)ConfigParams.SyncImageType:
                                if (ConfigValuesList[i].ConfigurationValue == null || ConfigValuesList[i].ConfigurationValue == "")
                                    rdbSyncEdited.IsChecked = true;
                                else if (ConfigValuesList[i].ConfigurationValue == "1")
                                    rdbSyncEdited.IsChecked = true;
                                else if (ConfigValuesList[i].ConfigurationValue == "2")
                                    rdbSyncPartialEdited.IsChecked = true;
                                else if (ConfigValuesList[i].ConfigurationValue == "3")
                                    rdbSyncBoth.IsChecked = true;
                                break;
                        }
                    }
                }
                if (chkIsEnabledStartEndSelect.IsChecked == false)
                {
                    chkEnd.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        /*
        private void SaveVosSettings()
        {
            try
            {
                List<iMIXConfigurationValue> objConfigList = new List<iMIXConfigurationValue>();
                iMIXConfigurationValue ConfigValue = null;
                int substoreId = Config.SubStoreId;
                _objDataLayer = new DigiPhotoDataServices();

                //Is Enabled
                ConfigValue = new iMIXConfigurationValue();
                ConfigValue.ConfigurationValue = chkIsEnabledStartEndSelect.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledStartEndSelect;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                if (chkIsEnabledStartEndSelect.IsChecked == true)
                {
                    ConfigValue = new iMIXConfigurationValue();
                    ConfigValue.ConfigurationValue = chkEnd.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledSelectEnd;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    objConfigList.Add(ConfigValue);
                }
                bool IsSaved = _objDataLayer.SaveUpdateNewConfig(objConfigList);
                if (IsSaved)
                {
                    MessageBox.Show("VOS configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        */
        private void SaveVosSettingsLoc()
        {
            try
            {
                List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
                iMixConfigurationLocationInfo ConfigValue = null;
                int substoreId = Config.SubStoreId;
                int locationId = cmbLocation.SelectedValue.ToInt32();
                configBusiness = new ConfigBusiness();

                //Is Enabled
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = chkIsEnabledStartEndSelect.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledSessionSelect;
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
                if (chkIsEnabledStartEndSelect.IsChecked == true)
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkEnd.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledSelectEnd;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = chkIsEnablePartialEditedImage.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnablePartialEditedImage;
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);

                if ((bool)chkIsEnablePartialEditedImage.IsChecked)
                {
                    int SyncImageTypeValue = 0;
                    if ((bool)rdbSyncEdited.IsChecked)
                        SyncImageTypeValue = 1;
                    else if ((bool)rdbSyncPartialEdited.IsChecked)
                        SyncImageTypeValue = 2;
                    else if ((bool)rdbSyncBoth.IsChecked)
                        SyncImageTypeValue = 3;
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = SyncImageTypeValue.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.SyncImageType;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                }


                // For Auto Color Correction
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = chkAutoColorCorrect.IsChecked.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsAutoColorCorrection;
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
                if (chkAutoColorCorrect.IsChecked == true)
                {
                    ConfigValue = new iMixConfigurationLocationInfo();
                    if (chkCorrectionDownload.IsChecked == true)
                        ConfigValue.ConfigurationValue = "true";
                    else
                        ConfigValue.ConfigurationValue = "false";

                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsCorrectAtDownloadActive;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkContrastCorrection.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsContrastCorrection;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkGammaCorrection.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsGammaCorrection;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkNoiseReduction.IsChecked.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsNoiseReduction;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                }////changed by latika for presold
                if (Convert.ToInt32(cmbLocation.SelectedValue) > 0)
                {

                    try
                    {
                        ///////////////created by latika for PreSold settings//////////////
                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = "False";
                        if (chkPresoldActive.IsChecked == true)
                        {
                            ConfigValue.ConfigurationValue = "True";
                        }
                        ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPrepaidAutoPurchaseActive;
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        //ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);
                        /////////////////end/////////////////////
                    }
                    catch { }

                    try
                    {
                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = "0";
                        if (chkisDelayPreSold.IsChecked == true)
                        {
                            ConfigValue.ConfigurationValue = txtDelayTimePresold.Text;
                        }
                        ConfigValue.IMIXConfigurationMasterId = (int)configBusiness.GETPreSoldDelayTmMasterID();
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        ConfigValue.LocationId = locationId;
                        objConfigList.Add(ConfigValue);
                    }
                    catch { }
                }
                else if (SetALLForPreSoldSetting == 1)////ALL Location Presold Config
                {
                    for (int i = 0; i < cmbLocation.Items.Count; i++)
                    {

                        string[] strLocation = ((cmbLocation.Items[i].ToString().Replace("[", "")).Replace("]", "")).Split(',');
                        int iLocationid = Convert.ToInt32(strLocation[0]);
                        if (iLocationid > 0)
                        {

                            try
                            {
                                ///////////////created by latika for PreSold settings//////////////
                                ConfigValue = new iMixConfigurationLocationInfo();
                                ConfigValue.ConfigurationValue = "False";
                                if (chkPresoldActive.IsChecked == true)
                                {
                                    ConfigValue.ConfigurationValue = "True";
                                }
                                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPrepaidAutoPurchaseActive;
                                ConfigValue.SubstoreId = Config.SubStoreId;
                                ConfigValue.LocationId = iLocationid;
                                objConfigList.Add(ConfigValue);
                                /////////////////end/////////////////////
                            }
                            catch { }


                            try
                            {
                                ///////////////created by latika for PreSold settings//////////////

                                ConfigValue = new iMixConfigurationLocationInfo();
                                ConfigValue.ConfigurationValue = "0";
                                if (chkisDelayPreSold.IsChecked == true)
                                {
                                    ConfigValue.ConfigurationValue = txtDelayTimePresold.Text;
                                }
                                ConfigValue.IMIXConfigurationMasterId = (int)configBusiness.GETPreSoldDelayTmMasterID();
                                ConfigValue.SubstoreId = Config.SubStoreId;
                                ConfigValue.LocationId = iLocationid;
                                objConfigList.Add(ConfigValue);
                            }
                            catch { }
                        }
                    }
                }
                /////////////////end/////////////////////

                bool IsSaved = configBusiness.SaveUpdateConfigLocation(objConfigList);
                if (IsSaved)
                {
                    MessageBox.Show("VOS configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                GetConfigLocationData();
                GetConfigData();///by latika 
                if (chkIsEnabledStartEndSelect.IsChecked == true)
                    HideShow(Visibility.Visible);
                else
                    HideShow(Visibility.Collapsed);

                if ((bool)chkIsEnablePartialEditedImage.IsChecked)
                    EnableDisableSyncRadio(true);
                else
                    EnableDisableSyncRadio(false);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Set configuration parameter to true to show images only from the sub store.
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
		//----Start--------Nilesh---Zero Search Functionality---27th Dec 2018--------
        private bool SetShowImagesFromSubStore(bool enable, bool isEnableSingleScreen, bool ischkReceiptPrintSetting, bool ischkDeliveryNotePrintSetting, Int32 Angle, bool showSubstorePhoto)
        {
            List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
            iMIXConfigurationInfo ConfigValue = new iMIXConfigurationInfo();
            ConfigValue.SubstoreId = Config.SubStoreId;
            ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ShowAllSubstorePhotos);
            ConfigValue.ConfigurationValue = enable.ToString();
            objConfigList.Add(ConfigValue);
            /////////////////////////////////////////////
            ConfigValue = new iMIXConfigurationInfo();
            ConfigValue.SubstoreId = Config.SubStoreId;
            ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsEnableSingleScreenPreview);
            ConfigValue.ConfigurationValue = isEnableSingleScreen.ToString();
            objConfigList.Add(ConfigValue);
            try
            {
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.SingleScreenPreviewAngle);
                ConfigValue.ConfigurationValue = cmbSingleScreenAngle.SelectedValue.ToString();
                objConfigList.Add(ConfigValue);
            }
            catch { }

            try
            {
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsEnableSlipPrint);
                ConfigValue.ConfigurationValue = Convert.ToString(ischkReceiptPrintSetting);
                objConfigList.Add(ConfigValue);
            }
            catch { }
            try
            {
                //******************Added by Manoj at 13-Dec-2018 for print Delivery Note Start**************************
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DeliveryNoteRequired);
                ConfigValue.ConfigurationValue = Convert.ToString(ischkDeliveryNotePrintSetting);
                objConfigList.Add(ConfigValue);
            }
            catch { }
            try
            {
                //******************Added by Manoj at 13-Dec-2018 for print Delivery Note End**************************
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsEnableImageEditingMDownload);
                ConfigValue.ConfigurationValue = chkIsEnableImageEditing.IsChecked.ToString();
                objConfigList.Add(ConfigValue);
            }
            catch { }
            try
            {
                //-----Start-------Nilesh----Show photo on specific substore--1Nov 2018---
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.SubstoreId = Config.SubStoreId;
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ShowCheckSubstorePhotos);
                ConfigValue.ConfigurationValue = showSubstorePhoto.ToString();
                objConfigList.Add(ConfigValue);
            }
            catch { }
            try
            {
                ///////////////created by latika for group delete settings//////////////
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = "False";
                if (chkGroupDelete.IsChecked == true)
                {
                    ConfigValue.ConfigurationValue = "True";
                }
                ConfigValue.IMIXConfigurationMasterId = configBusiness.GETGroupDeleteMasterID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                //ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
                /////////////////end/////////////////////
            }
            catch { }
            try
            {
                ///////////////created by latika for Stop Showing editing images in client view settings//////////////
                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = "False";
                if (chkClientView.IsChecked == true)
                {
                    ConfigValue.ConfigurationValue = "True";
                }
                ConfigValue.IMIXConfigurationMasterId = configBusiness.GETStopEditingImagesinclientViewMasterID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                //ConfigValue.LocationId = locationId;
                objConfigList.Add(ConfigValue);
                /////////////////end/////////////////////
            }
            catch { }
            try
            {
                ///////////////created by latika for delay time PreSold settings//////////////


                if (SetALLForPreSoldSetting == 1 && Convert.ToInt32(cmbLocation.SelectedValue) == 0)////ALL Location Presold Config
                {

                    ///////////////created by latika for PreSold settings//////////////
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = "False";
                    if (chkPresoldActive.IsChecked == true)
                    {
                        ConfigValue.ConfigurationValue = "True";
                    }
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPrepaidAutoPurchaseActive;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    //ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);
                    try
                    {
                        ConfigValue = new iMixConfigurationLocationInfo();
                        ConfigValue.ConfigurationValue = "0";
                        if (chkisDelayPreSold.IsChecked == true)
                        {
                            ConfigValue.ConfigurationValue = txtDelayTimePresold.Text;
                        }
                        ConfigValue.IMIXConfigurationMasterId = (int)configBusiness.GETPreSoldDelayTmMasterID();
                        ConfigValue.SubstoreId = Config.SubStoreId;
                        objConfigList.Add(ConfigValue);
                    }
                    catch { }
                    /////////////////end/////////////////////

                }
                /////////////////end/////////////////////
            }
            catch { }

            //-----End-------Nilesh----Show photo on specific substore--1Nov 2018-----

            ConfigBusiness conBiz = new ConfigBusiness();
            Boolean IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);
            return IsSaved;
        }

        /// <summary>
        /// Loads the configuration values
        /// </summary>
        private void GetConfigData()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                ConfigBusiness conBiz = new ConfigBusiness();
                int GroupDeleteMsterID = conBiz.GETGroupDeleteMasterID();////changed by latika
                chkisDelayPreSold.IsChecked = false;
                txtDelayTimePresold.Text = "0";
                chkClientView.IsChecked = false;
                /////made changes by latika

                if (conBiz.GETStopEditingImageClntvwBySubID(Config.SubStoreId) == true)
                {
                    chkClientView.IsChecked = true;
                }
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(Config.SubStoreId);
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    ////changed by latika for presold
                    chkGroupDelete.IsChecked = false;
                    chkPresoldActive.IsChecked = false;
                    chkisDelayPreSold.IsChecked = false;
                    List<Presold> PreSoldDelayTmVal = conBiz.GETPreSoldDelayTmVal(Config.SubStoreId, Convert.ToInt32(cmbLocation.SelectedValue));

                    if (PreSoldDelayTmVal[0].PreSoldDelayTime > 0)
                    {
                        chkisDelayPreSold.IsChecked = true;
                        txtDelayTimePresold.Text = PreSoldDelayTmVal[0].PreSoldDelayTime.ToString();
                    }
                    if (PreSoldDelayTmVal[0].IsPrefixActiveFlow == true)
                    {
                        chkPresoldActive.IsChecked = true;

                    }
                    if (conBiz.GETGroupDeleteID(Convert.ToInt32(Config.SubStoreId)) == true)
                    {
                        chkGroupDelete.IsChecked = true;
                    }
                    /////end by latika
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {



                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.ShowAllSubstorePhotos:
                                chkShowSubstorePhotos.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsEnableSingleScreenPreview:
                                chkEnableSingleScreenPreview.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsEnableSlipPrint:
                                chkReceiptPrintSetting.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.DeliveryNoteRequired:
                                chkDeliveryNote.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.SingleScreenPreviewAngle:
                                cmbSingleScreenAngle.SelectedValue = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : "0";
                                break;
                            case (int)ConfigParams.IsEnableImageEditingMDownload:
                                chkIsEnableImageEditing.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            //--------------Nilesh------Zero Search-----------27th Dec---------
                            case (int)ConfigParams.ShowCheckSubstorePhotos:
                                chkShowCheckedSubstorePhotos.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();
            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    //case (int)ConfigParams.TripCamCleanupDays:
                    //    txtTripCleanup.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : "0";
                    //    break;
                    //case (int)ConfigParams.ImixCleanUpDays:
                    //    txtImixCleanup.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : "0";
                    //    break;
                    case (int)ConfigParams.MaxDelayAllowedInDummyCaptureDownload:
                        txtDummyDelayAllowed.Text = ConfigValuesList[i].ConfigurationValue != "" ? ConfigValuesList[i].ConfigurationValue.ToString() : "10";
                        break;
                }
            }
            if (string.IsNullOrEmpty(txtDummyDelayAllowed.Text))
            {
                txtDummyDelayAllowed.Text = "10";
            }
        }

        private void chkAutoColorCorrect_Checked(object sender, RoutedEventArgs e)
        {
            spCorrectionMode.Visibility = Visibility.Visible;
            chkCorrectionPrinting.IsEnabled = true;
            chkCorrectionDownload.IsEnabled = true;
            chkCorrectionDownload.IsChecked = true;
            grpCorrectionType.Visibility = Visibility.Visible;
        }

        private void chkAutoColorCorrect_Unchecked(object sender, RoutedEventArgs e)
        {
            spCorrectionMode.Visibility = Visibility.Collapsed;
            chkCorrectionPrinting.IsEnabled = false;
            chkCorrectionDownload.IsEnabled = false;
            chkCorrectionDownload.IsChecked = false;
            chkCorrectionPrinting.IsChecked = false;
            grpCorrectionType.Visibility = Visibility.Collapsed;
        }

        private void chkCorrectionDownload_Checked(object sender, RoutedEventArgs e)
        {
            chkCorrectionPrinting.IsChecked = false;
        }

        private void chkCorrectionPrinting_Checked(object sender, RoutedEventArgs e)
        {
            chkCorrectionDownload.IsChecked = false;
        }

        private void chkEnableSingleScreenPreview_Checked(object sender, RoutedEventArgs e)
        {
            SingleScreenAngle.Visibility = Visibility.Visible;

        }
        private void chkEnableSingleScreenPreview_Unchecked(object sender, RoutedEventArgs e)
        {
            SingleScreenAngle.Visibility = Visibility.Hidden;

        }



        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        //private void btnSaveCleanup_Click(object sender, RoutedEventArgs e)
        //{
        //    List<iMIXStoreConfigurationInfo> objConfigList = new List<iMIXStoreConfigurationInfo>();
        //    iMIXStoreConfigurationInfo ConfigValue = new iMIXStoreConfigurationInfo();

        //    if (!string.IsNullOrEmpty(txtTripCleanup.Text))
        //    {
        //        ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.TripCamCleanupDays);
        //        ConfigValue.ConfigurationValue = txtTripCleanup.Text.Trim();
        //        objConfigList.Add(ConfigValue);
        //    }

        //     ConfigValue = new iMIXStoreConfigurationInfo();
        //     if (!string.IsNullOrEmpty(txtImixCleanup.Text))
        //     {
        //         ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ImixCleanUpDays);
        //         ConfigValue.ConfigurationValue = txtImixCleanup.Text.Trim();
        //         objConfigList.Add(ConfigValue);
        //     }

        //    ConfigBusiness conBiz = new ConfigBusiness();
        //    Boolean IsSaved = conBiz.SaveUpdateNewStoreConfig(objConfigList);

        //    if (IsSaved)
        //    {               
        //        MessageBox.Show("Cleanup configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //    else
        //    {
        //        MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}

        private void btnSaveRFIDSetting_Click(object sender, RoutedEventArgs e)
        {
            List<iMIXStoreConfigurationInfo> objConfigList = new List<iMIXStoreConfigurationInfo>();
            iMIXStoreConfigurationInfo ConfigValue = new iMIXStoreConfigurationInfo();

            ConfigValue = new iMIXStoreConfigurationInfo();
            if (!string.IsNullOrEmpty(txtDummyDelayAllowed.Text.Trim()))
            {
                if (Int32.Parse(txtDummyDelayAllowed.Text) > 30 || Int32.Parse(txtDummyDelayAllowed.Text) < 1)
                {
                    MessageBox.Show("Value must be between 1 to 30", "Digiphoto");
                    return;
                }
                ConfigValue.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.MaxDelayAllowedInDummyCaptureDownload);
                ConfigValue.ConfigurationValue = txtDummyDelayAllowed.Text.Trim();
                objConfigList.Add(ConfigValue);
            }
            else
            {
                MessageBox.Show("Please enter some value", "Digiphoto");
                return;
            }

            ConfigBusiness conBiz = new ConfigBusiness();
            Boolean IsSaved = conBiz.SaveUpdateNewStoreConfig(objConfigList);

            if (IsSaved)
            {
                MessageBox.Show("Dummy manual download settings saved successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void txtTripCleanup_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txtImixCleanup_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }


        private void chkIsEnablePartialEditedImage_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)chkIsEnablePartialEditedImage.IsChecked)
                EnableDisableSyncRadio(true);
            else
                EnableDisableSyncRadio(false);
        }
        private void EnableDisableSyncRadio(bool enable)
        {
            rdbSyncEdited.IsEnabled = enable;
            rdbSyncPartialEdited.IsEnabled = enable;
            rdbSyncBoth.IsEnabled = enable;
        }

        private void chkIsEnablePartialEditedImage_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)chkIsEnablePartialEditedImage.IsChecked)
                EnableDisableSyncRadio(true);
            else
                EnableDisableSyncRadio(false);
        }
        /////////////////modified by latika for locationwise setting image process and video process engine
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int PosLocationId = ((Button)sender).Tag.ToInt32();
                _PosID = PosLocationId;
                objLocatinlst = new LocationWiseSettingBusiness();
                PosLocationAssociationModel objPosDtls = new PosLocationAssociationModel();
                objPosDtls = objLocatinlst.GetPosLocationAssociationByID(PosLocationId, 0);
                // BindLocationWisetype(Convert.ToInt32(objPosDtls.IMIXConfigurationValueId));
                for (int i = 0; i < lstTypes.Items.Count; i++)
                {
                    DependencyObject obj = lstTypes.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        RadioButton chkSel = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
                                                                               // if (chkSel.IsChecked == true) { posDetailsID = Convert.ToInt32(chkSel.Tag); }
                        if (objPosDtls.IMIXConfigurationValueId == Convert.ToInt32(chkSel.Tag)) { chkSel.IsChecked = true; }
                        else { chkSel.IsChecked = false; }
                    }
                }

                for (int i = 0; i < lstLocation.Items.Count; i++)
                {
                    DependencyObject obj = lstLocation.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        CheckBox chkSel = FindVisualChild<CheckBox>(obj);
                        //            string []strLocatinIDs = strLocationIDs.Split(',');
                        //            for (int locid = 0; locid > strLocatinIDs.Length; locid++)
                        //   
                        chkSel.IsChecked = false;

                        if (chkSel.Content.ToString() == objPosDtls.DG_Location_Name)
                        {
                            chkSel.IsChecked = true;
                        }
                        //            }
                    }
                }

                cmbSystem.Text = objPosDtls.PosName;
                btnSaveLocation.Content = "Update";
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this camera?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    int PosLocationId = ((Button)sender).Tag.ToInt32();
                    _PosID = PosLocationId;
                    Save(_PosID, 2);

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public void BindLocationWisetype(int selectID)
        {
            objLocatinlst = new LocationWiseSettingBusiness();

            int LocationID = Convert.ToInt32(cmbLocation.SelectedValue);
            List<LocationTypeSettingModel> Locationlist = objLocatinlst.GetLocationTypeSettings(LocationID);
            try
            {
                for (int i = 0; i < lstLocation.Items.Count; i++)
                {
                    DependencyObject obj = lstLocation.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        CheckBox chkSel = FindVisualChild<CheckBox>(obj);
                        chkSel.IsChecked = false;

                    }
                }
                lstTypes.Items.Clear();
            }
            catch (Exception ex) { }
            lstTypes.ItemsSource = Locationlist;

        }
        public void BindLocationForSystem()
        {

            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = stoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                //lstLocationList.Add(0, "All Locations");

                lstLocation.ItemsSource = locSubstoreBased;

            }

            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }


        }
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
        where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }


        public void GetSystemName(long SubStoreID, int LocationID) //bind combo
        {
            try
            {
                //  _Level = RunLevel;

                _SubStoreID = Config.SubStoreId;
                if (SubStoreID > 0) { _SubStoreID = SubStoreID; }
                //
                //if(CmbPOSDetail.Items.Count>0)
                //{

                //CmbPOSDetail.Items.Clear();
                //}
                // CmbPOSDetail.SelectionChanged += CmbPOSDetail_SelectionChanged;
                List<ImixPOSDetail> imixPOSDetail = new List<ImixPOSDetail>();
                ServicePosInfoBusiness svcPOSBusiness = new ServicePosInfoBusiness();
                imixPOSDetail = svcPOSBusiness.GetPOSDetailByLocation(_SubStoreID, LocationID);
                //FrameworkHelper.CommonUtility.BindComboWithSelect<ImixPOSDetail>(CmbPOSDetail, imixPOSDetail, "SystemName", "iMixPOSDetailID", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                //CmbPOSDetail.SelectedIndex = 0;

                cmbSystem.ItemsSource = imixPOSDetail;
                cmbSystem.DisplayMemberPath = "SystemName";
                cmbSystem.SelectedValuePath = "iMixPOSDetailID";

            }
            catch (Exception ex)
            {

                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void btnSaveLocation_Click(object sender, RoutedEventArgs e)
        {
            if (btnSaveLocation.Content == "Update")
            { Save(_PosID, 1); btnSaveLocation.Content = "Save"; }
            else { Save(0, 0); }

        }
        public void Save(int PosID, int ChangeType)
        {
            LocationWiseSettingBusiness objLocation = new LocationWiseSettingBusiness();
            int posDetailsID = 0;
            int isValid = 0;
            bool isActive = true;
            string locationIDs = string.Empty;
            string posName = string.Empty;
            if (ChangeType != 2)
            {
                if (string.IsNullOrEmpty(cmbSystem.Text))
                {
                    isValid = 1;
                }
            }
            if (isValid == 0)
            {
                if (ChangeType != 2)
                {
                    if (lstLocationList.Count > 0)
                    {
                        for (int i = 0; i < lstTypes.Items.Count; i++)
                        {
                            DependencyObject obj = lstTypes.ItemContainerGenerator.ContainerFromIndex(i);
                            if (obj != null)
                            {
                                RadioButton chkSel = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
                                if (chkSel.IsChecked == true) { posDetailsID = Convert.ToInt32(chkSel.Tag); posName = chkSel.Content.ToString(); }
                            }
                        }

                        for (int i = 0; i < lstLocation.Items.Count; i++)
                        {
                            DependencyObject obj = lstLocation.ItemContainerGenerator.ContainerFromIndex(i);
                            if (obj != null)
                            {
                                CheckBox chkSel = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                                if (chkSel.IsChecked == true)
                                {
                                    if (locationIDs.Length <= 0) { locationIDs = chkSel.Tag.ToString(); }
                                    else
                                    {
                                        locationIDs = locationIDs + "," + chkSel.Tag;
                                    }
                                }
                            }
                        }

                        if (posDetailsID <= 0) { MessageBox.Show("Kindly select the settings!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error); return; }

                        if (string.IsNullOrEmpty(locationIDs)) { MessageBox.Show("Kindly select the Location!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error); return; }

                    }
                }

                if (ChangeType == 2) isActive = false;
                int returnVal = objLocation.AddEditPosLocationAssociation(PosID, Convert.ToString(cmbSystem.Text), locationIDs, posDetailsID, isActive);
                if (returnVal > 0)
                {
                    if (ChangeType == 0)
                    {
                        MessageBox.Show("Location wise Setting Saved successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (ChangeType == 1)
                    {
                        MessageBox.Show("Location wise Setting Edited successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (ChangeType == 2)
                    {
                        MessageBox.Show("Location wise Setting Deleted successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    clear();
                }
                else if (returnVal == -3)
                { MessageBox.Show(posName + " is already exists in selected location, kindly change the setting and try again!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error); }
                else { MessageBox.Show("Error occured kindly try again after some time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error); }

            }

            else { MessageBox.Show("Kindly select the System Name!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error); }
        }
        public void GetLocationSettings()
        {
            try
            {

                objLocatinlst = new LocationWiseSettingBusiness();
                List<PosLocationAssociationModel> Locationlist = objLocatinlst.GetPosLocationAssociationList(0, Convert.ToInt32(cmbLocation.SelectedValue));
                try
                {
                    dgLocationSett.Items.Clear();
                }
                catch { }
                dgLocationSett.ItemsSource = Locationlist;

            }
            catch (Exception ex)
            {

                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }


        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            clear();
        }
        public void clear()
        {
            BindLocationWisetype(0);
            GetSystemName(0, 0);
            GetLocationSettings();
            if (btnSaveLocation.Content == "Update")
            { btnSaveLocation.Content = "Save"; }
        }
        private void chkShowSubstorePhotos_Click(object sender, RoutedEventArgs e)
        {
            if (chkShowSubstorePhotos.IsChecked == true)
            {
                chkShowCheckedSubstorePhotos.IsChecked = false;
            }
        }

        private void chkShowCheckedSubstorePhotos_Click(object sender, RoutedEventArgs e)
        {
            if (chkShowCheckedSubstorePhotos.IsChecked == true)
            {
                chkShowSubstorePhotos.IsChecked = false;
            }
        }
        /// <summary>
        /// //////////created by latika for PresoldSerivice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkPresoldActive_Checked(object sender, RoutedEventArgs e)
        {
            IsPresoldDelay.Visibility = Visibility.Visible;
        }

        private void chkPresoldActive_Unchecked(object sender, RoutedEventArgs e)
        {
            IsPresoldDelay.Visibility = Visibility.Hidden;
            chkisDelayPreSold.IsChecked = false;
            txtDelayTimePresold.Text = "0";
        }

        private void chkisDelayPreSold_Unchecked(object sender, RoutedEventArgs e)
        {
            txtDelayTimePresold.Text = "0";
        }

        private void txtDelayTimePresold_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (chkisDelayPreSold.IsChecked == false)
            //{
            //    MessageBox.Show("Kindly check is delay time before adding daley time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            //}
            if (chkisDelayPreSold.IsChecked == true && (!string.IsNullOrEmpty(txtDelayTimePresold.Text)))
            {
                int delayval = Convert.ToInt32(txtDelayTimePresold.Text);
                if (delayval > 540)
                {
                    MessageBox.Show("Delay time should not be greater then 540min!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtDelayTimePresold.Text = "540";
                }
                if (delayval < 1)
                {
                    MessageBox.Show("Delay time should be greater then 0 min!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    //txtDelayTimePresold.Text = "540";
                }
            }
        }
        /////End by latika
    }



}
