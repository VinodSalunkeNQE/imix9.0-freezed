﻿using DigiConfigUtility.Utility;
using DigiPhoto;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Text.RegularExpressions;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for OpenCloseFormSettings.xaml
    /// </summary>
    public partial class OpenCloseFormSettings : UserControl
    {
        public OpenCloseFormSettings()
        {
            InitializeComponent();
            FillSubstore();
        }

        private void btnSearchSubStore_Click(object sender, RoutedEventArgs e)
        {
            GetOpeningClosingData();
        }
        private void FillSubstore()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                Dictionary<string, string> lstSubStore = new Dictionary<string, string>();
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                var list = stoBiz.GetSubstoreData();
                lstSubStore.Add("-Select-", "0");
                foreach (var item in list)
                {
                    lstSubStore.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
                }
                cmbSelectSubstore.ItemsSource = lstSubStore;
                cmbSelectSubstore.SelectedValue = Config.SubStoreId.ToString();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strResult = new InputBox("Kindly mention the reason for deleting.").ShowDialog();
                // if (MessageBox.Show("Do you want to delete this camera?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                if (strResult.Length > 1)
                {
                    StoreSubStoreDataBusniess StoreBuss = new StoreSubStoreDataBusniess();

                    OpeningClosingDeleteinfo objOPinfo = new OpeningClosingDeleteinfo();

                    string strVal = ((Button)sender).Tag.ToString();
                    string[] str = strVal.Split('~');
                    objOPinfo.TypeID = Convert.ToInt32(str[0]);
                    objOPinfo.FormID = Convert.ToInt32(str[1]);
                    objOPinfo.Reason = strResult;
                    objOPinfo.SubStoreID = Convert.ToInt32(cmbSelectSubstore.SelectedValue);
                    objOPinfo.MachineID = Environment.MachineName;
                    objOPinfo.UserName = Environment.UserName;

                    string RetMsg = StoreBuss.OpeningClosingDelete(objOPinfo);

                    MessageBox.Show(RetMsg);
                    GetOpeningClosingData();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        public void GetOpeningClosingData()
        {

            StoreSubStoreDataBusniess StoreBuss = new StoreSubStoreDataBusniess();
            List<OpeningCloseSettinginfo> _objOpeningCloseSettingdetail = StoreBuss.GetOpeningClosingDeletedData(Convert.ToInt32(cmbSelectSubstore.SelectedValue));
            if (_objOpeningCloseSettingdetail != null)
            {
                //foreach (var item in _objOpeningCloseSettingdetail)
                //{
                //    item.ErrorMessage = string.IsNullOrEmpty(item.ErrorMessage) ? string.Empty : item.FlushId.ToString();
                //    switch (item.Status)
                //    {
                //        case 1:
                //            item.StrStatus = "Completed";
                //            break;
                //        case 2:
                //            item.StrStatus = "Failed";
                //            break;
                //        case 3:
                //            item.StrStatus = "Scheduled";
                //            break;
                //        case 4:
                //            item.StrStatus = "Inprogress ";
                //            break;
                //        default:
                //            break;
                //    }
                //}
                //_objFlushHistorydetail = _objFlushHistorydetail.OrderByDescending(t => t.ScheduleFlushdate.HasValue).OrderByDescending(t => t.ScheduleFlushdate).ToList();
            }
            DGOpenCloseData.ItemsSource = _objOpeningCloseSettingdetail;

        }

        public class InputBox
        {

            Window Box = new Window();//window for the inputbox

            FontFamily font = new FontFamily("Tahoma");//font for the whole inputbox
            int FontSize = 12;//fontsize for the input
            StackPanel sp1 = new StackPanel();// items container
            string title = "Message Box";//title as heading
            string boxcontent;//title
            string defaulttext = "";//default textbox content
            string errormessage = "Kindly provide the Reason.";//error messagebox content
            string errortitle = "Errore";//error messagebox heading title
            string okbuttontext = "Submit";//Ok button content

            Brush BoxBackgroundColor = Brushes.LightGray;// Window Background
            Brush InputBackgroundColor = Brushes.White;// Textbox Background
            bool clicked = false;
            TextBox input = new TextBox();
            Button ok = new Button();
            bool inputreset = false;

            public InputBox(string content)
            {
                try
                {
                    boxcontent = content;
                }
                catch { boxcontent = "Error!"; }
                windowdef();
            }

            public InputBox(string content, string Htitle, string DefaultText)
            {
                try
                {
                    boxcontent = content;
                }
                catch { boxcontent = "Error!"; }
                try
                {
                    title = Htitle;
                }
                catch
                {
                    title = "Error!";
                }
                try
                {
                    defaulttext = DefaultText;
                }
                catch
                {
                    DefaultText = "Error!";
                }
                windowdef();
            }

            public InputBox(string content, string Htitle, string Font, int Fontsize)
            {
                try
                {
                    boxcontent = content;
                }
                catch { boxcontent = "Error!"; }
                try
                {
                    font = new FontFamily(Font);
                }
                catch { font = new FontFamily("Tahoma"); }
                try
                {
                    title = Htitle;
                }
                catch
                {
                    title = "Error!";
                }
                if (Fontsize >= 1)
                    FontSize = Fontsize;
                windowdef();
            }

            private void windowdef()// window building - check only for window size
            {
                Box.Name = "MessageBoxshow";
                Box.Height = 300;// Box Height
                Box.Width = 300;// Box Width
                Box.Background = BoxBackgroundColor;
                Box.Title = title;
                Box.Content = sp1;
                Box.Closing += Box_Closing;
                TextBlock content = new TextBlock();
                content.TextWrapping = TextWrapping.Wrap;
                content.Background = null;
                content.HorizontalAlignment = HorizontalAlignment.Center;
                content.VerticalAlignment = VerticalAlignment.Center;
                content.Text = boxcontent;
                content.FontFamily = font;
                content.FontSize = FontSize;
                content.Margin = new Thickness(20, 40, 20, 5);
                sp1.Children.Add(content);

                input.Background = InputBackgroundColor;
                input.FontFamily = font;
                input.FontSize = FontSize;
                input.HorizontalAlignment = HorizontalAlignment.Center;
                input.Text = defaulttext;
                input.TextWrapping = TextWrapping.Wrap;
                input.MinWidth = 200;
                input.MinHeight = 100;
                input.MaxHeight = 100;
                input.MaxWidth = 200;
                input.Margin = new Thickness(5, 5, 5, 5);
                input.MouseEnter += input_MouseDown;
                sp1.Children.Add(input);
                ok.Width = 70;
                ok.Height = 30;
                ok.Click += ok_Click;
                ok.Content = okbuttontext;
                ok.HorizontalAlignment = HorizontalAlignment.Center;
                sp1.Children.Add(ok);

            }

            void Box_Closing(object sender, System.ComponentModel.CancelEventArgs e)
            {
                if (!clicked)
                {

                    e.Cancel = true;
                    input.Text = "";
                    Box.Visibility = Visibility.Hidden;


                }


            }

            private void input_MouseDown(object sender, MouseEventArgs e)
            {
                if ((sender as TextBox).Text == defaulttext && inputreset == false)
                {
                    (sender as TextBox).Text = null;
                    inputreset = true;
                }
            }

            void ok_Click(object sender, RoutedEventArgs e)
            {
                clicked = true;
                if (input.Text == defaulttext || input.Text == "")
                    MessageBox.Show(errormessage, errortitle);
                else
                {
                    Box.Close();
                }
                clicked = false;
            }

            public string ShowDialog()
            {
                Box.ShowDialog();
                return input.Text;
            }
        }

    }
}
