﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.Common;
using System.IO;
using System.Xml;
using DigiPhoto.DataLayer.Model;
using MPLATFORMLib;
using System.Xml.Linq;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for VideoConfig.xaml
    /// </summary>
    /// 

    public partial class VideoConfig : UserControl
    {
        ConfigBusiness objConfigBL = new ConfigBusiness();
        long profileId = 0;
        Dictionary<string, string> lstProductList = new Dictionary<string, string>();
        Dictionary<string, int> lstProfileList = new Dictionary<string, int>();
        Dictionary<string, string> lstAudioBitRate = new Dictionary<string, string>();
        Dictionary<string, string> lstVideoBitRate = new Dictionary<string, string>();
        List<VideoConfigProfiles> lstVideoProfiles = new List<VideoConfigProfiles>();
        List<VideoProducts> lstVideoProduct = new List<VideoProducts>();
        string autoProVideoBackground = string.Empty;
        Dictionary<int, string> lstLocationList;
        string layeringString = string.Empty;
        MLiveClass m_objLive;
        MWriterClass m_objWriter;
        List<long> VideoConfigList;
        List<ConfigurationInfo> lstConfigurationInfo = new List<ConfigurationInfo>();
        System.ComponentModel.BackgroundWorker bw_DeleteSettings = new System.ComponentModel.BackgroundWorker();
        string configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
        public VideoConfig()
        {
            try
            {
                InitializeComponent();

                FillProductCombo();
                FillLocationCombo();
                FillBitRate();
                SetVideoConfigMasterId();
                AutoVideoSceneSettingsControlsBox.SetParent(this);
                //bindCudaCode();
                //Load Config settings              

                m_objLive = new MLiveClass();
                m_objWriter = new MWriterClass();
                mFormatControl.SetControlledObject(m_objLive);
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
                mFormatControl.comboBoxAudio.SelectedIndex = 8;

                mConfigList1.SetControlledObject(m_objWriter);
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
                //Nvidia Check
                if (GetNVIDIAGraphicFlag() == true)
                {
                    chkMP4CUDA.IsEnabled = true;
                }
                else
                {
                    chkMP4CUDA.IsEnabled = false;
                    chkMP4CUDA.IsChecked = false;
                }
                if (cmbLocation.SelectedIndex == 0)
                {
                    stkActiveForAutomatedFlow.IsEnabled = false;
                    stkActiveForVideo.IsEnabled = false;
                }

                GetVideoAutoProcessingConfigSettings();
                //SetVideoLength(VideoTemplateControlBox.videoSlotlist);

                GetAllSubstoreConfigdata();
                bw_DeleteSettings.DoWork += bw_DeleteSettings_DoWork;
                bw_DeleteSettings.RunWorkerCompleted += bw_DeleteSettings_RunWorkerCompleted;

                AutoVideoSceneSettingsControlsBox.ExecuteMethod += AutoVideoSceneSettingsControlsBox_ExecuteMethod;
                int profileId = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoProfileId) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoProfileId]) : 1;
                //LoadFramesData(FrameData);
                loadProfiles(profileId);

                // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
                chkIsOnlyMagicShot.Visibility = Visibility.Collapsed;
                if (chkIsGreenScreenFlow.IsChecked == true)
                {
                    chkIsOnlyMagicShot.Visibility = Visibility.Visible;
                }
                else
                {
                    chkIsOnlyMagicShot.Visibility = Visibility.Collapsed;
                }

                if (chkEnabledVidAutoProcessing.IsChecked == true)
                {
                    chkIsGreenScreenFlow.Visibility = Visibility.Visible;
                }
                else
                {
                    chkIsGreenScreenFlow.Visibility = Visibility.Collapsed;
                }
                //*** End Here ***//

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void FillBitRate()
        {
            //Fill Audio Bit Rate
            lstAudioBitRate.Clear();
            lstAudioBitRate.Add("256K", "256K");
            lstAudioBitRate.Add("384K", "384K");
            lstAudioBitRate.Add("512K", "512K");
            cmbAudioBitRate.ItemsSource = lstAudioBitRate.Keys;
            cmbAudioBitRate.SelectedIndex = 0;

            //Fill Video Bit Rate
            lstVideoBitRate.Clear();
            lstVideoBitRate.Add("1M", "1M");
            lstVideoBitRate.Add("2M", "2M");
            lstVideoBitRate.Add("5M", "5M");
            lstVideoBitRate.Add("8M", "8M");
            lstVideoBitRate.Add("16M", "16M");
            lstVideoBitRate.Add("30M", "30M");
            lstVideoBitRate.Add("35M", "35M");
            cmbVideoBitRate.ItemsSource = lstVideoBitRate.Keys;
            cmbVideoBitRate.SelectedIndex = 5;
        }

        private void LoadBitrates(string tempconf)
        {
            string[] allConfig = tempconf.Split(' '); string audioRate = string.Empty; string videoRate = string.Empty;
            foreach (var _allItem in allConfig)
            {
                if (_allItem.Contains("audio::bitrate"))
                {
                    audioRate = _allItem.Split('=')[1];
                    cmbAudioBitRate.SelectedValue = audioRate;
                }
                if (_allItem.Contains("video::bitrate"))
                {
                    videoRate = _allItem.Split('=')[1];
                    cmbVideoBitRate.SelectedValue = videoRate;
                }
            }
            
        }

        private string UpdateBitRates()
        {
            string audioRate, videoRate, configuration = string.Empty;
            audioRate = cmbAudioBitRate.SelectedValue.ToString();
            videoRate = cmbVideoBitRate.SelectedValue.ToString();
            configuration = "audio::bitrate=" + audioRate + " " + "video::bitrate=" + videoRate + " " + "video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
            return configuration;
        }
        private void SetVideoConfigMasterId()
        {
            VideoConfigList = new List<long>();
            VideoConfigList.Add(Convert.ToInt64(ConfigParams.IsAutoVideoFrameExtEnabled));
            VideoConfigList.Add(Convert.ToInt64(ConfigParams.VideoFrameCount));
            VideoConfigList.Add(Convert.ToInt64(ConfigParams.VideoFramePositions));
            VideoConfigList.Add(Convert.ToInt64(ConfigParams.VideoFrameCropRatio));
        }

        private void AutoVideoSceneSettingsControlsBox_ExecuteMethod(object sender, EventArgs e)
        {
            grpWS.Visibility = Visibility.Visible;
            AutoVideoSceneSettingsControlsBox.ExecuteMethod -= AutoVideoSceneSettingsControlsBox_ExecuteMethod;
        }

        public static int aIndex = 0;
        VideoSceneViewModel videoScene = new VideoSceneViewModel();
        VideoScene vScene = new VideoScene();
        VideoSceneObject vSceneObject = new VideoSceneObject();
        VideoObjectFileMapping vObjectFileMapping = new VideoObjectFileMapping();
        VideoSceneBusiness business;
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //Button btnSender = (Button)sender;
            //profileId = Convert.ToInt32(btnSender.Tag);
            bool isSceneSaved = false;

            //previous code
            try
            {
                if (IsValidEntry())
                {
                    //update profile value if changed
                    string item = comboProfiles.SelectedItem.ToString();
                    KeyValuePair<string, int> varSelectedItem = (KeyValuePair<string, int>)comboProfiles.SelectedItem;
                    if (!string.IsNullOrEmpty(varSelectedItem.Key))
                        profileId = lstProfileList[varSelectedItem.Key];
                    if (profileId > 0)
                    {
                        isSceneSaved = SaveUpdateMixerScene((int)profileId);

                    }
                    else
                    {
                        //if (chkEnabledVidAutoProcessing.IsChecked == true)
                        //{
                        MessageBox.Show("Please select profile!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                        //}
                    }

                    //if (IsValidEntry())
                    //{
                    if (ConfigManager.SubStoreId <= 0)
                    {
                        MessageBox.Show("Please configure your SubStore.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    string restartEngineAfterCount = string.Empty;
                    bool isMP4CUDA = chkMP4CUDA.IsChecked == true ? true : false;
                    bool isVideoEdit = chkIsEnabledVideoEditing.IsChecked == true ? true : false;
                    bool isVideoAutoProcessing = chkEnabledVidAutoProcessing.IsChecked == true ? true : false;
                    bool IsGreenScreenFlow = chkIsGreenScreenFlow.IsChecked == true ? true : false;
                    // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
                    bool IsOnlyMagicShot = chkIsOnlyMagicShot.IsChecked == true ? true : false;
                    // *** End Here ***//
                    bool IsAutoWorkflow = chkIsAutoWorkFlow.IsChecked == true ? true : false;
                    string memoryThresholdPercent = txtThresholdPercent.Text.Trim();

                    bool IsSaved = SaveConfigData(isVideoEdit, isMP4CUDA, isVideoAutoProcessing, profileId, IsGreenScreenFlow, IsOnlyMagicShot, IsAutoWorkflow, memoryThresholdPercent);

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                //throw;
            }
            loadProfiles(profileId);
            grdProfileName.Visibility = Visibility.Collapsed;
            btnCreateProfile.Visibility = Visibility.Visible;
        }

        private bool SaveConfigData(bool IsEnabledVideoEdit, bool isMP4CUDA, bool IsEnableAutoVidProcessing, long profileId, bool IsGreenScreenFlow, bool IsOnlyMagicShot, bool IsAutoWorkflow, string memoryThresholdPercent)
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                objConfigBL = new ConfigBusiness();



                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsEnabledVideoEdit.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnabledVideoEdit;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = isMP4CUDA.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ApplyMP4CUDA;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsEnableAutoVidProcessing.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEnableAutoVidProcessing;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsGreenScreenFlow.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsGreenScreenFlow;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsOnlyMagicShot.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsOnlyMagicShot;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);
                // *** End Here ***//

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = memoryThresholdPercent;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsAppMemoryOverflow;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsAutoWorkflow.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.AutomatedVideoEditWorkFlow;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = GetWriterSettings();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.VideoWriterSettings;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                bool IsSaved = objConfigBL.SaveUpdateNewConfig(objConfigList);
                return IsSaved;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
                throw;
            }
        }

        private string GetWriterSettings()
        {
            string AudioFormat = mFormatControl.comboBoxAudio.SelectedItem.ToString().Replace("<", "").Replace(">", "");
            string VideoFormat = mFormatControl.comboBoxVideo.SelectedItem.ToString().Replace("<", "").Replace(">", ""); ;
            string MOutputFormat = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text;
            string AudioCodec = string.Empty;
            string VideoCodec = string.Empty;
            configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
            if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text != null)
                AudioCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text;
            if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items.Count > 2)
                VideoCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text;
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            if (!(strConfig.Length > 20))
                strConfig = configSettings;
            return getSettingXml(AudioFormat, VideoFormat, MOutputFormat, AudioCodec, VideoCodec, strConfig);
        }
        private string getSettingXml(string AudioFormat, string VideoFormat, string OutputFormat, string AudioCodec, string VideoCodec, string strConfig)
        {
            string outSettingXML = string.Empty;
            string VideoSelectedIndex = mFormatControl.comboBoxVideo.SelectedIndex.ToString();
            string AudioSelectedIndex = mFormatControl.comboBoxAudio.SelectedIndex.ToString();
            outSettingXML += "<Settings>";
            outSettingXML += "<VideoResolution>";
            outSettingXML += VideoFormat;
            outSettingXML += "</VideoResolution>";
            outSettingXML += "<VideoResolutionSelectedIndex>";
            outSettingXML += VideoSelectedIndex;
            outSettingXML += "</VideoResolutionSelectedIndex>";
            outSettingXML += "<AudioResolution>";
            outSettingXML += AudioFormat;
            outSettingXML += "</AudioResolution>";
            outSettingXML += "<AudioResolutionSelectedIndex>";
            outSettingXML += AudioSelectedIndex;
            outSettingXML += "</AudioResolutionSelectedIndex>";
            outSettingXML += "<OutputFormat>";
            outSettingXML += OutputFormat;
            outSettingXML += "</OutputFormat>";
            outSettingXML += "<AudioCodec>";
            outSettingXML += AudioCodec;
            outSettingXML += "</AudioCodec>";
            outSettingXML += "<VideoCodec>";
            outSettingXML += VideoCodec;
            outSettingXML += "</VideoCodec>";
            outSettingXML += "<strConfig>";
            outSettingXML += UpdateBitRates();//strConfig;
            outSettingXML += "</strConfig>";
            outSettingXML += "</Settings>";
            return outSettingXML;

        }

        private void ReadSettingsXml(string Settings)
        {
            try
            {
                string VideoSetting = Settings;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(VideoSetting);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {

                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OutputFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText;
                        string Audiocodec = node.ChildNodes[5].InnerText;
                        if (node.ChildNodes.Count >= 6)
                            configSettings = node.ChildNodes[7].InnerText;
                        if(!string.IsNullOrEmpty(configSettings))
                            LoadBitrates(configSettings);
                        mFormatControl.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl.comboBoxAudio.SelectedIndex = Audioid;
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OutputFormat;//o/p format
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec
                        if (Videocodec != "")
                        {
                            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private bool IsValidEntry()
        {
            try
            {

                if (cmbLocation.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select location!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }

                if (string.IsNullOrEmpty(txtVideoLength.Text) || Convert.ToInt32(txtVideoLength.Text.Trim()) <= 0)
                {
                    txtVideoLength.Text = "10";
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
                throw;

            }
        }

        private bool IsNumeric(string text)
        {
            try
            {
                int output;
                return int.TryParse(text, out output);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
                throw;
            }
        }
        private void GetConfigSettings()//VideoConfigProfiles vcp)
        {
            try
            {
                chkEnabledVidAutoProcessing.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsEnableAutoVidProcessing) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsEnableAutoVidProcessing] : false.ToString());
                chkIsEnabledVideoEditing.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsEnabledVideoEdit) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsEnabledVideoEdit] : false.ToString());
                chkMP4CUDA.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ApplyMP4CUDA) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.ApplyMP4CUDA] : false.ToString());
                chkFlipAspectRatio.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.FlipVidAspectRatio) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.FlipVidAspectRatio] : false.ToString());
                chkIsGreenScreenFlow.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsGreenScreenFlow) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsGreenScreenFlow] : false.ToString());
                // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
                chkIsOnlyMagicShot.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsOnlyMagicShot) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsOnlyMagicShot] : false.ToString());
                //***End Here ***//
                // txtAppMemOverflow.Text = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsAppMemoryOverflow) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsAppMemoryOverflow] : "100";
                chkIsAutoWorkFlow.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutomatedVideoEditWorkFlow) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutomatedVideoEditWorkFlow] : false.ToString());
                txtThresholdPercent.Text = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsAppMemoryOverflow) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsAppMemoryOverflow] : "25";
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }

        private void btnSetToDefault_Click(object sender, RoutedEventArgs e)
        {
            comboProfiles.SelectedIndex = 1;
        }

        private void changeProfiles(int i)
        {
            VideoSceneViewModel prof = new VideoSceneViewModel();
            objConfigBL = new ConfigBusiness();
            prof = objConfigBL.GetVideoConfigProfileList(i, ConfigManager.SubStoreId).FirstOrDefault();
            if (prof != null)
            {
                comboProfiles.SelectedItem = prof.VideoScene.Name;
                chkIsActiveForVideoProcessing.IsChecked = prof.VideoScene.IsActiveForAdvanceProcessing;
                chkIsVerticalVideo.IsChecked = prof.VideoScene.IsVerticalVideo;
                cmbLocation.SelectedValue = prof.VideoScene.LocationId;
                txtVideoLength.Text = prof.VideoScene.VideoLength.ToString();
                ReadSettingsXml(prof.VideoScene.Settings);
                //SetAutoVideoEffects(prof.AutoVideoEffects);
                
            }

        }
        private void loadProfiles(long profileId)
        {
            VideoSceneViewModel selectedProfile = new VideoSceneViewModel();
            try
            {
                objConfigBL = new ConfigBusiness();
                List<VideoSceneViewModel> lstprofiles = objConfigBL.GetVideoConfigProfileList(0, ConfigManager.SubStoreId);

                selectedProfile = lstprofiles.Where(o => o.VideoScene.SceneId == profileId).FirstOrDefault();
                comboProfiles.ItemsSource = null;
                lstProfileList.Clear();
                lstProfileList.Add("-Select-", 0);
                foreach (VideoSceneViewModel profile in lstprofiles.OrderBy(o => o.VideoScene.SceneId))
                {
                    if (!(lstProfileList.Where(o => o.Key == profile.VideoScene.Name).Count() > 0))
                        lstProfileList.Add(profile.VideoScene.Name, Convert.ToInt32(profile.VideoScene.SceneId));
                }
                comboProfiles.ItemsSource = lstProfileList;//.Keys;
                VideoSceneViewModel lstprofile = null;
                lstprofile = lstprofiles.Find(x => x.VideoScene.IsActiveForAdvanceProcessing == true);
                if (lstprofile != null)
                    comboProfiles.SelectedValue = lstprofile.VideoScene.SceneId;
                else
                    comboProfiles.SelectedIndex = 0;

                GetConfigSettings();
                //SetAutoVideoEffects(selectedProfile.AutoVideoEffects);

                //return selectedProfile;

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                //throw;
            }

        }
        public bool CheckProfileName(string name)
        {
            VideoSceneBusiness business = new VideoSceneBusiness();
            bool result = business.CheckProfileName(name);

            if (result)
            {
                MessageBox.Show("Profile name already exists.Please enter diffrent profile name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            return true;
        }


        # region other function
        private void comboProfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboProfiles.Items.Count > 0)
            {

                if (comboProfiles.SelectedIndex > 0)
                {
                    btnDeleteProfile.Visibility = Visibility.Visible;
                    //DisableControls();
                    KeyValuePair<string, int> varSelectedItem = (KeyValuePair<string, int>)comboProfiles.SelectedItem;
                    //ComboBoxItem typeItem = (ComboBoxItem)comboProfiles.SelectedItem;
                    //string value = typeItem.Content.ToString();

                    int i = lstProfileList[varSelectedItem.Key];
                    changeProfiles(i);
                }
                else
                {
                    btnDeleteProfile.Visibility = Visibility.Collapsed;
                    ResetControls();

                }
                //else
                //{
                //    EnableControls();
                //    int i = lstProfileList[varSelectedItem.Key];
                //    changeProfiles(i);
                //}

            }



        }
        private void btnCreateProfile_Click(object sender, RoutedEventArgs e)
        {
            btnSaveProf.Visibility = Visibility.Collapsed;
            //comboProfiles.IsEnabled = false;
            grdProfileName.Visibility = Visibility.Visible;
            btnCreateProfile.Visibility = Visibility.Collapsed;
            EnableControls();
            comboProfiles.SelectedIndex = 0;

        }
        private void btnSaveProfile_Click(object sender, RoutedEventArgs e)
        {
            if (IsValidEntry())
            {
                //if (comboProfiles.SelectedIndex > 0)
                //    profileId = comboProfiles.SelectedIndex;
                //else
                //    profileId = 0;
                if (String.IsNullOrEmpty(txtProfileName.Text))
                {
                    MessageBox.Show("Please enter profile name", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                if (!CheckProfileName(txtProfileName.Text))
                    return;
                SaveUpdateMixerScene(0);
                  string destFile = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", txtProfileName.Text);
                  if (!System.IO.Directory.Exists(destFile))
                  {
                      Directory.CreateDirectory(destFile);
                  }
                txtProfileName.Text = string.Empty;
                loadProfiles(profileId);
                btnSaveProf.Visibility = Visibility.Visible;
                grdProfileName.Visibility = Visibility.Collapsed;
                btnCreateProfile.Visibility = Visibility.Visible;
            }

        }

        private bool SaveUpdateMixerScene(int sceneID)
        {
            bool isSaved = false;
            bool result = false;
            try
            {
                if (sceneID == 0)
                {
                    result = Checkprofilestatus(true);
                    string _sceneName = txtProfileName.Text.Trim();
                    if (!string.IsNullOrEmpty(_sceneName))
                    {
                        string sceneFolderPath = System.IO.Path.Combine("Profiles", _sceneName);
                        vScene.SceneId = sceneID;
                        vScene.Name = _sceneName;
                        vScene.IsMixerScene = true;
                        vScene.CG_ConfigID = AutoVideoSceneSettingsControlsBox.CgConfigId;
                        vScene.IsActive = true;
                        vScene.ScenePath = System.IO.Path.Combine(sceneFolderPath, _sceneName + ".xml");
                        vScene.VideoLength = Convert.ToInt32(txtVideoLength.Text);
                        vScene.LocationId = Convert.ToInt32(cmbLocation.SelectedValue);
                        vScene.Settings = GetWriterSettings();
                        vScene.IsActiveForAdvanceProcessing = (bool)chkIsActiveForVideoProcessing.IsChecked;
                        vScene.IsVerticalVideo = (bool)chkIsVerticalVideo.IsChecked;
                        // business.SaveVideoSceneObject(streamIDs.Remove(streamIDs.LastIndexOf(',')));
                        vSceneObject.VideoObjectId = "Guest_Video";
                        //vSceneObject.GuestVideoObject = Convert.ToBoolean(chkGuestVideoObject.IsChecked);
                        vObjectFileMapping.ChromaPath = System.IO.Path.Combine(sceneFolderPath, _sceneName + "_Chroma.xml");
                        //vObjectFileMapping.RoutePath = sceneFolderPath + _sceneName + "_Chroma.xml";
                        videoScene.VideoScene = vScene;
                        videoScene.VideoSceneObject = vSceneObject;
                        videoScene.VideoObjectFileMapping = vObjectFileMapping;
                    }
                }
                else
                {

                    result = Checkprofilestatus(false);
                    vScene.SceneId = sceneID;
                    vScene.IsMixerScene = true;
                    vScene.CG_ConfigID = AutoVideoSceneSettingsControlsBox.CgConfigId;
                    vScene.IsActive = true;
                    vScene.VideoLength = Convert.ToInt32(txtVideoLength.Text);
                    vScene.LocationId = Convert.ToInt32(cmbLocation.SelectedValue);
                    vScene.IsActiveForAdvanceProcessing = (bool)chkIsActiveForVideoProcessing.IsChecked;
                    vScene.IsVerticalVideo = (bool)chkIsVerticalVideo.IsChecked;
                    vScene.Settings = GetWriterSettings();
                    videoScene.VideoScene = vScene;
                    videoScene.VideoSceneObject = vSceneObject;
                    videoScene.VideoObjectFileMapping = vObjectFileMapping;
                }

                business = new VideoSceneBusiness();
                if (result)
                    isSaved = business.SaveMixerScene(videoScene);
                chkIsActiveForVideoProcessing.IsChecked = false;
                //chkIsVerticalVideo.IsChecked = false;
                if (isSaved)
                    MessageBox.Show("Profile saved successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
            return isSaved;
        }

        string DeleteFileName = string.Empty;
        private void btnDeleteProfile_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                //profileId = Convert.ToInt32(btnSender.Tag);

                profileId = Convert.ToInt64(comboProfiles.SelectedValue);
                if (comboProfiles.SelectedItem.ToString() == "Default")
                {
                    MessageBox.Show("Default profile can not be deleted!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                MessageBoxResult response = MessageBox.Show("Do you want to delete profile?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (response == MessageBoxResult.Yes)
                {
                    KeyValuePair<string, int> varSelectedItem = (KeyValuePair<string, int>)comboProfiles.SelectedItem;
                    //string item = comboProfiles.SelectedItem.ToString();
                    profileId = lstProfileList[varSelectedItem.Key];

                    ConfigBusiness objConfigBuss = new ConfigBusiness();
                    //VideoSceneViewModel obj = objConfigBuss.GetVideoConfigProfileList(profileId);
                    //if (obj != null)
                    //{

                    if (objConfigBuss.DeleteProfile(profileId))
                    {
                        MessageBox.Show("Profile deleted successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        string SourcePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", comboProfiles.Text);
                        DeleteFileName = comboProfiles.Text;
                        if (Directory.Exists(SourcePath))
                        {
                            var dir = new DirectoryInfo(SourcePath);
                            dir.Delete(true);
                            bs.Show();
                            bw_DeleteSettings.RunWorkerAsync();
                        }
                    }

                    //}
                }
                else
                    return;
                // getVideoConfigProfiles();
                loadProfiles(0);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }

        private void DeleteFromAllSubstore(List<ConfigurationInfo> _objConfigInfo, string DeleteFileName)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string strPath = string.Empty;

                        strPath = System.IO.Path.Combine(item.DG_Hot_Folder_Path, "Profiles", DeleteFileName);

                        string destFile = strPath;
                        if (System.IO.Directory.Exists(destFile))
                        {
                            var dir = new DirectoryInfo(destFile);
                            dir.Delete(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void ResetControls()
        {
            txtVideoLength.Text = string.Empty;
            chkIsActiveForVideoProcessing.IsChecked = false;
            chkIsVerticalVideo.IsChecked = false;
            mFormatControl.comboBoxVideo.SelectedIndex = 12;
            mFormatControl.comboBoxAudio.SelectedIndex = 8;
            cmbVideoBitRate.SelectedIndex = 5;
            cmbAudioBitRate.SelectedIndex = 0;
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtProfileName.Text = string.Empty;
            btnCreateProfile.Visibility = Visibility.Visible;
            grdProfileName.Visibility = Visibility.Collapsed;
            btnSaveProf.Visibility = Visibility.Visible;
            comboProfiles.IsEnabled = true;
            if (comboProfiles.SelectedIndex == 1)
            {
                DisableControls();
            }
        }
        private void DisableControls()
        {

            btnDeleteProfile.IsEnabled = false;


        }
        private void EnableControls()
        {

            btnDeleteProfile.IsEnabled = true;
            comboProfiles.IsEnabled = true;


        }
        # endregion

        private void FillProductCombo()
        {

            lstProductList.Add("-Select-", "0");
            try
            {
                ProcessedVideoBusiness objPV = new ProcessedVideoBusiness();
                lstVideoProduct = objPV.GetVideoPackages();
                foreach (VideoProducts item in lstVideoProduct.OrderBy(o => o.ProductID))
                {
                    if (!(lstProductList.Where(o => o.Key == item.ProductName).Count() > 0))
                        lstProductList.Add(item.ProductName, item.ProductID.ToString());

                }
                cmbVideoProduct.ItemsSource = lstProductList.Keys;
                cmbVideoProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }
        }
        //Get Auto Video Effects
        //private void SetAutoVideoEffects(string effects)
        //{
        //    if (string.IsNullOrEmpty(effects))
        //        return;
        //    else
        //        parseEffectXml(effects);
        //}

        private void GetVideoAutoProcessingConfigSettings()
        {
            try
            {
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled))
                {
                    chkAutoVidDisplayEnabled.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayEnabled] : false.ToString());
                }
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayFolder))
                {
                    txtAutoVidDisplayFolder.Text = (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayFolder) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayFolder] : string.Empty).ToString();
                }
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.NoOfDisplayScreens))
                {
                    txtNoOfScreens.Text = (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.NoOfDisplayScreens) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.NoOfDisplayScreens] : string.Empty).ToString();
                }
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsCyclicDisplay))
                {
                    chkIsCyclicDisplay.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsCyclicDisplay) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsCyclicDisplay] : false.ToString());
                }
                //if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsAutoVideoFrameExtEnabled))
                //{
                //    chkAutoFrameExtraction.IsChecked = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsAutoVideoFrameExtEnabled) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.IsAutoVideoFrameExtEnabled] : false.ToString());
                //    SetFrameStackVisibility();
                //}
                //if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFrameCount))
                //{
                //    txtFrames.Text = (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFrameCount) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoFrameCount] : string.Empty).ToString();
                //}
                //if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFramePositions))
                //{
                //    FrameData = (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFramePositions) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoFramePositions] : string.Empty).ToString();
                //}

                //if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFrameCropRatio))
                //{
                //    cmbCropRatio.Text = (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoFrameCropRatio) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoFrameCropRatio] : string.Empty).ToString();
                //}
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                throw;
            }

        }
        private void btnSaveAutoVideoConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValidEntry())
                {
                    bool isAutoVidDisplayEnabled = false;
                    string DisplayFolderPath = "";
                    int NoOfScreens = 0;
                    bool IsCyclicDisplay = false;
                    isAutoVidDisplayEnabled = chkAutoVidDisplayEnabled.IsChecked == true ? true : false;
                    if (ConfigManager.SubStoreId <= 0)
                    {
                        MessageBox.Show("Please configure your SubStore.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (string.IsNullOrEmpty(ConfigManager.DigiFolderPath))
                    {
                        MessageBox.Show("Please configure your HotFolder.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    if (chkAutoVidDisplayEnabled.IsChecked == true)
                    {
                        if (!IsNumeric(txtNoOfScreens.Text.Trim()))
                        {
                            MessageBox.Show("No of screen should be a numeric value.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        if (chkAutoVidDisplayEnabled.IsChecked == false)
                        {
                            MessageBox.Show("Please select the display videos on display utility.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        if (string.IsNullOrEmpty(txtAutoVidDisplayFolder.Text))
                        {
                            MessageBox.Show("Please browse the display folder.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        DisplayFolderPath = txtAutoVidDisplayFolder.Text;
                        NoOfScreens = Convert.ToInt32(txtNoOfScreens.Text);
                        IsCyclicDisplay = chkIsCyclicDisplay.IsChecked == true ? true : false;
                    }

                    if (!string.IsNullOrEmpty(DisplayFolderPath) & NoOfScreens > 0)
                    {
                        createDisplayFolder(DisplayFolderPath, NoOfScreens);
                    }

                    bool IsSaved = SaveAutoProcessingConfigData(isAutoVidDisplayEnabled, DisplayFolderPath, IsCyclicDisplay, NoOfScreens);
                    if (IsSaved)
                    {
                        MessageBox.Show("Settings saved successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        GetVideoAutoProcessingConfigSettings();
                    }
                    else
                        MessageBox.Show("Some problem occured try again!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                //throw;
            }
        }
        private void createDisplayFolder(string DisplayFolderPath, int NoOfScreens)
        {

            try
            {
                for (int i = 1; i <= NoOfScreens; i++)
                {
                    string folder = DisplayFolderPath + "\\Display\\Display" + i.ToString();
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string Marketingfolder = folder + "\\Marketing Videos";
                    if (!Directory.Exists(Marketingfolder))
                    {
                        Directory.CreateDirectory(Marketingfolder);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                // throw;
            }
        }

        private bool SaveAutoProcessingConfigData(bool isAutoVidDisplayEnabled, string DisplayFolderPath, bool IsCyclicDisplay, int NoOfScreens)
        {
            try
            {
                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                objConfigBL = new ConfigBusiness();

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = isAutoVidDisplayEnabled.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.AutoVidDisplayEnabled;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = DisplayFolderPath;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.AutoVidDisplayFolder;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = NoOfScreens.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.NoOfDisplayScreens;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsCyclicDisplay.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsCyclicDisplay;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                objConfigList.Add(ConfigValue);

                bool IsSaved = objConfigBL.SaveUpdateNewConfig(objConfigList);
                return IsSaved;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
                throw;
            }
        }
        private bool SaveAutoFrameExtraction(bool isFrameExtEnabled, int FramesCount, string FramePositions, string cropRatio)
        {
            try
            {
                List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
                iMixConfigurationLocationInfo ConfigValue = null;
                objConfigBL = new ConfigBusiness();

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = isFrameExtEnabled.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsAutoVideoFrameExtEnabled;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                ConfigValue.LocationId = (int)cmbLocation.SelectedValue;

                objConfigList.Add(ConfigValue);

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = FramesCount.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.VideoFrameCount;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                ConfigValue.LocationId = (int)cmbLocation.SelectedValue;

                objConfigList.Add(ConfigValue);

                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = FramePositions.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.VideoFramePositions;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                ConfigValue.LocationId = (int)cmbLocation.SelectedValue;

                objConfigList.Add(ConfigValue);


                ConfigValue = new iMixConfigurationLocationInfo();
                ConfigValue.ConfigurationValue = cropRatio;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.VideoFrameCropRatio;
                ConfigValue.SubstoreId = ConfigManager.SubStoreId;
                ConfigValue.LocationId = (int)cmbLocation.SelectedValue;

                objConfigList.Add(ConfigValue);
                bool IsSaved = objConfigBL.SaveUpdateConfigLocation(objConfigList);
                return IsSaved;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
                throw;
            }
        }
        private string SaveVideoBGDetails(string videoBGDisplayname, out int BGId)
        {
            string videoBGName = ""; BGId = 0;
            try
            {
                Guid _obj = Guid.NewGuid();
                videoBGName = System.IO.Path.GetFileNameWithoutExtension(videoBGDisplayname) + _obj.ToString() + System.IO.Path.GetExtension(videoBGDisplayname);
                string destFile = ConfigManager.DigiFolderPath + "VideoBackGround" + "\\" + videoBGName;
                if (!Directory.Exists(ConfigManager.DigiFolderPath + "VideoBackGround"))
                {
                    Directory.CreateDirectory(ConfigManager.DigiFolderPath + "VideoBackGround");
                }
                if (File.Exists(videoBGDisplayname))
                {
                    File.Copy(videoBGDisplayname, destFile, true);
                }
                VideoBackgroundInfo objVBG = new VideoBackgroundInfo();
                objVBG.DisplayName = System.IO.Path.GetFileName(videoBGDisplayname);
                objVBG.Name = videoBGName;
                objVBG.IsActive = true;
                objVBG.MediaType = 1;
                objVBG.Description = "Uploaded for video auto-processing";
                objVBG.CreatedBy = LoginUser.UserId;
                objVBG.CreatedOn = System.DateTime.Now;
                objVBG.VideoBackgroundId = 0;
                objVBG.ModifiedBy = LoginUser.UserId;
                objVBG.ModifiedOn = System.DateTime.Now;
                ConfigBusiness obj = new ConfigBusiness();
                BGId = obj.SaveUpdateVideoBackground(objVBG);
                return videoBGName;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return videoBGName;
            }
        }

        bool GetNVIDIAGraphicFlag()
        {
            try
            {
                //System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher("Select * from Win32_DisplayConfiguration");
                System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
                //SELECT * FROM Win32_VideoController Description
                foreach (System.Management.ManagementObject mo in searcher.Get())
                {

                    foreach (System.Management.PropertyData property in mo.Properties)
                    {
                        //if (property.Name == "DeviceName")
                        if (property.Name == "Description")
                        {
                            if (property.Value.ToString().Contains("NVIDIA"))
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
            }
        }

        List<TemplateListItems> AudioLst = new List<TemplateListItems>();


        private void btnBrowseDisplayPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (chkAutoVidDisplayEnabled.IsChecked == true)
                {
                    System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                    var result = fbDialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        txtAutoVidDisplayFolder.Text = fbDialog.SelectedPath.Replace(@"\", @"\\") + "\\\\";
                    }
                }
                else
                {
                    MessageBox.Show("Please check display videos on display utility.");
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = stoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                lstLocationList.Add(0, "-Select Location-");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name);
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private void btnVideoWriterSettings_Click(object sender, RoutedEventArgs e)
        {
            mFormatControl.comboBoxVideo.SelectedIndex = 12;
            mFormatControl.comboBoxAudio.SelectedIndex = 8;

            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
            cmbVideoBitRate.SelectedIndex = 5;
            cmbAudioBitRate.SelectedIndex = 0;
        }

        private void winMConfigListl_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {
        }

        private void btnVideoPreview_Click(object sender, RoutedEventArgs e)
        {
            if (comboProfiles.SelectedIndex > 0)
            {
                KeyValuePair<string, int> varSelectedItem = (KeyValuePair<string, int>)comboProfiles.SelectedItem;
                AutoVideoSceneSettingsControlsBox.SceneName = varSelectedItem.Key;
                AutoVideoSceneSettingsControlsBox.ExecuteMethod += AutoVideoSceneSettingsControlsBox_ExecuteMethod;
                AutoVideoSceneSettingsControlsBox.SceneID = varSelectedItem.Value;
                AutoVideoSceneSettingsControlsBox.IsVerticalVideo =(bool)chkIsVerticalVideo.IsChecked ? true : false;

                grpWS.Visibility = Visibility.Hidden;
                AutoVideoSceneSettingsControlsBox.ShowHandlerDialog();
                OuterBorder.IsEnabled = false;


            }
            else
                MessageBox.Show("Please select profile.");
        }
        string a;

        private void txtVideoLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        public bool Checkprofilestatus(bool status)
        {
            string SceneName = String.Empty;
            int locationId = Convert.ToInt32(cmbLocation.SelectedValue);
            int SceneId = Convert.ToInt32(comboProfiles.SelectedValue);
            business = new VideoSceneBusiness();
            //*****Start Updated by Ajay for multiple profile magic shot**//
            SceneName = business.checkIsActiveForVideoProcessing(locationId, SceneId);
            //***End***//
            if (status) // case run for save new profile
            {
                if (chkIsActiveForVideoProcessing.IsChecked == true)
                {
                    if (SceneName == null || SceneName == "")
                        chkIsActiveForVideoProcessing.IsChecked = true;
                    //commented for multiple profile.
                    //else
                    //{
                    //    MessageBox.Show("There is already an active profile " + SceneName + " for this location.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    chkIsActiveForVideoProcessing.IsChecked = false;

                    //}
                }
                return true;
            }
            else // case run for update the existing profile
            {
                if (chkIsActiveForVideoProcessing.IsChecked == true)
                {
                    //commented for the multiple profile 
                    //if (SceneName != comboProfiles.Text && SceneName != "")
                    //{
                    //    MessageBox.Show("There is already an active profile " + SceneName + " for this location.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    chkIsActiveForVideoProcessing.IsChecked = false;
                    //    return false;
                    //}
                    //else
                    return true;
                }
                return true;
            }

        }
        private void chkIsActiveForVideoProcessing_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbLocation.SelectedIndex == 0)
            {
                stkActiveForAutomatedFlow.IsEnabled = false;
                stkActiveForVideo.IsEnabled = false;
                chkIsActiveForVideoProcessing.IsChecked = false;
                chkIsVerticalVideo.IsChecked = false;
                ResetVideoConfig();
            }
            else
            {
                stkActiveForAutomatedFlow.IsEnabled = true;
                stkActiveForVideo.IsEnabled = true;
                ReadVideoConfig();
            }
        }
        List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();

        private void ReadVideoConfig()
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(Config.SubStoreId);
            int selectedLocationId = Convert.ToInt32(((System.Collections.Generic.KeyValuePair<int, string>)(cmbLocation.SelectedItem)).Key);
            List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == selectedLocationId && VideoConfigList.Contains(n.IMIXConfigurationMasterId)).ToList();

            if (lstLocation.Count > 0)
            {
                foreach (var loc in lstLocation)
                {
                    switch (loc.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.VideoFrameCropRatio:
                            cmbCropRatio.Text = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.IsAutoVideoFrameExtEnabled:
                            chkAutoFrameExtraction.IsChecked = loc.ConfigurationValue == "True" ? true : false;
                            SetFrameStackVisibility();
                            break;

                        case (int)ConfigParams.VideoFrameCount:
                            txtFrames.Text = loc.ConfigurationValue;
                            break;

                        case (int)ConfigParams.VideoFramePositions:
                            FrameData = loc.ConfigurationValue;
                            LoadFramesData(FrameData);
                            break;
                    }
                }

            }
            else
                ResetVideoConfig();

        }

        private void ResetVideoConfig()
        {
            cmbCropRatio.SelectedIndex = 0;
            txtFrames.Text = string.Empty;
            chkAutoFrameExtraction.IsChecked = false;
            lstFrames.ItemsSource = null;
        }

        private void bw_DeleteSettings_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
        }
        BusyWindow bs = new BusyWindow();
        private void bw_DeleteSettings_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DeleteFromAllSubstore(lstConfigurationInfo, DeleteFileName);
        }
        private void GetAllSubstoreConfigdata()
        {
            try
            {
                lstConfigurationInfo = (new ConfigBusiness()).GetAllSubstoreConfigdata();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }
        List<FrameClass> lstframesclass = new List<FrameClass>();
        private void txtFrames_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumeric(txtFrames.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(txtFrames.Text) && Convert.ToInt32(txtFrames.Text.Trim()) > 0)
                {
                    stkFramesPos.Visibility = Visibility.Visible;
                    if (lstframesclass.Count <= 0)
                    {
                        for (int i = 0; i < Convert.ToInt32(txtFrames.Text.Trim()); i++)
                        {
                            FrameClass fc = new FrameClass();
                            fc.Count = (i + 1).ToString();// +". ";
                            fc.Position = (i + 1).ToString();
                            lstframesclass.Add(fc);
                        }
                    }
                    else if (Convert.ToInt32(txtFrames.Text.Trim()) > lstframesclass.Count)
                    {
                        for (int i = lstframesclass.Count; i < Convert.ToInt32(txtFrames.Text.Trim()); i++)
                        {
                            FrameClass fc = new FrameClass();
                            fc.Count = (i + 1).ToString();// +". ";
                            fc.Position = (i + 1).ToString();
                            lstframesclass.Add(fc);
                        }
                    }
                    else if (Convert.ToInt32(txtFrames.Text.Trim()) < lstframesclass.Count)
                    {
                        int currentcount = lstframesclass.Count;
                        for (int i = lstframesclass.Count; i > Convert.ToInt32(txtFrames.Text.Trim()); i--)
                        {
                            lstframesclass.RemoveAt(lstframesclass.Count - 1);
                        }
                    }
                    lstFrames.ItemsSource = null;
                    lstFrames.ItemsSource = lstframesclass;
                }
                else
                {
                    stkFramesPos.Visibility = Visibility.Collapsed;
                    lstframesclass.Clear();
                    lstFrames.ItemsSource = null;
                    lstFrames.ItemsSource = lstframesclass;
                }
            }
            else if (string.IsNullOrWhiteSpace(txtFrames.Text))
            {
                //txtFrames.Text = "0";
                stkFramesPos.Visibility = Visibility.Collapsed;
            }
            else
            {
                txtFrames.Text = "0";
                stkFramesPos.Visibility = Visibility.Collapsed;
            }
        }

        private void chkAutoFrameExtraction_Click(object sender, RoutedEventArgs e)
        {
            SetFrameStackVisibility();
        }

        private void SetFrameStackVisibility()
        {
            if (chkAutoFrameExtraction.IsChecked == true)
            {
                stkFrames.Visibility = Visibility.Visible;
                btnResetFrames.Visibility = Visibility.Visible;
            }
            else
            {
                stkFrames.Visibility = Visibility.Collapsed;
                btnResetFrames.Visibility = Visibility.Collapsed;
            }
        }
        string FrameData = string.Empty;
        private void LoadFramesData(string FrameData)
        {
            try
            {
                lstframesclass.Clear();
                if (!string.IsNullOrEmpty(FrameData))
                {
                    XmlDocument xdc = new XmlDocument();
                    xdc.LoadXml(FrameData);
                    XmlNodeList nodes = xdc.GetElementsByTagName("Frames");
                    int count = 0;
                    foreach (XmlNode xn in nodes[0].ChildNodes)
                    {
                        FrameClass fc = new FrameClass();
                        fc.Count = (count + 1).ToString();
                        fc.Position = xn.InnerText;
                        lstframesclass.Add(fc);
                        count++;
                    }
                    txtFrames.Text = lstframesclass.Count.ToString();
                    lstFrames.ItemsSource = null;
                    lstFrames.ItemsSource = lstframesclass;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSaveFrame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateFrames())
                {
                    int TotalFrames = 0;
                    string strFramePosition = string.Empty;
                    bool IsAutoFrameExtraction = chkAutoFrameExtraction.IsChecked == true ? true : false;
                    if (!string.IsNullOrEmpty(txtFrames.Text))
                    {
                        TotalFrames = Convert.ToInt32(txtFrames.Text);
                        strFramePosition = WriteFramePositionXML(lstframesclass);                       
                    }
                    string cropRatio = cmbCropRatio.Text;
                    bool IsSaved = SaveAutoFrameExtraction(IsAutoFrameExtraction, TotalFrames, strFramePosition, cropRatio);
                    if (IsSaved)
                    {
                        MessageBox.Show("Settings saved successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        GetVideoAutoProcessingConfigSettings();
                    }
                    else
                        MessageBox.Show("Some problem occured try again!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private bool ValidateFrames()
        {
            if (cmbLocation.SelectedIndex == 0)
            {
                MessageBox.Show("Please select location");
                return false;
            }
           if (chkAutoFrameExtraction.IsChecked == true && string.IsNullOrEmpty(txtFrames.Text))
           {
               MessageBox.Show("No. of Frame can't be blank!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
               return false;
           }
           if (chkAutoFrameExtraction.IsChecked == true && (lstframesclass.Count == 0 || txtFrames.Text == "0"))
            {
                MessageBox.Show("No. of Frame can't be zero!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else
                foreach (var item in lstframesclass)
                {
                    if (!string.IsNullOrEmpty(item.Position.Trim().ToString()))
                    {
                        if (IsDouble(item.Position.Trim().ToString()))
                        {
                            foreach (var item1 in lstframesclass)
                            {
                                if (item.Position == item1.Position && item.Count != item1.Count)
                                {
                                    MessageBox.Show("Duplicate frame exists!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid frame exists!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Frame position can't be blank!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }

            return true;
        }
        private string WriteFramePositionXML(List<FrameClass> lstframes)
        {
            string strXML = string.Empty;
            strXML = "<Frames>" + "\n";
            foreach (var items in lstframes)
            {
                strXML += "<frame no='" + items.Count.Trim().Replace(". ", "") + "'>" + items.Position.Trim() + "</frame>" + "\n";
            }
            strXML += "</Frames>" + "\n";
            return strXML;
        }

        private void btnResetFrames_Click(object sender, RoutedEventArgs e)
        {
            lstframesclass.Clear();
            txtFrames.Text = "0";
            txtFrames_TextChanged(null, null);
            cmbCropRatio.SelectedIndex = 0;

        }
        private bool IsDouble(string value)
        {
            try
            {
                double price;
                bool isDouble = Double.TryParse(value, out price);
                if (isDouble)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            { return false; }

        }

        private void chkIsVerticalVideo_Checked(object sender, RoutedEventArgs e)
        {
            Handle(sender as CheckBox);
        }
        private void chkIsVerticalVideo_Unchecked(object sender, RoutedEventArgs e)
        {
            Handle(sender as CheckBox);
        }
        void Handle(CheckBox checkBox)
        {
            if ((bool)checkBox.IsChecked)
                mFormatControl.comboBoxVideo.Enabled = false;
            else
                mFormatControl.comboBoxVideo.Enabled = true;
        }

        // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
        private void chkEnabledVidAutoProcessing_Checked(object sender, RoutedEventArgs e)
        {
            if (chkEnabledVidAutoProcessing.IsChecked == true)
            {
                chkIsGreenScreenFlow.Visibility = Visibility.Visible;
            }
            else
            {
                chkIsGreenScreenFlow.Visibility = Visibility.Collapsed;
            }
        }

        private void chkEnabledVidAutoProcessing_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chkEnabledVidAutoProcessing.IsChecked == false)
            {
                chkIsGreenScreenFlow.IsChecked = false;
                chkIsGreenScreenFlow.Visibility = Visibility.Collapsed;
            }
            else
            {
                chkIsGreenScreenFlow.Visibility = Visibility.Visible;
            }
        }

        private void chkIsGreenScreenFlow_Checked(object sender, RoutedEventArgs e)
        {
            if (chkEnabledVidAutoProcessing.IsChecked == false)
            {
                chkIsGreenScreenFlow.IsChecked = false;
                chkIsGreenScreenFlow.Visibility = Visibility.Collapsed;
            }
            else
            {
                chkIsOnlyMagicShot.Visibility = Visibility.Visible;
            }
        }

        private void chkIsGreenScreenFlow_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chkIsGreenScreenFlow.IsChecked == false)
            {
                chkIsOnlyMagicShot.IsChecked = false;
                chkIsOnlyMagicShot.Visibility = Visibility.Collapsed;
            }
            else
            {
                chkIsOnlyMagicShot.Visibility = Visibility.Visible;
            }
        }
        //*** End Here ***//
    }

    class FrameClass
    {
        public string Count { get; set; }
        public string Position { get; set; }
    }
}
