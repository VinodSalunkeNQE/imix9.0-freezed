﻿using DigiConfigUtility.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Business;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
using System.ServiceProcess;
using System.Diagnostics;
using System.Management;
using System.IO;


namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for ServicesInformation.xaml
    /// </summary>
    public partial class ServicesPopup : System.Windows.Controls.UserControl
    {
        public long _ServiceId;
        public string _ServicePath;
        public bool _isService;
        public long _ServicePosMappingId;
        public long _SubStoreID;
        private UIElement _parent;
        private bool _result = false;
        string _SystemName;
        public string _ServiceName;
        int _Level;
        string UniqueCodes = string.Empty;
        BusyWindow bs = new BusyWindow();



        public ServicesPopup()
        {
            InitializeComponent();
            btnStopService.IsEnabled = false;
            btnStartService.IsEnabled = false;
            Visibility = Visibility.Hidden;


        }

        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        public bool ShowPanHandlerDialog() 
        {
            Visibility = Visibility.Visible;
            //  _parent.IsEnabled = false;
            return _result;
        }
        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
             //   _parent.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        //fill grid after 
        public void GetServiceInfo(long ServiceId, long ImixPOSDetailID, long SubStoreID, string ServiceName, string SubStoreName,bool isService,string ServicePath)
        {
            try
            {
                _ServicePath = ServicePath;
                _isService = isService;
                _ServiceName = ServiceName;
                _ServiceId = ServiceId;
                txtSvcName.Text = ServiceName;
                txtSubStoreName.Text = SubStoreName;
                List<SvcRunningInfo> svcRunningInfo = new List<SvcRunningInfo>();
                ServicePosInfoBusiness svcPOSBusiness = new ServicePosInfoBusiness();
                svcRunningInfo = svcPOSBusiness.GetServiceInfoBusiness(ServiceId, ImixPOSDetailID, SubStoreID);

               

                if (svcRunningInfo.Count > 0)
                {
                    CmbPOSDetail.Text = svcRunningInfo[0].SystemName;
                   _SystemName = svcRunningInfo[0].SystemName;
                    _ServicePosMappingId = svcRunningInfo[0].ServicePOSMappingID;
                }

                if (string.IsNullOrEmpty(_SystemName))
                {
                    btnStartService.IsEnabled = false;
                    btnStopService.IsEnabled = false;
                }
                else
                {
                    btnStartService.IsEnabled = false;
                    btnStopService.IsEnabled = true;
                }
                //if (svcRunningInfo[0].Status == "Running")
                //{
                //    btnStopService.IsEnabled = true;
                //}
                //else
                //{
                //    btnStartService.IsEnabled = false;
                //}

                grdServiceInfo.ItemsSource = null;
                
                grdServiceInfo.ItemsSource = svcRunningInfo;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public void GetPOSDetail(long SubStoreID, int RunLevel) //bind combo
        {
            try
            {
                _Level = RunLevel;
                _SubStoreID = SubStoreID;
                //
                //if(CmbPOSDetail.Items.Count>0)
                //{

                //CmbPOSDetail.Items.Clear();
                //}
               // CmbPOSDetail.SelectionChanged += CmbPOSDetail_SelectionChanged;
                List<ImixPOSDetail> imixPOSDetail = new List<ImixPOSDetail>();
                ServicePosInfoBusiness svcPOSBusiness = new ServicePosInfoBusiness();
                imixPOSDetail = svcPOSBusiness.GetPOSDetailBusiness(SubStoreID, RunLevel);
                //FrameworkHelper.CommonUtility.BindComboWithSelect<ImixPOSDetail>(CmbPOSDetail, imixPOSDetail, "SystemName", "iMixPOSDetailID", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                //CmbPOSDetail.SelectedIndex = 0;

                CmbPOSDetail.ItemsSource = imixPOSDetail;
                CmbPOSDetail.DisplayMemberPath = "SystemName";
                CmbPOSDetail.SelectedValuePath = "iMixPOSDetailID";



            }
            catch (Exception ex)
            {

                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void btnStartService_Click(object sender, RoutedEventArgs e)
        {
            
            string checkService = string.Empty;
            ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
            string serviceName = _ServiceName;
            string machineName = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).SystemName;
            long ImixPosDetailId = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).ImixPOSDetailID;
           
            try
            {
                if (_isService)
                {
                    ServiceController service = new ServiceController(serviceName, machineName);
                    String Current_status = service.Status.ToString();


                    checkService = svcPosInfoBusiness.CheckRunnignServiceBusiness(_ServiceId, _SubStoreID, _Level, machineName);
                    if (checkService != "")
                    {
                        if (Current_status == "Running")
                        {
                            MessageBox.Show("This Service Already Manually Running On The Selected System Kindly Stopped It Manually On The System ");
                        }
                        else
                            MessageBox.Show("This service already started on" + checkService + " first stop service and then try to start", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    else
                    {
                        service.Start();
                        bs.Show();
                        service.WaitForStatus(ServiceControllerStatus.Running);
                        MessageBox.Show("Service has started successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                       bs.Hide();
                        GetServiceInfo(_ServiceId, ImixPosDetailId, _SubStoreID, _ServiceName, txtSubStoreName.Text, _isService, _ServicePath);
                        btnStartService.IsEnabled = false;
                        btnStopService.IsEnabled = true;                      
                    }
                }
                else
                {
                   
                    String Current_status="";


                    foreach (Process clsProcess in Process.GetProcesses())
                    {
                        if (clsProcess.ProcessName.Contains(serviceName))
                        {
                            Current_status = "Running";
                        }
                        else
                        {
                            Current_status = "Stopped";     
                        }
                    }

                    checkService = svcPosInfoBusiness.CheckRunnignServiceBusiness(_ServiceId, _SubStoreID, _Level,machineName);
                    if (checkService != "")
                    {
                        if (Current_status == "Running")
                        {
                            MessageBox.Show("This Application Already Manually Running On The Selected System Kindly Stopped It Manually On The System ");
                        }
                        else
                            MessageBox.Show("This Application Already Started on" + checkService + " First Stop Application And Then Try To Start", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("This is an exe so you will have to start manually", "Information", MessageBoxButton.OK, MessageBoxImage.Information);                                               
                    }
                   
                }
               
            }
            catch (Exception ex)
            {
                //status = ServiceControllerStatus.Stopped;
                MessageBox.Show(ex.Message);
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void btnStopService_Click(object sender, RoutedEventArgs e)
        {
           ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
            string serviceName = _ServiceName;           
            string machineName = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).SystemName;
            
            long ImixPosDetailId = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).ImixPOSDetailID;                       
           ServiceControllerStatus status = ServiceControllerStatus.Stopped;
            try
            {
              
                if (_isService)
                {
                    ServiceController service = new ServiceController(serviceName, machineName);
                    status = service.Status;
                    service.Stop();
                    bs.Show();
                    service.WaitForStatus(ServiceControllerStatus.Stopped);
                    MessageBox.Show("Service has been stopped successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    bs.Hide();
                    GetServiceInfo(_ServiceId, ImixPosDetailId, _SubStoreID, _ServiceName, txtSubStoreName.Text, _isService, _ServicePath);
                    btnStartService.IsEnabled = true;
                    btnStopService.IsEnabled = false;
                    
                }
                else
                {
                    MessageBox.Show("Please stop "+serviceName+ " manually ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);


                }
              
            }
            catch (Exception ex)
            {
              status = ServiceControllerStatus.Stopped;
                MessageBox.Show(ex.Message);
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);               
            }




        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            grdServiceInfo.ItemsSource = null;
            HideHandlerDialog();
            OnExecuteMethod();
            CmbPOSDetail.SelectionChanged -= CmbPOSDetail_SelectionChanged;
            if (CmbPOSDetail.ItemsSource != null)
                CmbPOSDetail.ItemsSource = null;
            CmbPOSDetail.SelectionChanged += CmbPOSDetail_SelectionChanged;
            _SystemName = string.Empty;
            ((Services)_parent).SelectList();
      
        }
        long _id;
        private void CmbPOSDetail_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
                ComboBox cmb = sender as ComboBox;
                long imixposid = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).ImixPOSDetailID;

                UniqueCodes = _ServiceName + "," + ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).SystemName;
                LoadPopupGrid(_ServiceId, imixposid, _SubStoreID);

                //if (_SystemName == null)
                //{
                //    btnStartService.IsEnabled = false;
                //    btnStopService.IsEnabled = true;
                //}
                //else
                //{

                if (((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).SystemName == _SystemName)
                {
                    btnStartService.IsEnabled = false;
                    btnStopService.IsEnabled = true;
                }

                else
                {
                    btnStartService.IsEnabled = true;
                    btnStopService.IsEnabled = false;
                }
          //handle = !cmb.IsDropDownOpen;

        }

        public void LoadPopupGrid(long ServiceId, long ImixPOSDetailID, long SubStoreID)
        {
            try
            {
                List<SvcRunningInfo> svcRunningInfo = new List<SvcRunningInfo>();
                ServicePosInfoBusiness svcPOSBusiness = new ServicePosInfoBusiness();
                svcRunningInfo = svcPOSBusiness.GetServiceInfoBusiness(ServiceId, ImixPOSDetailID, SubStoreID);
                grdServiceInfo.ItemsSource = null;

                grdServiceInfo.ItemsSource = svcRunningInfo;
            }
            catch(Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
 
            }
        }

        private void btnStopfrmDB_Click(object sender, RoutedEventArgs e)
        {
            
            try
            {
                MessageBoxResult result = MessageBox.Show("This Operation will forcefully stop this service(s) on the selected system. please confirm ?", "Delete Fom DB", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    Button btnSender = (Button)sender;
                    // GetSubstoreDetails();
                    long ImixPosDetailId = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).ImixPOSDetailID;
                    ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
                    servicePosBusiness.StartStopServiceByPosIdBusiness(_ServiceId, 0, ImixPosDetailId, false, "Webusers");
                    MessageBox.Show("Data Updated Successfully");

                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                MessageBox.Show("Some error occured");
            }

        }

        


    }
}
