﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
using System.Xml;
using DigiPhoto.IMIX.Business;
using System.Net;
using System.IO;
using DigiPhoto.Common;
using DigiConfigUtility.Utility;
using MPLATFORMLib;
using MControls;
//using MPLATFORMLib;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility;
using System.Linq;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for AdvanceVideoSettings.xaml
    /// </summary>
    public partial class AdvanceVideoSettings : UserControl
    {

        #region Declaration
        List<ConfigurationInfo> lstConfigurationInfo = new List<ConfigurationInfo>();
        Dictionary<string, string> lstAudioBitRate = new Dictionary<string, string>();
        Dictionary<string, string> lstVideoBitRate = new Dictionary<string, string>();
        VideoSceneViewModel sceneViewMode = null;
        string a;
        VideoSceneBusiness business = null;
        Dictionary<int, string> lstLocationList = null;
        public MLiveClass m_objLive;
        ListViewEx listex;
        public MWriterClass m_objWriter;
        int? Scene_Id = 0;
        int _videoObjId = 0;
        bool guestObject = false;
        string StreamPath = string.Empty;
        string RoutePath = string.Empty;
        string guestText = string.Empty;
        string a1 = string.Empty;
        ConfigBusiness configBusiness;
        System.ComponentModel.BackgroundWorker bw_CopySettings = new System.ComponentModel.BackgroundWorker();
        System.ComponentModel.BackgroundWorker bw_DeleteSettings = new System.ComponentModel.BackgroundWorker();
        string destPath = string.Empty;
        string configSettings = string.Empty;
        string _sceneName = string.Empty;
        #endregion
        public AdvanceVideoSettings()
        {
            try
            {
                InitializeComponent();
                BindFlipModes();
                FillBitRate();
                GetAllSubstoreConfigdata();
                m_objLive = new MLiveClass();
                m_objWriter = new MWriterClass();
                mFormatControl.SetControlledObject(m_objLive);
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
                mFormatControl.comboBoxAudio.SelectedIndex = 8;

                bw_CopySettings.DoWork += bw_CopySettings_DoWork;
                //bw_CopySettings.RunWorkerCompleted += bw_CopySettings_RunWorkerCompleted;
                bw_DeleteSettings.DoWork += bw_DeleteSettings_DoWork;
                bw_DeleteSettings.RunWorkerCompleted += bw_DeleteSettings_RunWorkerCompleted;
                mConfigList1.SetControlledObject(m_objWriter);
                //  string OutputFormat = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text;

                mConfigList1.Width = 380;
                mConfigList1.Columns[0].Width = 80;
                mConfigList1.Columns[1].Width = 300;
                mConfigList1.Height = 100;
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
                (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
                configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
                FillLocationCombo();
                LoadVideoSceneGrid();
                GetConfigLocationData();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void bw_DeleteSettings_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
        }
        BusyWindow bs = new BusyWindow();
        private void bw_DeleteSettings_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

            DeleteFromAllSubstore(lstConfigurationInfo, SceneName);
        }

        private void bw_CopySettings_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            MessageBox.Show("Please enter scene name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void bw_CopySettings_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            CopyToAllSubstore(lstConfigurationInfo, destPath);
        }


        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            ResetProfile();
        }
        public void ResetProfile()
        {

            Scene_Id = 0;
            txtRoutePath.Clear();
            txtSceneName.Clear();
            txtSceneName.IsEnabled = true;
            txtScenePath.Clear();
            txtStreamPath.Clear();
            txtVideoLength.Clear();
            cmbStream.ItemsSource = null;
            txtGuestVideoObject.Clear();
            txtScenePath.IsEnabled = true;
            btnBrowseScenePath.IsEnabled = true;
            chkIsActive.IsChecked = false;
            chkGuestVideoObject.IsChecked = false;
            chkIsActiveForAdvanceProcessing.IsChecked = false;
            chkEnableAudio.IsChecked = false;
            chkIsVerticalVideo.IsChecked = false;
            mFormatControl.comboBoxVideo.SelectedIndex = 12;
            mFormatControl.comboBoxAudio.SelectedIndex = 8;
            cmbVideoBitRate.SelectedIndex = 5;
            cmbAudioBitRate.SelectedIndex = 0;
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
            configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {


            if (cmbLocation.SelectedIndex == 0)
            {
                MessageBox.Show("Please select location.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (string.IsNullOrEmpty(txtSceneName.Text))
            {
                MessageBox.Show("Please enter scene name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (txtSceneName.Text == "")
            {
                MessageBox.Show("Please browse scene file.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (cmbStream.SelectedIndex == 0)
            {
                MessageBox.Show("Please select video stream.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (string.IsNullOrEmpty(txtVideoLength.Text))
            {
                MessageBox.Show("Please enter video length.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (chkGuestVideoObject.IsChecked == false && txtGuestVideoObject.Text == "")
            {
                MessageBox.Show("Please check guest video object.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text == "Auto Select")
            {
                MessageBox.Show("Please select video output codec.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;

            }
            SaveUpdateRecord(Scene_Id);

        }
        private void FillBitRate()
        {
            //Fill Audio Bit Rate
            lstAudioBitRate.Clear();
            lstAudioBitRate.Add("256K", "256K");
            lstAudioBitRate.Add("384K", "384K");
            lstAudioBitRate.Add("512K", "512K");
            cmbAudioBitRate.ItemsSource = lstAudioBitRate.Keys;
            cmbAudioBitRate.SelectedIndex = 0;

            //Fill Video Bit Rate
            lstVideoBitRate.Clear();
            lstVideoBitRate.Add("1M", "1M");
            lstVideoBitRate.Add("2M", "2M");
            lstVideoBitRate.Add("5M", "5M");
            lstVideoBitRate.Add("8M", "8M");
            lstVideoBitRate.Add("16M", "16M");
            lstVideoBitRate.Add("30M", "30M");
            lstVideoBitRate.Add("35M", "35M");
            cmbVideoBitRate.ItemsSource = lstVideoBitRate.Keys;
            cmbVideoBitRate.SelectedIndex = 5;
        }

        private void LoadBitrates(string tempconf)
        {
            string[] allConfig = tempconf.Split(' '); string audioRate = string.Empty; string videoRate = string.Empty;
            foreach (var _allItem in allConfig)
            {
                if (_allItem.Contains("audio::bitrate"))
                {
                    audioRate = _allItem.Split('=')[1];
                    cmbAudioBitRate.SelectedValue = audioRate;
                }
                if (_allItem.Contains("video::bitrate"))
                {
                    videoRate = _allItem.Split('=')[1];
                    cmbVideoBitRate.SelectedValue = videoRate;
                }
            }

        }

        private string UpdateBitRates()
        {
            string audioRate, videoRate, configuration = string.Empty;
            audioRate = cmbAudioBitRate.SelectedValue.ToString();
            videoRate = cmbVideoBitRate.SelectedValue.ToString();
            configuration = "audio::bitrate=" + audioRate + " " + "video::bitrate=" + videoRate + " " + "video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
            return configuration;
        }
        public void LoadVideoSceneGrid()
        {
            try
            {
                List<VideoScene> lstVideoScene = new List<VideoScene>();
                business = new VideoSceneBusiness();
                lstVideoScene = business.GetVideoScene(0);
                grdVideoScene.ItemsSource = lstVideoScene;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }

            //var assemblies = typeof().Assembly.GetReferencedAssemblies();

        }
        string SceneName = string.Empty;
        private void btnDeleteScene_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;

            int? Sceneid = Convert.ToInt32(btnSender.Tag);
            try
            {

                if (Sceneid != null && MessageBox.Show("Do you want to delete this profile ?", "Confirm Delete", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    business = new VideoSceneBusiness();
                    SceneName = business.DeleteVideoSceneDetails(Sceneid);
                    {
                        string PathToDeleteScene = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", SceneName);
                        var dir = new DirectoryInfo(PathToDeleteScene);
                        dir.Delete(true);
                        MessageBox.Show("Profile is deleted successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        LoadVideoSceneGrid();
                        if (txtSceneName.Text == SceneName)
                        {
                            ResetProfile();
                        }
                        bs.Show();
                        bw_DeleteSettings.RunWorkerAsync();

                    }
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }

        public void SaveUpdateRecord(int? SceneId)
        {
            if (SceneId == 0)
            {
                bool result = business.CheckProfileName(txtSceneName.Text);
                if (result)
                {
                    MessageBox.Show("Scene name already exists.Please enter a different scene name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
            }
            #region Declaration
            business = new VideoSceneBusiness();
            VideoSceneViewModel videoScene = new VideoSceneViewModel();
            VideoScene vScene = new VideoScene();
            VideoSceneObject vSceneObject = new VideoSceneObject();
            VideoObjectFileMapping vObjectFileMapping = new VideoObjectFileMapping();
            string SettingXML = string.Empty;
            string AudioCodec = string.Empty;
            string VideoCodec = string.Empty;
            string ScenePath = txtScenePath.Text;
            string RoutePath = txtRoutePath.Text;
            string ChromaPath = txtStreamPath.Text;
            string Route_Path = System.IO.Path.GetFileNameWithoutExtension(RoutePath);
            string Route_PathExtension = System.IO.Path.GetExtension(RoutePath);
            string Chroma_Path = System.IO.Path.GetFileNameWithoutExtension(ChromaPath);
            string destPathForScene = System.IO.Path.Combine("Profiles", txtSceneName.Text);
            destPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath + destPathForScene);
            string destPathtoEdit = System.IO.Path.Combine(ConfigManager.DigiFolderPath);
            string AudioFormat = mFormatControl.comboBoxAudio.SelectedItem.ToString();
            string VideoFormat = mFormatControl.comboBoxVideo.SelectedItem.ToString();
            string OutputFormat = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text;
            bool isEnableAdvanceVideoEdit = chkIsEnabledAdvanceVideoEditing.IsChecked == true ? true : false;
            bool isEnableVerticalVideo = chkIsVerticalVideo.IsChecked == true ? true : false;
            if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text != null)
            {
                AudioCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text;
            }
            if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items.Count > 2)
            {
                VideoCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text;
            }
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            if (!(strConfig.Length > 20))
            {
                strConfig = configSettings;
            }
            #endregion
            //var v = mConfigList1;


            SettingXML = getSettingXml(AudioFormat, VideoFormat, OutputFormat, AudioCodec, VideoCodec, strConfig);

            if (!Directory.Exists(destPath) || SceneId != 0) //case executes for saving a new video file
            {
                if (SceneId == 0)
                {
                    if (!string.IsNullOrEmpty(txtScenePath.Text))
                    {

                        if (System.IO.File.Exists(txtScenePath.Text))
                        {
                            Directory.CreateDirectory(destPath);
                            System.IO.File.Copy(ScenePath, destPath + "\\" + txtSceneName.Text + ".xml", true);//path to copy in directory
                            ScenePath = destPathForScene + "\\" + txtSceneName.Text + ".xml";// path is combined to save in db
                        }
                        else
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;

                        }
                    }

                    else
                    {
                        MessageBox.Show("Please browse scene file.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                    if (!string.IsNullOrEmpty(txtRoutePath.Text))
                    {
                        if (System.IO.File.Exists(txtRoutePath.Text))
                        {
                            string Videoid = getVideoID(txtScenePath.Text);
                            System.IO.File.Copy(RoutePath, destPath + "\\" + "Route-" + Videoid + Route_PathExtension, true);

                            RoutePath = destPathForScene + "\\" + "Route-" + Videoid + Route_PathExtension;
                        }

                        else
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    if (!string.IsNullOrEmpty(txtStreamPath.Text))
                    {
                        if (System.IO.File.Exists(txtStreamPath.Text))
                        {
                            System.IO.File.Copy(ChromaPath, destPath + "\\" + "Chroma-" + cmbStream.Text + ".xml", true);
                            ChromaPath = destPathForScene + "\\" + "Chroma-" + cmbStream.Text + ".xml";
                        }
                        else
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;

                        }
                    }
                }

                else  //case executes while Updating
                {
                    if (!string.IsNullOrEmpty(txtScenePath.Text))
                    {

                        if (System.IO.File.Exists(txtScenePath.Text))
                        {
                            if (txtScenePath.Text != destPath + "\\" + txtSceneName.Text + ".xml")
                            {
                                System.IO.File.Copy(ScenePath, destPath + "\\" + txtSceneName.Text + ".xml", true);//path to copy in directory
                                ScenePath = destPathForScene + "\\" + txtSceneName.Text + ".xml";// path is combined to save in db
                            }
                            else
                            {
                                ScenePath = destPathForScene + "\\" + txtSceneName.Text + ".xml";// path is combined to save in db

                            }
                            // System.IO.File.Copy(destPathtoEdit + ScenePath, destPath + "\\" + txtSceneName.Text + ".xml", true);//path to copy in directory
                            // ScenePath = destPathForScene + "\\" + txtSceneName.Text + ".xml";// path is combined to save in db
                        }
                        else
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }

                    }



                    if (System.IO.File.Exists(txtRoutePath.Text))
                    {
                        string Videoid = getVideoID(txtScenePath.Text);
                        if (txtRoutePath.Text != destPath + "\\" + "Route-" + Videoid + Route_PathExtension)
                        {
                            System.IO.File.Copy(RoutePath, destPath + "\\" + "Route-" + Videoid + Route_PathExtension, true);
                            RoutePath = destPathForScene + "\\" + "Route-" + Videoid + Route_PathExtension;
                        }
                        else
                        {
                            RoutePath = destPathForScene + "\\" + "Route-" + Videoid + Route_PathExtension;

                        }
                        //string Videoid = getVideoID();
                        //System.IO.File.Copy(RoutePath, destPath + "\\" + "Route-" + Videoid + Route_PathExtension, true);

                        //RoutePath = destPathForScene + "\\" + "Route-" + Videoid + Route_PathExtension;
                    }
                    else
                    {
                        if (txtRoutePath.Text != "")
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);

                        }

                    }



                    if (System.IO.File.Exists(txtStreamPath.Text))
                    {
                        if (txtStreamPath.Text != destPath + "\\" + "Chroma-" + cmbStream.Text + ".xml")
                        {
                            System.IO.File.Copy(ChromaPath, destPath + "\\" + "Chroma-" + cmbStream.Text + ".xml", true);
                            ChromaPath = destPathForScene + "\\" + "Chroma-" + cmbStream.Text + ".xml";
                        }
                        else
                        {
                            ChromaPath = destPathForScene + "\\" + "Chroma-" + cmbStream.Text + ".xml";

                        }
                        // System.IO.File.Copy(destPathtoEdit + ChromaPath, destPath + "\\" + "Chroma-" + cmbStream.Text + ".xml", true);

                        //    ChromaPath = destPathForScene + "\\" + "Chroma-" + cmbStream.Text + ".xml";
                    }
                    else
                    {
                        if (txtStreamPath.Text != "")
                        {
                            MessageBox.Show("Incorrect file path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }

                }
            }

            else
            {

                MessageBox.Show("Scene name already exists.Please enter a different scene name.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            vScene.SceneId = (int)SceneId;
            vScene.Name = txtSceneName.Text;
            vScene.IsActive = Convert.ToBoolean(chkIsActive.IsChecked);
            vScene.ScenePath = ScenePath;
            vScene.VideoLength = Convert.ToInt32(txtVideoLength.Text);
            vScene.LocationId = Convert.ToInt32(cmbLocation.SelectedValue);
            vScene.Settings = SettingXML;
            vScene.IsVerticalVideo = isEnableVerticalVideo;
            vScene.IsActiveForAdvanceProcessing = (bool)chkIsActiveForAdvanceProcessing.IsChecked;
            vSceneObject.VideoObject_Pkey = Convert.ToInt32(cmbStream.SelectedValue);
            vSceneObject.GuestVideoObject = Convert.ToBoolean(chkGuestVideoObject.IsChecked);
            vObjectFileMapping.ChromaPath = ChromaPath;
            vObjectFileMapping.RoutePath = RoutePath;
            vObjectFileMapping.StreamAudioEnabled=chkEnableAudio.IsChecked==true?"true":"false";
            videoScene.VideoScene = vScene;
            videoScene.VideoSceneObject = vSceneObject;
            videoScene.VideoObjectFileMapping = vObjectFileMapping;
            
            if ((bool)chkGuestVideoObject.IsChecked)
            {
                txtGuestVideoObject.Text = cmbStream.Text;
            }

            if (business.SaveVideoScene(videoScene))
            {
                MessageBox.Show("Profile is saved successfully.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                LoadVideoSceneGrid();
                _sceneName = txtSceneName.Text;
                txtSceneName.IsEnabled = false;
                btnBrowseScenePath.IsEnabled = false;
                txtScenePath.IsEnabled = false;
                bw_CopySettings.RunWorkerAsync();
                //CopyToAllSubstore(lstConfigurationInfo, destPath);

            }
        }

        private string getSettingXml(string AudioFormat, string VideoFormat, string OutputFormat, string AudioCodec, string VideoCodec, string strConfig)
        {
            string outSettingXML = string.Empty;
            string VideoSelectedIndex = mFormatControl.comboBoxVideo.SelectedIndex.ToString();
            string AudioSelectedIndex = mFormatControl.comboBoxAudio.SelectedIndex.ToString();
            outSettingXML += "<Settings>";
            outSettingXML += "<VideoResolution>";
            outSettingXML += VideoFormat;
            outSettingXML += "</VideoResolution>";
            outSettingXML += "<VideoResolutionSelectedIndex>";
            outSettingXML += VideoSelectedIndex;
            outSettingXML += "</VideoResolutionSelectedIndex>";
            outSettingXML += "<AudioResolution>";
            outSettingXML += AudioFormat;
            outSettingXML += "</AudioResolution>";
            outSettingXML += "<AudioResolutionSelectedIndex>";
            outSettingXML += AudioSelectedIndex;
            outSettingXML += "</AudioResolutionSelectedIndex>";
            outSettingXML += "<OutputFormat>";
            outSettingXML += OutputFormat;
            outSettingXML += "</OutputFormat>";
            outSettingXML += "<AudioCodec>";
            outSettingXML += AudioCodec;
            outSettingXML += "</AudioCodec>";
            outSettingXML += "<VideoCodec>";
            outSettingXML += VideoCodec;
            outSettingXML += "</VideoCodec>";
            outSettingXML += "<strConfig>";
            outSettingXML += UpdateBitRates(); //strConfig;
            outSettingXML += "</strConfig>";
            outSettingXML += "</Settings>";
            return outSettingXML;

        }
        private void btnEditScene_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;

            int Sceneid = Convert.ToInt32(btnSender.Tag);
            try
            {
                if (Sceneid != null)
                {
                    txtScenePath.IsEnabled = false;
                    btnBrowseScenePath.IsEnabled = false;
                    VideoSceneViewModel objSceneViewMode = new VideoSceneViewModel();
                    VideoObjectFileMapping objVFM = new VideoObjectFileMapping();
                    txtSceneName.IsEnabled = false;
                    business = new VideoSceneBusiness();

                    objSceneViewMode = business.GetVideoSceneToEdit(Sceneid);
                    txtSceneName.Text = objSceneViewMode.VideoScene.Name;
                    txtScenePath.Text = ConfigManager.DigiFolderPath + objSceneViewMode.VideoScene.ScenePath;
                    cmbLocation.SelectedValue = objSceneViewMode.VideoScene.LocationId;
                    chkIsActiveForAdvanceProcessing.IsChecked = objSceneViewMode.VideoScene.IsActiveForAdvanceProcessing;

                    //cmbStream.ItemsSource = objSceneViewMode.ListVideoSceneObject;

                    BindVideoStreamCombo(Sceneid);
                    _videoObjId = objSceneViewMode.ListVideoSceneObject.Where(x => x.GuestVideoObject == true).FirstOrDefault().VideoObject_Pkey;

                    cmbStream.SelectedValue = _videoObjId;

                    txtGuestVideoObject.Text = cmbStream.Text;
                    a1 = txtGuestVideoObject.Text;
                    objVFM = business.GetobjectVideoDetails(_videoObjId);
                    string settxtstream = objVFM.ChromaPath;
                    string settxtRoute = objVFM.RoutePath;
                    chkEnableAudio.IsChecked = objVFM.StreamAudioEnabled=="true"?true:false;
                    if (settxtstream != "")
                    {
                        txtStreamPath.Text = ConfigManager.DigiFolderPath + objVFM.ChromaPath;

                    }
                    else
                    {
                        txtStreamPath.Text = "";
                    }

                    if (settxtRoute != "")
                    {
                        txtRoutePath.Text = ConfigManager.DigiFolderPath + objVFM.RoutePath;

                    }
                    else
                    {
                        txtRoutePath.Text = "";
                    }
                    StreamPath = txtStreamPath.Text;
                    RoutePath = txtRoutePath.Text;
                    guestObject = objSceneViewMode.ListVideoSceneObject.Where(x => x.GuestVideoObject == true).Select(x => x.GuestVideoObject).FirstOrDefault();
                    if (guestObject == false)
                    {
                        chkGuestVideoObject.IsChecked = false;
                    }
                    else
                    {
                        chkGuestVideoObject.IsChecked = true;
                    }
                    Scene_Id = Sceneid;
                    //int a = SelectComboStream();
                    //if (a == 0)
                    //    cmbStream.SelectedIndex = 0;
                    //else
                    //    cmbStream.SelectedValue = a;

                    txtVideoLength.Text = objSceneViewMode.VideoScene.VideoLength.ToString();
                    chkIsVerticalVideo.IsChecked = objSceneViewMode.VideoScene.IsVerticalVideo;
                    chkIsActive.IsChecked = objSceneViewMode.VideoScene.IsActive;
                    string Settings = objSceneViewMode.VideoScene.Settings;
                    ReadSettingsXml(Settings);

                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }

        private void ReadSettingsXml(string Settings)
        {
            string VideoSetting = Settings;
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(VideoSetting);
            XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
            if (nodes.Count > 0)
            {
                foreach (XmlNode node in nodes)
                {

                    int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                    int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                    string OutputFormat = node.ChildNodes[4].InnerText;
                    string Videocodec = node.ChildNodes[6].InnerText;
                    string Audiocodec = node.ChildNodes[5].InnerText;
                    if (node.ChildNodes.Count >= 6)
                        configSettings = node.ChildNodes[7].InnerText;
                    if (!string.IsNullOrEmpty(configSettings))
                        LoadBitrates(configSettings);
                    mFormatControl.comboBoxVideo.SelectedIndex = Videoid;
                    mFormatControl.comboBoxAudio.SelectedIndex = Audioid;
                    (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OutputFormat;//o/p format
                    (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec
                    if (Videocodec != "")
                    {
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec;
                    }


                }
            }
        }

        private void btnbrwsfolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
                fileDialog.Filter = "Xml files|*.xml";
                Button _objbtn = (Button)sender;
                switch (_objbtn.Name)
                {
                    case "btnBrowseRoutePath":
                        {
                            fileDialog.Filter = "Files |*.txt;*.xml";
                            fileDialog.ShowDialog();
                            if (!string.IsNullOrEmpty(fileDialog.FileName))
                            {
                                txtRoutePath.Text = fileDialog.FileName;
                            }
                            break;
                        }
                    case "btnBrowseStreamPath":
                        {

                            fileDialog.ShowDialog();
                            if (!string.IsNullOrEmpty(fileDialog.FileName))
                            {
                                txtStreamPath.Text = fileDialog.FileName;
                            }
                            break;
                        }
                    case "btnBrowseScenePath":
                        {
                            //fileDialog.Filter = "Xml files|*.xml";
                            //cmbStream.ItemsSource = null;
                            fileDialog.ShowDialog();
                            if (!string.IsNullOrEmpty(fileDialog.FileName))
                            {
                                txtScenePath.Text = fileDialog.FileName;
                                if (!FillSteamDropdown(fileDialog.FileName))
                                {
                                    MessageBox.Show("Please select valid scene file.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                                    cmbStream.ItemsSource = null;
                                }
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                //LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public string getVideoID(string FilePath)
        {
            string Videoid = string.Empty;
            if (System.IO.File.Exists(FilePath))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(FilePath);

                XmlNodeList xmlnodelist = xmlDoc.SelectNodes("//video[@stream_id]");
                foreach (XmlNode xmlnode in xmlnodelist)
                {
                    string streamid = xmlnode.Attributes["stream_id"].InnerText;

                    if (streamid == cmbStream.Text)
                    {
                        Videoid = xmlnode.Attributes["id"].InnerText;
                        return Videoid;
                    }

                }
            }
            return Videoid;
        }

        private bool FillSteamDropdown(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                VideoSceneObject videoSceneObject = new VideoSceneObject();
                List<VideoSceneObject> lstVideoObject = new List<VideoSceneObject>();
                business = new VideoSceneBusiness();
                XmlDocument xmlDoc = new XmlDocument();
                string streamIDs = string.Empty;
                xmlDoc.Load(filePath);
                XmlNodeList nodes = xmlDoc.GetElementsByTagName("file");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        streamIDs += node.Attributes.Count != 0 ? node.Attributes["stream_id"].InnerText + "," : String.Empty;
                    }
                    business.SaveVideoSceneObject(streamIDs.Remove(streamIDs.LastIndexOf(',')));
                    //lstVideoObject = business.GetVideoSceneObjects(videoSceneObject);
                    //cmbStream.ItemsSource = lstVideoObject;
                    BindVideoStreamCombo(0);
                    return true;
                }
                else
                    return false;

            }
            return false;

        }
        void BindVideoStreamCombo(int sceneID)
        {
            List<VideoSceneObject> lstVideoObject = new List<VideoSceneObject>();
            VideoSceneObject videoSceneObject = new VideoSceneObject();
            business = new VideoSceneBusiness();
            cmbStream.ItemsSource = null;
            Dictionary<int, string> dicStreams = new Dictionary<int, string>();
            dicStreams.Add(0, "-Select-");
            videoSceneObject.SceneId = sceneID;
            lstVideoObject = business.GetVideoSceneObjects(videoSceneObject);

            foreach (VideoSceneObject item in lstVideoObject)
            {
                dicStreams.Add(item.VideoObject_Pkey, item.VideoObjectId);
            }
            cmbStream.ItemsSource = dicStreams;
            cmbStream.SelectedIndex = 0;
            if (cmbStream.ItemsSource != null)
            {
                cmbStream.IsEnabled = true;
                chkGuestVideoObject.IsEnabled = true;
            }

            else
            {
                cmbStream.IsEnabled = false;
                chkGuestVideoObject.IsEnabled = false;
            }

        }

        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = stoBiz.GetLocationSubstoreWise(Config.SubStoreId);
                lstLocationList.Add(0, "-Select Location-");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name);
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                //LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public void SelectComboStream()
        {

            int cmbId = Convert.ToInt32(cmbStream.SelectedValue);
            string a = "";
            if (cmbId != 0)
            {
                VideoObjectFileMapping objVFM = new VideoObjectFileMapping();

                objVFM = business.GetobjectVideoDetails(cmbId);
                if (objVFM.RoutePath != "")
                {
                    txtRoutePath.Text = ConfigManager.DigiFolderPath + objVFM.RoutePath;

                }
                else
                {
                    txtRoutePath.Text = "";
                }
                if (objVFM.ChromaPath != "")
                {
                    txtStreamPath.Text = ConfigManager.DigiFolderPath + objVFM.ChromaPath;
                }
                else
                {
                    txtStreamPath.Text = "";
                }



                if ((cmbId == _videoObjId && txtGuestVideoObject.Text == a1) || ((System.Collections.Generic.KeyValuePair<int, string>)(cmbStream.SelectedItem)).Value == guestText)
                {
                    chkGuestVideoObject.IsChecked = true;

                }

                else
                {
                    chkGuestVideoObject.IsChecked = false;

                }

                //Set Enable Audio Straem Settings
                if (objVFM.StreamAudioEnabled != null)
                {
                    chkEnableAudio.IsChecked = objVFM.StreamAudioEnabled == "true" ? true : false;
                }
                else
                    chkEnableAudio.IsChecked = false;
            }

        }


        private void cmbStream_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbStream.SelectedValue != null)
                SelectComboStream();

            GetConfigLocationData();
        }

        private void winMConfigListl_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void txtVideoLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }

        private void SaveConfig()
        {

            List<iMixConfigurationLocationInfo> configlist = new List<iMixConfigurationLocationInfo>();
            iMixConfigurationLocationInfo config = new iMixConfigurationLocationInfo();
            config.IMIXConfigurationMasterId = (long)ConfigParams.IsAdvancedVideoEditActive;
            config.ConfigurationValue = Convert.ToString(chkIsEnabledAdvanceVideoEditing.IsChecked);
            config.SubstoreId = ConfigManager.SubStoreId;
            config.LocationId = (int)cmbLocation.SelectedValue;
            configlist.Add(config);

            config = new iMixConfigurationLocationInfo();
            config.IMIXConfigurationMasterId = (long)ConfigParams.VideoFlipMode;
            config.ConfigurationValue = ((int)(VideoFlipModes)cmbFlipMode.SelectedItem).ToString();
            config.SubstoreId = ConfigManager.SubStoreId;
            config.LocationId = (int)cmbLocation.SelectedValue;
            configlist.Add(config);

            ConfigBusiness business = new ConfigBusiness();
            business.SaveUpdateConfigLocation(configlist);
            MessageBox.Show("Advance video editing for location is saved successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);


        }

        public void GetConfigLocationData()
        {
            try
            {
                configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.IsAdvancedVideoEditActive);
                filterValues.Add((long)ConfigParams.VideoFlipMode);

                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigLocation((int)cmbLocation.SelectedValue, Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsAdvancedVideoEditActive:
                                if (!string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue))
                                    chkIsEnabledAdvanceVideoEditing.IsChecked = Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) == true ? true : false;
                                else chkIsEnabledAdvanceVideoEditing.IsChecked = false;
                                break;
                            case (int)ConfigParams.VideoFlipMode:
                                if (!string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue))
                                cmbFlipMode.SelectedIndex = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue);//VideoFlipModes.None;
                                else cmbFlipMode.SelectedItem = VideoFlipModes.None;
                                break; ;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSaveAdvSetting_Click(object sender, RoutedEventArgs e)
        {
            if (ConfigManager.SubStoreId > 0 && cmbLocation.SelectedIndex > 0)
                SaveConfig();
        }

        private void chkGuestVideoObject_Click(object sender, RoutedEventArgs e)
        {
            if (cmbStream.SelectedIndex > 0 && chkGuestVideoObject.IsChecked == true)
            {
                txtGuestVideoObject.Text = cmbStream.Text;
                guestText = txtGuestVideoObject.Text;
            }
            else
            {
                txtGuestVideoObject.Text = "";
                chkGuestVideoObject.IsChecked = false;
                guestText = "";
            }
        }

        private void btnVideoWriterSettings_Click(object sender, RoutedEventArgs e)
        {
            mFormatControl.comboBoxVideo.SelectedIndex = 12;
            mFormatControl.comboBoxAudio.SelectedIndex = 8;

            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = "MP4 (MPEG-4 Part 14)";//o/p format
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = "MPEG-4 part 2 Video";//audiocodec
            (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = "MP2 (MPEG audio layer 2)";//audiocodec
            configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
        }

        private void chkIsActiveForAdvanceProcessing_Click(object sender, RoutedEventArgs e)
        {

            if (chkIsActiveForAdvanceProcessing.IsChecked == true)
            {
                if (cmbLocation.SelectedIndex > 0)
                {
                    int locationId = Convert.ToInt32(cmbLocation.SelectedValue);
                    string SceneName = String.Empty;

                    business = new VideoSceneBusiness();
                    SceneName = business.checkIsActiveForAdvance(locationId);
                    if (SceneName == "" || SceneName == null)
                    {
                        chkIsActiveForAdvanceProcessing.IsChecked = true;

                    }
                    else
                    {
                        MessageBox.Show("Is Active For Advance Setting is already checked for " + SceneName + ".", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        if (txtSceneName.Text == SceneName)
                        {
                            chkIsActiveForAdvanceProcessing.IsChecked = true;

                        }
                        else
                        {
                            chkIsActiveForAdvanceProcessing.IsChecked = false;

                        }

                    }

                }

                else
                {
                    MessageBox.Show("Please select location.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    chkIsActiveForAdvanceProcessing.IsChecked = false;

                }
            }
            else
            {

                chkIsActiveForAdvanceProcessing.IsChecked = false;

            }
        }

        private void GetAllSubstoreConfigdata()
        {
            try
            {

                lstConfigurationInfo = (new ConfigBusiness()).GetAllSubstoreConfigdata();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }

        private void CopyToAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile)
        {

            try
            {

                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string strPath = string.Empty;
                        string fileName = string.Empty;
                        string target = string.Empty;
                        strPath = item.DG_Hot_Folder_Path + "Profiles\\" + _sceneName;


                        string destFile = strPath;
                        if (!System.IO.Directory.Exists(destFile))
                        {
                            System.IO.Directory.CreateDirectory(destFile);

                            string[] files = System.IO.Directory.GetFiles(Sourcefile);

                            // Copy the files and overwrite destination files if they already exist.
                            foreach (string s in files)
                            {
                                // Use static Path methods to extract only the file name from the path.
                                fileName = System.IO.Path.GetFileName(s);
                                target = System.IO.Path.Combine(destFile, fileName);
                                System.IO.File.Copy(s, target, true);
                            }

                        }
                        else
                        {
                            //System.IO.Directory.Delete(destFile, true);
                            //System.IO.Directory.CreateDirectory(destFile);
                            string[] files = System.IO.Directory.GetFiles(Sourcefile);

                            // Copy the files and overwrite destination files if they already exist.
                            foreach (string s in files)
                            {
                                // Use static Path methods to extract only the file name from the path.
                                fileName = System.IO.Path.GetFileName(s);
                                target = System.IO.Path.Combine(destFile, fileName);
                                System.IO.File.Copy(s, target, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }
        private void DeleteFromAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != ConfigManager.SubStoreId)
                    {
                        string strPath = string.Empty;

                        strPath = item.DG_Hot_Folder_Path + "Profiles\\" + Sourcefile;

                        string destFile = strPath;
                        if (System.IO.Directory.Exists(destFile))
                        {
                            var dir = new DirectoryInfo(destFile);
                            dir.Delete(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);

            }
        }
             
      private void BindFlipModes()
        {
            cmbFlipMode.ItemsSource = Enum.GetValues(typeof(VideoFlipModes));
            cmbFlipMode.SelectedItem = VideoFlipModes.None;
        }

      private void chkEnableAudio_Click(object sender, RoutedEventArgs e)
      {

      }
      private void chkIsVerticalVideo_Checked(object sender, RoutedEventArgs e)
      {
          Handle(sender as CheckBox);
      }
      private void chkIsVerticalVideo_Unchecked(object sender, RoutedEventArgs e)
      {
          Handle(sender as CheckBox);
      }
      void Handle(CheckBox checkBox)
      {
          if ((bool)checkBox.IsChecked)
              mFormatControl.comboBoxVideo.Enabled = false;
          else
              mFormatControl.comboBoxVideo.Enabled = true;
      }
    }
}
