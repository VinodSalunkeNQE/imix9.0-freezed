﻿using DigiConfigUtility.Utility;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Model.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for ImageConfig.xaml
    /// </summary>
    public partial class PreviewWallConfig : System.Windows.Controls.UserControl
    {
        #region Declaration

        Dictionary<int, string> lstLocationList;
        ConfigBusiness configBusiness = null;
        List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
        string WaterMarkFileName = String.Empty;
        string WaterMarkFileNameOnly = String.Empty;
        string HorizentalBorderFileName = String.Empty;
        string HorizentalBorderFileNameOnly = String.Empty;
        string VerticalBorderFileName = String.Empty;
        string VerticalBorderFileNameOnly = String.Empty;
        string LogoImgFileName = string.Empty;
        string LogoFileNameOnly = String.Empty;
        string Location_Name = string.Empty;
        string sitename = string.Empty;
        string folder = string.Empty;
        string DisplayFolderPath = string.Empty;
        int NoOfScreenExit;
        List<string> _positionList;
        List<long> _previewWallMasterId;

        #endregion

        #region Properties

        public string Location { get; set; }

        public int NoOfScreen { get; set; }

        public int DisplayTime { get; set; }

        public int PreviewTime { get; set; }

        public int HighLightTime { get; set; }

        public bool IsMktImg { get; set; }

        public string Angle { get; set; }

        public string MarketingImagePath { get; set; }

        public string WaterMarkImagePath { get; set; }
        public string VerticalBorderImagePath { get; set; }
        public string HorizentalBorderImagePath { get; set; }


        public bool IsLogo { get; set; }

        public string LogoPath { get; set; }

        public bool IsPreviewWallEnabled { get; set; }

        public bool IsSpecImgPreview { get; set; }

        #endregion

        #region Constructor
        public PreviewWallConfig()
        {
            InitializeComponent();
            DataContext = this;
            FillLocationCombo();
            SetPreviewWallMasterId();
            AssignValueToControlor();
            GetPreviewWallInfo(Config.SubStoreId);
            SiteNameHotfolder();

            //   GetConfigLocationData();
        }

        private void SetPreviewWallMasterId()
        {
            _previewWallMasterId = new List<long>();
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.NoOfScreenPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.DelayTimePW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.PreviewTimePW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.HighLightTimePW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.IsMktImgPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.MktImgPathPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.WaterMarkPathPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.IsLogoPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.LogoPathPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.IsPreviewEnabledPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.IsSpecImgPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.VerticalBorderPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.HorizentalBorderPW));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.PreviewWallLogoPosition));
            _previewWallMasterId.Add(Convert.ToInt64(ConfigParams.PreviewWallRFIDPostion));
        }
        #endregion

        #region KeyDown Events

        private void txtNoOfScreens_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }

        private void txtDelayTime_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }

        private void txtPreviewTime_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }

        private void txtHighlightTime_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }
        #endregion

        #region Click Events

        private void btnSaveConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<iMixConfigurationLocationInfo> objConfigList = new List<iMixConfigurationLocationInfo>();
                bool validation = Validation();
                if (validation.Equals(false))
                    return;
                if (chkPreviewWallEnabled.IsChecked.Equals(true))
                {
                    #region Enable Logic
                    Location = cmbLocation.SelectedValue.ToString();
                    NoOfScreen = Convert.ToInt32(txtNoOfScreens.Text);
                    DisplayTime = Convert.ToInt32(txtDelayTime.Text);
                    PreviewTime = Convert.ToInt32(txtPreviewTime.Text);
                    HighLightTime = Convert.ToInt32(txtHighlightTime.Text);
                    if (chkmarketingImages.IsChecked.Equals(true))
                        IsMktImg = true;
                    else
                        IsMktImg = false;
                    WaterMarkImagePath = txtWatermarkImagePath.Text.Trim();
                    HorizentalBorderImagePath = txtHorizentalBorderPath.Text.Trim();
                    VerticalBorderImagePath = txtVerticalBorderPath.Text.Trim();
                    MarketingImagePath = txtMarketingkImagePath.Text.Trim();
                    if (chklogo.IsChecked.Equals(true))
                        IsLogo = true;
                    else
                        IsLogo = false;
                    LogoPath = txtLogoPath.Text;

                    if (chkPreviewWallEnabled.IsChecked.Equals(true))
                        IsPreviewWallEnabled = true;
                    else
                        IsPreviewWallEnabled = false;

                    if (chkspecImg.IsChecked.Equals(true))
                        IsSpecImgPreview = true;
                    else
                        IsSpecImgPreview = false;


                    //if (IsMktImg.Equals(true))
                    //{
                    if (!string.IsNullOrEmpty(DisplayFolderPath) & NoOfScreen > 0)
                    {
                        createDisplayFolder(DisplayFolderPath, NoOfScreen);
                    }
                    //}

                    #region ConfigDataSaved


                    iMixConfigurationLocationInfo ConfigValue = null;

                    // No of Screens
                    ConfigValue = new iMixConfigurationLocationInfo();
                    int locationId = Convert.ToInt32(cmbLocation.SelectedValue);
                    ConfigValue.ConfigurationValue = NoOfScreen.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.NoOfScreenPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);


                    // Delay Time
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = DisplayTime.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.DelayTimePW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    // Preivew Time
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = PreviewTime.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.PreviewTimePW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    // HighLight Time
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = HighLightTime.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.HighLightTimePW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //Is Marketing Images
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = IsMktImg.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsMktImgPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = MarketingImagePath;
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.MktImgPathPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);


                    // WaterMark Images Path
                    ConfigValue = new iMixConfigurationLocationInfo();
                    if (!string.IsNullOrEmpty(WaterMarkFileNameOnly))
                        ConfigValue.ConfigurationValue = "WaterMark.png"; //WaterMarkFileNameOnly;
                    else
                        ConfigValue.ConfigurationValue = "";
                    // ConfigValue.ConfigurationValue = WaterMarkFileName;
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.WaterMarkPathPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //VerticalBorder
                    ConfigValue = new iMixConfigurationLocationInfo();
                    if (!string.IsNullOrEmpty(VerticalBorderFileNameOnly))
                        ConfigValue.ConfigurationValue = "VerticalBorder.png"; //WaterMarkFileNameOnly;
                    else
                        ConfigValue.ConfigurationValue = "";
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.VerticalBorderPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //HorizentalBorder
                    ConfigValue = new iMixConfigurationLocationInfo();
                    if (!string.IsNullOrEmpty(HorizentalBorderFileNameOnly))
                        ConfigValue.ConfigurationValue = "HorizentalBorder.png"; //WaterMarkFileNameOnly;
                    else
                        ConfigValue.ConfigurationValue = "";
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.HorizentalBorderPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);


                    // Is Logo
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = IsLogo.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsLogoPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    // Logo Path
                    ConfigValue = new iMixConfigurationLocationInfo();
                    //   ConfigValue.ConfigurationValue = LogoPath;
                    if (IsLogo)
                        ConfigValue.ConfigurationValue = "Logo.png"; //LogoFileNameOnly;
                    else
                        ConfigValue.ConfigurationValue = "";
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.LogoPathPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //Is Preview Wall Enabled
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = IsPreviewWallEnabled.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPreviewEnabledPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //Is Spec Image
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = IsSpecImgPreview.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsSpecImgPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //LogoPosition
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = cmbLogoPosition.SelectedValue.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.PreviewWallLogoPosition;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //RFIDPostion
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = cmbRFIDPostion.SelectedValue.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.PreviewWallRFIDPostion;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);


                    #region Added by VinS for Preview Wall Enhancement

                    if (!string.IsNullOrEmpty(txtGuestExplanatoryImgPathPW.Text.Trim()) & NoOfScreen > 0)
                    {
                        copyGuestExplanatoryImages(DisplayFolderPath, NoOfScreen);
                    }

                    //GuestExplanatoryImgPathPW
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = txtGuestExplanatoryImgPathPW.Text.Trim().ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.GuestExplanatoryImgPathPW;//Need to update from GUI control
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //IsHorizontalGuestExplatory
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkIsHorizontalRequired.IsChecked.ToString();//Need to update from GUI control
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsHorizontalGuestExplatory;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //IsLoopPreviewRequired
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = chkIsLoopReq.IsChecked.ToString();//Need to update from GUI control
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsLoopPreviewPhotos;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //LoopPreviewPhotosTime
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = txtLoopPrevieTime.Text.Trim().ToString();//Need to update from GUI control
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.LoopPreviewPhotosTime;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    //NoOFCaptures
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = txtNoOfCaptures.Text.Trim().ToString();//Need to update from GUI control
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RideNoOfCaptures;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);


                    //NoOFInterations
                    ConfigValue = new iMixConfigurationLocationInfo();
                    ConfigValue.ConfigurationValue = txtNoOfLoopIterations.Text.Trim().ToString();//Need to update from GUI control
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.RideNoOfCapturesIterations;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    #endregion


                    #endregion

                    #endregion
                }
                else
                {
                    #region Disable Logic

                    if (chkPreviewWallEnabled.IsChecked.Equals(true))
                        IsPreviewWallEnabled = true;
                    else
                        IsPreviewWallEnabled = false;

                    iMixConfigurationLocationInfo ConfigValue = null;

                    //Is Preview Wall Enabled
                    ConfigValue = new iMixConfigurationLocationInfo();
                    int locationId = Convert.ToInt32(cmbLocation.SelectedValue);
                    ConfigValue.ConfigurationValue = IsPreviewWallEnabled.ToString();
                    ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPreviewEnabledPW;
                    ConfigValue.SubstoreId = Config.SubStoreId;
                    ConfigValue.LocationId = locationId;
                    objConfigList.Add(ConfigValue);

                    #endregion
                }

                bool IsSaved = configBusiness.SaveUpdateConfigLocation(objConfigList);
                if (IsSaved)
                {
                    MessageBox.Show("Preview configuration updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    GetPreviewWallInfo(Config.SubStoreId);
                    ResetAll();
                    cmbLocation.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("There was some error while updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void copyGuestExplanatoryImages(string DisplayFolderPath, int NoOfScreens)
        {
            try
            {
                for (int i = 1; i <= NoOfScreens; i++)
                {
                    folder = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display" + i.ToString();
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string Masterfolder = folder + "\\MasterData";
                    if (!Directory.Exists(Masterfolder))
                    {
                        Directory.CreateDirectory(Masterfolder);
                    }

                    if (Convert.ToBoolean(chkIsHorizontalRequired.IsChecked))
                    {
                        if (!string.IsNullOrWhiteSpace(txtGuestExplanatoryImgPathPW.Text.Trim()))
                        {
                            if (string.IsNullOrEmpty(txtGuestExplanatoryImgPathPW.Text.Trim()))
                                File.Delete(Masterfolder + "\\" + "HorizontalScan.gif");
                            else
                            {
                                File.Copy(txtGuestExplanatoryImgPathPW.Text.Trim(), System.IO.Path.Combine(Masterfolder + "\\" + "HorizontalScan.gif"), true);
                            }
                        }
                        else if (i > 1)
                        {
                            String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "HorizontalScan.gif";
                            if (File.Exists(CopyFileName))
                                File.Copy(CopyFileName, System.IO.Path.Combine(Masterfolder + "\\" + "HorizontalScan.gif"), true);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(txtGuestExplanatoryImgPathPW.Text.Trim()))
                        {
                            if (string.IsNullOrEmpty(txtGuestExplanatoryImgPathPW.Text.Trim()))
                                File.Delete(Masterfolder + "\\" + "VerticalScan.gif");
                            else
                            {
                                LogConfigurator.log.Error("txtGuestExplanatoryImgPathPW.Text.Trim()" + txtGuestExplanatoryImgPathPW.Text.Trim());
                                LogConfigurator.log.Error("Dwstination path = " + System.IO.Path.Combine(Masterfolder + "\\" + "VerticalScan.gif"));
                                File.Copy(txtGuestExplanatoryImgPathPW.Text.Trim(), System.IO.Path.Combine(Masterfolder + "\\" + "VerticalScan.gif"), true);
                            }
                        }
                        else if (i > 1)
                        {
                            //String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "VerticalScan.gif";
                            File.Copy(txtGuestExplanatoryImgPathPW.Text.Trim(), System.IO.Path.Combine(Masterfolder + "\\" + "VerticalScan.gif"), true);
                        }
                    }
                }

                for (int i = NoOfScreen + 1; i <= NoOfScreenExit; i++)
                {
                    folder = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display" + i.ToString();
                    if (Directory.Exists(folder))
                    {
                        FileAttributes attr = File.GetAttributes(folder);
                        File.SetAttributes(folder, attr & ~FileAttributes.ReadOnly);
                        Directory.Delete(folder, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                // throw;
            }
        }

        private void chkIsHorizontalRequired_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnBrowseGuestExplanatoryImgPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                //dialog.Filter = "PNG(*.png,*.PNG)|*.png;*.PNG|JPG(*.jpg,*.jpeg)|*.jpg;*.jpeg;*.JPG;*.JPEG;*.GIF;*.gif;*.jfif";
                dialog.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif;*.tif";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    //VerticalBorderFileName = dialog.FileName;
                    txtGuestExplanatoryImgPathPW.Text = dialog.FileName;
                    //VerticalBorderFileNameOnly = dialog.SafeFileName;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message
                    + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseMarketingPath_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                txtMarketingkImagePath.Visibility = Visibility.Visible;
                txtMarketingkImagePath.Text = string.Empty;

                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtMarketingkImagePath.Text = dialog.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseWatermarkPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WaterMarkFileNameOnly = "";
                txtWatermarkImagePath.Visibility = Visibility.Visible;
                txtWatermarkImagePath.Text = string.Empty;
                ImgWaterMark.Visibility = Visibility.Collapsed;

                System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                dialog.Filter = "PNG(*.png,*.PNG)|*.png;*.PNG|JPG(*.jpg,*.jpeg)|*.jpg;*.jpeg;*.JPG;*.JPEG";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    WaterMarkFileName = dialog.FileName;
                    txtWatermarkImagePath.Text = dialog.FileName;
                    WaterMarkFileNameOnly = dialog.SafeFileName;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseLogoPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (chklogo.IsChecked.Equals(true))
                {
                    txtLogoPath.Visibility = Visibility.Visible;
                    txtLogoPath.Text = string.Empty;
                    ImgLogo.Visibility = Visibility.Collapsed;

                    System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                    dialog.Filter = "PNG(*.png,*.PNG)|*.png;*.PNG|JPG(*.jpg,*.jpeg)|*.jpg;*.jpeg;*.JPG;*.JPEG";
                    System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        LogoFileNameOnly = dialog.SafeFileName;
                        LogoImgFileName = dialog.FileName;
                        txtLogoPath.Text = dialog.FileName;
                    }
                }
                else
                {
                    MessageBox.Show("please check Logo.");
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseVerticalBorderPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                VerticalBorderFileNameOnly = "";
                txtVerticalBorderPath.Visibility = Visibility.Visible;
                txtVerticalBorderPath.Text = string.Empty;
                ImgVerticalBorder.Visibility = Visibility.Collapsed;

                System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                dialog.Filter = "PNG(*.png,*.PNG)|*.png;*.PNG|JPG(*.jpg,*.jpeg)|*.jpg;*.jpeg;*.JPG;*.JPEG";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    VerticalBorderFileName = dialog.FileName;
                    txtVerticalBorderPath.Text = dialog.FileName;
                    VerticalBorderFileNameOnly = dialog.SafeFileName;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnBrowseHorizentalBorderPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HorizentalBorderFileNameOnly = "";
                txtHorizentalBorderPath.Visibility = Visibility.Visible;
                txtHorizentalBorderPath.Text = string.Empty;
                ImgHorizentalBorder.Visibility = Visibility.Collapsed;

                System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                dialog.Filter = "PNG(*.png,*.PNG)|*.png;*.PNG|JPG(*.jpg,*.jpeg)|*.jpg;*.jpeg;*.JPG;*.JPEG";
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    HorizentalBorderFileName = dialog.FileName;
                    txtHorizentalBorderPath.Text = dialog.FileName;
                    HorizentalBorderFileNameOnly = dialog.SafeFileName;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }



        private void btnResetConfig_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
            cmbLocation.SelectedIndex = 0;
        }

        private void chkPreviewWallEnabled_Click(object sender, RoutedEventArgs e)
        {
            if (chkPreviewWallEnabled.IsChecked.Equals(true))
            {
                Enable();
            }
            else if (chkPreviewWallEnabled.IsChecked.Equals(false))
            {
                Disable();
            }
        }

        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue)).ToList();
            if (lstLocation.Count.Equals(0))
            {
                txtLogoPath.Visibility = System.Windows.Visibility.Collapsed;
                btnBrowseLogoPath.Visibility = System.Windows.Visibility.Collapsed;

                txtGuestExplanatoryImgPathPW.Text = string.Empty;
                txtLoopPrevieTime.Text = string.Empty;
                chkIsHorizontalRequired.IsChecked = false;
                chkIsLoopReq.IsChecked = false;
                txtNoOfCaptures.Text = string.Empty;
                txtNoOfLoopIterations.Text = string.Empty;
                txtWatermarkImagePath.Text= string.Empty;
            }
            else
            {
                txtLogoPath.Visibility = System.Windows.Visibility.Visible;
                btnBrowseLogoPath.Visibility = System.Windows.Visibility.Visible;

                txtGuestExplanatoryImgPathPW.Text = string.Empty;
                txtLoopPrevieTime.Text = string.Empty;
                chkIsHorizontalRequired.IsChecked = false;
                chkIsLoopReq.IsChecked = false;
                txtNoOfCaptures.Text = string.Empty;
                txtNoOfLoopIterations.Text = string.Empty;
                txtWatermarkImagePath.Text = string.Empty;
            }
            if (lstLocation != null || lstLocation.Count > 0)
            {
                ImgWaterMark.Source = null;
                ImgLogo.Source = null;
                ImgVerticalBorder.Source = null;
                ImgHorizentalBorder.Source = null;
                if (lstLocation.Count.Equals(0))
                {
                    txtWatermarkImagePath.Visibility = System.Windows.Visibility.Visible;
                    txtVerticalBorderPath.Visibility = System.Windows.Visibility.Visible;
                    txtHorizentalBorderPath.Visibility = System.Windows.Visibility.Visible;
                    ResetAll();
                }
                foreach (var loc in lstLocation)
                {
                    string locName = cmbLocation.SelectedItem.ToString();
                    locName = locName.Replace('[', ' ');
                    locName = locName.Replace(']', ' ');
                    string[] locarr = locName.Split(',');
                    locName = locarr[1].Trim();

                    string DisplayFolderPath = configBusiness.GetFolderStructureInfo(Config.SubStoreId).HotFolderPath;
                    string folder = System.IO.Path.Combine(DisplayFolderPath, "PreviewWall\\" + sitename + "\\" + locName + "\\Display1\\MasterData");
                    switch (loc.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.NoOfScreenPW:
                            txtNoOfScreens.Text = loc.ConfigurationValue;
                            NoOfScreenExit = Convert.ToInt32(loc.ConfigurationValue);
                            break;
                        case (int)ConfigParams.DelayTimePW:
                            txtDelayTime.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.PreviewTimePW:
                            txtPreviewTime.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.HighLightTimePW:
                            txtHighlightTime.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.IsMktImgPW:
                            if (loc.ConfigurationValue.ToUpper() == "FALSE")
                            {
                                chkmarketingImages.IsChecked = false;

                            }
                            else
                            {
                                chkmarketingImages.IsChecked = true;
                            }
                            chkmarketingImages_Click(null, null);
                            break;
                        case (int)ConfigParams.WaterMarkPathPW:
                            txtWatermarkImagePath.Text = loc.ConfigurationValue;
                            if (string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                txtWatermarkImagePath.Visibility = Visibility.Visible;
                                txtWatermarkImagePath.Text = loc.ConfigurationValue;
                                ImgWaterMark.Visibility = Visibility.Collapsed;
                            }
                            else
                            {
                                //txtWatermarkImagePath.Visibility = Visibility.Collapsed;
                                ImgWaterMark.Visibility = Visibility.Visible;
                                if (Directory.Exists(folder))
                                {
                                    string waterMarkImg = folder + "\\WaterMark.png";
                                    if (File.Exists(waterMarkImg))
                                    {
                                        BitmapImage testbmp = new BitmapImage();
                                        using (FileStream fileStream = File.OpenRead(waterMarkImg))
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            fileStream.CopyTo(ms);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            fileStream.Close();
                                            testbmp.BeginInit();
                                            testbmp.StreamSource = ms;
                                            testbmp.EndInit();
                                            testbmp.Freeze();
                                        }
                                        ImgWaterMark.Source = testbmp;
                                        WaterMarkFileNameOnly = "WaterMark.png";
                                    }
                                }
                            }
                            break;
                        case (int)ConfigParams.IsLogoPW:
                            if (loc.ConfigurationValue.ToUpper() == "FALSE")
                            {
                                chklogo.IsChecked = false;

                            }
                            else
                            {
                                chklogo.IsChecked = true;
                            }
                            chklogo_Click(null, null);
                            break;
                        case (int)ConfigParams.LogoPathPW:
                            if (string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                txtLogoPath.Visibility = Visibility.Visible;
                                txtLogoPath.Text = loc.ConfigurationValue;
                                ImgLogo.Visibility = Visibility.Collapsed;
                            }
                            else
                            {
                                txtLogoPath.Visibility = Visibility.Collapsed;
                                ImgLogo.Visibility = Visibility.Visible;
                                if (Directory.Exists(folder))
                                {
                                    string logoImg = folder + "\\Logo.png";
                                    BitmapImage testbmp = new BitmapImage();
                                    using (FileStream fileStream = File.OpenRead(logoImg))
                                    {
                                        MemoryStream ms = new MemoryStream();
                                        fileStream.CopyTo(ms);
                                        ms.Seek(0, SeekOrigin.Begin);
                                        fileStream.Close();
                                        testbmp.BeginInit();
                                        testbmp.StreamSource = ms;
                                        testbmp.EndInit();
                                        testbmp.Freeze();
                                    }
                                    ImgLogo.Source = testbmp;
                                    LogoImgFileName = "Logo.png";
                                }
                            }
                            chklogo_Click(null, null);
                            break;
                        case (int)ConfigParams.IsPreviewEnabledPW:
                            if (loc.ConfigurationValue.ToUpper() == "FALSE")
                            {
                                Disable();
                                chkPreviewWallEnabled.IsChecked = false;
                            }
                            else
                            {
                                Enable();
                                chkPreviewWallEnabled.IsChecked = true;
                            }
                            break;
                        case (int)ConfigParams.IsSpecImgPW:
                            if (loc.ConfigurationValue.ToUpper() == "FALSE")
                                chkspecImg.IsChecked = false;
                            else
                                chkspecImg.IsChecked = true;
                            break;
                        case (int)ConfigParams.MktImgPathPW:
                            chkmarketingImages_Click(null, null);
                            txtMarketingkImagePath.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.PreviewWallLogoPosition:
                            cmbLogoPosition.SelectedValue = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.PreviewWallRFIDPostion:
                            cmbRFIDPostion.SelectedValue = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.VerticalBorderPW:
                            if (string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                txtVerticalBorderPath.Visibility = Visibility.Visible;
                                txtVerticalBorderPath.Text = loc.ConfigurationValue;
                                ImgVerticalBorder.Visibility = Visibility.Collapsed;
                            }
                            else
                            {
                                txtVerticalBorderPath.Visibility = Visibility.Collapsed;
                                ImgVerticalBorder.Visibility = Visibility.Visible;
                                if (Directory.Exists(folder))
                                {
                                    string verticalBorderImg = folder + "\\VerticalBorder.png";
                                    if (File.Exists(verticalBorderImg))
                                    {
                                        BitmapImage testbmp = new BitmapImage();
                                        using (FileStream fileStream = File.OpenRead(verticalBorderImg))
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            fileStream.CopyTo(ms);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            fileStream.Close();
                                            testbmp.BeginInit();
                                            testbmp.StreamSource = ms;
                                            testbmp.EndInit();
                                            testbmp.Freeze();
                                        }
                                        ImgVerticalBorder.Source = testbmp;
                                        VerticalBorderFileNameOnly = "VerticalBorder.png";
                                    }
                                }
                            }
                            break;
                        case (int)ConfigParams.HorizentalBorderPW:
                            if (string.IsNullOrEmpty(loc.ConfigurationValue))
                            {
                                txtHorizentalBorderPath.Visibility = Visibility.Visible;
                                txtHorizentalBorderPath.Text = loc.ConfigurationValue;
                                ImgHorizentalBorder.Visibility = Visibility.Collapsed;
                            }
                            else
                            {
                                txtHorizentalBorderPath.Visibility = Visibility.Collapsed;
                                ImgHorizentalBorder.Visibility = Visibility.Visible;
                                if (Directory.Exists(folder))
                                {
                                    string horizntalBorderImg = folder + "\\HorizentalBorder.png";
                                    if (File.Exists(horizntalBorderImg))
                                    {
                                        BitmapImage testbmp = new BitmapImage();
                                        using (FileStream fileStream = File.OpenRead(horizntalBorderImg))
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            fileStream.CopyTo(ms);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            fileStream.Close();
                                            testbmp.BeginInit();
                                            testbmp.StreamSource = ms;
                                            testbmp.EndInit();
                                            testbmp.Freeze();
                                        }
                                        ImgHorizentalBorder.Source = testbmp;
                                        HorizentalBorderFileNameOnly = "HorizentalBorder.png";
                                    }
                                }
                            }
                            break;
                        //by Vins
                        case (int)ConfigParams.RideNoOfCapturesIterations:
                            txtNoOfLoopIterations.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.RideNoOfCaptures:
                            txtNoOfCaptures.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.IsHorizontalGuestExplatory:
                            chkIsHorizontalRequired.IsChecked = Convert.ToBoolean(loc.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsLoopPreviewPhotos:
                            chkIsLoopReq.IsChecked = Convert.ToBoolean(loc.ConfigurationValue);
                            break;
                        case (int)ConfigParams.GuestExplanatoryImgPathPW:
                            txtGuestExplanatoryImgPathPW.Text = loc.ConfigurationValue;
                            break;
                        case (int)ConfigParams.LoopPreviewPhotosTime:
                            txtLoopPrevieTime.Text = loc.ConfigurationValue;
                            break;

                    }
                }
            }
        }

        private void btnGridDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this Config?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    string locid = ((Button)sender).Tag.ToString();
                    int i = Convert.ToInt32(locid);

                    var substore = lstLocationList.Where(x => x.Key == i);
                    String name = null;
                    foreach (var item in substore)
                    {
                        name = item.Value;
                    }

                    configBusiness = new ConfigBusiness();
                    DisplayFolderPath = configBusiness.GetFolderStructureInfo(Config.SubStoreId).HotFolderPath;
                    // string folder = DisplayFolderPath + "\\Preview Wall\\" + sitename + "\\" + name;
                    string folder = System.IO.Path.Combine(DisplayFolderPath, "PreviewWall\\" + sitename + "\\" + name);

                    if (Directory.Exists(folder))
                    {
                        var dir = new DirectoryInfo(folder);
                        dir.Attributes = dir.Attributes & ~FileAttributes.ReadOnly;
                        dir.Delete(true);
                    }

                    if (!string.IsNullOrWhiteSpace(locid))
                    {
                        configBusiness = new ConfigBusiness();
                        configBusiness.DeleteLocationWiseConfigParams(Convert.ToInt32(locid));
                        MessageBox.Show("Config deleted successfully");
                        GetPreviewWallInfo(Config.SubStoreId);
                        ResetAll();
                        cmbLocation.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("LocId is null");
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void chklogo_Click(object sender, RoutedEventArgs e)
        {
            if (chklogo.IsChecked.Equals(true))
            {
                if (ImgLogo.Source != null)
                {
                    ImgLogo.Visibility = Visibility.Visible;
                    txtLogoPath.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ImgLogo.Visibility = Visibility.Collapsed;
                    txtLogoPath.Visibility = Visibility.Visible;
                }
                btnBrowseLogoPath.Visibility = Visibility.Visible;
            }
            else
            {
                ImgLogo.Visibility = Visibility.Collapsed;
                txtLogoPath.Visibility = Visibility.Collapsed;
                btnBrowseLogoPath.Visibility = Visibility.Collapsed;
            }
        }

        private void chkmarketingImages_Click(object sender, RoutedEventArgs e)
        {
            if (chkmarketingImages.IsChecked.Equals(true))
            {
                txtMarketingkImagePath.Visibility = Visibility.Visible;
                btnBrowseMarketingPath.Visibility = Visibility.Visible;
            }
            else
            {
                txtMarketingkImagePath.Visibility = Visibility.Collapsed;
                btnBrowseMarketingPath.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region Methods

        private void GetPreviewWallInfo(int SubStoreId)
        {
            configBusiness = new ConfigBusiness();
            lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(SubStoreId);

            List<PreviewWallInfo> lstGrid = new List<PreviewWallInfo>();
            foreach (var locInfo in lstLocationList)
            {
                PreviewWallInfo lstPW = new PreviewWallInfo();
                if (locInfo.Value.Equals("-Select Location-"))
                    continue;
                List<iMixConfigurationLocationInfo> lstLocation = lstLocationWiseConfigParams.Where(n => n.LocationId == locInfo.Key && _previewWallMasterId.Contains(n.IMIXConfigurationMasterId)).ToList();

                if (lstLocation != null || lstLocation.Count > 0)
                {
                    foreach (var loc in lstLocation)
                    {
                        lstPW.LocationNamePW = locInfo.Value;
                        lstPW.LocationIDPW = locInfo.Key;
                        switch (loc.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.NoOfScreenPW:
                                lstPW.NoOfScreenPW = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.DelayTimePW:
                                lstPW.DelayTimePW = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.PreviewTimePW:
                                lstPW.PreviewTimePW = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.HighLightTimePW:
                                lstPW.HighLightTimePW = Convert.ToInt32(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsMktImgPW:
                                if (loc.ConfigurationValue.Equals(false))
                                    lstPW.IsMktImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                else
                                    lstPW.IsMktImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.WaterMarkPathPW:
                                lstPW.WaterMarkPathPW = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.VerticalBorderPW:
                                lstPW.VerticalBorderPathPW = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.HorizentalBorderPW:
                                lstPW.HorizentalBorderPathPW = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsLogoPW:
                                if (loc.ConfigurationValue.Equals(false))
                                    lstPW.IsLogoPW = Convert.ToBoolean(loc.ConfigurationValue);
                                else
                                    lstPW.IsLogoPW = Convert.ToBoolean(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.LogoPathPW:
                                lstPW.LogoPathPW = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsPreviewEnabledPW:
                                if (loc.ConfigurationValue.Equals(false))
                                    lstPW.IsPreviewEnabledPW = Convert.ToBoolean(loc.ConfigurationValue);
                                else
                                    lstPW.IsPreviewEnabledPW = Convert.ToBoolean(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.IsSpecImgPW:
                                if (loc.ConfigurationValue.Equals(false))
                                    lstPW.IsSpecImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                else
                                    lstPW.IsSpecImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                break;
                            case (int)ConfigParams.MktImgPathPW:
                                lstPW.MarketingImgPathPW = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.PreviewWallLogoPosition:
                                lstPW.LogoPosition = loc.ConfigurationValue;
                                break;
                            case (int)ConfigParams.PreviewWallRFIDPostion:
                                lstPW.RFIDPostion = loc.ConfigurationValue;
                                break;
                        }
                    }
                    if (lstLocation.Count > 0)
                        lstGrid.Add(lstPW);
                }
            }
            dgrddisplayrecords.ItemsSource = lstGrid;
        }

        private void GetConfigLocationData()
        {
            try
            {
                configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                filterValues.Add((long)ConfigParams.NoOfScreenPW);
                filterValues.Add((long)ConfigParams.DelayTimePW);
                filterValues.Add((long)ConfigParams.PreviewTimePW);
                filterValues.Add((long)ConfigParams.HighLightTimePW);
                filterValues.Add((long)ConfigParams.IsMktImgPW);
                filterValues.Add((long)ConfigParams.MktImgPathPW);
                filterValues.Add((long)ConfigParams.WaterMarkPathPW);
                filterValues.Add((long)ConfigParams.VerticalBorderPW);
                filterValues.Add((long)ConfigParams.HorizentalBorderPW);
                filterValues.Add((long)ConfigParams.IsLogoPW);
                filterValues.Add((long)ConfigParams.LogoPathPW);
                filterValues.Add((long)ConfigParams.IsPreviewEnabledPW);
                filterValues.Add((long)ConfigParams.IsSpecImgPW);

                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigBasedOnLocation(Config.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();

                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.NoOfScreenPW:
                                txtNoOfScreens.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.DelayTimePW:
                                txtDelayTime.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.PreviewTimePW:
                                txtPreviewTime.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.HighLightTimePW:
                                txtHighlightTime.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsMktImgPW:
                                if (ConfigValuesList[i].ConfigurationValue.Equals(false))
                                    chkmarketingImages.IsChecked = false;
                                else
                                    chkmarketingImages.IsChecked = true;
                                break;
                            case (int)ConfigParams.WaterMarkPathPW:
                                txtWatermarkImagePath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsLogoPW:
                                if (ConfigValuesList[i].ConfigurationValue.Equals(false))
                                    chklogo.IsChecked = false;
                                else
                                    chklogo.IsChecked = true;
                                break;
                            case (int)ConfigParams.LogoPathPW:
                                txtLogoPath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsPreviewEnabledPW:
                                if (ConfigValuesList[i].ConfigurationValue.Equals(false))
                                    chkPreviewWallEnabled.IsChecked = false;
                                else
                                    chkPreviewWallEnabled.IsChecked = true;
                                break;
                            case (int)ConfigParams.IsSpecImgPW:
                                if (ConfigValuesList[i].ConfigurationValue.Equals(false))
                                    chkspecImg.IsChecked = false;
                                else
                                    chkspecImg.IsChecked = true;
                                break;
                            case (int)ConfigParams.MktImgPathPW:
                                txtMarketingkImagePath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.VerticalBorderPW:
                                txtVerticalBorderPath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.HorizentalBorderPW:
                                txtHorizentalBorderPath.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.PreviewWallLogoPosition:
                                cmbLogoPosition.SelectedValue = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.PreviewWallRFIDPostion:
                                cmbRFIDPostion.SelectedValue = ConfigValuesList[i].ConfigurationValue;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void FillLocationCombo()
        {
            lstLocationList = new Dictionary<int, string>();
            try
            {
                StoreSubStoreDataBusniess substoBiz = new StoreSubStoreDataBusniess();
                List<LocationInfo> locSubstoreBased = substoBiz.GetLocationSubstoreWise(Config.SubStoreId);

                lstLocationList.Add(0, "-Select Location-");
                foreach (var item in locSubstoreBased)
                {
                    lstLocationList.Add(item.DG_Location_pkey, item.DG_Location_Name);
                }
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void AssignValueToControlor()
        {
            _positionList = new List<string>();
            _positionList.Add("--Select--");
            _positionList.Add("Top Left");
            _positionList.Add("Top Centre");
            _positionList.Add("Top Right");
            _positionList.Add("Bottom Left");
            _positionList.Add("Bottom Centre");
            _positionList.Add("Bottom Right");
            cmbLogoPosition.ItemsSource = _positionList;
            cmbLogoPosition.SelectedValue = "--Select--";
            cmbRFIDPostion.ItemsSource = _positionList;
            cmbRFIDPostion.SelectedValue = "--Select--";
        }

        private void SiteNameHotfolder()
        {
            StoreSubStoreDataBusniess substoBiz = new StoreSubStoreDataBusniess();
            List<SubStoresInfo> SiteLst = substoBiz.GetLoginUserDefaultSubstores(Config.SubStoreId);
            if (SiteLst.Count > 0)
            {
                sitename = SiteLst[0].DG_SubStore_Name;
            }
            configBusiness = new ConfigBusiness();
            DisplayFolderPath = configBusiness.GetFolderStructureInfo(Config.SubStoreId).HotFolderPath;
        }

        private void createDisplayFolder(string DisplayFolderPath, int NoOfScreens)
        {
            try
            {
                for (int i = 1; i <= NoOfScreens; i++)
                {
                    folder = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display" + i.ToString();
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string Masterfolder = folder + "\\MasterData";
                    if (!Directory.Exists(Masterfolder))
                    {
                        Directory.CreateDirectory(Masterfolder);
                    }
                    if (!string.IsNullOrWhiteSpace(WaterMarkFileName))
                    {
                        if (string.IsNullOrEmpty(txtWatermarkImagePath.Text.Trim()))
                            File.Delete(Masterfolder + "\\" + "WaterMark.png");
                        else
                        {
                            if (WaterMarkFileName.ToUpper() != "WATERMARK.PNG")
                                File.Copy(WaterMarkFileName, System.IO.Path.Combine(Masterfolder + "\\" + "WaterMark.png"), true);
                        }
                    }
                    else if (i > 1)
                    {
                        String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "WaterMark.png";
                        if (File.Exists(CopyFileName))
                            File.Copy(CopyFileName, System.IO.Path.Combine(Masterfolder + "\\" + "WaterMark.png"), true);
                    }
                    if (!string.IsNullOrWhiteSpace(VerticalBorderFileName))
                    {
                        if (string.IsNullOrEmpty(txtVerticalBorderPath.Text.Trim()))
                            File.Delete(Masterfolder + "\\" + "VerticalBorder.png");
                        else
                        {
                            if (VerticalBorderFileName.ToUpper() != "VERTICALBORDER.PNG")
                                File.Copy(VerticalBorderFileName, System.IO.Path.Combine(Masterfolder + "\\" + "VerticalBorder.png"), true);
                        }
                    }
                    else if (i > 1)
                    {
                        String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "VerticalBorder.png";
                        if (File.Exists(CopyFileName))
                            File.Copy(CopyFileName, System.IO.Path.Combine(Masterfolder + "\\" + "VerticalBorder.png"), true);
                    }
                    if (!string.IsNullOrWhiteSpace(HorizentalBorderFileName))
                    {
                        if (string.IsNullOrEmpty(txtHorizentalBorderPath.Text.Trim()))
                            File.Delete(Masterfolder + "\\" + "HorizentalBorder.png");
                        else
                        {
                            if (HorizentalBorderFileName.ToUpper() != "HORIZENTALBORDER.PNG")
                                File.Copy(HorizentalBorderFileName, System.IO.Path.Combine(Masterfolder + "\\" + "HorizentalBorder.png"), true);
                        }
                    }
                    else if (i > 1)
                    {
                        String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "HorizentalBorder.png";
                        if (File.Exists(CopyFileName))
                            File.Copy(CopyFileName, System.IO.Path.Combine(Masterfolder + "\\" + "HorizentalBorder.png"), true);
                    }
                    if (!String.IsNullOrWhiteSpace(LogoImgFileName))
                    {
                        if (chklogo.IsChecked == false)
                            File.Delete(Masterfolder + "\\" + "Logo.png");
                        else
                        {
                            if (LogoImgFileName.ToUpper() != "LOGO.PNG")
                                File.Copy(LogoImgFileName, System.IO.Path.Combine(Masterfolder + "\\" + "Logo.png"), true);
                            else if (i > 1)
                            {
                                String CopyFileName = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display1" + "\\MasterData" + "\\" + "Logo.png";
                                if (File.Exists(CopyFileName))
                                    File.Copy(CopyFileName, System.IO.Path.Combine(Masterfolder + "\\" + "Logo.png"), true);
                            }
                        }
                    }

                }

                for (int i = NoOfScreen + 1; i <= NoOfScreenExit; i++)
                {
                    folder = DisplayFolderPath + "\\PreviewWall\\" + sitename + "\\" + cmbLocation.Text + "\\Display" + i.ToString();
                    if (Directory.Exists(folder))
                    {
                        FileAttributes attr = File.GetAttributes(folder);
                        File.SetAttributes(folder, attr & ~FileAttributes.ReadOnly);
                        Directory.Delete(folder, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                // throw;
            }
        }

        private bool IsNumberKey(Key inkey)
        {
            if (inkey == Key.Tab)
                return true;

            if (inkey < Key.D0 || inkey > Key.D9)
            {
                if (inkey < Key.NumPad0 || inkey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }

        private bool Validation()
        {
            try
            {
                if (cmbLocation.Text.Equals("-Select Location-"))
                {
                    MessageBox.Show("Select Location.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (string.IsNullOrWhiteSpace(txtNoOfScreens.Text.Trim()))
                {
                    MessageBox.Show("Please enter the value in No of screen.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (string.IsNullOrWhiteSpace(txtDelayTime.Text.Trim()))
                {
                    MessageBox.Show("Please enter the value in delay time .", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (string.IsNullOrWhiteSpace(txtPreviewTime.Text.Trim()))
                {
                    MessageBox.Show("Please enter the value in Preview time ", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (string.IsNullOrWhiteSpace(txtHighlightTime.Text.Trim()))
                {
                    MessageBox.Show("Please enter the value in Highlight time", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (chklogo.IsChecked == true)
                {
                    if (string.IsNullOrEmpty(LogoImgFileName))
                    {
                        MessageBox.Show("Please set any Logo ..", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return false;
                    }
                    if (cmbLogoPosition.SelectedValue == "--Select--")
                    {
                        MessageBox.Show("Please select Logo Position..", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return false;
                    }
                }
                if (cmbRFIDPostion.SelectedValue == "--Select--")
                {
                    MessageBox.Show("Please select ImageId Position..", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
                if (chkmarketingImages.IsChecked == true)
                {
                    if (string.IsNullOrEmpty(txtMarketingkImagePath.Text.Trim()))
                    {
                        MessageBox.Show("Please select Marketing Images path ..", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        return false;
                    }
                }
                if (Convert.ToInt32(txtPreviewTime.Text.Trim()) < Convert.ToInt32(txtHighlightTime.Text.Trim()))
                {
                    MessageBox.Show("Preview Time should be greater than Highlight Time", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
            }
            catch (Exception ex)
            {

            }


            return true;
        }

        private void Disable()
        {
            txtNoOfScreens.IsEnabled = false;
            txtDelayTime.IsEnabled = false;
            txtPreviewTime.IsEnabled = false;
            txtHighlightTime.IsEnabled = false;
            chkmarketingImages.IsEnabled = false;
            chklogo.IsEnabled = false;
            txtWatermarkImagePath.IsEnabled = false;
            txtVerticalBorderPath.IsEnabled = false;
            txtHorizentalBorderPath.IsEnabled = false;
            txtMarketingkImagePath.IsEnabled = false;
            btnBrowseMarketingPath.IsEnabled = false;
            btnBrowseWatermarkPath.IsEnabled = false;
            btnBrowseLogoPath.IsEnabled = false;
            txtLogoPath.IsEnabled = false;
            chkspecImg.IsEnabled = false;
            cmbLogoPosition.IsEnabled = false;
            cmbRFIDPostion.IsEnabled = false;
            btnBrowseVerticalBorderPath.IsEnabled = false;
            btnBrowseHorizentalBorderPath.IsEnabled = false;
        }

        private void Enable()
        {
            txtNoOfScreens.IsEnabled = true;
            txtDelayTime.IsEnabled = true;
            txtPreviewTime.IsEnabled = true;
            txtHighlightTime.IsEnabled = true;
            chkmarketingImages.IsEnabled = true;
            chklogo.IsEnabled = true;
            txtWatermarkImagePath.IsEnabled = true;
            txtMarketingkImagePath.IsEnabled = true;
            btnBrowseMarketingPath.IsEnabled = true;
            btnBrowseWatermarkPath.IsEnabled = true;
            btnBrowseLogoPath.IsEnabled = true;
            txtLogoPath.IsEnabled = true;
            chkspecImg.IsEnabled = true;
            cmbLogoPosition.IsEnabled = true;
            cmbRFIDPostion.IsEnabled = true;
            btnBrowseVerticalBorderPath.IsEnabled = true;
            btnBrowseHorizentalBorderPath.IsEnabled = true;

        }

        private void ResetAll()
        {
            txtNoOfScreens.Text = string.Empty;
            txtDelayTime.Text = string.Empty;
            txtPreviewTime.Text = string.Empty;
            txtHighlightTime.Text = string.Empty;
            chkmarketingImages.IsChecked = false;
            chklogo.IsChecked = false;
            txtWatermarkImagePath.Text = string.Empty;
            txtVerticalBorderPath.Text = string.Empty;
            txtHorizentalBorderPath.Text = string.Empty;
            txtMarketingkImagePath.Text = string.Empty;
            txtLogoPath.Text = string.Empty;
            chkspecImg.IsChecked = false;
            WaterMarkFileName = String.Empty;
            WaterMarkFileNameOnly = String.Empty;
            LogoImgFileName = string.Empty;
            LogoFileNameOnly = String.Empty;
            cmbLogoPosition.SelectedValue = "--Select--";
            cmbRFIDPostion.SelectedValue = "--Select--";
            VerticalBorderFileNameOnly = string.Empty;
            VerticalBorderFileName = string.Empty;
            HorizentalBorderFileNameOnly = string.Empty;
            HorizentalBorderFileName = string.Empty;

            txtGuestExplanatoryImgPathPW.Text = string.Empty;
            txtLoopPrevieTime.Text = string.Empty;
            chkIsHorizontalRequired.IsChecked = false;
            chkIsLoopReq.IsChecked = false;
            txtNoOfCaptures.Text = string.Empty;
            txtNoOfLoopIterations.Text = string.Empty;

        }

        #endregion

    }
}
