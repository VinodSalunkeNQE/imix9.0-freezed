﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Net;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for WhatsAppProduct.xaml
    /// </summary>
    public partial class WhatsAppProduct : UserControl, INotifyPropertyChanged
    {

        # region Property

        public string SelectedCounryName;

        protected string _mobileNumber;
        public string mobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                if (value != null)
                {
                    _mobileNumber = value;
                    NotifyPropertyChanged("mobileNumber");
                }
            }
        }

        protected string _APIKey;
        public string APIKey
        {
            get { return _APIKey; }
            set
            {
                if (value != null)
                {
                    _APIKey = value;
                    NotifyPropertyChanged("APIKey");
                }
            }
        }

        protected string _HostUrl;
        public string HostUrl
        {
            get { return _HostUrl; }
            set
            {
                if (value != null)
                {
                    _HostUrl = value;
                    NotifyPropertyChanged("HostUrl");
                }
            }
        }


        private ObservableCollection<string> countryList;
        public ObservableCollection<string> CountryList
        {
            get { return countryList; }
            set { countryList = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        #region Contructor
        public WhatsAppProduct()
        {
            InitializeComponent();
            DataContext = this;
            GetWhatsAppSettings();
            
        }
        #endregion

        #region Private Methods

        private void GetWhatsAppSettings()
        {
            try
            {              
                WhatsAppBusiness wtsAppData = new WhatsAppBusiness();
                CountryList = wtsAppData.getCountryList();
                WhatsAppSettings obj = wtsAppData.GetWhatsAppDetail();                
                mobileNumber= obj.MobileNumber;
                APIKey = obj.APIKey;
                HostUrl = obj.HostUrl;
                cmbTaxType.SelectedValue = obj.CountryName;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }

        }
        private ObservableCollection<string> getMobileApps()
        {
            ObservableCollection<string> lstTaxInfo = new ObservableCollection<string>();

          
            lstTaxInfo.Add("WhatsAPP");

         
            lstTaxInfo.Add("WeChat");

            return lstTaxInfo;
        }           

        private bool SaveWhatsAppDetails()
        {
            bool success = false;
            try
            {
                WhatsAppBusiness WhatsAppData = new WhatsAppBusiness();            
                success = WhatsAppData.SaveWhatsAppDetails(mobileNumberText.Text, SelectedCounryName, ApiKeyText.Text, HostUrlText.Text);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }
            return success;
        }

        private bool IsNumberKey(Key inputkey)
        {
            if (inputkey == Key.Decimal || inputkey == Key.OemPeriod)
                return true;
            if (inputkey < Key.D0 || inputkey > Key.D9)
            {
                if (inputkey < Key.NumPad0 || inputkey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Validation

        private bool Validation()
        {
            if (cmbTaxType.SelectedItem == null || cmbTaxType.SelectedIndex<1)
            {
                MessageBox.Show("Please Select country", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (mobileNumberText.Text == null || mobileNumberText.Text.Length < 1)
            {
                MessageBox.Show("Please enter valid Mobile Number", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (ApiKeyText.Text == null || ApiKeyText.Text.Length<1)
            {
                MessageBox.Show("Please enter valid API Key", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (HostUrlText.Text == null || HostUrlText.Text.Length < 1)
            {
                MessageBox.Show("Please enter valid Host URL", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

   
            return true;

        }

        #endregion

        #region Event
       

        private void txtTaxPercentage_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !IsNumberKey(e.Key);
        }

        private void txtTaxPercentage_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            //if (e.Command == ApplicationCommands.Paste) // e.Command == ApplicationCommands.Copy // || e.Command == ApplicationCommands.Cut ||
            //{
            //    e.Handled = true;
            //}
        }

   
        #endregion

        private void cmbTaxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedCounryName = cmbTaxType.SelectedValue.ToString();
            //GetVenueTaxValueDetails(TaxId);

        }

        private void btnCheckCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WhatsAppBusiness whatsApp = new WhatsAppBusiness();
                WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();

                Uri uri = new Uri(wtsAppData.HostUrl);
                WebRequest request = WebRequest.Create(uri);
                request.Timeout = 3000;
                WebResponse response;
                response = request.GetResponse();
                if (response == null)
                {
                    MessageBox.Show("No Connectivity");
                }
                else
                {
                    MessageBox.Show("Connection Success");
                }

            }
            catch (Exception loi) { MessageBox.Show(loi.Message); }
        }

        private void btnSaveWhatsApp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool valid;
                bool success = false;
                valid = Validation();

                if (valid.Equals(false))
                    return;

                success = SaveWhatsAppDetails();
                if (success.Equals(true))
                    MessageBox.Show("Successfully Saved", "Information", MessageBoxButton.OK, MessageBoxImage.Information);


                ////GetTaxMasterDetails();
                //int selectedTaxId = ((DigiPhoto.IMIX.Model.TaxDetailInfo)(cmbTaxType.SelectionBoxItem)).TaxId;
                //GetVenueTaxValueDetails(selectedTaxId);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message:" + ex.Message + "Error Stack Trace:" + ex.StackTrace);
            }
        }
    }
}

