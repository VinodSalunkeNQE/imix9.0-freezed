﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class AudioControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<SlotProperty> audioSlotlist = new List<SlotProperty>();
        int uniqueID = 1;
        public List<VideoTemplateInfo.VideoSlot> slotList;
        TemplateListItems tmpList;
        List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
        public int videoExpLen = 0;
        int EditAudioId = 0;
        bool IsAudioEdited = false;
        public string AudioFileName = string.Empty;
        public AudioControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            //cmbAudio.SelectedIndex = 0;
        }

        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (audioSlotlist.Count>0)
            SaveAudioXML(audioSlotlist);
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public void ControlListAutoFill()
        {
            DGManageAudios.ItemsSource = null;
            DGManageAudios.ItemsSource = audioSlotlist;
            DGManageAudios.Items.Refresh();
        }
        public bool ShowHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            // DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
            ConfigBusiness configBusiness = new ConfigBusiness();
            TemplateListItems tmpList;
            /// <summary>
            /// Loads the AudioTemplate.
            /// </summary>
            /// 

            List<AudioTemplateInfo> objAudioTemplatesList = configBusiness.GetAudioTemplateList();
            objTemplateList.Clear();
            for (int i = 0; i < objAudioTemplatesList.Count; i++)
            {
                if (objAudioTemplatesList[i].IsActive == true)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = System.IO.Path.GetFileName(objAudioTemplatesList[i].Name); ;
                    tmpList.Item_ID = objAudioTemplatesList[i].AudioTemplateId;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = objAudioTemplatesList[i].DisplayName;
                    tmpList.MediaType = 604;
                    tmpList.Length = objAudioTemplatesList[i].AudioLength;
                    tmpList.StartTime = 0;
                    tmpList.EndTime = (int)tmpList.Length;
                    tmpList.InsertTime = 0;
                    tmpList.Name = LoginUser.DigiFolderAudioPath + System.IO.Path.GetFileName(objAudioTemplatesList[i].Name);
                    tmpList.Tooltip = "Audio Template\n" + "Name : " + tmpList.DisplayName + "\n" + "Length: " + tmpList.Length.ToString() + " sec";
                    objTemplateList.Add(tmpList);
                }
            }
            cmbAudio.ItemsSource = null;
            cmbAudio.ItemsSource = objTemplateList; //((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608).ToList();
            UpdateLayout();
            if (objTemplateList.Count > 0)
            {
                cmbAudio.SelectedItem = objTemplateList[0];
                //  cmbAudio.s = objTemplateList[0];
                videoExpLen = Convert.ToInt32(objTemplateList[0].Length);
                txtStopTime.Text = videoExpLen.ToString();
                if (videoExpLen > 0)
                {
                    sldRange.Maximum = videoExpLen;
                }
            }
            LoadAudioList();
            return _result;
        }

        private void LoadAudioList()
        {
            try
            {
                audioSlotlist.Clear();
                XmlDocument xmlDoc = new XmlDocument();
                string audioXmlPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", AudioFileName, AudioFileName + "_Audio.xml");
                if (File.Exists(audioXmlPath))
                    xmlDoc.Load(audioXmlPath);
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("audio");
                foreach (XmlNode node in nodeList)
                {
                    SlotProperty objAudio = new SlotProperty();
                    objAudio.ID = Convert.ToInt32(node["ID"].InnerText);
                    objAudio.ItemID = Convert.ToInt32(node["ItemID"].InnerText);
                    objAudio.Name = node["Name"].InnerText;
                    objAudio.FilePath = node["FilePath"].InnerText.ToString();
                    objAudio.StartTime = Convert.ToInt32(node["StartTime"].InnerText);
                    objAudio.StopTime = Convert.ToInt32(node["StopTime"].InnerText);
                    objAudio.InsertTime = Convert.ToInt32(node["InsertTime"].InnerText);
                    audioSlotlist.Add(objAudio);
                }
                ControlListAutoFill();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long SlotId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                SlotProperty brdlst = audioSlotlist.Where(o => o.ID == SlotId).FirstOrDefault();
                long borderid = (audioSlotlist.Where(o => o.ID == SlotId).FirstOrDefault()).ItemID;
                audioSlotlist.RemoveAll(o => o.ID == SlotId);
                if (audioSlotlist.Where(o => o.ItemID == borderid).Count() == 0)
                {
                    TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == borderid).FirstOrDefault();
                    if (tmp != null)
                        tmp.IsChecked = false;
                }
                MessageBox.Show("Item deleted successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                DGManageAudios.ItemsSource = audioSlotlist;
                DGManageAudios.Items.Refresh();
                string filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", AudioFileName, AudioFileName + "_Audio.xml");
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
                if (audioSlotlist.Count == 0)
                {
                    uniqueID = 151;
                }
                SaveAudioXML(audioSlotlist);
            }
        }

        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strtemp = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                strtemp = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtStartTime.Text.Trim()) || string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtStartTime.Text.Trim()) >= Convert.ToInt32(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time can not be less then or equal to the End time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UnsubscribeEvents()
        {
            txtStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            btnAddAudio.Click -= new RoutedEventHandler(btnAddAudio_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            btnClearList.Click -= new RoutedEventHandler(btnClearList_Click);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }


        //private void btnAddAudio_Click(object sender, RoutedEventArgs e)
        //{
        //    SlotProperty audioProp = new SlotProperty();
        //    int stopTime = 0;
        //    int startTime = 0;
        //    if (!string.IsNullOrEmpty(txtStartTime.Text))
        //    {
        //        startTime = Convert.ToInt32(txtStartTime.Text);
        //    }
        //    if (!string.IsNullOrEmpty(txtStopTime.Text))
        //    {
        //        stopTime = Convert.ToInt32(txtStopTime.Text);
        //    }
        //    if (chkApplyAudio.IsChecked == true)
        //    {
        //        if (CheckSlotValidations())
        //        {
        //            bool added = false;
        //            long id = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).Item_ID;
        //            //if ((borderslotlist.Where(o => o.BorderStartTime == startTime).Count() > 0))
        //            //{
        //            //    MessageBox.Show("There is already a border in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //            //    return;
        //            //}
        //            audioProp.ID = uniqueID;
        //            audioProp.Name = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).DisplayName;
        //            audioProp.ItemID = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).Item_ID;
        //            audioProp.FilePath = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).FilePath;
        //            audioProp.StartTime = Convert.ToInt32(txtStartTime.Text);
        //            audioProp.StopTime = Convert.ToInt32(txtStopTime.Text);
        //            audioProp.InsertTime = Convert.ToInt32(txtInsertTime.Text);

        //            if (audioSlotlist.Count == 0)
        //            {
        //                audioSlotlist.Add(audioProp);
        //                added = true;
        //            }
        //            else
        //            {
        //                SlotProperty bslItem = audioSlotlist.OrderBy(o => o.StartTime).Where(o => o.StartTime <= startTime).LastOrDefault();
        //                SlotProperty bslFirst = audioSlotlist.OrderBy(o => o.StartTime).ToList().FirstOrDefault();
        //                if (bslItem != null)
        //                {
        //                    //if (startTime > bslItem.BorderStartTime + (bslItem.BorderStopTime - bslItem.BorderStartTime))
        //                    {
        //                        audioSlotlist.Add(audioProp);
        //                        added = true;
        //                    }
        //                    //else
        //                    //{
        //                    //    MessageBox.Show("There is already a border in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    //}
        //                }
        //                else if (bslFirst != null)
        //                {
        //                    if (startTime + stopTime <= bslFirst.StartTime)
        //                    {
        //                        audioSlotlist.Add(audioProp);
        //                        added = true;
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a audio in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else
        //                {
        //                    audioSlotlist.Add(audioProp);
        //                    added = true;
        //                }
        //            }
        //            if (added)
        //            {
        //                TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == id).FirstOrDefault();
        //                tmp.IsChecked = true;
        //            }
        //            DGManageAudios.ItemsSource = audioSlotlist.OrderBy(o => o.StartTime);
        //            DGManageAudios.Items.Refresh();
        //            uniqueID++;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please check Apply Audio.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //}
        private void btnAddAudio_Click(object sender, RoutedEventArgs e)
        {
            if (cmbAudio.Items.Count <= 0)
            {
                MessageBox.Show("Please upload audio template/s.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            SlotProperty audioProp = new SlotProperty();
            SlotValidation validation = new SlotValidation();
            bool added = false;
            int stopTime = 0;
            int startTime = 0;
            int insertTime = 0;
            if (!string.IsNullOrEmpty(txtStartTime.Text))
            {
                startTime = Convert.ToInt32(txtStartTime.Text);
            }
            if (!string.IsNullOrEmpty(txtStopTime.Text))
            {
                stopTime = Convert.ToInt32(txtStopTime.Text);
            }
            if (!string.IsNullOrEmpty(txtInsertTime.Text))
            {
                insertTime = Convert.ToInt32(txtInsertTime.Text.Trim());
            }
            long id = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).Item_ID;
            //if (chkApplyAudio.IsChecked == true)
            //{
            if (CheckSlotValidations())
            {
                if (IsAudioEdited)
                {

                    IsAudioEdited = false;
                    MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (response == MessageBoxResult.Yes)
                    {
                        audioProp = audioSlotlist.Where(o => o.ID == EditAudioId).FirstOrDefault();
                        audioSlotlist.Remove(audioProp);
                        if (IsValidSlot(audioSlotlist, startTime, stopTime, insertTime, id))
                        {
                            audioProp.ItemID = id;
                            audioProp.Name = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).DisplayName;
                            audioProp.FilePath = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).FilePath;
                            audioProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                            audioProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                            audioProp.InsertTime = Convert.ToInt32(txtInsertTime.Text);
                            audioSlotlist.Add(audioProp);
                        }
                        else
                            audioSlotlist.Add(audioProp);

                    }
                    else
                    {
                        txtStartTime.Text = "0";
                        txtStopTime.Text = "0";
                        txtInsertTime.Text = "0";
                    }
                }
                else
                {
                    if (IsValidSlot(audioSlotlist, startTime, stopTime, insertTime, id))
                    {
                        audioProp.ID = uniqueID;
                        audioProp.ItemID = id;
                        audioProp.Name = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).DisplayName;
                        audioProp.FilePath = ((TemplateListItems)(cmbAudio.SelectionBoxItem)).FilePath;
                        audioProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                        audioProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                        audioProp.InsertTime = Convert.ToInt32(txtInsertTime.Text);
                        audioSlotlist.Add(audioProp);
                        added = true;
                        uniqueID++;
                    }
                }
                if (added)
                {
                    TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == id).FirstOrDefault();
                    tmp.IsChecked = true;
                }
                DGManageAudios.ItemsSource = audioSlotlist.OrderBy(o => o.StartTime);
                DGManageAudios.Items.Refresh();
                //SaveAudioXML(audioSlotlist);
            }
            //}
            //else
            //{
            //    MessageBox.Show("Please check Apply Audio.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
            //}
        }
        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (audioSlotlist != null)
            {
                audioSlotlist.Clear();
                DGManageAudios.ItemsSource = audioSlotlist;
                DGManageAudios.Items.Refresh();
                objTemplateList.Where(o => o.MediaType == 604 && o.IsChecked == true).ToList().ForEach(t => t.IsChecked = false);
            }
        }

        private void cmbAudio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmbselectedItem = cmbAudio.SelectedItem;
            if (cmbselectedItem != null)
            {
                SlotProperty audioProp = new SlotProperty();
                long length = ((TemplateListItems)(cmbselectedItem)).Length;
                sldRange.Maximum = length;
                txtStopTime.Text = length.ToString();
            }
        }
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditAudioId = Convert.ToInt32(btnSender.Tag.ToString());
            SlotProperty audioSlot = audioSlotlist.Where(o => o.ID == EditAudioId).FirstOrDefault();
            TemplateListItems audioTemp = objTemplateList.Where(o => o.Item_ID == audioSlot.ItemID).FirstOrDefault();
            cmbAudio.SelectedItem = audioTemp;
            txtStartTime.Text = audioSlot.StartTime.ToString();
            txtStopTime.Text = audioSlot.StopTime.ToString();
            txtInsertTime.Text = audioSlot.InsertTime.ToString();
            IsAudioEdited = true;

        }
        public bool IsValidSlot(List<SlotProperty> slotList, int startTime, int stopTime, int insertTime, long itemID)
        {

            if (slotList.Exists(o => o.ItemID == itemID))
            {
                MessageBox.Show("Audio can not be repeated.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            SlotProperty lastSlot = slotList.OrderBy(o => o.InsertTime).LastOrDefault();
            SlotProperty firstSlot = slotList.OrderBy(o => o.InsertTime).FirstOrDefault();
            if (lastSlot != null)
            {
                if (insertTime >= lastSlot.InsertTime + (lastSlot.StopTime - lastSlot.StartTime))
                {
                    return true;
                }
                else if (insertTime + (stopTime - startTime) <= firstSlot.InsertTime + (firstSlot.StopTime - firstSlot.StopTime))
                {
                    return true;
                }
                else
                {
                    List<SlotProperty> templist = slotList.OrderBy(o => o.InsertTime).ToList();
                    SlotProperty slot = templist.Where(o => o.InsertTime <= insertTime).LastOrDefault();
                    SlotProperty nextSlot = null;
                    if (slot != null)
                    {
                        if (templist.ElementAtOrDefault(slotList.IndexOf(slot) + 1) != null)
                            nextSlot = templist[templist.IndexOf(slot) + 1];
                    }
                    if (nextSlot != null)
                    {
                        if (insertTime >= slot.InsertTime + (slot.StopTime - slot.StartTime) && insertTime + (stopTime - startTime) <= nextSlot.InsertTime)
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("There is already an item in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("There is already an item in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return false;
                    }
                }
            }
            else
                return true;

        }
        private void txtStopTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStopTime.Text.Trim()) > sldRange.Maximum)
                    txtStopTime.Text = sldRange.Maximum.ToString();
            }
        }

        private void txtStartTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStartTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStartTime.Text.Trim()) > sldRange.Maximum)
                    txtStartTime.Text = "0";
            }
        }
        private void txtInsertTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtInsertTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtInsertTime.Text.Trim()) > sldRange.Maximum)
                    txtInsertTime.Text = "0";
            }
        }

        private void SaveAudioXML(List<SlotProperty> audioSlotList)
        {
            try
            {
                string outXML = string.Empty;
                if (audioSlotList != null && audioSlotList.Count > 0)
                {

                    outXML += "<audios>";
                    foreach (SlotProperty lsp in audioSlotList)
                    {
                        outXML += "<audio>";
                        outXML += "<ID>" + lsp.ID + "</ID>";
                        outXML += "<ItemID>" + lsp.ItemID + "</ItemID>";
                        outXML += "<Name>" + lsp.Name + "</Name>";
                        outXML += "<FilePath>" + lsp.FilePath + "</FilePath>";
                        outXML += "<StartTime>" + lsp.StartTime + "</StartTime>";
                        outXML += "<StopTime>" + lsp.StopTime + "</StopTime>";
                        outXML += "<InsertTime>" + lsp.InsertTime + "</InsertTime>";
                        outXML += "</audio>";
                    }
                    outXML += "</audios>";
                }
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.LoadXml(outXML);
                string filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", AudioFileName, AudioFileName + "_Audio.xml");
                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(filePath)))
                    xmlDoc.Save(filePath);
            }
            catch (Exception ex)
            {

            }
        }
    }

}
