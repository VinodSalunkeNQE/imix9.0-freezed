﻿using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class BorderControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<SlotProperty> borderslotlist = new List<SlotProperty>();
        int uniqueID = 151;
        public List<VideoTemplateInfo.VideoSlot> slotList;
        TemplateListItems tmpList;
        bool IsBorderEdit = false;
        long EditBorderID = 0;
        List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
        public int videoExpLen = 0;
        public BorderControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            cmbBorders.SelectedIndex = 0;
        }

        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        public void ControlListAutoFill()
        {
            DGManageBorders.ItemsSource = borderslotlist;
            DGManageBorders.Items.Refresh();
        }
        public bool ShowHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Visible;
                _parent.IsEnabled = false;
                // DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();

                /// <summary>
                /// Loads the Borders.
                /// </summary>
                /// 
                var objBorders = (new BorderBusiness()).GetBorderDetails().Where(t => t.DG_IsActive == true && t.DG_ProductTypeID == 95);
                foreach (var item in objBorders)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = ConfigManager.DigiFolderPath + "\\Frames" + "\\Thumbnails\\" + item.DG_Border;
                    tmpList.Item_ID = item.DG_Borders_pkey;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DG_Border;
                    tmpList.MediaType = 608;
                    tmpList.Name = item.DG_Border;
                    tmpList.Tooltip = "Borders";
                    objTemplateList.Add(tmpList);
                }
                cmbBorders.ItemsSource = objTemplateList; //((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608).ToList();
                if (slotList != null && slotList.Count > 0)
                {
                    btnDefaultBorder.Visibility = Visibility.Visible;
                }
                else
                {
                    btnDefaultBorder.Visibility = Visibility.Collapsed;
                }
                txtStopTime.Text = videoExpLen.ToString();
                if (videoExpLen > 0)
                {
                    sldRange.Maximum = videoExpLen;
                }
                return _result;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return _result;
            }
           
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteSlots_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                long SlotId = long.Parse(btnSender.Tag.ToString());
                MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    SlotProperty brdlst = borderslotlist.Where(o => o.ID == SlotId).FirstOrDefault();
                    long borderid = (borderslotlist.Where(o => o.ID == SlotId).FirstOrDefault()).ItemID;
                    borderslotlist.RemoveAll(o => o.ID == SlotId);
                    if (borderslotlist.Where(o => o.ItemID == borderid).Count() == 0)
                    {
                        TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == borderid).FirstOrDefault();
                        if (tmp != null)
                            tmp.IsChecked = false;
                    }
                    MessageBox.Show("Item deleted Successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    DGManageBorders.ItemsSource = borderslotlist;
                    DGManageBorders.Items.Refresh();
                    if (borderslotlist.Count == 0)
                    {
                        uniqueID = 151;
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strtemp = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                strtemp = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtStartTime.Text.Trim()) || string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtStartTime.Text.Trim()) >= Convert.ToInt32(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time can not be less then or equal to the End time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UnsubscribeEvents()
        {
            txtStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            btnAddBorders.Click -= new RoutedEventHandler(btnAddBorders_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            btnDefaultBorder.Click -= new RoutedEventHandler(btnDefaultBorder_Click);
            btnClearList.Click -= new RoutedEventHandler(btnClearList_Click);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }   
        private void btnAddBorders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbBorders.Items.Count <= 0)
                {
                    MessageBox.Show("Please upload borders.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                SlotProperty borderProp = new SlotProperty();
                int stopTime = 0;
                int startTime = 0;
                if (!string.IsNullOrEmpty(txtStartTime.Text))
                {
                    startTime = Convert.ToInt32(txtStartTime.Text);
                }
                if (!string.IsNullOrEmpty(txtStopTime.Text))
                {
                    stopTime = Convert.ToInt32(txtStopTime.Text);
                }
                long id = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
                if (CheckSlotValidations())
                {
                    SlotValidation validation = new SlotValidation();
                    if (IsBorderEdit)
                    {
                        IsBorderEdit = false;
                        MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (response == MessageBoxResult.Yes)
                        {
                            borderProp = borderslotlist.Where(o => o.ID == EditBorderID).FirstOrDefault();
                            borderslotlist.Remove(borderProp);
                            if (SlotValidation.IsValidSlot(borderslotlist, startTime, stopTime))
                            {
                                borderProp.ItemID = id;
                                borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                                borderProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                                borderProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                                borderslotlist.Add(borderProp);
                                EditBorderID = 0;
                            }
                            else
                            {
                                borderslotlist.Add(borderProp);
                            }

                        }
                        else
                        {
                            cmbBorders.SelectedIndex = 0;
                            txtStartTime.Text = "0";
                            txtStopTime.Text = "0";
                            return;
                        }
                    }
                    else
                    {

                        //if (chkApplyBorders.IsChecked == true)
                        //{
                        borderProp.ID = uniqueID;
                        borderProp.ItemID = id;
                        borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                        borderProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                        borderProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                        if (SlotValidation.IsValidSlot(borderslotlist, startTime, stopTime))
                        {
                            borderslotlist.Add(borderProp);
                            uniqueID++;
                        }
                        //if (added)
                        //{
                        //    TemplateListItems tmp = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == id).FirstOrDefault();
                        //    tmp.IsChecked = true;
                        //}


                        //}
                        //else
                        //{
                        //    MessageBox.Show("Please check Apply Borders.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        //}
                    }
                    DGManageBorders.ItemsSource = borderslotlist.OrderBy(o => o.StartTime);
                    DGManageBorders.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (borderslotlist != null)
            {
                borderslotlist.Clear();
                DGManageBorders.ItemsSource = borderslotlist;
                DGManageBorders.Items.Refresh();
                objTemplateList.Where(o => o.MediaType == 608 && o.IsChecked == true).ToList().ForEach(t => t.IsChecked = false);
            }
        }

        private void btnDefaultBorder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                borderslotlist.Clear();
                long id = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
                //if (chkApplyBorders.IsChecked == true)
                //{
                if (slotList != null && slotList.Count > 0)
                {
                    borderslotlist.Clear();
                    uniqueID = 151;
                    for (int a = 0; a < slotList.Count; a++)
                    {
                        int startTime = Convert.ToInt32(slotList[a].FrameTimeIn);
                        int stopTime = Convert.ToInt32(slotList[a].PhotoDisplayTime) + Convert.ToInt32(slotList[a].FrameTimeIn);
                        SlotProperty borderProp = new SlotProperty();
                        borderProp.ID = uniqueID;
                        borderProp.ItemID = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
                        borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                        borderProp.StartTime = startTime;
                        borderProp.StopTime = stopTime;
                        borderslotlist.Add(borderProp);
                        uniqueID++;
                    }
                    TemplateListItems tmp = objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == id).FirstOrDefault();
                    tmp.IsChecked = true;
                    DGManageBorders.ItemsSource = borderslotlist.OrderBy(o => o.StartTime);
                    DGManageBorders.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
            // }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                EditBorderID = long.Parse(btnSender.Tag.ToString());
                SlotProperty borderSlot = borderslotlist.Where(o => o.ID == EditBorderID).FirstOrDefault();
                TemplateListItems border = objTemplateList.Where(o => o.Item_ID == borderSlot.ItemID).FirstOrDefault();
                cmbBorders.SelectedItem = border;
                txtStartTime.Text = borderSlot.StartTime.ToString();
                txtStopTime.Text = borderSlot.StopTime.ToString();
                IsBorderEdit = true;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void txtStopTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStopTime.Text.Trim()) > sldRange.Maximum)
                    txtStopTime.Text = sldRange.Maximum.ToString();
            }

        }

        private void txtStartTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtStartTime.Text.Trim()))
            {
                if (Convert.ToUInt32(txtStartTime.Text.Trim()) > sldRange.Maximum)
                    txtStartTime.Text = "0";
            }
        }

    }

}
