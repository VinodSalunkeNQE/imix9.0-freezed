﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
using DigiPhoto.DataLayer;
using DigiConfigUtility.Utility;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Threading;


namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for ReportConfiguration.xaml
    /// </summary>
    public partial class ReportConfiguration : System.Windows.Controls.UserControl, INotifyPropertyChanged
    {
        #region property
        ReportConfigurationDetails reportConfigurations;
       
        public ReportConfigurationDetails ReportConfigurations
        {
            get { return reportConfigurations; }
            set
            {
                reportConfigurations = value;
                PropertyModified("ReportConfigurations");

            }
        }

        #endregion
        public ReportConfiguration()
        {
            try
            {
                InitializeComponent();
                this.DataContext = this;
                Thread thread = new Thread(LoadConfigurationDetails);
                thread.Start();

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        void LoadConfigurationDetails()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                ReportConfigurations = configBusiness.GetReportConfigurationDetails();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {           
            try
            {
                bool status = false;
                string validationMessage = ReportConfigurations.Validate();
                ConfigBusiness configBusiness = new ConfigBusiness();
                if (string.IsNullOrEmpty(validationMessage))
                {
                    status = configBusiness.InsertReportConfig(ReportConfigurations);
                    if (status)
                    {
                        MessageBox.Show("Report configuration saved successfully", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        Thread thread = new Thread(LoadConfigurationDetails);
                        thread.Start();
                        busyIndicator.IsBusy = true;
                        BackgroundWorker serviceStarter = new BackgroundWorker();
                        serviceStarter.RunWorkerCompleted += serviceStarter_RunWorkerCompleted;
                        serviceStarter.DoWork += serviceStarter_DoWork;
                        serviceStarter.RunWorkerAsync();

                    }
                }
                else
                    MessageBox.Show(validationMessage);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                busyIndicator.IsBusy = false;      
            }
           
        }

        void serviceStarter_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ServiceController serviceController = new ServiceController();
                serviceController.ServiceName = "DigiReportExportService";              
                if (serviceController.Status == ServiceControllerStatus.Running)
                {
                    serviceController.Stop();
                    Thread.Sleep(TimeSpan.FromSeconds(15));
                    serviceController.Start();
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                }
                else if (serviceController.Status == ServiceControllerStatus.Stopped)
                {
                    serviceController.Start();
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                }
            }
            catch(Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                BusyIndicator work = delegate
                {
                    busyIndicator.IsBusy = false;
                };
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, work);
               
            }
        }
         delegate void BusyIndicator();
        void serviceStarter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                BusyIndicator work = delegate
                {
                    busyIndicator.IsBusy = false;
                };
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, work);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace); BusyIndicator work = delegate
                {
                    busyIndicator.IsBusy = false;
                };
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, work);
            }
            
        }


        private void btnBroseExportPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!reportConfigurations.HasError)
                {

                    System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                    var result = fbDialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        ReportConfigurations.ExportPath = fbDialog.SelectedPath + "\\";
                    }
                }
            }

            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        #region INotifypropertyChanged components
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void PropertyModified(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private void Reset(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadConfigurationDetails();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
    }
    public class OpacityConverter:IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;
            bool isBusy = (bool)value;
            return isBusy ? 0.2: 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LayoutRootEnabledConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return true;
            bool isBusy = (bool)value;
            return !isBusy;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
