﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using DigiConfigUtility.Utility;
using System.IO;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Data;
using System.Text;
using DigiPhoto;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for DataMigration.xaml
    /// </summary>
    public partial class DataMigration : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        System.ComponentModel.BackgroundWorker bwDataMigration = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        string backUpPath = string.Empty;
        bool IsEncryptPassword = false;
        public DataMigration()
        {
            bwDataMigration.DoWork += new System.ComponentModel.DoWorkEventHandler(bwDataMigration_DoWork);
            bwDataMigration.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bwDataMigration_RunWorkerCompleted);
            InitializeComponent();
        }



        private void SelectFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog oFileDialog = new OpenFileDialog();

                oFileDialog.Filter = "Backup file (.bak)|*.BAK";
                oFileDialog.FilterIndex = 1;
                DialogResult dgResult = oFileDialog.ShowDialog();

                if (dgResult == System.Windows.Forms.DialogResult.OK)
                {
                    txtMigrationDirectory.Text = oFileDialog.FileName;// .SelectedPath;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Please select data import/export folder", "DigiConfigUtility");
            }
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtMigrationDirectory.Text))
                {
                    IsEncryptPassword = chkEncryptPassword.IsChecked.Value;
                    backUpPath = txtMigrationDirectory.Text;
                    bs.Show();
                    bwDataMigration.RunWorkerAsync();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select backup file.", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Information);
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        string returnMsg = string.Empty;
        private void bwDataMigration_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {

            try
            {
                string _databasename = "DigiphotoOld";
                //_objDataLayer.RestoreDataBase(_databasename, backUpPath);
                //_objDataLayer.AddMissingColumnsInOldDB();
                //returnMsg = _objDataLayer.ImportTablesFromOldDBToNew();
                DataMigrationBusiness migBuss = new DataMigrationBusiness();
                returnMsg = migBuss.ExecuteDataMigration(_databasename, backUpPath);
                if (IsEncryptPassword)
                {
                    encryptPwdInDG_Users();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        private void bwDataMigration_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //System.Windows.MessageBox.Show(e.Error.Message);
                System.Windows.MessageBox.Show("Error occured while migration. View log for details.", "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Error);
                bs.Hide();
            }
            else if (e.Cancelled)
            {
                bs.Hide();
            }
            else
            {
                string strMsg = string.Empty;
                strMsg = "Database migrated successfully!" + Environment.NewLine + returnMsg.Replace("#", Environment.NewLine);

                returnMsg = string.Empty;
                bs.Hide();
                System.Windows.MessageBox.Show(strMsg, "DigiConfigUtility", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void encryptPwdInDG_Users()
        {
            UserBusiness useBiz = new UserBusiness();
            Dictionary<int, string> ltUsersPwd = useBiz.GetUsersPwdDetails();
            Dictionary<int, string> encryptedPwd = new Dictionary<int, string>();
            foreach (KeyValuePair<int, string> pair in ltUsersPwd)
            {
                encryptedPwd.Add(pair.Key, DigiPhoto.CryptorEngine.Encrypt(ReplaceQuoteValues(pair.Value), true));
            }

            bool isUpdated = useBiz.EncryptUsersPwd(encryptedPwd);
        }


        private string ReplaceQuoteValues(string quoteValues)
        {
            return quoteValues.Replace("\"", "");
        }

        private Boolean? CheckNullorEmptyBoolean(object value)
        {
            if (!string.IsNullOrEmpty(ReplaceQuoteValues(value.ToString())))
            {
                return ReplaceQuoteValues(value.ToString()).ToBoolean();
            }
            else
            {
                return null;
            }
        }

        private DateTime? CheckNullorEmptyDateTime(object value)
        {
            if (!string.IsNullOrEmpty(ReplaceQuoteValues(value.ToString())))
            {
                return ReplaceQuoteValues(value.ToString()).ToDateTime();
            }
            else
            {
                return null;
            }
        }

        private int? CheckNullorEmptyInteger(object value)
        {
            if (!string.IsNullOrEmpty(ReplaceQuoteValues(value.ToString())))
            {
                return ReplaceQuoteValues(value.ToString()).ToInt16();
            }
            else
            {
                return null;
            }
        }

        private double? CheckNullorEmptyDouble(object value)
        {
            if (!string.IsNullOrEmpty(ReplaceQuoteValues(value.ToString())))
            {
                return ReplaceQuoteValues(value.ToString()).ToDouble();
            }
            else
            {
                return null;
            }
        }


    }
}
