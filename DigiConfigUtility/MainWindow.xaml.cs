﻿using DigiConfigUtility.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static public bool IsRestart = false;
        static public bool isLicExp = false;
        Dictionary<int, TabItem> dicTabs = new Dictionary<int, TabItem>();
        public MainWindow()
        {
            
            ChromaKeypluginLic.IntializeProtection();
            DecoderlibLic.IntializeProtection();
            EncoderlibLic.IntializeProtection();
            MComposerlibLic.IntializeProtection();
            MPlatformSDKLic.IntializeProtection();
            InitializeComponent();

            LoadTabs(1);
            AddTabs();
           
            //if (isLicExp == true)
            //{
            //    tbAdvVideoConfig.IsEnabled = false;
            //    tbVideoConfig.IsEnabled = false;
            //}E:\7011CodeBitbucket\DigiPhotoEnhancedCode\DigiPhoto\Manage\Reports\ActivityReport.xaml
            //this.Closed += (s, e) => Application.Current.Shutdown();

        }
        public MainWindow(int adminType)
        {
            InitializeComponent();
            LoadTabs(adminType);
            AddTabs();
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Login objLogin = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "Config Utility Login")
                    {
                        objLogin = (Login)wnd;
                        break;
                    }
                }
                if (objLogin != null)
                {
                    objLogin.Show();//= new Login();
                    objLogin.txtUserName.Text = string.Empty;
                    objLogin.txtPassword.Password = string.Empty;
                    objLogin.txbErrorMessage.Text = string.Empty;
                    objLogin.txtUserName.Focus();
                }
                //_objLogin.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        public static void WriteToFile(string text)
        {
            string filePath = @"C:\";  // folder location
            string fileName = "Error" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            filePath = filePath + fileName;
            if (!Directory.Exists(filePath))
            {
                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + text + "<br/>" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "---------------------------------------------------" + Environment.NewLine);
                }
            }
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if(!MainWindow.IsRestart)
            {
                  Application.Current.Shutdown();
            } 
            
            else
            {
                MainWindow.IsRestart = false;
            }
        }
        private void btnShutdown_Click(object sender, RoutedEventArgs e)
        {
                Application.Current.Shutdown();
        }
        private void LoadTabs(int adminType)
        {
            try
            {
                LogConfigurator.log.Error("LoadTabs Line No : 1");
                int a = 0;
                dicTabs.Add(a++, (new TabItem() { Content = new DecryptConfig(), Header = "Decrypt Config", Name = "DecryptConfigCtrl" }));
                if (adminType != 0)
                {
                    dicTabs.Add(a++, (new TabItem() { Content = new Backup(), Header = "Backup", Name = "BackupCtrl" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new Email(), Header = "Email", Name = "EmailCtrl" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new DataMigration(), Header = "Data Migration", Name = "DataMigrationCtrl" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new SubStoreConfig(), Header = "SubStore Config", Name = "tabSubStore" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new RFIDConfig(), Header = "RFID", Name = "RFIDConfig" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new Others(), Header = "Other", Name = "Other" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new VOS(), Header = "VOS", Name = "Vos" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new VideoConfig(), Header = "Video Configuration", Name = "tbVideoConfig", IsEnabled = !isLicExp }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new AdvanceVideoSettings(), Header = "Interactive Video Settings", Name = "tbAdvVideoConfig", IsEnabled = !isLicExp }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new Services(), Header = "Services", Name = "Services" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new PosSubStoreMapping(), Header = "POS Mapping", Name = "PosSubStoreMappingCtrl" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new Tax(), Header = "Tax", Name = "TaxCtrl" }));

                    dicTabs.Add(a++, (new TabItem() { Content = new WhatsAppProduct(), Header = "WhatsApp Product", Name = "WhatsAppProductCtrl" }));

                    dicTabs.Add(a++, (new TabItem() { Content = new PreviewWallConfig(), Header = "Preview Wall", Name = "PreviewWallCtrl" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new CtrlPreviewallTag(), Header = "Tag Config", Name = "CtrlPreviewallTag" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new ReportConfiguration(), Header = "Report Scheduler", Name = "ReportConfiguration" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new StoreConfiguration(), Header = "Store Configuration", Name = "StoreConfiguration" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new GumRideControls(), Header = "Ride Integration", Name = "GumRideControls" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new ManagePrinting(), Header = "Manage Printing", Name = "ManagePrinting" }));
                    
                    dicTabs.Add(a++, (new TabItem() { Content = new Trigger(), Header = "Sync", Name = "EanbledisableSync" }));
                    dicTabs.Add(a++, (new TabItem() { Content = new OpenCloseFormSettings(), Header = "Opening/Closing form Settings", Name = "OpeningClosingformSettings" }));
                    dicTabs.Add(a++, (new TabItem() { Content = new WaterMarkConfig(), Header = "WaterMark Config", Name = "WaterMarkConfig" }));
                    dicTabs.Add(a++, (new TabItem() { Content = new DecryptUsernamePassword(), Header = "Encrypt Config", Name = "DecryptUsernamePassword" }));
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("LoadTabs Method exception : " + ex.StackTrace + ex.InnerException);
            }
           
        }
        private void AddTabs()
        {
            foreach (var item in dicTabs)
            {
                this.MainTab.Items.Add(item.Value);
            }
        }

        private void MainTab_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
    }
}
