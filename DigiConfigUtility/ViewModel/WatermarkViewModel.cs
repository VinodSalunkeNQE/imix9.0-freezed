using DigiConfigUtility.Utility;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using RelayCommandClass;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace DigiConfigUtility.ViewModel
{
    /// <summary>
    /// This class is a view model of a Watermark on Client view added by Ajay on 10 Feb 2020 
    /// </summary>
    public class WatermarkViewModel : WatermarkModel
    {
        #region Construction
        /// <summary>
        /// Constructs the default instance of a WatermarkViewModel
        /// </summary>
        public WatermarkViewModel()
        {
            TextFontfamily = new ObservableCollection<WatermarkModel>(LoadFontFamily());
            TextFontWeightlst = new ObservableCollection<WatermarkModel>(LoadFontWeight());
            TextFontStylelst = new ObservableCollection<WatermarkModel>(LoadFontstyle());
            if (watermarkModel == null)
                watermarkModel = new WatermarkModel();

            ConfigBusiness configBiz = new ConfigBusiness();
            WatermarkModel objWatermarkModel = new WatermarkModel();
            ConfigBusiness conBiz = new ConfigBusiness();
            objWatermarkModel = configBiz.GetClientWatermarkConfig(Config.SubStoreId);
            WatermarkText = objWatermarkModel.WatermarkText;
            TextFontSize = objWatermarkModel.TextFontSize;
            TextFontWeights = objWatermarkModel.TextFontWeights;
            TextFontfamilyNames = objWatermarkModel.TextFontfamilyNames;
            FontStyle = objWatermarkModel.FontStyle;
       
            //Bcolor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(objWatermarkModel.TextFontColor));
            ColorSelected = objWatermarkModel.TextFontColor;
            Opacity = objWatermarkModel.Opacity;
            WatermarkTextPreview = objWatermarkModel.WatermarkText;
            SolidColorBrush brush =new SolidColorBrush(ColorSelected);
            WatermarkPreviewForeground = brush;


            //WatermarkText = WatermarkModeldata.WatermarkText;
            //TextFontSize = WatermarkModeldata.TextFontSize;
            //TextFontWeights = WatermarkModeldata.TextFontWeights;
            //TextFontfamilyNames = WatermarkModeldata.TextFontfamilyNames;
            //ColorSelected = WatermarkModeldata.TextFontColor;
            //FontStyle = WatermarkModeldata.FontStyle;
            //Opacity = WatermarkModeldata.Opacity;
            //WatermarkPreviewForeground = new SolidColorBrush(WatermarkModeldata.TextFontColor);
        }
        public WatermarkModel WatermarkModeldata
        {
            get
            {
                return FillWaterConfig();
            }
            set
            {
                watermarkModel = value;
                OnPropertyChanged("WatermarkModeldata");
            }
        }
        #endregion

        #region Members
        public ObservableCollection<WatermarkModel> TextFontfamily { get; set; }
        public ObservableCollection<WatermarkModel> TextFontWeightlst { get; set; }

        public ObservableCollection<WatermarkModel> TextFontStylelst { get; set; }
        public WatermarkModel watermarkModel = null;
        WatermarkModel _Watermark;
        private ICommand updateCommand;
        int _count = 0;
        #endregion

        #region Properties
        public WatermarkModel Watermark
        {
            get
            {
                return _Watermark;
            }
            set
            {
                _Watermark = value;
            }
        }



        #endregion

        #region INotifyPropertyChanged Members

        //public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        //private void RaisePropertyChanged(string propertyName)
        //{
        //    // take a copy to prevent thread issues
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
        #endregion

        private List<WatermarkModel> LoadFontFamily()
        {
            List<WatermarkModel> fonts = new List<WatermarkModel>();
            //System.Drawing.Text.InstalledFontCollection col = new System.Drawing.Text.InstalledFontCollection();

            foreach (System.Windows.Media.FontFamily family in Fonts.SystemFontFamilies)
            {
                WatermarkModel watermarkitem = new WatermarkModel();
                watermarkitem.TextFontfamilyNames = family;
                fonts.Add(watermarkitem);
            }
            return fonts;
            //CmbFontFamily.ItemsSource = fonts;
            //CmbFontFamily.Text = "Times New Roman";
        }

        public List<WatermarkModel> LoadFontWeight()
        {
            List<WatermarkModel> TextFontWeights = new List<WatermarkModel>();
            List<FontWeight> fontWeights = fontWeights = new List<FontWeight>();
            System.Drawing.Text.InstalledFontCollection col = new System.Drawing.Text.InstalledFontCollection();
            var type = typeof(FontWeights);
            foreach (var p in type.GetProperties().Where(s => s.PropertyType == typeof(FontWeight)))
            {
                FontWeight wonderIfItsPresent = (FontWeight)p.GetValue(null, null);
                bool containsItem = fontWeights.Any(item => item.ToString() == wonderIfItsPresent.ToString());
                if (!containsItem)
                {
                    fontWeights.Add((FontWeight)p.GetValue(null, null));
                }
            }
            foreach (var item in fontWeights)
            {
                WatermarkModel obj = new WatermarkModel();
                obj.TextFontWeights = item;
                TextFontWeights.Add(obj);
            }

            return TextFontWeights;

        }
        public List<WatermarkModel> LoadFontstyle()
        {
            List<WatermarkModel> TextFontStyles = new List<WatermarkModel>();
            List<System.Windows.FontStyle> fontstyle = new List<System.Windows.FontStyle>();
            List<WatermarkModel> TextFontStylelst = new List<WatermarkModel>()
                                                   { new WatermarkModel {FontStyle =FontStyles.Italic },
                                                   { new WatermarkModel {FontStyle =FontStyles.Normal } },
                                                   { new WatermarkModel {FontStyle =FontStyles.Oblique } } };

           
            return TextFontStylelst;

        }
        internal WatermarkModel FillWaterConfig()
        {
            
            return watermarkModel;
        }
        #region Commands
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new RelayCommand(i => this.Update(), null);
                }
                return updateCommand;
            }
        }
        private void Update()
        {
            if (IsSelected)
            {
                ConfigBusiness conBiz = new ConfigBusiness();
                WatermarkModel objWatermarkDetails = new WatermarkModel();
                objWatermarkDetails.SubStoreId = Config.SubStoreId;
                objWatermarkDetails.TextFontSize = this.TextFontSize;
                objWatermarkDetails.TextFontWeights = this.TextFontWeights;
                objWatermarkDetails.FontStyle = this.FontStyle;
                objWatermarkDetails.TextFontfamilyNames = this.TextFontfamilyNames;
                //var color = GetColorName(this.ColorSelected);
                objWatermarkDetails.TextFontColor = this.ColorSelected;
                objWatermarkDetails.WatermarkText = this.WatermarkText;
                objWatermarkDetails.IsUnderline = this.IsUnderline;
                objWatermarkDetails.UserId = LoginUser.UserId;
                objWatermarkDetails.Opacity = this.Opacity;
                bool IsSaved = conBiz.SaveClientviewWatermark(objWatermarkDetails);
                if (IsSaved)
                {
                    WatermarkPreviewForeground = new SolidColorBrush(this.ColorSelected);
                    System.Windows.MessageBox.Show("Watermark setting saved successfully");
                }             
                
            }
            else
            {
                System.Windows.MessageBox.Show("Please select Watermark checkbox");
            }

        }

        //public void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        //{
        //    e.Handled = IsTextNumeric(e.Text);
        //}
        //private static bool IsTextNumeric(string str)
        //{
        //    System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
        //    return reg.IsMatch(str);
        //}


        //static string GetColorName(Color col)
        //{
        //    PropertyInfo colorProperty = typeof(Colors).GetProperties()
        //        .FirstOrDefault(p => Color.AreClose((Color)p.GetValue(null), col));
        //    return colorProperty != null ? colorProperty.Name : "unnamed color";
        //}
        #endregion



    }
}
