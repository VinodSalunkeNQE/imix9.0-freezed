﻿/*=============================================================================
  Copyright (C) 2012 Allied Vision Technologies.  All Rights Reserved.

  Redistribution of this file, in original or modified form, without
  prior written consent of Allied Vision Technologies is prohibited.

-------------------------------------------------------------------------------

  File:        VimbaHelper.cs

  Description: Implementation file for the VimbaHelper class that demonstrates
               how to implement an asynchronous, continuous image acquisition
               with VimbaNET.

-------------------------------------------------------------------------------

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE,
  NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using AVT.VmbAPINET;
using System.Collections.Specialized;
using System.Xml;
using System.Globalization;

namespace TripCaptureUtility
{
    
    //A simple container class for infos (name and ID) about a camera
    public class CameraInfo
    {
        private string m_Name = null;
        private string m_ID = null;

        public CameraInfo(string name, string id)
        {
            if(null == name)
            {
                throw new ArgumentNullException("name");
            }
            if(null == name)
            {
                throw new ArgumentNullException("id");
            }

            m_Name = name;
            m_ID = id;
        }

        public string Name
        {
            get
            {
                return m_Name;
            }
        }

        public string ID
        {
            get
            {
                return m_ID;
            }
        }

        public override string ToString()
        {
            return m_Name;
        }
    }

    //Event args class that will contain a single image
    public class FrameEventArgs : EventArgs
    {
        private Image       m_Image = null;
        private Exception   m_Exception = null;

        public FrameEventArgs(Image image)
        {
            if(null == image)
            {
                throw new ArgumentNullException("image");
            }

            m_Image = image;
        }

        public FrameEventArgs(Exception exception)
        {
            if(null == exception)
            {
                throw new ArgumentNullException("exception");
            }

            m_Exception = exception;
        }

        public Image Image
        {
            get
            {
                return m_Image;
            }
        }

        public Exception Exception
        {
            get
            {
                return m_Exception;
            }
        }
    }

    //helper class to provide necessary bitmap functions
    class RingBitmap
    {
        private int m_Size = 0;
        private Bitmap[] m_Bitmaps = null;              //Bitmaps to display images
        private int m_BitmapSelector = 0;               //selects Bitmap

        public RingBitmap(int size)
        {
            m_Size = size;
            m_Bitmaps = new Bitmap[m_Size];
        }

        //Bitmap rotation selector
        private void SwitchBitamp()
        {
            m_BitmapSelector++;

            if (m_Size == m_BitmapSelector)
                m_BitmapSelector = 0;
        }

        //return current bitmap as image
        public Image Image
        {
            get
            {
                return m_Bitmaps[m_BitmapSelector];
            }
        }

        //copy buffer in 8bppIndexed bitmap
        public void CopyToNextBitmap_8bppIndexed(int width, int height, byte[] buffer)
        {
            //switch to Bitmap object which is currently not in use by GUI
            SwitchBitamp();

            //check if this Bitmap object was already created -> else reuse it
            if (null == m_Bitmaps[m_BitmapSelector])
                m_Bitmaps[m_BitmapSelector] = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            //Set greyscale palette
            ColorPalette palette = m_Bitmaps[m_BitmapSelector].Palette;
            for (int i = 0; i < palette.Entries.Length; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }
            m_Bitmaps[m_BitmapSelector].Palette = palette;

            //Copy image data
            BitmapData bitmapData = m_Bitmaps[m_BitmapSelector].LockBits(new Rectangle(0,
                                                                                        0,
                                                                                        width,
                                                                                        height),
                                                                         ImageLockMode.WriteOnly,
                                                                         PixelFormat.Format8bppIndexed);
            try
            {
                //Copy image data line by line
                for (int y = 0; y < height; y++)
                {
                    System.Runtime.InteropServices.Marshal.Copy(buffer,
                                                                y * width,
                                                                new IntPtr(bitmapData.Scan0.ToInt64() + y * bitmapData.Stride),
                                                                width);
                }
            }
            finally
            {
                m_Bitmaps[m_BitmapSelector].UnlockBits(bitmapData);
            }
        }

        //copy buffer in 24bppRgb bitmap
        public void CopyToNextBitmap_24bppRgb(int width, int height, byte[] buffer)
        {
            //switch to Bitmap object which is currently not in use by GUI
            SwitchBitamp();

            //check if this Bitmap object was already created -> else reuse it
            if (null == m_Bitmaps[m_BitmapSelector])
                m_Bitmaps[m_BitmapSelector] = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            //Copy image data
            BitmapData bitmapData = m_Bitmaps[m_BitmapSelector].LockBits(new Rectangle(0,
                                                                                        0,
                                                                                        width,
                                                                                        height),
                                                                        ImageLockMode.WriteOnly,
                                                                        PixelFormat.Format24bppRgb);
            try
            {
                //Copy image data line by line
                for (int y = 0; y < height; y++)
                {
                    System.Runtime.InteropServices.Marshal.Copy(buffer,
                                                                y * width * 3,
                                                                new IntPtr(bitmapData.Scan0.ToInt64() + y * bitmapData.Stride),
                                                                width * 3);
                }
            }
            finally
            {
                m_Bitmaps[m_BitmapSelector].UnlockBits(bitmapData);
            }
        }
    }

    //Delegates for our callbacks
    public delegate void CameraListChangedHandler(object sender, EventArgs args);
    public delegate void FrameReceivedHandler(object sender, FrameEventArgs args);

    //A helper class as a wrapper around Vimba
    public class VimbaHelper
    {
        private Vimba                       m_Vimba = null;                     //Main Vimba API entry object
        private CameraListChangedHandler    m_CameraListChangedHandler = null;  //Camera list changed handler
        private Camera                      m_Camera = null;                    //Camera object if camera is open
        private bool                        m_Acquiring = false;                //Flag to remember if acquisition is running
        private FrameReceivedHandler        m_FrameReceivedHandler = null;      //Frames received handler
        private const int                   m_RingBitmapSize = 2;               //Amount of Bitmaps in RingBitmap
        private static RingBitmap           m_RingBitmap = null;                //Bitmaps to display images
        private static readonly object      m_ImageInUseSyncLock = new object();//Protector for m_ImageInUse
        private static bool                 m_ImageInUse = true;                //Signal of picture box that image is used

        public VimbaHelper()
        {
            m_RingBitmap = new RingBitmap(m_RingBitmapSize);
        }

        ~VimbaHelper()
        {
            //Release Vimba API if user forgot to call Shutdown
            ReleaseVimba();
        }

        #region Base Classes
        //A class to manage the target value
        //of a feature.
        private abstract class FeatureValue
        {
            private Feature m_Feature = null;

            public FeatureValue(Feature feature)
            {
                if (null == feature)
                {
                    throw new ArgumentNullException("feature");
                }

                m_Feature = feature;
            }

            //Returns the according feature
            public Feature Feature
            {
                get
                {
                    return m_Feature;
                }
            }

            //Returns true if feature is writeable at the moment
            public virtual bool IsWriteable
            {
                get
                {
                    return m_Feature.IsWritable();
                }
            }

            //Returns true if feature currently contains desired target value
            public abstract bool IsTargetValue
            {
                get;
            }

            //Writes target value into the feature
            public abstract void ApplyTargetValue();
        };

        //A class to manage the target value
        //of an integer feature.
        private class IntegerValue : FeatureValue
        {
            private long m_TargetValue = 0;

            public IntegerValue(Feature feature, long targetValue)
                : base(feature)
            {
                m_TargetValue = targetValue;
            }

            //Returns true if feature is writeable at the moment
            public override bool IsWriteable
            {
                get
                {
                    //Check if it is writeable at all
                    if (Feature.IsWritable() == false)
                    {
                        return false;
                    }

                    //Then check if the target value is within the current range and
                    //matches the increment.
                    long minValue = Feature.IntRangeMin;
                    long maxValue = Feature.IntRangeMax;
                    if ((m_TargetValue < minValue)
                        || (m_TargetValue > maxValue))
                    {
                        return false;
                    }

                    long incValue = Feature.IntIncrement;
                    if (incValue < 1)
                    {
                        throw new Exception("Invalid increment found in a feature.");
                    }
                    if (((m_TargetValue - minValue) % incValue) != 0)
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Returns true if feature currently contains desired target value
            public override bool IsTargetValue
            {
                get
                {
                    //Check if it is readable at all
                    if (Feature.IsReadable() == false)
                    {
                        return false;
                    }

                    //Now read the current value and compare it to our target value
                    if (Feature.IntValue != m_TargetValue)
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Writes target value into the feature
            public override void ApplyTargetValue()
            {
                Feature.IntValue = m_TargetValue;
            }
        };

        //A class to manage the target value
        //of an float feature.
        private class FloatValue : FeatureValue
        {
            private double m_TargetValue = 0.0;

            public FloatValue(Feature feature, double targetValue)
                : base(feature)
            {
                m_TargetValue = targetValue;
            }

            //Returns true if feature is writeable at the moment
            public override bool IsWriteable
            {
                get
                {
                    //Check if it is writeable at all
                    if (Feature.IsWritable() == false)
                    {
                        return false;
                    }

                    //Then check if the target value is within the current range and
                    //matches the increment.
                    double minValue = Feature.FloatRangeMin;
                    double maxValue = Feature.FloatRangeMax;
                    if ((m_TargetValue < minValue)
                        || (m_TargetValue > maxValue))
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Returns true if feature currently contains desired target value
            public override bool IsTargetValue
            {
                get
                {
                    //Check if it is readable at all
                    if (Feature.IsReadable() == false)
                    {
                        return false;
                    }

                    double value = Feature.FloatValue;
                    //Let's assume we don't want an exact match but
                    //at least a very close match.
                    if (Math.Abs(value - m_TargetValue) < 1e-8) //Match with absolute precision
                    {
                        return true;
                    }
                    else if ((Math.Abs(value - m_TargetValue) / Math.Max(Math.Abs(value), Math.Abs(m_TargetValue))) < 1e-8) //Match with relative precision
                    {
                        return true;
                    }

                    return false; //No match
                }
            }

            //Writes target value into the feature
            public override void ApplyTargetValue()
            {
                Feature.FloatValue = m_TargetValue;
            }
        };

        //A class to manage the target value
        //of an enumeration feature.
        private class EnumerationValue : FeatureValue
        {
            private string m_TargetValue = null;

            public EnumerationValue(Feature feature, string targetValue)
                : base(feature)
            {
                if (string.IsNullOrEmpty(targetValue))
                {
                    throw new ArgumentNullException("targetValue");
                }

                m_TargetValue = targetValue;
            }

            //Returns true if feature is writeable at the moment
            public override bool IsWriteable
            {
                get
                {
                    //Check if it is writeable at all
                    if (Feature.IsWritable() == false)
                    {
                        return false;
                    }

                    //Check if the target value is one of our enum entries
                    string[] enumValues = Feature.EnumValues;
                    foreach (string enumValue in enumValues)
                    {
                        if (string.Compare(enumValue, m_TargetValue, StringComparison.Ordinal) == 0)
                        {
                            if (Feature.IsEnumValueAvailable(enumValue))
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                }
            }

            //Returns true if feature currently contains desired target value
            public override bool IsTargetValue
            {
                get
                {
                    //Check if it is readable at all
                    if (Feature.IsReadable() == false)
                    {
                        return false;
                    }

                    if (string.Compare(Feature.EnumValue, m_TargetValue, StringComparison.Ordinal) != 0)
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Writes target value into the feature
            public override void ApplyTargetValue()
            {
                Feature.EnumValue = m_TargetValue;
            }
        };

        //A class to manage the target value
        //of an string feature.
        private class StringValue : FeatureValue
        {
            private string m_TargetValue = null;

            public StringValue(Feature feature, string targetValue)
                : base(feature)
            {
                if (null == targetValue)
                {
                    throw new ArgumentNullException("targetValue");
                }

                m_TargetValue = targetValue;
            }

            //Returns true if feature currently contains desired target value
            public override bool IsTargetValue
            {
                get
                {
                    //Check if it is readable at all
                    if (Feature.IsReadable() == false)
                    {
                        return false;
                    }

                    if (string.Compare(Feature.StringValue, m_TargetValue, StringComparison.Ordinal) != 0)
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Writes target value into the feature
            public override void ApplyTargetValue()
            {
                Feature.StringValue = m_TargetValue;
            }
        };

        //A class to manage the target value
        //of an boolean feature.
        private class BooleanValue : FeatureValue
        {
            private bool m_TargetValue = false;

            public BooleanValue(Feature feature, bool targetValue)
                : base(feature)
            {
                m_TargetValue = targetValue;
            }

            //Returns true if feature currently contains desired target value
            public override bool IsTargetValue
            {
                get
                {
                    //Check if it is readable at all
                    if (Feature.IsReadable() == false)
                    {
                        return false;
                    }

                    if (Feature.BoolValue != m_TargetValue)
                    {
                        return false;
                    }

                    return true;
                }
            }

            //Writes target value into the feature
            public override void ApplyTargetValue()
            {
                Feature.BoolValue = m_TargetValue;
            }
        };
        #endregion

        public static void LoadFromFile(Camera camera, string fileName, out StringCollection loadedFeatures, out StringCollection missingFeatures, bool ignoreStreamable)
        {
            LoadFromFile(camera, fileName, out loadedFeatures, out missingFeatures, ignoreStreamable, 5);
        }
        public static void LoadFromFile(Camera camera, string fileName, out StringCollection loadedFeatures, out StringCollection missingFeatures, bool ignoreStreamable, uint maxIterations)
        {
            //Check parameters
            if (null == camera)
            {
                throw new ArgumentNullException("camera");
            }
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            //Load the xml document from file
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);

            //Get the settings node
            XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("Settings");
            if (xmlNodeList.Count != 1)
            {
                throw new Exception("Invalid camera settings xml file.");
            }

            XmlNode settingsNode = xmlNodeList[0];
            if (null == settingsNode)
            {
                throw new Exception("Invalid camera settings xml file.");
            }

            //Get camera model
            string model = camera.Model;

            //Check if the camera model matches the one from the xml file
            XmlAttribute modelAttribute = settingsNode.Attributes["Model"];
            if (null == modelAttribute)
            {
                throw new Exception("Invalid camera settings xml file.");
            }

            if (string.Compare(model, modelAttribute.Value, StringComparison.Ordinal) != 0)
            {
                throw new Exception("Xml file doesn't match the camera model.");
            }

            StringCollection currentLoadedFeatures = new StringCollection();
            StringCollection currentMissingFeatures = new StringCollection();

            //First load all features from xml
            LinkedList<FeatureValue> featureValues = new LinkedList<FeatureValue>();
            foreach (XmlNode xmlNode in settingsNode.ChildNodes)
            {
                string type = xmlNode.Name;

                //Get the feature name from the attribute
                XmlAttribute nameAttribute = xmlNode.Attributes["Name"];
                if (null == nameAttribute)
                {
                    throw new Exception("Invalid camera settings xml file.");
                }

                string name = nameAttribute.Value;

                //Get the feature target value as a string
                string value = xmlNode.InnerText;

                //Try to find the feature with the given name
                Feature feature = null;
                try
                {
                    feature = camera.Features[name];
                }
                catch
                {
                    feature = null;
                }

                if (null != feature)
                {
                    bool loadFeature = true;
                    //if (false == ignoreStreamable)
                    //{
                    //    loadFeature = feature.IsStreamable();
                    //}

                    if (true == loadFeature)
                    {
                        //Create a feature value for the current feature
                        //by parsing the value depending on the features
                        //data type.
                        FeatureValue featureValue = null;
                        if (string.Compare(type, "Integer", StringComparison.Ordinal) == 0)
                        {
                            featureValue = new IntegerValue(feature, long.Parse(value.Trim()));
                        }
                        else if (string.Compare(type, "Float", StringComparison.Ordinal) == 0)
                        {
                            featureValue = new FloatValue(feature, double.Parse(value.Trim(), CultureInfo.InvariantCulture));
                        }
                        else if (string.Compare(type, "Enumeration", StringComparison.Ordinal) == 0)
                        {
                            featureValue = new EnumerationValue(feature, value.Trim());
                        }
                        else if (string.Compare(type, "String", StringComparison.Ordinal) == 0)
                        {
                            featureValue = new StringValue(feature, value);
                        }
                        else if (string.Compare(type, "Boolean", StringComparison.Ordinal) == 0)
                        {
                            string trimmedValue = value.Trim();
                            bool b = false;
                            if ((string.Compare(trimmedValue, "true", StringComparison.OrdinalIgnoreCase) == 0)
                                || (string.Compare(trimmedValue, "1", StringComparison.Ordinal) == 0))
                            {
                                b = true;
                            }
                            else if ((string.Compare(trimmedValue, "false", StringComparison.OrdinalIgnoreCase) == 0)
                                    || (string.Compare(trimmedValue, "0", StringComparison.Ordinal) == 0))
                            {
                                b = false;
                            }
                            else
                            {
                                throw new Exception("Invalid camera settings xml file.");
                            }

                            featureValue = new BooleanValue(feature, b);
                        }

                        //Check if we were able to allocate the feature value
                        if (null == featureValue)
                        {
                            throw new Exception("Invalid camera settings xml file.");
                        }

                        //Add the new feature value to the list of feature values
                        featureValues.AddLast(featureValue);
                    }
                    else
                    {
                        //We directly add the feature to the missing features list
                        //if feature is not streamable.
                        currentMissingFeatures.Add(name);
                    }
                }
                else
                {
                    //We directly add the feature to the missing features list
                    //if no feature exists with the given name.
                    currentMissingFeatures.Add(name);
                }
            }

            //Now we try to write all features into the camera
            uint iteration = 0;             //Counter for retries
            bool featuresComplete = false;  //Is true if all features have been set
            bool featuresWritten = true;    //Is true if any feature has been written during the last iteration

            while ((false == featuresComplete)     //Only iterate if we are not done yet
                    && (true == featuresWritten)       //Only iterate if we are not stuck (no features left that can be changed)
                    && (iteration < maxIterations))    //Only iterate until we reach the maximum number of interations/retries
            {
                featuresComplete = true;
                featuresWritten = false;

                //Iterate over all feature values and try to set them
                foreach (FeatureValue featureValue in featureValues)
                {
                    //We only set a feature if it doesn't already contain the target value
                    //if (false == featureValue.IsTargetValue)
                    //{
                    //Remember that there is at least one feature to be done
                    featuresComplete = false;

                    string name = featureValue.Feature.Name;

                    //We only set a feature if the target value can be written at the moment
                    if (featureValue.IsWriteable)
                    {
                        //Write the target value to the feature
                        featureValue.ApplyTargetValue();

                        //Remember that we changed at least one feature
                        featuresWritten = true;
                    }
                    //}
                }

                iteration++;
            }

            //Finally check the contents of all features once more to make sure that all features
            //now contain the according target value.
            foreach (FeatureValue featureValue in featureValues)
            {
                //Add the feature to one of our lists depending on if it contains the target value.
                if (featureValue.IsTargetValue)
                {
                    currentLoadedFeatures.Add(featureValue.Feature.Name);
                }
                else
                {
                    currentMissingFeatures.Add(featureValue.Feature.Name);
                }
            }

            //Return the feature lists to the user if there
            //was no error.
            loadedFeatures = currentLoadedFeatures;
            missingFeatures = currentMissingFeatures;
        }

        //set/get flag, signals a displayed image
        public static bool ImageInUse
        {
            set
            {
                lock (m_ImageInUseSyncLock)
                {
                    m_ImageInUse = value;
                }
            }
            get
            {
                lock (m_ImageInUseSyncLock)
                {
                    return m_ImageInUse;
                }
            }
        }

        //Convert frame to displayable image
        private static Image ConvertFrame(Frame frame)
        {
            if(null == frame)
            {
                throw new ArgumentNullException("frame");
            }

            //Check if the image is valid
            if(VmbFrameStatusType.VmbFrameStatusComplete != frame.ReceiveStatus)
            {
                throw new Exception("Invalid frame received. Reason: " + frame.ReceiveStatus.ToString());
            }

            //define return variable
            Image image = null;
            
            //check if current image is in use,
            //if not we drop the frame to get not in conflict with GUI
            //if (ImageInUse)
            //{
                //Convert raw frame data into image (for image display)
                switch (frame.PixelFormat)
                {
                    case VmbPixelFormatType.VmbPixelFormatMono8:
                        {
                            m_RingBitmap.CopyToNextBitmap_8bppIndexed((int)frame.Width,
                                                                      (int)frame.Height,
                                                                      frame.Buffer);

                            image = m_RingBitmap.Image;
                            ImageInUse = false;
                        }
                        break;

                    case VmbPixelFormatType.VmbPixelFormatBgr8:
                        {
                            m_RingBitmap.CopyToNextBitmap_24bppRgb((int)frame.Width,
                                                                    (int)frame.Height,
                                                                    frame.Buffer);

                            image = m_RingBitmap.Image;
                            ImageInUse = false;
                        }
                        break;

                    default:
                        throw new Exception("Current pixel format is not supported by this example (only Mono8 and BRG8Packed are supported).");
                //}
            }
                    
            return image;
        }

        //Adjust pixel format of given camera to match one that can be displayed
        //in this example.
        private void AdjustPixelFormat(Camera camera)
        {
            if(null == camera)
            {
                throw new ArgumentNullException("camera");
            }

            string[] supportedPixelFormats = new string[] { "BGR8Packed", "Mono8" };
            //Check for compatible pixel format
            Feature pixelFormatFeature = camera.Features["PixelFormat"];

            //Determine current pixel format
            string currentPixelFormat = pixelFormatFeature.EnumValue;

            //Check if current pixel format is supported
            bool currentPixelFormatSupported = false;
            foreach(string supportedPixelFormat in supportedPixelFormats)
            {
                if(string.Compare(currentPixelFormat, supportedPixelFormat, StringComparison.Ordinal) == 0)
                {
                    currentPixelFormatSupported = true;
                    break;
                }
            }

            //Only adjust pixel format if we not already have a compatible one.
            if(false == currentPixelFormatSupported)
            {
                //Determine available pixel formats
                string[] availablePixelFormats = pixelFormatFeature.EnumValues;
                    
                //Check if there is a supported pixel format
                bool pixelFormatSet = false;
                foreach(string supportedPixelFormat in supportedPixelFormats)
                {
                    foreach(string availablePixelFormat in availablePixelFormats)
                    {
                        if(     (string.Compare(supportedPixelFormat, availablePixelFormat, StringComparison.Ordinal) == 0)
                            &&  (pixelFormatFeature.IsEnumValueAvailable(supportedPixelFormat) == true))
                        {
                            //Set the found pixel format
                            pixelFormatFeature.EnumValue = supportedPixelFormat;
                            pixelFormatSet = true;
                            break;
                        }
                    }

                    if(true == pixelFormatSet)
                    {
                        break;
                    }
                }

                if(false == pixelFormatSet)
                {
                    throw new Exception("None of the pixel formats that are supported by this example (Mono8 and BRG8Packed) can be set in the camera.");
                }
            }
        }

        private void OnCameraListChange(VmbUpdateTriggerType reason)
        {
            switch(reason)
            {
            case VmbUpdateTriggerType.VmbUpdateTriggerPluggedIn:
            case VmbUpdateTriggerType.VmbUpdateTriggerPluggedOut:
                {
                    CameraListChangedHandler cameraListChangedHandler = m_CameraListChangedHandler;
                    if(null != cameraListChangedHandler)
                    {
                        cameraListChangedHandler(this, EventArgs.Empty);
                    }
                }
                break;

            default:
                break;
            }
        }

        private void OnFrameReceived(Frame frame)
        {
            try
            {
                //Convert frame into displayable image
                Image image = ConvertFrame(frame);

                FrameReceivedHandler frameReceivedHandler = m_FrameReceivedHandler;
                if (null != frameReceivedHandler && null != image)
                {
                    //Report image to user
                    frameReceivedHandler(this, new FrameEventArgs(image));
                }
            }
            catch(Exception exception)
            {
                FrameReceivedHandler frameReceivedHandler = m_FrameReceivedHandler;
                if(null != frameReceivedHandler)
                {
                    //Report an error to the user
                    frameReceivedHandler(this, new FrameEventArgs(exception));
                }
            }
            finally
            {
                //We make sure to always return the frame to the API
                m_Camera.QueueFrame(frame);
            }
        }

        //Release Camera
        private void ReleaseCamera()
        {
            if(null != m_Camera)
            {
                //We can use cascaded try-finally blocks to release the
                //camera step by step to make sure that every step is executed.
                try
                {
                    try
                    {
                        try
                        {
                            if(null != m_FrameReceivedHandler)
                            {
                                m_Camera.OnFrameReceived -= this.OnFrameReceived;
                            }
                        }
                        finally
                        {
                            m_FrameReceivedHandler = null;
                            if(true == m_Acquiring)
                            {
                                m_Camera.StopContinuousImageAcquisition();
                            }
                        }
                    }
                    finally
                    {
                        m_Acquiring = false;
                        m_Camera.Close();
                    }
                }
                finally
                {
                    m_Camera = null;
                }
            }
        }

        //Release Vimba API
        private void ReleaseVimba()
        {
            if(null != m_Vimba)
            {
                //We can use cascaded try-finally blocks to release the
                //Vimba API step by step to make sure that every step is executed.
                try
                {
                    try
                    {
                        try
                        {
                            //First we release the camera (if there is one)
                            ReleaseCamera(); 
                            m_CameraListChangedHandler = null;
                            m_Vimba.Shutdown();
                        }
                        finally
                        {
                            if(null != m_CameraListChangedHandler)
                            {
                                m_Vimba.OnCameraListChanged -= this.OnCameraListChange;
                            }
                        }
                    }
                    finally
                    {
                        //Now finally shutdown the API
                       
                    }
                }
                finally
                {
                    m_Vimba = null;
                }
            }
        }

        //Start up Vimba API
        public void Startup(CameraListChangedHandler cameraListChangedHandler)
        {
            //Instanciate main Vimba object
            Vimba vimba = new Vimba();

            //Start up Vimba API
            vimba.Startup();
            m_Vimba = vimba;

            bool bError = true;
            try
            {
                //Register camera list change delegate
                if(null != cameraListChangedHandler)
                {
                    m_Vimba.OnCameraListChanged += this.OnCameraListChange;
                    m_CameraListChangedHandler = cameraListChangedHandler;
                }

                bError = false;
            }
            finally
            {
                //Release Vimba API if an error occured
                if(true == bError)
                {
                    ReleaseVimba();
                }
            }
        }

        public void Startup()
        {
            //Instanciate main Vimba object
            Vimba vimba = new Vimba();

            //Start up Vimba API
            vimba.Startup();
            m_Vimba = vimba;

        }


        //Shutdown API
        public void Shutdown()
        {
            //Check if API has been started up at all
            if(null == m_Vimba)
            {
                throw new Exception("Vimba has not been started.");
            }

            ReleaseVimba();
        }

        //Property to get the current camera list
        public List<CameraInfo> CameraList
        {
            get
            {
                //Check if API has been started up at all
                if(null == m_Vimba)
                {
                    throw new Exception("Vimba is not started.");
                }

                List<CameraInfo> cameraList = new List<CameraInfo>();
                CameraCollection cameras = m_Vimba.Cameras;
                foreach(Camera camera in cameras)
                {
                    cameraList.Add(new CameraInfo(camera.Name, camera.Id));
                }

                return cameraList;
            }
        }

        public void StartContinuousImageAcquisition(string id, FrameReceivedHandler frameReceivedHandler)
        {
            //Check parameters
            if(null == id)
            {
                throw new ArgumentNullException("id");
            }

            //Check if API has been started up at all
            if(null == m_Vimba)
            {
                throw new Exception("Digi Ride Capture is not started.");
            }

            //Check if a camera is already open
            if(null != m_Camera)
            {
                throw new Exception("A camera is already open.");
            }

            //Open camera
            m_Camera = m_Vimba.OpenCameraByID(id, VmbAccessModeType.VmbAccessModeFull);
            if(null == m_Camera)
            {
                throw new NullReferenceException("No camera retrieved.");
            }

            bool bError = true;
            try
            {
                //Set a compatible pixel format
                AdjustPixelFormat(m_Camera);

                //Register frame callback
                if(null != frameReceivedHandler)
                {
                    m_Camera.OnFrameReceived += this.OnFrameReceived;
                    m_FrameReceivedHandler = frameReceivedHandler;
                }

                //Reset member variables
                m_RingBitmap = new RingBitmap(m_RingBitmapSize);
                m_ImageInUse = true;
                m_Acquiring = true;

                //Start synchronous image acquisition (grab)
                m_Camera.StartContinuousImageAcquisition(3);
                
                bError = false;
            }
            finally
            {
                //Close camera already if there was an error
                if(true == bError)
                {
                    ReleaseCamera();
                }
            }
        }

        public void StopContinuousImageAcquisition()
        {
            //Check if API has been started up at all
            if(null == m_Vimba)
            {
                throw new Exception("Vimba is not started.");
            }

            //Check if no camera is open
            if(null == m_Camera)
            {
                throw new Exception("No camera open.");
            }

            //Close camera
           // ReleaseCamera();
        }

        public Image AcquireSingleImage(string id)
        {
            //Check parameterbiy
            if (null == id)
            {
                throw new ArgumentNullException("id");
            }

            //Check if API has been started up at all
            if (null == m_Vimba)
            {
                throw new Exception("Vimba is not started.");
            }

            //Open camera
            Camera camera = m_Vimba.OpenCameraByID(id, VmbAccessModeType.VmbAccessModeFull);
            if (null == camera)
            {
                throw new NullReferenceException("No camera retrieved.");
            }

            Frame frame = null;
            try
            {
                //Set a compatible pixel format
                AdjustPixelFormat(camera);

                //Acquire an image synchronously (snap)
                camera.AcquireSingleImage(ref frame, 2000);
            }
            finally
            {
                camera.Close();
            }

            return ConvertFrame(frame);
        }
    }
}
