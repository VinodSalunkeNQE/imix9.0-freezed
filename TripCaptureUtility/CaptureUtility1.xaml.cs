﻿using AVT.VmbAPINET;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace TripCaptureUtility
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CaptureUtility1 : Window
    {
        #region Declaration
        Vimba system = null;
        private VimbaHelper m_VimbaHelper = null;
        CameraCollection cameras;
        TripCamInfo tripCamInfo;
        Camera selCamera1;
        int rotationValue = 0;
        bool running = false;
        string[] camDetails;
        bool showMsg = false;
        int autoTry = 5;
        TripCamBusiness tripCamBusiness = new TripCamBusiness();
        TripCamFeaturesInfo tripCamFeatureInfo;
        BackgroundWorker bw_BusyIndicator = new BackgroundWorker();
        Bitmap b;
        int attempts = 1;
        bool camStarted = false;
        public bool autoStartMode = false;
        System.Windows.Forms.Timer tmrVerifyCamera = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmrSelfStarter = new System.Windows.Forms.Timer();
        #endregion

        #region Constructor
        public CaptureUtility1()
        {
            InitializeComponent();
            GetCameras();
            bw_BusyIndicator.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_BusyIndicator_competed);
            bw_BusyIndicator.DoWork += bw_BusyIndicator_DoWork;
            tmrVerifyCamera.Tick += new EventHandler(tmrVerifyCamera_Tick);
            tmrVerifyCamera.Interval = 60000;//10 minutes

            tmrSelfStarter.Tick += new EventHandler(tmrSelfStarter_Tick);
            tmrSelfStarter.Interval = 30000;
            tmrSelfStarter.Enabled = false;
        }
        #endregion

        #region Timer Ticks
        void tmrSelfStarter_Tick(object sender, EventArgs e)
        {
            try
            {
                attempts++;
                autoStartMode = true;
                tmrVerifyCamera.Stop();
                camStarted = false;
                string atmptMsg = "Attempt number:" + Convert.ToString(attempts) + " Trying to self start the camera at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);

                //lblMsg.Text = atmptMsg;
                if (StartStopCam())
                {
                    //bwRideCapt.RunWorkerAsync();
                    camStarted = true;
                    autoStartMode = false;
                    atmptMsg = "Camera self started at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                    attempts = 0;
                    tmrSelfStarter.Stop();
                    tmrVerifyCamera.Start();
                    camStarted = true;
                }

                if (attempts == autoTry)
                {
                    autoStartMode = false;
                    atmptMsg = "Camera could not be self started after sevral attempts at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                    btnStartStopCapture.IsEnabled = true;
                    tmrSelfStarter.Stop();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            finally
            {
                if (selCamera1 != null)
                    selCamera1.Close();
            }
        }

        void tmrVerifyCamera_Tick(object sender, EventArgs e)
        {
            try
            {
                if (showMsg)
                {
                    if (!VerifyCamera())
                    {
                        showMsg = false;
                        tmrVerifyCamera.Stop();
                        //lblMsg.Text = "Camera error!! Trying to reset.";
                        tmrSelfStarter.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Background Worker
        private void bw_BusyIndicator_competed(object sender, RunWorkerCompletedEventArgs e)
        {
            bool issave = PushSettings();
            grdCapture.IsEnabled = true;
            stkBusyIndicator.Visibility = Visibility.Collapsed;
            if (issave == true)
                MessageBox.Show("Settings successfully loaded from file.");
            if (!String.IsNullOrWhiteSpace(tripCamFeatureInfo.CameraName))
                this.Title = tripCamFeatureInfo.CameraName;
        }

        private void bw_BusyIndicator_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(10000);
        }
        #endregion
        
        #region Events
        private void cmbCameraNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbCameraNames.SelectedIndex > 0)
                {
                    string CamName = cmbCameraNames.SelectedValue.ToString();
                    camDetails = CamName.Split('/');
                    if (ValidateCameraAvailability(camDetails[1]))
                    {
                        if (ValidateCameraRunningStatus(camDetails[1]))
                        {
                            tripCamInfo = tripCamBusiness.GetTripCameraInfoById(camDetails[0], camDetails[1]);
                            List<TripCamSettingInfo> lstTripCamInfo = tripCamBusiness.GetSavedTripCamSettingsForCameraId(tripCamInfo.Camera_pKey);
                            tripCamFeatureInfo = FillTripCamFeaturesFromDb(lstTripCamInfo);
                            string[] camInfoDetails = cmbCameraNames.SelectedValue.ToString().Split('/');
                            selCamera1 = cameras[camDetails[1]];
                            if (!string.IsNullOrWhiteSpace(tripCamFeatureInfo.IPAddress))
                                PushConfigModeSettingsToCamera();

                            grdCapture.IsEnabled = false;
                            stkBusyIndicator.Visibility = Visibility.Visible;
                            bw_BusyIndicator.RunWorkerAsync();
                        }
                        else
                        {
                            MessageBox.Show("Camera is already running");
                            cmbCameraNames.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Camera is not available. Please add camera in Manage Camera Section.");
                        cmbCameraNames.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There is some connection error. Please check digi error log.");
                cmbCameraNames.SelectedIndex = 0;
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                //system.Shutdown();
            }
        }

        private void btnStartStopCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StartStopCam();
            }
            catch (VimbaException vx)
            {
                ErrorHandler.ErrorHandler.LogError(vx);
                //MessageBox.Show(vx.Message);
            }
        }

        private void OnFrameReceived1(AVT.VmbAPINET.Frame frame)
        {
            try
            {
                ConvertFrame1(frame);
            }
            finally
            {
                try
                {
                    selCamera1.QueueFrame(frame);
                }
                catch (VimbaException vx)
                {
                    ErrorHandler.ErrorHandler.LogError(vx);
                    //MessageBox.Show(vx.Message);
                }
            }
        }
        bool first = false;
        private void OnFrameReceived(object sender, FrameEventArgs args)
        {
            try
            {
                
                if (running == true && first==true)
                {
                    Dispatcher.BeginInvoke(new FrameReceivedHandler(this.OnFrameReceived), sender, args);
                    first = false;
                    return;
                }

                if (true == running)
                {
                    System.Drawing.Image image = args.Image;
                    if (null != image)
                    {
                        using (b = new Bitmap(image))
                        {
                            switch (tripCamFeatureInfo.CameraRotation)
                            {
                                case 90:
                                    b.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    break;
                                case 180:
                                    b.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                    break;
                                case 270:
                                    b.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                    break;
                            }
                            
                            b.Save(System.IO.Path.Combine(tripCamInfo.CameraFolderpath, System.Guid.NewGuid().ToString() + ".jpg"));
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                // ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                // MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            system.Shutdown();
            Application.Current.Shutdown();
        }

        #endregion

        #region Common Functions

        private bool VerifyCamera()
        {
            bool res = true;
            try
            {
                selCamera1 = system.OpenCameraByID(selCamera1.Id, VmbAccessModeType.VmbAccessModeFull);
                if (null == selCamera1)
                {
                    res = false;
                }
            }
            catch (VimbaException ex)
            {

            }
            finally
            {
                //selCamera1.Close();
            }
            return res;
        }

        private bool StartStopCam()
        {
            if (cmbCameraNames.SelectedIndex > 0)
            {
                if (!running)
                {
                    StartVimba();
                    //selCamera1.OnFrameReceived += this.OnFrameReceived1;
                    //bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeFull);
                    //if (IsCameraopened)
                    //{
                        /*Please add here parameters like TriggerMode etc.
                        m_camera1.Features["StreamBytesPerSecond"].IntValue = 62000000;
                        m_camera1.Features["GevSCPSPacketSize"].IntValue = 8228;
                        m_camera1.Features["TriggerSource"].EnumValue = "Line 1";
                        m_camera1.Features["TriggerSelector"].EnumValue = "FrameStart";
                        m_camera1.Features["TriggerMode"].EnumValue = "On";
                        m_camera1.Features["SyncOutSelector"].EnumValue = "SyncOut1";
                        m_camera1.Features["SyncOutSource"].EnumValue = "Exposing";
                        ************************************************/
                        //selCamera1.StartContinuousImageAcquisition(3);
                        running = true;
                        btnStartStopCapture.Content = "Stop Capture";
                        showMsg = true;
                        first = true;
                        tmrVerifyCamera.Start();
                    //}
                    //else
                    //{
                    //    running = false;
                    //    MessageBox.Show("Camera cannot be opened, so unable to start acquisition");
                    //}
                }
                else
                {
                    btnStartStopCapture.Content = "Start Capture";
                    //selCamera1.OnFrameReceived -= this.OnFrameReceived1;
                    selCamera1.StopContinuousImageAcquisition();
                    selCamera1.Close();
                    running = false;
                    showMsg = false;
                }
            }
            else
            {
                MessageBox.Show("Please select a camera from the dropdown");
            }
            return running;
        }

        private void StartVimba()
        {
            try
            {
                VimbaHelper vimbaHelper = new VimbaHelper();
                vimbaHelper.Startup();
                m_VimbaHelper = vimbaHelper;
                StartCapture();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                if (!autoStartMode)
                    MessageBox.Show(ex.Message.ToString(), "Imix Ride Processor");
            }
        }

        public void StartCapture()
        {
            try
            {
                List<CameraInfo> cInfo = new List<CameraInfo>();
                cInfo = m_VimbaHelper.CameraList;
                CameraInfo camera = cInfo.Where(f=>f.ID==camDetails[1]).FirstOrDefault();
                startCapturing(camera);
                //showMsg = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //showMsg = false;
                if (!autoStartMode)
                    MessageBox.Show("Camera not found", "ImixRideProcessor");
            }
        }

        public void startCapturing(CameraInfo camera)
        {
            try
            {

                m_VimbaHelper.StartContinuousImageAcquisition(camera.ID, this.OnFrameReceived);
                //  UpdateAcquireButton();

            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message.ToString(), "Imix Ride Processor");
            }
        }

        private void GetCameras()
        {
            system = new Vimba();
            try
            {
                system.Startup();
                cameras = system.Cameras;
                int count = 0;
                while (count < cameras.Count && cameras.Count > 0)
                {
                    Camera cam = cameras[count];
                    try
                    {
                        cam = system.OpenCameraByID(cam.Id, VmbAccessModeType.VmbAccessModeFull);
                        if (null == cam)
                        {
                            MessageBox.Show("No camera retrieved.");
                        }
                        cam.Close();
                        cameras[count].Close();
                    }
                    catch (VimbaException ex)
                    {
                        //MessageBox.Show("Camera is already running. Please check the Capture Utility.");
                        cam.Close();
                        cameras[count].Close();
                        count++;
                        continue;
                    }
                    cmbCameraNames.Items.Add(Convert.ToString(cameras[count].Name + "/" + cameras[count].Id + "/" + cameras[count].SerialNumber));
                    count++;
                }
                //cmbCameraNames.Items.Add("GT3300C/SR-Test/Sr215673812");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                MessageBox.Show(ex.Message);
            }
            //finally
            //{
            //    system.Shutdown();
            //}
        }

        public TripCamFeaturesInfo FillTripCamFeaturesFromDb(List<TripCamSettingInfo> lstTripCamInfo)
        {
            tripCamFeatureInfo = new TripCamFeaturesInfo();
            try
            {
                foreach (TripCamSettingInfo tripFeature in lstTripCamInfo)
                {
                    switch (tripFeature.TripCamSettingsMasterId)
                    {
                        case (int)TripCamFeatures.Height:
                            tripCamFeatureInfo.Height = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.HeightMax:
                            tripCamFeatureInfo.HeightMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetX:
                            tripCamFeatureInfo.OffsetX = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetY:
                            tripCamFeatureInfo.OffsetY = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Width:
                            tripCamFeatureInfo.Width = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WidthMax:
                            tripCamFeatureInfo.WidthMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Hue:
                            tripCamFeatureInfo.Hue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Saturation:
                            tripCamFeatureInfo.Saturation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceBlue:
                            tripCamFeatureInfo.WhiteBalanceBlue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceRed:
                            tripCamFeatureInfo.WhiteBalanceRed = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.NoOfImages:
                            tripCamFeatureInfo.NoOfImages = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerDelay:
                            tripCamFeatureInfo.TriggerDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ExposureTime:
                            tripCamFeatureInfo.ExposureTime = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraRotation:
                            tripCamFeatureInfo.CameraRotation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDelay:
                            tripCamFeatureInfo.StrobeDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDuration:
                            tripCamFeatureInfo.StrobeDuration = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeSource:
                            tripCamFeatureInfo.StrobeSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "FrameTrigger" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDurationMode:
                            tripCamFeatureInfo.StrobeDurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Controlled" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PacketSize:
                            tripCamFeatureInfo.PacketSize = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1500 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.MACAddress:
                            tripCamFeatureInfo.MACAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPConfigurationMode:
                            tripCamFeatureInfo.IPConfigurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Persistent" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPAddress:
                            tripCamFeatureInfo.IPAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Subnet:
                            tripCamFeatureInfo.Subnet = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gateway:
                            tripCamFeatureInfo.Gateway = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PixelFormat:
                            tripCamFeatureInfo.PixelFormat = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "BayerGR12" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerSource:
                            tripCamFeatureInfo.TriggerSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Line1" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.LensFocus:
                            tripCamFeatureInfo.LensFocus = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.LensFocusMax:
                            tripCamFeatureInfo.LensFocusMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Aperture:
                            tripCamFeatureInfo.Aperture = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ApertureMin:
                            tripCamFeatureInfo.ApertureMin = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ApertureMax:
                            tripCamFeatureInfo.ApertureMin = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraName:
                            tripCamFeatureInfo.CameraName = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return tripCamFeatureInfo;
        }

        public bool ValidateCameraRunningStatus(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraRunningStatus(CamMake);
            return result;
        }

        public bool ValidateCameraAvailability(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraAvailability(CamMake);
            return result;
        }

        private void PushConfigModeSettingsToCamera()
        {
            try
            {
                bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeConfig);
                if (IsCameraopened)
                {
                    selCamera1.Features["GevIPConfigurationMode"].EnumValue = tripCamFeatureInfo.IPConfigurationMode;

                    if (tripCamFeatureInfo.IPConfigurationMode.ToLower() == "persistent")
                    {
                        selCamera1.Features["GevPersistentDefaultGateway"].IntValue = IP2Long(tripCamFeatureInfo.Gateway);

                        selCamera1.Features["GevPersistentIPAddress"].IntValue = IP2Long(tripCamFeatureInfo.IPAddress);

                        selCamera1.Features["GevPersistentSubnetMask"].IntValue = IP2Long(tripCamFeatureInfo.Subnet);
                    }

                    selCamera1.Features["GevIPConfigurationApply"].RunCommand();
                }
                else
                {
                    MessageBox.Show("Camera cannot be opened. Please check the camera");
                }
            }
            catch (VimbaException vex)
            {
                ErrorHandler.ErrorHandler.LogError(vex);
            }
            finally
            {
                selCamera1.Close();
            }
        }

        private long IP2Long(string IP)
        {
            double num = 0;
            try
            {
                string[] IpBytes;

                if (!string.IsNullOrEmpty(IP))
                {
                    IpBytes = IP.Split('.');
                    Array.Reverse(IpBytes);
                    for (int i = IpBytes.Length - 1; i >= 0; i--)
                    {
                        num += ((int.Parse(IpBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return (long)num;
        }

        private bool OpenCamera(VmbAccessModeType OpenMode)
        {
            bool IsCamOpened = false;
            try
            {
                //Check parameters
                if (null == selCamera1.Id)
                {
                    ErrorHandler.ErrorHandler.LogError(new ArgumentNullException("camera id"));
                }

                //Open camera
                selCamera1 = system.OpenCameraByID(selCamera1.Id, OpenMode);
                if (null == selCamera1)
                {
                    ErrorHandler.ErrorHandler.LogError(new ArgumentNullException("No camera retrieved."));
                }
                else
                {
                    IsCamOpened = true;
                }
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
                MessageBox.Show(ve.Message);
            }
            return IsCamOpened;
        }

        private bool PushSettings()
        {
            bool res = false;
            try
            {
                //VmbAccessModeType accessMode = camera.PermittedAccess;
                string fileName = selCamera1.Id + (".xml");
                if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull))// & accessMode))
                {
                    //Now get the camera ID
                    string cameraID = selCamera1.Id;

                    //Try to open the camera

                    bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeFull);

                    if (IsCameraopened)
                    {
                        //Load settings from file
                        StringCollection loadedFeatures = null;
                        StringCollection missingFeatures = null;
                        bool ignoreStreamable = false;
                        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        fileName = System.IO.Path.Combine(path, fileName);
                        if (System.IO.File.Exists(fileName))
                        {
                            Helper.LoadFromFile(selCamera1, fileName, out loadedFeatures, out missingFeatures, ignoreStreamable);
                            res = true;
                            //MessageBox.Show("Settings successfully loaded from file.");
                        }
                        else
                        {
                            MessageBox.Show("No settings saved for this camera. Please save settings through Camera Settings Utility.");
                            cmbCameraNames.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Camera cannot be opened. Please check the connection");
                    }
                }
            }
            catch (VimbaException vx)
            {
                ErrorHandler.ErrorHandler.LogError(vx);
                MessageBox.Show(vx.Message);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                selCamera1.Close();
            }
            return res;
        }

        private void ConvertFrame1(AVT.VmbAPINET.Frame frame)
        {
            try
            {
                if (null == frame)
                {
                    ErrorHandler.ErrorHandler.LogError(new Exception("frame"));
                    throw new ArgumentNullException("frame");
                }

                if (VmbFrameStatusType.VmbFrameStatusComplete != frame.ReceiveStatus)
                {
                    ErrorHandler.ErrorHandler.LogError(new Exception("Invalid frame received. Reason: " + frame.ReceiveStatus.ToString()));
                }
                Bitmap bitmap = null;
                bitmap = new Bitmap((int)frame.Width, (int)frame.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                frame.Fill(ref bitmap);
                switch (tripCamFeatureInfo.CameraRotation)
                {
                    case 90:
                        bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    case 180:
                        bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    case 270:
                        bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                }
                System.Drawing.Image img = bitmap;
                string path = System.IO.Path.Combine(tripCamInfo.CameraFolderpath, (frame.FrameID + DateTime.Now.ToString("HHmmss") + ".jpg"));
                //string path = System.IO.Path.Combine(@"D:\", ("TESTIMG" + ".jpg"));
                img.Save(path);
                //CompressImage(path);
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
                MessageBox.Show(ve.Message);
            }
        }

        private void CompressImage(string fileName)
        {
            string FilePath = string.Empty;
            string items = fileName;
            double MaxLength = 4.8;
            try
            {
                //string[] moreMessage = OtherMessage.Split('{');
                string filename = System.IO.Path.GetFileName(items);
                if (filename != "Thumbs.db")
                {
                    FilePath = items;
                    string CompressedImageDirectory = "";
                    FileInfo emailFile = new FileInfo(items);
                    if (emailFile.Length > MaxLength * 1024 * 1024)
                    {
                        CompressedImageDirectory = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "CompressedImages");
                        if (!Directory.Exists(CompressedImageDirectory))
                            Directory.CreateDirectory(CompressedImageDirectory);
                        //Calculate imageCompression

                        decimal imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(MaxLength * 1024 * 1024));
                        // decimal imageRatio = decimal.Divide(emailFile.Length,2621440);

                        decimal imageCompression = 100 / imageRatio;
                        imageCompression = Math.Round(imageCompression);
                        FilePath = System.IO.Path.Combine(CompressedImageDirectory, filename);

                        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                        imgCompress.GetImage = new System.Drawing.Bitmap(items);
                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                        imgCompress.Save(filename, CompressedImageDirectory);

                        //code to compress the email untill its size is reduce to 3 MB


                        string imagefile = System.IO.Path.Combine(CompressedImageDirectory, filename);
                        string getCompressedFile = System.IO.Path.GetFileName(imagefile);
                        FileInfo compressedEmailFile = new FileInfo(imagefile);

                        while (compressedEmailFile.Length > MaxLength * 1024 * 1024)
                        {
                            double divisor = MaxLength * 1024 * 1024;
                            imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(divisor));
                            // decimal imageRatio = decimal.Divide(emailFile.Length,2621440);

                            imageCompression = 100 / imageRatio;
                            imageCompression = Math.Round(imageCompression);
                            FilePath = System.IO.Path.Combine(CompressedImageDirectory, filename);

                            imgCompress = ImageCompress.GetImageCompressObject;
                            imgCompress.GetImage = new System.Drawing.Bitmap(items);
                            imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                            imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                            imgCompress.Save(filename, CompressedImageDirectory);
                            MaxLength = MaxLength - 0.2;
                            imagefile = System.IO.Path.Combine(CompressedImageDirectory, filename);
                            getCompressedFile = System.IO.Path.GetFileName(imagefile);
                            compressedEmailFile = new FileInfo(imagefile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

    }
}
