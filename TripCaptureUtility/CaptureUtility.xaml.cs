﻿using AVT.VmbAPINET;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using System.ComponentModel;
using System.IO;

namespace TripCaptureUtility
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CaptureUtility : Window
    {
        #region Declaration
        Vimba system = new Vimba();
        CameraCollection cameras;
        TripCamInfo tripCamInfo;
        Camera selCamera1;
        int rotationValue = 0;
        bool running = false;
        string[] camDetails;
        bool showMsg = false;
        int autoTry = 5;
        TripCamBusiness tripCamBusiness = new TripCamBusiness();
        TripCamFeaturesInfo tripCamFeatureInfo;
        BackgroundWorker bw_BusyIndicator = new BackgroundWorker();
        int attempts = 1;
        bool camStarted = false;
        public bool autoStartMode = false;
        bool IsPushSettings = false;
        System.Windows.Forms.Timer tmrVerifyCamera = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmrSelfStarter = new System.Windows.Forms.Timer();
        #endregion

        #region Constructor
        public CaptureUtility()
        {
            InitializeComponent();
            GetCameras();

            bw_BusyIndicator.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_BusyIndicator_competed);
            bw_BusyIndicator.DoWork += bw_BusyIndicator_DoWork;

            tmrVerifyCamera.Tick += new EventHandler(tmrVerifyCamera_Tick);
            tmrVerifyCamera.Interval = 30000;//0.5 minutes

            tmrSelfStarter.Tick += new EventHandler(tmrSelfStarter_Tick);
            tmrSelfStarter.Interval = 30000;
            tmrSelfStarter.Enabled = false;
        }
        #endregion

        #region Timer Ticks
        void tmrSelfStarter_Tick(object sender, EventArgs e)
        {
            try
            {
                attempts++;
                autoStartMode = true;
                tmrVerifyCamera.Stop();
                camStarted = false;
                string atmptMsg = "Attempt number:" + Convert.ToString(attempts) + " Trying to self start the camera at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);

                if (StartStopCam())
                {
                    camStarted = true;
                    autoStartMode = false;
                    atmptMsg = "Camera self started at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                    attempts = 0;
                    tmrSelfStarter.Stop();
                    tmrVerifyCamera.Start();
                    camStarted = true;
                }

                //if (attempts == autoTry)
                //{
                //    autoStartMode = false;
                //    atmptMsg = "Camera could not be self started after sevral attempts at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                //    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                //    btnStartStopCapture.IsEnabled = true;
                //    tmrSelfStarter.Stop();
                //}
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                if (selCamera1 != null)
                    selCamera1.Close();
            }
        }

        void tmrVerifyCamera_Tick(object sender, EventArgs e)
        {
            try
            {
                if (showMsg)
                {
                    int returnCode = 0;
                    bool IsCameraVerified = VerifyCamera(ref returnCode);
                    if ((!IsCameraVerified && returnCode == -3) || (IsCameraVerified && returnCode == 0))
                    {
                        IsPushSettings = true;
                        showMsg = false;
                        tmrVerifyCamera.Stop();
                        tmrSelfStarter.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Background Worker
        private void bw_BusyIndicator_competed(object sender, RunWorkerCompletedEventArgs e)
        {
            bool issave = PushSettings();
            grdCapture.IsEnabled = true;
            stkBusyIndicator.Visibility = Visibility.Collapsed;
            if (issave == true)
                MessageBox.Show("Settings successfully loaded from file.");
            if (!String.IsNullOrWhiteSpace(tripCamFeatureInfo.CameraName))
                this.Title = tripCamFeatureInfo.CameraName;
        }

        private void bw_BusyIndicator_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(15000);
        }
        #endregion

        #region Events
        private void cmbCameraNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbCameraNames.SelectedIndex > 0)
                {
                    string CamName = cmbCameraNames.SelectedValue.ToString();
                    camDetails = CamName.Split('/');
                    if (ValidateCameraAvailability(camDetails[1]))
                    {
                        if (ValidateCameraRunningStatus(camDetails[1]))
                        {
                            tripCamInfo = tripCamBusiness.GetTripCameraInfoById(camDetails[0], camDetails[1]);
                            List<TripCamSettingInfo> lstTripCamInfo = tripCamBusiness.GetSavedTripCamSettingsForCameraId(tripCamInfo.Camera_pKey);
                            tripCamFeatureInfo = FillTripCamFeaturesFromDb(lstTripCamInfo);
                            string[] camInfoDetails = cmbCameraNames.SelectedValue.ToString().Split('/');
                            selCamera1 = cameras[camDetails[1]];
                            if (!string.IsNullOrWhiteSpace(tripCamFeatureInfo.IPAddress))
                                PushConfigModeSettingsToCamera();

                            grdCapture.IsEnabled = false;
                            stkBusyIndicator.Visibility = Visibility.Visible;
                            bw_BusyIndicator.RunWorkerAsync();
                        }
                        else
                        {
                            MessageBox.Show("Camera is already running");
                            cmbCameraNames.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Camera is not available. Please add camera in Manage Camera Section.");
                        cmbCameraNames.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There is some connection error. Please check digi error log.");
                cmbCameraNames.SelectedIndex = 0;
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnStartStopCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StartStopCam();
            }
            catch (VimbaException vx)
            {
                ErrorHandler.ErrorHandler.LogError(vx);
            }
        }

        private void OnFrameReceived1(AVT.VmbAPINET.Frame frame)
        {
            try
            {
                ConvertFrame1(frame);
            }
            finally
            {
                try
                {
                    this.selCamera1.QueueFrame(frame);
                }
                catch (VimbaException vx)
                {
                    ErrorHandler.ErrorHandler.LogError(vx);
                    ErrorHandler.ErrorHandler.LogFileWrite(vx.ReturnCode.ToString());
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            system.Shutdown();
            Application.Current.Shutdown();
        }

        #endregion

        #region Private Functions

        private bool VerifyCamera(ref int returnCode)
        {
            bool res = true;
            try
            {
                selCamera1 = system.OpenCameraByID(selCamera1.Id, VmbAccessModeType.VmbAccessModeFull);
                if (null == selCamera1)
                {
                    res = false;
                }
            }
            catch (VimbaException ex)
            {
                returnCode = ex.ReturnCode;
                if (returnCode == -6)
                    res = true;
                else if (returnCode == -3)
                    res = false;
            }
            return res;
        }

        private bool StartStopCam()
        {
            if (cmbCameraNames.SelectedIndex > 0)
            {
                if (!running)
                {
                    selCamera1.OnFrameReceived += this.OnFrameReceived1;
                    int returnCode = 0;
                    bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeFull, ref returnCode);
                    if (IsCameraopened)
                    {
                        if (IsPushSettings)
                        {
                            PushSettingsForOpenedCamera();
                            IsPushSettings = false;
                        }
                        /*Please add here parameters like TriggerMode etc.
                        m_camera1.Features["StreamBytesPerSecond"].IntValue = 62000000;
                        m_camera1.Features["GevSCPSPacketSize"].IntValue = 8228;
                        ************************************************/
                        selCamera1.StartContinuousImageAcquisition(10);
                        running = true;
                        btnStartStopCapture.Content = "Stop Capture";
                        showMsg = true;
                        tmrVerifyCamera.Start();
                        cmbCameraNames.IsEnabled = false;
                        lblCamStatus.Content = "*Image Capturing has started";
                    }
                    else
                    {
                        running = false;
                        if (returnCode == -6)
                            MessageBox.Show("Camera is already in use, so unable to start acquisition");
                        else if (returnCode == -3)
                        {
                            lblCamStatus.Content = "*Camera Not Connected";
                        }
                        else
                            MessageBox.Show("Camera cannot be opened, so unable to start acquisition");
                    }
                }
                else
                {
                    cmbCameraNames.IsEnabled = true;
                    btnStartStopCapture.Content = "Start Capture";
                    selCamera1.OnFrameReceived -= this.OnFrameReceived1;
                    selCamera1.Close();
                    running = false;
                    showMsg = false;
                    lblCamStatus.Content = "*Image Capturing has stopped";
                    //selCamera1.StopContinuousImageAcquisition();
                }
            }
            else
            {
                MessageBox.Show("Please select a camera from the dropdown");
            }
            return running;
        }

        private void GetCameras()
        {
            try
            {
                system.Startup();
                cameras = system.Cameras;
                int count = 0;
                while (count < cameras.Count && cameras.Count > 0)
                {
                    Camera cam = cameras[count];
                    try
                    {
                        cam = system.OpenCameraByID(cam.Id, VmbAccessModeType.VmbAccessModeFull);
                        if (null == cam)
                        {
                            MessageBox.Show("No camera retrieved.");
                        }
                        cam.Close();
                        cameras[count].Close();
                    }
                    catch (VimbaException ex)
                    {
                        cam.Close();
                        cameras[count].Close();
                        count++;
                        continue;
                    }
                    cmbCameraNames.Items.Add(Convert.ToString(cameras[count].Name + "/" + cameras[count].Id + "/" + cameras[count].SerialNumber));
                    count++;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private TripCamFeaturesInfo FillTripCamFeaturesFromDb(List<TripCamSettingInfo> lstTripCamInfo)
        {
            tripCamFeatureInfo = new TripCamFeaturesInfo();
            try
            {
                foreach (TripCamSettingInfo tripFeature in lstTripCamInfo)
                {
                    switch (tripFeature.TripCamSettingsMasterId)
                    {
                        case (int)TripCamFeatures.Height:
                            tripCamFeatureInfo.Height = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.HeightMax:
                            tripCamFeatureInfo.HeightMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetX:
                            tripCamFeatureInfo.OffsetX = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetY:
                            tripCamFeatureInfo.OffsetY = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Width:
                            tripCamFeatureInfo.Width = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WidthMax:
                            tripCamFeatureInfo.WidthMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Hue:
                            tripCamFeatureInfo.Hue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Saturation:
                            tripCamFeatureInfo.Saturation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceBlue:
                            tripCamFeatureInfo.WhiteBalanceBlue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceRed:
                            tripCamFeatureInfo.WhiteBalanceRed = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.NoOfImages:
                            tripCamFeatureInfo.NoOfImages = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerDelay:
                            tripCamFeatureInfo.TriggerDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ExposureTime:
                            tripCamFeatureInfo.ExposureTime = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraRotation:
                            tripCamFeatureInfo.CameraRotation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDelay:
                            tripCamFeatureInfo.StrobeDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDuration:
                            tripCamFeatureInfo.StrobeDuration = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeSource:
                            tripCamFeatureInfo.StrobeSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "FrameTrigger" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDurationMode:
                            tripCamFeatureInfo.StrobeDurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Controlled" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PacketSize:
                            tripCamFeatureInfo.PacketSize = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1500 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.MACAddress:
                            tripCamFeatureInfo.MACAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPConfigurationMode:
                            tripCamFeatureInfo.IPConfigurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Persistent" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPAddress:
                            tripCamFeatureInfo.IPAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Subnet:
                            tripCamFeatureInfo.Subnet = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gateway:
                            tripCamFeatureInfo.Gateway = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PixelFormat:
                            tripCamFeatureInfo.PixelFormat = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "BayerGR12" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerSource:
                            tripCamFeatureInfo.TriggerSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Line1" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.LensFocus:
                            tripCamFeatureInfo.LensFocus = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.LensFocusMax:
                            tripCamFeatureInfo.LensFocusMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Aperture:
                            tripCamFeatureInfo.Aperture = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ApertureMin:
                            tripCamFeatureInfo.ApertureMin = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ApertureMax:
                            tripCamFeatureInfo.ApertureMin = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraName:
                            tripCamFeatureInfo.CameraName = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return tripCamFeatureInfo;
        }

        private bool ValidateCameraRunningStatus(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraRunningStatus(CamMake);
            return result;
        }

        private bool ValidateCameraAvailability(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraAvailability(CamMake);
            return result;
        }

        private void PushConfigModeSettingsToCamera()
        {
            try
            {
                int returnCode = 0;
                bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeConfig, ref returnCode);
                if (IsCameraopened)
                {
                    selCamera1.Features["GevIPConfigurationMode"].EnumValue = tripCamFeatureInfo.IPConfigurationMode;

                    if (tripCamFeatureInfo.IPConfigurationMode.ToLower() == "persistent")
                    {
                        selCamera1.Features["GevPersistentDefaultGateway"].IntValue = IP2Long(tripCamFeatureInfo.Gateway);

                        selCamera1.Features["GevPersistentIPAddress"].IntValue = IP2Long(tripCamFeatureInfo.IPAddress);

                        selCamera1.Features["GevPersistentSubnetMask"].IntValue = IP2Long(tripCamFeatureInfo.Subnet);
                    }

                    selCamera1.Features["GevIPConfigurationApply"].RunCommand();
                }
                else
                {
                    MessageBox.Show("Camera cannot be opened. Please check the camera");
                }
            }
            catch (VimbaException vex)
            {
                ErrorHandler.ErrorHandler.LogError(vex);
            }
            finally
            {
                selCamera1.Close();
            }
        }

        private long IP2Long(string IP)
        {
            double num = 0;
            try
            {
                string[] IpBytes;

                if (!string.IsNullOrEmpty(IP))
                {
                    IpBytes = IP.Split('.');
                    Array.Reverse(IpBytes);
                    for (int i = IpBytes.Length - 1; i >= 0; i--)
                    {
                        num += ((int.Parse(IpBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return (long)num;
        }

        private bool OpenCamera(VmbAccessModeType OpenMode, ref int returnCode)
        {
            bool IsCamOpened = false;
            try
            {
                //Check parameters
                if (null == selCamera1.Id)
                {
                    ErrorHandler.ErrorHandler.LogError(new ArgumentNullException("camera id"));
                }

                //Open camera
                selCamera1 = system.OpenCameraByID(selCamera1.Id, OpenMode);
                if (null == selCamera1)
                {
                    ErrorHandler.ErrorHandler.LogError(new ArgumentNullException("No camera retrieved."));
                }
                else
                {
                    IsCamOpened = true;
                    lblCamStatus.Content = "*Camera Connected Successfully";
                }
            }
            catch (VimbaException ve)
            {
                returnCode = ve.ReturnCode;
                ErrorHandler.ErrorHandler.LogError(ve);
                lblCamStatus.Content = "*Camera Not Connected";
            }
            return IsCamOpened;
        }

        private bool PushSettings()
        {
            bool res = false;
            try
            {
                string fileName = selCamera1.Id + (".xml");
                if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull))
                {
                    string cameraID = selCamera1.Id;
                    System.Threading.Thread.Sleep(8000);
                    //Try to open the camera
                    int returnCode = 0;
                    bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeFull, ref returnCode);
                    ErrorHandler.ErrorHandler.LogFileWrite("Camera Open Status is " + IsCameraopened.ToString());
                    if (IsCameraopened)
                    {
                        //Load settings from file
                        StringCollection loadedFeatures = null;
                        StringCollection missingFeatures = null;
                        bool ignoreStreamable = false;
                        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        fileName = System.IO.Path.Combine(path, fileName);
                        if (System.IO.File.Exists(fileName))
                        {
                            Helper.LoadFromFile(selCamera1, fileName, out loadedFeatures, out missingFeatures, ignoreStreamable);
                            res = true;
                        }
                        else
                        {
                            MessageBox.Show("No settings saved for this camera. Please save settings through Camera Settings Utility.");
                            cmbCameraNames.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Camera cannot be opened. Please check the connection");
                    }
                }
            }
            catch (VimbaException vx)
            {
                ErrorHandler.ErrorHandler.LogError(vx);
                MessageBox.Show(vx.Message);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                selCamera1.Close();
            }
            return res;
        }

        private bool PushSettingsForOpenedCamera()
        {
            bool res = false;
            string fileName = selCamera1.Id + (".xml");

            //Load settings from file
            StringCollection loadedFeatures = null;
            StringCollection missingFeatures = null;
            bool ignoreStreamable = false;
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            fileName = System.IO.Path.Combine(path, fileName);
            if (System.IO.File.Exists(fileName))
            {
                Helper.LoadFromFile(selCamera1, fileName, out loadedFeatures, out missingFeatures, ignoreStreamable);
                res = true;
            }
            else
            {
                MessageBox.Show("No settings saved for this camera. Please save settings through Camera Settings Utility.");
                cmbCameraNames.SelectedIndex = 0;
            }
            return res;
        }

        private void ConvertFrame1(AVT.VmbAPINET.Frame frame)
        {
            try
            {
                if (null == frame)
                {
                    ErrorHandler.ErrorHandler.LogError(new Exception("frame"));
                    throw new ArgumentNullException("frame");
                }
                if (VmbFrameStatusType.VmbFrameStatusComplete != frame.ReceiveStatus)
                {
                    ErrorHandler.ErrorHandler.LogError(new Exception("Invalid frame received. Reason: " + frame.ReceiveStatus.ToString()));
                }
                Bitmap bitmap = null;
                bitmap = new Bitmap((int)frame.Width, (int)frame.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                frame.Fill(ref bitmap);
                switch (tripCamFeatureInfo.CameraRotation)
                {
                    case 90:
                        bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    case 180:
                        bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    case 270:
                        bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                }
                System.Drawing.Image img = bitmap;
                string path = System.IO.Path.Combine(tripCamInfo.CameraFolderpath, (frame.FrameID + DateTime.Now.ToString("HHmmss") + ".jpg"));
                img.Save(path);
                //CompressImage(path);
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
                MessageBox.Show(ve.Message);
            }
        }

        private void CompressImage(string fileName)
        {
            string FilePath = string.Empty;
            string items = fileName;
            double MaxLength = 4.8;
            try
            {
                string filename = System.IO.Path.GetFileName(items);
                if (filename != "Thumbs.db")
                {
                    FilePath = items;
                    string CompressedImageDirectory = "";
                    FileInfo emailFile = new FileInfo(items);
                    if (emailFile.Length > MaxLength * 1024 * 1024)
                    {
                        CompressedImageDirectory = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "CompressedImages");
                        if (!Directory.Exists(CompressedImageDirectory))
                            Directory.CreateDirectory(CompressedImageDirectory);
                        //Calculate imageCompression

                        decimal imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(MaxLength * 1024 * 1024));
                        // decimal imageRatio = decimal.Divide(emailFile.Length,2621440);

                        decimal imageCompression = 100 / imageRatio;
                        imageCompression = Math.Round(imageCompression);
                        FilePath = System.IO.Path.Combine(CompressedImageDirectory, filename);

                        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                        imgCompress.GetImage = new System.Drawing.Bitmap(items);
                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                        imgCompress.Save(filename, CompressedImageDirectory);

                        //code to compress the email untill its size is reduce to 3 MB


                        string imagefile = System.IO.Path.Combine(CompressedImageDirectory, filename);
                        string getCompressedFile = System.IO.Path.GetFileName(imagefile);
                        FileInfo compressedEmailFile = new FileInfo(imagefile);

                        while (compressedEmailFile.Length > MaxLength * 1024 * 1024)
                        {
                            double divisor = MaxLength * 1024 * 1024;
                            imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(divisor));
                            // decimal imageRatio = decimal.Divide(emailFile.Length,2621440);

                            imageCompression = 100 / imageRatio;
                            imageCompression = Math.Round(imageCompression);
                            FilePath = System.IO.Path.Combine(CompressedImageDirectory, filename);

                            imgCompress = ImageCompress.GetImageCompressObject;
                            imgCompress.GetImage = new System.Drawing.Bitmap(items);
                            imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                            imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                            imgCompress.Save(filename, CompressedImageDirectory);
                            MaxLength = MaxLength - 0.2;
                            imagefile = System.IO.Path.Combine(CompressedImageDirectory, filename);
                            getCompressedFile = System.IO.Path.GetFileName(imagefile);
                            compressedEmailFile = new FileInfo(imagefile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

    }
}