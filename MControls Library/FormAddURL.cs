﻿using System;
using System.Windows.Forms;
using MPLATFORMLib;

namespace MControls
{
    public partial class FormAddURL : Form
    {

        public string sourceName;
        public FormAddURL()
        {
            InitializeComponent();
        }

        public string url;
        public string _parameters;
        
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            url = urlBox.Text;
            _parameters = parameters.Text;
        }

        private void FormAddURL_Load(object sender, EventArgs e)
        {
            urlBox.Items.Clear();
            MSendersClass pSenders = new MSendersClass();
            int nCount = 0;
            pSenders.SendersGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                string strName;
                M_VID_PROPS vidProps;
                M_AUD_PROPS audProps;
                pSenders.SendersGetByIndex(i, out strName, out vidProps, out audProps);
                if (sourceName == strName) continue;
                urlBox.Items.Add(@"mp://" + strName);
            }
            urlBox.SelectedIndex = -1;
        }

    }
}
