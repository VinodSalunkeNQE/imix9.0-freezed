using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MControls
{
	public partial class FormChromaKey : Form
	{
	public 	MCHROMAKEYLib.MChromaKey m_pChromaKey;

		public FormChromaKey()
		{
			InitializeComponent();
		}

		public FormChromaKey(object pChromaKey)
		{
            try
            {
                InitializeComponent();
                m_pChromaKey = (MCHROMAKEYLib.MChromaKey)pChromaKey;
                this.DoubleBuffered = true;
            }
            catch { }
		}

		private void FormChromaKey_Load(object sender, EventArgs e)
		{ try
            {
			mChromaKey1.SetControlledObject(m_pChromaKey);
            }
        catch { }
		}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


	}
}