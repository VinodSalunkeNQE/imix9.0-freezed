﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    class ConfigurationValueProcessor : BaseProcessor
    {

        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    int substoreid = ChangeInfo.SubStore.SubStoreId;
                    ConfigurationValueInfo configInfo = CommonUtility.DeserializeXML<ConfigurationValueInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        //var existingconfigCount = dbContext.iIMIXConfigurationMasters.Count(r => r.SyncCode == configInfo.configuartionInfoList.);

                        foreach (ConfigurationValueInfo cv in configInfo.configuartionInfoList)
                        {
                            var masterid = dbContext.iIMIXConfigurationMasters.Where(r => r.SyncCode == cv.MasterSyncCode).FirstOrDefault();
                            if (masterid != null)
                            {
                                if (cv.IMIXConfigurationMasterId == 112)
                                {
                                    var currency = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();

                                    if (currency != null)
                                    {
                                        var _objcurrency = dbContext.DG_Currency.Where(t => t.DG_Currency_pkey == currency.DG_Currency_pkey).FirstOrDefault();
                                        if (_objcurrency != null)
                                        {
                                            _objcurrency.DG_Currency_Default = true;
                                            foreach (var item in dbContext.DG_Currency.Where(t => t.DG_Currency_pkey != currency.DG_Currency_pkey).ToList())
                                            {
                                                item.DG_Currency_Default = false;
                                                item.IsSynced = true;
                                            }
                                        }
                                        dbContext.SaveChanges();
                                    }

                                }
                                var abc = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId && r.SubstoreId == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();
                                if (abc == null)
                                {


                                    if (cv.IMIXConfigurationMasterId == 112)
                                    {
                                        var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId).FirstOrDefault();
                                        if (abcd != null)
                                        {
                                            var configvalue = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();
                                            abcd.ConfigurationValue = configvalue.DG_Currency_pkey.ToString();
                                            abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                            abcd.SyncCode = cv.SyncCode;
                                            abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                            abcd.IsSynced = true;
                                            dbContext.iMIXConfigurationValues.Add(abcd);
                                            dbContext.SaveChanges();
                                        }
                                    }



                                    else if (cv.IMIXConfigurationMasterId == 73)
                                    {

                                        var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId).FirstOrDefault();
                                        if (abcd != null)
                                        {
                                            var configvalue = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == cv.ProductSyncCode).FirstOrDefault();
                                            abcd.ConfigurationValue = configvalue.DG_Orders_ProductType_pkey.ToString();
                                            abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                            abcd.SyncCode = cv.SyncCode;
                                            abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                            abcd.IsSynced = true;
                                            dbContext.iMIXConfigurationValues.Add(abcd);
                                            dbContext.SaveChanges();
                                        }
                                    }

                                    else
                                    {
                                        if(masterid.DataType=="BIT")
                                        {
                                            if(cv.ConfigurationValue=="1")
                                            {
                                                iMIXConfigurationValue configvalues = new iMIXConfigurationValue();
                                                configvalues.ConfigurationValue = "true";
                                                configvalues.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                configvalues.SyncCode = cv.SyncCode;
                                                configvalues.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                configvalues.IsSynced = true;
                                                dbContext.iMIXConfigurationValues.Add(configvalues);
                                                dbContext.SaveChanges();
                                            }
                                            else
                                            {
                                                iMIXConfigurationValue configvalues = new iMIXConfigurationValue();
                                                configvalues.ConfigurationValue = "False";
                                                configvalues.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                configvalues.SyncCode = cv.SyncCode;
                                                configvalues.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                configvalues.IsSynced = true;
                                                dbContext.iMIXConfigurationValues.Add(configvalues);
                                                dbContext.SaveChanges();

                                            }
                                           
                                        }
                                        else
                                        {
                                            iMIXConfigurationValue configvalues = new iMIXConfigurationValue();
                                            configvalues.ConfigurationValue = cv.ConfigurationValue;
                                            configvalues.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                            configvalues.SyncCode = cv.SyncCode;
                                            configvalues.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                            configvalues.IsSynced = true;
                                            dbContext.iMIXConfigurationValues.Add(configvalues);
                                            dbContext.SaveChanges();
                                        }
                                      
                                    }
                                }

                                else
                                {
                                    if (cv.IMIXConfigurationMasterId == 112)
                                    {

                                        var currency = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();

                                        if (currency != null)
                                        {
                                            var _objcurrency = dbContext.DG_Currency.Where(t => t.DG_Currency_pkey == currency.DG_Currency_pkey).FirstOrDefault();
                                            if (_objcurrency != null)
                                            {
                                                _objcurrency.DG_Currency_Default = true;
                                                foreach (var item in dbContext.DG_Currency.Where(t => t.DG_Currency_pkey != currency.DG_Currency_pkey).ToList())
                                                {
                                                    item.IsSynced = true;
                                                    item.DG_Currency_Default = false;
                                                }
                                            }
                                            dbContext.SaveChanges();
                                        }
                                    }
                                    var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId && r.SubstoreId == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();
                                    if (abcd != null)
                                    {
                                        if (cv.IMIXConfigurationMasterId == 112)
                                        {
                                            var configvalue = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();
                                            abcd.ConfigurationValue = configvalue.DG_Currency_pkey.ToString();
                                            abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                            abcd.SyncCode = cv.SyncCode;
                                            abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                            abcd.IsSynced = true;
                                            dbContext.SaveChanges();
                                        }

                                        else if (cv.IMIXConfigurationMasterId == 73)
                                        {
                                                var configvalue = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == cv.ProductSyncCode).FirstOrDefault();
                                                abcd.ConfigurationValue = configvalue.DG_Orders_ProductType_pkey.ToString();
                                                abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                abcd.SyncCode = cv.SyncCode;
                                                abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                abcd.IsSynced = true;
                                                dbContext.SaveChanges();
                                            
                                        }


                                        else
                                        {
                                            if (masterid.DataType == "BIT")
                                            {
                                                if (cv.ConfigurationValue == "1")
                                                {

                                                    abcd.ConfigurationValue = "true";
                                                    abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                    abcd.SyncCode = cv.SyncCode;
                                                    abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                    abcd.IsSynced = true;
                                                    dbContext.iMIXConfigurationValues.Add(abcd);
                                                    dbContext.SaveChanges();
                                                }
                                                else
                                                {

                                                    abcd.ConfigurationValue = "False";
                                                    abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                    abcd.SyncCode = cv.SyncCode;
                                                    abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                    abcd.IsSynced = true;
                                                    dbContext.iMIXConfigurationValues.Add(abcd);
                                                    dbContext.SaveChanges();

                                                }

                                            }

                                            else
                                            {
                                                abcd.ConfigurationValue = cv.ConfigurationValue;
                                                abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                abcd.SyncCode = cv.SyncCode;
                                                abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                abcd.IsSynced = true;
                                                dbContext.SaveChanges();
                                            }
                                        }
                                    }


                                }
                            }

                        }





                    }

                    configupdate();
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in configuration Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.configurationProcessor";
                throw outerException;
            }

        }

        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    int substoreid = ChangeInfo.SubStore.SubStoreId;
                    ConfigurationValueInfo configInfo = CommonUtility.DeserializeXML<ConfigurationValueInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {

                        foreach (ConfigurationValueInfo cv in configInfo.configuartionInfoList)
                        {
                            if (cv.IMIXConfigurationMasterId == 112)
                            {
                                var currency = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();

                                if (currency != null)
                                {
                                    var _objcurrency = dbContext.DG_Currency.Where(t => t.DG_Currency_pkey == currency.DG_Currency_pkey).FirstOrDefault();
                                    if (_objcurrency != null)
                                    {
                                        _objcurrency.DG_Currency_Default = true;
                                        foreach (var item in dbContext.DG_Currency.Where(t => t.DG_Currency_pkey != currency.DG_Currency_pkey).ToList())
                                        {
                                            item.DG_Currency_Default = false;
                                            item.IsSynced = true;
                                        }
                                    }
                                    dbContext.SaveChanges();
                                }
                            }

                            var masterid = dbContext.iIMIXConfigurationMasters.Where(r => r.SyncCode == cv.MasterSyncCode).FirstOrDefault();
                            if (masterid != null)
                            {
                                if (cv.IMIXConfigurationMasterId == 112)
                                {
                                    var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId).FirstOrDefault();
                                    if (abcd != null)
                                    {
                                        var configvalue = dbContext.DG_Currency.Where(r => r.SyncCode == cv.currencySyncCode).FirstOrDefault();
                                        abcd.ConfigurationValue = configvalue.DG_Currency_pkey.ToString();
                                        abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                        abcd.SyncCode = cv.SyncCode;
                                        abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                        abcd.IsSynced = true;
                                        dbContext.SaveChanges();
                                    }
                                }

                                else if(cv.IMIXConfigurationMasterId == 73)
                                {

                                    var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId).FirstOrDefault();
                                    if (abcd != null)
                                    {
                                        var configvalue = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == cv.ProductSyncCode).FirstOrDefault();
                                        abcd.ConfigurationValue = configvalue.DG_Orders_ProductType_pkey.ToString();
                                        abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                        abcd.SyncCode = cv.SyncCode;
                                        abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                        abcd.IsSynced = true;
                                        dbContext.SaveChanges();
                                    }
                                }
                                else
                                {
                                    var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId && r.SubstoreId == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();
                                    if (abcd != null)
                                    {

                                        if (masterid.DataType == "BIT")
                                        {
                                            if (cv.ConfigurationValue == "1")
                                            {

                                                abcd.ConfigurationValue = "true";
                                                abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                abcd.SyncCode = cv.SyncCode;
                                                abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                abcd.IsSynced = true;
                                                dbContext.iMIXConfigurationValues.Add(abcd);
                                                dbContext.SaveChanges();
                                            }
                                            else
                                            {

                                                abcd.ConfigurationValue = "False";
                                                abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                                abcd.SyncCode = cv.SyncCode;
                                                abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                                abcd.IsSynced = true;
                                                dbContext.iMIXConfigurationValues.Add(abcd);
                                                dbContext.SaveChanges();

                                            }

                                        }
                                        else
                                        {
                                            abcd.ConfigurationValue = cv.ConfigurationValue;
                                            abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                            abcd.SyncCode = cv.SyncCode;
                                            abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                            abcd.IsSynced = true;
                                            dbContext.SaveChanges();
                                        
                                        }

                                       
                                    }
                                }
                                //var abcd = dbContext.iMIXConfigurationValues.Where(r => r.IMIXConfigurationMasterId == masterid.IMIXConfigurationMasterId && r.SubstoreId == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();
                                //if (abcd != null)
                                //{
                                //    abcd.ConfigurationValue = cv.ConfigurationValue;
                                //    abcd.IMIXConfigurationMasterId = Convert.ToInt64(masterid.IMIXConfigurationMasterId);
                                //    abcd.SyncCode = cv.SyncCode;
                                //    abcd.SubstoreId = ChangeInfo.SubStore.SubStoreId;
                                //    dbContext.SaveChanges();
                                //}
                            }
                        }

                    }
                    configupdate();
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Configuration Processor Update.", e);
                outerException.Source = "ClientDataProcessor.ConfigurationProcessor";
                throw outerException;
            }
        }


        protected override void ProcessDelete()
        {
            try
            {


                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbconfig = dbContext.iMIXConfigurationValues.Where(r => r.SubstoreId == ChangeInfo.SubStore.SubStoreId).ToList();
                    if (dbconfig != null)
                    {
                        foreach (var item in dbconfig)
                        {
                            dbContext.iMIXConfigurationValues.Remove(item);
                            dbContext.SaveChanges();
                        }
                    }
                    var dgconfig = dbContext.DG_Configuration.Where(r => r.DG_Substore_Id == ChangeInfo.SubStore.SubStoreId).ToList();
                    if (dgconfig != null)
                    {
                        foreach (var item in dgconfig)
                        {
                            dbContext.DG_Configuration.Remove(item);
                            dbContext.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Configuration Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.ConfigurationProcessor";
                throw outerException;
            }
        }

        public void configupdate()
        {
            using (DigiPhotoContext dbContext = new DigiPhotoContext())
            {
                int substoreid = ChangeInfo.SubStore.SubStoreId;
                ConfigurationValueInfo configInfo = CommonUtility.DeserializeXML<ConfigurationValueInfo>(ChangeInfo.dataXML);
                DG_Configuration config = dbContext.DG_Configuration.Where(r => r.DG_Substore_Id == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();
                bool isExists = true;
                if (config == null)
                {
                    isExists = false;
                    config = new DG_Configuration();
                    config.DG_Substore_Id = ChangeInfo.SubStore.SubStoreId;
                    config.IsSynced = true;
                }
                foreach (ConfigurationValueInfo cv in configInfo.configuartionInfoList)
                {

                    switch (cv.IMIXConfigurationMasterId)
                    {
                        case 1:
                            config.DG_Hot_Folder_Path = cv.ConfigurationValue;
                            break;
                        case 2:
                            config.DG_Frame_Path = cv.ConfigurationValue;
                            break;
                        case 3:
                            config.DG_BG_Path = cv.ConfigurationValue;
                            break;
                        case 4:
                            config.DG_Mod_Password = cv.ConfigurationValue;
                            break;
                        case 5:
                            config.DG_NoOfPhotos = Convert.ToInt32(cv.ConfigurationValue);
                            break;
                        case 6:
                            config.DG_Watermark = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 7:
                            config.DG_SemiOrder = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 8:
                            config.DG_HighResolution = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 9:
                            config.DG_AllowDiscount = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 10:
                            config.DG_EnableDiscountOnTotal = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 11:
                            config.WiFiStartingNumber = Convert.ToDecimal(cv.ConfigurationValue);
                            break;
                        case 12:
                            config.FolderStartingNumber = Convert.ToDecimal(cv.ConfigurationValue);
                            break;
                        case 13:
                            config.IsAutoLock = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 14:
                            config.DG_SemiOrderMain = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 15:
                            config.PosOnOff = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 16:
                            config.DG_ReceiptPrinter = cv.ConfigurationValue;
                            break;
                        case 17:
                            config.DG_IsAutoRotate = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 18:
                            config.DG_Graphics_Path = cv.ConfigurationValue;
                            break;
                        case 19:
                            config.DG_IsCompression = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 20:
                            config.DG_IsEnableGroup = Convert.ToBoolean(cv.ConfigurationValue);
                            break;
                        case 21:
                            config.DG_Substore_Id = ChangeInfo.SubStore.SubStoreId;
                            break;
                        case 22:
                            config.DG_NoOfBillReceipt = Convert.ToInt32(cv.ConfigurationValue);
                            break;
                        case 23:
                            config.DG_ChromaColor = cv.ConfigurationValue;
                            break;
                        case 24:
                            config.DG_ChromaTolerance = Convert.ToDecimal(cv.ConfigurationValue);
                            break;
                        case 31:
                            config.DG_PageCountGrid = Convert.ToInt32(cv.ConfigurationValue);
                            break;
                        case 32:
                            config.DG_PageCountPreview = Convert.ToInt32(cv.ConfigurationValue);
                            break;
                        case 33:
                            config.DG_NoOfPhotoIdSearch = Convert.ToInt32(cv.ConfigurationValue);
                            break;

                        default:
                            break;


                    }
                }
                if (isExists)
                {

                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.DG_Configuration.Add(config);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}

