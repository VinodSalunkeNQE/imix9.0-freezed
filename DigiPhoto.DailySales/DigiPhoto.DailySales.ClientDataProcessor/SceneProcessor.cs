﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using log4net;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public class SceneProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (ChangeInfo != null && !string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    SceneInfo sceneInfo = CommonUtility.DeserializeXML<SceneInfo>(ChangeInfo.dataXML);

                    if (sceneInfo != null)
                    {
                        using (DigiPhotoContext dbContext = new DigiPhotoContext())
                        {
                            ProductInfo product = (from p in dbContext.DG_Orders_ProductType
                                                   where p.SyncCode == sceneInfo.ProductSyncCode
                                                   select new ProductInfo
                                                   {
                                                       ProductId = p.DG_Orders_ProductType_pkey
                                                   }
                                                  ).FirstOrDefault();

                            var existingSceneCount = dbContext.DG_Scene.Count(r => r.SyncCode == sceneInfo.SyncCode);
                            var background = dbContext.DG_BackGround.FirstOrDefault(bg => bg.SyncCode == sceneInfo.BackgroundSyncCode);
                            var border = dbContext.DG_Borders.FirstOrDefault(bg => bg.SyncCode == sceneInfo.BorderSyncCode);

                            if (existingSceneCount == 0)
                            {
                                if (product != null)
                                {
                                    if (background != null)
                                    {
                                        if (border != null)
                                        {
                                            var scene = new DG_Scene();
                                            scene.DG_BackGround_Id = Convert.ToInt32(background.DG_Background_pkey);
                                            scene.DG_Border_Id = Convert.ToInt32(border.DG_Borders_pkey);
                                            scene.DG_Graphics_Id = 0;
                                            scene.DG_Product_Id = Convert.ToInt32(product.ProductId);
                                            scene.DG_SceneName = sceneInfo.SceneName;
                                            scene.IsActive = sceneInfo.IsActive;
                                            scene.IsSynced = sceneInfo.IsSynced;
                                            scene.SyncCode = sceneInfo.SyncCode;
                                            dbContext.DG_Scene.Add(scene);
                                            dbContext.SaveChanges();
                                            log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: The scene was successfully inserted to local database");
                                        }
                                        else
                                        {
                                            log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: This border doesn't exists in the local database");
                                        }
                                    }
                                    else
                                    {
                                        log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: This background doesn't exists in the local database");
                                    }
                                }
                                else
                                {
                                    log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: This Product doesn't exists in the local database");
                                }
                            }
                            else
                            {
                                log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: This scene already exists in the local database");
                            }
                        }
                    }
                    else
                    {
                        log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: SceneInfo was null. DataXML couldn't be parsed into SceneInfo");
                    }
                }
                else
                {
                    log.Error("ClientDataProcessor.ScenedProcessor:ProcessInsert:: Message: Change Info and  DataXML was null");
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Scene Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.SceneProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (ChangeInfo != null && !string.IsNullOrEmpty(ChangeInfo.dataXML))
                {

                    SceneInfo sceneInfo = CommonUtility.DeserializeXML<SceneInfo>(ChangeInfo.dataXML);
                    if (sceneInfo != null)
                    {
                        using (DigiPhotoContext dbContext = new DigiPhotoContext())
                        {
                            ProductInfo product = (from p in dbContext.DG_Orders_ProductType
                                                   where p.SyncCode == sceneInfo.ProductSyncCode
                                                   select new ProductInfo
                                                   {
                                                       ProductId = p.DG_Orders_ProductType_pkey
                                                   }
                                                   ).FirstOrDefault();

                            var background = dbContext.DG_BackGround.FirstOrDefault(bg => bg.SyncCode == sceneInfo.BackgroundSyncCode);
                            var border = dbContext.DG_Borders.FirstOrDefault(bg => bg.SyncCode == sceneInfo.BorderSyncCode);
                            var dbScene = dbContext.DG_Scene.Where(r => r.SyncCode == sceneInfo.SyncCode).FirstOrDefault();
                            if (dbScene != null)
                            {
                                if (product != null)
                                {
                                    if (background != null)
                                    {
                                        if (border != null)
                                        {
                                            dbScene.DG_Border_Id = Convert.ToInt32(border.DG_Borders_pkey);
                                            dbScene.DG_BackGround_Id = Convert.ToInt32(background.DG_Background_pkey);
                                            dbScene.DG_Product_Id = Convert.ToInt32(sceneInfo.ProductId);
                                            dbScene.DG_SceneName = sceneInfo.SceneName;
                                            dbScene.IsActive = sceneInfo.IsActive;
                                            dbScene.IsSynced = sceneInfo.IsSynced;
                                            dbScene.SyncCode = sceneInfo.SyncCode;
                                            dbContext.SaveChanges();
                                            log.Error("ClientDataProcessor.ScenedProcessor:ProcessUpdate:: Message: The scene was successfully updated to local database");
                                        }
                                        else
                                        {
                                            log.Error("ClientDataprocessor.ScenedProcessor:ProcessUpdate:: Message: This border doesn't exists in the Central database");
                                        }
                                    }
                                    else
                                    {
                                        log.Error("ClientDataprocessor.ScenedProcessor:ProcessUpdate:: Message: This background doesn't exists in the local database");
                                    }
                                }
                                else
                                {
                                    log.Error("ClientDataprocessor.ScenedProcessor:ProcessUpdate:: Message: This Product doesn't exists in the local database");
                                }

                            }
                            else
                            {
                                log.Error("ClientDataProcessor.ScenedProcessor:ProcessUpdate:: Message: This scene doesn't exists in the local database");
                            }
                        }
                    }
                    else
                    {
                        log.Error("ClientDataProcessor.ScenedProcessor:ProcessUpdate:: Message: SceneInfo was null. DataXML couldn't be parsed into SceneInfo");
                    }
                }
                else
                {
                    log.Error("ClientDataProcessor.ScenedProcessor:ProcessUpdate:: Message: Change Info and  DataXML was null");
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Scene Processor Update.", e);
                outerException.Source = "ClientDataProcessor.SceneProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                if (ChangeInfo != null && !string.IsNullOrEmpty(ChangeInfo.EntityCode))
                {
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {

                        var dbScene = dbContext.DG_Scene.FirstOrDefault(r => r.SyncCode == ChangeInfo.EntityCode);
                        if (dbScene != null)
                        {
                            dbContext.DG_Scene.Remove(dbScene);
                            dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                            log.Error("ScenedProcessor:ProcessDelete:: Message: The scene was successfully deleted from the local database");
                        }
                        else
                        {
                            log.Error("ClientDataProcessor.ScenedProcessor:ProcessDelete:: Message: This scene doesn't exists in the local database.");
                        }
                    }
                }
                else
                {
                    log.Error("ClientDataProcessor.ScenedProcessor:ProcessDelete:: Message: Change Info and  DataXML was null");
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Scene Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.SceneProcessor";
                throw outerException;
            }
        }

    }
}
