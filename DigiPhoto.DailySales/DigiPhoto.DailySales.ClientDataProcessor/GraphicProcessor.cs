﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System.IO;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public class GraphicProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    GraphicInfo graphicInfo = CommonUtility.DeserializeXML<GraphicInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingGraphicCount = dbContext.DG_Graphics.Count(r => r.SyncCode == graphicInfo.SyncCode);
                        if (existingGraphicCount == 0)
                        {
                            var Graphics = new DG_Graphics();
                            Graphics.DG_Graphics_Name = graphicInfo.Name;
                            Graphics.DG_Graphics_Displayname = graphicInfo.DisplayName;
                            Graphics.DG_Graphics_IsActive = graphicInfo.IsActive;
                            Graphics.SyncCode = graphicInfo.SyncCode;
                            Graphics.IsSynced = true;
                            Graphics.CreatedBy = 1;
                            Graphics.ModifiedBy = 1;
                            Graphics.CreatedDate = DateTime.Now;
                            Graphics.ModifiedDate = DateTime.Now;
                            dbContext.DG_Graphics.Add(Graphics);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Graphic Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.GraphicProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    GraphicInfo graphicInfo = CommonUtility.DeserializeXML<GraphicInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbGraphics = dbContext.DG_Graphics.Where(r => r.SyncCode == graphicInfo.SyncCode).FirstOrDefault();
                        if (dbGraphics != null)
                        {
                            //Update Role Name
                            dbGraphics.DG_Graphics_Name = graphicInfo.Name;
                            dbGraphics.DG_Graphics_Displayname = graphicInfo.DisplayName;
                            dbGraphics.DG_Graphics_IsActive = graphicInfo.IsActive;
                            dbGraphics.SyncCode = graphicInfo.SyncCode;
                            dbGraphics.ModifiedBy = 1;
                            dbGraphics.ModifiedDate = DateTime.Now;
                            dbGraphics.IsSynced = true;
                            dbContext.SaveChanges();
                            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Graphic Processor Update.", e);
                outerException.Source = "ClientDataProcessor.GraphicProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {

                    var dbGraphics = dbContext.DG_Graphics.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbGraphics != null)
                    {
                        dbContext.DG_Graphics.Remove(dbGraphics);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                    }
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Graphic Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.GraphicProcessor";
                throw outerException;
            }
        }
    }
}
