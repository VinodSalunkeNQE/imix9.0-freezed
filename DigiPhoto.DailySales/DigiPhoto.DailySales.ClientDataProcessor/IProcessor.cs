﻿using DigiPhoto.DailySales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public interface IProcesser
    {
        void Process(ChangeInfo changeInfo);
    }
}
