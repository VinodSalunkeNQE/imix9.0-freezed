﻿using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class VersionUpdateSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var clientVersion = dbContext.ApplicationClientVersion.Where(r => r.ApplicationClientVersionID == ObjectValueId).FirstOrDefault();

                    if (clientVersion != null)
                    {
                        ApplicationClientVersionInfo versionUpdateInfo = new ApplicationClientVersionInfo()
                        {
                            ApplicationVersionID = clientVersion.ApplicationVersionID,
                            MACAddress = clientVersion.MACAddress,
                            MachineName = clientVersion.MachineName,
                            IPAddress = clientVersion.IPAddress,
                            UpdatedDate = clientVersion.UpdatedDate,
                            Country = clientVersion.Country,
                            Store = clientVersion.Store,
                            SubStore = clientVersion.SubStore,
                            Status = clientVersion.Status,
                            SyncCode = clientVersion.SyncCode,
                            Description = clientVersion.Description
                        };

                        dataXML = CommonUtility.SerializeObject<ApplicationClientVersionInfo>(versionUpdateInfo);
                    }
                    return dataXML;
                }
            }

            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Currency Serialization.", e);
                outerException.Source = "Serialization.CurrencySerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
