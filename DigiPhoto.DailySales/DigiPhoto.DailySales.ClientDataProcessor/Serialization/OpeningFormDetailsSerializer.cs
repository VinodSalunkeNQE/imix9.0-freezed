﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using DigiPhoto.DailySales.ClientDataProcessor.Controller;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class OpeningFormDetailsSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    string FilledBySyncCode = string.Empty;
                    string errorPhotoId = string.Empty;
                    DG_SubStores subStoreInfo = new DG_SubStores();
                    subStoreInfo = (
                                     from openingFormDetails in dbContext.OpeningFormDetails
                                     join subStore in dbContext.DG_SubStores on openingFormDetails.SubStoreID equals subStore.DG_SubStore_pkey
                                     where openingFormDetails.OpeningFormDetailID == ObjectValueId
                                     select subStore).FirstOrDefault();
                    if (subStoreInfo == null)
                    {
                        subStoreInfo = new DG_SubStores();
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                        subStoreInfo.SyncCode = string.Empty;
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                    }
                    FilledBySyncCode = (
                                     from openingFormDetails in dbContext.OpeningFormDetails
                                     join users in dbContext.DG_Users on openingFormDetails.FilledBy equals users.DG_User_pkey
                                     where openingFormDetails.OpeningFormDetailID == ObjectValueId
                                     select users.SyncCode).FirstOrDefault();
                    var openingFormDetail = dbContext.OpeningFormDetails.Where(r => r.OpeningFormDetailID == ObjectValueId).FirstOrDefault();
                    if (openingFormDetail != null)
                    {
                        OpeningFormDetailsInfo openingFormDetailsInfo = new OpeningFormDetailsInfo()
                        {
                            OpeningFormDetailID = openingFormDetail.OpeningFormDetailID,
                            BussinessDate = openingFormDetail.BussinessDate,
                            StartingNumber6X8 = openingFormDetail.StartingNumber6X8,
                            StartingNumber8X10 = openingFormDetail.StartingNumber8X10,
                            PosterStartingNumber = openingFormDetail.PosterStartingNumber,
                            AutoOpening6X8PrinterCount = openingFormDetail.Auto6X8StartingNumber,
                            AutoOpening8x10PrinterCount = openingFormDetail.Auto8X10StartingNumber,
                            CashFloatAmount = openingFormDetail.CashFloatAmount,
                            SubStoreID = openingFormDetail.SubStoreID,
                            OpeningDate = openingFormDetail.OpeningDate,
                            FilledBy = openingFormDetail.FilledBy,
                            FilledBySyncCode = FilledBySyncCode,
                            SyncCode=openingFormDetail.SyncCode,
                            SubStore = new SubStoreInfo
                            {
                                SubStoreCode = subStoreInfo.DG_SubStore_Name,
                                SyncCode = subStoreInfo.SyncCode,
                                Store = subStoreInfo.DG_SubStore_Name
                            },
                        };
                        dataXML = CommonUtility.SerializeObject<OpeningFormDetailsInfo>(openingFormDetailsInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in OpeningFormDetails Serialization.", e);
                outerException.Source = "Serialization.OpeningFormDetailsinfoSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
