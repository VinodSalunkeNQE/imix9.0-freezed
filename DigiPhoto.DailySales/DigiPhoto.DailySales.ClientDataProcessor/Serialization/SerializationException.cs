﻿using DigiPhoto.DailySales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    class SerializationException : BaseException
    {
        public SerializationException()
            : base()
        {
        }
        public SerializationException(string message)
            : base(message)
        {
        }
        public SerializationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
