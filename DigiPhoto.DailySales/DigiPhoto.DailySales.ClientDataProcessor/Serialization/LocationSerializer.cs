﻿using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class LocationSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    // SubStore Details Fetch
                    var Store = (from store in dbContext.DG_Store
                                 select store).FirstOrDefault();
                    var subStore = (
                                     from SubStore in dbContext.DG_SubStores
                                     //join SubLoc in dbContext.DG_SubStore_Locations on SubStore.DG_SubStore_pkey equals SubLoc.DG_SubStore_ID
                                     //join Loc in dbContext.DG_Location on SubLoc.DG_Location_ID equals Loc.DG_Location_pkey
                                     //join st in dbContext.DG_Store on Loc.DG_Store_ID equals st.DG_Store_pkey
                                     where SubStore.DG_SubStore_pkey == ObjectValueId
                                     select new { mysubstore = SubStore}//, mySubLoc = SubLoc, myLoc = Loc, myStore = st }
                                     ).FirstOrDefault();
                   
                     var location = (
                                     from SubStore in dbContext.DG_SubStores
                                     join SubLoc in dbContext.DG_SubStore_Locations on SubStore.DG_SubStore_pkey equals SubLoc.DG_SubStore_ID
                                     join Loc in dbContext.DG_Location on SubLoc.DG_Location_ID equals Loc.DG_Location_pkey
                                     join st in dbContext.DG_Store on Loc.DG_Store_ID equals st.DG_Store_pkey
                                     where SubStore.DG_SubStore_pkey == ObjectValueId
                                     select Loc
                                     
                                     ).ToList();
                   

                   

                   
                    SiteInfo siteinfo = new SiteInfo();
                    if (subStore != null)
                    {
                        var logicalsubStoreCode = (
                                      from SubStore in dbContext.DG_SubStores
                                      where SubStore.DG_SubStore_pkey == subStore.mysubstore.LogicalSubStoreID
                                      select SubStore).FirstOrDefault();

                        siteinfo.SiteName = subStore.mysubstore.DG_SubStore_Name;
                        siteinfo.SiteCode = subStore.mysubstore.SyncCode;
                        siteinfo.SubStoreId = subStore.mysubstore.DG_SubStore_pkey;
                        siteinfo.DG_SiteCode = subStore.mysubstore.DG_SubStore_Code;
                        siteinfo.IsLogicalSubStore = subStore.mysubstore.IsLogicalSubStore;
                        if (logicalsubStoreCode != null)
                        {
                            siteinfo.LogicalSubStoreDetails = new SiteInfo();    
                            siteinfo.LogicalSubStoreDetails.SubStoreId = logicalsubStoreCode.DG_SubStore_pkey;
                            siteinfo.LogicalSubStoreDetails.SiteName = logicalsubStoreCode.DG_SubStore_Name;
                            siteinfo.LogicalSubStoreDetails.SiteCode = logicalsubStoreCode.SyncCode;
                        }
                        StoreInfo St = new StoreInfo();
                        St.StoreName = Store.DG_Store_Name;// subStore.myStore.DG_Store_Name;
                        St.StoreCode = Store.StoreCode;// subStore.myStore.StoreCode;
                        CountryInfo Country = new CountryInfo();
                        Country.CountryName = Store.Country;// subStore.myStore.Country;
                        Country.CountryCode = Store.CountryCode;// subStore.myStore.CountryCode;
                        St.Country = Country;
                        siteinfo.Store = St;
                        List<LocationInfo> LIF = new List<LocationInfo>();


                        foreach (DG_Location dg in location)
                        {
                          LocationInfo Location = new LocationInfo();
                          Location.Name = dg.DG_Location_Name;
                          Location.SyncCode = dg.SyncCode;
                          Location.DigiMasterLocationId = dg.DG_Location_pkey;
                          Location.IsActive =Convert.ToBoolean(dg.DG_Location_IsActive);// Convert.ToBoolean(subStore.myLoc.DG_Location_IsActive);
                          LIF.Add(Location);
                         

                        }

                        siteinfo.SubstorelocationDetails = LIF;
                        

                        dataXML = CommonUtility.SerializeObject<SiteInfo>(siteinfo);
                    }
                }
                return dataXML;
            }

            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Order Serialization.", e);
                outerException.Source = "Serialization.SubStoreSerializer";
                throw outerException;
            }

        }

        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
