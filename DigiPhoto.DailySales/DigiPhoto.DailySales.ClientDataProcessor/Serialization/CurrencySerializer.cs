﻿using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class CurrencySerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var currency = dbContext.DG_Currency.Where(r => r.DG_Currency_pkey == ObjectValueId).FirstOrDefault();
                  
                    if (currency != null)
                    {
                        var user = dbContext.DG_Users.Where(r => r.DG_User_pkey == currency.DG_Currency_ModifiedBy).FirstOrDefault();
                        if(user == null)
                        {
                            user = dbContext.DG_Users.Where(r => r.DG_User_Name.ToUpper().Equals("ADMIN")).FirstOrDefault();
                        }
                        CurrencyInfo currencyInfo = new CurrencyInfo()
                        {
                          Name = currency.DG_Currency_Name,
                          Rate = currency.DG_Currency_Rate,
                          Symbol = currency.DG_Currency_Symbol,
                          ModifiedDateTime = currency.DG_Currency_UpdatedDate,
                          ModifiedBy = currency.DG_Currency_ModifiedBy,
                          UserSyncCode=user.SyncCode,
                          CurrencyCode = currency.DG_Currency_Code,
                          CurrencyIcon = currency.DG_Currency_Icon,
                          IsDefault = Convert.ToBoolean(currency.DG_Currency_Default),
                          SyncCode = currency.SyncCode,
                        };

                        dataXML = CommonUtility.SerializeObject<CurrencyInfo>(currencyInfo);
                    }
                    return dataXML;
                }
            }

            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Currency Serialization.", e);
                outerException.Source = "Serialization.CurrencySerializer";
                throw outerException;
            }
        }

        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
