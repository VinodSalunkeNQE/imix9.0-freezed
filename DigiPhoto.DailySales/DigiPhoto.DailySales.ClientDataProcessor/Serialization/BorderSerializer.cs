﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class BorderSerializer : Serializer, IUploadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var border = dbContext.DG_Borders.Where(r => r.DG_Borders_pkey == ObjectValueId).FirstOrDefault();
                    // Code Added by Anis for Border sync issue
                    //var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode.ToString()).FirstOrDefault();
                     var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode).FirstOrDefault();
                    //End
                    if (border != null)
                    {
                        BordersInfo borderInfo = new BordersInfo()
                        {
                            Name = border.DG_Border,
                            ProductId = border.DG_ProductTypeID,
                            IsActive = border.DG_IsActive,
                            SyncCode = border.SyncCode,
                            ModifiedDate = System.DateTime.Now,
                            SiteCodeSyncCode = siteSynccode,
                        };
                        if (border.CreatedBy.HasValue)
                            borderInfo.CreatedBy = border.CreatedBy.Value;
                        if (border.CreatedDate.HasValue)
                            borderInfo.CreatedDate = border.CreatedDate.Value;
                        else
                            borderInfo.CreatedDate = DateTime.Now;

                        if (border.ModifiedBy.HasValue)
                            borderInfo.ModifiedBy = border.ModifiedBy.Value;

                        var configurationData = dbContext.DG_Configuration.FirstOrDefault();
                        if (configurationData != null)
                        {
                            //string Folderpath = ConfigurationManager.AppSettings["DigiphotoBorderPath"].ToString();
                            uploadFile = new UploadFile();

                            uploadFile.UploadFilePath = Path.Combine(configurationData.DG_Frame_Path, border.DG_Border);
                            uploadFile.UploadThumbnailFilePath = Path.Combine(configurationData.DG_Frame_Path, "Thumbnails", border.DG_Border);
                            //uploadFile.SaveFolderPath = Path.Combine(Folderpath);
                            //uploadFile.SaveThumbnailFolderPath = Path.Combine(Folderpath, "Thumbnails");
                            uploadFile.FileType = 1;
                        }
                        dataXML = CommonUtility.SerializeObject<BordersInfo>(borderInfo);
                    }

                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Border Serialization.", e);
                outerException.Source = "Serialization.BorderSerializer";
                throw outerException;
            }
        }
        private UploadFile uploadFile;
        public UploadFile UploadFile
        {
            get
            {
                return uploadFile;
            }
            set
            {
                uploadFile = value;
            }
        }
        private List<UploadInfo> uploadList;
        public List<UploadInfo> UploadList
        {
            get
            {
                return uploadList;
            }
            set
            {
                uploadList = value;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

    }
}
