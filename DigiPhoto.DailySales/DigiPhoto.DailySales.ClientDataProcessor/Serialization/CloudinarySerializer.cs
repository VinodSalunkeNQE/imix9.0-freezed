﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using DigiPhoto.DailySales.ClientDataProcessor.Controller;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class CloudinarySerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    string errorPhotoId = string.Empty;
                    var cloudinary = dbContext.CloudinaryDtls.Where(r => r.CloudinaryInfoID == ObjectValueId).FirstOrDefault();
                    if (cloudinary != null)
                    {
                        //Check Cloudinary Status before Cloudinary Serilization
                        if(cloudinary.CloudinaryStatusID != 1)
                        {
                            //Retry to upload on Cloudinary
                            int defaultWidth = ImageHelper.GetDefaultWidth;
                            int defaultHeight = ImageHelper.GetDefaultHeight;

                            UploadFileInfo obj = new UploadFileInfo
                            {
                                Title = System.IO.Path.GetFileName(cloudinary.UploadCompressedFilePath),
                                ImagePath = cloudinary.UploadCompressedFilePath,
                                TargetDirectory = cloudinary.TargetDirectory,
                                ImageDefaultHeight = defaultHeight,
                                ImageDefaultWidth = defaultWidth,
                                //PhotoId = cloudinary.PhotoID,
                            };
                            CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, out errorPhotoId);
                            //Update Cloudinary upload status in CloudinaryDtl table for same CloudinaryInfoID
                            cloudinary.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                            cloudinary.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                            cloudinary.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                            cloudinary.RetryCount += 1;
                            cloudinary.ModifiedDateTime = DateTime.Now;
                            cloudinary.Height = objCloudinaryUploadInfo.Height;
                            cloudinary.Width = objCloudinaryUploadInfo.Width;
                            cloudinary.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                            dbContext.SaveChanges();
                        }
                        
                        var subStoreSyncCode = (
                                     from Cloudinary in dbContext.CloudinaryDtls
                                     join Photos in dbContext.DG_Photos on Cloudinary.PhotoID equals Photos.DG_Photos_pkey
                                     join SubStore in dbContext.DG_SubStores on Photos.DG_SubStoreId equals SubStore.DG_SubStore_pkey
                                     join Order in dbContext.DG_Orders on Cloudinary.OrderId equals Order.DG_Orders_pkey
                                     where Cloudinary.CloudinaryInfoID == ObjectValueId
                                     select new
                                     {
                                         SyncCode=SubStore.SyncCode,
                                         OrderNumber = Order.DG_Orders_Number
                                     }).FirstOrDefault();


                        CloudinaryInfo cloudinaryinfo = new CloudinaryInfo();
                        cloudinaryinfo.CloudinaryInfoID = cloudinary.CloudinaryInfoID;
                        cloudinaryinfo.PhotoID = cloudinary.PhotoID;
                        cloudinaryinfo.SourceImageID = cloudinary.SourceImageID;
                        cloudinaryinfo.CloudinaryStatusID = cloudinary.CloudinaryStatusID;
                        cloudinaryinfo.CloudinaryPublicID = cloudinary.CloudinaryPublicID;
                        cloudinaryinfo.RetryCount = cloudinary.RetryCount;
                        cloudinaryinfo.ErrorMessage = cloudinary.ErrorMessage;
                        cloudinaryinfo.AddedBy = cloudinary.AddedBy;
                        cloudinaryinfo.CreatedDateTime = cloudinary.CreatedDateTime;
                        cloudinaryinfo.ModifiedBy = cloudinary.ModifiedBy;
                        cloudinaryinfo.ModifiedDateTime = cloudinary.ModifiedDateTime;
                        cloudinaryinfo.IsActive = cloudinary.IsActive;
                        cloudinaryinfo.SubStoreSyncCode = subStoreSyncCode.SyncCode;
                        cloudinaryinfo.SyncCode = cloudinary.SyncCode;
                        cloudinaryinfo.OrderNumber = subStoreSyncCode.OrderNumber;
                        cloudinaryinfo.IdentificationCode = cloudinary.IdentificationCode;
                        cloudinaryinfo.Height = cloudinary.Height;
                        cloudinaryinfo.Width = cloudinary.Width;
                        cloudinaryinfo.ThumbnailDimension = cloudinary.ThumbnailDimension;
                        dataXML = CommonUtility.SerializeObject<CloudinaryInfo>(cloudinaryinfo);

                        //var checkIsTotalOrderSync = dbContext.CloudinaryDtls.Where(o => o.OrderId == cloudinary.OrderId && o.CloudinaryStatusID != 1).FirstOrDefault();
                        //if (checkIsTotalOrderSync == null)
                        //{
                        //    var changeTrackingId = dbContext.ChangeTrackings.Where(o => o.ObjectValueId == cloudinary.OrderId && o.ApplicationObjectId == 14).FirstOrDefault();
                        //    if (changeTrackingId != null)
                        //    {
                        //        SyncController.UpdateSyncStatus(changeTrackingId.ChangeTrackingId, (int)SyncStatus.Synced);
                        //    }
                        //} 
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in cloudinaryinfo Serialization.", e);
                outerException.Source = "Serialization.cloudinaryinfoSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
