﻿using DigiPhoto.DailySales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public interface IUploadable
    {
        List<UploadInfo> UploadList { get; set; }
        UploadFile UploadFile { get; set; }
    }
}
