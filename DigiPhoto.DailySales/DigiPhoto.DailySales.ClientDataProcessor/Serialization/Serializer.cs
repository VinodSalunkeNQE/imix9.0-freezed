﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public abstract class Serializer : IExtendedProperties
    {
        public abstract string Serialize(long ObjectValueId);

        public abstract object Deserialize(string input);

        public virtual string GetAdditionalInfo(long ObjectValueId)
        {
            return string.Empty;
        }
    }

    public interface IExtendedProperties
    {
        string GetAdditionalInfo(long ObjectValueId);
    }
}
