﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class UserSerializer : Serializer
    {

        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;

                    var user = dbContext.DG_Users.Where(r => r.DG_User_pkey == ObjectValueId).FirstOrDefault();
                    if (user != null)
                    {
                        var role = (from users in dbContext.DG_Users
                                    join userrole in dbContext.DG_User_Roles on users.DG_User_Roles_Id equals userrole.DG_User_Roles_pkey
                                    where users.DG_User_pkey == ObjectValueId
                                    select new { myrole = userrole }).FirstOrDefault();

                        var locations = (from users in dbContext.DG_Users
                                         join location in dbContext.DG_Location on users.DG_Location_ID equals location.DG_Location_pkey
                                         where users.DG_User_pkey == ObjectValueId
                                         select new { myloc = location }).FirstOrDefault();
                        int storeid = locations.myloc.DG_Store_ID;

                        var storename = dbContext.DG_Store.Where(r => r.DG_Store_pkey == storeid).FirstOrDefault();

                        UserInfo userinfo = new UserInfo();

                        userinfo.UserName = user.DG_User_Name;
                        userinfo.FirstName = user.DG_User_First_Name;
                        userinfo.LastName = user.DG_User_Last_Name;
                        userinfo.Password = user.DG_User_Password;
                        userinfo.IsActive = user.DG_User_Status;
                        userinfo.PhoneNumber = user.DG_User_PhoneNo;
                        userinfo.EmailAddress = user.DG_User_Email;
                        userinfo.CreatedDateTime = user.DG_User_CreatedDate;
                        userinfo.SyncCode = user.SyncCode;
                        userinfo.Location_ID = user.DG_Location_ID;
                        userinfo.Role_ID = user.DG_User_Roles_Id;
                        userinfo.RoleSyncCode = role.myrole.SyncCode;
                        userinfo.LocationSyncCode = locations.myloc.SyncCode;
                        userinfo.StoreName = storename.DG_Store_Name;
                        userinfo.CountryName = storename.Country;
                        dataXML = CommonUtility.SerializeObject<UserInfo>(userinfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in User Serialization.", e);
                outerException.Source = "Serialization.UserSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
