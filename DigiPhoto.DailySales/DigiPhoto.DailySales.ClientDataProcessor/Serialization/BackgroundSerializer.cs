﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System.IO;

namespace DigiPhoto.DailySales.ClientDataProcessor.Serialization
{
    public class BackgroundSerializer : Serializer, IUploadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var backGround = dbContext.DG_BackGround.Where(r => r.DG_Background_pkey == ObjectValueId).FirstOrDefault();
                    //Code Added by Anis for Background issue
                    //var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode.ToString()).FirstOrDefault();
                    var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode).FirstOrDefault();
                    //End
                    if (backGround != null)
                    {
                        BackgroundInfo backGroundInfo = new BackgroundInfo()
                        {
                            BackgroundImage = backGround.DG_BackGround_Image_Name,
                            ProductId = backGround.DG_Product_Id > 0 ? backGround.DG_Product_Id : 2,//backGround.DG_Product_Id,
                            DisplayName = backGround.DG_BackGround_Image_Display_Name,
                            BackgroundGroupId = backGround.DG_BackGround_Group_Id,
                            ModifiedDate = System.DateTime.Now,
                            SyncCode = backGround.SyncCode,
                            SiteCodeSyncCode = siteSynccode,
                            IsActive = backGround.DG_Background_IsActive,

                        };
                        if (backGround.CreatedBy.HasValue)
                            backGroundInfo.CreatedBy = backGround.CreatedBy.Value;
                        if (backGround.CreatedDate.HasValue)
                            backGroundInfo.CreatedDate = backGround.CreatedDate.Value;
                        else
                            backGroundInfo.CreatedDate = DateTime.Now;

                        if (backGround.ModifiedBy.HasValue)
                            backGroundInfo.ModifiedBy = backGround.ModifiedBy.Value;

                        var configurationData = dbContext.DG_Configuration.FirstOrDefault();
                        if (configurationData != null)
                        {
                            //string Folderpath = ConfigurationManager.AppSettings["DigiphotoBorderPath"].ToString();
                            uploadFile = new UploadFile();
                            if (File.Exists(Path.Combine(configurationData.DG_BG_Path, backGround.DG_BackGround_Image_Name)))
                                uploadFile.UploadFilePath = Path.Combine(configurationData.DG_BG_Path, backGround.DG_BackGround_Image_Name);
                            else
                                uploadFile.UploadFilePath = Path.Combine(configurationData.DG_BG_Path, "8x10", backGround.DG_BackGround_Image_Name);
                            uploadFile.UploadThumbnailFilePath = Path.Combine(configurationData.DG_BG_Path, "Thumbnails", backGround.DG_BackGround_Image_Name);
                            uploadFile.FileType = 2;
                        }
                        dataXML = CommonUtility.SerializeObject<BackgroundInfo>(backGroundInfo);
                    }

                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Background Serialization.", e);
                outerException.Source = "Serialization.BackgroundSerializer";
                throw outerException;
            }
        }

        private UploadFile uploadFile;
        public UploadFile UploadFile
        {
            get
            {
                return uploadFile;
            }
            set
            {
                uploadFile = value;
            }
        }
        private List<UploadInfo> uploadList;
        public List<UploadInfo> UploadList
        {
            get
            {
                return uploadList;
            }
            set
            {
                uploadList = value;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
