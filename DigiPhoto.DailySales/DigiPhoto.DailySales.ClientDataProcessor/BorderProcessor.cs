﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public class BorderProcessor : BaseProcessor
    {

        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    BordersInfo borderInfo = CommonUtility.DeserializeXML<BordersInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        ProductInfo product = (from p in dbContext.DG_Orders_ProductType
                                               where p.SyncCode == borderInfo.Product.SyncCode
                                               select new ProductInfo
                                               {
                                                   ProductId = p.DG_Orders_ProductType_pkey
                                               }
                                              ).FirstOrDefault();

                        var existingBorderCount = dbContext.DG_Borders.Count(r => r.SyncCode == borderInfo.SyncCode);

                        if (existingBorderCount == 0)
                        {
                            var Borders = new DG_Borders();
                            Borders.DG_Border = borderInfo.Name;
                            Borders.DG_ProductTypeID = product != null ? Convert.ToInt32(product.ProductId) : 0;
                            Borders.DG_IsActive = borderInfo.IsActive;
                            Borders.SyncCode = borderInfo.SyncCode;
                            Borders.IsSynced = true;
                            Borders.CreatedBy = 1;
                            Borders.CreatedDate = DateTime.Now;
                            Borders.ModifiedBy = 1;
                            Borders.ModifiedDate = DateTime.Now;
                            dbContext.DG_Borders.Add(Borders);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Border Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.BorderProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {

                    BordersInfo borderInfo = CommonUtility.DeserializeXML<BordersInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        ProductInfo product = (from p in dbContext.DG_Orders_ProductType
                                               where p.SyncCode == borderInfo.Product.SyncCode
                                               select new ProductInfo
                                               {
                                                   ProductId = p.DG_Orders_ProductType_pkey
                                               }
                                               ).FirstOrDefault();


                        var dbBorder = dbContext.DG_Borders.Where(r => r.SyncCode == borderInfo.SyncCode).FirstOrDefault();
                        if (dbBorder != null)
                        {
                            //Update Border Name
                            dbBorder.DG_Border = borderInfo.Name;

                            //need to be discuss
                            dbBorder.DG_ProductTypeID = product != null ? Convert.ToInt32(product.ProductId) : 0;

                            dbBorder.DG_IsActive = borderInfo.IsActive;
                            dbBorder.SyncCode = borderInfo.SyncCode;
                            dbBorder.IsSynced = true;
                            dbBorder.ModifiedBy = 1;
                            dbBorder.ModifiedDate = DateTime.Now;
                            dbContext.SaveChanges();
                            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Border Processor Update.", e);
                outerException.Source = "ClientDataProcessor.BorderProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {

                    var dbBorder = dbContext.DG_Borders.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbBorder != null)
                    {
                        dbContext.DG_Borders.Remove(dbBorder);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                    }
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Border Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.BorderProcessor";
                throw outerException;
            }
        }

    }
}
