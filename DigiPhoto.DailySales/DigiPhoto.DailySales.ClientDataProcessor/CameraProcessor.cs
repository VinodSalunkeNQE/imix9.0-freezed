﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    class CameraProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    CameraInfo cameraInfo = CommonUtility.DeserializeXML<CameraInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingCameraCount = dbContext.DG_CameraDetails.Count(r => r.SyncCode == cameraInfo.SyncCode);
                        if (existingCameraCount == 0)
                        {
                            var camera = new DG_CameraDetails();

                            camera.DG_Camera_Name = cameraInfo.Name;
                            camera.DG_Camera_Make = cameraInfo.Make;
                            camera.DG_Camera_Model = cameraInfo.Model;
                            camera.DG_Updatedby = Convert.ToInt32(cameraInfo.ModifiedBy);
                            camera.DG_UpdatedDate = cameraInfo.ModifiedDateTime;
                            bool Isdeleted = false;
                            if (cameraInfo.IsActive == true)
                                Isdeleted = false;
                            else if (cameraInfo.IsActive == false)
                                Isdeleted = true;

                            camera.DG_Camera_IsDeleted = Isdeleted;

                            camera.SyncCode = cameraInfo.SyncCode;
                            camera.DG_Camera_Start_Series = "NA";
                            camera.DG_Camera_ID = Convert.ToInt32(cameraInfo.CameraId);
                            camera.IsSynced = true;
                            dbContext.DG_CameraDetails.Add(camera);
                            dbContext.SaveChanges();

                            //int AssignTo = dbContext.DG_Users.Where(s => s.SyncCode == cameraInfo.syncCodeForUser).FirstOrDefault().DG_User_pkey;
                            //camera.DG_AssignTo = AssignTo;
                            //camera.DG_Camera_Start_Series = cameraInfo.Start_Series;
                            //cameraId is not to be mapped
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Camera Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.CameraProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    CameraInfo cameraInfo = CommonUtility.DeserializeXML<CameraInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbCamera = dbContext.DG_CameraDetails.Where(r => r.SyncCode == cameraInfo.SyncCode).FirstOrDefault();
                        if (dbCamera != null)
                        {
                            //Update Role Name
                            dbCamera.DG_Camera_Name = cameraInfo.Name;
                            dbCamera.DG_Camera_Make = cameraInfo.Make;
                            dbCamera.DG_Camera_Model = cameraInfo.Model;
                            dbCamera.DG_Updatedby = Convert.ToInt32(cameraInfo.ModifiedBy);
                            dbCamera.DG_UpdatedDate = cameraInfo.ModifiedDateTime;
                            dbCamera.SyncCode = cameraInfo.SyncCode;
                            dbCamera.DG_Camera_ID = Convert.ToInt32(cameraInfo.CameraId);
                            
                            bool Isdeleted = false;
                            if (cameraInfo.IsActive == true)
                                Isdeleted = false;
                            else if (cameraInfo.IsActive == false)
                                Isdeleted = true;

                            dbCamera.DG_Camera_IsDeleted = Isdeleted;

                            //string syncCodeforAssignto = dbContext.Users.Where(s => s.UserId == camera.AssignTo).FirstOrDefault().syncCode;
                            //int AssignTo = dbContext.DG_Users.Where(s => s.SyncCode == cameraInfo.syncCodeForUser).FirstOrDefault().DG_User_pkey;
                            //dbCamera.DG_AssignTo = AssignTo;
                            dbCamera.IsSynced = true;
                            dbContext.SaveChanges();
                            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Camera Processor Update.", e);
                outerException.Source = "ClientDataProcessor.CameraProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbCamera = dbContext.DG_CameraDetails.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbCamera != null)
                    {
                        dbContext.DG_CameraDetails.Remove(dbCamera);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                    }
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Camera Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.CameraProcessor";
                throw outerException;
            }
        }
    }
}
