﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    class ProductProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    ProductInfo productInfo = CommonUtility.DeserializeXML<ProductInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingProductCount = dbContext.DG_Orders_ProductType.Count(r => r.SyncCode == productInfo.SyncCode);
                        if (existingProductCount == 0)
                        {
                            var product = new DG_Orders_ProductType();

                            product.DG_Orders_ProductType_Name = productInfo.Name;
                            product.DG_Orders_ProductType_Desc = productInfo.Description;
                            product.DG_Orders_ProductCode = productInfo.Code;
                            product.DG_Orders_ProductNumber = Convert.ToInt32(productInfo.ProductNumber);
                            product.DG_Orders_ProductType_Image = productInfo.ImagePath;
                            product.DG_MaxQuantity = productInfo.MaximumQuantity;
                            product.DG_Orders_ProductType_IsBundled = productInfo.IsBundled;
                            product.DG_IsAccessory = productInfo.IsAccessory;
                            product.DG_IsActive = productInfo.IsActive;
                            product.DG_Orders_ProductType_DiscountApplied = productInfo.IsDiscountApplied;
                            product.DG_IsPrimary = productInfo.IsPrimary;
                            product.SyncCode = productInfo.SyncCode;
                            product.DG_Orders_ProductType_Active = true;
                            product.DG_IsBorder = false;
                            product.DG_IsPackage = false;
                            product.IsSynced = true;
                            dbContext.DG_Orders_ProductType.Add(product);
                            dbContext.SaveChanges();

                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                            int StoreId = 0;
                            int UserId = 0;
                            int CurrencyId = 0;
                            int ProductId = 0;

                            if (dbContext.DG_Location.Where(r => r.SyncCode == productInfo.LocationSyncCode).FirstOrDefault() != null)
                                StoreId = dbContext.DG_Location.Where(r => r.SyncCode == productInfo.LocationSyncCode).FirstOrDefault().DG_Store_ID;

                            if (dbContext.DG_Users.Where(u => u.SyncCode == productInfo.UserSyncCode).FirstOrDefault() != null)
                                UserId = dbContext.DG_Users.Where(u => u.SyncCode == productInfo.UserSyncCode).FirstOrDefault().DG_User_pkey;

                            if (dbContext.DG_Currency.Where(c => c.SyncCode == productInfo.CurrencySyncCode).FirstOrDefault() != null)
                                CurrencyId = dbContext.DG_Currency.Where(c => c.SyncCode == productInfo.CurrencySyncCode).FirstOrDefault().DG_Currency_pkey;

                            if (dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == productInfo.SyncCode).FirstOrDefault() != null)
                                ProductId = dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == productInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                            ProductPricing.DG_Product_Pricing_CreatedBy = UserId;
                            ProductPricing.DG_Product_Pricing_Currency_ID = 1;

                            //ProductPricing.DG_Product_Pricing_Currency_ID = CurrencyId;

                            ProductPricing.DG_Product_Pricing_IsAvaliable = true;
                            ProductPricing.DG_Product_Pricing_ProductPrice = productInfo.Price;
                            ProductPricing.DG_Product_Pricing_ProductType = ProductId;
                            ProductPricing.DG_Product_Pricing_StoreId = StoreId;

                            ProductPricing.DG_Product_Pricing_UpdateDate = productInfo.CreatedDateTime;
                            dbContext.DG_Product_Pricing.Add(ProductPricing);
                            dbContext.SaveChanges();


                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Product Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.ProductProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    ProductInfo productInfo = CommonUtility.DeserializeXML<ProductInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbProduct = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault();
                        if (dbProduct != null)
                        {
                            //Update Product Details
                            dbProduct.DG_Orders_ProductType_Name = productInfo.Name;
                            dbProduct.DG_Orders_ProductType_Desc = productInfo.Description;
                            dbProduct.DG_Orders_ProductCode = productInfo.Code;
                            dbProduct.DG_Orders_ProductNumber = Convert.ToInt32(productInfo.ProductNumber);
                            dbProduct.DG_Orders_ProductType_Image = productInfo.ImagePath;
                            dbProduct.DG_MaxQuantity = productInfo.MaximumQuantity;
                            dbProduct.DG_Orders_ProductType_IsBundled = productInfo.IsBundled;
                            dbProduct.DG_IsAccessory = productInfo.IsAccessory;
                            dbProduct.DG_IsActive = productInfo.IsActive;
                            dbProduct.DG_Orders_ProductType_DiscountApplied = productInfo.IsDiscountApplied;
                            dbProduct.DG_IsPrimary = productInfo.IsPrimary;
                            dbProduct.DG_Orders_ProductType_Active = true;
                            dbProduct.DG_IsBorder = false;
                            dbProduct.DG_IsPackage = false;
                            dbProduct.SyncCode = productInfo.SyncCode;
                            dbProduct.IsSynced = true;

                            dbContext.SaveChanges();

                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                            var priceitem = dbContext.DG_Product_Pricing.ToList().Where(t => t.DG_Product_Pricing_ProductType == dbProduct.DG_Orders_ProductType_pkey).FirstOrDefault();
                            if (priceitem != null)
                            {
                                int StoreId = 0;
                                int UserId = 0;
                                int CurrencyId = 0;
                                int ProductId = 0;

                                if (dbContext.DG_Location.Where(r => r.SyncCode == productInfo.LocationSyncCode).FirstOrDefault() != null)
                                    StoreId = dbContext.DG_Location.Where(r => r.SyncCode == productInfo.LocationSyncCode).FirstOrDefault().DG_Store_ID;

                                if (dbContext.DG_Users.Where(u => u.SyncCode == productInfo.UserSyncCode).FirstOrDefault() != null)
                                    UserId = dbContext.DG_Users.Where(u => u.SyncCode == productInfo.UserSyncCode).FirstOrDefault().DG_User_pkey;

                                if (dbContext.DG_Currency.Where(c => c.SyncCode == productInfo.CurrencySyncCode).FirstOrDefault() != null)
                                    CurrencyId = dbContext.DG_Currency.Where(c => c.SyncCode == productInfo.CurrencySyncCode).FirstOrDefault().DG_Currency_pkey;

                                if (dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == productInfo.SyncCode).FirstOrDefault() != null)
                                    ProductId = dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == productInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                                priceitem.DG_Product_Pricing_CreatedBy = UserId;
                                priceitem.DG_Product_Pricing_Currency_ID = 1;

                                //ProductPricing.DG_Product_Pricing_Currency_ID = CurrencyId;

                                priceitem.DG_Product_Pricing_IsAvaliable = true;
                                priceitem.DG_Product_Pricing_ProductPrice = productInfo.Price;
                                priceitem.DG_Product_Pricing_ProductType = ProductId;
                                priceitem.DG_Product_Pricing_StoreId = StoreId;
                                
                                priceitem.DG_Product_Pricing_UpdateDate = productInfo.CreatedDateTime;
                                //// dbContext.DG_Product_Pricing.Add(ProductPricing);
                                dbContext.SaveChanges();
                            }
                           
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Product Processor Update.", e);
                outerException.Source = "ClientDataProcessor.ProductProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbProduct = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbProduct != null)
                    {
                        int ProductId = 0;
                        if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault() != null)
                            ProductId = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault().DG_Orders_ProductType_pkey;
                        if (ProductId != 0)
                        {
                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                             var priceitem = dbContext.DG_Product_Pricing.ToList().Where(t => t.DG_Product_Pricing_ProductType == dbProduct.DG_Orders_ProductType_pkey).FirstOrDefault();
                             if (priceitem != null)
                             {
                                 //ProductPricing = dbContext.DG_Product_Pricing.Where(p => p.DG_Product_Pricing_ProductType == ProductId).FirstOrDefault();
                                 dbContext.DG_Product_Pricing.Remove(priceitem);
                                 dbContext.SaveChanges();
                             }
                        }
                        dbContext.DG_Orders_ProductType.Remove(dbProduct);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.



                    }

                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Product Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.ProductProcessor";
                throw outerException;
            }
        }
    }
}
