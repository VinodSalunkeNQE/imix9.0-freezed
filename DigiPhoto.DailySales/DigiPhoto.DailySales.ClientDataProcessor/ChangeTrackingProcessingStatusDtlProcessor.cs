﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using DigiPhoto.DailySales.ClientDataProcessor.Controller;
using DigiPhoto.DailySales.ServiceLibrary.Interface;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public class ChangeTrackingProcessingStatusDtlProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    ChangeTrackingProcessingStatusDtlInfo changeTrackingProcessStatusDtlInfo = CommonUtility.DeserializeXML<ChangeTrackingProcessingStatusDtlInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        using (var dbTran = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                var existingchangeTrackingProcessStatusDtl = dbContext.ChangeTrackingProcessingStatusDtls.Count(r => r.SyncCode == changeTrackingProcessStatusDtlInfo.SyncCode);

                                if (existingchangeTrackingProcessStatusDtl == 0)
                                {

                                    var vchangeTrackinggProcessStatusDtl = new ChangeTrackingProcessingStatusDtl();

                                    vchangeTrackinggProcessStatusDtl.Status = changeTrackingProcessStatusDtlInfo.Status;
                                    vchangeTrackinggProcessStatusDtl.Message = changeTrackingProcessStatusDtlInfo.Message;
                                    vchangeTrackinggProcessStatusDtl.Stage = changeTrackingProcessStatusDtlInfo.Stage;
                                    vchangeTrackinggProcessStatusDtl.ExceptionType = changeTrackingProcessStatusDtlInfo.ExceptionType;
                                    vchangeTrackinggProcessStatusDtl.ExceptionSource = changeTrackingProcessStatusDtlInfo.ExceptionSource;
                                    vchangeTrackinggProcessStatusDtl.ExceptionStackTrace = changeTrackingProcessStatusDtlInfo.ExceptionStackTrace;
                                    vchangeTrackinggProcessStatusDtl.InnerException = changeTrackingProcessStatusDtlInfo.InnerException;
                                    vchangeTrackinggProcessStatusDtl.IncomingChangeId = changeTrackingProcessStatusDtlInfo.IncomingChangeId;
                                    vchangeTrackinggProcessStatusDtl.ChangeTrackingId = changeTrackingProcessStatusDtlInfo.ChangeTrackingId;
                                    vchangeTrackinggProcessStatusDtl.VenueName = changeTrackingProcessStatusDtlInfo.VenueName;
                                    vchangeTrackinggProcessStatusDtl.CreatedOn = changeTrackingProcessStatusDtlInfo.CreatedOn;
                                    vchangeTrackinggProcessStatusDtl.ErrorID = changeTrackingProcessStatusDtlInfo.ErrorID;
                                    dbContext.ChangeTrackingProcessingStatusDtls.Add(vchangeTrackinggProcessStatusDtl);
                                    dbContext.SaveChanges();                            
                                }
                                dbTran.Commit();
                            }
                            catch (Exception e)
                            {
                                Exception outerException = new ProcessingException("Error occured in ChangeTrackingProcessingStatusDtl Processor Insert.", e);
                                outerException.Source = "ClientDataProcessor.ChangeTrackingProcessingStatusDtlProcessor";
                                throw outerException;
                            }
                        }
                    }                   
                }
            }
            catch (Exception e)
            {
                //here i will insert into table that sync has been failed
                Exception outerException = new ProcessingException("Error occured in ChangeTrackingProcessingStatusDtl Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.ChangeTrackingProcessingStatusDtlProcessor";
                throw outerException;

            }
        }
        protected override void ProcessUpdate()
        {
            throw new ProcessingException("Not Emplemented");
        }
        protected override void ProcessDelete()
        {
            throw new ProcessingException("Not Emplemented");
        }
    }
}
