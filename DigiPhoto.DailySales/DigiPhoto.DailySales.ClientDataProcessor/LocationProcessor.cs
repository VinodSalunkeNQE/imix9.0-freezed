﻿using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ClientDataProcessor
{
    public class LocationProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    LocationInfo locationInfo = CommonUtility.DeserializeXML<LocationInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                       
                        if (locationInfo.Level == 4)
                        {

                            //-------------------- Updated sync code come from IMS ------------------------
                            var countryVenueCode = dbContext.DG_Store.Where(r => r.CountryCode == locationInfo.ParentLocation.ParentLocation.Code && r.StoreCode== locationInfo.ParentLocation.Code).FirstOrDefault();
                            var existingSubStoreCountByCode = dbContext.DG_SubStores.Where(r => r.DG_SubStore_Code == locationInfo.Code && r.DG_SubStore_Name == locationInfo.Name && r.SyncCode != locationInfo.SyncCode).FirstOrDefault();
                            if (existingSubStoreCountByCode != null && countryVenueCode !=null)
                            {
                                int substoreId = dbContext.DG_SubStores.Where(r => r.DG_SubStore_Code == locationInfo.Code).FirstOrDefault().DG_SubStore_pkey;
                                existingSubStoreCountByCode.DG_SubStore_pkey = substoreId;
                                existingSubStoreCountByCode.SyncCode = locationInfo.SyncCode;
                                dbContext.SaveChanges();
                            }
                            //-------------------- Updated sync code come from IMS ------------------------
                            else
                            {
                                int? logicalsubstoreID = null;
                            var existingSubStoreCount = dbContext.DG_SubStores.Count(r => r.SyncCode == locationInfo.SyncCode);
                                var existingLogicalSubStoreCount = dbContext.DG_SubStores.Where(r => r.SyncCode == locationInfo.LogicalSubStoreDetails.SyncCode).FirstOrDefault();
                                if (existingLogicalSubStoreCount != null)
                                {
                                    logicalsubstoreID = existingLogicalSubStoreCount.DG_SubStore_pkey;
                                }
                            if (existingSubStoreCount == 0)
                            {
                                var substore = new DG_SubStores();
                                substore.DG_SubStore_Name = locationInfo.Name;
                                substore.DG_SubStore_Description = locationInfo.Description;
                                substore.DG_SubStore_IsActive = locationInfo.IsActive;
                                substore.SyncCode = locationInfo.SyncCode;
                                substore.IsSynced = true;
                                    substore.DG_SubStore_Code = locationInfo.Code;
                                    //substore.logicalSubStoreDetails.DG_SubStore_pkey = Convert.ToInt32(locationInfo.LogicalSubStoreDetails.SubStoreId);
                                    //substore.logicalSubStoreDetails.DG_SubStore_Name = locationInfo.LogicalSubStoreDetails.SubStore;
                                    //substore.logicalSubStoreDetails.SyncCode = locationInfo.LogicalSubStoreDetails.SyncCode;
                                    substore.IsLogicalSubStore = locationInfo.IsLogicalSubStore;
                                    substore.LogicalSubStoreID = logicalsubstoreID;
                                dbContext.DG_SubStores.Add(substore);
                                dbContext.SaveChanges();
                                
                            }
                        }
                          

                        }
                        else if (locationInfo.Level == 5)
                        {
                            var existingLocationCount = dbContext.DG_Location.Count(r => r.SyncCode == locationInfo.SyncCode);
                            if (existingLocationCount == 0)
                            {
                                int substoreId = dbContext.DG_SubStores.Where(r => r.DG_SubStore_Name == locationInfo.ParentLocation.Name).FirstOrDefault().DG_SubStore_pkey;
                                var Location = new DG_Location();
                                Location.DG_Location_Name = locationInfo.Name;
                               // Location.DG_Store_ID = substoreId;
                                Location.DG_Store_ID = 1;
                                Location.DG_Location_IsActive = locationInfo.IsActive;
                                Location.SyncCode = locationInfo.SyncCode;
                                Location.IsSynced = true;
                                dbContext.DG_Location.Add(Location);
                                dbContext.SaveChanges();

                                // Insert DG_SubStore_Locations
                                var existingSubStoreLocCount = dbContext.DG_SubStore_Locations.Count(r => r.DG_Location_ID == Location.DG_Location_pkey && r.DG_SubStore_ID == Location.DG_Store_ID);
                                if (existingSubStoreLocCount == 0)
                                {
                                    var substorloc = new DG_SubStore_Locations();
                                    substorloc.DG_Location_ID = Location.DG_Location_pkey;
                                    substorloc.DG_SubStore_ID = substoreId;
                                    dbContext.DG_SubStore_Locations.Add(substorloc);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Location Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.LocationProcessor";
                throw outerException;
            }
        }

        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    LocationInfo locationInfo = CommonUtility.DeserializeXML<LocationInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        if (locationInfo.Level == 4)
                        {
                            int? logicalsubstoreID = null;
                            var existingLogicalSubStoreCount = dbContext.DG_SubStores.Where(r => r.SyncCode == locationInfo.LogicalSubStoreDetails.SyncCode).FirstOrDefault();
                            if (existingLogicalSubStoreCount != null)
                            {
                                logicalsubstoreID = existingLogicalSubStoreCount.DG_SubStore_pkey;
                            }
                            var dbSubStore = dbContext.DG_SubStores.Where(r => r.SyncCode == locationInfo.SyncCode).FirstOrDefault();
                            if (dbSubStore != null)
                            {

                                int substoreId = dbContext.DG_SubStores.Where(r => r.SyncCode == locationInfo.SyncCode).FirstOrDefault().DG_SubStore_pkey;
                                dbSubStore.DG_SubStore_pkey = substoreId;
                                dbSubStore.DG_SubStore_Name = locationInfo.Name;
                                dbSubStore.DG_SubStore_IsActive = locationInfo.IsActive;
                                dbSubStore.SyncCode = locationInfo.SyncCode;
                                dbSubStore.DG_SubStore_Description = locationInfo.Description;
                                dbSubStore.IsSynced = true;
                                dbSubStore.DG_SubStore_Code = locationInfo.Code;
                                // dbSubStore.LogicalSubStoreID = locationInfo.LogicalSubStoreDetails.SubStoreCode;
                                dbSubStore.IsLogicalSubStore = locationInfo.IsLogicalSubStore;
                                dbSubStore.LogicalSubStoreID = logicalsubstoreID;
                                dbContext.SaveChanges();
                                
                                //Update country and venue code
                                var countryVenueCode = dbContext.DG_Store.Where(r => r.Country == locationInfo.ParentLocation.ParentLocation.Name && r.DG_Store_Name == locationInfo.ParentLocation.Name).FirstOrDefault();
                                if (countryVenueCode != null)
                                {
                                    countryVenueCode.CountryCode = locationInfo.ParentLocation.ParentLocation.Code;
                                    countryVenueCode.StoreCode = locationInfo.ParentLocation.Code;
                                dbContext.SaveChanges();
                            }
                        }

                         
                        }
                        else if (locationInfo.Level == 5)
                        {
                            var dbLocation = dbContext.DG_Location.Where(r => r.SyncCode == locationInfo.SyncCode).FirstOrDefault();
                            if (dbLocation != null)
                            {
                                int locationId = dbContext.DG_Location.Where(r => r.SyncCode == locationInfo.SyncCode).FirstOrDefault().DG_Location_pkey;
                                int substoreId = dbContext.DG_SubStores.Where(r => r.DG_SubStore_Name == locationInfo.ParentLocation.Name).FirstOrDefault().DG_SubStore_pkey;
                                dbLocation.DG_Location_Name = locationInfo.Name;
                                dbLocation.DG_Store_ID = substoreId;
                                dbLocation.DG_Location_IsActive = locationInfo.IsActive;
                                dbLocation.DG_Location_pkey = locationId;
                                dbLocation.SyncCode = locationInfo.SyncCode;
                                dbLocation.IsSynced = true;
                                dbContext.SaveChanges();

                                // Update DG_SubStore_Locations
                                var existingSubStoreLocCount = dbContext.DG_SubStore_Locations.Where(r => r.DG_Location_ID == locationId).FirstOrDefault();
                                if (existingSubStoreLocCount != null)
                                {
                                    existingSubStoreLocCount.DG_Location_ID = locationId;
                                    existingSubStoreLocCount.DG_SubStore_ID = substoreId;
                                    dbContext.SaveChanges();
                                }
                            }

                        }
                    }
                }
            }

            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Location Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.LocationProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    LocationInfo locationInfo = CommonUtility.DeserializeXML<LocationInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        //if (locationInfo.Level == 4)
                        //{
                        var dbSubStore = dbContext.DG_SubStores.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                            if (dbSubStore != null)
                            {
                                dbSubStore.DG_SubStore_IsActive = locationInfo.IsActive;
                                dbSubStore.IsSynced = true;
                                dbContext.SaveChanges();
                            }
                        //}
                        //else if (locationInfo.Level == 5)
                        //{
                            var dbLocation = dbContext.DG_Location.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                            if (dbLocation != null)
                            {
                                dbLocation.DG_Location_IsActive = locationInfo.IsActive;
                                dbLocation.IsSynced = true;
                                dbContext.SaveChanges();
                            }
                        //}

                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Location Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.LocationProcessor";
                throw outerException;
            }
        }
    }
}
