﻿using DigiPhoto.DailySales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.ServiceLibrary.Interface
{
    [ServiceContract]
    public interface IFileService
    {
        [OperationContract]
        RemoteFileInfo DownloadFile(DownloadRequest request);
        
        [OperationContract]
        void UploadFile(RemoteFileInfo request);

        [OperationContract]
        void UploadFileBytes(RemoteFileBytesInfo request);
        [OperationContract]
        RemoteFileInfo DownloadFileForRelativeFilePath(DownloadRequest request);
        [OperationContract]
        void CreateFreshThumbnail(DownloadRequest request);
    }
}
