﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DigiPhoto.DailySales.Model;

namespace DigiPhoto.DailySales.ServiceLibrary.Interface
{
    [ServiceContract]
    public interface IDataSyncService
    {
        [OperationContract]
        String GetServerDate();

        [OperationContract]
        ChangeInfo GetNextChangeInfo(ChangeInfo lastChangeInfo);

        #region Sync Service Enhancement - Ashirwad
        [OperationContract]
        OnlineOrderStatus GetOnlineOrderStatus(QrCodeStatus qrCode);

        [OperationContract]
        int SaveDailySalesData(DailySalesReportData reportData);
        #endregion
        [OperationContract]
        bool SaveChangeInfo(ChangeInfo change);
        [OperationContract]
        CardLimitInfo CheckCardLimit(string code);
        [OperationContract]
        CardLimitInfo CheckAnonymousCardLimit(string code, PackageInfo packageInfo);

        [OperationContract]
        bool SaveCurrencySyncStatusInfo(CurrencySyncStatusInfo currencySyncStatus);       

         [OperationContract]
        bool updateChangeTrackingHistory(ChangeInfo changeInfo, SiteInfo site,Int32 ProcessingStatus); 
        [OperationContract]//////created by latika
        int CheckIsPersonalized(int OrderID);
        //////created by latika end
        #region Obsolete
        //[OperationContract]
        //SyncResponseInfo SignIn(SyncRequestInfo syncRequest);
        //[OperationContract]
        //bool SignOut(Guid SessionTokenId, long endChangeTrackingId);
        #endregion
    }
}
