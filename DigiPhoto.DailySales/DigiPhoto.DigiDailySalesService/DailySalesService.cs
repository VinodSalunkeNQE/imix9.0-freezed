﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.ClientDataProcessor;
using DigiPhoto.DailySales.Utility;
using DigiPhoto.DailySales.ClientDataProcessor.Controller;
using DigiPhoto.DailySales.ServiceLibrary.Interface;
using System.Configuration;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.IMIX.Business;

namespace DataSyncService
{
    public partial class DailySalesService : ServiceBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer timer;
        private static bool isNewArchEnabled = false;

        public DailySalesService()
        {
            InitializeComponent();
        }

        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }



        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {

                    isNewArchEnabled = SyncController.IsNewArchitectureEnabled();
                    this.timer = new System.Timers.Timer();
                    this.timer.Interval = 1000 * 100;
                    this.timer.AutoReset = true;
                    this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    this.timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }

                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);

            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                //ExitCode = 13816;
                this.Stop();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                this.timer.Stop();
                
                SaveDailySalesDataStatus();

            }
            catch (Exception ex)
            {

                log.Error("timer_Elapsed:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.timer.Start();
            }
            finally
            {
                this.timer.Start();
            }
        }


        private void SaveDailySalesDataStatus()
        {
            try
            {
                SyncController.GetPendingDailySalesData();

            }
            catch (Exception ex)
            {

                log.Error("Error:SaveDailySalesDataStatus-" + ex.Message);
            }


        }

    }
}





