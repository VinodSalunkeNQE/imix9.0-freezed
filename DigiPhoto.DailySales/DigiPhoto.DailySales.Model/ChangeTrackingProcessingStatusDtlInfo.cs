﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class ChangeTrackingProcessingStatusDtlInfo
    {
      
        public long ExceptionDtlID { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public int Stage { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string InnerException { get; set; }
        public long IncomingChangeId { get; set; }
        public string SyncCode { get; set; }
        public long ChangeTrackingId { get; set; }
        public string VenueName { get; set; }
        public int ErrorID { get; set; }
    }
}
