﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class CountryInfo
    {
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
