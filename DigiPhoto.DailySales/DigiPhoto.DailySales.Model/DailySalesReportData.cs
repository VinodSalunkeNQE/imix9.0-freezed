﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class DailySalesReportData
    {

        public long ClosingFormDetailID { get; set; }

        public DateTime ClosingDate { get; set; }

        public DateTime TransDate { get; set; }

        public Int32 SubStoreID { get; set; }

        public string SiteSyncCode { get; set; }

        public Int32 NoOfTrans { get; set; }

        public decimal? NetCost { get; set; }

        public decimal? Cash { get; set; }

        public decimal? CreditCard { get; set; }

        public decimal? Amex { get; set; }

        public decimal? FCV { get; set; }

        public decimal? RoomCharges { get; set; }

        public decimal? KVL { get; set; }

        public decimal? Vouchers { get; set; }

        public decimal LaborHour { get; set; }

        public Int32 Attendance { get; set; }

        public long NoOfCapture { get; set; }

        public long NoOfPreview { get; set; }

        public long NoOfImageSold { get; set; }

        public string Comments { get; set; }
              
        public Int32 FilledBy { get; set; }
      
        public string SyncCode { get; set; }


    }
}
