﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class CurrencyInfo
    {
        public long CurrencyId { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public double Rate { get; set; }
        public string Symbol { get; set; }
        public bool IsDefault { get; set; }
        public string CurrencyIcon { get; set; }
        public String SyncCode { get; set; }
        public String UserSyncCode { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public long ? ModifiedBy { get; set; }
        
        
    }
}
