﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class VenueInfo
    {
        public string VenueName { get; set; }
        public string VenueCode { get; set; }
        public SiteInfo Site { get; set; }
    }
}
