﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class UploadFile
    {
        public string UploadFilePath { get; set; }
        public string SaveFolderPath { get; set; }
        public string UploadThumbnailFilePath { get; set; }
        public string SaveThumbnailFolderPath { get; set; }

        public string IdentificationCode { get; set; }

        public Int32 FileType { get; set; }
    }
}