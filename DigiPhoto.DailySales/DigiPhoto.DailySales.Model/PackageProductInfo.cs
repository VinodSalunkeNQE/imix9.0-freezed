﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
  public   class PackageProductInfo
    {
        public long PackageId { get; set; }

   
        public long ProductId { get; set; }

        public int? ProductQuantity { get; set; }

        public int? ProductMaxImage { get; set; }
        public string ProductSyncCode { get; set; }

    

      
    }
}
