﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class UploadFileInfo
    {
        public int PhotoId { get; set; }
        public string Title { get; set; }
        public string ImageNumber { get; set; }

        public DateTime CreatedDate { get; set; }
        public bool IsChecked { get; set; }
        public bool IsCodeType { get; set; }
        public string ImagePath { get; set; }
        public bool IsCorrupt { get; set; }

        public string SourceDirectory { get; set; }
        public string TargetDirectory { get; set; }

        public int ImageDefaultWidth {get;set;}
        public int ImageDefaultHeight {get;set;}

        public int MediaType { get; set; }
    }
}
