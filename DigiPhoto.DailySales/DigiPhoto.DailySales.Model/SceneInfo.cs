﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class SceneInfo
    {
        public long SceneId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public long BackGroundId { get; set; }
        public string BackgroundName { get; set; }
        public long BorderId { get; set; }
        public string BorderName { get; set; }
        public int GraphicsId { get; set; }
        public string GraphicName { get; set; }
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public bool IsActive { get; set; }
        public string SceneName { get; set; }
        public string ProductSyncCode { get; set; }
        public string BackgroundSyncCode { get; set; }
        public string BorderSyncCode { get; set; }
    }
}
