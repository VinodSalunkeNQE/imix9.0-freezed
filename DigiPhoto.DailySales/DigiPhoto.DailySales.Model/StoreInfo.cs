﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class StoreInfo
    {
        public string StoreName { get; set; }
        public string StoreCode { get; set; }
        public CountryInfo Country { get; set; }
    }
}
