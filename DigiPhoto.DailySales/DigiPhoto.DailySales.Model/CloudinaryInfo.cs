﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class CloudinaryInfo
    {

        public Int64 CloudinaryInfoID { get; set; }

        public Int64 PhotoID { get; set; }

        public string SourceImageID { get; set; }

        public string CloudinaryPublicID { get; set; }

        public int CloudinaryStatusID { get; set; }
        public int RetryCount { get; set; }
        public string ErrorMessage { get; set; }

        public string AddedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDateTime { get; set; }

        public bool IsActive { get; set; }

        public string SyncCode { get; set; }

        public string SubStoreSyncCode { get; set; }

        public string OrderNumber { get; set; }

        public string IdentificationCode { get; set; }
        public Int32 Height { get; set; }
        public Int32 Width { get; set; }
        public string ThumbnailDimension { get; set; }


    }
}
