﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
   
   public class RefundDetailInfo
    {
       public int RefundId { get; set; }
       public int OrderId { get; set; }
       public Decimal RefundAmount { get; set; }
       public int UserId { get; set; }
       public int RefundMode { get; set; }
       public int? orderdetailId { get; set; }
       public int RefundMasterid { get; set; }
       public DateTime RefundDate { get; set; }
       public string RefundPhotoId { get; set; }
       public string UserSynccode { get; set; }
       public string OrderSyncCode { get; set; }
       public string OrderDetailsSyncCode { get; set; }
       public Decimal RefundedAmount { get; set; }
       public string RefundReason { get; set; }
       public List<RefundDetailInfo> refundphotolist { get; set; }
       

    }
}
