﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class QrCodeStatus
    {
        public string OrderNumber { get; set; }
        public string IdentificationCode { get; set; }
    }
}
