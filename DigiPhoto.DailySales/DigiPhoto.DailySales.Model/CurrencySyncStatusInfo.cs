﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class CurrencySyncStatusInfo
    {
        public long CurrencySyncStatusID { get; set; }        
        public long CurrencyProfileID { get; set; }      
        public long VenueID { get; set; }       
        public Int32 SyncStatus { get; set; }       
        public long CreatedBy { get; set; }      
        public DateTime CreatedOn { get; set; }     
        public bool CurrencyProfileStatus { get; set; }
        public string SubStoreSyncCode { get; set; }
    }
}
