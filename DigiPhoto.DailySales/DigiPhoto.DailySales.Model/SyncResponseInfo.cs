﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class SyncResponseInfo
    {
        public Guid SessionTokenId { get; set; }
    }
}
