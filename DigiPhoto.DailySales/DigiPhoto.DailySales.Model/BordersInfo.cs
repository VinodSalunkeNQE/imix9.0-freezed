﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class BordersInfo
    {
        public long BorderId { get; set; }
        public string Name { get; set; }
        public long? ProductId { get; set; }
        public bool IsActive { get; set; }
        public string SyncCode { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public ProductInfo Product { get; set; }
        public string SiteCodeSyncCode { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
