﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class UploadInfo
    {
        public string UploadFilePath { get; set; }
        public string EditedUploadFilePath { get; set; }
        public string PartialEditedUploadFilePath { get; set; }
        public string SaveFolderPath { get; set; }
        public string UploadCompressedFilePath { get; set; }
        public string OnlineImageCompression { get; set; }
        public string UploadThumbnailFilePath { get; set; }
        public string SaveThumbnailFolderPath { get; set; }
        public Int32 MediaType { get; set; }

        public Int32 PhotoId { get; set; }

        public Int32 OrderId { get; set; }

        public string IdentificationCode { get; set; }

        public int? LocationId { get; set; }

        public int? SubstoreId { get; set; }
    }
}