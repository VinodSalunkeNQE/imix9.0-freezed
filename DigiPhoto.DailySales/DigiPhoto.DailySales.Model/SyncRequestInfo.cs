﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class SyncRequestInfo
    {
        public VenueInfo Venue { get; set; }
        public long StartChangeTrackingId { get; set; }
        public DateTime? LastSyncOnDate { get; set; }
    }
}
