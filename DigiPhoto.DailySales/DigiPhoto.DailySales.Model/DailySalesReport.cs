﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class DailySalesReport
    {
        public long ClosingFormDetailID { get; set; }
        public DateTime TransDate { get; set; }

        public decimal? NetCost { get; set; }

        public long NoOfTransactions { get; set; }

        public decimal? Cash { get; set; }

        public decimal? CreditCard { get; set; }

        public decimal? Amex { get; set; }

        public decimal? FCV { get; set; }

        public decimal? RoomCharges { get; set; }

        public decimal? KVL { get; set; }

        public decimal? Vouchers { get; set; }
    
        public Int32 Attendance { get; set; }

        public long NoOfCapture { get; set; }

        public decimal LaborHour { get; set; }

        public Int32 FilledBy { get; set; }

        public Int32 SubStoreID { get; set; }
     
        public string ClosingSyncCode { get; set; }

        public string DigiMasterLocationSyncCode { get; set; }

    }


}

