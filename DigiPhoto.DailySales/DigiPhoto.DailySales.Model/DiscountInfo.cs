﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
  public  class DiscountInfo
    {
        public long DiscountTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool? Secure { get; set; }
        public bool? ItemLevel { get; set; }
        public bool? AsPercentage { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public string SyncCode { get; set; }
      
    }
}
