﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class ImageAttribute
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Dimension { get; set; }
        public Size ImageSize { get; set; }
    }
}
