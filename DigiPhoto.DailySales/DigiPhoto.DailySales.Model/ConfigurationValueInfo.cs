﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class ConfigurationValueInfo
    {
        public long IMIXConfigurationValueId { get; set; }
        public string ConfigurationValue { get; set; }
        public long? IMIXConfigurationMasterId { get; set; }
        //public int SubstoreId { get; set; }
        public string SyncCode { get; set; }
        public long ConfigurationId { get; set; }

        public string MasterSyncCode { get; set; }
        public string currencySyncCode { get; set; }
        public string ProductSyncCode { get; set; } 
        public List<ConfigurationValueInfo> configuartionInfoList { get; set; }
    }
}
