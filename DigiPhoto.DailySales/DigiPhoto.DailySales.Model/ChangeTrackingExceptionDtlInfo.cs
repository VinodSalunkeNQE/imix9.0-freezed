﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Model
{
    public class ChangeTrackingExceptionDtlInfo
    {
        public long ExceptionDtlID { get; set; }
        public long ChangeTrackingId { get; set; }
        public long ApplicationObjectId { get; set; }
        public long ObjectValueId { get; set; }
        public int ChangeAction { get; set; }
        public DateTime ChangeDate { get; set; }
        //public int SyncStatus { get; set; }
        public string ExceptionMsg { get; set; }
        public string ExceptionStackTrace{get; set;}
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string InnerException { get; set; }
        public DateTime ExceptionOnDate { get; set; }
    }
}
