﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class LocationInfo
    {
        public long DigiMasterLocationId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Code { get; set; }
        public long? ParentDigiMasterLocationId { get; set; }
        public Int32 Level { get; set; }
        public int? DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public String SyncCode { get; set; }
        public LocationInfo ParentLocation { get; set; }

        public SubStoreInfo LogicalSubStoreDetails { get; set; }

        public bool IsLogicalSubStore { get; set; }
    }
}
