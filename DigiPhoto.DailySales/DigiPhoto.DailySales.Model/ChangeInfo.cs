﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace DigiPhoto.DailySales.Model
{

    public class ChangeInfo
    {
        public long ApplicationObjectId { get; set; }
        public long ObjectValueId { get; set; }
        public long ChangeTrackingId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string dataXML { get; set; }
        public int ChangeAction { get; set; }
        public long ChangeBy { get; set; }
        public long sessionId { get; set; }
        public string EntityCode { get; set; }
        public SiteInfo SubStore { get; set; }
        public DownloadInfo DownloadDetail { get; set; }
        public List<UploadInfo> UploadList { get; set; }

        public UploadFile UploadFile { get; set; }      
        public bool? IsDeleted { get; set; }

        #region NewArch Fields Added Ashirwad
        public string QrCodes { get; set; }
        #endregion
    }
}
