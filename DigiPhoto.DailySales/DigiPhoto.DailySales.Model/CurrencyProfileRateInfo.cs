﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Model
{
    public class CurrencyProfileRateInfo
    {
        public long ProfileRateID { get; set; }
        public long ProfileAuditID { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? Updatedby { get; set; }
        public DateTime? updatedon { get; set; }
        public bool? IsDeleted { get; set; }
        //public string CurrencySyncCode { get; set; }
  
    }
}
