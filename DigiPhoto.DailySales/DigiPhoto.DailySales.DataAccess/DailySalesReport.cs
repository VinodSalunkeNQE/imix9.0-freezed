﻿

namespace DigiPhoto.DailySales.DataAccess
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DailySalesReport", Schema = "Sage")]
    public class DailySalesReport
    {

        [Key]
        public long ClosingFormDetailID { get; set; }

        [Column("iMIXClosingFormDetailID")]
        public long iMIXClosingFormDetailID { get; set; }

        public DateTime ClosingDate { get; set; }

    
        public DateTime TransDate { get; set; }

        [Column("SubStoreID")]
        public Int32 SubStoreID { get; set; }

        [Column("SiteSyncCode")]
        public string SiteSyncCode { get; set; }

        [Column("NoOfTrans")]
        public Int32 NoOfTrans { get; set; }

        [Column("NetCost")]
        public decimal? NetCost { get; set; }

        [Column("Cash")]
        public decimal? Cash { get; set; }
        [Column("CreditCard")]
        public decimal? CreditCard { get; set; }
        [Column("Amex")]
        public decimal? Amex { get; set; }
        [Column("FCV")]
        public decimal? FCV { get; set; }
        [Column("RoomCharges")]
        public decimal? RoomCharges { get; set; }
        [Column("KVL")]
        public decimal? KVL { get; set; }
        [Column("Vouchers")]
        public decimal? Vouchers { get; set; }
        [Column("LaborHour")]
        public decimal LaborHour { get; set; }
        [Column("Attendance")]
        public Int32 Attendance { get; set; }
        [Column("NoOfCapture")]
        public long NoOfCapture { get; set; }
        [Column("NoOfPreview")]
        public long NoOfPreview { get; set; }
        [Column("NoOfImageSold")]
        public long NoOfImageSold { get; set; }
        [Column("Comments")]
        public string Comments { get; set; }
        [Column("FilledBy")]
        public Int32 FilledBy { get; set; }
        [Column("SyncCode")]
        public string SyncCode { get; set; }

    }


}

