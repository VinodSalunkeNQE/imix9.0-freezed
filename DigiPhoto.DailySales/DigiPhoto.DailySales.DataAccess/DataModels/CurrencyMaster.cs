﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
   public partial class CurrencyMaster
    {   
        public long CurrencyMasterID { get; set; }
        [Required]
        [StringLength(50)]
        public string CurrencyName { get; set; }
        [Required]
        [StringLength(50)]
        public string CurrencyCode { get; set; }    
       
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public long CreatedBy { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
