namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DigiConfiguration")]
    public partial class DigiConfiguration
    {
        public DigiConfiguration()
        {
            IMixConfigurationValues = new HashSet<IMixConfigurationValue>();
        }

        public long DigiConfigurationId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual ICollection<IMixConfigurationValue> IMixConfigurationValues { get; set; }
    }
}
