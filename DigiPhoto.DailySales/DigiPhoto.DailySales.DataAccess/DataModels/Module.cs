namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Module")]
    public partial class Module
    {
        public Module()
        {
            Roles = new HashSet<Role>();
        }

        public long ModuleId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string ActionUrl { get; set; }

        [StringLength(50)]
        public string IconClass { get; set; }

        public bool IsActive { get; set; }

        public int? SortBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
