namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MessageProfile
    {
        public MessageProfile()
        {
            MessageQueues = new HashSet<MessageQueue>();
        }

        [Key]
        public long ProfileId { get; set; }

        [StringLength(250)]
        public string ProfileName { get; set; }

        public long ProfileType { get; set; }

        [StringLength(500)]
        public string ProfileFrom { get; set; }

        [StringLength(500)]
        public string ProfileCC { get; set; }

        [StringLength(500)]
        public string ProfileBCC { get; set; }

        [StringLength(500)]
        public string ProfileSubject { get; set; }

        public string ProfileMessageBody { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<MessageQueue> MessageQueues { get; set; }
    }
}
