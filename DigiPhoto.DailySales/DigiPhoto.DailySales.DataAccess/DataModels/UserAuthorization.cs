namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAuthorization")]
    public partial class UserAuthorization
    {
        public long DigiMasterLocationId { get; set; }

        public long UserId { get; set; }

        public long UserAuthorizationId { get; set; }

        public virtual DigiMasterLocation DigiMasterLocation { get; set; }

        public virtual User User { get; set; }
    }
}
