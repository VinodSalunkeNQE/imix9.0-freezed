namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Currency")]
    public partial class Currency
    {
        public Currency()
        {
            Packages = new HashSet<Package>();
            Products = new HashSet<Product>();
            ProductPricings = new HashSet<ProductPricing>();
        }

        public int CurrencyId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public double Rate { get; set; }

        [StringLength(50)]
        public string Symbol { get; set; }

        public bool IsDefault { get; set; }

        public string CurrencyIcon { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long? ModifiedBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Package> Packages { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<ProductPricing> ProductPricings { get; set; }
    }
}
