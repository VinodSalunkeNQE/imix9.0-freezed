namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserActivityLog")]
    public partial class UserActivityLog
    {
        public long UserActivityLogId { get; set; }

        public int ActionTypeId { get; set; }

        [StringLength(200)]
        public string ActivityDescription { get; set; }

        public DateTime ActivityDateTime { get; set; }

        public long CreatedBy { get; set; }

        public virtual ActionType ActionType { get; set; }

        public virtual User User { get; set; }
    }
}
