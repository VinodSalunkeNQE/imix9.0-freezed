namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SyncSession
    {
        public long SyncSessionId { get; set; }

        public Guid SessionTokenId { get; set; }

        [Required]
        [StringLength(50)]
        public string ClientStoreCode { get; set; }

        [StringLength(50)]
        public string ClientSiteCode { get; set; }

        [StringLength(50)]
        public string ClientVenueCode { get; set; }

        public DateTime SignInDate { get; set; }

        public DateTime? SignOutDate { get; set; }

        public int Status { get; set; }

        public long StartChangeTrackingId { get; set; }

        public long? EndChangeTrackingId { get; set; }

        public DateTime? LastSyncOnDate { get; set; }
    }
}
