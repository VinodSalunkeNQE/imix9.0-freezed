﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class CurrencyProfileRate
    {
        public long CurrencyProfileRateID { get; set; }
        [Required]
        public long CurrencyProfileID { get; set; }
        [Required]
        public long CurrencyMasterID { get; set; }
        [Required]
        public decimal ExchangeRate { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public long CreatedBy { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
       // public string SyncCode { get; set; }
    }
}
