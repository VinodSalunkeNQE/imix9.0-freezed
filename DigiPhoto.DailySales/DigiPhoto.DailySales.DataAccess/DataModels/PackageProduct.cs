namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PackageProduct
    {
        [Key]
        public long PackageProductId { get; set; }
        
        public long PackageId { get; set; }

        public long ProductId { get; set; }

        public int? ProductQuantity { get; set; }

        public int? ProductMaxImage { get; set; }

        public virtual Package Package { get; set; }

        public virtual Product Product { get; set; }
    }
}
