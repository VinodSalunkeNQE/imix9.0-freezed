﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    [Table("OpeningFormDetails", Schema = "Sage")]
    public partial class OpeningFormDetails
    {
        [Key]
        public long OpeningFormDetailID { get; set; }

        [Column(TypeName = "date")]
        public DateTime BussinessDate { get; set; }

        [Column("6X8OpeningPrinterCount")]
        public long StartingNumber6X8 { get; set; }

        [Column("8X10OpeningPrinterCount")]
        public long StartingNumber8X10 { get; set; }

        [Column("6X8AutoOpeningPrinterCount")]
        public long Auto6X8OpeningPrinterCount { get; set; }

        [Column("8x10AutoOpeningPrinterCount")]
        public long Auto8x10OpeningPrinterCount { get; set; }

        [Column("PosterOpeningPrinterCount")]
        public long PosterStartingNumber { get; set; }

        [Required]
        public decimal CashFloatAmount { get; set; }

        public long SubStoreId { get; set; }

        [Required]
        public DateTime OpeningDate { get; set; }

        public long? FilledBy { get; set; }

        public string SyncCode { get; set; }
        // BY KCB ON 09 AUG 2019 FOR storing panorama printer inventory
        [Column("6900OpeningPrinterCount")]
        public long StartingNumber6900 { get; set; }

        [Column("6900AutoOpeningPrinterCount")]
        public long Auto6900OpeningPrinterCount { get; set; }
        //END

    }

}
