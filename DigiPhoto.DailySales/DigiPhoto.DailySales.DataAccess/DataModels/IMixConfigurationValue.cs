namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IMixConfigurationValue")]
    public partial class IMixConfigurationValue
    {
        [Key]
        public long ConfigurationValueId { get; set; }

        public long ConfigurationId { get; set; }

        [Required]
        [StringLength(200)]
        public string ConfigurationValue { get; set; }

        public int ConfigurationMasterId { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual DigiConfiguration DigiConfiguration { get; set; }
    }
}
