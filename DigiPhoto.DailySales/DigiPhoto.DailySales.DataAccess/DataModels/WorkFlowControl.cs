﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WorkFlowControl", Schema = "Sage")]
    public partial class WorkFlowControl
    {
        [Key]
        public long WorkFlowControlID { get; set; }

        public long SiteId { get; set; }

        public Int32 FormTypeID { get; set; }

        public long FormID { get; set; }

        public DateTime FilledOn { get; set; }

        public long? FiledBy { get; set; }
        
        [Column(TypeName = "date")]
        public DateTime BussinessDate { get; set; }
    }
}
