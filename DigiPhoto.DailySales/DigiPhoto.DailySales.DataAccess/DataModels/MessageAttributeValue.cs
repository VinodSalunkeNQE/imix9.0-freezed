namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageAttributeValue")]
    public partial class MessageAttributeValue
    {
        public long MessageAttributeValueId { get; set; }

        public long MessageId { get; set; }

        public long AttributeId { get; set; }

        [StringLength(1000)]
        public string Value { get; set; }

        public virtual MessageAttribute MessageAttribute { get; set; }

        public virtual MessageQueue MessageQueue { get; set; }
    }
}
