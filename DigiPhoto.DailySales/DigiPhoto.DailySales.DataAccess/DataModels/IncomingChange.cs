namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IncomingChange")]
    public partial class IncomingChange
    {
        [Key]
        public long IncomingChangeId { get; set; }

        public long ChangeTrackingId { get; set; }

        public long ApplicationObjectId { get; set; }

        public long ObjectValueId { get; set; }

        public int ChangeAction { get; set; }

        public DateTime ChangeDateOnIMIX { get; set; }

        public long ChangeByOnIMIX { get; set; }

        public DateTime ChangeRecievedOn { get; set; }

        [Column(TypeName = "xml")]
        public string DataXML { get; set; }

        [StringLength(50)]
        public string StoreName { get; set; }

        [StringLength(50)]
        public string SubStoreCode { get; set; }
        
        public int ProcessingStatus { get; set; }

        [Required]
        [StringLength(100)]
        public string EntityCode { get; set; }
    }
}

