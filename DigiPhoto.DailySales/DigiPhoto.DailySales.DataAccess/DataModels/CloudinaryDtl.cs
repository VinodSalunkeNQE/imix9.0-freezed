﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CloudinaryDtl")]
    public partial class CloudinaryDtl
    {
        [Key]
        public long CloudinaryInfoID { get; set; }

        public Int32 PartnerId { get; set; }

        public Int32 WebPhotoID { get; set; }
        [StringLength(500)]
        public string SourceImageID { get; set; }
        [StringLength(500)]
        public string CloudinaryPublicID { get; set; }

        public Int16 CloudinaryStatusID { get; set; }
        public Int16 RetryCount { get; set; }
        public string ErrorMessage { get; set; }
        [Required]
        [StringLength(125)]
        public string AddedBy { get; set; }
        [Required]
        public DateTime CreatedDateTime { get; set; }
        [StringLength(125)]
        public string ModifiedBy { get; set; }

        public DateTime ModifiedDateTime { get; set; }
        [Required]
        public bool IsActive { get; set; }

    }
}
