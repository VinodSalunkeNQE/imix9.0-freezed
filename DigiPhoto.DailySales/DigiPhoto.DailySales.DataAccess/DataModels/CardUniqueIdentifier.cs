namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CardUniqueIdentifier")]
    public partial class CardUniqueIdentifier
    {
        public int CardUniqueIdentifierId { get; set; }

        public int CardTypeId { get; set; }

        [Column("CardUniqueIdentifier")]
        [StringLength(50)]
        public string CardUniqueIdentifier1 { get; set; }

        public int? DigiMasterLocationId { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }
    }
}
