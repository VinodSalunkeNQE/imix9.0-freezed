﻿

namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Scene")]
    public class Scene
    {
        [Key]
        public long SceneId { get; set; }

        [StringLength(1000)]
        public string SceneName { get; set; }
        public long ProductId { get; set; }
        public long BorderId { get; set; }
        public long BackgoundId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public Nullable<System.DateTime> ModifiedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }

    }
}
