namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        public Customer()
        {
            CustomerQRCodeMappings = new HashSet<CustomerQRCodeMapping>();
        }

        [Key]
        [Column(Order = 0)]
        public long CustomerId { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Password { get; set; }

        [StringLength(20)]
        public string MobileNumber { get; set; }

        public int CustomerType { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<CustomerQRCodeMapping> CustomerQRCodeMappings { get; set; }
    }
}
