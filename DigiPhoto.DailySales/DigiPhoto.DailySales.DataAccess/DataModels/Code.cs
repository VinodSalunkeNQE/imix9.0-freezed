namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Code
    {
        public Code()
        {
            Users = new HashSet<User>();
        }

        public long CodeId { get; set; }

        public long CodeTableId { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public long? ParentCodeId { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }

        public virtual CodeTable CodeTable { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
