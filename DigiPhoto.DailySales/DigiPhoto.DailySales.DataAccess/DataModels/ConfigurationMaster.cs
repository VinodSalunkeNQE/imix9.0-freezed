namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConfigurationMaster")]
    public partial class ConfigurationMaster
    {
        public int ConfigurationMasterId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DisplayLabel { get; set; }

        [Required]
        [StringLength(50)]
        public string ControlType { get; set; }

        public int OrderBy { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string TableNameForDdl { get; set; }

        public bool? IsRequired { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
