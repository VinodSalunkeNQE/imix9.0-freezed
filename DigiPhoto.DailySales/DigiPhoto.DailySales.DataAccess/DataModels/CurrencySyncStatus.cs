﻿namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class CurrencySyncStatus
    {
 
        public long CurrencySyncStatusID { get; set; }
        [Required]       
        public long CurrencyProfileID { get; set; }
        [Required]
        public long VenueID { get; set; }
        [Required]
        public Int32 SyncStatus { get; set; }
        [Required]     
        public long CreatedBy { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }     
        [Required]
        public bool CurrencyProfileStatus { get; set; }

    }
}
