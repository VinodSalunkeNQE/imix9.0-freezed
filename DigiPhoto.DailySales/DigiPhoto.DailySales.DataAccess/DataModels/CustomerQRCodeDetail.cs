namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerQRCodeDetail")]
    public partial class CustomerQRCodeDetail
    {
        [Key]
        public long CustomerQRCodeId { get; set; }

     
        public string IdentificationCode { get; set; }

        [Required]
        [StringLength(50)]
        public string OrderId { get; set; }

        public Int64? PackageId { get; set; }

        public string Photos { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public bool IsActive { get; set; }

        public long? SubStoreId { get; set; }

        public DateTime OrderDate { get; set; }

        public virtual Package1 Package1 { get; set; }

        public virtual SubStore SubStore { get; set; }

        public Int64? IncomingChangeId { get; set; }
    }
}
