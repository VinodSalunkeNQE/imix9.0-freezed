namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApplicationObject")]
    public partial class ApplicationObject
    {
        public ApplicationObject()
        {
            ChangeTrackings = new HashSet<ChangeTracking>();
            ObjectAuthorizations = new HashSet<ObjectAuthorization>();
        }

        public long ApplicationObjectId { get; set; }

        [Required]
        [StringLength(100)]
        public string ApplicationObjectName { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<ChangeTracking> ChangeTrackings { get; set; }

        public virtual ICollection<ObjectAuthorization> ObjectAuthorizations { get; set; }
    }
}
