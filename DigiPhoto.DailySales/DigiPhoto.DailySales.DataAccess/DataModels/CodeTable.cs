namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CodeTable")]
    public partial class CodeTable
    {
        public CodeTable()
        {
            Codes = new HashSet<Code>();
        }

        public long CodeTableId { get; set; }

        [StringLength(100)]
        public string CodeTableName { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        public int? Category { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }

        public virtual ICollection<Code> Codes { get; set; }
    }
}
