namespace DigiPhoto.DailySales.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Photo
    {
        public int PhotoId { get; set; }

        [Required]
        [StringLength(150)]
        public string FileName { get; set; }

        [StringLength(150)]
        public string RFID { get; set; }
        public string Frame { get; set; }
        public string Background { get; set; }
        public string Layering { get; set; }
        public string Effects { get; set; }
        public bool? IsCroped { get; set; }
        public bool? IsGreen { get; set; }
        public string MetaData { get; set; }
        public bool? IsRedEye { get; set; }
        public bool? IsArchived { get; set; }
        public long? LocationId { get; set; }
        public bool Active { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long SubStoreId { get; set; }
        public int IMIXPhotoId { get; set; }
        public DateTime CreatedOnMIX { get; set; }
        public DateTime? CaptureDate { get; set; }
        public virtual SubStore SubStore { get; set; }
        public decimal? GPSLatitude { get; set; }
        public decimal? GPSLongitude { get; set; }
        public int? MediaType { get; set; }
        public decimal? VideoLength { get; set; }
        //Added to get insert PartnerID in dbo.photos by shweta
        public long? PartnerID { get; set; }
        //end
    }
}
