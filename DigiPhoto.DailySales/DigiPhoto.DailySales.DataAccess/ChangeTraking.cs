﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.Model;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DigiPhoto.DailySales.Utility;
using log4net;

namespace DigiPhoto.DailySales.DataAccess
{
    public class ChangeTraking
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string ConnectionString = CommonUtility.ConnectionString;
        public static string ConnectionStringlocal = CommonUtility.ConnectionStringLocatl;

        public static ChangeInfo GetChangeInfo(ChangeInfo lastChangeInfo)
        {
            //log.StartMethod();
            SqlDataReader reader = null;
            SqlParameter[] parameters = new SqlParameter[]
                                        {
                                             new SqlParameter("@SubStoreCode", lastChangeInfo.SubStore.SiteCode),
                                             new SqlParameter("@ChangeTrackingId",lastChangeInfo.ChangeTrackingId),

                                             new SqlParameter("@countrycode",lastChangeInfo.SubStore.Store.Country.CountryCode),
                                             new SqlParameter("@VenueCode",lastChangeInfo.SubStore.Store.StoreCode),
                                             new SqlParameter("@SiteCode",lastChangeInfo.SubStore.DG_SiteCode)
                                        };

            try
            {
                reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.StoredProcedure, "uspGetNextChange", parameters);
                ChangeInfo changeInfo = new ChangeInfo();
                while (reader.Read())
                {
                    changeInfo.ChangeTrackingId = Convert.ToInt64(reader["ChangeTrackingId"]);
                    changeInfo.ApplicationObjectId = Convert.ToInt64(reader["ApplicationObjectId"]);
                    changeInfo.ObjectValueId = Convert.ToInt64(reader["ObjectValueId"]);
                    changeInfo.ChangeAction = Convert.ToInt32(reader["ChangeAction"]);
                    changeInfo.ChangeDate = Convert.ToDateTime(reader["ChangeDate"]);
                    //changeInfo.dataXML = GetChangedDataXML(changeInfo.ApplicationObjectId, changeInfo.ObjectValueId);
                    changeInfo.ChangeBy = Convert.ToInt64(reader["ChangeBy"]);
                    changeInfo.EntityCode = Convert.ToString(reader["EntityCode"]);
                    changeInfo.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);

                }
                return changeInfo;
            }
            finally
            {
                //log.EndMethod();
                reader.Close();
            }
        }

        #region Sync Service Enhancement - Ashirwad
        public static long GetOnlinePartnerStatus(QrCodeStatus QRCODE)
        {
            //log.StartMethod();
            long id = 0;
            SqlDataReader reader = null;
            SqlParameter[] parameters = new SqlParameter[]
                                        {
                                             new SqlParameter("@QRCODE", QRCODE.IdentificationCode),
                                             new SqlParameter("@OrderNumber", QRCODE.OrderNumber)
                                        };

            try
            {
                reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.StoredProcedure, "[Partner].[GetOnlineOrderStatus]", parameters);

                while (reader.Read())
                {
                    id = Convert.ToInt64(reader["CustomerQRCodeId"]);

                }
                return id;
            }
            finally
            {
                //log.EndMethod();
                reader.Close();
            }
        }
        #endregion


        //////created by latika start for AR Personalised
        public static int CheckIsPersonalized(int OrderID)
        {
            log.StartMethod();
            int retVal = 0;
            SqlParameter[] parameters = new SqlParameter[]
                                        {
                                             new SqlParameter("@OrderID", OrderID)
                                        };

            try
            {
                retVal = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionStringlocal, CommandType.StoredProcedure, "USP_CheckIsPersonalized", parameters));


            }
            catch (Exception ex)
            {
                log.Error("CheckIsPersonalized:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;

            }
            return retVal;
        }
        //////created by latika end
    }
}
