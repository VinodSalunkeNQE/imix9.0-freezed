﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OpeningFormDetails", Schema = "Sage")]
    public partial class OpeningFormDetails
    {
        [Key]
        public long OpeningFormDetailID { get; set; }

        [Required]
        public DateTime BussinessDate { get; set; }

        [Column("6X8StartingNumber")]
        public long StartingNumber6X8 { get; set; }

        [Column("8X10StartingNumber")]
        public long StartingNumber8X10 { get; set; }

        public long PosterStartingNumber { get; set; }

        [Column("6X8AutoStartingNumber")]
        public long Auto6X8StartingNumber{ get; set; }

        [Column("8X10AutoStartingNumber")]
        public long Auto8X10StartingNumber { get; set; }

        [Required]
        public decimal CashFloatAmount { get; set; }

        public Int32 SubStoreID { get; set; }

        [Required]
        public DateTime OpeningDate { get; set; }
        
        public Int32 FilledBy { get; set; }
        
        public string SyncCode { get; set; }
    }
}
