namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_CameraDetails
    {
        [Key]
        public int DG_Camera_pkey { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Camera_Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Camera_Make { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Camera_Model { get; set; }

        public int? DG_AssignTo { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Camera_Start_Series { get; set; }

        public int? DG_Updatedby { get; set; }

        public DateTime? DG_UpdatedDate { get; set; }

        public bool? DG_Camera_IsDeleted { get; set; }

        public int? DG_Camera_ID { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }
    }
}
