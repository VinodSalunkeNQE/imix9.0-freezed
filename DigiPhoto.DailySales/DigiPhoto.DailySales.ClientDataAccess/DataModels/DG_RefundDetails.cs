namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_RefundDetails
    {
        [Key]
        public int DG_RefundDetail_ID { get; set; }

        public int? DG_LineItemId { get; set; }

        [StringLength(50)]
        public string RefundPhotoId { get; set; }

        public int? DG_RefundMaster_ID { get; set; }

        [Column(TypeName = "money")]
        public decimal? Refunded_Amount { get; set; }

        public string RefundReason { get; set; }
    }
}
