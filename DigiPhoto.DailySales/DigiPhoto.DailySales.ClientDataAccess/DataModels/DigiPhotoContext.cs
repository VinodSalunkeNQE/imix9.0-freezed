﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Collections.Generic;

    public partial class DigiPhotoContext : DbContext
    {
        public DigiPhotoContext()
            : base("name=DigiPhotoEntities")
        {
        }

        public virtual DbSet<ApplicationObject> ApplicationObjects { get; set; }

        public virtual DbSet<DailySalesSyncStatus> DailySalesSyncStatus { get; set; }
        public virtual DbSet<ChangeTracking> ChangeTrackings { get; set; }
        public virtual DbSet<ChangeTrackingExceptionDtl> ChangeTrackingExceptionDtls { get; set; }
        public virtual DbSet<ArchivedPhoto> ArchivedPhotos { get; set; }
        public virtual DbSet<AudioTemplate> AudioTemplates { get; set; }
        public virtual DbSet<BackupHistory> BackupHistories { get; set; }
        public virtual DbSet<ClientApplicationVersion> ClientApplicationVersions { get; set; }
        public virtual DbSet<DG_Actions_Type> DG_Actions_Type { get; set; }
        public virtual DbSet<DG_Activity> DG_Activity { get; set; }
        public virtual DbSet<DG_AssociatedPrinters> DG_AssociatedPrinters { get; set; }
        public virtual DbSet<DG_BackGround> DG_BackGround { get; set; }
        public virtual DbSet<DG_Bill_Format> DG_Bill_Format { get; set; }
        public virtual DbSet<DG_Borders> DG_Borders { get; set; }
        public virtual DbSet<DG_CameraDetails> DG_CameraDetails { get; set; }
        public virtual DbSet<DG_CashBox> DG_CashBox { get; set; }
        public virtual DbSet<DG_Configuration> DG_Configuration { get; set; }
        public virtual DbSet<DG_Currency> DG_Currency { get; set; }
        public virtual DbSet<DG_Emails> DG_Emails { get; set; }
        public virtual DbSet<DG_EmailSettings> DG_EmailSettings { get; set; }
        public virtual DbSet<DG_Graphics> DG_Graphics { get; set; }
        public virtual DbSet<DG_Location> DG_Location { get; set; }
        public virtual DbSet<DG_Moderate_Photos> DG_Moderate_Photos { get; set; }
        public virtual DbSet<DG_Orders> DG_Orders { get; set; }
        public virtual DbSet<DG_Orders_Details> DG_Orders_Details { get; set; }
        public virtual DbSet<DG_Orders_DiscountType> DG_Orders_DiscountType { get; set; }
        public virtual DbSet<DG_Orders_ProductType> DG_Orders_ProductType { get; set; }
        public virtual DbSet<DG_PackageDetails> DG_PackageDetails { get; set; }
        public virtual DbSet<DG_Permission_Role> DG_Permission_Role { get; set; }
        public virtual DbSet<DG_Permissions> DG_Permissions { get; set; }
        public virtual DbSet<DG_Photo_Effects> DG_Photo_Effects { get; set; }
        public virtual DbSet<DG_PhotoGroup> DG_PhotoGroup { get; set; }
        public virtual DbSet<DG_PhotoNumberConfiguration> DG_PhotoNumberConfiguration { get; set; }
        public virtual DbSet<DG_Photos> DG_Photos { get; set; }
        public virtual DbSet<DG_Photos_Changes> DG_Photos_Changes { get; set; }
        public virtual DbSet<DG_Preview_Counter> DG_Preview_Counter { get; set; }
        public virtual DbSet<DG_PrinterQueue> DG_PrinterQueue { get; set; }
        public virtual DbSet<DG_Printers> DG_Printers { get; set; }
        public virtual DbSet<DG_PrintLog> DG_PrintLog { get; set; }
        public virtual DbSet<DG_Product_Pricing> DG_Product_Pricing { get; set; }
        public virtual DbSet<DG_ReceiptPrinter> DG_ReceiptPrinter { get; set; }
        public virtual DbSet<DG_Refund> DG_Refund { get; set; }
        public virtual DbSet<DG_RefundDetails> DG_RefundDetails { get; set; }
        public virtual DbSet<DG_RideCameraConfiguration> DG_RideCameraConfiguration { get; set; }
        public virtual DbSet<DG_SemiOrder_Settings> DG_SemiOrder_Settings { get; set; }
        public virtual DbSet<DG_Services> DG_Services { get; set; }
        public virtual DbSet<DG_Store> DG_Store { get; set; }
        public virtual DbSet<DG_SubStore_Locations> DG_SubStore_Locations { get; set; }
        public virtual DbSet<DG_SubStores> DG_SubStores { get; set; }
        public virtual DbSet<DG_SyncStatus> DG_SyncStatus { get; set; }
        public virtual DbSet<DG_User_Roles> DG_User_Roles { get; set; }
        public virtual DbSet<DG_Users> DG_Users { get; set; }
        public virtual DbSet<DG_VersionHistory> DG_VersionHistory { get; set; }
        public virtual DbSet<Emai_Messages> Emai_Messages { get; set; }
        public virtual DbSet<iIMIXConfigurationMaster> iIMIXConfigurationMasters { get; set; }
        public virtual DbSet<iMIXConfigurationValue> iMIXConfigurationValues { get; set; }
        public virtual DbSet<IncomingChange> IncomingChanges { get; set; }
        public virtual DbSet<PhotoAlbumPrintOrder> PhotoAlbumPrintOrders { get; set; }
        public virtual DbSet<ProcessedVideo> ProcessedVideos { get; set; }
        public virtual DbSet<SyncSession> SyncSessions { get; set; }
        public virtual DbSet<tblPhotoDetail> tblPhotoDetails { get; set; }
        public virtual DbSet<tblprinter> tblprinters { get; set; }
        public virtual DbSet<ValueType> ValueTypes { get; set; }
        public virtual DbSet<ValueTypeGroup> ValueTypeGroups { get; set; }
        public virtual DbSet<VideoBackground> VideoBackgrounds { get; set; }
        public virtual DbSet<VideoTemplate> VideoTemplates { get; set; }
        public virtual DbSet<DG_Albums> DG_Albums { get; set; }
        public virtual DbSet<DG_Albums_Photos> DG_Albums_Photos { get; set; }
        public virtual DbSet<DG_ApplicationSettings> DG_ApplicationSettings { get; set; }
        public virtual DbSet<DG_ColorCodes> DG_ColorCodes { get; set; }
        public virtual DbSet<DG_SubProducts> DG_SubProducts { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<LastChangeDetail> LastChangeDetails { get; set; }
        public virtual DbSet<ProcessedVideoDetail> ProcessedVideoDetails { get; set; }
        public virtual DbSet<VideoSlot> VideoSlots { get; set; }

        public virtual DbSet<CloudinaryDtl> CloudinaryDtls { get; set; }
        public virtual DbSet<ApplicationClientVersion> ApplicationClientVersion { get; set; }

        public virtual DbSet<ImixPosDetail> ImixPosDetails { get; set; }

        public virtual DbSet<OpeningFormDetails> OpeningFormDetails { get; set; }

        public virtual DbSet<ClosingFormDetails> ClosingFormDetails { get; set; }

        public virtual DbSet<TransDetails> TransDetails { get; set; }

        public virtual DbSet<InventoryConsumable> InventoryConsumables { get; set; }
        public virtual DbSet<CurrencyProfile> CurrencyProfiles { get; set; }
        public virtual DbSet<CurrencyProfileRate> CurrencyProfileRates { get; set; }
        public DbSet<DG_Scene> DG_Scene { get; set; }

        public virtual DbSet<ResyncHistory> ResyncHistorys { get; set; }

        public virtual DbSet<ChangeTrackingProcessingStatusDtl> ChangeTrackingProcessingStatusDtls { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationObject>()
                .Property(e => e.ApplicationObjectName)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationObject>()
                .HasMany(e => e.ChangeTrackings)
                .WithRequired(e => e.ApplicationObject)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ChangeTracking>()
                .Property(e => e.EntityCode)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivedPhoto>()
                .Property(e => e.DG_Photos_FileName)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivedPhoto>()
                .Property(e => e.DG_Photos_RFID)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivedPhoto>()
                .Property(e => e.DG_Photos_Background)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivedPhoto>()
                .Property(e => e.DG_Photos_Frame)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivedPhoto>()
                .Property(e => e.DG_Photos_Sizes)
                .IsUnicode(false);

            modelBuilder.Entity<AudioTemplate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AudioTemplate>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<AudioTemplate>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<BackupHistory>()
                .Property(e => e.ErrorMessage)
                .IsUnicode(false);

            modelBuilder.Entity<ClientApplicationVersion>()
                .Property(e => e.VersionName)
                .IsUnicode(false);

            modelBuilder.Entity<ClientApplicationVersion>()
                .Property(e => e.MACAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ClientApplicationVersion>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ClientApplicationVersion>()
                .Property(e => e.Path)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Actions_Type>()
                .Property(e => e.DG_Actions_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Activity>()
                .Property(e => e.DG_Acitivity_Descrption)
                .IsUnicode(false);

            modelBuilder.Entity<DG_AssociatedPrinters>()
                .Property(e => e.DG_AssociatedPrinters_PaperSize)
                .IsUnicode(false);

            modelBuilder.Entity<DG_BackGround>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Bill_Format>()
                .Property(e => e.DG_Refund_Slogan)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Borders>()
                .Property(e => e.DG_Border)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Borders>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CameraDetails>()
                .Property(e => e.DG_Camera_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CameraDetails>()
                .Property(e => e.DG_Camera_Make)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CameraDetails>()
                .Property(e => e.DG_Camera_Model)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CameraDetails>()
                .Property(e => e.DG_Camera_Start_Series)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CameraDetails>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_CashBox>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_Hot_Folder_Path)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_Frame_Path)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_BG_Path)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.WiFiStartingNumber)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.FolderStartingNumber)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_ReceiptPrinter)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_Graphics_Path)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_ChromaColor)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_ChromaTolerance)
                .HasPrecision(3, 1);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_DbBackupPath)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_CleanupTables)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_HfBackupPath)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_ScheduleBackup)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.DG_MktImgPath)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.EK_SampleImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.FtpIP)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.FtpUid)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.FtpPwd)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Configuration>()
                .Property(e => e.FtpFolder)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Currency>()
                .Property(e => e.DG_Currency_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Currency>()
                .Property(e => e.DG_Currency_Symbol)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Currency>()
                .Property(e => e.DG_Currency_Icon)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Currency>()
                .Property(e => e.DG_Currency_Code)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Currency>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_OrderId)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_Email_To)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_Email_Bcc)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_EmailSender)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_Message)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_MediaName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DG_Emails>()
                .Property(e => e.DG_OtherMessage)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_MailSendFrom)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_MailSubject)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_MailBody)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_SmtpServername)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_SmtpServerport)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_SmtpServerUsername)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_SmtpServerPassword)
                .IsUnicode(false);

            modelBuilder.Entity<DG_EmailSettings>()
                .Property(e => e.DG_MailBCC)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Graphics>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Location>()
                .Property(e => e.DG_Location_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Location>()
                .Property(e => e.DG_Location_PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Location>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders>()
                .Property(e => e.DG_Orders_Number)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders>()
                .Property(e => e.DG_Order_Mode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders>()
                .Property(e => e.DG_Orders_Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders>()
                .Property(e => e.DG_Orders_NetCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders>()
                .Property(e => e.DG_Orders_Canceled_Reason)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Photos_ID)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_Details>()
               .Property(e => e.SyncCode)
               .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Orders_LineItems_DiscountType)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Orders_LineItems_DiscountAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Orders_Details_Items_UniPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Orders_Details_Items_TotalCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders_Details>()
                .Property(e => e.DG_Orders_Details_Items_NetPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_Orders_Details>()
                .HasMany(e => e.PhotoAlbumPrintOrders)
                .WithRequired(e => e.DG_Orders_Details)
                .HasForeignKey(e => e.OrderLineItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DG_Orders_DiscountType>()
                .Property(e => e.DG_Orders_DiscountType_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_DiscountType>()
                .Property(e => e.DG_Orders_DiscountType_Desc)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_DiscountType>()
                .Property(e => e.DG_Orders_DiscountType_Code)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_DiscountType>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_ProductType>()
                .Property(e => e.DG_Orders_ProductType_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_ProductType>()
                .Property(e => e.DG_Orders_ProductType_Desc)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_ProductType>()
                .Property(e => e.DG_Orders_ProductType_Image)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_ProductType>()
                .Property(e => e.DG_Orders_ProductCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Orders_ProductType>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PackageDetails>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Permissions>()
                .Property(e => e.DG_Permission_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Permissions>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photo_Effects>()
                .Property(e => e.DG_Photo_Effects_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PhotoGroup>()
                .Property(e => e.DG_Photo_RFID)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PhotoNumberConfiguration>()
                .Property(e => e.DG_WifiName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PhotoNumberConfiguration>()
                .Property(e => e.DG_StartingNumber)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DG_Photos>()
                .Property(e => e.DG_Photos_FileName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photos>()
                .Property(e => e.DG_Photos_RFID)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photos>()
                .Property(e => e.DG_Photos_Background)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photos>()
                .Property(e => e.DG_Photos_Frame)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photos>()
                .Property(e => e.DG_Photos_Sizes)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Photos>()
                .HasMany(e => e.PhotoAlbumPrintOrders)
                .WithRequired(e => e.DG_Photos)
                .HasForeignKey(e => e.PhotoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DG_Photos_Changes>()
                .Property(e => e.DG_Photos_Changes_Value)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PrinterQueue>()
                .Property(e => e.DG_PrinterQueue_Image_Pkey)
                .IsUnicode(false);

            modelBuilder.Entity<DG_PrinterQueue>()
                .Property(e => e.RotationAngle)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Printers>()
                .Property(e => e.DG_Printers_NickName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Printers>()
                .Property(e => e.DG_Printers_DefaultName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Printers>()
                .Property(e => e.DG_Printers_Address)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Refund>()
                .Property(e => e.RefundAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_RefundDetails>()
                .Property(e => e.RefundPhotoId)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RefundDetails>()
                .Property(e => e.Refunded_Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DG_RefundDetails>()
                .Property(e => e.RefundReason)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.DG_RideCamera_Id)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.AcquisitionFrameCount)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.AcquisitionFrameRateAbs)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.AcquisitionFrameRateLimit)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.AcquisitionMode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.BalanceRatioAbs)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.BalanceRatioSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.BalanceWhiteAuto)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.BalanceWhiteAutoAdjustTol)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.BalanceWhiteAutoRate)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.EventSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.EventsEnable1)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAuto)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoAdjustTol)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoAlg)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoMax)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoMin)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoOutliers)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoRate)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureAutoTarget)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ExposureTimeAbs)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAuto)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoAdjustTol)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoMax)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoMin)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoOutliers)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoRate)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainAutoTarget)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainRaw)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.GainSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.StrobeDelay)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.StrobeDuration)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.StrobeDurationMode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.StrobeSource)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncInGlitchFilter)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncInLevels)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncInSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncOutLevels)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncOutPolarity)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncOutSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.SyncOutSource)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerActivation)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerDelayAbs)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerMode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerOverlap)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.TriggerSource)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.UserSetDefaultSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.UserSetSelector)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.Width)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.WidthMax)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.Height)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.HeightMax)
                .IsUnicode(false);

            modelBuilder.Entity<DG_RideCameraConfiguration>()
                .Property(e => e.ImageSize)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SemiOrder_Settings>()
                .Property(e => e.DG_SemiOrder_Settings_ImageFrame)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SemiOrder_Settings>()
                .Property(e => e.DG_SemiOrder_Settings_ImageFrame_Vertical)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SemiOrder_Settings>()
                .Property(e => e.DG_SemiOrder_BG)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SemiOrder_Settings>()
                .Property(e => e.DG_SemiOrder_Graphics_layer)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SemiOrder_Settings>()
                .Property(e => e.DG_SemiOrder_Image_ZoomInfo)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Services>()
                .Property(e => e.DG_Sevice_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Services>()
                .Property(e => e.DG_Service_Display_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_Store_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_CentralServerIP)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_CenetralServerUName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_CenetralServerPassword)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_PreferredTimeToSyncFrom)
                .HasPrecision(18, 5);

            modelBuilder.Entity<DG_Store>()
                .Property(e => e.DG_PreferredTimeToSyncTo)
                .HasPrecision(18, 5);

            modelBuilder.Entity<DG_SubStores>()
                .Property(e => e.DG_SubStore_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_SubStores>()
                .Property(e => e.DG_SubStore_Description)
                .IsUnicode(false);

            modelBuilder.Entity<DG_User_Roles>()
                .Property(e => e.DG_User_Role)
                .IsUnicode(false);

            modelBuilder.Entity<DG_User_Roles>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.DG_User_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.DG_User_First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.DG_User_Last_Name)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.DG_User_PhoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.DG_User_Email)
                .IsUnicode(false);

            modelBuilder.Entity<DG_Users>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<DG_VersionHistory>()
                .Property(e => e.DG_Version_Number)
                .IsUnicode(false);

            modelBuilder.Entity<DG_VersionHistory>()
                .Property(e => e.DG_Machine)
                .IsUnicode(false);

            modelBuilder.Entity<Emai_Messages>()
                .Property(e => e.DG_Message)
                .IsUnicode(false);

            modelBuilder.Entity<iIMIXConfigurationMaster>()
                .Property(e => e.DataType)
                .IsUnicode(false);

            modelBuilder.Entity<iIMIXConfigurationMaster>()
                .HasMany(e => e.iMIXConfigurationValues)
                .WithRequired(e => e.iIMIXConfigurationMaster)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<iMIXConfigurationValue>()
                .Property(e => e.ConfigurationValue)
                .IsUnicode(false);

            modelBuilder.Entity<iMIXConfigurationValue>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessedVideo>()
                .Property(e => e.OutputVideoFileName)
                .IsUnicode(false);

            modelBuilder.Entity<ProcessedVideo>()
                .HasMany(e => e.ProcessedVideoDetails)
                .WithRequired(e => e.ProcessedVideo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SyncSession>()
                .Property(e => e.StoreCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncSession>()
                .Property(e => e.SiteCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncSession>()
                .Property(e => e.VenueCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncSession>()
                .HasMany(e => e.IncomingChanges)
                .WithRequired(e => e.SyncSession)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblprinter>()
                .Property(e => e.Printer1)
                .IsUnicode(false);

            modelBuilder.Entity<tblprinter>()
                .Property(e => e.Printer2)
                .IsUnicode(false);

            modelBuilder.Entity<ValueType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ValueTypeGroup>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<VideoBackground>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<VideoBackground>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<VideoBackground>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<VideoTemplate>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<VideoTemplate>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<VideoTemplate>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<DG_ColorCodes>()
                .Property(e => e.DG_ColorCode)
                .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.AddedBy)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.SourceImageID)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.CloudinaryPublicID)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.ErrorMessage)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.ModifiedBy)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.SyncCode)
               .IsUnicode(false);

            //modelBuilder.Entity<CloudinaryDtl>()
            //   .Property(e => e.OrderNumber)
            //   .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.IdentificationCode)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.ThumbnailDimension)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.UploadCompressedFilePath)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.TargetDirectory)
               .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.MACAddress)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.MachineName)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Country)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Store)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.SubStore)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
               .Property(e => e.Status)
               .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
               .Property(e => e.Description)
               .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
              .Property(e => e.SyncCode)
              .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.ThumbnailDimension)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.SystemName)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.IPAddress)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.MacAddress)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.CreatedBy)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.UpdatedBy)
               .IsUnicode(false);

            modelBuilder.Entity<ImixPosDetail>()
               .Property(e => e.SyncCode)
               .IsUnicode(false);

            modelBuilder.Entity<CurrencyProfile>()
                     .Property(e => e.ProfileName)
                     .IsUnicode(false);
            modelBuilder.Entity<CurrencyProfile>()
          .Property(e => e.SyncCode)
          .IsUnicode(false);
            modelBuilder.Entity<OpeningFormDetails>().ToTable("OpeningFormDetails", schemaName: "Sage");

            modelBuilder.Entity<ClosingFormDetails>().ToTable("ClosingFormDetails", schemaName: "Sage");

            modelBuilder.Entity<TransDetails>().ToTable("TransDetails", schemaName: "Sage");

            modelBuilder.Entity<DailySalesSyncStatus>().ToTable("DailySalesSyncStatus", schemaName: "dbo");


            modelBuilder.Entity<InventoryConsumable>().ToTable("InventoryConsumables", schemaName: "Sage");
            modelBuilder.Entity<CurrencyProfile>().ToTable("ProfileAudit", schemaName: "Currency");
            modelBuilder.Entity<CurrencyProfileRate>().ToTable("ProfileRate", schemaName: "Currency");


        }
        public List<iMIXconfigurationLocationValue> usp_getConfigurationLocationValue()
        {
            return this.Database.SqlQuery<iMIXconfigurationLocationValue>("Exec USP_GET_PreviewWallSubstoreWiseData").ToList();
        }
    }
}
