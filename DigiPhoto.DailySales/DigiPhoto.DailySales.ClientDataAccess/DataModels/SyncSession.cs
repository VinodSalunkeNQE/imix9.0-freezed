namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SyncSession
    {
        public SyncSession()
        {
            IncomingChanges = new HashSet<IncomingChange>();
        }

        public long SyncSessionId { get; set; }

        public Guid TokenId { get; set; }

        [Required]
        [StringLength(50)]
        public string StoreCode { get; set; }

        [StringLength(50)]
        public string SiteCode { get; set; }

        [StringLength(50)]
        public string VenueCode { get; set; }

        public DateTime SignInDate { get; set; }

        public DateTime? SignOutDate { get; set; }

        public long StartChangeTrackingId { get; set; }

        public long? EndChangeTrackingId { get; set; }

        public int Status { get; set; }

        public virtual ICollection<IncomingChange> IncomingChanges { get; set; }
    }
}
