namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_RideCameraConfiguration
    {
        [Key]
        public int DG_RideCamera_pkey { get; set; }

        [StringLength(200)]
        public string DG_RideCamera_Id { get; set; }

        [StringLength(200)]
        public string AcquisitionFrameCount { get; set; }

        [StringLength(200)]
        public string AcquisitionFrameRateAbs { get; set; }

        [StringLength(200)]
        public string AcquisitionFrameRateLimit { get; set; }

        [StringLength(200)]
        public string AcquisitionMode { get; set; }

        [StringLength(200)]
        public string BalanceRatioAbs { get; set; }

        [StringLength(200)]
        public string BalanceRatioSelector { get; set; }

        [StringLength(200)]
        public string BalanceWhiteAuto { get; set; }

        [StringLength(200)]
        public string BalanceWhiteAutoAdjustTol { get; set; }

        [StringLength(200)]
        public string BalanceWhiteAutoRate { get; set; }

        [StringLength(200)]
        public string EventSelector { get; set; }

        [StringLength(200)]
        public string EventsEnable1 { get; set; }

        [StringLength(200)]
        public string ExposureAuto { get; set; }

        [StringLength(200)]
        public string ExposureAutoAdjustTol { get; set; }

        [StringLength(200)]
        public string ExposureAutoAlg { get; set; }

        [StringLength(200)]
        public string ExposureAutoMax { get; set; }

        [StringLength(200)]
        public string ExposureAutoMin { get; set; }

        [StringLength(200)]
        public string ExposureAutoOutliers { get; set; }

        [StringLength(200)]
        public string ExposureAutoRate { get; set; }

        [StringLength(200)]
        public string ExposureAutoTarget { get; set; }

        [StringLength(200)]
        public string ExposureTimeAbs { get; set; }

        [StringLength(200)]
        public string GainAuto { get; set; }

        [StringLength(200)]
        public string GainAutoAdjustTol { get; set; }

        [StringLength(200)]
        public string GainAutoMax { get; set; }

        [StringLength(200)]
        public string GainAutoMin { get; set; }

        [StringLength(200)]
        public string GainAutoOutliers { get; set; }

        [StringLength(200)]
        public string GainAutoRate { get; set; }

        [StringLength(200)]
        public string GainAutoTarget { get; set; }

        [StringLength(200)]
        public string GainRaw { get; set; }

        [StringLength(200)]
        public string GainSelector { get; set; }

        [StringLength(200)]
        public string StrobeDelay { get; set; }

        [StringLength(200)]
        public string StrobeDuration { get; set; }

        [StringLength(200)]
        public string StrobeDurationMode { get; set; }

        [StringLength(200)]
        public string StrobeSource { get; set; }

        [StringLength(200)]
        public string SyncInGlitchFilter { get; set; }

        [StringLength(200)]
        public string SyncInLevels { get; set; }

        [StringLength(200)]
        public string SyncInSelector { get; set; }

        [StringLength(200)]
        public string SyncOutLevels { get; set; }

        [StringLength(200)]
        public string SyncOutPolarity { get; set; }

        [StringLength(200)]
        public string SyncOutSelector { get; set; }

        [StringLength(200)]
        public string SyncOutSource { get; set; }

        [StringLength(200)]
        public string TriggerActivation { get; set; }

        [StringLength(200)]
        public string TriggerDelayAbs { get; set; }

        [StringLength(200)]
        public string TriggerMode { get; set; }

        [StringLength(200)]
        public string TriggerOverlap { get; set; }

        [StringLength(200)]
        public string TriggerSelector { get; set; }

        [StringLength(200)]
        public string TriggerSource { get; set; }

        [StringLength(200)]
        public string UserSetDefaultSelector { get; set; }

        [StringLength(200)]
        public string UserSetSelector { get; set; }

        [StringLength(200)]
        public string Width { get; set; }

        [StringLength(200)]
        public string WidthMax { get; set; }

        [StringLength(200)]
        public string Height { get; set; }

        [StringLength(200)]
        public string HeightMax { get; set; }

        [Required]
        [StringLength(200)]
        public string ImageSize { get; set; }
    }
}
