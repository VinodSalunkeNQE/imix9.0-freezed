namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Printers
    {
        [Key]
        public int DG_Printers_Pkey { get; set; }

        [StringLength(250)]
        public string DG_Printers_NickName { get; set; }

        [StringLength(50)]
        public string DG_Printers_DefaultName { get; set; }

        [StringLength(250)]
        public string DG_Printers_Address { get; set; }
    }
}
