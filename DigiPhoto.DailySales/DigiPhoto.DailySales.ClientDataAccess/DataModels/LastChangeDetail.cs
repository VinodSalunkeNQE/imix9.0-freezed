namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LastChangeDetail")]
    public partial class LastChangeDetail
    {
        [Key]
        public long LastChangeDetailId { get; set; }

        public long LastChangeTrackingId { get; set; }
      
        public DateTime LastChangeDate { get; set; }

        public int SubStoreId { get; set; }
    }
}
