namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_ColorCodes
    {
        [Key]
        [Column(Order = 0)]
        public int DG_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string DG_ColorCode { get; set; }
    }
}
