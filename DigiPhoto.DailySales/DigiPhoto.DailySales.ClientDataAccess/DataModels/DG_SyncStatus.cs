namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_SyncStatus
    {
        [Key]
        public int DG_Sync_pkey { get; set; }

        public DateTime? DG_Sync_Date { get; set; }

        public bool? DG_Sync_Status { get; set; }
    }
}
