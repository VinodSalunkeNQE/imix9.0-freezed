﻿

namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class DG_Scene
    {
        [Key]
        public int DG_Scene_pkey { get; set; }
        public int DG_Product_Id { get; set; }
        public Nullable<int> DG_BackGround_Id { get; set; }
        public Nullable<int> DG_Border_Id { get; set; }
        public Nullable<int> DG_Graphics_Id { get; set; }
        public string SyncCode { get; set; }
        public Nullable<bool> IsSynced { get; set; }
        public bool IsActive { get; set; }
        public string DG_SceneName { get; set; }
    }
}
