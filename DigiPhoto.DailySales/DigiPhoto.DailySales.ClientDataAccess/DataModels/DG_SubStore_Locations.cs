namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_SubStore_Locations
    {
        [Key]
        public int DG_SubStore_Location_Pkey { get; set; }

        public int DG_SubStore_ID { get; set; }

        public int DG_Location_ID { get; set; }
    }
}
