namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VideoSlot
    {
        public long VideoSlotId { get; set; }

        public long? VideoTemplateId { get; set; }

        public long? FrameTimeIn { get; set; }

        public int? PhotoDisplayTime { get; set; }

        public virtual VideoTemplate VideoTemplate { get; set; }
    }
}
