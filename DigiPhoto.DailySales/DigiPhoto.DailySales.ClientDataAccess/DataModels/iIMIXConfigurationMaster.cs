namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("iIMIXConfigurationMaster")]
    public partial class iIMIXConfigurationMaster
    {
        public iIMIXConfigurationMaster()
        {
            iMIXConfigurationValues = new HashSet<iMIXConfigurationValue>();
        }

        [Key]
        public long IMIXConfigurationMasterId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string DisplayLabel { get; set; }

        [StringLength(50)]
        public string DataType { get; set; }
        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsSynced { get; set; }
        public virtual ICollection<iMIXConfigurationValue> iMIXConfigurationValues { get; set; }
    }
}
