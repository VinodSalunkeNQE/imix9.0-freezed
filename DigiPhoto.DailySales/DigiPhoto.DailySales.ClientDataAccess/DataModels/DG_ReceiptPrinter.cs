namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_ReceiptPrinter
    {
        [Key]
        public int DG_Receipt_PrinterId { get; set; }

        public string DG_Receipt_PrinterName { get; set; }

        public int DG_SubStore_Id { get; set; }
    }
}
