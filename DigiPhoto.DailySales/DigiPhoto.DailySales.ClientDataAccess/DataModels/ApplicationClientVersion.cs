﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

     [Table("ApplicationClientVersion")]
    public partial class ApplicationClientVersion
    {
        [Key]
        public int ApplicationClientVersionID { get; set; }
        public int ApplicationVersionID { get; set; }
        [StringLength(50)]
        public string MACAddress { get; set; }

        [StringLength(100)]
        public string MachineName { get; set; }
        
        [StringLength(50)]
        public string IPAddress { get; set; }
        public DateTime UpdatedDate { get; set; }
        [StringLength(50)]
        public string Country { get; set; }
        
        [StringLength(50)]
        public string Store { get; set; }
        
        [StringLength(50)]
        public string SubStore { get; set; }
        
        [StringLength(100)]
        public string Status { get; set; }
       
        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(100)]
        public string SyncCode { get; set; }
    }
}
