﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{


    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DailySalesSyncStatus", Schema = "dbo")]
    public class DailySalesSyncStatus
    {
        [Key]
        public long ID { get; set; }

        public long ClosingFormDetailID { get; set; }

        public int ReportSyncStatus { get; set; }
    }
}
