namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Location
    {
        [Key]
        public int DG_Location_pkey { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Location_Name { get; set; }

        public int DG_Store_ID { get; set; }

        [StringLength(150)]
        public string DG_Location_PhoneNumber { get; set; }

        public bool? DG_Location_IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }
    }
}
