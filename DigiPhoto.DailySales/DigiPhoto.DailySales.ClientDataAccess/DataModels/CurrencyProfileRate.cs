﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("ProfileRate")]
   public partial class CurrencyProfileRate
    {
          [Key]
       public long ProfileRateID { get; set; }
        [Required]
          public long ProfileAuditID { get; set; }
        [Required]
        public string CurrencyCode { get; set; }

       [Required]
        public string CurrencyName { get; set; }
       
        [Required]
        public decimal ExchangeRate { get; set; }
        [Required]
        public bool IsActive { get; set; }
 
        public long? CreatedBy { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public long? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
