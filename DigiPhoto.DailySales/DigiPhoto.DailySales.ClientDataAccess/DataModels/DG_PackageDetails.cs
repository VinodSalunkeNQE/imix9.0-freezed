namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_PackageDetails
    {
        [Key]
        public int DG_Package_Details_Pkey { get; set; }

        public int DG_ProductTypeId { get; set; }

        public int DG_PackageId { get; set; }

        public int? DG_Product_Quantity { get; set; }

        public int? DG_Product_MaxImage { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
