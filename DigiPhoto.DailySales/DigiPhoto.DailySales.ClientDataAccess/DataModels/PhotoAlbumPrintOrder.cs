namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhotoAlbumPrintOrder")]
    public partial class PhotoAlbumPrintOrder
    {
        [Key]
        public long PrintOrderId { get; set; }

        public int OrderLineItemId { get; set; }

        public int PhotoId { get; set; }

        public int PageNo { get; set; }

        public int PrintPosition { get; set; }

        public int? RotationAngle { get; set; }

        public virtual DG_Orders_Details DG_Orders_Details { get; set; }

        public virtual DG_Photos DG_Photos { get; set; }
    }
}
