﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CloudinaryDtl")]
    public partial class CloudinaryDtl
    {
        [Key]
        public long CloudinaryInfoID { get; set; }

        public Int32 PhotoID { get; set; }
        [StringLength(1000)]
        public string SourceImageID { get; set; }
        [StringLength(1000)]
        public string CloudinaryPublicID { get; set; }

        public Int16 CloudinaryStatusID { get; set; }
        public Int16 RetryCount { get; set; }
        public string ErrorMessage { get; set; }
        [Required]
        public string AddedBy { get; set; }
        [Required]
        public DateTime CreatedDateTime { get; set; }
        [StringLength(250)]
        public string ModifiedBy { get; set; }

        public DateTime ModifiedDateTime { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [StringLength(100)]
        public string SyncCode { get; set; }
        public Int32 OrderId { get; set; }
        [StringLength(50)]
        public string IdentificationCode { get; set; }
        public Int32 Height { get; set; }

        public Int32 Width { get; set; }
        [StringLength(50)]
        public string ThumbnailDimension { get; set; }
        [StringLength(512)]
        public string UploadCompressedFilePath { get; set; }
        [StringLength(512)]
        public string TargetDirectory { get; set; }


    }
}
