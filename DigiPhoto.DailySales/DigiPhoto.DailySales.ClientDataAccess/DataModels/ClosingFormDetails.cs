﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClosingFormDetails", Schema = "Sage")]
    public partial class ClosingFormDetails
    {
        [Key]
        public long ClosingFormDetailID { get; set; }

        [Column("6X8ClosingNumber")]
        public long ClosingNumber6X8 { get; set; }

        [Column("8X10ClosingNumber")]
        public long ClosingNumber8X10 { get; set; }

        public long PosterClosingNumber { get; set; }

        [Column("6X8AutoClosingNumber")]
        public long Auto6X8ClosingNumber { get; set; }

        [Column("8X10AutoClosingNumber")]
        public long Auto8X10ClosingNumber { get; set; }

        //[Column("6x8Folder")]
        //[Required]
        //public long Folder6x8 { get; set; }

        //[Column("8x10Folder")]
        //[Required]
        //public long Folder8x10 { get; set; }

        //[Column("6x8FolderCase")]
        //[Required]
        //public long FolderCase6x8 { get; set; }

        //[Column("8x10FolderCase")]
        //[Required]
        //public long FolderCase8x10 { get; set; }

        //[Required]
        //public long RovingTicket { get; set; }

        //[Required]
        //public long RovingBand { get; set; }

        //[Required]
        //public Int32 PlasticBag { get; set; }

        [Column("6x8Westage")]
        public long Westage6x8 { get; set; }

        [Column("8x10Westage")]
        public long Westage8x10 { get; set; }

        public long PosterWestage { get; set; }

        [Column("6X8AutoWestage")]
        public long Auto6X8Westage { get; set; }

        [Column("8X10AutoWestage")]
        public long Auto8X10Westage { get; set; }

        [Required]
        public Int32 Attendance { get; set; }

        [Required]
        public decimal LaborHour { get; set; }

        [Required]
        public long NoOfCapture { get; set; }

        [Required]
        public long NoOfPreview { get; set; }

        [Required]
        public long NoOfImageSold { get; set; }

        public string Comments { get; set; }

        public Int32 SubStoreID { get; set; }

        [Required]
        public DateTime ClosingDate { get; set; }

        [Required]
        public Int32 FilledBy { get; set; }

        public DateTime TransDate { get; set; }

        public decimal? Cash { get; set; }

        public decimal? CreditCard { get; set; }

        public decimal? Amex { get; set; }

        public decimal? FCV { get; set; }

        public decimal? RoomCharges { get; set; }

        public decimal? KVL { get; set; }

        public decimal? Vouchers { get; set; }

        [Column("6x8PrintCount")]
        public long PrintCount6x8 { get; set; }

        [Column("8x10PrintCount")]
        public long PrintCount8x10 { get; set; }

        public long? PosterPrintCount { get; set; }
        public string SyncCode { get; set; }
        public int NoOfTransactions { get; set; }
    }
}
