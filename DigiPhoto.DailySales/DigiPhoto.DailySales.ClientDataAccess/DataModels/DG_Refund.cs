namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Refund
    {
        public int DG_RefundId { get; set; }

        public int? DG_OrderId { get; set; }

        [Column(TypeName = "money")]
        public decimal? RefundAmount { get; set; }

        public DateTime? RefundDate { get; set; }

        public int? UserId { get; set; }

        public int? Refund_Mode { get; set; }
    }
}
