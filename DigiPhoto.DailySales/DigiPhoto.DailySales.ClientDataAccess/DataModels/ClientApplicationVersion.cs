namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ClientApplicationVersion
    {
        public int ClientApplicationVersionID { get; set; }

        [Required]
        [StringLength(50)]
        public string VersionName { get; set; }

        [Required]
        [StringLength(50)]
        public string MACAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string IPAddress { get; set; }

        public int Major { get; set; }

        public int Minor { get; set; }

        public int Revision { get; set; }

        public int BuildNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReleaseDate { get; set; }

        [StringLength(100)]
        public string Path { get; set; }

        public bool IsActive { get; set; }
    }
}
