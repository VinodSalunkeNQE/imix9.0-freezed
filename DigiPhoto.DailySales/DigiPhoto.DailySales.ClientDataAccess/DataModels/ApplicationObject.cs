namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApplicationObject")]
    public partial class ApplicationObject
    {
        public ApplicationObject()
        {
            ChangeTrackings = new HashSet<ChangeTracking>();
        }

        public long ApplicationObjectId { get; set; }

        [Required]
        [StringLength(100)]
        public string ApplicationObjectName { get; set; }

        public bool IsActive { get; set; }

        #region Sync Service Enhancement - Ashirwad
        public int SyncPriority { get; set; }
        #endregion
        public virtual ICollection<ChangeTracking> ChangeTrackings { get; set; }
    }
}
