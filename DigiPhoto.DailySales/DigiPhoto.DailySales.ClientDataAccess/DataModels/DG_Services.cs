namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Services
    {
        [Key]
        public int DG_Service_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Sevice_Name { get; set; }

        [Required]
        [StringLength(100)]
        public string DG_Service_Display_Name { get; set; }

        [StringLength(500)]
        public string DG_Service_Path { get; set; }

        public bool? IsInterface { get; set; }
    }
}
