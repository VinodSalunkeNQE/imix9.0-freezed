namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_EmailSettings
    {
        [Key]
        [Column("DG-EmailSettings_pkey")]
        public int DG_EmailSettings_pkey { get; set; }

        [StringLength(200)]
        public string DG_MailSendFrom { get; set; }

        [StringLength(250)]
        public string DG_MailSubject { get; set; }

        public string DG_MailBody { get; set; }

        [StringLength(200)]
        public string DG_SmtpServername { get; set; }

        [StringLength(50)]
        public string DG_SmtpServerport { get; set; }

        public bool? DG_SmtpUserDefaultCredentials { get; set; }

        [StringLength(200)]
        public string DG_SmtpServerUsername { get; set; }

        [StringLength(100)]
        public string DG_SmtpServerPassword { get; set; }

        public bool? DG_SmtpServerEnableSSL { get; set; }

        [StringLength(200)]
        public string DG_MailBCC { get; set; }
    }
}
