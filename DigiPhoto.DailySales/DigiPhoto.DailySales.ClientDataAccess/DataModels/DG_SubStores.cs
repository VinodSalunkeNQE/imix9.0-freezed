﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_SubStores
    {
        [Key]
        public int DG_SubStore_pkey { get; set; }

        [StringLength(200)]
        public string DG_SubStore_Name { get; set; }

        [StringLength(500)]
        public string DG_SubStore_Description { get; set; }

        public bool? DG_SubStore_IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }

        public string DG_SubStore_Code { get; set; }

        public Int32? LogicalSubStoreID { get; set; }
        
        //public DG_SubStores logicalSubStoreDetails { get; set; }

        public bool IsLogicalSubStore { get; set; }
    }
}
