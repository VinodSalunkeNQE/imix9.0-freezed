﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChangeTrackingExceptionDtl")]
    public partial class ChangeTrackingExceptionDtl
    {
        [Key]
        public long ExceptionDtlID { get; set; }
        [Required]
        public long ChangeTrackingId { get; set; }
        public string ExceptionMsg { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string InnerException { get; set; }
        [Required]
        public DateTime ExceptionOnDate { get; set; }
        public string SyncCode { get; set; }
        public virtual ChangeTracking ChangeTracking { get; set; }
    }
}
