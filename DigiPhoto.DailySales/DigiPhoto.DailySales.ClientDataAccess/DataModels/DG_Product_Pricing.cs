namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Product_Pricing
    {
        [Key]
        public int DG_Product_Pricing_Pkey { get; set; }

        public int? DG_Product_Pricing_ProductType { get; set; }

        public double? DG_Product_Pricing_ProductPrice { get; set; }

        public int? DG_Product_Pricing_Currency_ID { get; set; }

        public DateTime? DG_Product_Pricing_UpdateDate { get; set; }

        public int? DG_Product_Pricing_CreatedBy { get; set; }

        public int? DG_Product_Pricing_StoreId { get; set; }

        public bool? DG_Product_Pricing_IsAvaliable { get; set; }
    }
}
