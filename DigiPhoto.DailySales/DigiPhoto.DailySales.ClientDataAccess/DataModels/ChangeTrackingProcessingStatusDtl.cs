﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

     [Table("ChangeTrackingProcessingStatusDtl")]
    public partial class ChangeTrackingProcessingStatusDtl
    {
         [Key]
        public long ExceptionDtlID { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public int Stage { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string InnerException { get; set; }
        public long IncomingChangeId { get; set; }

        public long ChangeTrackingId { get; set; }
        public string VenueName { get; set; }
        public string SyncCode { get; set; }
        public int ErrorID { get; set; }
    }
}
