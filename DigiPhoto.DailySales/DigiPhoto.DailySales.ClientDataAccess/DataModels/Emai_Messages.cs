namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Emai_Messages
    {
        [Key]
        public int DG_Email_Message_pkey { get; set; }

        public string DG_Message { get; set; }
    }
}
