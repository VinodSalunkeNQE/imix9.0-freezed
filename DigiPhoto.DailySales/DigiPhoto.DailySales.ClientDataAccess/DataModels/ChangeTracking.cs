namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChangeTracking")]
    public partial class ChangeTracking
    {
        public long ChangeTrackingId { get; set; }

        public long ApplicationObjectId { get; set; }

        public long ObjectValueId { get; set; }

        public int ChangeAction { get; set; }

        public DateTime ChangeDate { get; set; }

        public long ChangeBy { get; set; }

        public int SyncStatus { get; set; }

        public DateTime? SyncDate { get; set; }

        [Required]
        [StringLength(100)]
        public string EntityCode { get; set; }

       //public DateTime? SyncDate { get; set; }

        public virtual ApplicationObject ApplicationObject { get; set; }

        public int SyncPriority { get; set; }

        public int RetryCount { get; set; }

        //public DateTime NextRetryDateTime { get; set; }

        public int? SyncRetryCount { get; set; }
    }
}
