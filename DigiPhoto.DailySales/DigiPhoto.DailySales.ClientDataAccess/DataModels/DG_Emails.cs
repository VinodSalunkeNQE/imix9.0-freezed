namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Emails
    {
        [Key]
        public int DG_Email_pkey { get; set; }

        [StringLength(100)]
        public string DG_OrderId { get; set; }

        public string DG_Email_To { get; set; }

        public string DG_Email_Bcc { get; set; }

        [StringLength(100)]
        public string DG_EmailSender { get; set; }

        public string DG_Message { get; set; }

        public bool? DG_Email_IsSent { get; set; }

        public DateTime? DG_DateTime { get; set; }

        [StringLength(2)]
        public string DG_MediaName { get; set; }

        public string DG_OtherMessage { get; set; }
    }
}
