namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BackupHistory")]
    public partial class BackupHistory
    {
        [Key]
        public int BackupId { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int Status { get; set; }

        [StringLength(1000)]
        public string ErrorMessage { get; set; }

        public int SubStoreId { get; set; }
    }
}
