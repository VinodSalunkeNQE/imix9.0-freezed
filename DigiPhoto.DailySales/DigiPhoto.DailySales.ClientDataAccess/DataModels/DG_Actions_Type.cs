namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Actions_Type
    {
        [Key]
        public int DG_Actions_pkey { get; set; }

        [StringLength(150)]
        public string DG_Actions_Name { get; set; }
    }
}
