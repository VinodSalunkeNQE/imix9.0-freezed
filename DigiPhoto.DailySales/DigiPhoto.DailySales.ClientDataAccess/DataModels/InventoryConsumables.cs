﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InventoryConsumables", Schema = "Sage")]
    public partial class InventoryConsumable
    {
        [Key]
        public long InventoryConsumablesID { get; set; }
        //Foreign Key
        [Required]
        public long ClosingFormDetailID { get; set; }
        [Required]
        public long AccessoryID { get; set; }
        [Required]
        public long ConsumeValue { get; set; }
        //Navigation properties 
        public virtual ClosingFormDetails ClosingFormDetails { get; set; }
    }
}