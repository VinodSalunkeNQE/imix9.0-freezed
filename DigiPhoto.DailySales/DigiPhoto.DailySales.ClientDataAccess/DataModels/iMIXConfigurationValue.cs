namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("iMIXConfigurationValue")]
    public partial class iMIXConfigurationValue
    {
        public long IMIXConfigurationValueId { get; set; }

        public long IMIXConfigurationMasterId { get; set; }

        [Required]
        [StringLength(500)]
        public string ConfigurationValue { get; set; }

        public int SubstoreId { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual iIMIXConfigurationMaster iIMIXConfigurationMaster { get; set; }
    }
}
