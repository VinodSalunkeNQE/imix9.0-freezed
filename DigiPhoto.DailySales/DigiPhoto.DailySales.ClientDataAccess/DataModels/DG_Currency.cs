namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Currency
    {
        [Key]
        public int DG_Currency_pkey { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_Currency_Name { get; set; }

        public double DG_Currency_Rate { get; set; }

        [StringLength(50)]
        public string DG_Currency_Symbol { get; set; }

        public DateTime? DG_Currency_UpdatedDate { get; set; }

        public int? DG_Currency_ModifiedBy { get; set; }

        public bool? DG_Currency_Default { get; set; }

        public string DG_Currency_Icon { get; set; }

        [StringLength(100)]
        public string DG_Currency_Code { get; set; }

        public bool? DG_Currency_IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
