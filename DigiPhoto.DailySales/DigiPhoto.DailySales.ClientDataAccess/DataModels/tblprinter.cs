namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblprinter
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Printer1 { get; set; }

        [StringLength(50)]
        public string Printer2 { get; set; }
    }
}
