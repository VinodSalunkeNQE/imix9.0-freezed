namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Photo_Effects
    {
        [Key]
        public int DG_Photo_Effects_pkey { get; set; }

        [StringLength(50)]
        public string DG_Photo_Effects_Name { get; set; }
    }
}
