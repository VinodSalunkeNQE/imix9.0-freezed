namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Users
    {
        [Key]
        public int DG_User_pkey { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_User_Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_User_First_Name { get; set; }

        [StringLength(50)]
        public string DG_User_Last_Name { get; set; }

        [Required]
        [StringLength(50)]
        public string DG_User_Password { get; set; }

        public int DG_User_Roles_Id { get; set; }

        public int DG_Location_ID { get; set; }

        public bool? DG_User_Status { get; set; }

        [StringLength(20)]
        public string DG_User_PhoneNo { get; set; }

        [StringLength(50)]
        public string DG_User_Email { get; set; }

        public DateTime? DG_User_CreatedDate { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
