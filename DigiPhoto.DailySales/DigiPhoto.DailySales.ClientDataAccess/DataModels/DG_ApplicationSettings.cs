namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_ApplicationSettings
    {
        [Key]
        public Guid DG_ApplicationID { get; set; }

        public DateTime? DG_LastSync { get; set; }
    }
}
