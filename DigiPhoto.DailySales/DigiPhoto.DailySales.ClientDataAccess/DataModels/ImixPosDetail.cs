﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ImixPosDetail")]
    public partial class ImixPosDetail
    {
        [Key]
        public long ImixPOSDetailID { get; set; }

        [StringLength(1000)]
        [Required]
        public string SystemName { get; set; }

        [StringLength(50)]
        [Required]
        public string IPAddress { get; set; }

        [StringLength(50)]
        [Required]
        public string MacAddress { get; set; }

        public long SubStoreID { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [StringLength(100)]
        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }
        [StringLength(100)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public bool IsStart { get; set; }

        public DateTime StartStopTime { get; set; }

        [StringLength(100)]
        public string SyncCode { get; set; }

    }
}
