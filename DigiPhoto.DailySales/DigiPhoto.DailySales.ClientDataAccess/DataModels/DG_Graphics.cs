namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Graphics
    {
        [Key]
        public int DG_Graphics_pkey { get; set; }

        [StringLength(100)]
        public string DG_Graphics_Name { get; set; }

        [StringLength(100)]
        public string DG_Graphics_Displayname { get; set; }

        public bool DG_Graphics_IsActive { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
