﻿namespace DigiPhoto.DailySales.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("ResyncHistory")]
    public partial class ResyncHistory
    {
        [Key]
        public Int64 ResyncHistoryID { get; set; }
        public DateTime ResyncDatetime { get; set; }
        public Int32 ResyncStatus { get; set; }
        public Int32 ResyncType { get; set; }
    }
}
