﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Business.Processing
{
    public class ActivityProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    ActivityInfo activityInfo = CommonUtility.DeserializeXML<ActivityInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var user = dbContext.Users.Where(r => r.SyncCode == activityInfo.UserSyncCode).FirstOrDefault();
                        if (user != null)
                        {
                            var activity = new Activity();
                            activity.AcitivityId = activityInfo.AcitivityId;
                            activity.AcitivityDate = activityInfo.AcitivityDate;
                            activity.AcitivityDescrption = activityInfo.AcitivityDescrption;
                            activity.ActionType = activityInfo.ActionType;
                            //activity.AcitivityId = activityInfo.AcitivityId;
                            activity.ReferenceID = activityInfo.ReferenceID;
                            activity.AcitivityBy = user.UserId;
                            activity.SyncCode = activityInfo.SyncCode;
                            //mark IsSynced as true, this is already synced.
                            activity.IsSynced = true;
                            dbContext.Activities.Add(activity);
                            dbContext.SaveChanges();
                            //var activityId = activity.AcitivityId;
                            //if (activityId != null)
                            //{
                            //    var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == activityId &&  r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                            //    if (objectauth != null)
                            //    {
                            //        dbContext.ObjectAuthorizations.Remove(objectauth);
                            //        dbContext.SaveChanges();

                            //    }
                            //    ObjectAuthorization obj = new ObjectAuthorization();
                            //    obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                            //    obj.ObjectValueId = activityId;
                            //    //obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                            //    dbContext.ObjectAuthorizations.Add(obj);
                            //    dbContext.SaveChanges();

                            //}

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ActivityProcessor:ProcessInser:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Activity Processor Insert.", ex);
                outerException.Source = "Business.Processing.ActivityProcessor";
                throw outerException;
            }
        }
    }
}
