﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DailySales.Business.Processing
{
    public class SceneProcessor:BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (ChangeInfo!=null && !string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    SceneInfo sceneInfo = CommonUtility.DeserializeXML<SceneInfo>(ChangeInfo.DataXML);
                    if (sceneInfo != null)
                    {
                        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                        {
                            var existingScene = dbContext.Scenes.Count(r => r.SyncCode == sceneInfo.SyncCode);
                            var siteSynccode = ChangeInfo.SubStore.SiteCode;
                            var storeId = (from DMStore in dbContext.DigiMasterLocations
                                           join DMSite in dbContext.DigiMasterLocations
                                           on DMStore.DigiMasterLocationId equals DMSite.ParentDigiMasterLocationId
                                           where DMSite.SyncCode == siteSynccode
                                           select DMStore.DigiMasterLocationId).FirstOrDefault();
                            var border = dbContext.Borders.FirstOrDefault(bdr => bdr.SyncCode == sceneInfo.BorderSyncCode);
                            var background = dbContext.Backgrounds.FirstOrDefault(bkg => bkg.SyncCode == sceneInfo.BackgroundSyncCode);
                            var product = dbContext.Products.FirstOrDefault(prd => prd.SyncCode == sceneInfo.ProductSyncCode);
                            if (existingScene == 0)
                            {
                                if (product != null)
                                {
                                    if (background != null)
                                    {
                                        if (border != null)
                                        {
                                            var scene = new Scene();
                                            scene.BackgoundId = background.BackgoundId;
                                            scene.BorderId = border.BorderId;
                                            scene.CreatedBy = 1;
                                            scene.CreatedDateTime = DateTime.Now;
                                            scene.IsActive = sceneInfo.IsActive;
                                            scene.IsSynced = sceneInfo.IsSynced;
                                            scene.ProductId = product.ProductId;
                                            scene.SceneName = sceneInfo.SceneName;
                                            scene.SyncCode = sceneInfo.SyncCode;
                                            dbContext.Scenes.Add(scene);
                                            dbContext.SaveChanges();
                                            Int64 sceneId = scene.SceneId;
                                            ObjectAuthorization obj = new ObjectAuthorization();
                                            obj.ApplicationObjectId = 25;
                                            obj.ObjectValueId = sceneId;
                                            obj.IsDeleted = 0;
                                            obj.DigiMasterLocationId = Convert.ToInt64(storeId);
                                            dbContext.ObjectAuthorizations.Add(obj);
                                            dbContext.SaveChanges();
                                            log.Error("ScenedProcessor:ProcessInsert:: Message: The scene was successfully saved to central database");
                                        }
                                        else
                                        {
                                            log.Error("ScenedProcessor:ProcessInsert:: Message: This border doesn't exist in the Central Database");
                                        }
                                    }
                                    else
                                    {
                                        log.Error("ScenedProcessor:ProcessInsert:: Message: This Background doesn't exist in the Central Database");
                                    }
                                }
                                else
                                {
                                    log.Error("ScenedProcessor:ProcessInsert:: Message: This product doesn't exist in the Central Database");
                                }
                            }
                            else
                            {
                                log.Error("ScenedProcessor:ProcessInsert:: Message: This scene already exists in the Central database");
                            }
                        }
                    }
                    else
                    {
                        log.Error("ScenedProcessor:ProcessInsert:: Message: SceneInfo was null. DataXML couldn't be parsed into SceneInfo");
                    }

                }
                else
                {
                    log.Error("ScenedProcessor:ProcessInsert:: Message: Change Info and  DataXML was null");
                }
            }
            catch (Exception ex)
            {
                log.Error("ScenedProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Scene Processor Insert.", ex);
                outerException.Source = "Business.Processing.SceneProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (ChangeInfo!=null && !string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    SceneInfo sceneInfo = CommonUtility.DeserializeXML<SceneInfo>(ChangeInfo.DataXML);
                    if (sceneInfo != null)
                    {
                        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                        {
                            var scene = dbContext.Scenes.Where(r => r.SyncCode == sceneInfo.SyncCode).FirstOrDefault();
                            var border = dbContext.Borders.FirstOrDefault(bdr => bdr.SyncCode == sceneInfo.BorderSyncCode);
                            var background = dbContext.Backgrounds.FirstOrDefault(bkg => bkg.SyncCode == sceneInfo.BackgroundSyncCode);
                            var product = dbContext.Products.FirstOrDefault(prd => prd.SyncCode == sceneInfo.ProductSyncCode);

                            if (scene != null)
                            {
                                if (product != null)
                                {
                                    if (background != null)
                                    {
                                        if (border != null)
                                        {
                                            scene.BackgoundId = background.BackgoundId;
                                            scene.BorderId = border.BorderId;
                                            scene.IsActive = sceneInfo.IsActive;
                                            scene.IsSynced = true;
                                            scene.ModifiedBy = 1;
                                            scene.ModifiedDateTime = DateTime.Now;
                                            scene.ProductId = product.ProductId;
                                            scene.SceneName = sceneInfo.SceneName;
                                            scene.SyncCode = sceneInfo.SyncCode;
                                            dbContext.SaveChanges();
                                            log.Error("ScenedProcessor:ProcessDelete:: Message: The scene was successfully updated to central database");
                                        }
                                        else
                                        {
                                            log.Error("ScenedProcessor:ProcessUpdate:: Message: This border doesn't exists in the Central database");
                                        }
                                    }
                                    else
                                    {
                                        log.Error("ScenedProcessor:ProcessUpdate:: Message: This background doesn't exists in the Central database");
                                    }
                                }
                                else
                                {
                                    log.Error("ScenedProcessor:ProcessUpdate:: Message: This Product doesn't exists in the Central database");
                                }
                            }
                            else
                            {
                                log.Error("ScenedProcessor:ProcessUpdate:: Message: This scene doesn't exists in the Central database");
                            }
                        }
                    }
                    else
                    {
                        log.Error("ScenedProcessor:ProcessUpdate:: Message: SceneInfo was null. DataXML couldn't be parsed into SceneInfo");
                    }
                }
                else
                {
                    log.Error("ScenedProcessor:ProcessUpdate:: Message: Change Info and  DataXML was null");

                }
            }
            catch (Exception ex)
            {
                log.Error("SceneProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Scene Processor Update.", ex);
                outerException.Source = "Business.Processing.SceneProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                if (ChangeInfo != null && !string.IsNullOrEmpty(ChangeInfo.EntityCode))
                {

                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbScene = dbContext.Scenes.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                        if (dbScene != null)
                        {
                            dbContext.Scenes.Remove(dbScene);
                            dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.
                            log.Error("ScenedProcessor:ProcessDelete:: Message: The scene was successfully deleted from the central database");
                        }
                        else
                        {
                            log.Error("ScenedProcessor:ProcessDelete:: Message: This scene doesn't exists in the Central database.");
                        }
                    }
                }
                else
                {
                    log.Error("ScenedProcessor:ProcessDelete:: Message: Change Info and  DataXML was null");
                }
            }
            catch (Exception ex)
            {
                log.Error("SceneProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Scene Processor  Delete.", ex);
                outerException.Source = "Business.Processing.SceneProcessor";
                throw outerException;
            }
        }
    }
}
