﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using log4net;
namespace DigiPhoto.DailySales.Business.Processing
{
    public abstract class BaseProcessor : IProcesser
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IncomingChangeInfo _ChangeInfo;
        protected IncomingChangeInfo ChangeInfo
        {
            get { return _ChangeInfo; }
            set { _ChangeInfo = value; }
        }
        public void Process(IncomingChangeInfo changeInfo)
        {
            this._ChangeInfo = changeInfo;
            try
            {
                PreProcess();
                DoProcess();
                PostProcess();
            }
            catch (Exception ex)
            {
                //mark this change with error.//TBD
                log.Error("Business.Processing:BaseProcessor::: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                SyncController.UpdateProcessingStatus(_ChangeInfo.IncomingChangeId, (int)SyncProcessingStatus.Error);
                throw ex;
            }
        }
        protected virtual void PreProcess()
        {

        }
        private void DoProcess()
        {
            if (!string.IsNullOrEmpty(_ChangeInfo.DataXML))
            {
                if (this._ChangeInfo.ChangeAction == 1)
                    ProcessInsert();
                else if (this._ChangeInfo.ChangeAction == 2)
                    ProcessUpdate();
                else if (this._ChangeInfo.ChangeAction == 3)
                    ProcessDelete();
            }
            //TBD - Enum
            else
            {
                if (this._ChangeInfo.ChangeAction == 3)
                    ProcessDelete();
            }
        }

        protected virtual void ProcessInsert()
        { }
        protected virtual void ProcessUpdate()
        { }
        protected virtual void ProcessDelete()
        { }
        protected virtual void PostProcess()
        {
            //mark this change as processed.//TBD  
           SyncController.UpdateProcessingStatus(_ChangeInfo.IncomingChangeId, (int)SyncProcessingStatus.Processed);
        }
    }
}
