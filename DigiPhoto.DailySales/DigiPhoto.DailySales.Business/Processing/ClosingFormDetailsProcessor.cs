﻿using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DailySales.DataAccess.DataModels;

namespace DigiPhoto.DailySales.Business.Processing
{
    public class ClosingFormDetailsProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    ClosingFormDetailsInfo closingFormDetailsInfo = CommonUtility.DeserializeXML<ClosingFormDetailsInfo>(ChangeInfo.DataXML);
                    if (closingFormDetailsInfo != null)
                    {
                        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                        {
                            Int32? UserId = null;
                            long SiteID = 0;
                            var SiteObject = dbContext.DigiMasterLocations.Where(s => s.SyncCode == closingFormDetailsInfo.SubStore.SyncCode && s.Level == 4).FirstOrDefault();
                            if (SiteObject == null)
                            {
                                throw new Exception("Site not found with Sync Code " + closingFormDetailsInfo.SubStore.SyncCode);
                            }
                            SiteID = SiteObject.DigiMasterLocationId;

                            //If Closing form is already submitted by for same bussiness date
                            var submittedClosingFormDetail = dbContext.ClosingFormDetails.Where(cf => cf.SubStoreID == SiteID && cf.TransDate.Year == closingFormDetailsInfo.TransDate.Year && cf.TransDate.Month == closingFormDetailsInfo.TransDate.Month && cf.TransDate.Day == closingFormDetailsInfo.TransDate.Day).FirstOrDefault();
                            if (submittedClosingFormDetail != null)
                            {
                                throw new Exception("Closing form already submitted for " + SiteObject.Name + " for bussiness date " + closingFormDetailsInfo.TransDate.ToShortDateString());
                            }
                            if (closingFormDetailsInfo.FilledBySyncCode != null)
                                UserId = Convert.ToInt32(dbContext.Users.Where(s => s.SyncCode == closingFormDetailsInfo.FilledBySyncCode).FirstOrDefault().UserId);
                            ClosingFormDetails closingFormDetail = new ClosingFormDetails();
                            closingFormDetail.ClosingFormDetailID = closingFormDetailsInfo.ClosingFormDetailID;
                            closingFormDetail.ClosingNumber6X8 = closingFormDetailsInfo.ClosingNumber6X8;
                            closingFormDetail.ClosingNumber8X10 = closingFormDetailsInfo.ClosingNumber8X10;
                            closingFormDetail.PosterClosingPrinterCount = closingFormDetailsInfo.PosterClosingNumber;
                            closingFormDetail.Auto6X8ClosingPrinterCount = closingFormDetailsInfo.Auto6X8ClosingPrinterCount;
                            closingFormDetail.Auto8X10ClosingPrinterCount = closingFormDetailsInfo.Auto8X10ClosingPrinterCount;
                            // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                            closingFormDetail.ClosingNumber6900 = closingFormDetailsInfo.ClosingNumber6900;
                            closingFormDetail.Auto6900ClosingPrinterCount = closingFormDetailsInfo.Auto6900ClosingPrinterCount;
                            closingFormDetail.Westage6900 = closingFormDetailsInfo.Westage6900;
                            //END
                            //Inventory Consumable is dynamic form.
                            //closingFormDetail.Folder6x8 = closingFormDetailsInfo.Folder6x8;
                            //closingFormDetail.Folder8x10 = closingFormDetailsInfo.Folder8x10;
                            //closingFormDetail.FolderCase6x8 = closingFormDetailsInfo.FolderCase6x8;
                            //closingFormDetail.FolderCase8x10 = closingFormDetailsInfo.FolderCase8x10;
                            //closingFormDetail.RovingTicket = closingFormDetailsInfo.RovingTicket;
                            //closingFormDetail.RovingBand = closingFormDetailsInfo.RovingBand;
                            //closingFormDetail.PlasticBag = closingFormDetailsInfo.PlasticBag;
                            closingFormDetail.Westage6x8 = closingFormDetailsInfo.Westage6x8;
                            closingFormDetail.Westage8x10 = closingFormDetailsInfo.Westage8x10;
                            closingFormDetail.PosterWestage = closingFormDetailsInfo.PosterWestage;
                            closingFormDetail.Auto6X8Westage = closingFormDetailsInfo.Auto6X8Westage;
                            closingFormDetail.Auto8x10Westage = closingFormDetailsInfo.Auto8x10Westage;
                            closingFormDetail.Attendance = closingFormDetailsInfo.Attendance;
                            closingFormDetail.LaborHour = closingFormDetailsInfo.LaborHour;
                            closingFormDetail.NoOfCapture = closingFormDetailsInfo.NoOfCapture;
                            closingFormDetail.NoOfPreview = closingFormDetailsInfo.NoOfPreview;
                            closingFormDetail.NoOfImageSold = closingFormDetailsInfo.NoOfImageSold;
                            closingFormDetail.Comments = closingFormDetailsInfo.Comments;
                            closingFormDetail.SubStoreID = SiteID;
                            closingFormDetail.ClosingDate = closingFormDetailsInfo.ClosingDate;
                            closingFormDetail.FilledBy = UserId;
                            closingFormDetail.TransDate = closingFormDetailsInfo.TransDate;
                            closingFormDetail.Cash = closingFormDetailsInfo.Cash;
                            closingFormDetail.CreditCard = closingFormDetailsInfo.CreditCard;
                            closingFormDetail.FCV = closingFormDetailsInfo.FCV;
                            closingFormDetail.KVL = closingFormDetailsInfo.KVL;
                            closingFormDetail.Amex = closingFormDetailsInfo.Amex;
                            closingFormDetail.RoomCharges = closingFormDetailsInfo.RoomCharges;
                            closingFormDetail.Vouchers = closingFormDetailsInfo.Vouchers;
                            closingFormDetail.PrintCount6x8 = closingFormDetailsInfo.PrintCount6x8;
                            closingFormDetail.PrintCount8x10 = closingFormDetailsInfo.PrintCount8x10;
                            closingFormDetail.PosterPrintCount = closingFormDetailsInfo.PosterPrintCount;
                            closingFormDetail.SyncCode = closingFormDetailsInfo.SyncCode;
                            closingFormDetail.NoOfTransactions = closingFormDetailsInfo.NoOfTransactions;
                            dbContext.ClosingFormDetails.Add(closingFormDetail);
                            dbContext.SaveChanges();

                            foreach (TransDetailsInfo transDetailsInfo in closingFormDetailsInfo.TransDetails)
                            {
                                long PackageId = dbContext.Packages.Where(x => x.SyncCode == transDetailsInfo.PackageSyncCode).FirstOrDefault().PackageId;
                                TransDetails transDetailsInfoObj = new TransDetails()
                                {
                                    SubstoreID = SiteID,
                                    TransDate = transDetailsInfo.TransDate,
                                    PackageID = Convert.ToInt32(PackageId),
                                    UnitPrice = transDetailsInfo.UnitPrice,
                                    Quantity = transDetailsInfo.Quantity,
                                    Discount = transDetailsInfo.Discount,
                                    Total = transDetailsInfo.Total,
                                    ClosingFormDetailID = closingFormDetail.ClosingFormDetailID
                                };
                                dbContext.TransDetails.Add(transDetailsInfoObj);
                            }
                            dbContext.SaveChanges();

                            //Inventory Consumable
                            foreach (InventoryConsumableInfo inventoryConsumableInfo in closingFormDetailsInfo.InventoryConsumables)
                            {
                                long accessoryID = dbContext.Products.Where(p => p.SyncCode == inventoryConsumableInfo.AccessorySyncCode).FirstOrDefault().ProductId;
                                InventoryConsumable inventoryConsumable = new InventoryConsumable()
                                {
                                    ClosingFormDetailID = closingFormDetail.ClosingFormDetailID,
                                    AccessoryID = accessoryID,
                                    ConsumeValue = inventoryConsumableInfo.ConsumeValue
                                };
                                dbContext.InventoryConsumables.Add(inventoryConsumable);
                            }
                            dbContext.SaveChanges();

                            WorkFlowControl workFlowControl = new WorkFlowControl()
                            {
                                SiteId = SiteID,
                                FormTypeID = 2,  //for closingFormDetails
                                FormID = closingFormDetail.ClosingFormDetailID,
                                FilledOn = closingFormDetail.ClosingDate,  //Closing Form Filled On.
                                BussinessDate =  closingFormDetail.TransDate.Date, //Closing Form on bussiness date.
                                FiledBy = UserId
                            };
                            dbContext.WorkFlowControls.Add(workFlowControl);
                            dbContext.SaveChanges();
                        } //End DbContext
                    }
                } //End null XML
            }  //End try
            catch (Exception ex)
            {
                log.Error("ClosingFormDetailsProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in ClosingFormDetails Processor Insert.", ex);
                outerException.Source = "Business.Processing.ClosingFormDetailsProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            throw new Exception("Not emplemented");
        }

        protected override void ProcessDelete()
        {
            throw new Exception("Not emplemented");
        }
    }
}
