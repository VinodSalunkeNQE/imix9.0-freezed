﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DailySales.Business.Processing
{
    public class LocationProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                long StoreId = 0;
                long SubStoreId = 0;
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    SiteInfo SubStorLst = CommonUtility.DeserializeXML<SiteInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        //var country = dbContext.DigiMasterLocations.Where(r => r.Name == SubStorLst.Store.Country.CountryName && r.Level== (int) LocationType.Country ).FirstOrDefault();
                        var country = dbContext.DigiMasterLocations.Where(r => r.Level == (int)LocationType.Country && (r.Code == SubStorLst.Store.Country.CountryCode || r.Name == SubStorLst.Store.Country.CountryName)).FirstOrDefault();
                        //Check Country Exist
                        if (country != null)
                        {
                            long? Locationid = country.DigiMasterLocationId;// dbContext.DigiMasterLocations.Where(x => x.Name == SubStorLst.Store.Country.CountryName).ToList().FirstOrDefault().DigiMasterLocationId;
                            var store = dbContext.DigiMasterLocations.Where(x => x.Level == (int)LocationType.Store &&
                                (x.Code == SubStorLst.Store.StoreCode
                                || x.Name == SubStorLst.Store.StoreName)
                                && 
                                x.ParentDigiMasterLocationId == country.DigiMasterLocationId).FirstOrDefault();
                            var locSub = new DigiMasterLocation();
                            if (store == null)
                            {
                                // Store Add
                                locSub.Name = SubStorLst.Store.StoreName;
                                locSub.SyncCode = CommonUtility.GetUniqueSynccode("13", "00", "000", "00");//  SubStorLst.Store.StoreCode;
                                locSub.Level = (int)LocationType.Store;
                                locSub.ParentDigiMasterLocationId = Locationid;
                                locSub.IsActive = true;
                                locSub.Description = SubStorLst.Store.StoreName;
                                locSub.Code = SubStorLst.Store.StoreCode;
                                locSub.IsSynced = true;
                                locSub.IsLogicalSubStore = false;
                                dbContext.DigiMasterLocations.Add(locSub);
                                dbContext.SaveChanges();
                                StoreId = locSub.DigiMasterLocationId;
                            }
                            else
                            {
                                //store Update
                                store.Name = SubStorLst.Store.StoreName;
                                //locSub.SyncCode = CommonUtility.GetUniqueSynccode("13", "00", "000", "00");
                                store.Level = (int)LocationType.Store;
                                store.ParentDigiMasterLocationId = Locationid;
                                store.IsActive = true;
                                store.Description = SubStorLst.Store.StoreName;
                               // store.Code = SubStorLst.Store.StoreCode;
                                store.IsSynced = true;
                                store.IsLogicalSubStore = false;
                                dbContext.SaveChanges();
                                StoreId = store.DigiMasterLocationId;
                            }
                            if (StoreId > 0)
                            {
                                var chkSubstor = new DigiMasterLocation();
                                var substore = chkSubstor;
                                if (SubStorLst.DG_SiteCode == null)
                                {
                                    substore = dbContext.DigiMasterLocations.Where(x => x.ParentDigiMasterLocationId == StoreId && x.SyncCode == SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                }
                                else
                                {
                                    substore = dbContext.DigiMasterLocations.Where(x => x.ParentDigiMasterLocationId == StoreId && x.Code == SubStorLst.DG_SiteCode && x.Name == SubStorLst.SiteName && x.SyncCode != SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                }
                                //var substore = dbContext.DigiMasterLocations.Where(x => x.ParentDigiMasterLocationId == StoreId && x.Code == SubStorLst.DG_SiteCode && x.Name == SubStorLst.SiteName && x.SyncCode != SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                var substoresynccode = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                // var substore = dbContext.DigiMasterLocations.Where(x => x.ParentDigiMasterLocationId==StoreId &&  x.SyncCode == SubStorLst.SiteCode && x.Level== (int)LocationType.SubStore).FirstOrDefault();
                                if (substore == null && substoresynccode==null)
                                {
                                    //Sub Store Add
                                    
                                    locSub.Name = SubStorLst.SiteName;
                                    locSub.SyncCode = SubStorLst.SiteCode;
                                    locSub.Level =  (int) LocationType.SubStore;
                                    locSub.ParentDigiMasterLocationId = StoreId;
                                    locSub.IsActive = true;
                                    locSub.Description = SubStorLst.SiteName;
                                    //locSub.Code = SubStorLst.SiteName;
                                    locSub.IsSynced = true;
                                    locSub.Code = SubStorLst.DG_SiteCode;
                                    locSub.IsLogicalSubStore = SubStorLst.IsLogicalSubStore;
                                    if (SubStorLst.LogicalSubStoreDetails != null)
                                    {
                                        var logicalsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.LogicalSubStoreDetails.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                        if (logicalsubstore != null)
                                            locSub.LogicalDigiMasterLocationId = Convert.ToInt32(logicalsubstore.DigiMasterLocationId);
                                    }

                                    dbContext.DigiMasterLocations.Add(locSub);
                                    dbContext.SaveChanges();
                                    SubStoreId = locSub.DigiMasterLocationId;
                                }
                                else
                                {
                                    if (substore.ParentDigiMasterLocationId == StoreId)
                                    {
                                        //Sub Store Add
                                        substore.Name = SubStorLst.SiteName;
                                        //No need to update Sync Code when updating existing site.
                                        //substore.SyncCode = SubStorLst.SiteCode;
                                        substore.Level = (int)LocationType.SubStore;
                                        substore.ParentDigiMasterLocationId = StoreId;
                                        substore.IsActive = true;
                                        substore.Description = SubStorLst.SiteName;
                                        //locSub.Code = SubStorLst.SiteName;
                                        substore.IsSynced = true;
                                      //  substore.Code = SubStorLst.DG_SiteCode;
                                        substore.IsLogicalSubStore = SubStorLst.IsLogicalSubStore;
                                        if (SubStorLst.LogicalSubStoreDetails != null)
                                        {
                                            var logicalsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.LogicalSubStoreDetails.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                            if (logicalsubstore != null)
                                                substore.LogicalDigiMasterLocationId = Convert.ToInt32(logicalsubstore.DigiMasterLocationId);
                                        }

                                        dbContext.SaveChanges();
                                        SubStoreId = substore.DigiMasterLocationId;
                                    }
                                    SubStoreId = substore.DigiMasterLocationId;
                                }

                                if (SubStoreId > 0)
                                {

                                    foreach (LocationInfo loc in SubStorLst.SubstorelocationDetails)
                                    {
                                        var location = dbContext.DigiMasterLocations.Where(x =>x.ParentDigiMasterLocationId==SubStoreId &&  x.Level== (int)LocationType.Location && x.SyncCode == loc.SyncCode).FirstOrDefault();
                                        if (location == null)
                                        {
                                            //Location Add
                                            locSub.Name = loc.Name;
                                            locSub.SyncCode = loc.SyncCode;
                                            locSub.Level = (int)LocationType.Location;
                                            locSub.ParentDigiMasterLocationId = SubStoreId;
                                            locSub.IsActive = true;
                                            locSub.Description = loc.Name;
                                            locSub.Code = loc.Name;
                                            locSub.IsSynced = true;
                                            locSub.IsLogicalSubStore = false;
                                            dbContext.DigiMasterLocations.Add(locSub);
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var locations = dbContext.DigiMasterLocations.Where(x => x.Level == 1).FirstOrDefault();
                            // CountryAdd.
                            // To do syncode
                            var locSub = new DigiMasterLocation();
                            locSub.Name = SubStorLst.Store.Country.CountryName;
                            locSub.Level = 2;
                            if (locations != null)
                            {
                                locSub.ParentDigiMasterLocationId = locations.DigiMasterLocationId;
                            }
                            else
                            {
                                locSub.ParentDigiMasterLocationId = 1;
                            }
                            locSub.IsActive = true;
                            locSub.Description = SubStorLst.Store.Country.CountryName;
                            locSub.Code = SubStorLst.Store.Country.CountryCode;
                            locSub.SyncCode = CommonUtility.GetUniqueSynccode("13", "00", "000", "00");// SubStorLst.Store.Country.CountryCode;
                            locSub.IsSynced = true;
                            locSub.IsLogicalSubStore = false;
                            dbContext.DigiMasterLocations.Add(locSub);
                            dbContext.SaveChanges();
                            long Locationid = locSub.DigiMasterLocationId;

                            if (Locationid > 0)
                            {
                                // Store Add
                                locSub.Name = SubStorLst.Store.StoreName;
                                locSub.SyncCode = CommonUtility.GetUniqueSynccode("13", "00", "000", "00");// SubStorLst.Store.StoreCode;
                                locSub.Level = 3;
                                locSub.ParentDigiMasterLocationId = Locationid;
                                locSub.IsActive = true;
                                locSub.Description = SubStorLst.Store.StoreName;
                                locSub.Code = SubStorLst.Store.StoreCode;
                                locSub.IsSynced = true;
                                locSub.IsLogicalSubStore = false;
                                dbContext.DigiMasterLocations.Add(locSub);
                                dbContext.SaveChanges();
                                StoreId = locSub.DigiMasterLocationId;

                                if (StoreId > 0)
                                {
                                    //Sub Store Add
                                    var objsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                    if (objsubstore == null)
                                    {
                                    locSub.Name = SubStorLst.SiteName;
                                    locSub.SyncCode = SubStorLst.SiteCode;
                                    locSub.Level = 4;
                                    locSub.ParentDigiMasterLocationId = StoreId;
                                    locSub.IsActive = true;
                                    locSub.Description = SubStorLst.SiteName;
                                    //locSub.Code = SubStorLst.SiteName;
                                    locSub.Code = SubStorLst.DG_SiteCode;
                                    locSub.IsLogicalSubStore = SubStorLst.IsLogicalSubStore;
                                    if (SubStorLst.LogicalSubStoreDetails != null)
                                    {
                                        var logicalsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.LogicalSubStoreDetails.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                        if (logicalsubstore != null)
                                            locSub.LogicalDigiMasterLocationId = Convert.ToInt32(logicalsubstore.DigiMasterLocationId);
                                    }
                                    locSub.IsSynced = true;
                                    dbContext.DigiMasterLocations.Add(locSub);
                                    dbContext.SaveChanges();
                                    SubStoreId = locSub.DigiMasterLocationId;
                                    }
                                    else
                                    {
                                        log.Info("Duplicate sync code found for site code" + SubStorLst.DG_SiteCode + " with sync code " + SubStorLst.SiteCode);
                                        SubStoreId = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SubStorLst.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault().DigiMasterLocationId;
                                    }
                                    if (SubStoreId > 0)
                                    {
                                        foreach (LocationInfo lI in SubStorLst.SubstorelocationDetails)
                                        {
                                            //Location Add

                                            locSub.Name = lI.Name;
                                            locSub.SyncCode = lI.SyncCode;
                                            locSub.Level = 5;
                                            locSub.ParentDigiMasterLocationId = SubStoreId;
                                            locSub.IsActive = true;
                                            locSub.Description = lI.Name;
                                            locSub.Code = lI.Name;
                                            locSub.IsSynced = true;
                                            locSub.IsLogicalSubStore = false;
                                            dbContext.DigiMasterLocations.Add(locSub);
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LocationProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Location Processor Insert.", ex);
                outerException.Source = "Business.Processing.LocationProcessor";
                throw outerException;
            }
        }

        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    SiteInfo SiteInfo = CommonUtility.DeserializeXML<SiteInfo>(ChangeInfo.DataXML);
                    long subStoreId = 0;
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        // Sub Store  Update
                        //If SubStore not Exist then Add the SubStore
                        var dbSubStore = dbContext.DigiMasterLocations.Where(r => r.SyncCode == SiteInfo.SiteCode).FirstOrDefault();
                        
                        if (dbSubStore != null)
                        {
                            if (dbSubStore.Level == Convert.ToInt32(LocationType.SubStore))
                            {
                                dbSubStore.Name = SiteInfo.SiteName;
                                dbSubStore.SyncCode = SiteInfo.SiteCode;
                                //dbSubStore.Code = SiteInfo.SiteName;
                                dbSubStore.Description = SiteInfo.SiteName;
                                dbSubStore.IsSynced = true;
                               // dbSubStore.Code = SiteInfo.DG_SiteCode;
                                dbSubStore.IsLogicalSubStore = SiteInfo.IsLogicalSubStore;
                                if (SiteInfo.LogicalSubStoreDetails != null)
                                {
                                    var logicalsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SiteInfo.LogicalSubStoreDetails.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                    if (logicalsubstore != null)
                                        dbSubStore.LogicalDigiMasterLocationId = Convert.ToInt32(logicalsubstore.DigiMasterLocationId);
                                }
                                dbContext.SaveChanges();
                                subStoreId = dbSubStore.DigiMasterLocationId;
                            }
                        }
                        else
                        {
                            //var store = (from s in dbContext.DigiMasterLocations
                            //             join c in dbContext.DigiMasterLocations on s.ParentDigiMasterLocationId equals c.ParentDigiMasterLocationId
                            //             where s.Name == SiteInfo.SiteName && s.Level == 3 && c.Name == SiteInfo.Store.Country.CountryName
                            //             select  new { store =  s }).FirstOrDefault();
                            var country = dbContext.DigiMasterLocations.Where(l => l.Name == SiteInfo.Store.Country.CountryName && l.Level == (int)LocationType.Country).FirstOrDefault();
                            var store = dbContext.DigiMasterLocations.Where(l => l.Name == SiteInfo.Store.StoreName && l.Level == (int)LocationType.Store && l.ParentDigiMasterLocationId == country.DigiMasterLocationId).FirstOrDefault();
                            if (store != null)
                            {
                                var locSub = new DigiMasterLocation();
                                locSub.Name = SiteInfo.SiteName;
                                locSub.SyncCode = SiteInfo.SiteCode;
                                locSub.Level = Convert.ToInt32(LocationType.SubStore);
                                locSub.ParentDigiMasterLocationId = store.DigiMasterLocationId;
                                locSub.IsActive = true;
                                locSub.Description = SiteInfo.SiteName;
                                //locSub.Code = SiteInfo.SiteName;
                                locSub.IsSynced = true;
                                locSub.Code = SiteInfo.DG_SiteCode;
                                locSub.IsLogicalSubStore = SiteInfo.IsLogicalSubStore;
                                if (SiteInfo.LogicalSubStoreDetails != null)
                                {
                                    var logicalsubstore = dbContext.DigiMasterLocations.Where(x => x.SyncCode == SiteInfo.LogicalSubStoreDetails.SiteCode && x.Level == (int)LocationType.SubStore).FirstOrDefault();
                                    if (logicalsubstore != null)
                                        locSub.LogicalDigiMasterLocationId = Convert.ToInt32(logicalsubstore.DigiMasterLocationId);
                                }
                                dbContext.DigiMasterLocations.Add(locSub);
                                dbContext.SaveChanges();
                                subStoreId = locSub.DigiMasterLocationId;
                            }

                        }

                        foreach (LocationInfo loc in SiteInfo.SubstorelocationDetails)
                        {
                            //Location Type Condition missing 
                            //If exist then update otherwise add location same for Substore
                            var dbLocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == loc.SyncCode).ToList().FirstOrDefault();
                            if (dbLocation != null)
                            {
                                if (dbLocation.Level == Convert.ToInt32(LocationType.Location))
                                {
                                    //Location Add
                                    dbLocation.Name = loc.Name;
                                    dbLocation.SyncCode = loc.SyncCode;
                                    dbLocation.Level = Convert.ToInt32(LocationType.Location);
                                    dbLocation.IsActive = true;
                                    dbLocation.Description = loc.Name;
                                    dbLocation.Code = loc.Name;
                                    dbLocation.IsSynced = true;
                                    dbLocation.IsLogicalSubStore = false;
                                    dbContext.SaveChanges();
                                }
                            }
                            else
                            {
                                var locSub = new DigiMasterLocation();
                                locSub.ParentDigiMasterLocationId = subStoreId;
                                locSub.Level = Convert.ToInt32(LocationType.Location);
                                locSub.Name = loc.Name;
                                locSub.Description = loc.Name;
                                locSub.Code = loc.Name;
                                locSub.SyncCode = loc.SyncCode;
                                locSub.IsSynced = true;
                                locSub.IsActive = true;
                                locSub.IsLogicalSubStore = false;
                                dbContext.DigiMasterLocations.Add(locSub);
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LocationProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Location Processor Update.", ex);
                outerException.Source = "Business.Processing.LocationProcessor";
                throw outerException;
            }
        }

        protected override void ProcessDelete()
        {
            try
            {

                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    SiteInfo SiteInfo = CommonUtility.DeserializeXML<SiteInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        //Location deleted.
                        var dbLocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == SiteInfo.Location.SyncCode).FirstOrDefault();
                        if (dbLocation != null)
                        {
                            if (dbLocation.Level == 5)
                            {
                                dbLocation.IsActive = false;
                                dbLocation.IsSynced = true;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                log.Error("LocationProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Location Processor  Delete.", ex);
                outerException.Source = "Business.Processing.LocationProcessor";
                throw outerException;
            }
        }

    }
}
