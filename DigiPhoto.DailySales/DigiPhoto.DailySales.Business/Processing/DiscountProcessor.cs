﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DailySales.Business.Processing
{
    public class DiscountProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    DiscountInfo discountInfo = CommonUtility.DeserializeXML<DiscountInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var user = dbContext.Users.Where(r => r.UserName == "Administrator").FirstOrDefault();
                        var existingDicountCount = dbContext.DiscountTypes.Count(r => r.SyncCode == discountInfo.SyncCode);
                        if (existingDicountCount == 0)
                        {
                            var discount = new DiscountType();

                            discount.Name = discountInfo.Name;
                            discount.Description = discountInfo.Description;
                            discount.IsActive = discountInfo.IsActive;
                            discount.Code = discountInfo.Code;
                            discount.Secure = discountInfo.Secure;
                            discount.ItemLevel = discountInfo.ItemLevel;
                            discount.AsPercentage = discountInfo.AsPercentage;
                            discount.SyncCode = discountInfo.SyncCode;
                            discount.CreatedDateTime = System.DateTime.Now;
                            discount.IsSynced = true;
                            discount.CreatedBy = user != null ? user.UserId :1;
                            dbContext.DiscountTypes.Add(discount);
                            dbContext.SaveChanges();
                            var digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                            long discountid = discount.DiscountTypeId;
                            if (discountid > 0)
                            {

                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == discountid && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                obj.ObjectValueId = discountid;
                                obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DiscountProcessor:ProcessInser:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Discount Processor Insert.", ex);
                outerException.Source = "Business.Processing.DiscountProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    DiscountInfo discountInfo = CommonUtility.DeserializeXML<DiscountInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbDiscount = dbContext.DiscountTypes.Where(r => r.SyncCode == discountInfo.SyncCode).FirstOrDefault();
                        if (dbDiscount != null)
                        {
                            //Update Role Name
                            dbDiscount.Name = discountInfo.Name;
                            dbDiscount.Description = discountInfo.Description;
                            dbDiscount.IsActive = discountInfo.IsActive;
                            dbDiscount.Code = discountInfo.Code;
                            dbDiscount.Secure = discountInfo.Secure;
                            dbDiscount.ItemLevel = discountInfo.ItemLevel;
                            dbDiscount.AsPercentage = discountInfo.AsPercentage;
                            dbDiscount.CreatedDateTime = System.DateTime.Now;
                            dbDiscount.IsSynced = true;
                            dbContext.SaveChanges();
                            //If multiple update operations are done, commit will be at last.

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DiscountProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Discount Processor Update.", ex);
                outerException.Source = "Business.Processing.DiscountProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbDiscount = dbContext.DiscountTypes.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    var digimasterlocation = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == dbDiscount.DiscountTypeId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();
                    if (digimasterlocation != null)
                    {

                        dbContext.ObjectAuthorizations.Remove(digimasterlocation);
                        dbContext.SaveChanges();

                    }
                    if (dbDiscount != null)
                    {
                        dbContext.DiscountTypes.Remove(dbDiscount);
                        dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.
                    }


                }


            }
            catch (Exception ex)
            {
                log.Error("DiscountProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Discount Processor  Delete.", ex);
                outerException.Source = "Business.Processing.DiscountProcessor";
                throw outerException;
            }
        }
    }
}
