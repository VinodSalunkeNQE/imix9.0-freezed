﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DailySales.Business.Processing
{
    public interface IProcesser
    {
        void Process(IncomingChangeInfo changeInfo);
    }
}
