﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DailySales.Business.Processing
{
    public class CurrencyProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    CurrencyInfo CurrencyInfo = CommonUtility.DeserializeXML<CurrencyInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var existingDicountCount = dbContext.Currencies.Count(r => r.SyncCode == CurrencyInfo.SyncCode);
                        var user = dbContext.Users.Where(r => r.SyncCode == CurrencyInfo.UserSyncCode).FirstOrDefault();
                        var digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                        if (existingDicountCount == 0)
                        {
                            var Currrency = new Currency();

                            Currrency.Name = CurrencyInfo.Name;
                            Currrency.Rate = CurrencyInfo.Rate;
                            Currrency.CurrencyIcon = CurrencyInfo.CurrencyIcon;
                            Currrency.Code = CurrencyInfo.CurrencyCode;
                            Currrency.IsDefault = CurrencyInfo.IsDefault;
                            if(user!=null)
                            {
                                Currrency.ModifiedBy = user.UserId;
                            }
                         
                            Currrency.ModifiedDateTime = CurrencyInfo.ModifiedDateTime;
                            Currrency.SyncCode = CurrencyInfo.SyncCode;
                            Currrency.Symbol = CurrencyInfo.Symbol;
                            Currrency.IsSynced = true;
                            dbContext.Currencies.Add(Currrency);
                            dbContext.SaveChanges();

                            long curriencyid = Currrency.CurrencyId;
                            if (curriencyid > 0)
                            {

                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == curriencyid && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                obj.ObjectValueId = curriencyid;
                                obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("CurrencyProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Currencies Processor Insert.", ex);
                outerException.Source = "Business.Processing.CurrencyProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    CurrencyInfo CurrencyInfo = CommonUtility.DeserializeXML<CurrencyInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbCurrency = dbContext.Currencies.Where(r => r.SyncCode == CurrencyInfo.SyncCode).FirstOrDefault();
                        var user = dbContext.Users.Where(r => r.SyncCode == CurrencyInfo.UserSyncCode).FirstOrDefault();
                        if (dbCurrency != null)
                        {
                            dbCurrency.Name = CurrencyInfo.Name;
                            dbCurrency.Rate = CurrencyInfo.Rate;
                            dbCurrency.CurrencyIcon = CurrencyInfo.CurrencyIcon;
                            dbCurrency.Code = CurrencyInfo.CurrencyCode;
                            dbCurrency.IsDefault = CurrencyInfo.IsDefault;
                            dbCurrency.ModifiedBy = user.UserId;
                            dbCurrency.ModifiedDateTime = CurrencyInfo.ModifiedDateTime;
                            dbCurrency.SyncCode = CurrencyInfo.SyncCode;
                            dbCurrency.Symbol = CurrencyInfo.Symbol;
                            dbCurrency.IsSynced = true;
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("CurrencyProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Currencies Processor Update.", ex);
                outerException.Source = "Business.Processing.CurrencyProcessor";
                throw outerException;
            }
        }

        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbCurrency = dbContext.Currencies.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbCurrency != null)
                    {
                        dbContext.Currencies.Remove(dbCurrency);
                        dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.

                        var digimasterlocation = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == dbCurrency.CurrencyId).FirstOrDefault();
                        if (digimasterlocation != null)
                        {
                            dbContext.ObjectAuthorizations.Remove(digimasterlocation);
                            dbContext.SaveChanges();

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("CurrencyProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Currency Processor  Delete.", ex);
                outerException.Source = "Business.Processing.CurrencyProcessor";
                throw outerException;
            }
        }
    }
}
