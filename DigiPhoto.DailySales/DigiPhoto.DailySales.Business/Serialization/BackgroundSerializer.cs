﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System.Configuration;
namespace DigiPhoto.DailySales.Business.Serialization
{
    public class BackgroundSerializer : Serializer, IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                   var background = dbContext.Backgrounds.Where(g => g.BackgoundId == ObjectValueId).FirstOrDefault();

                    BackgroundInfo backgroundInfo = (from b in dbContext.Backgrounds
                                                     join p in dbContext.Products on b.ProductId equals p.ProductId
                                                     where b.BackgoundId == ObjectValueId
                                                     select new BackgroundInfo
                                                        {
                                                            BackgoundId = b.BackgoundId,
                                                            BackgroundImage = b.BackgroundImage,
                                                            DisplayName = b.DisplayName,
                                                            BackgroundGroupId = b.BackgroundGroupId,
                                                            SyncCode = b.SyncCode,
                                                            Product = new ProductInfo
                                                            {
                                                                ProductId = p.ProductId,
                                                                Code = p.Code,
                                                                SyncCode = p.SyncCode
                                                            }
                                                            , IsActive=b.IsActive
                                                        }).FirstOrDefault();


                    if (backgroundInfo != null)
                    {
                        backgroundInfo.CreatedDate = DateTime.Now;
                        backgroundInfo.CreatedBy = background.CreatedBy;
                        if (background.ModifiedBy.HasValue)
                            backgroundInfo.ModifiedBy = background.ModifiedBy.Value;
                        if (background.ModifiedDateTime.HasValue)
                            background.ModifiedDateTime = background.ModifiedDateTime.Value;

                        dataXML = CommonUtility.SerializeObject<BackgroundInfo>(backgroundInfo);
                        //If this class implements IDownloadable interface, so populate object of DownloadInfo
                        string hostingWebPath = ConfigurationManager.AppSettings["DigiphotoiMixWebHostingPath"];

                        string BackroundFilePhysicalPath = Path.Combine(hostingWebPath, "Images\\Backgrounds", backgroundInfo.BackgroundImage);

                        if (File.Exists(BackroundFilePhysicalPath))
                        {
                            download = new DownloadInfo()
                            {
                                DownloadFilePath = BackroundFilePhysicalPath
                            };
                        }
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in GraphBackground Serialization.", e);
                outerException.Source = "Business.Serialization.BackgroundSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return download;
            }
            set
            {
                download = value;
            }
        }

    }
}
