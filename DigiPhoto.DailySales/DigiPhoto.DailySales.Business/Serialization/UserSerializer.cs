﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.Business.Serialization
{
    class UserSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;

                    var user = (from users in dbContext.Users join 
                                userdetail in dbContext.UserDetails on users.UserDetailId equals userdetail.UserDetailId join
                                 userrole in dbContext.UserRoles on users.UserId equals userrole.UserId join
                                 roles in dbContext.Roles on userrole.RoleId equals roles.RoleId   join
                                 userlocation in dbContext.UserAuthorizations on users.UserId equals userlocation.UserId join 
                                 location in dbContext.DigiMasterLocations on userlocation.DigiMasterLocationId  equals location.DigiMasterLocationId 
                                where    users.UserId == ObjectValueId                             

                                select new { myuser = users, myuserdetails = userdetail, myrole = roles,mylocation=location }).FirstOrDefault();

                    UserInfo userinfo = new UserInfo();
                    if (user != null)
                    {
                       
                        userinfo.UserName = user.myuser.UserName;
                        userinfo.FirstName = user.myuserdetails.FirstName;
                        userinfo.LastName = user.myuserdetails.LastName;
                        userinfo.Password = user.myuser.PasswordHash;
                        userinfo.IsActive = user.myuser.IsActive;
                        userinfo.PhoneNumber = user.myuserdetails.PhoneNumber;
                        userinfo.EmailAddress = user.myuserdetails.EmailAddress;
                        userinfo.CreatedDateTime = user.myuser.CreatedDateTime;
                        userinfo.LocationSyncCode= user.mylocation.SyncCode;
                        userinfo.RoleSyncCode = user.myrole.SyncCode;
                        userinfo.SyncCode = user.myuser.SyncCode;

                       
                        dataXML = CommonUtility.SerializeObject<UserInfo>(userinfo);
                    }


                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in User Serialization.", e);
                outerException.Source = "Business.Serialization.UserSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
