﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
namespace DigiPhoto.DailySales.Business.Serialization
{
    class DiscountSerializer:Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var discount = dbContext.DiscountTypes.Where(r => r.DiscountTypeId == ObjectValueId).FirstOrDefault();
                    if (discount != null)
                    {
                        DiscountInfo discountInfo = new DiscountInfo()
                        {
                            Name = discount.Name,
                            Description= discount.Description,
                            IsActive = discount.IsActive,                          
                            Code=discount.Code,
                            Secure=discount.Secure,
                            ItemLevel=discount.ItemLevel,
                            AsPercentage=discount.AsPercentage,
                            SyncCode = discount.SyncCode,
                       
                        };

                        dataXML = CommonUtility.SerializeObject<DiscountInfo>(discountInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Discount Serialization.", e);
                outerException.Source = "Business.Serialization.DiscountSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
