﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DailySales.Business.Serialization
{
    public class CurrencyProfileSerializer : Serializer, IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;

                    //here i need to get the values from ObjectAuthorizations to get the value ofis deleted

                    CurrencyProfileInfo objCurrencyProfile = (from b in dbContext.CurrencyProfiles
                                                              where b.CurrencyProfileID == ObjectValueId
                                           // join p in dbContext.ObjectAuthorizations on b.CurrencyProfileID equals p.ObjectValueId                                           
                                         //   && p.ApplicationObjectId==(Int64)ApplicationObjectEnum.CurrencyProfile
                                          //  orderby p.CreatedOn descending
                                         select new CurrencyProfileInfo
                                          {
                                              ProfileAuditID = b.CurrencyProfileID,
                                              ProfileName = b.ProfileName,
                                              startDate = b.StartDate,
                                              Enddate = b.EndDate,
                                              CreatedOn=b.CreatedOn,
                                              IsActive = b.IsActive,
                                              SyncCode = b.SyncCode,
                                              PublishedOn=b.PublishedOn,
                                              IsDeleted=b.IsDeleted
                                          }).FirstOrDefault();
                    if (objCurrencyProfile != null)
                    {

                        List<CurrencyProfileRateInfo> oblLst = (from a in dbContext.CurrencyProfileRates
                                                                join b in dbContext.CurrencyProfiles on a.CurrencyProfileID equals b.CurrencyProfileID
                                                                join c in dbContext.CurrencyMasters on a.CurrencyMasterID equals c.CurrencyMasterID
                                                                where a.CurrencyProfileID == objCurrencyProfile.ProfileAuditID
                                                                select new CurrencyProfileRateInfo
                                                                {
                                                                    ProfileRateID = a.CurrencyProfileRateID,
                                                                    ProfileAuditID = b.CurrencyProfileID,
                                                                    CurrencyName = c.CurrencyName,
                                                                    CurrencyCode = c.CurrencyCode,
                                                                    ExchangeRate = a.ExchangeRate,
                                                                    IsActive = b.IsActive,
                                                                    CreatedBy = b.CreatedBy,
                                                                    CreatedOn = b.CreatedOn,
                                                                    Updatedby = b.UpdatedBy,
                                                                    updatedon = b.UpdatedOn
                                                                }).ToList();

                        List<CurrencyProfileRateInfo> CurrencyProfileRateInfolst = new List<CurrencyProfileRateInfo>();
                        foreach (var item in oblLst)
                        {
                            CurrencyProfileRateInfo currencyProfileRateInfo = new CurrencyProfileRateInfo();
                            currencyProfileRateInfo.CreatedBy = item.CreatedBy;

                            currencyProfileRateInfo.ProfileRateID = item.ProfileRateID;
                            currencyProfileRateInfo.ProfileAuditID = item.ProfileAuditID;
                            currencyProfileRateInfo.CurrencyName = item.CurrencyName;
                            currencyProfileRateInfo.CurrencyCode = item.CurrencyCode;
                            currencyProfileRateInfo.ExchangeRate = item.ExchangeRate;
                            currencyProfileRateInfo.IsActive = item.IsActive;
                            currencyProfileRateInfo.CreatedBy = item.CreatedBy;
                            currencyProfileRateInfo.CreatedOn = item.CreatedOn;
                            currencyProfileRateInfo.Updatedby = item.Updatedby;
                            currencyProfileRateInfo.updatedon = item.updatedon;


                            CurrencyProfileRateInfolst.Add(currencyProfileRateInfo);


                        }
                        objCurrencyProfile.lstCurrencyProfileRate = CurrencyProfileRateInfolst;
                        //if (border != null)
                        //{

                        //    dataXML = CommonUtility.SerializeObject<BordersInfo>(border);
                        //    //If this class implements IDownloadable interface, so populate object of DownloadInfo
                        //    //TBD
                        //    string hostingWebPath = ConfigurationManager.AppSettings["DigiphotoiMixWebHostingPath"];
                        //    string BorderFilePhysicalPath = Path.Combine(hostingWebPath, "Images\\Borders", border.Name);
                        //    if (File.Exists(BorderFilePhysicalPath))
                        //    {
                        //        download = new DownloadInfo()
                        //        {
                        //            DownloadFilePath = BorderFilePhysicalPath
                        //        };
                        //    }

                        //}
                        dataXML = CommonUtility.SerializeObject<CurrencyProfileInfo>(objCurrencyProfile);
                    }
                    return dataXML;

                    //
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Currency Profile Serialization.", e);
                outerException.Source = "Business.Serialization.CurrencyProfileSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return download;
            }
            set
            {
                download = value;
            }
        }
    }
}
