﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DailySales.Business.Serialization
{
    public class BorderSerializer : Serializer , IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var borderInfo = dbContext.Borders.Where(g => g.BorderId == ObjectValueId).FirstOrDefault();
                    BordersInfo border = (from b in dbContext.Borders
                                          join p in dbContext.Products on b.ProductId equals p.ProductId
                                          where b.BorderId == ObjectValueId
                                          select new BordersInfo
                                          {
                                              BorderId = b.BorderId,
                                              Name = b.Name,
                                              ProductId = b.ProductId,
                                              IsActive = b.IsActive,
                                              SyncCode = b.SyncCode,
                                              Product = new ProductInfo
                                              {
                                                  ProductId = p.ProductId,
                                                  Code=p.Code,
                                                  SyncCode=p.SyncCode
                                              }

                                          }).FirstOrDefault();

                    if (border != null)
                    {
                        border.CreatedDate = DateTime.Now;
                        border.CreatedBy = borderInfo.CreatedBy;
                        if (borderInfo.ModifiedBy.HasValue)
                            border.ModifiedBy = borderInfo.ModifiedBy.Value;
                        if (borderInfo.ModifiedDateTime.HasValue)
                            border.ModifiedDate = borderInfo.ModifiedDateTime.Value;

                        dataXML = CommonUtility.SerializeObject<BordersInfo>(border);
                        //If this class implements IDownloadable interface, so populate object of DownloadInfo
                        //TBD
                        string hostingWebPath = ConfigurationManager.AppSettings["DigiphotoiMixWebHostingPath"];
                        string BorderFilePhysicalPath = Path.Combine(hostingWebPath, "Images\\Borders", border.Name);
                        if (File.Exists(BorderFilePhysicalPath))
                        {
                            download = new DownloadInfo()
                            {
                                DownloadFilePath = BorderFilePhysicalPath
                            };
                        }
                       
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Border Serialization.", e);
                outerException.Source = "Business.Serialization.BorderSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return download;
            }
            set
            {
                download = value;
            }
        }
    }
}
