﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DailySales.Business.Serialization
{
    public class ChangeTrackingProcessingStatusDtlSerializer: Serializer, IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;

               

                    ChangeTrackingProcessingStatusDtlInfo objChangeTrackingProcessingStatusDtl = (from b in dbContext.ChangeTrackingProcessingStatusDtls                                                             
                                                                          join p in dbContext.IncomingChanges on b.IncomingChangeId equals p.IncomingChangeId    
                                                                          where b.ExceptionDtlID == ObjectValueId

                                                         select new ChangeTrackingProcessingStatusDtlInfo
                                                              {
                                                                  Status = b.Status,
                                                                  Message = b.Message,
                                                                  Stage = b.Stage,
                                                                  CreatedOn = b.CreatedOn,
                                                                  ExceptionType = b.ExceptionType,
                                                                  ExceptionSource = b.ExceptionSource,
                                                                  ExceptionStackTrace = b.ExceptionStackTrace,
                                                                  InnerException = b.InnerException,
                                                                  IncomingChangeId = b.IncomingChangeId,
                                                                  SyncCode=b.SyncCode,
                                                                  ChangeTrackingId=p.ChangeTrackingId,
                                                                  VenueName=p.StoreName
                                                              }).FirstOrDefault();
                    if (objChangeTrackingProcessingStatusDtl != null)
                    {
                        dataXML = CommonUtility.SerializeObject<ChangeTrackingProcessingStatusDtlInfo>(objChangeTrackingProcessingStatusDtl);
                    }
                    return dataXML;

                    //
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in ChangeTrackingProcessingStatusDtl Serialization.", e);
                outerException.Source = "Business.Serialization.ChangeTrackingProcessingStatusDtlSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return download;
            }
            set
            {
                download = value;
            }
        }
    }
}
