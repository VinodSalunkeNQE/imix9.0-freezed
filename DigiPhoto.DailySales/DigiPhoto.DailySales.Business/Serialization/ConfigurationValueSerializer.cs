﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;

namespace DigiPhoto.DailySales.Business.Serialization
{
    class ConfigurationValueSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var Configuration = dbContext.IMixConfigurationValues.Where(r => r.ConfigurationId == ObjectValueId).ToList();
                    if (Configuration != null)
                    {
                        List<ConfigurationValueInfo> configValue = new List<ConfigurationValueInfo>();

                        foreach (IMixConfigurationValue cv in Configuration)
                        {
                            
                            ConfigurationValueInfo CVI = new ConfigurationValueInfo();
                            var configMasterSyncode = (from c in dbContext.IMixConfigurationValues
                                                       join cvi in dbContext.ConfigurationMasters on c.ConfigurationMasterId equals cvi.ConfigurationMasterId
                                                       where c.ConfigurationMasterId == cv.ConfigurationMasterId
                                                       select new { myconfig = cvi }).FirstOrDefault();

                            CVI.IMIXConfigurationMasterId = cv.ConfigurationMasterId;
                            CVI.IMIXConfigurationValueId = cv.ConfigurationValueId;
                            if (cv.ConfigurationValue != "" && cv.ConfigurationValue != string.Empty)
                            {
                                if (configMasterSyncode.myconfig.ControlType == "checkbox")
                                {
                                    if(cv.ConfigurationValue=="1")
                                    {
                                        CVI.ConfigurationValue = "true";
                                    
                                    }
                                    
                                }
                                else

                                {

                                 CVI.ConfigurationValue = cv.ConfigurationValue;

                                }
                                
                            }
                            else
                            {
                                if (configMasterSyncode.myconfig.ControlType=="checkbox")
                                {

                                    CVI.ConfigurationValue = "false";
                                }
                                else

                                {
                                    CVI.ConfigurationValue = "0";
                                }
                               

                            }

                            if (cv.ConfigurationMasterId == 112)
                            {

                                int currcyid = Convert.ToInt32(CVI.ConfigurationValue);
                                var currencysyncode = dbContext.Currencies.Where(r => r.CurrencyId == currcyid).FirstOrDefault();
                                if (currencysyncode != null)
                                {
                                    CVI.currencySyncCode = currencysyncode.SyncCode;

                                }
                            }

                            if(cv.ConfigurationMasterId==73)
                            {
                                int productid = Convert.ToInt32(CVI.ConfigurationValue);
                                var productcode = dbContext.Products.Where(r => r.ProductId == productid).FirstOrDefault();
                                if (productcode != null)
                                {
                                    CVI.ProductSyncCode = productcode.SyncCode.ToString();
                                }


                            }
                            CVI.SyncCode = cv.SyncCode;
                            CVI.ConfigurationId = cv.ConfigurationId;
                            CVI.MasterSyncCode = configMasterSyncode.myconfig.SyncCode;
                            configValue.Add(CVI);

                        }


                        ConfigurationValueInfo CI = new ConfigurationValueInfo()
                        {
                            configuartionInfoList = configValue,

                        };

                        dataXML = CommonUtility.SerializeObject<ConfigurationValueInfo>(CI);

                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in configuration Serialization.", e);
                outerException.Source = "Business.Serialization.configuration";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
