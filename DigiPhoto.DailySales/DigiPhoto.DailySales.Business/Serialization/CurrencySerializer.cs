﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Business.Serialization
{
     class CurrencySerializer :Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var currency = dbContext.Currencies.Where(r => r.CurrencyId == ObjectValueId).FirstOrDefault();
                    if (currency != null)
                    {
                        CurrencyInfo currencyInfo = new CurrencyInfo()
                        {
                            CurrencyId = currency.CurrencyId,
                            Name = currency.Name,
                            CurrencyCode = currency.Code,
                            CurrencyIcon = currency.CurrencyIcon,
                            IsDefault = currency.IsDefault,
                            Rate = currency.Rate,
                            Symbol = currency.Symbol,
                            SyncCode=currency.SyncCode,
                            ModifiedDateTime = currency.ModifiedDateTime,
                            ModifiedBy=currency.ModifiedBy
                            
                            
                        };

                        dataXML = CommonUtility.SerializeObject<CurrencyInfo>(currencyInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Role Serialization.", e);
                outerException.Source = "Business.Serialization.RoleSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
