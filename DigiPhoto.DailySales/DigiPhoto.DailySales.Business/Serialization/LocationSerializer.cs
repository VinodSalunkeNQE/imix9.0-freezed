﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DailySales.Business.Serialization
{
    class LocationSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var location = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == ObjectValueId).FirstOrDefault();
                    if (location != null)
                    {
                        string SubStoreName = string.Empty;
                        string venueCode = string.Empty;
                        string countryCode = string.Empty;
                        string venueName = string.Empty;
                        string countryName = string.Empty;
                        string logicalsubstoreSyncCode = string.Empty;

                        if (location.Level == 5)
                        {
                            SubStoreName = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == location.ParentDigiMasterLocationId && r.Level == 4).FirstOrDefault().Name;
                            
                        }
                        if (location.Level == 4)
                        {
                            var venue = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == location.ParentDigiMasterLocationId && r.Level == 3).FirstOrDefault();
                            if (venue != null)
                            {
                                venueCode = venue.Code;
                                venueName = venue.Name;
                            }
                            //venueCode = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == location.ParentDigiMasterLocationId && r.Level == 3).FirstOrDefault().Code;
                            //venueName = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == location.ParentDigiMasterLocationId && r.Level == 3).FirstOrDefault().Name;
                            var country = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == venue.ParentDigiMasterLocationId && r.Level == 2).FirstOrDefault();
                            if (country != null)
                            {
                                countryCode = country.Code;
                                countryName = country.Name;
                            }

                            //countryCode = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == venue.ParentDigiMasterLocationId && r.Level == 2).FirstOrDefault().Code;
                            //countryName = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == venue.ParentDigiMasterLocationId && r.Level == 2).FirstOrDefault().Name;
                            var logicalsubstore = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == location.LogicalDigiMasterLocationId && r.Level == 4).FirstOrDefault();
                            if (logicalsubstore != null)
                            {
                                logicalsubstoreSyncCode = logicalsubstore.SyncCode;
                            }

                        }
                        if (location.Level != 5)
                        {
                            LocationInfo loc = new LocationInfo()
                            {
                                DigiMasterLocationId = location.DigiMasterLocationId,
                                Name = location.Name,
                                Description = location.Description,
                                Code = location.Code,
                                ParentDigiMasterLocationId = location.ParentDigiMasterLocationId,
                                Level = location.Level,
                                DisplayOrder = location.DisplayOrder,
                                IsActive = location.IsActive,
                                SyncCode = location.SyncCode,
                                IsLogicalSubStore = location.IsLogicalSubStore,
                                LogicalSubStoreDetails = new SubStoreInfo
                                {
                                    SyncCode = logicalsubstoreSyncCode
                                },
                                ParentLocation = new LocationInfo
                                {
                                    Code = venueCode,
                                    Name = venueName,
                                    ParentLocation = new LocationInfo
                                    {
                                        Code = countryCode,
                                        Name = countryName
                                    }
                                }
                            };
                            dataXML = CommonUtility.SerializeObject<LocationInfo>(loc);
                        }
                        else
                        {
                            LocationInfo loc = new LocationInfo()
                            {
                                DigiMasterLocationId = location.DigiMasterLocationId,
                                Name = location.Name,
                                Description = location.Description,
                                Code = location.Code,
                                ParentDigiMasterLocationId = location.ParentDigiMasterLocationId,
                                Level = location.Level,
                                DisplayOrder = location.DisplayOrder,
                                IsActive = location.IsActive,
                                SyncCode = location.SyncCode,
                                IsLogicalSubStore = location.IsLogicalSubStore,
                                LogicalSubStoreDetails = new SubStoreInfo
                                {
                                    SyncCode = logicalsubstoreSyncCode
                                },
                                ParentLocation = new LocationInfo
                                {
                                    
                                    Name = SubStoreName
                                   
                                }
                            };
                            dataXML = CommonUtility.SerializeObject<LocationInfo>(loc);

                        }
                       
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Location Serialization.", e);
                outerException.Source = "Business.Serialization.Location";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
