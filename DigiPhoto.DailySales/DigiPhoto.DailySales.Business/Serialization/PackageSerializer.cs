﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System.Collections;
using System.Data;

namespace DigiPhoto.DailySales.Business.Serialization
{
    class PackageSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;

                    var package = (from p in dbContext.Packages
                                  //join OA in dbContext.ObjectAuthorizations on p.PackageId equals OA.ObjectValueId
                                   from OA in dbContext.ObjectAuthorizations
                                   where p.PackageId == ObjectValueId
                                   select new { mypackage = p }).FirstOrDefault();//, myproductpackage = PP
                    //  var product = dbContext.Products join .Where(r => r.ProductId == ObjectValueId).FirstOrDefault();



                    //var package = (from p in dbContext.Packages
                    //         join OAP in dbContext.ObjectAuthorizations on p.PackageId equals OAP.ObjectValueId
                    //         into g
                    //         from result in g.DefaultIfEmpty()
                    //         where result.ApplicationObjectId == 5
                    //               select new { mypackage = p, location = result }).FirstOrDefault();
                     string locationsynccode = string.Empty;
                    var Location = (from OA in dbContext.ObjectAuthorizations
                                     join L in dbContext.DigiMasterLocations on OA.DigiMasterLocationId equals L.DigiMasterLocationId
                                     where OA.ApplicationObjectId == 5 && OA.ObjectValueId == package.mypackage.PackageId
                                          && L.Level == 4
                                     select new { myLocation = L}
                                         ).FirstOrDefault();
                    if(Location != null)
                    {
                        locationsynccode = Location.myLocation.SyncCode;
                    }

                   
                    if (package != null)
                    {

                        var packageproduct = dbContext.PackageProducts.Where(p => p.PackageId == package.mypackage.PackageId).ToList();
                        List<PackageProductInfo> pps = new List<PackageProductInfo>();

                        foreach(PackageProduct pp in packageproduct)
                        {
                            PackageProductInfo ppi = new PackageProductInfo();
                            ppi.PackageId = pp.PackageId;
                            ppi.ProductId = pp.ProductId;
                            ppi.ProductMaxImage = pp.ProductMaxImage;
                            ppi.ProductQuantity = pp.ProductQuantity;
                            ppi.ProductSyncCode = dbContext.Products.Where(p => p.ProductId == pp.ProductId).FirstOrDefault().SyncCode;
                            pps.Add(ppi);
                        }
                      
                        
                        PackageInfo packageinfo = new PackageInfo()
                        {
                            PackageId = package.mypackage.PackageId,
                            Name = package.mypackage.Name,
                            Description = package.mypackage.Description,
                            Code = package.mypackage.Code,
                            IsActive = package.mypackage.IsActive,
                            IsPersonalizedAR= package.mypackage.IsPersonalizedAR,
                            IsDiscountApplied = package.mypackage.IsDiscountApplied,
                            Price = package.mypackage.Price,
                            CreatedDateTime = package.mypackage.CreatedDateTime,
                            SyncCode = package.mypackage.SyncCode,
                            UserSyncCode=dbContext.Users.Where(p=>p.UserId==package.mypackage.CreatedBy).FirstOrDefault().SyncCode,
                            IsPackage = true,

                            packageProductInfoList = pps,
                            LocationSyncCode = locationsynccode,
                           

                            //////this data is to be discussed with Anand sir////////////////////////////////

                            // ProductNumber = package.mypackage.ProductNumber,
                            // ImagePath = package.myproductpackage.ImagePath,                           
                            // IsBundled = package.mypackage.IsBundled,
                            //  IsAccessory = package.mypackage.IsAccessory,  
                            //  MaximumQuantity = package.mypackage.MaximumQuantity,                         
                            //   IsPrimary = package.mypackage.IsPrimary,

                            //this Is to be used for ProductPricing

                            // CurrencySyncCode = dbContext.Currencies.Where(c => c.CurrencyId == package.mypackage.CurrencyID).FirstOrDefault().SyncCode,
                           //  UserSyncCode = dbContext.Users.Where(u => u.UserId == package.mypackage.CreatedBy).FirstOrDefault().SyncCode,
                            //LocationSyncCode = dbContext.DigiMasterLocations.Where(d => d.DigiMasterLocationId == package.DigiMasterLocationId).FirstOrDefault().SyncCode




                        };

                        dataXML = CommonUtility.SerializeObject<PackageInfo>(packageinfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Product Serialization.", e);
                outerException.Source = "Business.Serialization.ProductSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
