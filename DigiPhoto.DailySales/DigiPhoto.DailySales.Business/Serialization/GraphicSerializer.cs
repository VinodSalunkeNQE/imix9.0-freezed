﻿using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.DailySales.Model;
using DigiPhoto.DailySales.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DailySales.Business.Serialization
{
    public class GraphicSerializer : Serializer, IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var graphic = dbContext.Graphics.Where(g => g.GraphicsId == ObjectValueId).FirstOrDefault();
                    if (graphic != null)
                    {
                        GraphicInfo graphicInfo = new GraphicInfo()
                        {
                            GraphicsId = graphic.GraphicsId,
                            Name = graphic.Name,
                            DisplayName = graphic.DisplayName,
                            IsActive = graphic.IsActive,
                            SyncCode = graphic.SyncCode,
                        };

                        graphicInfo.CreatedDate = DateTime.Now;
                        graphicInfo.CreatedBy = graphic.CreatedBy;
                        if (graphic.ModifiedBy.HasValue)
                            graphicInfo.ModifiedBy = graphic.ModifiedBy.Value;
                        if (graphic.ModifiedDateTime.HasValue)
                            graphicInfo.ModifiedDate = graphic.ModifiedDateTime.Value;

                        dataXML = CommonUtility.SerializeObject<GraphicInfo>(graphicInfo);
                        //If this class implements IDownloadable interface, so populate object of DownloadInfo
                        string hostingWebPath = ConfigurationManager.AppSettings["DigiphotoiMixWebHostingPath"];
                        string graphicFilePhysicalPath = Path.Combine(hostingWebPath, "Images//Graphics", graphic.Name);

                        if (File.Exists(graphicFilePhysicalPath))
                        {
                            download = new DownloadInfo()
                            {
                                DownloadFilePath = graphicFilePhysicalPath
                            };
                        }
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Graphic Serialization.", e);
                outerException.Source = "Business.Serialization.GraphicSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return download;
            }
            set
            {
                download = value;
            }
        }
    }
}
