﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DigiPhoto.DailySales.Utility
{

    public enum ApplicationObjectEnum
    {
        Border = 1,
        Background = 2,
        Graphic = 3,
        Product = 4,
        Package = 5,
        Discount = 6,
        Camera = 7,
        Currency = 8,
        ConfigurationValue = 9,
        Version = 10,
        Role = 11,
        User = 12,
        Location = 13,
        Order = 14,
        Refund = 15,
        Activity = 16,
        Cloudinary = 18,
        VersionUpdate = 19,
        ImixPosDetail = 20,
        ChangeTrackingExceptionDtl = 21,
        OpeningFormDetails = 22,
        ClosingFormDetails = 23,
        CurrencyProfile = 24,
        Scene = 25,
        ChangeTrackingProcessingStatusDtl = 26,
    }
    public enum SyncProcessingStatus
    {
        NotProcessed = 0,
        Processed = 1,
        Error = -1
    }
    #region Sync Service Enhancement - Ashirwad
    public enum SyncStatus
    {
        NotSynced = 0,
        Synced = 1,
        Error = -1,
        Invalid = -2,
        Inprogress = 2,
        UploadedDataOnCentralServerandCloudinary = 3,
        UploadedDataOnCentralServerButFailedonCloudinary = -3,
        ProcessedDataOnIMS = 4,
        FailedToProcessDataOnIMS = -4,
        
        UploadedDataOnCentralServerandCancel = 5,
        PartillyFailedUploading = -5,

        UploadedGraphicsToCloud =6,
        FailedUploadedGraphicsToCloud = -6,

        InProgressSavingIncomingChangeUploadedDataOn = 7,
        InProgressCompressingImages = 8,
        InProgressUploadPhotosCloudinary = 9,
        ErrorSavingIncomingChange = -7,
        FailedImageNotFound = -8,
        FailedUploadPhotosCloudinary = -9,

        FailedCompressingImages = -10,

        NotProcessedinIMS=11,
        ProcessedinIMS=12,
        HWupdatedinIMS= 13,
        HWnotupdatedinIMS=14,
        CodeisClaimable=15


    }
    #endregion
    public enum IMIXConfigurationValues
    {
        IsBarcodeActive = 1,
        DefaultMappingCode = 2,
        ScanType = 3,
        LostImgTimeGap = 4,
        OnlineImageResize = 5,
        StorageMediaImageResize = 6
    }
    public enum LocationType
    {
        Global = 1,
        Country = 2,
        Store = 3,
        SubStore = 4,
        Location = 5,
    }

    public enum iMixConfigurationMasterId
    {
        DgServiceURLReport = 228,
        //new arch start
        IsNewArchitectureEnable = 220,
        DgServicePullURL = 219,
        //new arch end
        IsSyncedEnabled = 69,
        SyncServiceInterval = 70,
        SyncDataDelay = 71,
        IsEnablePartialEditedImage = 182,
        SyncImageType = 194,

        #region Resync Config
        IsResyncActive = 195,
        InitialResyncDateTime = 196,
        ResyncIntervalCount = 197,
        ResyncIntervalType = 198,
        ResyncMaxRetryCount = 199
        #endregion
    }

    public enum ChangeTrackingProcessingStatus
    {
        ProcessedDataOnCentralServer=4,
        FailedToProcessDataOnCentralServer=-4,
        FailedtoProcessonPartnerDB=-5,
        Success=1,
    }

    public enum ChangeTrackingProcessingError
    {
        UserMissing = 1,
        ProductMissing = 2,
        PackageMissing = 3,
        SiteMissing = 4,
        LocationMissing = 5,
        CurrencyMissing = 6,       
    }
    public enum ChangeTrackingProcessingStages
    {
        CentralServer = 1,
        CentralServerToPartner = 2,       
    }

}
