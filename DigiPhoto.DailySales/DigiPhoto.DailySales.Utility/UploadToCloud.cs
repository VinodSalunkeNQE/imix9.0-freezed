﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DigiPhoto.DailySales.Model;
using System.IO;
//using DigiPhoto.DailySales.ClientDataAccess.DataModels;
//using DigiPhoto.DailySales.ClientDataProcessor.Controller;
using DigiPhoto.DailySales.Utility;
using log4net;


namespace DigiPhoto.DailySales.Utility
{
    public static class UploadToCloud
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static string cloudName = string.Empty;
        static string cloudApiKey = string.Empty;
        static string cloudApiSecret = string.Empty;
        static Account _account = null;
        static string cloundApiBaseAddress = string.Empty;
        static Int32 CloudinaryImagesBufferSize, CloudinaryVideosBufferSize = 0;
        static UploadToCloud()
        {
            string cloudName = ConfigurationManager.AppSettings["CloudName"].ToString();
            string cloudApiKey = ConfigurationManager.AppSettings["ApiKey"].ToString();
            string cloudApiSecret = ConfigurationManager.AppSettings["ApiSecret"].ToString();

            _account = new Account
                        (
                            cloudName,
                            cloudApiKey,
                            cloudApiSecret
                        );
            cloundApiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"].ToString();

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryImagesBufferSize"))
            {
                CloudinaryImagesBufferSize = Convert.ToInt32(ConfigurationManager.AppSettings["CloudinaryImagesBufferSize"].ToString());
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryVideosBufferSize"))
            {
                CloudinaryVideosBufferSize = Convert.ToInt32(ConfigurationManager.AppSettings["CloudinaryVideosBufferSize"].ToString());
            }

            if (String.IsNullOrEmpty(cloudName))
                throw new ArgumentNullException("Cloud name must be specified in configuration (app.config)!");

            if (String.IsNullOrEmpty(cloudApiKey))
                throw new ArgumentNullException("Cloudinary API key must be specified in configuration (app.config)!");

            if (String.IsNullOrEmpty(cloudApiSecret))
                throw new ArgumentNullException("Cloudinary API secret must be specified in test configuration (app.config)!");
        }

        public static CloudinaryUploadInfo UploadFile(UploadFileInfo _uploadFileInfo, out string errorPhotoId)
        {
            CloudinaryUploadInfo objCloudinaryUploadInfo = new CloudinaryUploadInfo();
            try
            {
                string CloudinaryOrderImagesPath = "OrderImages";
                errorPhotoId = string.Empty;
                ImageUploadResult uploadResult;
                VideoUploadResult uploadResultVideo;
                // CloudinaryUploadInfo objCloudinaryUploadInfo = new CloudinaryUploadInfo();
                if (!File.Exists(_uploadFileInfo.ImagePath))
                {
                    errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                    objCloudinaryUploadInfo.CloudinaryStatusID = 4;
                    objCloudinaryUploadInfo.ErrorMessage = "Source image was missing";
                }
                else
                {
                    Cloudinary objCloudinary = new Cloudinary(_account);
                    objCloudinary.Api.ApiBaseAddress = cloundApiBaseAddress;
                    if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryOrderImagesPath"))
                    {
                        CloudinaryOrderImagesPath = ConfigurationManager.AppSettings["CloudinaryOrderImagesPath"].ToString();
                    }

                    if (_uploadFileInfo.MediaType == 1)
                    {
                        var uploadparameters = new ImageUploadParams
                        {
                            File = new FileDescription(_uploadFileInfo.ImagePath),
                            PublicId = Path.Combine(CloudinaryOrderImagesPath, Path.Combine(_uploadFileInfo.TargetDirectory, Path.GetFileNameWithoutExtension(_uploadFileInfo.Title))).Replace(@"\", @"/"),
                            Invalidate = true
                        };
                        if (CloudinaryImagesBufferSize == 0)
                            uploadResult = objCloudinary.Upload(uploadparameters);
                        else
                            uploadResult = objCloudinary.UploadLarge(uploadparameters, CloudinaryImagesBufferSize);
                        if (uploadResult.Error != null)
                        {
                            errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                            objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                            objCloudinaryUploadInfo.ErrorMessage = uploadResult.Error.Message;

                        }
                        else
                        {
                            objCloudinaryUploadInfo.CloudinaryStatusID = 1;
                            objCloudinaryUploadInfo.ErrorMessage = "Uploaded successfully";
                        }
                        objCloudinaryUploadInfo.CloudinaryPublicID = uploadResult.PublicId;
                    }
                    else
                    {
                        var uploadparameters = new VideoUploadParams
                        {
                            File = new FileDescription(_uploadFileInfo.ImagePath),
                            PublicId = Path.Combine(CloudinaryOrderImagesPath, Path.Combine(_uploadFileInfo.TargetDirectory, Path.GetFileNameWithoutExtension(_uploadFileInfo.Title))).Replace(@"\", @"/"),
                            Invalidate = true
                        };
                        //uploadResultVideo = objCloudinary.Upload(uploadparameters, "video");
                        if (CloudinaryVideosBufferSize == 0)
                            uploadResultVideo = objCloudinary.Upload(uploadparameters);
                        else
                            uploadResultVideo = objCloudinary.UploadLarge(uploadparameters, CloudinaryVideosBufferSize);
                        if (uploadResultVideo.Error != null)
                        {
                            errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                            objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                            objCloudinaryUploadInfo.ErrorMessage = uploadResultVideo.Error.Message;
                        }
                        else
                        {
                            objCloudinaryUploadInfo.CloudinaryStatusID = 1;
                            objCloudinaryUploadInfo.ErrorMessage = "Uploaded successfully";
                        }
                        objCloudinaryUploadInfo.CloudinaryPublicID = uploadResultVideo.PublicId;
                    }
                }
            }
            catch (Exception ex)
            {
                errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                objCloudinaryUploadInfo.ErrorMessage = "Push:UnKnown Message : " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source;
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            ImageAttribute imageAttribute = ImageHelper.GetImageHeightWidth(_uploadFileInfo.ImagePath);

            System.Drawing.Size size = new System.Drawing.Size(_uploadFileInfo.ImageDefaultWidth, _uploadFileInfo.ImageDefaultHeight);

            imageAttribute.Dimension = ImageHelper.ScaleImage(imageAttribute.Width, imageAttribute.Height, size);

            objCloudinaryUploadInfo.Width = imageAttribute.Width;
            objCloudinaryUploadInfo.Height = imageAttribute.Height;
            objCloudinaryUploadInfo.ThumbnailDimension = imageAttribute.Dimension;

            return objCloudinaryUploadInfo;
        }

    }
}