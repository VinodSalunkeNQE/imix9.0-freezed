﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace DigiWhatsAppShareService
{
    static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //WhatsAppShareService obj = new WhatsAppShareService();
            //obj.GetWhatsAppPendingOrders();
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Whatsapp Service Started");

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WhatsAppShareService()
            };
            ServiceBase.Run(ServicesToRun);
        }

    }
    
}
