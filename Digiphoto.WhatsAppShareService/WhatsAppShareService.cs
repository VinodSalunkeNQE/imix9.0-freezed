﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using DigiPhoto.DataLayer;
using System.Globalization;
using DigiPhoto;
using System.Reflection;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper.Common;
using log4net;
using System.Net;
using System.Web.Script.Serialization;


namespace DigiWhatsAppShareService
{
    public partial class WhatsAppShareService : ServiceBase
    {

        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer timer;

        private string ApiUrl;
        public WhatsAppShareService()
        {
            InitializeComponent();
        }

        public void onDebug()
        {
            OnStart(null);
        }

        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }



        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {

                    ApiUrl = ConfigurationManager.AppSettings["WhatsAppApiURL"];
                    this.timer = new System.Timers.Timer();
                    this.timer.Interval = 1000 * 100;
                    this.timer.AutoReset = true;
                    this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    this.timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }

                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);

            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                //ExitCode = 13816;
                this.Stop();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                
                this.timer.Stop();
                // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // svcPosinfoBusiness.ServiceStart(false);
                GetWhatsAppPendingOrders();
                GetWhatsAppOrderStatus();

            }
            catch (Exception ex)
            {

                log.Error("timer_Elapsed:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.timer.Start();
            }
            finally
            {
                this.timer.Start();
            }
        }

        public void GetWhatsAppOrderStatus()
        {
            try
            {
                log.Info("---Started WhatsApp Get Status Method---");
                WhatsAppBusiness whatsApp = new WhatsAppBusiness();
                WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();


                List<WhatsAppSettingsTracking> wtsAppOrders = whatsApp.GetWhatsAppExactStatus();

                string temporder = "";
                foreach (WhatsAppSettingsTracking eachOrder in wtsAppOrders)
                {
                    if (temporder.Equals(eachOrder.Order_Number))
                    {
                        continue;
                    }

                    temporder = eachOrder.Order_Number;
                  
                    int status = 1;
                    //string mainurl = System.Web.HttpUtility.UrlEncode(eachOrder.WhtsApp_URL_SourcePath);

                    //var client = new RestClient("https://panel.apiwha.com/send_message.php?apikey=" + wtsAppData.APIKey + "&number=" + eachOrder.Guest_MobileNumber.Trim() + "&text=" + mainurl);

                    //var request = new RestRequest(Method.GET);
                    //IRestResponse response = client.Execute(request);

                    //log.Info("Completed for WhatsAPP  ID:" + eachOrder.WhatsAppId + " Status=" + response.ResponseStatus);



                    log.Info("DB will updated for WhatsAPP  ID:" + eachOrder.WhatsAppId);
                    string response = "";

                    Uri uri = new Uri(ApiUrl + "get_messages.php?apikey=" + System.Web.HttpUtility.UrlEncode(wtsAppData.APIKey) + "&type=OUT&number=" + System.Web.HttpUtility.UrlEncode(eachOrder.Guest_MobileNumber.Trim())
                        + "&custom_data=" + System.Web.HttpUtility.UrlEncode(eachOrder.Order_Number));

                    System.Net.HttpWebRequest requestFile = (HttpWebRequest)WebRequest.Create(uri);

                    requestFile.ContentType = "application/json";

                    HttpWebResponse webResp = requestFile.GetResponse() as HttpWebResponse;

                    if (requestFile.HaveResponse)
                    {
                        if (webResp.StatusCode == HttpStatusCode.OK || webResp.StatusCode == HttpStatusCode.Accepted)
                        {
                            StreamReader respReader = new StreamReader(webResp.GetResponseStream(), Encoding.GetEncoding("utf-8"));

                            response = respReader.ReadToEnd(); // This is the APIWHA response		

                            //var result = MessageBox.Show(response, "Notice",
                            //             MessageBoxButtons.OK,
                            //             MessageBoxIcon.Information);
                        }

                        var serializer = new JavaScriptSerializer();

                        var persons = serializer.Deserialize<List<ApiWhaGetResponse>>(response);
                        bool res = false;
                        foreach (var item in persons)
                        {
                            try
                            {
                                log.Info("process_date:" + item.process_date + " and failed_date: " + item.failed_date);
                                if (item.process_date != null || item.process_date == string.Empty)
                                {
                                    if (item.process_date.Length > 5)
                                    {
                                        status = 3;

                                    }
                                }
                                if (item.failed_date != null || item.failed_date == string.Empty)
                                {
                                    if (item.failed_date.Length > 5)
                                    {
                                        status = -106;

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                status = 1;
                                log.Error("GetWhatsAppOrderStatus=" + eachOrder.WhtsApp_URL_SourcePath + " " + ex.Message);
                            }


                            res=whatsApp.UpdateWtsAppOrderStatus(item.text.Trim(), status, item.creation_date, item.process_date, item.failed_date, item.custom_data);
                        }

                        log.Info(" StatusDB updated for WhatsAPP  ID:" + eachOrder.WhatsAppId + " & Status=" + status +"& res="+ res);

                    }



                    webResp = null;

                }



                log.Info("---END WhatsApp Get Status Method---");

            }
            catch (Exception ex)
            {

                log.Error("GetWhatsAppOrderStatus=" + ex.Message);
            }
        }
        public void GetWhatsAppPendingOrders()
        {
            try
            {
                int status = 0;
                log.Info("---Started Sending WhatsAppPendingOrders Method---");
                WhatsAppBusiness whatsApp = new WhatsAppBusiness();
                WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();

                List<WhatsAppSettingsTracking> wtsAppOrders = whatsApp.GetWhatsAppOrders();

                foreach (WhatsAppSettingsTracking eachOrder in wtsAppOrders)
                {
                    if (File.Exists(eachOrder.WhtsApp_Image_SourcePath))
                    {
                        whatsApp.UpdateWtsAppOrder(eachOrder.WhatsAppId, 2);


                        log.Info("DB will updated for WhatsAPP  ID:" + eachOrder.WhatsAppId);
                        string response = "";

                        Uri uri = new Uri(ApiUrl + "send_message.php?apikey=" + System.Web.HttpUtility.UrlEncode(wtsAppData.APIKey) + "&number=" + System.Web.HttpUtility.UrlEncode(eachOrder.Guest_MobileNumber.Trim())
                            + "&text=" + System.Web.HttpUtility.UrlEncode(eachOrder.WhtsApp_URL_SourcePath) + "&custom_data=" + eachOrder.Order_Number);

                        System.Net.HttpWebRequest requestFile = (HttpWebRequest)WebRequest.Create(uri);

                        requestFile.ContentType = ".application/json";

                        HttpWebResponse webResp = requestFile.GetResponse() as HttpWebResponse;

                        if (requestFile.HaveResponse)
                        {
                            if (webResp.StatusCode == HttpStatusCode.OK || webResp.StatusCode == HttpStatusCode.Accepted)
                            {
                                StreamReader respReader = new StreamReader(webResp.GetResponseStream(), Encoding.GetEncoding("utf-8"));

                                response = respReader.ReadToEnd(); // This is the APIWHA response		

                                //var result = MessageBox.Show(response, "Notice",
                                //             MessageBoxButtons.OK,
                                //             MessageBoxIcon.Information);
                            }

                            var serializer = new JavaScriptSerializer();

                            var persons = serializer.Deserialize<ApiWhaSendResponse>(response);
                            status = Convert.ToInt32(persons.result_code);

                        }
                        else
                        {
                            status = -102;
                        }


                        webResp = null;

                        if (status == 0)
                        {
                            status = 1;
                        }


                    }
                    else
                    {
                        //Image not found
                        status = -101;

                    }

                    whatsApp.UpdateWtsAppOrder(eachOrder.WhatsAppId, status);

                    log.Info("DB updated for WhatsAPP  ID:" + eachOrder.WhatsAppId + " & Status=" + status);

                }

                log.Info("---END Sending WhatsAppPendingOrders Method---");
            }
            catch (Exception ex)
            {

                log.Error("Error:GetWthsOrdersStatus-" + ex.Message);
            }


        }

    }

    public class ApiWhaSendResponse
    {
        public string success { get; set; }
        public string description { get; set; }
        public string result_code { get; set; }


    }


    public class ApiWhaGetResponse
    {
        public string id { get; set; }
        public string number { get; set; }
        public string from_number { get; set; }
        public string to_number { get; set; }
        public string type { get; set; }
        public string text { get; set; }
        public string creation_date { get; set; }
        public string process_date { get; set; }
        public string failed_date { get; set; }
        public string custom_data { get; set; }

    }

}
