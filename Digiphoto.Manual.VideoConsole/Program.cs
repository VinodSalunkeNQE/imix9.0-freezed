﻿using DigiPhoto.Cache.DataCache;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digiphoto.ManualDownload.Console
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
#if DEBUG
            ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.ManualDownload.video console started");
#endif
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
            DataCacheFactory.Register();

            //while (true)
            //{
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            (new ManualDownLoad()).StartMaualDownloadProcess();

            //System.Threading.Thread.Sleep(1000);
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop("Total" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            ErrorHandler.ErrorHandler.LogFileWrite(e.ExceptionObject.ToString());
            Environment.Exit(1);
        }
    }
}
