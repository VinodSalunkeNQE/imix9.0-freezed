﻿using Digiphoto.ManualDownload.Console.IBusiness;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digiphoto.ManualDownload.Console
{
    public class ManualDownloadBusiness : IManualDownLoad
    {


        public List<SemiOrderSettings> GetSemiSettings()
        {
            return (new SemiOrderBusiness()).GetLstSemiOrderSettings(0);
        }
        public ManualDownloadInfo GetManualDownloadInfo()
        {
            throw new NotImplementedException();
        }

        public bool SaveManualDownloadInfo()
        {
            throw new NotImplementedException();
        }

        public bool CheckManualDownloadInfo()
        {
            throw new NotImplementedException();
        }

        public bool CheckResetDPIRequired(int photographerId)
        {
            throw new NotImplementedException();
        }


        public bool UpdateData(ManualDownloadInfo objInfo)
        {
            throw new NotImplementedException();
        }
    }
}
