﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digiphoto.ManualDownload.Console
{
    public class ManualDownloadInfo
    {

        public long PhotoNo{get;set;}
        public int PhotographerId{get;set;}
        public int LocationId {get;set;}
        public int SubStoreId{get;set;}
        public int CharacterId { get; set; }
        public bool IsDownloadProcessed {get;set;}
    }
}
