﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.LogEnvelop.Logger;
using ExifLib;
using FrameworkHelper;
using LevDan.Exif;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;
using System.Configuration;
using System.Collections;

using System.Windows.Threading;
using System.Xml;
using System.Diagnostics;
using FrameworkHelper.Common;
using DigiPhoto.IMIX.Model.Base;

namespace Digiphoto.ManualDownload.Console
{
    public class ManualDownLoad
    {

        //private ExceptionLogger _logger = null;

        private static volatile object _object = new object();
        PhotoBusiness photoBusiness = new PhotoBusiness();
        public ManualDownLoad()
        {
            CommonUtility.CleanFolder(Environment.CurrentDirectory, new string[] { "DownloadVideo" });
        }

        #region Fields and members
        bool RunManualDownloadLocWise = false;
        List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = new List<ManualDownloadPosLocationAssociation>();
        private int _SubStoreId = -1;
        private string _SubStoreName = String.Empty;
        private List<SemiOrderSettings> _lstSemiSettings = null;
        private string _defaultBrightness = string.Empty;
        private string _defaultContrast = string.Empty;
        private string _thumbnailFilePathDate;
        private string _bigThumbnailFilePathDate;

        private int? _rfidScanType = null;
        private bool _isRFIDEnabled = false;
        private bool _isVideoProcessed = true;
        string[] mediaExtensions = new[] { ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg", ".ffmpeg" };

        /// <summary>
        /// The photo layer
        /// </summary>

        private string _networkPath = string.Empty;//@"\\192.168.29.179\DigiImages\ManualDownload";
        private string _networkBackupPath = string.Empty;//@"\\192.168.29.179\DigiImages\ManualDownlaodBackup";

        private string _filePathDate = string.Empty;
        private string _filePath = string.Empty;

        //private ManualDownloadInfo _objInfo = null;
        private ConfigurationInfo _configInfo = null;
        private Nullable<bool> _IsAutoRotate;
        public Hashtable htVidL = new Hashtable();
        VideoSceneBusiness VBusiness = new VideoSceneBusiness();
        //private const string _metaDataFormat = "<image Dimensions={0} CameraManufacture={1} HorizontalResolution={2} VerticalResolution={3} CameraModel={4} ISO-SpeedRating={5} DateTaken={6} ExposureMode={7} Sharpness={8} Orientation={9} GPSLatitude={10} GPSLongitude={11}></image>";
        //private const string _defaultEffects = "<image brightness = '{0}' contrast = '{1}' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
        //private const string _layeringdata = "<photo zoomfactor='{0}' border='{1}' bg= '{2}' canvasleft='{3}' canvastop='{4}' scalecentrex='{5}' scalecentrey='{6}'>{7}</photo>";
        //private const string FileExt = ".jpg";

        #endregion

        void SetSubStoreId()
        {

            string pathtosave = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite("Path-> " + pathtosave + "\\ss.dat");
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;

                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        string subID = CryptorEngine.Decrypt(line, true);
                        if (!String.IsNullOrEmpty(subID))
                        {
                            _SubStoreId = (subID.Split(',')[0]).ToInt32();
                            _SubStoreName = (new StoreSubStoreDataBusniess()).GetLoginUserDefaultSubstores(_SubStoreId).FirstOrDefault().DG_SubStore_Name;
                        }
                        else
                            ErrorHandler.ErrorHandler.LogFileWrite("Invalid data in ss.dat file.");
                    }
                }
            }
            catch (Exception ex)
            {
                _SubStoreId = -1;
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }

        }
        public void StartMaualDownloadProcess()
        {
            try
            {

                lock (_object)
                {
                    SetSubStoreId();
                    SetNewConfigValues();
                    if (GetManualDownloadLocationWisesettings())
                        lstPosLocationAssociation = GetPosLocationAssociation();

                    CollectFilesForProcessing();
                }

                //_logger.LogException("Collecting files is completed", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //ErrorHandler.ErrorHandler.LogFileWrite("Collecting files is completed");


            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                SetSubStoreId();
            }
        }

        private bool GetManualDownloadLocationWisesettings()
        {
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();

            return RunManualDownloadLocWise = store.RunManualDownloadLocationWise;
        }
        private List<ManualDownloadPosLocationAssociation> GetPosLocationAssociation()
        {
            List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = new List<ManualDownloadPosLocationAssociation>();
            lstPosLocationAssociation = new ConfigBusiness().GetPosLocationMapping(Environment.MachineName);

            return lstPosLocationAssociation;
        }

        private bool SetNetWorkAndFilePath(int subStoreId)
        {
            _configInfo = (new ConfigBusiness()).GetConfigurationData(subStoreId);

            if (_configInfo == null)
            {
                //_logger.LogException("Invalid sub store id.", DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite("Invalid sub store id.");
                return false;
            }

            _filePath = _configInfo.DG_Hot_Folder_Path;
            _filePathDate = Path.Combine(_filePath, "Videos", DateTime.Now.ToString("yyyyMMdd"));

            _thumbnailFilePathDate = Path.Combine(_filePath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
            _bigThumbnailFilePathDate = Path.Combine(_filePath, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));

            if (!Directory.Exists(_filePathDate))
                Directory.CreateDirectory(_filePathDate);

            if (!Directory.Exists(_thumbnailFilePathDate))
                Directory.CreateDirectory(_thumbnailFilePathDate);

            if (!Directory.Exists(_bigThumbnailFilePathDate))
                Directory.CreateDirectory(_bigThumbnailFilePathDate);

            //_BorderFolder = _configInfo.DG_Frame_Path;


            //_networkPath = _configInfo.DG_Hot_Folder_Path + "\\ManualDownload";
            //_networkBackupPath = _configInfo.DG_Hot_Folder_Path + "\\ManualDownloadBackup";
            _IsAutoRotate = _configInfo.DG_IsAutoRotate;

            return true;
        }

        private void GetRfidConfigInfo(int subStoreId)
        {
            var configList = (new ConfigBusiness()).GetNewConfigValues(subStoreId);
            if (configList == null)
                return;
            // var configList = new ConfigBusiness().GetNewConfigValues(subStoreId);
            iMIXConfigurationInfo configValue = configList.FirstOrDefault
                                    (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsAnonymousQrCodeEnabled);

            iMIXConfigurationInfo configValue3 = configList.FirstOrDefault
                                (o => o.IMIXConfigurationMasterId == (long)ConfigParams.RFIDScanType);

            iMIXConfigurationInfo configValue2 = configList.FirstOrDefault
                                            (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsEnabledRFID);

            iMIXConfigurationInfo configValue4 = configList.FirstOrDefault
                                               (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsEnableAutoVidProcessing);


            if (configValue2 != null && !string.IsNullOrEmpty(configValue2.ConfigurationValue))
            {
                _isRFIDEnabled = Convert.ToBoolean(configValue2.ConfigurationValue);
            }
            else
            {
                _isRFIDEnabled = false;
            }

            if (_isRFIDEnabled == true && configValue3 != null)
            {
                _rfidScanType = Convert.ToInt32(configValue3.ConfigurationValue);
            }
            else
                _rfidScanType = null;
            if (configValue4 != null && !string.IsNullOrEmpty(configValue4.ConfigurationValue))
            {
                if (Convert.ToBoolean(configValue4.ConfigurationValue) == true)
                    _isVideoProcessed = false;
            }
            //case (int)ConfigParams.IsAdvancedVideoEditActive:
            //        _IsAdvancedVideoEditActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
            //        break;

        }

        private void CollectFilesForProcessing()
        {
            if (_SubStoreId <= 0)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Invalid sub store id.");
                return;
            }

            ConfigurationInfo config = (new ConfigBusiness()).GetConfigurationData(_SubStoreId);
            //ErrorHandler.ErrorHandler.LogFileWrite(String.Format("Hotfolder path: {0} and sub store id {1} -->", config.DG_Hot_Folder_Path, _SubStoreId));

            _networkBackupPath = config.DG_Hot_Folder_Path + "ManualDownloadCorrupt";
            if (lstPosLocationAssociation.Count == 0)
            {
                // in this case service will work substore wise.
                _networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload", _SubStoreName);
                ProcessVideos(config);
            }

            else
            {
                //service will work location wise.
                foreach (string locationName in lstPosLocationAssociation.Select(x => x.LocationName))
                {
                    _networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload", _SubStoreName, locationName);
                    ProcessVideos(config);
                }
            }



        }

        public void ProcessVideos(ConfigurationInfo config)
        {

             //_networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload", _SubStoreName);
            _networkBackupPath = config.DG_Hot_Folder_Path + "ManualDownloadCorrupt";
            ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 275");
            //_networkPath = ConfigurationManager.AppSettings["NetworkPath"];
            //_networkBackupPath = ConfigurationManager.AppSettings["NetworkPathBackup"];

            DirectoryInfo info = new DirectoryInfo(_networkPath);
            //FileInfo[] files = info.GetFiles("*.jpg").ToArray();
            //To get video files in search
            if (info.Exists)
            {
                FileInfo[] files = info.EnumerateFiles().Where(f => mediaExtensions.Contains(f.Extension.ToLower())).ToArray();

                if (files == null || files.Count() <= 0)
                    return;


                var imageList = new List<MyImageClass>();
                var count = files.Count();//(files.Count() == 1) ? files.Count() + 1 : files.Count();

                foreach (var file in files)//.Take(count - 1))
                {
                    //var title = file.Name.Substring(file.Name.LastIndexOf("\\") + 1).Replace(FileExt, "").Replace(".jpeg", "");
                    var title = System.IO.Path.GetFileNameWithoutExtension(file.Name);

                    string[] infoCol = title.Split('_');
                    var imageClass = new MyImageClass
                    {
                        //Title = file.Name.Substring(file.Name.LastIndexOf("\\") + 1).Replace(FileExt, "").Replace(".jpeg", ""),
                        Title = title,
                        ImagePath = file.DirectoryName,
                        IsChecked = false,
                        PhotoNumber = Convert.ToInt64(infoCol[1]),
                        PhotoGrapherID = Convert.ToInt32(infoCol[2]),
                        FileExtension = Path.GetExtension(file.Name)
                    };

                    imageList.Add(imageClass);

                }

                imageList = imageList.OrderBy(o => o.PhotoGrapherID).ThenBy(p => p.PhotoNumber).ToList();
                ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 315");
                MyImagesList imgList = new MyImagesList();
                //_objInfo = new ManualDownloadInfo();
                _lstSemiSettings = (new ManualDownloadBusiness()).GetSemiSettings();
                ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 319");
                List<long> PhotoNumbers = new List<long>();
                foreach (var item in imageList)
                {
                    PhotoNumbers.Add(item.PhotoNumber);
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 325");
                List<ManualDisplayOrder> MDOList = new List<ManualDisplayOrder>();
                MDOList = photoBusiness.GetPhotosDisplayNumber(PhotoNumbers);
                PhotoNumbers.Clear();
                List<MyImagesList> ImagesList = new List<MyImagesList>();
                foreach (var item in imageList)
                {
                    foreach (var item1 in MDOList)
                    {
                        if (item.PhotoNumber == item1.PhotoNumber)
                        {
                            ImagesList.Add(new MyImagesList() { MyImageClass = item, DisplayOrder = item1.DisplayOrder });
                        }
                    }
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 340");
                //ErrorHandler.ErrorHandler.LogFileWrite("Start processing file.");
                //ProcessFile(imageList);
                ProcessFile(ImagesList);
                //ErrorHandler.ErrorHandler.LogFileWrite("Processing finised.");
                ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.Manual.VideoConsole  : ProcessVideos : 345");
            }
        }

        /// <summary>
        /// ProcessFile, pick the file from download folder and apply effects.
        /// </summary>
        /// <param name="imageList">List of MyImageClass</param>
        //private void ProcessFile(List<MyImageClass> imageList)
        //{

        //    if (imageList == null || imageList.Count == 0)
        //        return;

        //    //_logger.LogException("Start download flow", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
        //    //ErrorHandler.ErrorHandler.LogFileWrite("Start normal download process.");

        //    if (!Directory.Exists(_networkBackupPath))
        //    {
        //        DirectoryInfo di = new DirectoryInfo(_networkBackupPath);
        //        di.Create();
        //    }


        //    ManualDownloadInfo downloadInfo = new ManualDownloadInfo();// PopulateManualDownloadInfo(imageList[0]);
        //    NormalDownloadFlow(imageList, downloadInfo);

        //    //_logger.LogException("Completed download flow", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
        //    //ErrorHandler.ErrorHandler.LogFileWrite("Completed download flow.");

        //}


        private void ProcessFile(List<MyImagesList> imageList)
        {

            if (imageList == null || imageList.Count == 0)
                return;

            //_logger.LogException("Start download flow", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
            //ErrorHandler.ErrorHandler.LogFileWrite("Start normal download process.");

            if (!Directory.Exists(_networkBackupPath))
            {
                DirectoryInfo di = new DirectoryInfo(_networkBackupPath);
                di.Create();
            }

            foreach (var item in imageList)
            {
                var info = new List<MyImageClass>();
                info.Add(item.MyImageClass);
                ManualDownloadInfo downloadInfo = new ManualDownloadInfo();// PopulateManualDownloadInfo(imageList[0]);
                //NormalDownloadFlow(info, downloadInfo);
                NormalDownloadFlow(info, downloadInfo,item.DisplayOrder);
            }
        }


        /// <summary>
        /// PopulateManualDownloadInfo
        /// </summary>
        /// <param name="info"></param>
        private ManualDownloadInfo PopulateManualDownloadInfo(MyImageClass info)
        {
            string[] infoCol = info.Title.Split('_');
            //0-PhotoNo, 1 -PhotographerId, 2- SubstoreId, 3- LocationId
            ManualDownloadInfo objInfo = new ManualDownloadInfo();
            objInfo.PhotoNo = Convert.ToInt64(infoCol[1]);
            objInfo.PhotographerId = Convert.ToInt32(infoCol[2]);
            objInfo.SubStoreId = Convert.ToInt32(infoCol[3]);
            objInfo.LocationId = Convert.ToInt32(infoCol[4]);
            objInfo.CharacterId = Convert.ToInt32(infoCol[5]);

            if (SetNetWorkAndFilePath(objInfo.SubStoreId) == false)
                return null;

            GetRfidConfigInfo(objInfo.SubStoreId);

            return objInfo;

        }
        List<Watchersetting> watchersetting = new List<DigiPhoto.IMIX.Model.Watchersetting>();
        /// <summary>
        /// NormalDownloadFlow
        /// </summary>
        /// <param name="imagelist"></param>
        public void NormalDownloadFlow(List<MyImageClass> imagelist, ManualDownloadInfo objInfo)
        {

            //String s = String.Empty;
            //int i = 0;
            //List<MyImageClassList> lstMyImgClassList = new List<MyImageClassList>();

            //MyImageClassList myImgClassList = new MyImageClassList();

            foreach (var item in imagelist)
            {
                try
                {

                    FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + item.FileExtension);
                    ErrorHandler.ErrorHandler.LogFileWrite("Video comparess function call.");
                    objInfo = PopulateManualDownloadInfo(item);
                    if (objInfo == null)
                        continue;
                    //string vidExtension = System.IO.Path.GetExtension(item.Title.ToLower());
                    //string filename = _objDataLayer.ServerDateTime().Day.ToString() + _objDataLayer.ServerDateTime().Month.ToString() + PhotoNo.ToString() + "_" + PhotographerId.ToString();
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    string srcImageName = item.Title + item.FileExtension;
                    DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                    int vidCount = photoBiz.PhotoCountCurrentDate(RFID: objInfo.PhotoNo.ToString(), photographerID: objInfo.PhotographerId, mediaType: 2);
                    string filename = string.Empty;
                    if (vidCount > 0)
                    {
                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                       + "_" + objInfo.PhotographerId.ToString() + "(" + vidCount + ")" + item.FileExtension;
                    }
                    else
                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                       + "_" + objInfo.PhotographerId.ToString() + item.FileExtension;



                    ErrorHandler.ErrorHandler.LogFileWrite("Video comparess function call.   line number : 420");
                    //if (photoBiz.CheckPhotos(filename, objInfo.PhotographerId))
                    //{
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 423 video");
                    SetNewConfigLocationValues(objInfo.LocationId, objInfo.SubStoreId);
                    File.Copy(Path.Combine(item.ImagePath, srcImageName), Path.Combine(_filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension)), true);
                    // Create Minified Videos By Suraj....
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 427");
                    CreateMinifiedVideo(Path.Combine(_filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension)), _filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension));
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 429 video");
                    // End Changes .....
                    string srcThumb = Path.GetDirectoryName(_filePath) + "\\" + Path.GetFileName(item.Title) + ".jpg";
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 432 video");
                    //Extract thumbnail & resize
                    //FrameworkHelper.Common.MLHelpers mlHelper = new FrameworkHelper.Common.MLHelpers();
                    ////ThumbnailExtractor.ExtractThumbnailFromVideo(Path.Combine(item.ImagePath, srcImageName), 4, 4, Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg"), ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.MediaPlayerLicenseKey) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.MediaPlayerLicenseKey] : string.Empty, ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeUsername) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeUsername] : string.Empty, ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeEmailId) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeEmailId] : string.Empty);
                    //string FilePath = FrameworkHelper.Common.MLHelpers.ExtractThumbnail(Path.Combine(item.ImagePath, srcImageName));

                    //if (!string.IsNullOrEmpty(FilePath))
                    //    FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg"));
                    //Save Video Thumbnail.
                    // Added by Suraj .... for Thumbnil are not genrated properly ....
                    string filSavePath = Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg");
                    string minifidFilePath = Path.Combine(_thumbnailFilePathDate, "m_" + Path.GetFileNameWithoutExtension(filename) + ".jpg");
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 444");
                    //ThumbnailExtractor.ExtractThumbnailFromVideo(file.FullName, 4, 4, filSavePath, _VisioForgeLicenseKey, _VisioForgeUserName, _VisioForgeEmailID);
                    string FilePath = MLHelpers.ExtractThumbnail(Path.Combine(item.ImagePath, srcImageName));
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 447");
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, filSavePath);
                        File.Copy(filSavePath, minifidFilePath);
                    }
                    // End Changes ...
                    if (IsAutoVideoFrameExtEnabled)
                    {

                        List<double> framesec = ReadXml();
                        if (framesec != null || framesec.Count > 0)
                        {
                            List<string> FrameFilePath = FrameworkHelper.Common.MLHelpers.ExtractFramesManualDownload(framesec, item.Title, item.ImagePath, item.FileExtension, videoFrameCropRatio);

                            //Move File into download folder
                            foreach (string derivedFrame in FrameFilePath)
                            {
                                ResetLowDPI(derivedFrame, derivedFrame.Replace(".jpg", "1.jpg"));
                                string _filename = Path.GetFileName(derivedFrame);
                                File.Move(derivedFrame.Replace(".jpg", "1.jpg"), item.ImagePath + "\\" + _filename);
                            }
                        }
                    }

                    long vidLength = Convert.ToInt64(FrameworkHelper.Common.MLHelpers.VideoLength);
                    //DB Entry
                    //int mediaType = 2; //1 for images and 2 for videosf
                    //bool isVideoProcessed = true;

                    watchersetting = VBusiness.GetSettingWatcher(objInfo.LocationId);

                    int VideoSceneCount = watchersetting.Where(e => e.SceneName != "" && (e.IsMixerScene == null || e.IsMixerScene == false)).Count();
                    int ProfileNameCount = watchersetting.Where(e => e.SceneName != "" && e.IsMixerScene == true).Count();
                    _isVideoProcessed = true;
                    if (_IsAdvancedVideoEditActive)
                    {
                        if (VideoSceneCount > 0)  //look in the Videoscene table for scene name
                            _isVideoProcessed = false;

                    }
                    if (isAutoVideoProcessingActive && _isVideoProcessed == true)
                        if (ProfileNameCount > 0)
                            _isVideoProcessed = false;

                    int lastPhotoPKey = photoBiz.SetPhotoDetails
                    (objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename, serverDate, objInfo.PhotographerId.ToString(), string.Empty, objInfo.LocationId, string.Empty, null, null, null, 1, objInfo.CharacterId, vidLength, _isVideoProcessed, 2);

                    if (fileInfo.Exists)
                    {
                        if (fileInfo.IsReadOnly)
                            File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                        fileInfo.Delete();
                    }
                    //File.Delete(_networkBackupPath + "\\" + fileInfo.Name);
                    //fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
                    //}
                }

                catch (Exception ex)
                {
                    //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                    ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                }

            }

        }

        public void NormalDownloadFlow(List<MyImageClass> imagelist, ManualDownloadInfo objInfo, long DisplayOrderNumber)
        {

            //String s = String.Empty;
            //int i = 0;
            //List<MyImageClassList> lstMyImgClassList = new List<MyImageClassList>();

            //MyImageClassList myImgClassList = new MyImageClassList();

            foreach (var item in imagelist)
            {
                try
                {

                    FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + item.FileExtension);
                    ErrorHandler.ErrorHandler.LogFileWrite("Video comparess function call.");
                    objInfo = PopulateManualDownloadInfo(item);
                    if (objInfo == null)
                        continue;
                    //string vidExtension = System.IO.Path.GetExtension(item.Title.ToLower());
                    //string filename = _objDataLayer.ServerDateTime().Day.ToString() + _objDataLayer.ServerDateTime().Month.ToString() + PhotoNo.ToString() + "_" + PhotographerId.ToString();
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    string srcImageName = item.Title + item.FileExtension;
                    DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                    int vidCount = photoBiz.PhotoCountCurrentDate(RFID: objInfo.PhotoNo.ToString(), photographerID: objInfo.PhotographerId, mediaType: 2);
                    string filename = string.Empty;
                    if (vidCount > 0)
                    {
                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                       + "_" + objInfo.PhotographerId.ToString() + "(" + vidCount + ")" + item.FileExtension;
                    }
                    else
                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                       + "_" + objInfo.PhotographerId.ToString() + item.FileExtension;



                    ErrorHandler.ErrorHandler.LogFileWrite("Video comparess function call.   line number : 420");
                    //if (photoBiz.CheckPhotos(filename, objInfo.PhotographerId))
                    //{
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 423 video");
                    SetNewConfigLocationValues(objInfo.LocationId, objInfo.SubStoreId);
                    File.Copy(Path.Combine(item.ImagePath, srcImageName), Path.Combine(_filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension)), true);
                    // Create Minified Videos By Suraj....
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 427");
                    CreateMinifiedVideo(Path.Combine(_filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension)), _filePathDate, (Path.GetFileNameWithoutExtension(filename) + item.FileExtension));
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 429 video");
                    // End Changes .....
                    string srcThumb = Path.GetDirectoryName(_filePath) + "\\" + Path.GetFileName(item.Title) + ".jpg";
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 432 video");
                    //Extract thumbnail & resize
                    //FrameworkHelper.Common.MLHelpers mlHelper = new FrameworkHelper.Common.MLHelpers();
                    ////ThumbnailExtractor.ExtractThumbnailFromVideo(Path.Combine(item.ImagePath, srcImageName), 4, 4, Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg"), ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.MediaPlayerLicenseKey) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.MediaPlayerLicenseKey] : string.Empty, ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeUsername) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeUsername] : string.Empty, ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeEmailId) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeEmailId] : string.Empty);
                    //string FilePath = FrameworkHelper.Common.MLHelpers.ExtractThumbnail(Path.Combine(item.ImagePath, srcImageName));

                    //if (!string.IsNullOrEmpty(FilePath))
                    //    FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg"));
                    //Save Video Thumbnail.
                    // Added by Suraj .... for Thumbnil are not genrated properly ....
                    string filSavePath = Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(filename) + ".jpg");
                    string minifidFilePath = Path.Combine(_thumbnailFilePathDate, "m_" + Path.GetFileNameWithoutExtension(filename) + ".jpg");
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 444");
                    //ThumbnailExtractor.ExtractThumbnailFromVideo(file.FullName, 4, 4, filSavePath, _VisioForgeLicenseKey, _VisioForgeUserName, _VisioForgeEmailID);
                    string FilePath = MLHelpers.ExtractThumbnail(Path.Combine(item.ImagePath, srcImageName));
                    ErrorHandler.ErrorHandler.LogFileWrite("line number : 447");
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, filSavePath);
                        File.Copy(filSavePath, minifidFilePath);
                    }
                    // End Changes ...
                    if (IsAutoVideoFrameExtEnabled)
                    {

                        List<double> framesec = ReadXml();
                        if (framesec != null || framesec.Count > 0)
                        {
                            List<string> FrameFilePath = FrameworkHelper.Common.MLHelpers.ExtractFramesManualDownload(framesec, item.Title, item.ImagePath, item.FileExtension, videoFrameCropRatio);

                            //Move File into download folder
                            foreach (string derivedFrame in FrameFilePath)
                            {
                                ResetLowDPI(derivedFrame, derivedFrame.Replace(".jpg", "1.jpg"));
                                string _filename = Path.GetFileName(derivedFrame);
                                File.Move(derivedFrame.Replace(".jpg", "1.jpg"), item.ImagePath + "\\" + _filename);
                            }
                        }
                    }

                    long vidLength = Convert.ToInt64(FrameworkHelper.Common.MLHelpers.VideoLength);
                    //DB Entry
                    //int mediaType = 2; //1 for images and 2 for videosf
                    //bool isVideoProcessed = true;

                    watchersetting = VBusiness.GetSettingWatcher(objInfo.LocationId);

                    int VideoSceneCount = watchersetting.Where(e => e.SceneName != "" && (e.IsMixerScene == null || e.IsMixerScene == false)).Count();
                    int ProfileNameCount = watchersetting.Where(e => e.SceneName != "" && e.IsMixerScene == true).Count();
                    _isVideoProcessed = true;
                    if (_IsAdvancedVideoEditActive)
                    {
                        if (VideoSceneCount > 0)  //look in the Videoscene table for scene name
                            _isVideoProcessed = false;

                    }
                    if (isAutoVideoProcessingActive && _isVideoProcessed == true)
                        if (ProfileNameCount > 0)
                            _isVideoProcessed = false;

                    //int lastPhotoPKey = photoBiz.SetPhotoDetails
                    //(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename, serverDate, objInfo.PhotographerId.ToString(), string.Empty, objInfo.LocationId, string.Empty, null, null, null, 1, objInfo.CharacterId, vidLength, _isVideoProcessed, 2);

                    int lastPhotoPKey = photoBiz.SetPhotoDetails
                    (objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename, serverDate, objInfo.PhotographerId.ToString(), string.Empty, objInfo.LocationId, string.Empty, null, null, null, 1, objInfo.CharacterId, vidLength, _isVideoProcessed, 2,photoDisplayOrder:DisplayOrderNumber);

                    if (fileInfo.Exists)
                    {
                        if (fileInfo.IsReadOnly)
                            File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                        fileInfo.Delete();
                    }
                    //File.Delete(_networkBackupPath + "\\" + fileInfo.Name);
                    //fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
                    //}
                }

                catch (Exception ex)
                {
                    //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                    ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                }

            }

        }

        private bool ChageDPI(string filePath)
        {
            Int32 HorizontalDPI = 0;
            Int32 VerticalDPI = 0;
            // if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
            // {
            BitmapImage testbmp = new BitmapImage();
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                MemoryStream ms = new MemoryStream();
                fileStream.CopyTo(ms);
                ms.Seek(0, SeekOrigin.Begin);
                fileStream.Close();
                testbmp.BeginInit();
                testbmp.StreamSource = ms;
                testbmp.EndInit();
                testbmp.Freeze();
            }
            HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
            VerticalDPI = Convert.ToInt32(testbmp.DpiY);
            if (HorizontalDPI != 300 && VerticalDPI != 300)
            {
                return true;
            }
            else return false;
            // }
        }
        private void ResetLowDPI(string InputFilePath, string OutputFilePath)
        {
            try
            {
                if (ChageDPI(InputFilePath))
                {
                    System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);
                    System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);
                    outImage.Save(OutputFilePath);
                    newImage.Dispose();
                    outImage.Dispose();
                    File.Delete(InputFilePath);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        bool isAutoVideoProcessingActive = false;
        bool _IsAdvancedVideoEditActive = false;
        string videoFramePositions = string.Empty;
        bool IsAutoVideoFrameExtEnabled = false;
        string videoFrameCropRatio = string.Empty;
        List<iMixConfigurationLocationInfoList> NewConfigList = new List<iMixConfigurationLocationInfoList>();
        private void SetNewConfigLocationValues(int locationId, int substoreId)
        {
            List<iMixConfigurationLocationInfo> _objNewConfigList = new List<iMixConfigurationLocationInfo>();
            // if (NewConfigList.Count > 0)
            {
                var result = NewConfigList.Where(x => x.SubstoreId == substoreId && x.LocationId == locationId).FirstOrDefault();
                if (result != null)
                {
                    _objNewConfigList = result.iMixConfigurationLocationList;
                }
                else
                {
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    _objNewConfigList = configBusiness.GetConfigLocation(locationId, substoreId);
                    iMixConfigurationLocationInfoList configList = new iMixConfigurationLocationInfoList();
                    configList.iMixConfigurationLocationList = _objNewConfigList;
                    configList.SubstoreId = substoreId;
                    configList.LocationId = locationId;
                    NewConfigList.Add(configList);
                }

                foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
                {
                    switch (_objNewConfig.IMIXConfigurationMasterId)
                    {

                        case (int)ConfigParams.IsEnableAutoVidProcessing:

                            isAutoVideoProcessingActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;

                        case (int)ConfigParams.IsAdvancedVideoEditActive:
                            _IsAdvancedVideoEditActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.VideoFramePositions:
                            videoFramePositions = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "Empty";
                            break;
                        case (int)ConfigParams.IsAutoVideoFrameExtEnabled:
                            IsAutoVideoFrameExtEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.VideoFrameCropRatio:
                            videoFrameCropRatio = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "None";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Gets the rotation value.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;
                default:
                    return 0;
            }
        }

        private string ConvertDegreeAngleToDouble(string degrees, string minutes, string seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return (degrees.ToDouble() + (minutes.ToDouble() / 60) + (seconds.ToDouble() / 3600)).ToString();
        }

        //private void ResetLowDPI(string InputFilePath, string OutputFilePath)
        //{
        //    System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);
        //    System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);
        //    outImage.Save(OutputFilePath);
        //}
        private bool CheckResetDPIRequired(string HR, string VR, string PhotographerId)
        {
            try
            {
                int hr = Convert.ToInt32(HR.Replace("'", ""));
                int vr = Convert.ToInt32(VR.Replace("'", ""));
                //bool cameraSetting = _objDataLayer.GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                bool cameraSetting = (new CameraBusiness()).GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                if ((hr != 300 || vr != 300) && cameraSetting)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        private void SetNewConfigValues()
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMIXConfigurationInfo> _objNewConfigList = configBusiness.GetNewConfigValues(_SubStoreId);
            //foreach (iMIXConfigurationInfo _objNewConfig in _objNewConfigList)
            //{
            //    switch (_objNewConfig.IMIXConfigurationMasterId)
            //    {

            //        //case (int)ConfigParams.IsEnabledRFID:
            //        //    IsRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
            //        //    break;
            //        //case (int)ConfigParams.RFIDScanType:
            //        //    RfidScanType = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? 0 : Convert.ToInt32(_objNewConfig.ConfigurationValue);
            //        //    break;
            //        //case (int)ConfigParams.IsGreenScreenFlow:
            //        //    IsGreenScreenWorkFlow = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
            //        //    break;
            //        //  case (int)ConfigParams.MediaPlayerLicenseKey:
            //        //     _VisioForgeLicenseKey = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : string.Empty;
            //        //    break;

            //        default:
            //            break;
            //    }
            //}
        }

        private List<double> ReadXml()
        {

            if (videoFramePositions != "Empty")
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(videoFramePositions);
                XmlNodeList itemNodes = xmlDocument.SelectNodes("Frames/frame");
                List<double> frameSec = new List<double>();
                foreach (XmlNode loc in itemNodes)
                {
                    frameSec.Add(Convert.ToDouble(loc.InnerText));
                }
                return frameSec;

            }
            return null;
        }

        // Create MinifiedVideos Function is created by Suraj.
        private void CreateMinifiedVideo(string sourceFilePath, string tragetFilePath ,string minifiedFileName)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function line number : 739 video");
                string file = sourceFilePath;
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function minified FileName : "+minifiedFileName);
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function minified tragetFilepath : " + tragetFilePath);
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function minified sourceFilepath : " + sourceFilePath);
                string command = "C:\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;
                //string command = "C:\\Program Files (x86)\\iMix\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;  // For deployment.

                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);

                procStartInfo.RedirectStandardOutput = false;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function line number : 750 video");
                // wrap IDisposable into using (in order to release hProcess) 
                using (Process process = new Process())
                {
                    process.StartInfo = procStartInfo;
                    process.Start();

                    process.WaitForExit();
                    process.WaitForExit();

                    ///and only then read the result
                    //string result = process.StandardOutput.ReadToEnd();
                    //Console.WriteLine(result);
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Minified video Function line number : 764 video");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        // End Changes
    }
}
