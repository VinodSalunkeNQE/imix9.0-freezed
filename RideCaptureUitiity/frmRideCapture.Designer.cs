﻿namespace RideCaptureUtility
{
    partial class frmRideCapture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRideCapture));
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnStartCamera = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnRFIDSync = new System.Windows.Forms.Button();
            this.picSync = new System.Windows.Forms.PictureBox();
            this.rdb270 = new System.Windows.Forms.RadioButton();
            this.rdb180 = new System.Windows.Forms.RadioButton();
            this.rdb90 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbNone = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.picSync)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ride Capture Uitility (Ver 2.0)";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(436, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnStartCamera
            // 
            this.btnStartCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartCamera.Location = new System.Drawing.Point(12, 39);
            this.btnStartCamera.Name = "btnStartCamera";
            this.btnStartCamera.Size = new System.Drawing.Size(105, 28);
            this.btnStartCamera.TabIndex = 3;
            this.btnStartCamera.Text = "Start Camera";
            this.btnStartCamera.UseVisualStyleBackColor = true;
            this.btnStartCamera.Click += new System.EventHandler(this.btnStartCamera_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(11, 78);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(0, 13);
            this.lblMsg.TabIndex = 4;
            // 
            // btnRFIDSync
            // 
            this.btnRFIDSync.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRFIDSync.Location = new System.Drawing.Point(138, 39);
            this.btnRFIDSync.Name = "btnRFIDSync";
            this.btnRFIDSync.Size = new System.Drawing.Size(105, 28);
            this.btnRFIDSync.TabIndex = 5;
            this.btnRFIDSync.Text = "RFID Sync";
            this.btnRFIDSync.UseVisualStyleBackColor = true;
            this.btnRFIDSync.Click += new System.EventHandler(this.btnRFIDSync_Click);
            // 
            // picSync
            // 
            this.picSync.Location = new System.Drawing.Point(14, 102);
            this.picSync.Name = "picSync";
            this.picSync.Size = new System.Drawing.Size(399, 356);
            this.picSync.TabIndex = 6;
            this.picSync.TabStop = false;
            // 
            // rdb270
            // 
            this.rdb270.AutoSize = true;
            this.rdb270.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdb270.Location = new System.Drawing.Point(451, 45);
            this.rdb270.Name = "rdb270";
            this.rdb270.Size = new System.Drawing.Size(46, 17);
            this.rdb270.TabIndex = 9;
            this.rdb270.Text = "270";
            this.rdb270.UseVisualStyleBackColor = true;
            // 
            // rdb180
            // 
            this.rdb180.AutoSize = true;
            this.rdb180.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdb180.Location = new System.Drawing.Point(407, 45);
            this.rdb180.Name = "rdb180";
            this.rdb180.Size = new System.Drawing.Size(46, 17);
            this.rdb180.TabIndex = 8;
            this.rdb180.Text = "180";
            this.rdb180.UseVisualStyleBackColor = true;
            // 
            // rdb90
            // 
            this.rdb90.AutoSize = true;
            this.rdb90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdb90.Location = new System.Drawing.Point(362, 45);
            this.rdb90.Name = "rdb90";
            this.rdb90.Size = new System.Drawing.Size(39, 17);
            this.rdb90.TabIndex = 7;
            this.rdb90.Text = "90";
            this.rdb90.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(250, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Rotate (Degree)";
            // 
            // rdbNone
            // 
            this.rdbNone.AutoSize = true;
            this.rdbNone.Checked = true;
            this.rdbNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbNone.Location = new System.Drawing.Point(503, 45);
            this.rdbNone.Name = "rdbNone";
            this.rdbNone.Size = new System.Drawing.Size(55, 17);
            this.rdbNone.TabIndex = 11;
            this.rdbNone.TabStop = true;
            this.rdbNone.Text = "None";
            this.rdbNone.UseVisualStyleBackColor = true;
            // 
            // frmRideCapture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 492);
            this.Controls.Add(this.rdbNone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rdb270);
            this.Controls.Add(this.rdb180);
            this.Controls.Add(this.rdb90);
            this.Controls.Add(this.picSync);
            this.Controls.Add(this.btnRFIDSync);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnStartCamera);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmRideCapture";
            this.Text = "Ride Capture Uitility";
            this.Load += new System.EventHandler(this.frmRideCapture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picSync)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnStartCamera;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnRFIDSync;
        private System.Windows.Forms.PictureBox picSync;
        private System.Windows.Forms.RadioButton rdb270;
        private System.Windows.Forms.RadioButton rdb180;
        private System.Windows.Forms.RadioButton rdb90;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbNone;
    }
}

