﻿using AVT.VmbAPINET;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace RideCaptureUtility
{
    public partial class frmRideCapture : Form
    {
        public string camid;
        public bool autoStartMode = false;
        Bitmap b;
        BackgroundWorker bwRideCapt = new BackgroundWorker();
        private VimbaHelper m_VimbaHelper = null;
        private bool m_Acquiring = false;
        private string camerapath = string.Empty;
        System.Windows.Forms.Timer tmrVerifyCamera = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmrSelfStarter = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmrVerifySettings = new System.Windows.Forms.Timer();
        int autoTry = 5;
        int rotateAngle = 0;
        Hashtable htSettings;
        string currentSettingsFile = string.Empty;
        enum SettingsMode
        {
            Unknown = 0,
            Save = 1,
            Load = 2
        };
        public frmRideCapture()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            bwRideCapt.DoWork += bwRideCapt_DoWork;
            bwRideCapt.RunWorkerCompleted += bwRideCapt_RunWorkerCompleted;
            bwRideCapt.WorkerSupportsCancellation = true;
            tmrVerifyCamera.Tick += new EventHandler(tmrVerifyCamera_Tick);
            tmrVerifyCamera.Interval = 600000;//10 minutes
            tmrVerifyCamera.Start();

            tmrSelfStarter.Tick += new EventHandler(tmrSelfStarter_Tick);
            tmrSelfStarter.Interval = 30000;
            tmrSelfStarter.Enabled = false;

            LoadMasterSettings("MasterSettings.xml");

            tmrVerifySettings.Tick += new EventHandler(tmrVerifySettings_Tick);
            tmrVerifySettings.Interval = 600000;//10 minutes
            tmrVerifySettings.Start();

        }
        /// <summary>
        /// Timer to update camera settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tmrVerifySettings_Tick(object sender, EventArgs e)
        {
            tmrVerifySettings.Stop();
            string newSettingsFile = GetNewSettings();
            if (String.IsNullOrEmpty(currentSettingsFile) && newSettingsFile != currentSettingsFile)
            {
                currentSettingsFile = newSettingsFile;
                //stop camera -> update settings file -> start camera 
                if(stopCamera())
                {
                    ResetCameraSettings(currentSettingsFile);
                }
            }
            tmrVerifySettings.Start();
        }

        private bool stopCamera()
        {
            bool stopped = false;
            try
            {
                if (m_VimbaHelper != null)
                {
                    m_VimbaHelper.Shutdown();
                    stopped = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                stopped = false;
            }
            return stopped;
        }
        int attempts = 1;
        bool camStarted = false;
        void tmrSelfStarter_Tick(object sender, EventArgs e)
        {
            attempts++;
            autoStartMode = true;
            tmrVerifyCamera.Stop();
            camStarted = false;
            string atmptMsg = "Attempt number:" + Convert.ToString(attempts) + " Trying to self start the camera at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
            ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
            lblMsg.Text = atmptMsg;
            if (ResetCameraSettings(currentSettingsFile))
            {
                bwRideCapt.RunWorkerAsync();
                camStarted = true;
                autoStartMode = false;
                atmptMsg = "Camera self started at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                lblMsg.Text = atmptMsg;
                ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                attempts = 0;
                tmrSelfStarter.Stop();
                tmrVerifyCamera.Start();
                camStarted = true;
            }

            if (attempts == autoTry)
            {
                autoStartMode = false;
                atmptMsg = "Camera could not be self started after sevral attempts at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                lblMsg.Text = atmptMsg;
                ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                btnStartCamera.Enabled = true;
                rdb90.Enabled = true;
                rdb270.Enabled = true;
                rdb180.Enabled = true;
                tmrSelfStarter.Stop();
            }
        }
        bool showMsg = true;
        void tmrVerifyCamera_Tick(object sender, EventArgs e)
        {
            try
            {
                if (showMsg)
                {
                    if (m_VimbaHelper.CameraList.Count == 0)
                    {
                        showMsg = false; tmrVerifyCamera.Stop();
                        lblMsg.Text = "Camera error!! Trying to reset.";
                        tmrSelfStarter.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool CameraSelfStart()
        {
            autoStartMode = true;
            tmrVerifyCamera.Stop();
            bool camStarted = false;
            for (int i = 0; i < autoTry; i++)
            {
                string atmptMsg = "Attempt number:" + Convert.ToString(i + 1) + " Trying to self start the camera at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                //MessageBox.Show(atmptMsg);
                lblMsg.Text = atmptMsg;
                Thread.Sleep(30000);
                if (ResetCameraSettings(currentSettingsFile))
                {
                    tmrVerifyCamera.Start();
                    bwRideCapt.RunWorkerAsync();
                    camStarted = true;
                    autoStartMode = false;
                    atmptMsg = "Camera self started at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                    lblMsg.Text = atmptMsg;
                    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                    break;
                }
                else
                {
                    autoStartMode = false;
                    atmptMsg = "Camera could not be self started after several attempts at " + DateTime.Now.ToString("dd/MMM/yyyy, hh:mm:ss");
                    lblMsg.Text = atmptMsg;
                    ErrorHandler.ErrorHandler.LogFileWrite(atmptMsg);
                    btnStartCamera.Enabled = true;
                    rdb90.Enabled = true;
                    rdb270.Enabled = true;
                    rdb180.Enabled = true;
                }
            }
            return camStarted;
        }
        /// <summary>
        /// Starts the capture.
        /// </summary>
        public void StartCapture()
        {
            try
            {
                List<CameraInfo> cInfo = new List<CameraInfo>();
                cInfo = m_VimbaHelper.CameraList;
                CameraInfo camera = cInfo[0];
                camid = camera.ID.ToString();
                camerapath = (new CameraBusiness()).GetCameraPathForRideCameraId(camid); 
                //Common.GetCameraPath(camid);
                startCapturing(camera);
                showMsg = true;
                ErrorHandler.ErrorHandler.LogFileWrite("camera id: == " + camid);
                ErrorHandler.ErrorHandler.LogFileWrite("camera path: == " + camerapath);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                showMsg = false;
                btnStartCamera.Enabled = true;
                rdb90.Enabled = true;
                rdb270.Enabled = true;
                rdb180.Enabled = true;
                if (!autoStartMode)
                    MessageBox.Show("Camera not found", "ImixRideProcessor");
            }
        }

        /// <summary>
        /// Called when [frame received].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="FrameEventArgs"/> instance containing the event data.</param>
        private void OnFrameReceived(object sender, FrameEventArgs args)
        {
            try
            {
                if (InvokeRequired == true)
                {
                    BeginInvoke(new FrameReceivedHandler(this.OnFrameReceived), sender, args);
                    return;
                }

                if (true == m_Acquiring)
                {
                    Image image = args.Image;
                    if (null != image)
                    {
                        using (b = new Bitmap(image))
                        {
                            if (rotateAngle == 90)
                                b.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            else if (rotateAngle == 180)
                                b.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            else if (rotateAngle == 270)
                                b.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            b.Save(camerapath + "\\" + System.Guid.NewGuid().ToString() + ".jpg");
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                // ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                // MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Starts the capturing.
        /// </summary>
        /// <param name="camera">The camera.</param>
        public void startCapturing(CameraInfo camera)
        {
            try
            {

                m_VimbaHelper.StartContinuousImageAcquisition(camera.ID, this.OnFrameReceived);
                m_Acquiring = true;
                //  UpdateAcquireButton();

            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message.ToString(), "Imix Ride Processor");
            }

        }
        /// <summary>
        /// Handles the Click event of the btnExit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult drExit = MessageBox.Show("Are you sure you want to close the application?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drExit == DialogResult.No)
                return;
            try
            {
                //On stop button click (UI layer):
                if (m_VimbaHelper != null)
                {
                    m_VimbaHelper.Shutdown();
                }
            }
            catch
            {
            }
            Application.Exit();
        }

        /// <summary>
        /// Handles the Load event of the frmRideCapture control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void frmRideCapture_Load(object sender, EventArgs e)
        {
            //bwRideCapt.RunWorkerAsync();
        }

        private void StartVimba()
        {
            try
            {
                VimbaHelper vimbaHelper = new VimbaHelper();
                vimbaHelper.Startup();
                vimbaHelper.Startup();
                m_VimbaHelper = vimbaHelper;
                StartCapture();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                if (!autoStartMode)
                    MessageBox.Show(ex.Message.ToString(), "Imix Ride Processor");
            }
        }

        void bwRideCapt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Vimba started...");
        }

        void bwRideCapt_DoWork(object sender, DoWorkEventArgs e)
        {
            btnStartCamera.Enabled = false;
            rdb90.Enabled = false;
            rdb270.Enabled = false;
            rdb180.Enabled = false;
            StartVimba();
        }

        /// <summary>
        /// Sets the method.
        /// </summary>
        /// <exception cref="System.Exception">
        /// No camera available.
        /// or
        /// Could not open any camera.
        /// </exception>
        public bool ResetCameraSettings(string settingsFile)
        {
            bool setSaved = false;
            string cameraID = null;
            string fileName = null;
            SettingsMode settingsMode = SettingsMode.Load;
            bool printHelp = false;
            bool ignoreStreamable = false;
            try
            {
                if (null == fileName)
                {
                    fileName = settingsFile;//"CameraSettings.xml";
                }

                //Create a new Vimba entry object
                AVT.VmbAPINET.Vimba vimbaSystem = new AVT.VmbAPINET.Vimba();

                //Startup API
                vimbaSystem.Startup();

                try
                {
                    //Open camera
                    AVT.VmbAPINET.Camera camera = null;
                    try
                    {
                        if (null == cameraID)
                        {
                            //Open first available camera

                            //Fetch all cameras known to Vimba
                            AVT.VmbAPINET.CameraCollection cameras = vimbaSystem.Cameras;
                            if (cameras.Count < 0)
                            {
                                throw new Exception("No camera available.");
                            }

                            foreach (AVT.VmbAPINET.Camera currentCamera in cameras)
                            {
                                //Check if we can open the camere in full mode
                                VmbAccessModeType accessMode = currentCamera.PermittedAccess;
                                if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                                {
                                    //Now get the camera ID
                                    cameraID = currentCamera.Id;

                                    //Try to open the camera
                                    try
                                    {
                                        currentCamera.Open(VmbAccessModeType.VmbAccessModeFull);
                                    }
                                    catch
                                    {
                                        //We can ignore this exception because we simply try
                                        //to open the next camera.
                                        continue;
                                    }

                                    camera = currentCamera;
                                    break;
                                }
                            }

                            if (null == camera)
                            {
                                throw new Exception("Could not open any camera.");
                            }
                        }
                        else
                        {
                            //Open specific camera
                            camera = vimbaSystem.OpenCameraByID(cameraID, VmbAccessModeType.VmbAccessModeFull);
                        }



                        switch (settingsMode)
                        {
                            case SettingsMode.Load:
                                {
                                    RideCaptureUtility.VmbAPINET.Examples.LoadSaveSettings.LoadFromFile(camera, fileName);
                                    setSaved = true;
                                }
                                break;
                        }
                    }
                    finally
                    {
                        if (null != camera)
                        {
                            camera.Close();
                        }
                    }
                }
                finally
                {
                    vimbaSystem.Shutdown();
                }
            }
            catch (Exception exception)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(exception);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage); setSaved = false;
                if (!autoStartMode)
                    MessageBox.Show(exception.Message);
            }
            return setSaved;
        }



        private void btnStartCamera_Click(object sender, EventArgs e)
        {
            tmrVerifyCamera.Stop();
            if (ResetCameraSettings(currentSettingsFile))
            {
                autoStartMode = false;
                lblMsg.Text = "Camera started successfully!";
                if (rdb90.Checked)
                    rotateAngle = 90;
                else if (rdb180.Checked)
                    rotateAngle = 180;
                else if (rdb270.Checked)
                    rotateAngle = 270;
                else
                    rotateAngle = 0;

                tmrVerifyCamera.Start();
                bwRideCapt.RunWorkerAsync();
            }
            else
            {
                //btnResetCamera.Enabled = true;
                btnStartCamera.Enabled = true;
                rdb90.Enabled = true;
                rdb270.Enabled = true;
                rdb180.Enabled = true;
            }

        }

        private void btnRFIDSync_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateSettings("Off");
                ResetCameraSettings(currentSettingsFile);
                Bitmap b;
                List<CameraInfo> cInfo = new List<CameraInfo>();
                if (m_VimbaHelper == null)
                {
                    VimbaHelper vimbaHelper = new VimbaHelper();
                    m_VimbaHelper = vimbaHelper;
                }
                m_VimbaHelper.Startup();
                cInfo = m_VimbaHelper.CameraList;
                if (cInfo.Count > 0)
                {
                    CameraInfo camera = cInfo[0];
                    camerapath = (new CameraBusiness()).GetCameraPathForRideCameraId(camera.ID); 
                    System.Drawing.Image _objimage = m_VimbaHelper.AcquireSingleImage(camera.ID);
                    //picSync.Image = _objimage;
                    using (b = new Bitmap(_objimage))
                    {
                        if (rdb90.Checked)
                            b.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        else if (rdb180.Checked)
                            b.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        else if (rdb270.Checked)
                            b.RotateFlip(RotateFlipType.Rotate270FlipNone);

                        string strPath = camerapath + "\\" + System.Guid.NewGuid().ToString() + ".jpg";
                        b.Save(strPath);
                        picSync.ImageLocation = strPath;
                        picSync.SizeMode = PictureBoxSizeMode.StretchImage;
                        lblMsg.Text = "Image captured successfully!";
                    }
                }
                UpdateSettings("On");
                ResetCameraSettings(currentSettingsFile);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void UpdateSettings(string TriggerMode)
        {
            string clsExe = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\CameraSettings.xml";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(clsExe);
            XmlNodeList xndList = xdoc.GetElementsByTagName("Enumeration");
            foreach (XmlNode item in xndList)
            {
                if (item.Attributes["Name"].Value == "TriggerMode")
                {
                    item.InnerText = TriggerMode;
                    break;
                }
            }
            xdoc.Save(clsExe);
        }

        /// <summary>
        /// Function to load Master Settings File for dynamic time based camera settings profile selection
        /// </summary>
        /// <param name="MasterSettingsFilePath">Master settings file path </param>
        private void LoadMasterSettings(string MasterSettingsFilePath)
        {
            try
            {
                htSettings = new Hashtable();
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(MasterSettingsFilePath);
                XmlNodeList xndList = xdoc.GetElementsByTagName("Configuration");
                for (int i = 0; i <= xndList.Count - 1; i++)
                {
                    XmlNode xnode = xndList[i];
                    string loadTime = xnode.Attributes["LoadTime"].Value.ToString();
                    DateTime dtLoadTime = Convert.ToDateTime(loadTime);
                    string File = xnode.Attributes["File"].Value.ToString();
                    if (!htSettings.ContainsKey(dtLoadTime))
                        htSettings.Add(i, dtLoadTime + "_" + File);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Fetch new settings file for camera profile from the master settings file
        /// </summary>
        /// <returns>settings file name or path</returns>
        private string GetNewSettings()
        {
            string retFile = string.Empty;
            try
            {
                DateTime dtCurrentTime = DateTime.Now;
                for (int j = 0; j <= htSettings.Count - 1; j++)
                {
                    string[] tempVarThis = htSettings[j].ToString().Split('_');
                    DateTime dtLoadTimeThis = Convert.ToDateTime(tempVarThis[0]);
                    string[] tempVarNext; DateTime dtLoadTimeNext;
                    if (htSettings.ContainsKey(j + 1))
                    {
                        tempVarNext = htSettings[j + 1].ToString().Split('_');
                        dtLoadTimeNext = Convert.ToDateTime(tempVarNext[0]);
                    }
                    else
                    {
                        tempVarNext = htSettings[j].ToString().Split('_');
                        dtLoadTimeNext = DateTime.Now.AddHours(1);
                    }
                    int greaterThanCurrentTime = TimeSpan.Compare(dtCurrentTime.TimeOfDay, dtLoadTimeThis.TimeOfDay);
                    int lessThanNextTime = TimeSpan.Compare(dtCurrentTime.TimeOfDay, dtLoadTimeNext.TimeOfDay);

                    if (greaterThanCurrentTime.Equals(1) && lessThanNextTime.Equals(-1))
                    {
                        retFile = tempVarThis[1];
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return retFile;
        }
    }
}
