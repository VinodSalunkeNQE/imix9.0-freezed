﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
//using Microsoft.ApplicationBlocks.Data;

namespace RideCaptureUtility
{
    class Common
    {
        static readonly String strConn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        public static string GetCameraPath(string CameraId)
        {
            try
            {
                SqlParameter[] spparam = { new SqlParameter("@DgCameraModel", CameraId) };
                object _objnumber = SqlHelper.ExecuteScalar(strConn, "GetRideCameraSettingbyId", spparam);
                return Convert.ToString(_objnumber);
               
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
