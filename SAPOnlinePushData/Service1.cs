﻿using DigiPhoto.IMIX.Business;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;


//using Microsoft.Azure.Management.Fluent;//Install-Package Microsoft.Azure.Management.Fluent -Version 1.32.0
//using Microsoft.Azure.Management.ResourceManager.Fluent;
//using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

using Microsoft.Azure.KeyVault;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using RestSharp;//Install-Package RestSharp -Version 106.10.1
using Newtonsoft.Json;

//using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Collections;
//using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.IMIX.Model;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.IMIX.DataAccess;
using System.Threading;
using System.Configuration;
using DigiPhoto.IMIX.Model.Base;
using System.Data.SqlClient;

namespace DigiAzureUploadService
{
    public partial class Service1 : ServiceBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static System.Timers.Timer timer;

        public Service1()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("OnStart");
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Upload now");
                    ErrorHandler.ErrorHandler.LogFileWrite("UploadToAzureBlob() line 60 start ");
                    StartScheduler();
                }
                else
                {
                    throw new Exception("Already Started");
                }
            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.Stop();
            }
        }

        public void SyncOrderToBlob()
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                    //this.timer = new System.Timers.Timer();
                    //this.timer.Interval = 1000 * 5;
                    //this.timer.AutoReset = true;
                    //this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    //this.timer.Enabled = false;
                    //this.timer.Start();
                    //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
                    //timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }
            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.Stop();
            }

            //bool IsSyncEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSet3Enabled"]);
            //if (!IsSyncEnabled)
            //{
            //    log.Info("Set 3 is not Enabled !");
            //}

            //if (IsSyncEnabled)
            //{
            //    //Pull();
            //    //Push();
            //}
        }

        public void SyncPortalOrderImages()
        {

        }

        public void SAPOnlinePush()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("SAPOnlinePush method start ");
                bool isSet3Enabled = false;
                ConfigBusiness conBiz = new ConfigBusiness();
                int set3Enabled = (int)conBiz.GETSet3EnabledID();

                string path = Path.Combine(@"C:\Program Files (x86)\iMix\AzureUploadService", "azureauth.properties"); // commited by Suraj
                string rgName = System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"];

                SAPOnlinePushData();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
        }

        private void SAPOnlinePushData()
        {
            bool Flag = true;
            ErrorHandler.ErrorHandler.LogFileWrite("SAPOnlinePushData is Called");
            SAPOnlinePushData _SAP = new SAPOnlinePushData();
            List<SAPOnlineDataPushInfo> lstAllLatest = _SAP.SelectAllSAPOnlinePushData().OrderByDescending(t => t.Id).ToList();
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 606  ");

            if (lstAllLatest.Count > 0)
            {
                if (!Flag)
                {
                    return;
                }
                APIWrapper wrapper = APIWrapper.GetInstance;
                bool isDataUpload = true;
                int RetryConfigCount = 0;
                if (ConfigurationManager.AppSettings["RetryCount"] != null)
                {
                    RetryConfigCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                }

                foreach (var sapitem in lstAllLatest)
                {
                    if (_SAP.GetretryFiledCount(sapitem.SalesOrderno, RetryConfigCount))
                    {
                        string photoFilePath = string.Empty;

                        // SAP ONLINE PUSH DATA

                        List<SAPOnlineDataPushInfo> _DG_SapOnlineData = new List<SAPOnlineDataPushInfo>();
                        var _SapOnlineData = new SAPOnlineDataPushInfo();

                        _SapOnlineData.SalesOrderno = sapitem.SalesOrderno;
                        _SapOnlineData.ItemNo = sapitem.ItemNo;
                        _SapOnlineData.Customer = sapitem.Customer;
                        _SapOnlineData.CustomerDescription = sapitem.CustomerDescription;
                        _SapOnlineData.SalesOrg = sapitem.SalesOrg;
                        _SapOnlineData.SalesOrgDescription = sapitem.SalesOrgDescription;
                        _SapOnlineData.DistributionChannel = sapitem.DistributionChannel;
                        _SapOnlineData.DistributionChannelDescription = sapitem.DistributionChannelDescription;
                        _SapOnlineData.Division = sapitem.Division;
                        _SapOnlineData.DivisionDescription = sapitem.DistributionChannelDescription;
                        _SapOnlineData.Material = sapitem.Material;
                        _SapOnlineData.MaterialDescription = sapitem.MaterialDescription;
                        _SapOnlineData.SendingPlant = sapitem.SendingPlant;
                        _SapOnlineData.SendingPlantDescription = sapitem.SendingPlantDescription;
                        _SapOnlineData.Quantity = sapitem.Quantity;
                        _SapOnlineData.UOM = sapitem.UOM;
                        _SapOnlineData.TotalPrice = sapitem.TotalPrice;
                        _SapOnlineData.Cash = sapitem.Cash;
                        _SapOnlineData.Visa = sapitem.Visa;
                        _SapOnlineData.Master = sapitem.Master;
                        _SapOnlineData.Amex = sapitem.Amex;
                        _SapOnlineData.JCB = sapitem.JCB;
                        _SapOnlineData.Unionpay = sapitem.Unionpay;
                        _SapOnlineData.Voucher = sapitem.Voucher;
                        _SapOnlineData.MPU = sapitem.MPU;
                        _SapOnlineData.Exchange = sapitem.Exchange;
                        _SapOnlineData.MobileBanking = sapitem.MobileBanking;
                        _SapOnlineData.KVL = sapitem.KVL;
                        _SapOnlineData.Room = sapitem.Room;
                        _SapOnlineData.FC = sapitem.FC;
                        _SapOnlineData.Others = sapitem.Others;
                        _SapOnlineData.Tax_deduction = sapitem.Tax_deduction;
                        _SapOnlineData.Tax = sapitem.Tax;
                        _SapOnlineData.Discount = sapitem.Discount;
                        _SapOnlineData.OrderDate = sapitem.OrderDate;
                        _SapOnlineData.Customerreference = sapitem.Customerreference;
                        _SapOnlineData.CanceledInvoicelineitem = sapitem.CanceledInvoicelineitem;
                        _SapOnlineData.CanceledBillDoc = sapitem.CanceledBillDoc;
                        _SapOnlineData.Createddate = sapitem.Createddate;
                        _SapOnlineData.CreatedTime = sapitem.CreatedTime;
                        _SapOnlineData.CountryCode = sapitem.CountryCode;
                        _SapOnlineData.PkgSyncCode = sapitem.PkgSyncCode;
                        _SapOnlineData.Syncstatus = sapitem.Syncstatus;
                        _SapOnlineData.UploadStatus = sapitem.UploadStatus;
                        _SapOnlineData.RetryCount = sapitem.RetryCount;
                        _SapOnlineData.Field1 = sapitem.Field1;
                        _SapOnlineData.Field2 = sapitem.Field2;
                        _DG_SapOnlineData.Add(_SapOnlineData);

                        string jsonrequestphoto = Newtonsoft.Json.JsonConvert.SerializeObject(_DG_SapOnlineData);
                        if (wrapper.PostData(jsonrequestphoto, "dgsaponline") == "OK")
                        {
                            if (isDataUpload)
                                isDataUpload = true;
                        }
                        else
                            isDataUpload = false;
                        _DG_SapOnlineData.Clear();
                    }
                    else
                    {
                        log.Error("MetaDeta Upload: MetaDeta Uplo :");
                    }
                };
            }

        }

        public void StartScheduler()
        {
            try
            {

                timer = new System.Timers.Timer();
                timer.Interval = new TimeSpan(0, 0, 10).TotalMilliseconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Enabled = false;
                SAPOnlinePush();

                ErrorHandler.ErrorHandler.LogFileWrite("Timer hits at " + ' ' + DateTime.Now.ToString());

                timer.Start();
                timer.Enabled = true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                timer.Start();
            }
            finally
            {
                timer.Start();
            }
        }

        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
    }
}




