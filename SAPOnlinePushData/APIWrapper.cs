﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiAzureUploadService
{
    class APIWrapper
    {

        string _url;
        private static APIWrapper instance = null;
        private static readonly object obj = new object();

        string Client_Authorization = "#$%85RT";

        private APIWrapper()
        {
            if (ConfigurationManager.AppSettings["URL"] != null)
            {
                this._url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                if (string.IsNullOrEmpty(this._url))
                    this._url = "http://saponlinepushdata-digiphotoglobal-cn.chinacloudsites.cn/api/v1/sap/";
            }

            if (ConfigurationManager.AppSettings["Authorization"] != null)
            {
                string _authorization = Convert.ToString(ConfigurationManager.AppSettings["Authorization"]);
                if (!string.IsNullOrEmpty(_authorization))
                    this.Client_Authorization = _authorization;
            }
        }
        public static APIWrapper GetInstance
        {
            get
            {
                lock (obj)
                {
                    if (instance == null)
                        instance = new APIWrapper();
                }
                return instance;
            }
        }
        public string PostData(string JsonRequest, string APIActionName)
        {
            string result;
            try
            {
                string url = _url + APIActionName;
                RestClient client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("APIKEY", ConfigurationManager.AppSettings["SAPONLINEPUSHDATAAPIKEY"].ToString());
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/x-www-form-urlencoded", JsonRequest, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;

                IRestResponse response = client.Execute(request);
                result = response.StatusCode.ToString();

                ErrorHandler.ErrorHandler.LogFileWrite(response.Content);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                result = "Error";
            }
            return result;
        }
    }
    public class DG_SapOnlineData
    {

        public string SalesOrderno { get; set; }
        public int ItemNo { get; set; }
        public string Customer { get; set; }
        public string CustomerDescription { get; set; }
        public string SalesOrg { get; set; }
        public string SalesOrgDescription { get; set; }
        public string DistributionChannel { get; set; }
        public string DistributionChannelDescription { get; set; }
        public string Division { get; set; }
        public string DivisionDescription { get; set; }
        public string Material { get; set; }
        public string MaterialDescription { get; set; }
        public string SendingPlant { get; set; }
        public string SendingPlantDescription { get; set; }
        public int Quantity { get; set; }
        public string UOM { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        public decimal Cash { get; set; }
        public decimal Visa { get; set; }
        public decimal Master { get; set; }
        public decimal Amex { get; set; }
        public decimal JCB { get; set; }
        public decimal Unionpay { get; set; }
        public decimal Voucher { get; set; }
        public decimal MPU { get; set; }
        public decimal Exchange { get; set; }
        public decimal MobileBanking { get; set; }
        public decimal KVL { get; set; }
        public decimal Room { get; set; }
        public decimal FC { get; set; }
        public decimal Others { get; set; }
        public decimal Tax_deduction { get; set; }
        public decimal Tax { get; set; }
        public decimal Discount { get; set; }
        public DateTime OrderDate { get; set; }
        public string Customerreference { get; set; }
        public string CanceledInvoicelineitem { get; set; }
        public string CanceledBillDoc { get; set; }
        public DateTime Createddate { get; set; }
        public TimeSpan? CreatedTime { get; set; }
        public string CountryCode { get; set; }
        public string PkgSyncCode { get; set; }
        [Required]
        public string Syncstatus { get; set; }
        [Required]
        public string UploadStatus { get; set; }
        public int RetryCount { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }



    }
}
