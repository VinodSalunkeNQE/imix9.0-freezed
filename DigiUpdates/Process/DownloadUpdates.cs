﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.iMix.ViewModel;
using System.IO;
using DigiUpdates.Model;
using System.Threading;

namespace DigiUpdates.Process
{
    public class DownloadUpdates : IProcess
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static int result = 0;
        public int Apply(int option, ref UpdateDetailInfo updateDetails)
        {
            try
            {
                log.Info("Application download option : " + (option == 1 ? "Local" : "Server"));
                Console.WriteLine("Application download from server started...");
                ServiceDownload(ref updateDetails);
                //if (option == 2)
                //{
                //    Console.WriteLine("Application download from server started...");
                //    ServiceDownload(ref updateDetails);
                //}
                //else
                //{
                //    Console.WriteLine("Application download from local started...");
                //    Console.OpenStandardOutput().Flush();
                //    LocalDownload(ref updateDetails);
                //}
            }
            catch (Exception exp)
            {
                log.Error("DownloadUpdates.Apply: ", exp);
            }
            return result;
        }
        private static void LocalDownload(ref UpdateDetailInfo updateDetails)
        {
            try
            {
                log.Info("Application download from local started");
                updateDetails.ProgressBar = new ProgressBar();// ProgressBarReport("Application Download From Local", new ProgressBar(), 0, 100, 1);
                updateDetails.ProgressBar.Report(0.1);
                ProgressBar.RunProgressBarToEnd(updateDetails.ProgressBar, 1, 95, TimeSpan.FromMilliseconds(5000));
                string pathDownload = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");
                string currentVersion = Utility.DBAction.CurrentRegistryVersion();
                List<VersonViewModel> versions = Utility.DBAction.GetUpdateVersionDetails(currentVersion);
                foreach (VersonViewModel version in versions)
                {
                    string localZipPath = Utility.FileAction.CopyUpdate(version.Path, pathDownload);
                    //if (!string.IsNullOrEmpty(localZipPath))
                    if (!string.IsNullOrEmpty(updateDetails.DatabaseBackupPath))//  server updated happening
                    {
                        updateDetails.Versions.Where(x => x.VersionName == version.VersionName).FirstOrDefault().LocalMachineZipPath = localZipPath;
                    }
                    else // local update
                    {
                        log.Info("Application download From local started");
                        VersionInfo vinfo = new VersionInfo();
                        vinfo.VerisonId = version.ApplicationVersionID;
                        vinfo.VersionName = version.VersionName;
                        vinfo.Major = version.Major;
                        vinfo.Minor = version.Minor;
                        vinfo.Revision = version.Revision;
                        vinfo.Build = version.BuildNumber;
                        vinfo.HotFolderPath = version.Path;
                        vinfo.LocalMachineZipPath = localZipPath;
                        updateDetails.Versions.Add(vinfo);
                    }
                }
                log.Info("Application download from local Completed");
                result = 0;
                ProgressBar.AbortProgressBarRunToEnd(100);
            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("DownloadUpdates.LocalDownload: ", exp);
                throw exp;
            }
        }
        private static void ServiceDownload(ref UpdateDetailInfo updateDetails)
        {
            try
            {
                log.Info("ServiceDownload Started ----------------------- ");
                updateDetails.ProgressBar = new ProgressBar();// ProgressBarReport("Application Download From Server", new ProgressBar(), 0, 100, 1);
                updateDetails.ProgressBar.Report(0.01);
                // download updates at hot folder path
                string hotFolder = Utility.DBAction.GetHFPath(Utility.Config.SubStoreId);
                string pathDownload = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");
                // Delete directory if exist
                //string iMixonlineFolder = hotFolder + "i-Mix-online";
                //if (Directory.Exists(iMixonlineFolder))
                //{
                //    DeleteDirectory(iMixonlineFolder);
                //}
                //
                string currentVersion = Utility.DBAction.CurrentRegistryVersion();
                //ProgressBarReport pr = updateDetails.ProgressBarReport;
                ProgressBar p = updateDetails.ProgressBar;
                VersonViewModel[] versions = Utility.DBAction.VerifyUpdatesAtServer();
                foreach (VersonViewModel version in versions.OrderBy(x => x.ReleaseDate))
                {
                    log.Info("Application download From server started for version : " + version.VersionName);
                    VersionInfo ver = new VersionInfo();
                    version.IPAddress = Utility.MachineIdentification.GetComputer_LanIP();
                    version.MACAddress = Utility.MachineIdentification.GetMACAddress();
                    updateDetails.ProgressBar.Report(.1);
                    ProgressBar.RunProgressBarToEnd(p, 1, 95, TimeSpan.FromSeconds(15));
                    //string zipFilePath = Utility.FileAction.DownlaodUpdates(hotFolder, version.VersionName, currentVersion);
                    if (Utility.CommonAction.IsDatabaseServer == 1)
                    {
                        Utility.CommonAction.ServerMachineZipPath = Utility.FileAction.DownlaodUpdates(hotFolder, version.VersionName, currentVersion);
                    }
                    else
                    {
                        Utility.CommonAction.ServerMachineZipPath = Utility.FileAction.DownlaodUpdates(pathDownload, version.VersionName, currentVersion);
                    }
                    //@"E:\DigiImages\Update_Version_ImixVersion6312.zip";
                    if (string.IsNullOrEmpty(Utility.CommonAction.ServerMachineZipPath))                    
                        throw (new Exception("The zip file path is empty. There was probably some problem downloading the zip file from the server."));
                    ProgressBar.RunProgressBarToEnd(p, 1, 98, TimeSpan.FromMilliseconds(1000));
                    log.Info("Application downloaded at : " + Utility.CommonAction.ServerMachineZipPath);
                    Utility.DBAction.UpdateStatusLocal(version, Utility.CommonAction.ServerMachineZipPath);
                    ver.VerisonId = version.ApplicationVersionID;
                    ver.VersionName = version.VersionName;
                    ver.Major = version.Major;
                    ver.Minor = version.Minor;
                    ver.Revision = version.Revision;
                    ver.Build = version.BuildNumber;
                    ver.HotFolderPath = Utility.CommonAction.ServerMachineZipPath;
                    updateDetails.Versions.Add(ver);
                    ProgressBar.AbortProgressBarRunToEnd(100);
                }
                result = 0;
                log.Info("ServiceDownload Completed ----------------------- ");
            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("DownloadUpdates.ServiceDownload: ", exp);
                throw exp;
            }
        }

        private static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                //Delete all files from the Directory
                foreach (string file in Directory.GetFiles(path))
                {
                    File.Delete(file);
                }
                //Delete all child Directories
                foreach (string directory in Directory.GetDirectories(path))
                {
                    DeleteDirectory(directory);
                }
                //Delete a Directory
                Directory.Delete(path);
            }
        }
    }
}
