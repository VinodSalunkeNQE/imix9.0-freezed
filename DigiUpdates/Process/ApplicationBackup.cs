﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.IO;
using DigiUpdates.Model;
namespace DigiUpdates.Process
{
    public class ApplicationBackup : IProcess
    {
        static int result = 0;
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public int Apply(int option, ref UpdateDetailInfo updates)
        {
            try
            {
                log.Info("Application  Backup started");                
                Application(ref updates);
                Console.WriteLine("Application backup Completed...");
                log.Info("Application Backup Completed");
                if (option == 2)
                {
                    if (Utility.CommonAction.IsDatabaseServer == 1)
                    {
                        //Console.WriteLine("Database backup started...");
                        log.Info("Database Backup started");
                        DataBase(ref updates);
                        log.Info("Database Backup completed");
                        Console.WriteLine("Database backup Completed...");
                    }
                }
            }
            catch (Exception exp)
            {
                log.Error("ApplicationBackup.Apply: ", exp);
                updates.UpdateException += " ApplicationBackup.Apply:"+ exp.Message;
            }
            return result;

        }
        public static string BackupPath(string name)
        {
            string versionName = Utility.DBAction.CurrentRegistryVersion();
            DirectoryInfo di = new DirectoryInfo(Utility.FileAction.ExecutablePath);
            string destinationFolderName = Path.Combine(di.Parent.FullName, name + "_" + versionName);
            if (!Directory.Exists(destinationFolderName))
                Directory.CreateDirectory(destinationFolderName);
            return destinationFolderName;
        }
        private static void Application(ref UpdateDetailInfo updates)
        {
            updates.ProgressBar = new ProgressBar();
            try
            {
                string sourceLocation = @"C:\Program Files (x86)\iMix\";
                Console.WriteLine("Application backup started..."); 
                //updates.ProgressBarReport = new ProgressBarReport("Applcation Backup", new ProgressBar(true), 0, 100, 1);
                updates.ProgressBar.Report(.01);
                string destinationFolderName = BackupPath("DigiAppBackup");                
                updates.ApplicationBackupPath = destinationFolderName;
                updates.OldVersion = Utility.DBAction.CurrentRegistryVersion();               
                updates.ProgressBar.Report(.05);
                Utility.FileAction.updates = updates;
                //result = Utility.FileAction.BackupRestoreFiles(Utility.FileAction.ExecutablePath, destinationFolderName);
                result = Utility.FileAction.BackupRestoreFiles(sourceLocation, destinationFolderName);
            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("ApplicationBackup.Application: ", exp);
                updates.UpdateException += " ApplicationBackup.Application:" + exp.Message;
            }
            //updates.ProgressBar.Dispose();
        }
        private static void DataBase(ref UpdateDetailInfo updates)
        {
            try
            {
                Console.WriteLine("Database backup started...");
                //updates.ProgressBarReport = new ProgressBarReport("Database Backup", new ProgressBar(true), 0, 100, 1);
                updates.ProgressBar = new ProgressBar();
                updates.ProgressBar.Report(.01);
                string backupPath = Utility.DBAction.GetHFPath(Utility.Config.SubStoreId) + "\\DigiDatabaseBackup_" + Utility.DBAction.CurrentRegistryVersion();
                if (!Directory.Exists(backupPath))
                    Directory.CreateDirectory(backupPath);
                
                updates.ProgressBar.Report(.03);
                Utility.DBAction.updates = updates;
                string path = Utility.DBAction.BackupDatabase();
                if (!string.IsNullOrEmpty(path))
                {
                    updates.DatabaseBackupPath = path;
                    result = 0;
                }
                else
                    result = -2;
            }
            catch (Exception exp)
            {
                result = -2;
                log.Error("ApplicationBackup.DataBase: ", exp);
                updates.UpdateException += " ApplicationBackup.DataBase:" + exp.Message;
            }
        }

    }
}
