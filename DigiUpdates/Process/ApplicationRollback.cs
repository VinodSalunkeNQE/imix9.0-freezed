﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiUpdates.Model;
namespace DigiUpdates.Process
{
    public class ApplicationRollback:IProcess
    {
        static int result = 0;
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public int Apply(int option, ref UpdateDetailInfo updateDetails)
        {
            try
            {
                Application(ref updateDetails);
                if (option == 2)
                {
                    DataBase(ref updateDetails);
                }
            }
            catch (Exception exp)
            {
                log.Error(exp);
                updateDetails.UpdateException += " ApplicationRollback.Apply:" + exp.Message;
            }
            return result;

        }
        private static void Application(ref UpdateDetailInfo updateDetails)
        {
            try
            {
                //rollback file system roll back.

            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("ApplicationRollback.Application: ", exp);
                updateDetails.UpdateException += " ApplicationRollback.Application:" + exp.Message;
            }
        }
        private static void DataBase(ref UpdateDetailInfo updateDetails)
        {
            try
            {
                //restore the backedupo database

            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("ApplicationRollback.DataBase: ", exp);
                updateDetails.UpdateException += " ApplicationRollback.Application:" + exp.Message;
            }
        }
    }
}
