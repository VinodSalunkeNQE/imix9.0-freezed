﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiUpdates.Model;
namespace DigiUpdates.Process
{
    public class ApplyUpdates : IProcess
    {
        static int result = 0;
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public int Apply(int option, ref UpdateDetailInfo updateDetails)
        {
            try
            {
                ApplicationUpdate();
                if (option == 2)
                {
                    DataBaseUpdate();
                }
            }
            catch (Exception exp)
            {
                log.Error("ApplyUpdates.Apply: ", exp);
                throw exp;
            }
            return result;

        }
        private static void ApplicationUpdate()
        {
            try
            {
                // paste the files from local path

            }
            catch (Exception exp)
            {
                result = -1;
                log.Error("ApplyUpdates.ApplicationUpdate: ", exp);
                throw exp;
            }
        }
        private static void DataBaseUpdate()
        {
            try
            {
                //execute db patch files

            }
            catch (Exception exp)
            {
                result += -1;
                log.Error("ApplyUpdates.DataBaseUpdate: ", exp);
                throw exp;
            }
        }
    }
}
