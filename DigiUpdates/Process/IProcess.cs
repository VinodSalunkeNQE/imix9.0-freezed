﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiUpdates.Model;
namespace DigiUpdates.Process
{
    interface IProcess
    {
        int Apply(int option, ref UpdateDetailInfo updates);
    }
}
