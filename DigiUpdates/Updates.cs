﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.iMix.ViewModel;
using DigiUpdates.Model;
using System.Collections;
using System.Net;
using System.Management;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using DigiPhoto.DataLayer;
using System.Data.SqlClient;
using System.Data;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using System.IO;
using Ionic.Zip;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using System.ServiceProcess;

namespace DigiUpdates
{
    public class Updates
    {
        static string exception = string.Empty;
        static Hashtable htApp = new Hashtable();
        static Hashtable htWinService = new Hashtable();
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool Update(int option)
        {
            try
            {
                UpdateDetailInfo updateDetails = new UpdateDetailInfo();
                int result = 0;
                int rollback = 0;
                bool isSuccess = false;
                result = ProcessBackupCopyUpdates(option, ref updateDetails);
                if (result == 0)
                {
                    result += ProcessApplyUpdates(option, ref updateDetails);
                    rollback = RollbackUpdates(result, option, ref updateDetails);
                    int status = UpdateStatus(result, rollback, exception, ref updateDetails);
                    if (result == 0 && status > 0)
                    {
                        isSuccess = true;
                    }
                    else
                    {
                        isSuccess = false;
                        Console.WriteLine("There were some problems in applying updates, please refer the details: " + exception);
                        Console.Read();
                    }
                }
                else
                {
                    isSuccess = false;
                    Console.WriteLine("There are some problem in backup processing " + exception);
                    log.Error(exception);
                }
                return isSuccess;
            }
            catch (Exception exp)
            {
                return false;
                log.Error("Updates.Update: ", exp);
                exception = exp.Message;
            }
        }
        private static int ProcessBackupCopyUpdates(int option, ref UpdateDetailInfo updateDetails)
        {
            //backup Section Start
            Console.WriteLine("Backup Process started...");
            Process.ApplicationBackup appBackup = new Process.ApplicationBackup();
            int result = appBackup.Apply(option, ref updateDetails);
            if (result == 0)
                Console.WriteLine("Backup Process completed...");
            else if (result == -1)
                Console.WriteLine("Application backup failed...");
            else if (result == -2)
                Console.WriteLine("Database backup failed...");
            //backup Section End

            //Download Application Start
            Process.DownloadUpdates download = new Process.DownloadUpdates();
            //Console.WriteLine("Application download started...");
            result += download.Apply(option, ref updateDetails);
            if (result == 0)
                Console.WriteLine("Download completed...");
            else
                Console.WriteLine("Download failed...");
            //Download Application End

            return result;
        }
        private static int ProcessApplyUpdates(int option, ref UpdateDetailInfo updateDetails)
        {
            int result = 0;
            int DbUpdate = 0;
            Utility.CommonAction.IsResult = false;
            log.Info("ProcessApplyUpdates Started-------- :");
            Console.WriteLine("Applying application updates...");
            string hotFolder = Utility.DBAction.GetHFPath(Utility.Config.SubStoreId);
            //string hotFolder = Utility.CommonAction.ServerMachineZipPath;
            string pathDownloadLocal = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");
            //updateDetails.ProgressBarReport = new ProgressBarReport("Aplying application updates", new ProgressBar(), 0, 100, 1);                
            ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 95, TimeSpan.FromMilliseconds(3000));
            //result = Utility.FileAction.ExtractZip(item.LocalMachineZipPath, Utility.FileAction.ExecutablePath);
            string SourceLocation = string.Empty;
            if (Utility.CommonAction.IsDatabaseServer == 1)
            {
                result = Utility.FileAction.ExtractZip(Utility.CommonAction.ServerMachineZipPath, hotFolder);
                SourceLocation = hotFolder;

            }
            else
            {
                result = Utility.FileAction.ExtractZip(Utility.CommonAction.ServerMachineZipPath, pathDownloadLocal);
                SourceLocation = pathDownloadLocal;
            }
            ApplicationUpdate(SourceLocation);
            InstallFramework46();
            ProgressBar.AbortProgressBarRunToEnd(100);
            if (result == 0)
            {
                Console.WriteLine("Application update successful...");
                Utility.CommonAction.IsResult = true;
            if (option == 1)//#CV
                {
                    //Console.WriteLine("Aplying database updates...");
                    //Apply Database changes
                    //result += Utility.DBAction.ExecuteScript();
                }
            }
            else
                Console.WriteLine("Application update failed...");

            if (option == 2 && result == 0 && Utility.CommonAction.IsDatabaseServer == 1)
            {
                Console.WriteLine("Applying database updates...");
                ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 95, TimeSpan.FromMilliseconds(1000));
                //Apply Database changes
                result += Utility.DBAction.ExecuteScript();
                ProgressBar.AbortProgressBarRunToEnd(100);
                if (result == 0)
                {
                    DbUpdate = 0;
                    Console.WriteLine("Database update successful...");
                    Utility.CommonAction.IsResult = true;

                }
                else
                {
                    DbUpdate = -1;
                    Console.WriteLine("Database update failed...");
                }

            }

        //function to update or insert pos detail
        if (DbUpdate == 0)
        {
            Console.WriteLine("Updating POS details....");
            ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 95, TimeSpan.FromMilliseconds(500));
            bool sysstatus = RegisterSystem();
            ProgressBar.AbortProgressBarRunToEnd(100);
            if (sysstatus == true)
            {
                Console.WriteLine("Pos detail updated successfully...");
            }
            else
            {
                Console.WriteLine("There were some problems in updating the pos detail");
            }

        }
        log.Info("ProcessApplyUpdates End-------- :");
        return result;
        }

        private static void KillAllApps()
        {
            foreach (string item in htApp.Keys)
            {
                KillOpenedProcess(item);
            }
        }

        public static void KillOpenedProcess(string name)
        {
            foreach (System.Diagnostics.Process clsProcess in System.Diagnostics.Process.GetProcesses())
            {
                if (clsProcess.ProcessName.ToLower().Contains(name.ToLower()))
                {
                    clsProcess.Kill();
                }
            }
        }

        private static void CloseAllDigiInstances()
        {
            try
            {
                    LoadRunningApplications();
                    KillAllApps();
                    StopWinService();
            }
            catch (Exception exp)
            {
                log.Error("CloseAllDigiInstances: ", exp);
            }
        }

        private static void StopWinService()
        {
            foreach (string item in htWinService.Keys)
            {
                StopWindowService(item);
            }
        }
        private static void StopWindowService(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Running")
                {
                    myService.Stop();
                }
            }
            catch (Exception exp)
            {
                log.Error("StopWindowService: ", exp);
            }
        }
        private static void LoadRunningApplications()
        {
            string clsExe = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\CloseExecutables.xml";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(clsExe);
            XmlNodeList xndList = xdoc.GetElementsByTagName("Application");
            foreach (XmlNode item in xndList)
            {
                if (!htApp.ContainsKey(item.InnerText))
                    htApp.Add(item.InnerText, item.InnerText);
            }

            XmlNodeList xndList2 = xdoc.GetElementsByTagName("Service");
            foreach (XmlNode item in xndList2)
            {
                if (!htWinService.ContainsKey(item.InnerText))
                    htWinService.Add(item.InnerText, IsServiceRunning(item.InnerText));
            }
        }

        private static bool IsServiceRunning(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Running")
                    return true;
                else
                    return false;

            }
            catch (Exception exp)
            {
                log.Error("IsServiceRunning: ", exp);
                return false;
            }
        }
        private static void ApplicationUpdate(string SourceLocation)
        {
            try
            {
                CloseAllDigiInstances();
                log.Info("ApplicationUpdate  started--------------------------------: ");
                VersionInfo vInfo = new VersionInfo();
                string sourceLocationUnzip = SourceLocation;
                string filePathSource = sourceLocationUnzip + "\\i-Mix-online";
                string destinationLocation = @"C:\Program Files (x86)\iMix\";
                string sourceLocation = @"C:\Program Files (x86)\iMix\";
                string finalSourceLocation = sourceLocation + "i-Mix-online";
                string finalDestinationLocation = @"C:\Program Files (x86)\iMix\";

                log.Info("filePathSource  --------------------------------: " + filePathSource);
                log.Info("finalSourceLocation  --------------------------------: " + finalSourceLocation);
                foreach (string dirPath in Directory.GetDirectories(filePathSource, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(filePathSource, destinationLocation));
                }
                foreach (string newPath in Directory.GetFiles(filePathSource, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(filePathSource, destinationLocation), true);
                }

                if (Directory.Exists(finalSourceLocation))
                {
                    log.Info("Directory.Exists(finalSourceLocation)  --------------------------------: ");
                    foreach (string dirPath in Directory.GetDirectories(finalSourceLocation, "*", SearchOption.AllDirectories))
                    {
                        Directory.CreateDirectory(dirPath.Replace(finalSourceLocation, finalDestinationLocation));
                    }
                    foreach (string newPath in Directory.GetFiles(finalSourceLocation, "*.*", SearchOption.AllDirectories))
                    {
                        File.Copy(newPath, newPath.Replace(finalSourceLocation, finalDestinationLocation), true);
                    }
                }

                 log.Info("ApplicationUpdate  Completed--------------------------------: ");
            }
            catch (Exception exp)
            {
                log.Error("ApplyUpdates.ApplicationUpdate: ", exp);
                throw exp;
            }
        }

        private static void InstallFramework46()
        {
            try
            {
                log.Info("InstallFramework46  started--------------------------------: ");
                System.Diagnostics.Process myProcess = new System.Diagnostics.Process();
                string filePathSource = Utility.FileAction.ExecutablePath + "\\NDP46-KB3045557-x86-x64-AllOS-ENU.exe";
                //string fullPath = filePathSource.Replace(@"\\", @"\");
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.Verb = "runas";
                myProcess.StartInfo.Arguments = "/q /norestart /ChainingPackage ADMINDEPLOYMENT";
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.StartInfo.FileName = filePathSource;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                log.Info("InstallFramework46  End--------------------------------:" + filePathSource);
            }
            catch (Exception exp)
            {
                log.Error("InstallFramework.InstallFramework: ", exp);
                throw exp;
            }
        }

        private static int RollbackUpdates(int action, int option, ref UpdateDetailInfo updateDetails)
        {
            int result = 0;
            if (action < 0)
            {
                //updateDetails.ProgressBar = new ProgressBar();//("Application Rollback", new ProgressBar(), 1, 100, 1);
                string destinationLocation = @"C:\Program Files (x86)\iMix\";
                Console.WriteLine("Aplying application rollback...");
                ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 100, TimeSpan.FromMilliseconds(500));
                //result = Utility.FileAction.BackupRestoreFiles(updateDetails.ApplicationBackupPath, Utility.FileAction.ExecutablePath);
                result = Utility.FileAction.BackupRestoreFiles(updateDetails.ApplicationBackupPath, destinationLocation);
                ProgressBar.AbortProgressBarRunToEnd(100);
                if (result == 0)
                    Console.WriteLine("Application rollback successful...");
                else
                    Console.WriteLine("Application rollback failed...");
                if (option == 2)
                {
                    //updateDetails.ProgressBar = new ProgressBar();//ProgressBarReport("Application Rollback", new ProgressBar(), 1, 100, 1);
                    Console.WriteLine("Aplying database rollback...");
                    ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 100, TimeSpan.FromMilliseconds(500));
                    result += Utility.DBAction.RestoreDataBase(updateDetails.DatabaseBackupPath);
                    ProgressBar.AbortProgressBarRunToEnd(100);
                    if (result == 0)
                        Console.WriteLine("Database rollback successful...");
                    else
                        Console.WriteLine("Database rollback failed...");
                }
                Utility.FileAction.UpdateRegistry("InstallVersion", updateDetails.OldVersion);

            }
            return result;
        }
        private static String GetStatus(int result, int rollback)
        {
            if (result == 0 && rollback == 0)
                return "Success";
            if (result == -1)
                return "Faied in Application update";
            if (result == -2)
                return "Faied in Data update";
            if (result == -3)
                return "Faied in Application and Data update";
            if (rollback == -1)
                return "Faied in Application Rollback";
            if (rollback == -2)
                return "Faied Database Rollback";
            if (rollback == -3)
                return "Faied in Application and Database Rollback";
            return string.Empty;
        }
        private static int UpdateStatus(int result, int rollback, string exception, ref UpdateDetailInfo updateDetails)
        {
            try
            {
                Console.WriteLine("Updating Status...");
                ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 100, TimeSpan.FromMilliseconds(500));
                int output = 0;
                string IPAddress = Utility.MachineIdentification.GetComputer_LanIP();
                string MacAddress = Utility.MachineIdentification.GetMACAddress();
                string HostName = Utility.MachineIdentification.HostName;
                Hashtable htStoreInfo = new Hashtable();
                htStoreInfo = Utility.DBAction.GetStoreDetails();
                foreach (var item in updateDetails.Versions)
                {
                    VersonViewModel version = new VersonViewModel();
                    string versionName = item.Major + "." + item.Minor + "." + item.Revision + "." + item.Build;
                    version.IPAddress = IPAddress;
                    version.MACAddress = MacAddress;
                    version.VersionName = versionName;
                    version.Country = htStoreInfo.ContainsKey("Country") ? htStoreInfo["Country"].ToString() : "";
                    version.Store = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString() : "";
                    version.status = GetStatus(result, rollback);
                    version.SubStoreId = Utility.Config.SubStoreId;
                    version.Description = string.IsNullOrEmpty(updateDetails.UpdateException) ? "" : updateDetails.UpdateException;
                    version.User = "admin";
                    version.Password = "admin";
                    version.ApplicationVersionID = item.VerisonId;

                    output = Utility.DBAction.SaveVersionUpdateDetails(version, HostName);
                    //output = Utility.DBAction.UpdateServerStatus(version);
                    if (output > 0 && result == 0)
                    {
                        string newRegVer = item.Major + "." + item.Minor + "." + item.Revision + "." + item.Build;
                        Utility.FileAction.UpdateRegistry("InstallVersion", newRegVer);
                    }
                }
                ProgressBar.AbortProgressBarRunToEnd(100);
                Console.WriteLine("Update Status Completed...");
                return output;
            }
            catch (Exception exp)
            {
                log.Error("Updates.UpdateStatus: ", exp);
                exception = exp.Message;
                return 0;
            }
        }
        private static bool RegisterSystem()
        {
            bool output = false;
            try
            {
                /// string connnectionName = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                int Count = 0;
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemName = queryObj["Name"];
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.PosDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                ImixPOSDetail imixposdetail = new ImixPOSDetail();
                imixposdetail.SystemName = SystemName.ToString();
                imixposdetail.IPAddress = myIP;
                imixposdetail.MacAddress = mac;
                imixposdetail.SubStoreID = 0;
                imixposdetail.IsActive = true;
                imixposdetail.CreatedBy = "webusers";
                imixposdetail.ImixPOSDetailID = 0;
                imixposdetail.IsStart = false;
                imixposdetail.SyncCode = SyncCode;
                output = Utility.DBAction.InsertImixPosDetail(imixposdetail);
                //ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
                //Count = svcPosInfoBusiness.InsertImixPosBusiness(imixposdetail,conn);
                return output;
            }
            catch (Exception ex)
            {
                output = false;
                return output;
            }
        }
    }
}
