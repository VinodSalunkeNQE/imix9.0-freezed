﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
//using Microsoft.SqlServer.Management.Utility;
using Microsoft.SqlServer.Management.Common;
using DigiPhoto.iMix.ViewModel;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Collections;
using log4net;
using DigiPhoto.IMIX.Model;
using DigiUpdates.Model;
using System.Threading;

namespace DigiUpdates.Utility
{

    class DBAction
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static UpdateDetailInfo updates { get; set; }

        static string _connstr = "";//ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        static string _databasename = "";//ConfigurationManager.AppSettings["DatabaseName"];
        static string _username = "";//ConfigurationManager.AppSettings["UserName"];
        static string _pass = "";//ConfigurationManager.AppSettings["UserPass"];
        static string _server = "";//ConfigurationManager.AppSettings["ServerName"];
        static DBAction()
        {
            fillConfigServerVariables();
        }
        
        private static void fillConfigServerVariables()
        {
            try
            {
                string appConString = string.Empty;
                _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                appConString = _connstr;
                string[] strVar = appConString.Split(';');
                Hashtable htCon = new Hashtable();
                if (strVar.Length != 0)
                {
                    for (int k = 0; k <= strVar.Length - 1; k++)
                    {
                        string[] tempK = strVar[k].Split('=');
                        if (tempK.Length != 0)
                        {
                            if (!htCon.ContainsKey(tempK[0]))
                            {
                                htCon.Add(tempK[0], tempK[1]);
                            }
                        }
                    }
                }

                _databasename = htCon.ContainsKey("Initial Catalog") ? htCon["Initial Catalog"].ToString() : "";
                _username = htCon.ContainsKey("User ID") ? htCon["User ID"].ToString() : "";
                _pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : "";
                _server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : "";
            }
            catch (Exception exp)
            {
                log.Error("DBAction.fillConfigServerVariables: ", exp);
            }
        }
        private static Server ServerCredentials()
        {
            ServerConnection connection = new ServerConnection(_server, _username, _pass);
            //To Avoid TimeOut Exception
            Server sqlServer = new Server(connection);
            sqlServer.ConnectionContext.StatementTimeout = 60 * 60;
            return sqlServer;
        }
        /// <summary>
        /// Function to backup the database
        /// </summary>
        /// <param name="databaseName">Database Name</param>
        /// <param name="userName">User Name</param>
        /// <param name="password">Password</param>
        /// <param name="serverName">Server Name</param>
        /// <param name="destinationPath">Destination Location to store the backup. Directory path.</param>
        /// <returns></returns>
        public static string BackupDatabase()
        {
            string localBackUppath = string.Empty;
            try
            {
                //Console.WriteLine("Backup is being executed...");
                ProgressBar p = ProgressBar.RunProgressBarToEnd(updates.ProgressBar, 1, 30, TimeSpan.FromMilliseconds(1000));
                //Define a Backup object variable.
                Backup sqlBackup = new Backup();
                //Specify the type of backup, the description, the name, and the database to be backed up.
                sqlBackup.Action = BackupActionType.Database;
                sqlBackup.BackupSetDescription = "BackUp of:" + _databasename + "on" + DateTime.Now.ToShortDateString();
                sqlBackup.BackupSetName = "FullBackUp";
                sqlBackup.Database = _databasename;
                string bakfilename = _databasename + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".bak";
                //Define Server connection
                Server sqlServer = ServerCredentials();
                localBackUppath = sqlServer.BackupDirectory + "\\" + bakfilename;

                //Declare a BackupDeviceItem
                //BackupDeviceItem deviceItem = new BackupDeviceItem(destinationPath + "FullBackUp.bak", DeviceType.File);
                BackupDeviceItem deviceItem = new BackupDeviceItem(localBackUppath, DeviceType.File);
                Database db = sqlServer.Databases[_databasename];
                sqlBackup.Initialize = true;
                sqlBackup.Checksum = true;
                sqlBackup.ContinueAfterError = true;

                //Add the device to the Backup object.
                sqlBackup.Devices.Add(deviceItem);
                //Set the Incremental property to False to specify that this is a full database backup.
                sqlBackup.Incremental = false;
                sqlBackup.ExpirationDate = DateTime.Now.AddDays(3);
                //Specify that the log must be truncated after the backup is complete.
                sqlBackup.LogTruncation = BackupTruncateLogType.Truncate;
                //sqlBackup.CompressionOption = BackupCompressionOptions.On;
                sqlBackup.FormatMedia = false;
                ProgressBar.RunProgressBarToEnd(p, 1, 50, TimeSpan.FromMilliseconds(1000));
                ProgressBar.RunProgressBarToEnd(updates.ProgressBar, 1, 100, TimeSpan.FromMilliseconds(1000));                
                //Run SqlBackup to perform the full database backup on the instance of SQL Server.
                sqlBackup.SqlBackup(sqlServer);
                //Remove the backup device from the Backup object.
                sqlBackup.Devices.Remove(deviceItem);
                //File.Copy(localBackUppath, backupfullpath, true);
                ProgressBar.AbortProgressBarRunToEnd(100);
                Console.WriteLine("Backup successfully taken at: " + localBackUppath);
                //Console.WriteLine("Press any key to exit...");
                //Console.ReadLine();
            }
            catch (Exception exp)
            {
                localBackUppath = string.Empty;
                log.Error("DBAction.BackupDatabase: ", exp);
                Console.WriteLine("Database backup operation could not be executed. Error: " + exp.Message);
            }
            return localBackUppath;
        }

        /// <summary>
        /// Function to restore the backedup database
        /// </summary>
        /// <param name="dataBaseName">Database Name</param>
        /// <param name="userName">User Name</param>
        /// <param name="password">Password</param>
        /// <param name="serverName">Server Name</param>
        /// <param name="sourceBackupPath">Location where database backup is stored. Url for *.bak file.</param>
        /// <returns></returns>
        public static int RestoreDataBase(string backUpPath)
        {
            int result = 0;
            try
            {
                ProgressBar.RunProgressBarToEnd(updates.ProgressBar, 1, 30, TimeSpan.FromMilliseconds(1000));
                Server sqlServer = ServerCredentials();
                //string lastFolderName = Path.GetFileName(Path.GetDirectoryName(backUpPath));
                //var directory = new DirectoryInfo(backUpPath);
                //var sqlBakFile = (from file in directory.GetFiles()
                //                  orderby file.LastWriteTime descending
                //                  select file).First();

                string localBackUppath = sqlServer.BackupDirectory + "\\" + System.IO.Path.GetFileName(backUpPath);
                //string localBackUppath = sqlServer.BackupDirectory + "\\" + sqlBakFile;
                //File.Copy(backUpPath, localBackUppath, true);
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    con.Open();

                    string UseMaster = "USE master";
                    SqlCommand UseMasterCommand = new SqlCommand(UseMaster, con);
                    UseMasterCommand.ExecuteNonQuery();

                    string Alter1 = @"ALTER DATABASE [" + _databasename + "] SET Single_User WITH Rollback Immediate";
                    SqlCommand Alter1Cmd = new SqlCommand(Alter1, con);
                    Alter1Cmd.ExecuteNonQuery();
                    ProgressBar.AbortProgressBarRunToEnd(30);
                    ProgressBar.RunProgressBarToEnd(updates.ProgressBar, 1, 100, TimeSpan.FromMilliseconds(1000));
                    string Restore = @"RESTORE DATABASE [" + _databasename + "] FROM DISK = N'" + localBackUppath + @"' WITH  FILE = 1,  NOUNLOAD,  STATS = 10";
                    SqlCommand RestoreCmd = new SqlCommand(Restore, con);
                    RestoreCmd.CommandTimeout = 600;
                    RestoreCmd.ExecuteNonQuery();

                    string Alter2 = @"ALTER DATABASE [" + _databasename + "] SET Multi_User";
                    SqlCommand Alter2Cmd = new SqlCommand(Alter2, con);
                    RestoreCmd.CommandTimeout = 600;
                    Alter2Cmd.ExecuteNonQuery();

                    result = 0;
                    ProgressBar.AbortProgressBarRunToEnd(100);
                }
            }
            catch (Exception ex)
            {
                result = -2;
                log.Error("DBAction.RestoreDataBase: ", ex);
                ProgressBar.AbortProgressBarRunToEnd(100);
                Console.WriteLine("Database restore operation could not be executed. Error: " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Function to update the local database for successful update download. 
        /// </summary>
        /// <param name="viewModel">VersonViewModel</param>
        /// <param name="downloadPath">File location where the update has been downloaded and stored. Probably the hotfolder path.</param>
        /// <returns>true if successfully updated</returns>
        public static bool UpdateStatusLocal(VersonViewModel viewModel, string downloadPath)
        {
            bool isDBUpdated;
            try
            {
                if (viewModel != null)
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand cmd = new SqlCommand("uspUpdateStatusLocal", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@VersionName", SqlDbType.VarChar).Value = viewModel.VersionName;
                            cmd.Parameters.Add("@MACAddress", SqlDbType.VarChar).Value = viewModel.MACAddress;
                            cmd.Parameters.Add("@IPAddress", SqlDbType.VarChar).Value = viewModel.IPAddress;
                            cmd.Parameters.Add("@Major", SqlDbType.Int).Value = Convert.ToUInt32(viewModel.Major);
                            cmd.Parameters.Add("@Minor", SqlDbType.Int).Value = Convert.ToUInt32(viewModel.Minor);
                            cmd.Parameters.Add("@Revision", SqlDbType.Int).Value = Convert.ToUInt32(viewModel.Revision);
                            cmd.Parameters.Add("@BuildNumber", SqlDbType.Int).Value = Convert.ToUInt32(viewModel.BuildNumber);
                            cmd.Parameters.Add("@ReleaseDate", SqlDbType.DateTime).Value = Convert.ToDateTime(viewModel.ReleaseDate);
                            cmd.Parameters.Add("@Path", SqlDbType.VarChar).Value = downloadPath.Replace(@"\", @"\\");//new download path
                            con.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                isDBUpdated = true;
            }
            catch (Exception exp)
            {
                log.Error("DBAction.UpdateStatusLocal: ", exp);
                isDBUpdated = false;
            }
            return isDBUpdated;
        }
        public static int SaveVersionUpdateDetails(VersonViewModel version, string machineName)
        {
            int result = -5;
            try
            {
                if (version != null)
                {
                    using (SqlConnection _con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand _cmd = new SqlCommand("uspSaveVersionUpdateDetails", _con))
                        {
                            _cmd.CommandType = CommandType.StoredProcedure;
                            _cmd.Parameters.Add("@ParamApplicationVersionID", SqlDbType.Int).Value = version.ApplicationVersionID;
                            _cmd.Parameters.Add("@ParamMACAddress", SqlDbType.VarChar).Value = version.MACAddress;
                            _cmd.Parameters.Add("@ParamMachineName", SqlDbType.VarChar).Value = machineName;
                            _cmd.Parameters.Add("@ParamIPAddress", SqlDbType.VarChar).Value = version.IPAddress;
                            _cmd.Parameters.Add("@ParamCountry", SqlDbType.VarChar).Value = version.Country;
                            _cmd.Parameters.Add("@ParamStore", SqlDbType.VarChar).Value = version.Store;
                            _cmd.Parameters.Add("@ParamSubStoreID", SqlDbType.VarChar).Value = version.SubStoreId;
                            _cmd.Parameters.Add("@ParamStatus", SqlDbType.VarChar).Value = version.status;
                            _cmd.Parameters.Add("@ParamDescription", SqlDbType.VarChar).Value = version.Description;
                            _con.Open();
                            _cmd.ExecuteNonQuery();
                            result = 1;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                log.Error("DBAction.UpdateStatusLocal: ", exp);
                return result;

            }

            return result;
        }
        /// <summary>
        /// Function to fetch the WCF Update Service Path
        /// </summary>
        /// <returns>Update Service Path</returns>
        public static string GetUpdateServicePath()
        {
            string servicePath = string.Empty;
            int subStoreId = Config.SubStoreId;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.uspGetVersionUpdateServicePath", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                servicePath = Convert.ToString(rdr["ServicePath"]);
                            }
                        }
                    }
                }
            }

            return servicePath;
        }
        /// <summary>
        /// Function to fetch the Hot Folder Path
        /// </summary>
        /// <returns>Hot folder path</returns>
        public static string GetHFPath(int subStoreId)
        {
            string hfPath = string.Empty;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[DG_GetConfigData]", con))
                {
                    cmd.Parameters.AddWithValue("@SubStoreID", subStoreId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                hfPath = Convert.ToString(rdr["DG_Hot_Folder_Path"]);
                            }
                        }
                    }
                }
            }
            //hfPath = hfPath.Replace("\\\\", "\\");
            return hfPath;
        }
        public static Hashtable GetStoreDetails()
        {
            Hashtable htTemp = new Hashtable();
            try
            {
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.uspGetStoreDetails", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    string storeName = Convert.ToString(rdr["DG_Store_Name"]);
                                    string country = Convert.ToString(rdr["Country"]);
                                    htTemp.Add("Store", storeName);
                                    htTemp.Add("Country", country);
                                    break;//There should always be 1 store or row in table. Just incase more rows are return then it should exit.
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                log.Error("DBAction.GetStoreDetails: ", exp);
                htTemp = null;
            }
            return htTemp;
        }
        /// <summary>
        /// Function to fetch the latest available update
        /// </summary>
        /// <param name="currRegistry">Current registry version</param>
        /// <returns></returns>
        public static List<VersonViewModel> GetUpdateVersionDetails(string currRegistry)
        {
            List<VersonViewModel> vmTemps = new List<VersonViewModel>();
            try
            {
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.uspGetLocalUpdates", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Version", SqlDbType.VarChar).Value = currRegistry;
                        con.Open();
                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    VersonViewModel vmTemp = new VersonViewModel();
                                    vmTemp.VersionName = Convert.ToString(rdr["VersionName"]);
                                    vmTemp.MACAddress = Convert.ToString(rdr["MACAddress"]);
                                    vmTemp.IPAddress = Convert.ToString(rdr["IPAddress"]);
                                    vmTemp.Major = Convert.ToInt32(rdr["Major"]);
                                    vmTemp.Minor = Convert.ToInt32(rdr["Minor"]);
                                    vmTemp.Revision = Convert.ToInt32(rdr["Revision"]);
                                    vmTemp.BuildNumber = Convert.ToInt32(rdr["BuildNumber"]);
                                    vmTemp.ReleaseDate = Convert.ToDateTime(rdr["ReleaseDate"]);
                                    vmTemp.Path = Convert.ToString(rdr["Path"]);
                                    vmTemps.Add(vmTemp);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                log.Error("DBAction.GetUpdateVersionDetails: ", exp);
                vmTemps = null;
            }
            return vmTemps;
        }
        /// <summary>
        /// Function to update the status at server using WCF service.
        /// </summary>
        /// <param name="viewModel">VersonViewModel</param>
        /// <returns>-5 if update failed</returns>
        public static int UpdateServerStatus(VersonViewModel viewModel)
        {
            int result = -5;
            try
            {
                string epAdd = GetUpdateServicePath();
                VersionServiceClient client = new VersionServiceClient("BasicHttpBinding_IVersionService", epAdd);
                result = client.UpdateStatus(viewModel);
            }
            catch (Exception exp)
            {
                log.Error("DBAction.UpdateServerStatus: ", exp);
                result = -5;
            }
            return result;
        }

        /// <summary>
        /// Reads the script content from .sql file and execute on SQl Server
        /// </summary>
        /// <param name="scriptFilePath">.sql Script file path</param>
        /// <returns>Operation status <c>true: Success</c><c>false: Failed</c></returns>
        public static int ExecuteScript()
        {
            int result = 0;
            string currentScript = string.Empty;
            try
            {
                //string hotFolder = Utility.DBAction.GetHFPath(Utility.Config.SubStoreId);
                //string filePathSource = hotFolder + "i-Mix-online";
                string filePathSource = Utility.FileAction.ExecutablePath;
                FileInfo[] scriptFiles = new DirectoryInfo(filePathSource).EnumerateFiles("*.sql").ToArray();
                if (null != scriptFiles)
                {
                    foreach (FileInfo item in scriptFiles)
                    {
                        currentScript = item.Name;
                        using (SqlConnection connection = new SqlConnection(_connstr))
                        {
                            connection.Open();
                            Server server = new Server(new ServerConnection(connection));
                            string script = File.ReadAllText(Path.Combine(item.DirectoryName, item.Name));
                            server.ConnectionContext.ExecuteNonQuery(script);
                            log.Info("DBAction.ExecuteScript: Execution Sucess for script : " + currentScript);
                        }
                    }
                }
                result = 0;
                return result;
            }
            catch (Exception exp)
            {
                log.Error("DBAction.ExecuteScript: Execution failed for script : " + currentScript, exp);
                result = -2;
            }
            return result;
        }
        public static VersonViewModel[] VerifyUpdatesAtServer()
        {
            Hashtable htStoreInfo = GetStoreDetails();
            string ipaddr = MachineIdentification.GetComputer_LanIP();
            string macaddr = MachineIdentification.GetMACAddress();
            string curReg = CurrentRegistryVersion();
            VersonViewModel viewModel = new VersonViewModel();
            viewModel.Country = htStoreInfo.ContainsKey("Country") ? htStoreInfo["Country"].ToString() : "";
            viewModel.Store = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString() : "";
            viewModel.Description = "Test description";
            viewModel.MACAddress = macaddr;
            viewModel.IPAddress = ipaddr;
            viewModel.Location = "";
            viewModel.SubStore = "";
            viewModel.User = "admin";//as per the wcf service web.config
            viewModel.Password = "admin";//as per the wcf service web.config
            viewModel.Version = curReg;
            VersonViewModel[] lstTemp = VerifyUpdateService(viewModel);
            return lstTemp;
        }
        /// <summary>
        /// Get all the updates available for the given store
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private static VersonViewModel[] VerifyUpdateService(VersonViewModel viewModel)
        {
            try
            {
                string epAdd = GetUpdateServicePath();
                VersionServiceClient client = new VersionServiceClient("BasicHttpBinding_IVersionService", epAdd);
                VersonViewModel[] lstTemp = client.CheckAvailableUpdates(viewModel);
                return lstTemp;
            }
            catch (Exception exp)
            {
                log.Error("DBAction.VerifyUpdateService: ", exp);
                return null;
            }
        }
        /// <summary>
        /// Get the current software registry version
        /// </summary>
        /// <returns></returns>
        public static string CurrentRegistryVersion()
        {
            ModifyRegistry mr = new ModifyRegistry();
            string currRegistry = mr.Read("InstallVersion");
            return currRegistry;
        }
        /// <summary>
        /// insert update imix pos detail
        /// </summary>
        /// <param name="imixposdetail"></param>
        public static bool InsertImixPosDetail(ImixPOSDetail imixposdetail)
        {
            bool isDBUpdated;
            try
            {
                using (SqlConnection cn = new SqlConnection(_connstr))
                {
                    SqlCommand cmd = new SqlCommand("USP_INSERTUPDATEIMIXPOSDETAIL", cn);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@iMixPOSDetailID", imixposdetail.ImixPOSDetailID);
                    cmd.Parameters.AddWithValue("@SystemName", imixposdetail.SystemName);
                    cmd.Parameters.AddWithValue("@IPAddress", imixposdetail.IPAddress);
                    cmd.Parameters.AddWithValue("@MacAddress", imixposdetail.MacAddress);
                    cmd.Parameters.AddWithValue("@SubStoreID", imixposdetail.SubStoreID);
                    cmd.Parameters.AddWithValue("@IsActive", imixposdetail.IsActive);
                    cmd.Parameters.AddWithValue("@CreatedBy", imixposdetail.CreatedBy);
                    cmd.Parameters.AddWithValue("@IsStart", imixposdetail.IsStart);
                    cmd.Parameters.AddWithValue("@SyncCode", imixposdetail.SyncCode);
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                isDBUpdated = true;
            }
            catch (Exception exp)
            {
                log.Error("DBAction.UpdateStatusLocal: ", exp);
                isDBUpdated = false;
            }
            return isDBUpdated;
        }
    }

}
