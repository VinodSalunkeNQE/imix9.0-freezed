﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using log4net;
namespace DigiUpdates.Utility
{
    public class MachineIdentification
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string HostName { get; set; }
        /// <summary>
        /// Get computer LAN address like 192.168.1.3
        /// </summary>
        /// <returns></returns>
        public static string GetComputer_LanIP()
        {
            try
            {
                HostName = System.Net.Dns.GetHostName();

                IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(HostName);

                foreach (IPAddress ipAddress in ipEntry.AddressList)
                {
                    if (ipAddress.AddressFamily.ToString() == "InterNetwork")
                    {
                        return ipAddress.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                log.Error("MachineIdentification.GetComputer_LanIP: ", exp);
                return string.Empty;
            }
            return string.Empty; ;
        }
        /// <summary>
        /// Get local system machine address (MAC address)
        /// </summary>
        /// <returns></returns>
        public static string GetMACAddress()
        {

            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                String sMacAddress = string.Empty;
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress == String.Empty)// only return MAC Address from first card  
                    {
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        sMacAddress = adapter.GetPhysicalAddress().ToString();
                    }
                }
                return sMacAddress;
            }
            catch (Exception exp)
            {
                log.Error("MachineIdentification.GetMACAddress: ", exp);
                return string.Empty;
            }

        }
    }
}
