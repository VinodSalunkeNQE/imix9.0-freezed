﻿using DigiPhoto.iMix.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using System.Security.Principal;
using System.Security.AccessControl;
using Ionic.Zip;
using System.Collections;
using DigiUpdates.Model;
using System.Threading;

namespace DigiUpdates.Utility
{
    public class FileAction
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static UpdateDetailInfo updates { get; set; }

        public static string ExecutablePath
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }
        /// <summary>
        /// Function to copy files and directories recursively from source to destination.
        /// Same function can be used to backup and restore the files by interchanging the source and destination paths.
        /// </summary>
        /// <param name="SourceDir">Valid directory path</param>
        /// <param name="DestinationDir">Valid directory path</param>
        /// <returns></returns>
        public static int BackupRestoreFiles(string SourceDir, string DestinationDir)
        {
            try
            {
                log.Info("Application BackupRestoreFiles Started- SourceDir : " + SourceDir + "DestinationDir :" + DestinationDir);
                updates.ProgressBar.Report(.1);
                //SetPermissions(DestinationDir);
                List<string> excludedFiles = LoadExcludedFiles();
                //int i = Updates.ProgressBarReport.ProgressBar.Current;                                
                ProgressBar.RunProgressBarToEnd(updates.ProgressBar, 1, 95, TimeSpan.FromMilliseconds(1300));
                int dirsCopied = DirectoryCopy(SourceDir, DestinationDir, excludedFiles);
                //ProgressBar.AbortProgressBarRunToEnd(thProgressCopy, Updates.ProgressBarReport.ProgressBar, ref i, 100);
                ProgressBar.AbortProgressBarRunToEnd(100);
                log.Info("Application BackupRestoreFiles Completed :");
                return (dirsCopied);
            }
            catch (Exception exp)
            {
                log.Error("FileAction.BackupRestoreFiles: ", exp);
                return -1;
            }           
        }

        /// <summary>
        /// Function to copy directories
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        private static int DirectoryCopy(string sourceDirName, string destDirName, List<string> excludedFiles)
        {
            bool copySubDirs = true;
            int result = 0;
            try
            {
                log.Info("DirectoryCopy SourceDir Started : " + sourceDirName + "destDirName :" + destDirName);
                // Get the subdirectories for the specified directory.
                DirectoryInfo dir = new DirectoryInfo(sourceDirName);

                // If the destination directory doesn't exist, create it. 
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }
                //SetPermissions(destDirName); 
                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                //since we will be using it for sub directory copy as well
                //double stepSize = ((double)(100 - Updates.ProgressBarReport.ProgressBar.Current)/(2*100))/ files.Length;
                //double reportValue = (double)Updates.ProgressBarReport.ProgressBar.Current / 100;
                foreach (var process in System.Diagnostics.Process.GetProcessesByName("DigiPhoto.DataLayer.dll"))
                {
                    process.Kill();
                }

                foreach (FileInfo file in files)
                {
                    if (!excludedFiles.Contains(file.Name))
                    {
                        string temppath = Path.Combine(destDirName, file.Name);
                        file.CopyTo(temppath, true);                        
                    }                    
                    //Updates.ProgressBarReport.ProgressBar.Report(reportValue+=stepSize);
                }

                
                // If copying subdirectories, copy them and their contents to new location. 
                if (copySubDirs)
                {
                    foreach (DirectoryInfo subdir in dir.GetDirectories())
                    {

                        string temppath = Path.Combine(destDirName, subdir.Name);
                        log.Warn(temppath);
                        DirectoryCopy(subdir.FullName, temppath, excludedFiles);
                    }
                   
                }
                log.Info("DirectoryCopy SourceDir Completed : " + sourceDirName + "destDirName :" + destDirName);
                result = 0;
            }
            catch (Exception exp)
            {
                log.Error("FileAction.DirectoryCopy: ", exp);
                result = -1;
            }
            return result;
        }

        private static List<string> LoadExcludedFiles()
        {
            string excludeFilePath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\exclude.xml";
            List<string> excludedFiles = new List<string>();
            if (File.Exists(excludeFilePath))
            {
                //string ExcludeFilePath = "exclude.xml";
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(excludeFilePath);

                XmlNodeList xNodeList = xdoc.GetElementsByTagName("fileName");

                if (xNodeList.Count != 0)
                {
                    for (int i = 0; i < xNodeList.Count; i++)
                    {
                        excludedFiles.Add(xNodeList[i].InnerText.ToString());
                    }
                }
            }
            return excludedFiles;
        }
        /// <summary>
        /// Zip file extraction
        /// </summary>
        /// <param name="ZipFilePath"></param>
        /// <param name="ZipExtractionPath"></param>
        /// <returns></returns>
        public static int ExtractZip(string ZipFilePath, string ZipExtractionPath)
        {
            try
            {
                log.Info("ExtractZip Started--------------------------------:" + ZipFilePath + ZipExtractionPath);
                Decompress(ZipFilePath, ZipExtractionPath);
                log.Info("ExtractZip End--------------------------------:" + ZipFilePath + ZipExtractionPath);
                return 0;
            }
            catch (Exception exp)
            {
                log.Error("FileAction.ExtractZip: ", exp);
                return -1;
            }
        }
        /// <summary>
        /// Extract the given zip file
        /// </summary>
        /// <param name="ZipFilePath">Source path for the ZIP file</param>
        /// <param name="ZipExtractionPath">Path to extract the zip file to a directory</param>
        //private static void Decompress(string ZipFilePath, string ZipExtractionPath)
        //{
        //    try
        //    {
        //        FileInfo fi = new FileInfo(ZipFilePath);
        //        using (FileStream inFile = fi.OpenRead())
        //        {

        //            string curFile = fi.FullName;
        //            ////using (FileStream outFile = new FileStream(ZipExtractionPath, FileMode.Create))
        //            SetPermissions(ZipExtractionPath);
        //            using (FileStream outFile = new FileStream(ZipExtractionPath, FileMode.Create))
        //            {
        //                using (GZipStream Decompress = new GZipStream(inFile, CompressionMode.Decompress))
        //                {
        //                    Decompress.CopyTo(outFile);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        log.Error("FileAction.Decompress: ", exp);
        //    }
        //}
        /// <summary>
        /// Extract the given zip file
        /// </summary>
        /// <param name="ZipFilePath">Source path for the ZIP file</param>
        /// <param name="ZipExtractionPath">Path to extract the zip file to a directory</param>
        private static void Decompress(string ZipFilePath, string ZipExtractionPath)
        {
            List<string> excludedFiles = LoadExcludedFiles();

            using (ZipFile zip1 = ZipFile.Read(ZipFilePath))
            {
                foreach (ZipEntry e in zip1)
                {
                    if (!excludedFiles.Contains(e.FileName))
                    {
                        try
                        {
                            e.Extract(ZipExtractionPath, ExtractExistingFileAction.OverwriteSilently);
                        }
                        catch (Exception ex)
                        {
                            log.Error("FileAction.Decompress: ", ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Copy update *.ZIP from a network or local directory
        /// </summary>
        /// <param name="SourceFilePath">Zip file location</param>
        /// <param name="DestinationDirPath">Local file storage path</param>
        public static string CopyUpdate(string SourceFilePath, string DestinationDirPath)
        {
            try
            {
                return CopyZip(SourceFilePath, DestinationDirPath);
            }
            catch (Exception exp)
            {
                log.Error("FileAction.CopyUpdate: ", exp);
                return string.Empty;
            }
        }
        /// <summary>
        /// Copy file
        /// </summary>
        /// <param name="SourceFilePath">source file path</param>
        /// <param name="DestinationDirPath">destination directory path.</param>
        private static string CopyZip(string SourceFilePath, string DestinationDirPath)
        {
            string fullPath = string.Empty;
            try
            {
                if (File.Exists(SourceFilePath))
                {
                    SetPermissions(DestinationDirPath);
                    if (!Directory.Exists(DestinationDirPath))
                    {
                        Directory.CreateDirectory(DestinationDirPath);
                    }
                    string fiName = Path.GetFileName(SourceFilePath);
                    fullPath = DestinationDirPath + "\\" + fiName;
                    File.Copy(SourceFilePath, fullPath, true);
                }
            }
            catch (Exception exp)
            {
                log.Error("FileAction.CopyZip: ", exp);
                fullPath = string.Empty;
            }
            return fullPath;
        }
        public static string DownlaodUpdates(string FileDownloadPath, string NewVersion, string curReg)
        {
            try
            {
                return DownloadUpdatesFromServer(FileDownloadPath, NewVersion, curReg);
            }
            catch (Exception exp)
            {
                log.Error("FileAction.DownlaodUpdates: ", exp);
                return string.Empty;
            }
        }
        /// <summary>
        /// Download zip as a stream and re-save it as a zip
        /// </summary>
        /// <param name="FileDownloadPath">Directory path to download the update zip</param>
        /// <param name="NewVersion">New version number to be downloaded</param>
        /// <param name="curReg">Current system registry version</param>
        private static string DownloadUpdatesFromServer(string FileDownloadPath, string NewVersion, string curReg)
        {
            //Download file from WCF service
            string epAdd = DBAction.GetUpdateServicePath();
            Hashtable htStoreInfo = new Hashtable();
            htStoreInfo = Utility.DBAction.GetStoreDetails();
            VersionServiceClient client = new VersionServiceClient("BasicHttpBinding_IVersionService", epAdd);
            VersonViewModel viewModel = new VersonViewModel();
            viewModel.User = "admin";//as per the wcf service web.config
            viewModel.Password = "admin";//as per the wcf service web.config
            viewModel.Version = curReg;
            viewModel.Country = htStoreInfo.ContainsKey("Country") ? htStoreInfo["Country"].ToString() : "";
            viewModel.Store = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString() : "";
            viewModel.Location = "";
            viewModel.SubStore = "";
            string newFileName = Path.Combine(FileDownloadPath, "Update_Version_" + NewVersion + ".zip");
            try
            {
                Stream strm = client.DownloadUpdates(viewModel);                
                using (Stream destination = File.Create(newFileName))
                    Write(strm, destination);
            }
            catch (Exception exp)
            {
                log.Error("FileAction.DownloadUpdatesFromServer: ", exp);
                newFileName = string.Empty;
            }
            return newFileName;
        }
        public static void Write(Stream from, Stream to)
        {
            for (int a = from.ReadByte(); a != -1; a = from.ReadByte())
                to.WriteByte((byte)a);
        }
        /// <summary>
        /// Function to update the registry
        /// </summary>
        /// <param name="regKey">InstallVersion</param>
        /// <param name="regVal">#.#.#.#</param>
        /// <returns></returns>
        public static bool UpdateRegistry(string regKey, string regVal)
        {
            ModifyRegistry mr = new ModifyRegistry();
            bool success = mr.Write(regKey, regVal);
            return success;
        }

        private static void SetPermissions(string dirPath)
        {
            DirectoryInfo info = new DirectoryInfo(dirPath);
            WindowsIdentity self = System.Security.Principal.WindowsIdentity.GetCurrent();
            DirectorySecurity ds = info.GetAccessControl();
            ds.AddAccessRule(new FileSystemAccessRule(self.Name,
            FileSystemRights.FullControl,
            InheritanceFlags.ObjectInherit |
            InheritanceFlags.ContainerInherit,
            PropagationFlags.None,
            AccessControlType.Allow));
            info.SetAccessControl(ds);
        }

    }

    public static class CommonAction
    {
        public static bool IsResult { get; set; }
        private static string myValue;
        private static string lValue;
        public static string ServerMachineZipPath
        {
            get { return myValue; }
            set { myValue = value; }
        }

        public static string localMachineZipPath
        {
            get { return lValue; }
            set { lValue = value; }
        }
        private static int isDatabaseServer;
        public static int IsDatabaseServer
        {
            get { return isDatabaseServer; }
            set { isDatabaseServer = value; }
        }
    }
}
