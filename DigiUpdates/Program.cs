﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Collections;
using System.Diagnostics;
using System.ServiceProcess;
using System.Xml;
using System.IO;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Management;
using System.Threading;
using DigiUpdates.Model;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace DigiUpdates
{
    public class Program
    {
        static Hashtable htApp = new Hashtable();
        static Hashtable htWinService = new Hashtable();
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string DataSource, InitialCatalog, UserID, Password = string.Empty;
        static string xmlData = string.Empty;
        static string FilePath = string.Empty;

        private const int MF_BYCOMMAND = 0x00000000;
        public const int SC_CLOSE = 0xF060;

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        // A delegate type to be used as the handler routine 
        // for SetConsoleCtrlHandler.
        delegate bool HandlerRoutine(CtrlTypes CtrlType);
        
        //public static event EventHandler WindowClosing
        //{
        //    add { _onWindowClosing += value; }
        //    remove { if (_onWindowClosing != null) _onWindowClosing -= value; }
        //}

        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            //if (_commandLineWindow == null)
            //    _commandLineWindow = new CommandLineWindow();
            //// Put your own handler here
            //if (ctrlType == CtrlTypes.CTRL_CLOSE_EVENT)
            //    _onWindowClosing(_commandLineWindow, null);

            return true;
        }

        public static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
                //int option = Convert.ToInt32(args[0]);
                 int option = 2; 
                Console.WriteLine("There are updates available for application, please wait while the changes are being applied...");
                log.Warn("Start: Update");
                
                if (CloseAllDigiInstances())
                {
                    string answer;
                    Console.Write("Is This Database Server ? (yes/no): ");
                    answer = Console.ReadLine();
                    if (answer.ToUpper().Equals("YES", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Utility.CommonAction.IsDatabaseServer = 1;
                    }
                    else if (answer.ToUpper().Equals("NO", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Utility.CommonAction.IsDatabaseServer = 0;
                    }

                    //Thread.Sleep(TimeSpan.FromSeconds(30));
                    CommandLineWindow.WindowClosing += WindowClosing;
                    //SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);
                    log.Info("option----------------" + option);
                    bool isSuccess = Updates.Update(option);
                    
                    //StartWinService();
                    if (isSuccess)
                    {
                        StartWinService();
                        Console.WriteLine("Installing Services...");
                        ProgressBar p = ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 95, TimeSpan.FromMilliseconds(500));
                        InstallPresoldServices();
                        InstallReportServices();
                        InstallCleanupService();
                        InstallWatermarkService();
                        log.Info("Service Installed Sucessfully: ");
                        RegisterBatFile();
                        bool Templatestatus = CopyItemTemplate();
                        ProgressBar.AbortProgressBarRunToEnd(100);
                        if (Templatestatus == true)
                        {
                            Console.WriteLine("item template directory updated successfully...");
                        }
                        else
                        {
                            Console.WriteLine("There were some problems in creating the item template folder.");
                        }
                        Console.WriteLine("Copying Video folder....");
                        ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 99, TimeSpan.FromMilliseconds(500));
                        bool VideoFolderstatus = CopyVideoFolder();
                        ProgressBar.AbortProgressBarRunToEnd(100);
                        if (VideoFolderstatus == true)
                        {
                            Console.WriteLine("Video Folder directory updated successfully...");
                        }
                        else
                        {
                            Console.WriteLine("There were some problems in creating the Video Folder.");
                        }
                        Console.WriteLine("Application updated sucessfully to version: i-Mix V - " + Utility.DBAction.CurrentRegistryVersion());
                        Console.WriteLine("Starting i-Mix application V - " + Utility.DBAction.CurrentRegistryVersion());
                        ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 99, TimeSpan.FromMilliseconds(200));
                        log.Info("About to start CreateShortcutLiveEdit---------- : ");
                        CreateShortcutLiveEdit("LiveVideoEditing", @"C:\Users\Public\Desktop\", @"C:\Program Files (x86)\iMix\LiveVideoEditing.exe");
                        CreateShortcutvideoprocessicon("VideoProcessingEngine", @"C:\Users\Public\Desktop\", @"C:\Program Files (x86)\iMix\VideoProcessingEngine.exe");
                        UpdateConfigFile();
                        if (Utility.CommonAction.IsResult)
                        {
                            string sourceLocation = @"C:\Program Files (x86)\iMix\";
                            string fSourceLocation = sourceLocation + "i-Mix-online";
                            if (Directory.Exists(fSourceLocation))
                            {
                                DeleteDirectory(fSourceLocation);
                            }
                            DeleteScriptFile();
                        }
                        // Launch i-Mix.
                        log.Info("About to launch Imix appication");
                        Thread thread = new Thread(new ThreadStart(LaunchImix));
                        thread.Start();
                        thread.Join();
                        ProgressBar.AbortProgressBarRunToEnd(100);
                        log.Info("Imix appication launch completed");
                    }
                    else
                    {
                        Console.Read();
                    }
                }
                else
                {
                    Console.WriteLine("Some related applications are running. Please close them and try again. Press any key to exit...");
                    Console.Read();
                }
                log.Info("End: Update");
            }
            catch (Exception exp)
            {
                log.Info("Catch InstallCleanupService :" + exp);
                log.Error("Main: ", exp);
            }
        }

        public static void LaunchImix()
        {
            string appPath = Utility.FileAction.ExecutablePath + "\\DigiPhoto.exe";
            log.Info("appPath----------------------------: " + appPath);
            Thread.Sleep(200000);
            System.Diagnostics.Process.Start(appPath);
        }
        public static void DeleteScriptFile()
        {
            try
            {
                string filePathSource = @"C:\Program Files (x86)\iMix\";
                FileInfo[] scriptFiles = new DirectoryInfo(filePathSource).EnumerateFiles("*.sql").ToArray();
                FileInfo[] FrameworkFile = new DirectoryInfo(filePathSource).EnumerateFiles("NDP46-KB3045560-Web.exe").ToArray();
                if (null != scriptFiles)
                {
                    foreach (FileInfo item in scriptFiles)
                    {
                        item.Delete();
                    }
                }
                if (null != FrameworkFile)
                {
                    foreach (FileInfo item in FrameworkFile)
                    {
                        item.Delete();
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Main: ", ex);
            }
        }

        private static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                //Delete all files from the Directory
                foreach (string file in Directory.GetFiles(path))
                {
                    File.Delete(file);
                }
                //Delete all child Directories
                foreach (string directory in Directory.GetDirectories(path))
                {
                    DeleteDirectory(directory);
                }
                //Delete a Directory
                Directory.Delete(path);
            }
        }

        public static void CreateShortcutLiveEdit(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);
            shortcut.Description = "";   // The description of the shortcut
            shortcut.IconLocation = @"C:\Program Files (x86)\iMix\LiveEdit.ico";           // The icon of the shortcut
            shortcut.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcut.Save();                                    // Save the shortcut
        }
        public static void CreateShortcutvideoprocessicon(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.Description = "";   // The description of the shortcut
            shortcut.IconLocation = @"C:\Program Files (x86)\iMix\videoprocessicon.ico";           // The icon of the shortcut
            shortcut.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
            shortcut.Save();                                    // Save the shortcut
        }

        private static void InstallPresoldServices()
        {
            var service = ServiceController.GetServices().Where(x => x.ServiceName == "DigiPreSoldService").FirstOrDefault();
            if (null == service)
            {
                string[] commandLineOptions = new string[1] { "/LogFile=install.log" };
                string exeFilename = @"C:\Program Files (x86)\iMix\PreSoldService.exe";
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Configuration.Install.AssemblyInstaller installer = new System.Configuration.Install.AssemblyInstaller(exeFilename, commandLineOptions);
                    installer.UseNewContext = true;
                    installer.Install(null);
                    installer.Commit(null);
                }
            }
        }

        private static void InstallReportServices()
        {
            var service = ServiceController.GetServices().Where(x => x.ServiceName == "DigiReportExportService").FirstOrDefault();
            if (null == service)
            {
                string[] commandLineOptions = new string[1] { "/LogFile=install.log" };
                string exeFilename = @"C:\Program Files (x86)\iMix\DGReportExportService.exe";
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Configuration.Install.AssemblyInstaller installer = new System.Configuration.Install.AssemblyInstaller(exeFilename, commandLineOptions);
                    installer.UseNewContext = true;
                    installer.Install(null);
                    installer.Commit(null);
                }
            }
        }

        private static void InstallCleanupService()
        {
            try
            {
                log.Info("InstallCleanupService Started 1-----------------: ");
                var service = ServiceController.GetServices().Where(x => x.ServiceName == "DigiDbCleanupService").FirstOrDefault();
                log.Info("InstallCleanupService service 2:-----------------: " + service);
                if (null == service)
                {
                    string[] commandLineOptions = new string[1] { "/LogFile=install.log" };
                    string exeFilename = @"C:\Program Files (x86)\iMix\DigiDbCleanupService.exe";
                    log.Info("InstallCleanupService exeFilename 3:-----------------: " + exeFilename);
                    if (System.IO.File.Exists(exeFilename))
                    {
                        log.Info("InstallCleanupService exeFilename 4:-----------------: ");
                        System.Configuration.Install.AssemblyInstaller installer = new System.Configuration.Install.AssemblyInstaller(exeFilename, commandLineOptions);
                        log.Info("InstallCleanupService exeFilename 5:-----------------: ");
                        installer.UseNewContext = true;
                        installer.Install(null);
                        log.Info("InstallCleanupService exeFilename 6:-----------------: ");
                        installer.Commit(null);
                        log.Info("InstallCleanupService exeFilename 7:-----------------: ");
                    }
                }
            }

            catch(Exception ex)
            {
                log.Error("InstallCleanupService Catch: ", ex);
            }
        }

        private static void InstallWatermarkService()
        {
            try
            {
                log.Info("InstallWatermarkService Started 1-----------------: ");
                var service = ServiceController.GetServices().Where(x => x.ServiceName == "DigiWatermarkService").FirstOrDefault();
                log.Info("InstallWatermarkService service 2:-----------------: " + service);
                if (null == service)
                {
                    string[] commandLineOptions = new string[1] { "/LogFile=install.log" };
                    string exeFilename = @"C:\Program Files (x86)\iMix\DigiWatermarkService.exe";
                    log.Info("InstallWatermarkService exeFilename 3:-----------------: " + exeFilename);
                    if (System.IO.File.Exists(exeFilename))
                    {
                        log.Info("InstallWatermarkService exeFilename 4:-----------------: ");
                        System.Configuration.Install.AssemblyInstaller installer = new System.Configuration.Install.AssemblyInstaller(exeFilename, commandLineOptions);
                        log.Info("InstallWatermarkService exeFilename 5:-----------------: ");
                        installer.UseNewContext = true;
                        installer.Install(null);
                        log.Info("InstallWatermarkService exeFilename 6:-----------------: ");
                        installer.Commit(null);
                        log.Info("InstallWatermarkService exeFilename 7:-----------------: ");
                    }
                }
                log.Info("InstallWatermarkService Started Completed -----------------: ");
            }
            catch(Exception ex)
            {
                log.Error("InstallWatermarkService Catch: ", ex);
            }
        }

        private static void RegisterBatFile()
        {
            try
            {
                System.Diagnostics.Process Crypt = new System.Diagnostics.Process();
                Crypt.StartInfo.FileName = System.IO.Path.Combine(Utility.FileAction.ExecutablePath, "Reg_x64.bat");
                Crypt.StartInfo.UseShellExecute = false;
                Crypt.StartInfo.RedirectStandardOutput = true;
                //Crypt.StartInfo.Arguments = "-encrypt -key " + EncryptionKey + " -infile " + InputFile + " -outfile " + OutputFile;
                Crypt.Start();
                string output = Crypt.StandardOutput.ReadToEnd();
                Crypt.WaitForExit();
            }
            catch (Exception ex)
            {
                log.Error("Main: ", ex);
            }
        }
        private static bool CloseAllDigiInstances()
        {
            bool isClosed = false;
            try
            {
                Console.WriteLine("Closing background applications...");
                using (var progressBar = new ProgressBar())
                {
                    ProgressBar.RunProgressBarToEnd(new ProgressBar(), 1, 95, TimeSpan.FromMilliseconds(10));
                    LoadRunningApplications();
                    KillAllApps();                    
                    StopWinService();                    
                    ProgressBar.AbortProgressBarRunToEnd(100);
                }
                isClosed = true;
            }
            catch (Exception exp)
            {
                log.Error("CloseAllDigiInstances: ", exp);
            }
            return isClosed;
        }

        private static void LoadRunningApplications()
        {
            string clsExe = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\CloseExecutables.xml";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(clsExe);
            XmlNodeList xndList = xdoc.GetElementsByTagName("Application");
            foreach (XmlNode item in xndList)
            {
                if (!htApp.ContainsKey(item.InnerText))
                    htApp.Add(item.InnerText, item.InnerText);
            }

            XmlNodeList xndList2 = xdoc.GetElementsByTagName("Service");
            foreach (XmlNode item in xndList2)
            {
                if (!htWinService.ContainsKey(item.InnerText))
                    htWinService.Add(item.InnerText, IsServiceRunning(item.InnerText));
            }
        }
        private static void KillAllApps()
        {
            foreach (string item in htApp.Keys)
            {
                KillOpenedProcess(item);
            }
        }
        private static void StopWinService()
        {
            foreach (string item in htWinService.Keys)
            {
                StopWindowService(item);
            }
        }
        private static void StopWindowService(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Running")
                {
                    myService.Stop();
                }
            }
            catch (Exception exp)
            {
                log.Error("StopWindowService: ", exp);
            }
        }
        public static void KillOpenedProcess(string name)
        {
            foreach (System.Diagnostics.Process clsProcess in System.Diagnostics.Process.GetProcesses())
            {
                if (clsProcess.ProcessName.ToLower().Contains(name.ToLower()))
                {
                    clsProcess.Kill();
                }
            }
        }
        private static bool IsServiceRunning(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Running")
                    return true;
                else
                    return false;

            }
            catch (Exception exp)
            {
                log.Error("IsServiceRunning: ", exp);
                return false;
            }
        }
        private static void StartWindowService(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Stopped")
                {
                    myService.Start();
                }

            }
            catch (Exception exp)
            {
                log.Error("StartWindowService: ", exp);
            }
        }
        private static void StartWinService()
        {
            Console.WriteLine("Background applications started...");
            using (var progress = new ProgressBar())
            {
                double i = 0, stepSize = (double)(100 / htWinService.Keys.Count);
                foreach (string item in htWinService.Keys)
                {
                    if (Convert.ToBoolean(htWinService[item]))
                        StartWindowService(item); progress.Report((double)i / 100);
                    i += stepSize;
                }
                progress.Report(1);
            }
        }
        private static bool CopyItemTemplate()
        {
            bool output = false;
            try
            {
                string itemPath = @"C:\Program Files (x86)\iMix\ItemTemplate";
                //string itemPath = @"D:\Project\Releases\V4.7.4.1\DigiPhotoEnhancedCode\DigiUpdates\bin\Debug\ItemTemplate";

                ConfigBusiness conBiz = new ConfigBusiness();
                conBiz = new ConfigBusiness();
                List<ConfigurationInfo> Item1 = conBiz.GetDeafultPathlist();
                foreach (ConfigurationInfo obj in Item1)
                {
                    if (Directory.Exists(obj.DG_Hot_Folder_Path) && Directory.Exists(itemPath))// && !Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "ItemTemplate")))
                    {
                        if (Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "ItemTemplate")))
                            DeleteDir(Path.Combine(obj.DG_Hot_Folder_Path, "ItemTemplate"));
                        CopyDir(itemPath, Path.Combine(obj.DG_Hot_Folder_Path, "ItemTemplate"));
                    }
                }
                output = true;
                return output;
            }
            catch (Exception ex)
            {
                output = false;
                return output;
            }
        }

        private static bool CopyVideoFolder()
        {
            bool output = false;
            try
            {
                string filesPath = @"C:\Program Files (x86)\iMix";
                //string filesPath = @"D:\Project\Releases\V4.7.4.1\DigiPhotoEnhancedCode\DigiUpdates\bin\Debug";

                ConfigBusiness conBiz = new ConfigBusiness();
                conBiz = new ConfigBusiness();
                List<ConfigurationInfo> Item1 = conBiz.GetDeafultPathlist();
                foreach (ConfigurationInfo obj in Item1)
                {
                    if (Directory.Exists(obj.DG_Hot_Folder_Path) && Directory.Exists(Path.Combine(filesPath, "Profiles")))// && !Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "ItemTemplate")))
                    {
                        if (Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "Profiles")))
                            DeleteDir(Path.Combine(obj.DG_Hot_Folder_Path, "Profiles"));
                        CopyDir(Path.Combine(filesPath, "Profiles"), Path.Combine(obj.DG_Hot_Folder_Path, "Profiles"));
                    }
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "Audio")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "Audio"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "CGSettings")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "CGSettings"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "GumBallRide")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "GumBallRide"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "ProcessedVideos")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "ProcessedVideos"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "VideoBackGround")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "VideoBackGround"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "Videos")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "Videos"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "VideoTemplate")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "VideoTemplate"));

                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "3x3")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "3x3"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "4x6")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "4x6"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "5x7")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "5x7"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "6x8")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "6x8"));
                    if (!Directory.Exists(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "8x10")))
                        Directory.CreateDirectory(Path.Combine(obj.DG_Hot_Folder_Path, "BG", "8x10"));
                }

                output = true;
                return output;
            }
            catch (Exception ex)
            {
                output = false;
                return output;
            }
        }

        private static void CopyDir(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);
            foreach (var file in Directory.GetFiles(sourceDir))
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

            foreach (var directory in Directory.GetDirectories(sourceDir))
                CopyDir(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }

        private static void DeleteDir(string deleteDir)
        {
            Directory.CreateDirectory(deleteDir);
            foreach (var file in Directory.GetFiles(deleteDir))
                File.Delete(file);

            foreach (var directory in Directory.GetDirectories(deleteDir))
                DeleteDir(Path.Combine(deleteDir, Path.GetFileName(directory)));
        }
        private static bool UpdateConfigFile()
        {
            bool output = false;
            try
            {
                FilePath = @"C:\Program Files (x86)\iMix";

                if (File.Exists(FilePath + "\\Digiphoto.exe") && File.Exists(FilePath + "\\Digiphoto.exe.config"))
                {
                    decrypt(FilePath + "\\Digiphoto.exe");
                    ShowXmlData(string.Concat(FilePath, "\\Digiphoto.exe.config"));
                    encrypt(FilePath + "\\Digiphoto.exe");
                }
                SaveDigiphotoManualVideoConsole(FilePath);
                SaveLiveVideoEditing(FilePath);
                SaveManualDownloadProcess(FilePath);
                SaveVideoProcessingEngine(FilePath);
                SaveDGReportExportService(FilePath);
                SavePresoldService(FilePath);
				SaveSyncService(FilePath);
                SaveCleanupService(FilePath);
                SaveWatermarkService(FilePath);
                output = true;
                return output;

            }
            catch (Exception ex)
            {
                log.Error("CloseAllDigiInstances: ", ex);
                return output;
            }
        }
        static string ConvertStringArrayToString(string[] array, string seprator)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(seprator);
            }
            return builder.ToString();
        }
        private static void WriteXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');
                                StringBuilder strColNew = new StringBuilder();
                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');


                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "'Data Source":
                                        case "Data Source":
                                            strColChild[strColChild.Length - 1] = DataSource;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");


                                            break;
                                        case "Initial Catalog":

                                            strColChild[strColChild.Length - 1] = InitialCatalog;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");

                                            //strColNew[strColChild.Length - 1] = textBoxInitialCatalog.Text;
                                            break;
                                        case "User ID":
                                            strColChild[strColChild.Length - 1] = UserID;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxUserID.Text;
                                            break;
                                        case "Password":
                                            strColChild[strColChild.Length - 1] = Password;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxPassword.Text;
                                            break;
                                        default:
                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            break;
                                    }
                                }
                                //strColNew.Append(ConvertStringArrayToString(strColChild, ";"));
                                //stringData = ConvertStringArrayToString(strColNew);
                                attrColection.Value = strColNew.ToString();
                            }
                        }
                    }
                }

                doc.Save(fileName);
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private static void WriteXmlDataDBCleanUpWaterMark(string fileName)
        {
            try
            {
                string HostName = System.Net.Dns.GetHostName();
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');
                                StringBuilder strColNew = new StringBuilder();
                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');


                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "'Data Source":
                                        case "Data Source":
                                            strColChild[strColChild.Length - 1] = HostName;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");


                                            break;
                                        case "Initial Catalog":

                                            strColChild[strColChild.Length - 1] = InitialCatalog;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");

                                            //strColNew[strColChild.Length - 1] = textBoxInitialCatalog.Text;
                                            break;
                                        case "User ID":
                                            strColChild[strColChild.Length - 1] = UserID;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxUserID.Text;
                                            break;
                                        case "Password":
                                            strColChild[strColChild.Length - 1] = Password;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxPassword.Text;
                                            break;
                                        default:
                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            break;
                                    }
                                }
                                //strColNew.Append(ConvertStringArrayToString(strColChild, ";"));
                                //stringData = ConvertStringArrayToString(strColNew);
                                attrColection.Value = strColNew.ToString();
                            }
                        }
                    }
                }

                doc.Save(fileName);
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private static void SaveDigiphotoManualVideoConsole(string fileName)
        {
            fileName += "//Digiphoto.Manual.VideoConsole.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\Digiphoto.Manual.VideoConsole.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\Digiphoto.Manual.VideoConsole.exe");
        }
        private static void SaveLiveVideoEditing(string fileName)
        {
            fileName += "//LiveVideoEditing.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\LiveVideoEditing.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\LiveVideoEditing.exe");
        }
        private static void SaveManualDownloadProcess(string fileName)
        {
            fileName += "//ManualDownloadProcess.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\ManualDownloadProcess.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\ManualDownloadProcess.exe");
        }
        private static void SaveVideoProcessingEngine(string fileName)
        {
            fileName += "//VideoProcessingEngine.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\VideoProcessingEngine.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\VideoProcessingEngine.exe");
        }
        private static void SaveDGReportExportService(string fileName)
        {
            fileName += "//DGReportExportService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\VideoProcessingEngine.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DGReportExportService.exe");
        }
        private static void SavePresoldService(string fileName)
        {
            fileName += "//PreSoldService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\VideoProcessingEngine.exe");

            WriteXmlData(fileName);
            encrypt(FilePath + "\\PreSoldService.exe");
        }
        private static void SaveSyncService(string fileName)
        {
            fileName += "//DataSyncWinService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            //decrypt(FilePath + "\\VideoProcessingEngine.exe");

            WriteXmlData(fileName);
            encrypt(FilePath + "\\DataSyncWinService.exe");
        }

        private static void SaveCleanupService(string fileName)
        {
            try
            {
                fileName += "//DigiDbCleanupService.exe.config";
                if (!File.Exists(fileName))
                    return;//message

                //decrypt(FilePath + "\\VideoProcessingEngine.exe");

                WriteXmlDataDBCleanUpWaterMark(fileName);
                encrypt(FilePath + "\\DigiDbCleanupService.exe");
            }
            catch(Exception ex)
            {
                log.Info("SaveCleanupService Catch", ex);
            }
        }

        private static void SaveWatermarkService(string fileName)
        {
            try
            {
                fileName += "//DigiWatermarkService.exe.config";
                if (!File.Exists(fileName))
                    return;//message

                //decrypt(FilePath + "\\VideoProcessingEngine.exe");

                WriteXmlDataDBCleanUpWaterMark(fileName);
                encrypt(FilePath + "\\DigiWatermarkService.exe");
            }
            catch(Exception ex)
            {
                log.Info("SaveWatermarkService Catch", ex);
            }
        }

        private static void UnprotectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, false);
        }
        private static void ToggleConnectionStringProtection(string pathName, bool protect)
        {
            // Define the Dpapi provider name.
            string strProvider = "DataProtectionConfigurationProvider";
            // string strProvider = "RSAProtectedConfigurationProvider";

            System.Configuration.Configuration oConfiguration = null;
            System.Configuration.ConnectionStringsSection oSection = null;

            try
            {
                // Open the configuration file and retrieve the connectionStrings section.

                // For Web!
                // oConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

                // For Windows!
                // Takes the executable file name without the config extension.
                oConfiguration = System.Configuration.ConfigurationManager.OpenExeConfiguration(pathName);

                if (oConfiguration != null)
                {
                    bool blnChanged = false;
                    oSection = oConfiguration.GetSection("connectionStrings") as System.Configuration.ConnectionStringsSection;

                    if (oSection != null)
                    {
                        if ((!(oSection.ElementInformation.IsLocked)) && (!(oSection.SectionInformation.IsLocked)))
                        {
                            if (protect)
                            {
                                if (!(oSection.SectionInformation.IsProtected))
                                {
                                    blnChanged = true;

                                    // Encrypt the section.
                                    oSection.SectionInformation.ProtectSection(strProvider);
                                }
                            }
                            else
                            {
                                if (oSection.SectionInformation.IsProtected)
                                {
                                    blnChanged = true;

                                    // Remove encryption.
                                    oSection.SectionInformation.UnprotectSection();
                                }
                            }
                        }

                        if (blnChanged)
                        {
                            // Indicates whether the associated configuration section will be saved even if it has not been modified.
                            oSection.SectionInformation.ForceSave = true;

                            // Save the current configuration.
                            oConfiguration.Save();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        private static void decrypt(string configfileName)
        {
            try
            {
                UnprotectConnectionString(configfileName);
                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private static void Save(string configfileName)
        {
            try
            {
                StreamWriter b = new StreamWriter(File.Open(configfileName, FileMode.Create));
                b.Write(xmlData);

                b.Close();

            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private static void encrypt(string configfileName)
        {
            try
            {
                ProtectConnectionString(configfileName);
                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private static void ProtectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, true);
        }

        private static void ShowXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);
                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings"); // You can also use XPath here
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');

                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');
                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "Data Source":
                                            DataSource = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Initial Catalog":
                                            InitialCatalog = strColChild[strColChild.Length - 1];
                                            break;
                                        case "User ID":
                                            UserID = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Password":
                                            Password = strColChild[strColChild.Length - 1];
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        static void WindowClosing(object sender, EventArgs e)
        {
            CommandLineWindow cmd = sender as CommandLineWindow;
            if (cmd.AskIfYouWantExit())
            {
                Console.WriteLine("Doing clean up! Please wait....");
                Thread.Sleep(TimeSpan.FromSeconds(10));
                Environment.Exit(-1);
            }
        }
    }

    public class CommandLineWindow {

        static CommandLineWindow _commandLineWindow;
        static AutoResetEvent _autoReset = new AutoResetEvent(false);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        // A delegate type to be used as the handler routine 
        // for SetConsoleCtrlHandler.
        delegate bool HandlerRoutine(CtrlTypes CtrlType);
        
        static EventHandler _onWindowClosing;
        public static event EventHandler WindowClosing { add { _onWindowClosing += value; }
                                                        remove { if (_onWindowClosing != null) _onWindowClosing -= value; } }
        
        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
                if (_commandLineWindow == null)
                    _commandLineWindow = new CommandLineWindow();
                // Put your own handler here
                if ((ctrlType == CtrlTypes.CTRL_CLOSE_EVENT || ctrlType == CtrlTypes.CTRL_C_EVENT) && _onWindowClosing != null)
                    _onWindowClosing(_commandLineWindow, null);
            //_autoReset.WaitOne();
            //char k = 'n';
            //Thread th = new Thread(() =>
            //                        {
            //                            Console.WriteLine("Do you really want to exit (y/n)?:   ");
            //                            ConsoleKeyInfo cki = Console.ReadKey();
            //                            k = cki.KeyChar;
            //                        });
            //th.Start();
            //th.Join();
            ////Thread.Sleep(TimeSpan.FromSeconds(40));
            //Console.WriteLine("Char read: " + k);
            ////return (k == 'y' || k == 'Y');
            return true;
        }

        static CommandLineWindow() {
            SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);
        }

        public bool AskIfYouWantExit()
        {
            //_autoReset.WaitOne();
            char k = 'n';
            Thread th = new Thread(() =>
                                        {
                                            Console.WriteLine("Do you really want to exit (y/n)?:   ");
                                            ConsoleKeyInfo cki = Console.ReadKey();
                                            k = cki.KeyChar;
                                        });
            th.Start();
            th.Join();
            //System.Diagnostics.Process.GetCurrentProcess().WaitForExit();
            //Thread.Sleep(TimeSpan.FromSeconds(40));
            Console.WriteLine("Char read: " + k);
            return (k == 'y' || k == 'Y');
        }

    }
}