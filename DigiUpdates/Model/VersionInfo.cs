﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiUpdates.Model
{
    public class VersionInfo
    {
        public int VerisonId { get; set; }
        public string VersionName { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string LocalMachineZipPath { get; set; }
        public string HotFolderPath { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public int Revision { get; set; }
    }
}
