﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiUpdates.Model
{
    public class UpdateDetailInfo
    {
        private List<VersionInfo> _version = new List<VersionInfo>();
        public string DatabaseBackupPath { get; set; }
        public string OldVersion { get; set; }
        public string ApplicationBackupPath { get; set; }
        public string LocationName { get; set; }

        public string Store { get; set; }

        public string Country { get; set; }
        public string UpdateException { get; set; }
        public List<VersionInfo> Versions
        {
            get { return _version; }
            set { _version = value; }
        }

        public ProgressBar ProgressBar { get; set; }
    }
}
