﻿using System;
using System.Threading;

namespace DigiUpdates
{

    public struct ProgressBarThread
    {

        ProgressBar _progressBar;
        public ProgressBar ProgressBar { get { return (_progressBar); } }

        Thread _thProgress;
        public Thread ProgressThread { get { return (_thProgress); } }

        int _end;
        public int End { get { return (_end); } }

        public ProgressBarThread(ProgressBar progressBar, Thread thProgress, int end)
        {
            _progressBar = progressBar;
            _thProgress = thProgress;
            _end = end;
        }

    }

    public class ProgressBar : IDisposable, IProgress<double>
    {
        private const int blockCount = 10;
        private readonly TimeSpan animationInterval = TimeSpan.FromSeconds(1.0 / 8);
        private const string animation = @"|/-\";
        //private readonly string[] animation = new string[] { "-", "" };
        private bool showProgressBar = true;
        //static Thread _thProgress = null;
        static object _lockTimerHandlerObj = new object();
        static object _lockRunObj = new object();
        public bool _autoDisposeOn100 = true;
        public bool AutoDisposeOn100 { get { return (_autoDisposeOn100); } }

        private readonly Timer timer;
        int _lastUpdatedProgress = 0;
        private double currentProgress = 0;
        public int Current { get { return (int)Math.Ceiling(currentProgress * 100); } }
        static ProgressBarThread _progressBarThread;
        public static ProgressBarThread CurrentProgress { get { return (_progressBarThread); } }
        private string currentText = string.Empty;
        private bool disposed = false;
        private int animationIndex = 0;
        int _progressBarCursorLeft = 5;
        int _progressBarCursorTop = 0;
        bool _isCursorPositionSet = false;

        public ProgressBar(bool ShowProgressBar = true, bool autoDisposeOn100 = true)
        {
            showProgressBar = ShowProgressBar;
            timer = new Timer(TimerHandler);

            // A progress bar is only for temporary display in a console window.
            // If the console output is redirected to a file, draw nothing.
            // Otherwise, we'll end up with a lot of garbage in the target file.
            //if (!Console.IsOutputRedirected)
            //{
            ResetTimer();
            //Report(0);
            //}
        }
        public void Report(double value)
        {
            //Thread th = new Thread(() =>{
            if (value < 1.001)
            {
                // Make sure value is in [0..1] range
                value = Math.Max(0, Math.Min(1, value));
                Interlocked.Exchange(ref currentProgress, value);
                //if (Current < 2 || Current >= 100) lock (_lockTimerHandlerObj)
                TimerHandler(null);
                //});

            }
            //th.Start();
            //th.Join();
        }
        private void TimerHandler(object state)
        {
            lock (_lockTimerHandlerObj)
            {
                if (disposed) return;
                if (Current > 0 && Current < 101)
                {
                    string text = "";
                    int percent = (int)(currentProgress * 100);
                    _lastUpdatedProgress = percent;
                    if (showProgressBar)
                    {
                        Interlocked.Exchange(ref percent, (int)(currentProgress * 100));
                        int progressBlockCount = (int)(currentProgress * blockCount);
                        text = string.Format("{0}{1} {2,3}% {3}",
                                new string('█', progressBlockCount), new string('-', blockCount - progressBlockCount),
                                percent,
                                Current > 99 ? '\n' : animation[animationIndex++ % animation.Length]);
                        /*
                        if (Current > 99)
                        {
                            text = text.Remove(text.Length - 2) + "\n";
                            if (AutoDisposeOn100) this.Dispose();
                        }*/
                    }
                    else
                    {
                        text = animation[animationIndex++ % animation.Length].ToString();
                    }
                    UpdateText(text);
                }
                ResetTimer();
            }
        }
        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
        private void UpdateText(string text)
        {
            // Get length of common portion
            //int commonPrefixLength = 0;
            //int commonLength = Math.Min(currentText.Length, text.Length);
            //while (commonPrefixLength < commonLength &&  text[commonPrefixLength] == currentText[commonPrefixLength])
            //{
            //    commonPrefixLength++;
            //}

            //// Backtrack to the first differing character
            //StringBuilder outputBuilder = new StringBuilder();
            //outputBuilder.Append('\b', currentText.Length - commonPrefixLength);

            //// Output new suffix
            //outputBuilder.Append(text.Substring(commonPrefixLength));

            //// If the new text is shorter than the old one: delete overlapping characters
            //int overlapCount = currentText.Length - text.Length;
            //if (overlapCount > 0)
            //{
            //    outputBuilder.Append(' ', overlapCount);
            //    outputBuilder.Append('\b', overlapCount);
            //}
            ////Console.SetBufferSize()
            //if (string.IsNullOrEmpty(text))
            //{
            lock (_lockTimerHandlerObj)
            {
                //if (string.IsNullOrEmpty(text) && _isCursorPositionSet)
                //{
                //    StringBuilder outputBuilder = new StringBuilder(currentText);
                //    //outputBuilder.Append(' ', currentText.Length);
                //    outputBuilder.Append('\b', 2);
                //    text = outputBuilder.ToString();//new string('\b', currentText.Length);                    
                //    text += "\n";
                //}

                //int cursorLeft = Console.CursorLeft;
                //int cursorTop = Console.CursorTop;
                if (!_isCursorPositionSet)
                {
                    _isCursorPositionSet = true;
                    _progressBarCursorLeft = 0;// Console.CursorLeft - text.Length;
                    _progressBarCursorTop = Console.CursorTop;// + 1;
                    Console.Write(text);
                }
                else if (!string.IsNullOrEmpty(text))
                {
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write(text);
                }
                if (currentText.CompareTo(text) != 0)
                {
                    currentText = text;
                }
            }
        }
        private void ResetTimer()
        {
            if (!disposed)
                timer.Change(animationInterval, TimeSpan.FromMilliseconds(-1));
        }
        public void Dispose()
        {
            lock (_lockTimerHandlerObj)
            {
                disposed = true;
                timer.Dispose();
                //UpdateText(string.Empty);                
            }
        }

        public static void RunProgressBarToEnd(ProgressBar progress, ref int i, int end, TimeSpan tsSleep)
        {
            for (; i < end; i++)
            {
                Thread.Sleep(tsSleep);
                progress.Report((double)i / 100);
            }
        }

        public static ProgressBar RunProgressBarToEnd(ProgressBar progressBar, double stepSize, int end, TimeSpan tsSleep)
        {
            AbortProgressBarRunToEnd(null);
            //
            Thread thProgress = new Thread(() => {
                while (progressBar.Current < _progressBarThread.End)
                {
                    Thread.Sleep(tsSleep);
                    lock (_lockRunObj)
                        progressBar.Report(((double)(progressBar.Current + stepSize)) / 100);
                }
            });
            _progressBarThread = new ProgressBarThread(progressBar, thProgress, end);
            thProgress.Start();
            return (progressBar);
            //}
        }
        /*
        public static void RunProgressBarToEnd(ProgressBarReport progressBarReport, double stepSize, int end, TimeSpan tsSleep)
        {
            lock (lockRunObj)
            {
                thProgress = new Thread(() =>
                {
                    progressBarReport.LastStepSize = stepSize;
                    while (progressBarReport.ProgressBar.Current < end)
                    {
                        //if (isDone) { i = end; }
                        progressBarReport.ProgressBar.Report((double)(progressBarReport.ProgressBar.Current + progressBarReport.LastStepSize) / 100);
                        Thread.Sleep(tsSleep);
                    }
                });
                thProgress.Start();
            }            
        }
        */
        public static void AbortProgressBarRunToEnd(Thread thProgress, ProgressBar progress, ref int start, int end)
        {
            if (start < end)
            {
                progress.Report((double)end / 100);
                progress.TimerHandler(null);
            }
            if (thProgress.IsAlive) thProgress.Abort();
            else thProgress.Join();
            start = end;
        }

        //public static void AbortProgressBarRunToEnd(ProgressBar progressBar, int end)
        //{
        //    lock (lockAbortObj)
        //    {
        //        if (progressBar.Current < end)
        //            progressBar.Report((double)end / 100);

        //        if (_thProgress.IsAlive) _thProgress.Abort();
        //        else _thProgress.Join();
        //        ProgressBar.ClearCurrentConsoleLine();
        //    }
        //}

        public static void AbortProgressBarRunToEnd(int? end)
        {
            lock (_lockRunObj)
            {
                if (CurrentProgress.ProgressThread != null && CurrentProgress.ProgressBar != null)
                {
                    if (end == null) end = CurrentProgress.End;
                    if (CurrentProgress.ProgressBar.Current < end)
                    {
                        CurrentProgress.ProgressBar.Report((double)end / 100);
                    }
                    if (CurrentProgress.ProgressThread.IsAlive) CurrentProgress.ProgressThread.Abort();
                    else CurrentProgress.ProgressThread.Join();
                    if (CurrentProgress.ProgressBar.Current > 99)
                        CurrentProgress.ProgressBar.Dispose();
                    //ProgressBar.ClearCurrentConsoleLine();
                }
            }
        }

        /*
        public static void AbortProgressBarRunToEnd(ProgressBarReport progressBarReport, int end)
        {
            lock (lockAbortObj)
            {
                if (progressBarReport.ProgressBar.Current < end)
                    progressBarReport.ProgressBar.Report((double)end / 100);

                if (thProgress.IsAlive) thProgress.Abort();
                else thProgress.Join();
                ProgressBar.ClearCurrentConsoleLine();
            }
        }
        */

        ~ProgressBar()
        {
            Dispose();
        }
    }

    //
    // Summary:
    //     Defines a provider for progress updates.
    //
    // Type parameters:
    //   T:
    //     The type of progress update value.This type parameter is contravariant. That
    //     is, you can use either the type you specified or any type that is less derived.
    //     For more information about covariance and contravariance, see Covariance and
    //     Contravariance in Generics.
    public interface IProgress<in T>
    {
        //
        // Summary:
        //     Reports a progress update.
        //
        // Parameters:
        //   value:
        //     The value of the updated progress.
        void Report(T value);
    }

}

