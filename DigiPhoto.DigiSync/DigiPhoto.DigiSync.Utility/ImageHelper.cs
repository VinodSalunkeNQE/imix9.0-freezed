﻿using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Utility
{
    public class ImageHelper
    {
        internal ImageHelper() { }


        public static int GetDefaultWidth
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings.Get("DefaultCanvasWidth"));
                }
                catch
                {
                    return 500;
                }
            }
        }

        public static int GetDefaultHeight
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings.Get("DefaultCanvasHeight"));
                }
                catch
                {
                    return 500;
                }
            }
        }

        public static ImageAttribute GetImageHeightWidth(string filePath)
        {
            var imageInfo = new ImageAttribute();
            try
            {
                using (var image = System.Drawing.Image.FromFile(filePath))
                {
                    imageInfo.Width = image.Width;
                    imageInfo.Height = image.Height;
                    imageInfo.ImageSize = image.Size;
                }
            }
            catch
            {
                imageInfo.Width = 500;
                imageInfo.Height = 500;
                imageInfo.ImageSize = new Size(500, 500);
            }

            return imageInfo;
        }


        public static string ScaleImage(System.Drawing.Image image, System.Drawing.Size size, bool preserveAspectRatio = true)
        {
            string dimension = string.Empty;
            int newWidth = 500;
            int newHeight = 500;
            try
            {
                if (preserveAspectRatio)
                {
                    int originalWidth = image.Width;
                    int originalHeight = image.Height;
                    float percentWidth = (float)size.Width / (float)originalWidth;
                    float percentHeight = (float)size.Height / (float)originalHeight;
                    float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                    newWidth = (int)(originalWidth * percent);
                    newHeight = (int)(originalHeight * percent);
                }
                else
                {
                    newWidth = size.Width;
                    newHeight = size.Height;
                }

                System.Drawing.Image newImage = new System.Drawing.Bitmap(newWidth, newHeight);
                using (System.Drawing.Graphics graphicsHandle = System.Drawing.Graphics.FromImage(newImage))
                {
                    graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
                }
                dimension = newImage.Height.ToString() + "," + newImage.Width.ToString();
            }
            catch
            {
                dimension = "400,500";
            }
            return dimension;
        }

        public static string ScaleImage(int imageWidth, int imageHeight, Size size, bool preserveAspectRatio = true)
        {
            string dimension = string.Empty;
            try
            {
                int newWidth;
                int newHeight;
                if (preserveAspectRatio)
                {
                    float percentWidth = (float)size.Width / (float)imageWidth;
                    float percentHeight = (float)size.Height / (float)imageHeight;
                    float percent = (percentHeight < percentWidth) ? percentHeight : percentWidth;
                    newWidth = (int)((float)imageWidth * percent);
                    newHeight = (int)((float)imageHeight * percent);
                }
                else
                {
                    newWidth = size.Width;
                    newHeight = size.Height;
                }
                dimension = newHeight + "," + newWidth;
            }
            catch
            {
                dimension = "400,500";
            }
            return dimension;
        }

    }
}

