﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Configuration;
namespace DigiPhoto.DigiSync.Utility
{
    public static class CommonUtility
    {
        public static string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["DigiPhotoWebEntities"] == null)
                {
                    throw new Exception("Connection String not specified");
                }
                return ConfigurationManager.ConnectionStrings["DigiPhotoWebEntities"].ConnectionString;
            }
        }
        public static string ConnectionStringLocatl
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["DigiConnectionString"] == null)
                {
                    throw new Exception("Connection String not specified");
                }
                return ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            }
        }
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            StringWriter textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        }

        public static T DeserializeXML<T>(this string xmlString)
        {
            T returnValue = default(T);
            XmlSerializer serial = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xmlString);
            object result = serial.Deserialize(reader);
            if (result != null && result is T)
            {
                returnValue = ((T)result);
            }
            reader.Close();
            return returnValue;
        }

        public static string GetRandomString(int length)
        {
            string[] array = new string[32]
	        {
		        "0","1","2","3","4","5","6","8","9","A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","S","T","U","V","W","X","Y","Z"
	        };
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < length; i++) sb.Append(array[GetRandomNumber(32)]);
            return sb.ToString();
        }

        public static int GetRandomNumber(int maxNumber)
        {
            if (maxNumber < 1)
                throw new System.Exception("The maxNumber value should be greater than 1");
            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            int seed = (b[0] & 0x7f) << 24 | b[1] << 16 | b[2] << 8 | b[3];
            System.Random r = new System.Random(seed);
            return r.Next(1, maxNumber);
        }

        public static string GetUniqueSynccode(string ApplicationObject, string countryCode, string storeCode, string subStoreid)
        {
            string dateString = String.Format("{0:yyyyMMdd}", DateTime.Now);
            string timeString = String.Format("{0}{1:HHmmssfff}", "T", DateTime.Now);
            string guidString = Guid.NewGuid().ToString();
            string guidst = guidString.Substring(0, 27).Replace("-", "");
            string sb = countryCode + storeCode + subStoreid + dateString + timeString + guidst + ApplicationObject;
            return sb.ToUpper();
        }
    }
}
