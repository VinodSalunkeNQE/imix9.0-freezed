﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DigiPhotoWebContext : DbContext
    {
        public DigiPhotoWebContext()
            : base("name=DigiPhotoWebEntities")
        {
        }

        public virtual DbSet<ActionType> ActionTypes { get; set; }
        public virtual DbSet<ApplicationClientVersion> ApplicationClientVersions { get; set; }
        public virtual DbSet<ApplicationObject> ApplicationObjects { get; set; }
        public virtual DbSet<ApplicationVersion> ApplicationVersions { get; set; }
        public virtual DbSet<Background> Backgrounds { get; set; }
        public virtual DbSet<Border> Borders { get; set; }
        public virtual DbSet<Camera> Cameras { get; set; }
        public virtual DbSet<ChangeTracking> ChangeTrackings { get; set; }
        public virtual DbSet<Code> Codes { get; set; }
        public virtual DbSet<CodeTable> CodeTables { get; set; }
        public virtual DbSet<ConfigurationMaster> ConfigurationMasters { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<DigiConfiguration> DigiConfigurations { get; set; }
        //public virtual DbSet<DigiImage> DigiImages { get; set; }
        public virtual DbSet<DigiMasterLocation> DigiMasterLocations { get; set; }
        public virtual DbSet<DiscountType> DiscountTypes { get; set; }
        public virtual DbSet<Graphic> Graphics { get; set; }
        public virtual DbSet<IMixConfiguration_Delete> IMixConfiguration_Delete { get; set; }
        public virtual DbSet<IMixConfigurationValue> IMixConfigurationValues { get; set; }

        public virtual DbSet<MessageAttribute> MessageAttributes { get; set; }
        public virtual DbSet<MessageAttributeValue> MessageAttributeValues { get; set; }
        public virtual DbSet<MessageProfile> MessageProfiles { get; set; }
        public virtual DbSet<MessageQueue> MessageQueues { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<ObjectAuthorization> ObjectAuthorizations { get; set; }
        public virtual DbSet<PrinterQueueData> PrinterQueues { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderMaster> OrderMasters { get; set; }
        public virtual DbSet<Package> Packages { get; set; }
        public virtual DbSet<PackageProduct> PackageProducts { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductPricing> ProductPricings { get; set; }
        public virtual DbSet<ProductRefund> ProductRefunds { get; set; }
        public virtual DbSet<ProductRefundDetail> ProductRefundDetails { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SB_AT_ServiceBrokerLogs> SB_AT_ServiceBrokerLogs { get; set; }
        public virtual DbSet<SyncRegistration> SyncRegistrations { get; set; }
        public virtual DbSet<SyncSession> SyncSessions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserActivityLog> UserActivityLogs { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<TempObjAuth> TempObjAuths { get; set; }
        public virtual DbSet<UserAuthorization> UserAuthorizations { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<RoleModule> RoleModules { get; set; }


        public virtual DbSet<CardUniqueIdentifier> CardUniqueIdentifiers { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerQRCodeDetail> CustomerQRCodeDetails { get; set; }
        public virtual DbSet<CustomerQRCodeMapping> CustomerQRCodeMappings { get; set; }
        public virtual DbSet<iMixImageCardType> iMixImageCardTypes { get; set; }
        public virtual DbSet<IncomingChange> IncomingChanges { get; set; }
        public virtual DbSet<Package1> Packages1 { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<WebPhoto> WebPhotos { get; set; }
        public virtual DbSet<SubStore> SubStores { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }

        public virtual DbSet<CloudinaryDtl> CloudinaryDtls { get; set; }
        public virtual DbSet<PartnerLocation> PartnerLocations { get; set; }

        public virtual DbSet<CloudinaryExceptionDtl> CloudinaryExceptionDtls { get; set; }

        public virtual DbSet<OpeningFormDetails> OpeningFormDetails { get; set; }

        public virtual DbSet<ClosingFormDetails> ClosingFormDetails { get; set; }

        public virtual DbSet<TransDetails> TransDetails { get; set; }

        public virtual DbSet<WorkFlowControl> WorkFlowControls { get; set; }

        public virtual DbSet<InventoryConsumable> InventoryConsumables { get; set; }
        public virtual DbSet<CurrencyMaster> CurrencyMasters { get; set; }
public virtual DbSet<CurrencyProfile> CurrencyProfiles { get; set; }
        public virtual DbSet<CurrencyProfileRate> CurrencyProfileRates { get; set; }
        public virtual DbSet<CurrencySyncStatus> CurrencySyncStatuses { get; set; }
public virtual DbSet<Scene> Scenes { get; set; }

        public virtual DbSet<ChangeTrackingDownLoadHistory> ChangeTrackingDownLoadHistorys { get; set; }
        public virtual DbSet<ChangeTrackingProcessingStatusDtl> ChangeTrackingProcessingStatusDtls { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IncomingChange>().ToTable("IncomingChange");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<UserDetail>().ToTable("UserDetail");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<DigiMasterLocation>().ToTable("DigiMasterLocation");
            modelBuilder.Entity<UserAuthorization>().ToTable("UserAuthorization");
            modelBuilder.Entity<Currency>().ToTable("Currency");
            modelBuilder.Entity<DiscountType>().ToTable("DiscountType");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<Package>().ToTable("Package");
            modelBuilder.Entity<ObjectAuthorization>().ToTable("ObjectAuthorization");
            modelBuilder.Entity<OrderMaster>().ToTable("OrderMaster");
            modelBuilder.Entity<SubStore>().ToTable("SubStore");
            modelBuilder.Entity<OrderDetail>().ToTable("OrderDetail");
            modelBuilder.Entity<CustomerQRCodeDetail>().ToTable("CustomerQRCodeDetail");
            modelBuilder.Entity<ProductRefund>().ToTable("ProductRefund");
            modelBuilder.Entity<ProductRefundDetail>().ToTable("ProductRefundDetail");




            modelBuilder.Entity<ActionType>()
                .Property(e => e.ActionName)
                .IsUnicode(false);

            modelBuilder.Entity<ActionType>()
                .HasMany(e => e.UserActivityLogs)
                .WithRequired(e => e.ActionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.MACAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Store)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationClientVersion>()
                .Property(e => e.Description)
                .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
               .Property(e => e.MachineName)
               .IsUnicode(false);
            modelBuilder.Entity<ApplicationClientVersion>()
               .Property(e => e.SubStore)
               .IsUnicode(false);

            modelBuilder.Entity<ApplicationObject>()
                .Property(e => e.ApplicationObjectName)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationObject>()
                .HasMany(e => e.ChangeTrackings)
                .WithRequired(e => e.ApplicationObject)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationObject>()
                .HasMany(e => e.ObjectAuthorizations)
                .WithRequired(e => e.ApplicationObject)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationVersion>()
                .Property(e => e.VersionName)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationVersion>()
                .Property(e => e.Path)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicationVersion>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Background>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Border>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Border>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Border>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Border>()
               .Property(e => e.DisplayName)
               .IsUnicode(false);

            modelBuilder.Entity<Camera>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Camera>()
                .Property(e => e.Make)
                .IsUnicode(false);

            modelBuilder.Entity<Camera>()
                .Property(e => e.Model)
                .IsUnicode(false);

            modelBuilder.Entity<Camera>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<ChangeTracking>()
                .Property(e => e.EntityCode)
                .IsUnicode(false);

            modelBuilder.Entity<Code>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Code>()
                .Property(e => e.RowVersion)
                .IsFixedLength();

            modelBuilder.Entity<Code>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Code)
                .HasForeignKey(e => e.UserTypeCodeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CodeTable>()
                .Property(e => e.CodeTableName)
                .IsUnicode(false);

            modelBuilder.Entity<CodeTable>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CodeTable>()
                .Property(e => e.RowVersion)
                .IsFixedLength();

            modelBuilder.Entity<CodeTable>()
                .HasMany(e => e.Codes)
                .WithRequired(e => e.CodeTable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConfigurationMaster>()
                .Property(e => e.ControlType)
                .IsUnicode(false);

            modelBuilder.Entity<ConfigurationMaster>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Symbol)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.CurrencyIcon)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.ProductPricings)
                .WithRequired(e => e.Currency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DigiConfiguration>()
                .HasMany(e => e.IMixConfigurationValues)
                .WithRequired(e => e.DigiConfiguration)
                .HasForeignKey(e => e.ConfigurationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DigiImage>()
                .Property(e => e.Size)
                .IsUnicode(false);

            //modelBuilder.Entity<DigiImage>()
            //    .HasMany(e => e.OrderDetails)
            //    .WithRequired(e => e.DigiImage)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<DigiMasterLocation>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DigiMasterLocation>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<DigiMasterLocation>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<DigiMasterLocation>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            //modelBuilder.Entity<DigiMasterLocation>()
            //    .HasMany(e => e.DigiImages)
            //    .WithRequired(e => e.DigiMasterLocation)
            //    .HasForeignKey(e => e.LocationId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DigiMasterLocation>()
            //    .HasMany(e => e.DigiImages1)
            //    .WithRequired(e => e.DigiMasterLocation1)
            //    .HasForeignKey(e => e.SubStoreId)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<DigiMasterLocation>()
                .HasMany(e => e.ObjectAuthorizations)
                .WithRequired(e => e.DigiMasterLocation)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DigiMasterLocation>()
            //    .HasMany(e => e.OrderDetails)
            //    .WithRequired(e => e.DigiMasterLocation)
            //    .HasForeignKey(e => e.SubStoreId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DigiMasterLocation>()
            //    .HasMany(e => e.Users)
            //    .WithMany(e => e.DigiMasterLocations)
            //    .Map(m => m.ToTable("UserAuthorization").MapLeftKey("DigiMasterLocationId").MapRightKey("UserId"));

            modelBuilder.Entity<DiscountType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DiscountType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<DiscountType>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<DiscountType>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Graphic>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<IMixConfiguration_Delete>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<IMixConfigurationValue>()
                .Property(e => e.ConfigurationValue)
                .IsUnicode(false);

            modelBuilder.Entity<IMixConfigurationValue>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<MessageAttribute>()
                .HasMany(e => e.MessageAttributeValues)
                .WithRequired(e => e.MessageAttribute)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageProfile>()
                .Property(e => e.ProfileFrom)
                .IsUnicode(false);

            modelBuilder.Entity<MessageProfile>()
                .Property(e => e.ProfileCC)
                .IsUnicode(false);

            modelBuilder.Entity<MessageProfile>()
                .Property(e => e.ProfileBCC)
                .IsUnicode(false);

            modelBuilder.Entity<MessageProfile>()
                .Property(e => e.ProfileSubject)
                .IsUnicode(false);

            modelBuilder.Entity<MessageProfile>()
                .HasMany(e => e.MessageQueues)
                .WithRequired(e => e.MessageProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageQueue>()
                .HasMany(e => e.MessageAttributeValues)
                .WithRequired(e => e.MessageQueue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.ActionUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.IconClass)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            //modelBuilder.Entity<Module>()
            //    .HasMany(e => e.Roles)
            //    .WithMany(e => e.Modules)
            //    .Map(m => m.ToTable("RoleModules").MapLeftKey("ModuleId").MapRightKey("RoleId"));

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.DiscountAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.UnitPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.TotalCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.NetPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetail>()
                .HasMany(e => e.ProductRefundDetails)
                .WithRequired(e => e.OrderDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderMaster>()
                .Property(e => e.OrderNumber)
                .IsUnicode(false);

            modelBuilder.Entity<OrderMaster>()
                .Property(e => e.OrderMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OrderMaster>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderMaster>()
                .Property(e => e.NetCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderMaster>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<OrderMaster>()
                .HasMany(e => e.OrderDetails)
                .WithRequired(e => e.OrderMaster)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderMaster>()
                .HasMany(e => e.ProductRefunds)
                .WithRequired(e => e.OrderMaster)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .HasMany(e => e.PackageProducts)
                .WithRequired(e => e.Package)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.ImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.PackageProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductPricings)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductRefund>()
                .Property(e => e.RefundAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProductRefund>()
                .HasMany(e => e.ProductRefundDetails)
                .WithRequired(e => e.ProductRefund)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductRefundDetail>()
                .Property(e => e.RefundDigiImageId)
                .IsUnicode(false);

            modelBuilder.Entity<ProductRefundDetail>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Role>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            //modelBuilder.Entity<Role>()
            //    .HasMany(e => e.Users)
            //    .WithMany(e => e.Roles)
            //    .Map(m => m.ToTable("UserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.ClientVersion)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.CountryName)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.StoreName)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.SubStoreName)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.StoreCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncRegistration>()
                .Property(e => e.SubStoreCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncSession>()
                .Property(e => e.ClientSiteCode)
                .IsUnicode(false);

            modelBuilder.Entity<SyncSession>()
                .Property(e => e.ClientVenueCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.SyncCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Borders)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Borders1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Currencies)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DigiConfigurations)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DigiConfigurations1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DigiImages)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Products)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Products1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProductRefunds)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.RefundBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserActivityLogs)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserActivityLog>()
                .Property(e => e.ActivityDescription)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserDetail>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserDetail)
                .WillCascadeOnDelete(false);




            modelBuilder.Entity<CardUniqueIdentifier>()
               .Property(e => e.CardUniqueIdentifier1)
               .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CustomerQRCodeMappings)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);



            modelBuilder.Entity<CustomerQRCodeDetail>()
                .Property(e => e.IdentificationCode)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerQRCodeDetail>()
                .Property(e => e.OrderId)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerQRCodeDetail>()
                .Property(e => e.Photos)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerQRCodeMapping>()
                .Property(e => e.IdentificationCode)
                .IsUnicode(false);


            modelBuilder.Entity<CustomerQRCodeMapping>().HasKey(t => new { t.CustomerId, t.IdentificationCode });


            modelBuilder.Entity<iMixImageCardType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<iMixImageCardType>()
                .Property(e => e.CardIdentificationDigit)
                .IsUnicode(false);

            modelBuilder.Entity<iMixImageCardType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<IncomingChange>()
                .Property(e => e.StoreName)
                .IsUnicode(false);

            modelBuilder.Entity<IncomingChange>()
               .Property(e => e.SubStoreCode)
               .IsUnicode(false);

            modelBuilder.Entity<IncomingChange>()
                .Property(e => e.EntityCode)
                .IsUnicode(false);


            modelBuilder.Entity<IncomingChange>().HasKey(T => new { T.IncomingChangeId });

            //modelBuilder.Entity<UserRole>().HasKey(T => new { T.UserRoleId });

            modelBuilder.Entity<Package1>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Package1>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Package1>().HasKey(t => new { t.PackageId });

            modelBuilder.Entity<Photo>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.RFID)
                .IsUnicode(false);

            modelBuilder.Entity<WebPhoto>()
                .Property(e => e.IdentificationCode).IsUnicode(false);

            modelBuilder.Entity<WebPhoto>()
                .Property(e => e.OrderNumber).IsUnicode(false);

            modelBuilder.Entity<SubStore>()
                .Property(e => e.SubStoreCode)
                .IsUnicode(false);

            //modelBuilder.Entity<SubStore>()
            //    .Property(e => e.SubStore1)
            //    .IsUnicode(false);

            modelBuilder.Entity<SubStore>()
                .Property(e => e.Store)
                .IsUnicode(false);

            modelBuilder.Entity<SubStore>()
                .HasMany(e => e.Photos)
                .WithRequired(e => e.SubStore)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.SourceImageID)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.CloudinaryPublicID)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.AddedBy)
               .IsUnicode(false);

            modelBuilder.Entity<CloudinaryDtl>()
               .Property(e => e.ModifiedBy)
               .IsUnicode(false);

            modelBuilder.Entity<WebPhoto>()
            .Property(e => e.ThumbnailDimension)
            .IsUnicode(false);

            modelBuilder.Entity<CloudinaryExceptionDtl>()
           .Property(e => e.AddedBy)
           .IsUnicode(false);

            modelBuilder.Entity<CloudinaryExceptionDtl>()
          .Property(e => e.ModifiedBy)
          .IsUnicode(false);

            modelBuilder.Entity<OpeningFormDetails>().ToTable("OpeningFormDetails", schemaName: "Sage");

            modelBuilder.Entity<ClosingFormDetails>().ToTable("ClosingFormDetails", schemaName: "Sage");

            modelBuilder.Entity<TransDetails>().ToTable("TransDetails", schemaName: "Sage");

            modelBuilder.Entity<WorkFlowControl>().ToTable("WorkFlowControl", schemaName: "Sage");

            modelBuilder.Entity<InventoryConsumable>().ToTable("InventoryConsumables", schemaName: "Sage");
   modelBuilder.Entity<CurrencyProfile>()
        .Property(e => e.ProfileName)
        .IsUnicode(false);
            modelBuilder.Entity<CurrencyProfile>()
          .Property(e => e.SyncCode)
          .IsUnicode(false);

            modelBuilder.Entity<CurrencyMaster>()
        .Property(e => e.CurrencyCode)
        .IsUnicode(false);
            modelBuilder.Entity<CurrencyMaster>()
        .Property(e => e.CurrencyName)
        .IsUnicode(false);
        
            modelBuilder.Entity<ChangeTrackingDownLoadHistory>()
              .Property(e => e.SiteSyncCode)
              .IsUnicode(false);

            modelBuilder.Entity<ChangeTrackingDownLoadHistory>()
             .Property(e => e.EntityCode)
             .IsUnicode(false);


            modelBuilder.Entity<CurrencyProfile>().ToTable("CurrencyProfile", schemaName: "Currency");
            modelBuilder.Entity<CurrencyProfileRate>().ToTable("CurrencyProfileRate", schemaName: "Currency");
            modelBuilder.Entity<CurrencyMaster>().ToTable("CurrencyMaster", schemaName: "Currency");
            modelBuilder.Entity<CurrencySyncStatus>().ToTable("CurrencySyncStatus", schemaName: "Currency");
        }
    }
}
