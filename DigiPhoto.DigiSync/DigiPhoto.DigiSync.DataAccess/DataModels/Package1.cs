namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Package1")]
    public partial class Package1
    {
        public Package1()
        {
            CustomerQRCodeDetails = new HashSet<CustomerQRCodeDetail>();
        }

        public Int64 PackageId { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        public int? MaxQuantity { get; set; }

        public double? PackagePrice { get; set; }

        public virtual ICollection<CustomerQRCodeDetail> CustomerQRCodeDetails { get; set; }
    }
}
