﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClosingFormDetails", Schema = "Sage")]
    public partial class ClosingFormDetails
    {
        [Key]
        public long ClosingFormDetailID { get; set; }

        [Column("6X8ClosingPrinterCount")]
        public long ClosingNumber6X8 { get; set; }

        [Column("8X10ClosingPrinterCount")]
        public long ClosingNumber8X10 { get; set; }

        public long PosterClosingPrinterCount { get; set; }

        [Column("6X8AutoClosingPrinterCount")]
        public long Auto6X8ClosingPrinterCount { get; set; }

        [Column("8X10AutoClosingPrinterCount")]
        public long Auto8X10ClosingPrinterCount { get; set; }
        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        [Column("6900ClosingPrinterCount")]
        public long ClosingNumber6900 { get; set; }

        [Column("8X10AutoClosingPrinterCount1")]
        public long Auto6900ClosingPrinterCount { get; set; }

        [Column("6900Westage")]
        public long Westage6900 { get; set; }
        //END

        [Column("6x8Westage")]
        public long Westage6x8 { get; set; }

        [Column("8x10Westage")]
        public long Westage8x10 { get; set; }

        public long PosterWestage { get; set; }

        [Column("6X8AutoWestage")]
        public long Auto6X8Westage{ get; set; }
        
        [Column("8x10AutoWestage")]
        public long Auto8x10Westage { get; set; }

        [Required]
        public int Attendance { get; set; }

        [Required]
        public decimal LaborHour { get; set; }

        [Required]
        public long NoOfCapture { get; set; }

        [Required]
        public long NoOfPreview { get; set; }

        [Required]
        public long NoOfImageSold { get; set; }

        public string Comments { get; set; }

         [Column("SiteID")]
        public Int64 SubStoreID { get; set; }

        [Required]
        public DateTime ClosingDate { get; set; }

        public Int64? FilledBy { get; set; }

        //public string FilledBySyncCode { get; set; }

        public DateTime TransDate { get; set; }

        public decimal? Cash { get; set; }

        public decimal? CreditCard { get; set; }

        public decimal? Amex { get; set; }

        public decimal? FCV { get; set; }

        public decimal? RoomCharges { get; set; }

        public decimal? KVL { get; set; }

        public decimal? Vouchers { get; set; }

        [Column("6x8PrintCount")]
        public long PrintCount6x8 { get; set; }

        [Column("8x10PrintCount")]
        public long PrintCount8x10 { get; set; }
        public long? PosterPrintCount { get; set; }
        public string SyncCode { get; set; }
        public int NoOfTransactions { get; set; }
    }
}
