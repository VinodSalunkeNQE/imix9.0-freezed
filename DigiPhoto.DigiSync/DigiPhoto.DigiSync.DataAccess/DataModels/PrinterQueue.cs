namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    [Table("PrinterQueueData")]
    public partial class PrinterQueueData
    {
        [Key]
        public int PrinterQueueID { get; set; }
        public int? OrderDetailId { get; set; }

        public bool? SentToPrinter { get; set; }

        public bool? isActive { get; set; }
        public int? StoreID { get; set; }

        public int? ReprintCount { get; set; }
    }
}
