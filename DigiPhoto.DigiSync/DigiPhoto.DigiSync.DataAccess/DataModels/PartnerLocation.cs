﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PartnerLocation")]
    public partial class PartnerLocation
    {
        [Key]
        public Int32 PartnerLocationID { get; set; }

        public Int32 PartnerID { get; set; }

        public Int64 StoreID { get; set; }

    }
}
