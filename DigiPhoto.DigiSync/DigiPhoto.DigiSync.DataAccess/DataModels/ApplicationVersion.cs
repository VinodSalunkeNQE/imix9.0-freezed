namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicationVersion
    {
        public ApplicationVersion()
        {
            ApplicationClientVersions = new HashSet<ApplicationClientVersion>();
        }

        public int ApplicationVersionID { get; set; }

        [Required]
        [StringLength(50)]
        public string VersionName { get; set; }

        public int Major { get; set; }

        public int Minor { get; set; }

        public int Revision { get; set; }

        public int BuildNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [StringLength(500)]
        public string Path { get; set; }

        public bool IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual ICollection<ApplicationClientVersion> ApplicationClientVersions { get; set; }
    }
}
