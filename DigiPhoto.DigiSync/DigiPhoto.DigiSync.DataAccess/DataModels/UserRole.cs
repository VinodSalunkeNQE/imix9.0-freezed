namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserRole
    {
        public long UserId { get; set; }

        public long RoleId { get; set; }

        [Key]
        public long UserRoleId { get; set; }

        //public virtual Role Role { get; set; }

       // public virtual User User { get; set; }
    }
}
