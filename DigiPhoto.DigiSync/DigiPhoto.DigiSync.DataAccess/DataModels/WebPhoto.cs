﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Partner.WebPhotos")]
    public partial class WebPhoto
    {
        public int WebPhotoId { get; set; }
        [Required]
        public string OrderNumber { get; set; }
        [Required]
        public string IdentificationCode { get; set; }
        public string LayeringObjects { get; set; }
        public string Dimention { get; set; }
        public int PhotoId { get; set; }
        public virtual Photo Photo { get; set; }

        public int Height { get; set; }
        public int Width { get; set; }
        [StringLength(50)]
        public string ThumbnailDimension { get; set; }
        public bool IsPaidImage { get; set; }
        public string CloudSourceImageURL { get; set; }
    }
}
