namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApplicationClientVersion")]
    public partial class ApplicationClientVersion
    {
        public int ApplicationClientVersionID { get; set; }

        public int? ApplicationVersionID { get; set; }

        [Required]
        [StringLength(50)]
        public string MACAddress { get; set; }

        [StringLength(50)]
        public string MachineName { get; set; }
        [Required]
        [StringLength(50)]
        public string IPAddress { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Country { get; set; }

        [Required]
        [StringLength(50)]
        public string Store { get; set; }
        [StringLength(50)]
        public string SubStore { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }

        public string Description { get; set; }

        public virtual ApplicationVersion ApplicationVersion { get; set; }
    }
}
