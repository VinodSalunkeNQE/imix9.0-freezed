namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ObjectAuthorization")]
    public partial class ObjectAuthorization
    {
        public long ObjectAuthorizationId { get; set; }

        public long ApplicationObjectId { get; set; }

        public long ObjectValueId { get; set; }

        public long DigiMasterLocationId { get; set; }

        public virtual ApplicationObject ApplicationObject { get; set; }

        public virtual DigiMasterLocation DigiMasterLocation { get; set; }
        public int? IsDeleted { get; set; }
        //public DateTime CreatedOn { get; set; }
    }
}
