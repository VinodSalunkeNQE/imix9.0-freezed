namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DigiMasterLocation")]
    public partial class DigiMasterLocation
    {
        public DigiMasterLocation()
        {
            //DigiImages = new HashSet<DigiImage>();
            //DigiImages1 = new HashSet<DigiImage>();
            ObjectAuthorizations = new HashSet<ObjectAuthorization>();
            //OrderDetails = new HashSet<OrderDetail>();
            Users = new HashSet<User>();
        }

        public long DigiMasterLocationId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public long? ParentDigiMasterLocationId { get; set; }

        public int Level { get; set; }

        public int? DisplayOrder { get; set; }

        public bool IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
       // public virtual ICollection<DigiImage> DigiImages { get; set; }

       // public virtual ICollection<DigiImage> DigiImages1 { get; set; }

        public virtual ICollection<ObjectAuthorization> ObjectAuthorizations { get; set; }

       // public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public int? LogicalDigiMasterLocationId { get; set; }

        public bool IsLogicalSubStore { get; set; }
    }
}
