namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SB_AT_ServiceBrokerLogs
    {
        [Key]
        public long LogID { get; set; }

        public DateTime LogDate { get; set; }

        public int SPID { get; set; }

        [Required]
        [StringLength(255)]
        public string ProgramName { get; set; }

        [Required]
        [StringLength(255)]
        public string HostName { get; set; }

        public int ErrorSeverity { get; set; }

        public string ErrorMessage { get; set; }

        public int? ErrorLine { get; set; }

        [StringLength(128)]
        public string ErrorProc { get; set; }

        [Column(TypeName = "xml")]
        public string QueueMessage { get; set; }
    }
}
