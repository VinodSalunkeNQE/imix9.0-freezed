namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.Spatial;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;


    [Table("CustomerQRCodeMapping")]
    public partial class CustomerQRCodeMapping
    {
       
        [Key]
        [Column(Order = 0)]
        public long CustomerId { get; set; }


        [Key]
        [Column(Order = 1)]
        public string IdentificationCode { get; set; }

       [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
    }
}
