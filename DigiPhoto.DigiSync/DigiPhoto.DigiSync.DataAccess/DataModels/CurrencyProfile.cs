﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public partial class CurrencyProfile
    {
       public long CurrencyProfileID{get;set;}
       [Required]
       [StringLength(50)]
        public string ProfileName{get;set;}
        [Required]    
        public DateTime StartDate{get;set;}       
        public DateTime? EndDate{get;set;}
        [Required] 
        public  bool IsActive{get;set;}
        [Required] 
        public long CreatedBy{get;set;}
        [Required] 
        public DateTime CreatedOn{get;set;}
        public long? UpdatedBy{get;set;}
        public DateTime? UpdatedOn { get; set; }
        public DateTime? PublishedOn { get; set; }

        [Required, StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsDeleted { get; set; }
    }
}
