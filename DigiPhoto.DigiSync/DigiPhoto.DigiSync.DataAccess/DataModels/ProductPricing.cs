namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductPricing")]
    public partial class ProductPricing
    {
        public int ProductPricingId { get; set; }

        public long ProductId { get; set; }

        public double Price { get; set; }

        public int CurrencyId { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public bool IsActive { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual Product Product { get; set; }
    }
}
