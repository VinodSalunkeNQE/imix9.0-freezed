namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChangeTracking")]
    public partial class ChangeTracking
    {
        public long ChangeTrackingId { get; set; }

        public long ApplicationObjectId { get; set; }

        public long ObjectValueId { get; set; }

        public int ChangeAction { get; set; }

        public DateTime ChangeDate { get; set; }

        public long ChangeBy { get; set; }

        [StringLength(50)]
        public string EntityCode { get; set; }

        public virtual ApplicationObject ApplicationObject { get; set; }

        public Int32 SyncStatus { get; set; }
    }
}
