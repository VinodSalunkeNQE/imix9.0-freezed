namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Bill_Format
    {
        [Key]
        public int DG_Bill_Format_pkey { get; set; }

        public int? DG_Bill_Type { get; set; }

        [StringLength(150)]
        public string DG_Refund_Slogan { get; set; }
    }
}
