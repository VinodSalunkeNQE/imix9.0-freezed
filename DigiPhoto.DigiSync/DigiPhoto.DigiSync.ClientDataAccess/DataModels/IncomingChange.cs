namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IncomingChange")]
    public partial class IncomingChange
    {
        public int IncomingChangeId { get; set; }

        public long ChangeTrackingId { get; set; }

        public long ApplicationObjectId { get; set; }

        public long ObjectValueId { get; set; }

        public int ChangeAction { get; set; }

        public DateTime ChangeDateOnServer { get; set; }

        public long ChangeByOnServer { get; set; }

        public DateTime ChangeRecievedOn { get; set; }

        [Column(TypeName = "xml")]
        public string DataXML { get; set; }

        public long SyncSessionId { get; set; }

        public virtual SyncSession SyncSession { get; set; }

        public int ProcessingStatus { get; set; }
    }
}
