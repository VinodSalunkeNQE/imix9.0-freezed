namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Orders_DiscountType
    {
        [Key]
        public int DG_Orders_DiscountType_Pkey { get; set; }

        [StringLength(250)]
        public string DG_Orders_DiscountType_Name { get; set; }

        [StringLength(500)]
        public string DG_Orders_DiscountType_Desc { get; set; }

        public bool? DG_Orders_DiscountType_Active { get; set; }

        public bool? DG_Orders_DiscountType_Secure { get; set; }

        public bool? DG_Orders_DiscountType_ItemLevel { get; set; }

        public bool? DG_Orders_DiscountType_AsPercentage { get; set; }

        [StringLength(100)]
        public string DG_Orders_DiscountType_Code { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
