namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_CashBox
    {
        public int Id { get; set; }

        [Required]
        public string Reason { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
