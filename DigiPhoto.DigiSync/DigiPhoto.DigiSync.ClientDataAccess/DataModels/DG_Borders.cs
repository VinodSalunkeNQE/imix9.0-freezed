namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Borders
    {
        [Key]
        public int DG_Borders_pkey { get; set; }

        [Required]
        [StringLength(100)]
        public string DG_Border { get; set; }

        public int DG_ProductTypeID { get; set; }

        public bool DG_IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
