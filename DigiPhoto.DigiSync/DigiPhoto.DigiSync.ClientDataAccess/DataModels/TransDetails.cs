﻿namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransDetails", Schema = "Sage")]
    public partial class TransDetails
    {
        [Key]
        public long TransDetailID { get; set; }

        [Required]
        public Int32 SubstoreID { get; set; }

        [Required]
        public DateTime TransDate { get; set; }

        [Required]
        public Int32 PackageID { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Quantity { get; set; }

        public decimal Discount { get; set; }

        public decimal Total { get; set; }

        [Required]
        public long ClosingFormDetailID { get; set; }

    }
}
