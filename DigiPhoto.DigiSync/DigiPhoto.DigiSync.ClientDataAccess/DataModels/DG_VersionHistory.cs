namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_VersionHistory
    {
        [Key]
        public int DG_Version_Pkey { get; set; }

        [StringLength(50)]
        public string DG_Version_Number { get; set; }

        public DateTime? DG_Version_Date { get; set; }

        public int? DG_UpdatedBY { get; set; }

        [StringLength(100)]
        public string DG_Machine { get; set; }
    }
}
