namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Activity
    {
        [Key]
        public int DG_Acitivity_Action_Pkey { get; set; }

        public int? DG_Acitivity_ActionType { get; set; }

        public DateTime? DG_Acitivity_Date { get; set; }

        public int DG_Acitivity_By { get; set; }

        public string DG_Acitivity_Descrption { get; set; }

        public int? DG_Reference_ID { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
