﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ErrorHandler;
using log4net;

namespace DataSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            //DigiSyncService.SyncOrder();
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Sync Service Started");
            //try
            //{
            //    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //    if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryImagesBufferSize") == false)
            //    {
            //        config.AppSettings.Settings.Add("CloudinaryImagesBufferSize", "20971520");
            //    }

            //    if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryVideosBufferSize") == false)
            //    {
            //        config.AppSettings.Settings.Add("CloudinaryVideosBufferSize", "20971520");
            //    }

            //    if (ConfigurationManager.AppSettings.AllKeys.Contains("AuthKey") == false)
            //    {
            //        config.AppSettings.Settings.Add("AuthKey", "123");
            //    }

            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}
            //catch (Exception ex)
            //{
            //    ErrorHandler.ErrorHandler.LogError(ex);
            //}
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new DigiSyncService()
            };
            ServiceBase.Run(ServicesToRun);
            log.Info("Sync Service stopped now at :" + DateTime.Now.ToString());
        }
        //catch (Exception ex)
        //{
        //    log.Info("exception is" + ex.Message);
        //}

    }
}
