﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    class DiscountProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    DiscountInfo discountInfo = CommonUtility.DeserializeXML<DiscountInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingDicountCount = dbContext.DG_Orders_DiscountType.Count(r => r.SyncCode == discountInfo.SyncCode);
                        if (existingDicountCount == 0)
                        {
                            var discount = new DG_Orders_DiscountType();

                            discount.DG_Orders_DiscountType_Name = discountInfo.Name;
                            discount.DG_Orders_DiscountType_Desc = discountInfo.Description;
                            discount.DG_Orders_DiscountType_Active = discountInfo.IsActive;
                            discount.DG_Orders_DiscountType_Code = discountInfo.Code;
                            discount.DG_Orders_DiscountType_Secure = discountInfo.Secure;
                            discount.DG_Orders_DiscountType_ItemLevel = discountInfo.ItemLevel;
                            discount.DG_Orders_DiscountType_AsPercentage = discountInfo.AsPercentage;
                            discount.SyncCode = discountInfo.SyncCode;
                            discount.IsSynced = true;
                            dbContext.DG_Orders_DiscountType.Add(discount);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Discount Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.DiscountProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    DiscountInfo discountInfo = CommonUtility.DeserializeXML<DiscountInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbDiscount = dbContext.DG_Orders_DiscountType.Where(r => r.SyncCode == discountInfo.SyncCode).FirstOrDefault();
                        if (dbDiscount != null)
                        {
                            //Update Role Name
                            dbDiscount.DG_Orders_DiscountType_Name = discountInfo.Name;
                            dbDiscount.DG_Orders_DiscountType_Desc = discountInfo.Description;
                            dbDiscount.DG_Orders_DiscountType_Active = discountInfo.IsActive;
                            dbDiscount.DG_Orders_DiscountType_Code = discountInfo.Code;
                            dbDiscount.DG_Orders_DiscountType_Secure = discountInfo.Secure;
                            dbDiscount.DG_Orders_DiscountType_ItemLevel = discountInfo.ItemLevel;
                            dbDiscount.DG_Orders_DiscountType_AsPercentage = discountInfo.AsPercentage;
                            dbDiscount.IsSynced = true;
                            dbContext.SaveChanges();         
                            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Discount Processor Update.", e);
                outerException.Source = "ClientDataProcessor.DiscountProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                      using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbDiscount = dbContext.DG_Orders_DiscountType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                        if (dbDiscount != null)
                        {
                            dbContext.DG_Orders_DiscountType.Remove(dbDiscount);
                            dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                        }
                    }
               
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Discount Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.DiscountProcessor";
                throw outerException;
            }
        }
    }
}
