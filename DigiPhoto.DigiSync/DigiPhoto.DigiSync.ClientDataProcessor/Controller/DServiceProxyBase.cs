﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Configuration;
using log4net;
using DigiPhoto.DigiSync.ClientDataProcessor;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Net.Security;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Controller
{
    public class DServiceProxy<T>
    {
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Use(Action<T> action)
        {
            BasicHttpBinding binding = new BasicHttpBinding();

            string apiLocation = SyncController.SyncServiceURL();//"https://pushapi4-ubr-cn-staging.chinacloudsites.cn/";// "http://localhost:1857";//"https://pushapi2-ubr-cn-staging.chinacloudsites.cn"; //
            if (string.IsNullOrEmpty(apiLocation))
            {
                apiLocation = ConfigurationManager.AppSettings["API_URL"].ToString();
            }
            
            bool useHttps = false;

            if (apiLocation.Contains("https:"))
            {
                useHttps = true;
            }

            if (typeof(T).Name.StartsWith("I"))
            {
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            }
            else
            {
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";
            }

            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }
            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);

            binding.OpenTimeout = new TimeSpan(0, 0, 60*2);
            binding.CloseTimeout = new TimeSpan(0, 0, 60*2);
            binding.SendTimeout = new TimeSpan(0, 0, 60*2);   //most important
            binding.ReceiveTimeout = new TimeSpan(0, 0, 60*2);

            if (useHttps)
            {
                binding.Security = new BasicHttpSecurity();
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            }

            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;

            try
            {
                if (useHttps)
                    SetCertificatePolicy();


                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            catch (CommunicationException cex)
            {
                log.Error(cex);
                throw cex;
            }
            catch (TimeoutException tex)
            {

                log.Error(tex);
                throw tex;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
        }

        public static bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        { return true; }
        public static void SetCertificatePolicy()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
        }

        private static bool RemoteCertificateValidate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {
            return true;
        }
    }

    // new arch start
    public class DGPullServiceProxy<T>
    {
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Use(Action<T> action)
        {
            BasicHttpBinding binding = new BasicHttpBinding();

            string apiLocation = SyncController.SyncPullServiceURL();
            if (string.IsNullOrEmpty(apiLocation))
            {
                apiLocation = ConfigurationManager.AppSettings["API_URL"].ToString();
            }


            if (typeof(T).Name.StartsWith("I"))
            {
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            }
            else
            {
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";
            }

            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }
            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);

            binding.OpenTimeout = new TimeSpan(0, 0, 60 * 2);
            binding.CloseTimeout = new TimeSpan(0, 0, 60 * 2);
            binding.SendTimeout = new TimeSpan(0, 0, 60 * 2);   //most important
            binding.ReceiveTimeout = new TimeSpan(0, 0, 60 * 2);

            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;

            try
            {
                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            catch (CommunicationException cex)
            {
                log.Error(cex);
                throw cex;
            }
            catch (TimeoutException tex)
            {

                log.Error(tex);
                throw tex;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
        }
    }

    // new arch stop
}
