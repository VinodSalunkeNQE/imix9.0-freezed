﻿using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public interface IProcesser
    {
        void Process(ChangeInfo changeInfo);
    }
}
