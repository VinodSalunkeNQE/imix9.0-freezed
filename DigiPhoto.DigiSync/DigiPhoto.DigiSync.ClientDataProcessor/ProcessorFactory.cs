﻿using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public class ProcessorFactory
    {
        private static Dictionary<long, IProcesser> processors = new Dictionary<long, IProcesser>();
        public static IProcesser Create(long applicationObjectId)
        {
            try
            {
                object objLock = new object();
                lock (objLock)
                {
                    ApplicationObjectEnum applicationObject = (ApplicationObjectEnum)applicationObjectId;
                    string applicationObjectName = applicationObject.ToString();
                    if (applicationObjectName == "Configuration")
                    {
                        string className = applicationObjectName + "ValueProcessor";
                        Type type = Type.GetType("DigiPhoto.DigiSync.ClientDataProcessor." + className);
                        processors[applicationObjectId] = Activator.CreateInstance(type) as IProcesser;
                       
                    }
                    else
                    {
                        string className = applicationObjectName + "Processor";
                        Type type = Type.GetType("DigiPhoto.DigiSync.ClientDataProcessor." + className);
                        processors[applicationObjectId] = Activator.CreateInstance(type) as IProcesser;

                    }
                    return processors[applicationObjectId];
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
