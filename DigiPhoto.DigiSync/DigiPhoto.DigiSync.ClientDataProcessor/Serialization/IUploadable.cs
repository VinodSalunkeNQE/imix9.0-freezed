﻿using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public interface IUploadable
    {
        List<UploadInfo> UploadList { get; set; }
        UploadFile UploadFile { get; set; }
    }
}
