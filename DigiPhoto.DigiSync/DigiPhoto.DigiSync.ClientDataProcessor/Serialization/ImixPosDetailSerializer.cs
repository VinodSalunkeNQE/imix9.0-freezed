﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class ImixPosDetailSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    string errorPhotoId = string.Empty;
                    DG_SubStores subStoreInfo = new DG_SubStores();
                    subStoreInfo = (
                                     from imixPosDetails in dbContext.ImixPosDetails
                                     join subStore in dbContext.DG_SubStores on imixPosDetails.SubStoreID equals subStore.DG_SubStore_pkey
                                     select subStore).FirstOrDefault();
                    if (subStoreInfo == null)
                    {
                        subStoreInfo = new DG_SubStores();
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                        subStoreInfo.SyncCode = string.Empty;
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                    }
                    var imixPosDetail = dbContext.ImixPosDetails.Where(r => r.ImixPOSDetailID == ObjectValueId).FirstOrDefault();
                    if (imixPosDetail != null)
                    {
                        ImixPosDetailInfo imixPosDetailInfo = new ImixPosDetailInfo()
                        {
                            ImixPOSDetailID = imixPosDetail.ImixPOSDetailID,
                            SystemName = imixPosDetail.SystemName,
                            IPAddress = imixPosDetail.IPAddress,
                            MacAddress = imixPosDetail.MacAddress,
                            SubStoreID = imixPosDetail.SubStoreID,
                            IsActive = imixPosDetail.IsActive,
                            CreatedBy = imixPosDetail.CreatedBy,
                            CreatedOn = imixPosDetail.CreatedOn,
                            UpdatedBy = imixPosDetail.UpdatedBy,
                            UpdatedOn = imixPosDetail.UpdatedOn,
                            IsStart = imixPosDetail.IsStart,
                            StartStopTime = imixPosDetail.StartStopTime,
                            SyncCode = imixPosDetail.SyncCode,
                            SubStore = new SubStoreInfo
                            {
                                SubStoreCode = subStoreInfo.DG_SubStore_Name,
                                SyncCode = subStoreInfo.SyncCode,
                                Store = subStoreInfo.DG_SubStore_Name
                            },
                        };

                        dataXML = CommonUtility.SerializeObject<ImixPosDetailInfo>(imixPosDetailInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in ImixPosDetail Serialization.", e);
                outerException.Source = "Serialization.ImixPosDetailinfoSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
