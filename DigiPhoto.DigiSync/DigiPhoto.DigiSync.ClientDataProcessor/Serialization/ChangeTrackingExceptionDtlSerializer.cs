﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    class ChangeTrackingExceptionDtlSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var ChangeTrackingExceptionDtl = (
                                                        from ctexcdtl in dbContext.ChangeTrackingExceptionDtls
                                                        join ct in dbContext.ChangeTrackings on ctexcdtl.ChangeTrackingId equals ct.ChangeTrackingId
                                                        where ctexcdtl.ExceptionDtlID == ObjectValueId
                                                        select new ChangeTrackingExceptionDtlInfo
                                                        {
                                                            ExceptionDtlID = ctexcdtl.ExceptionDtlID,
                                                            ChangeTrackingId = ctexcdtl.ChangeTrackingId,
                                                            ApplicationObjectId = ct.ApplicationObjectId,
                                                            ChangeAction = ct.ChangeAction,
                                                            ChangeDate = ct.ChangeDate,
                                                            ExceptionMsg = ctexcdtl.ExceptionMsg,
                                                            ExceptionStackTrace = ctexcdtl.ExceptionStackTrace,
                                                            ExceptionType = ctexcdtl.ExceptionType,
                                                            ExceptionSource = ctexcdtl.ExceptionSource,
                                                            InnerException = ctexcdtl.InnerException,
                                                            ExceptionOnDate = ctexcdtl.ExceptionOnDate,
                                                            ObjectValueId = ct.ObjectValueId                                                                                                                         
                                                        }
                                                       ).FirstOrDefault();

                    dataXML = CommonUtility.SerializeObject<ChangeTrackingExceptionDtlInfo>(ChangeTrackingExceptionDtl);
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in ChangeTrackingExceptionDtl Serialization.", e);
                outerException.Source = "Serialization.ChangeTrackingExceptionDtlSerializer";
                throw outerException;
            }
        }

        public override object Deserialize(string input)
        {
            throw new NotImplementedException();
        }
    }
}
