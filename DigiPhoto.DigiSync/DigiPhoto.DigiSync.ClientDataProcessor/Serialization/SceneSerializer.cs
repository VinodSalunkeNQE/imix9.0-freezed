﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System.IO;
using System.Configuration;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class SceneSerializer : Serializer, IUploadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {                    
                    SceneInfo sceneInfo = (from s in dbContext.DG_Scene
                                           join p in dbContext.DG_Orders_ProductType on s.DG_Product_Id equals p.DG_Orders_ProductType_pkey
                                           join bg in dbContext.DG_BackGround on s.DG_BackGround_Id equals bg.DG_Background_pkey
                                           join br in dbContext.DG_Borders on s.DG_Border_Id equals br.DG_Borders_pkey
                                           where s.DG_Scene_pkey == ObjectValueId
                                           select new SceneInfo
                                           {
                                               SceneId = s.DG_Scene_pkey,
                                               BackGroundId =bg.DG_Background_pkey,
                                               BackgroundName = bg.DG_BackGround_Image_Name,
                                               BorderId = br.DG_Borders_pkey,
                                               BorderName = br.DG_Border,
                                               GraphicName = string.Empty,
                                               GraphicsId = 0,
                                               IsActive = s.IsActive,
                                               IsSynced = (s.IsSynced.HasValue && s.IsSynced.Value)?s.IsSynced.Value:false,
                                               ProductId = p.DG_Orders_ProductType_pkey,
                                               ProductName = p.DG_Orders_ProductType_Name,
                                               SceneName = s.DG_SceneName,
                                               SyncCode = s.SyncCode,
                                               ProductSyncCode = p.SyncCode,
                                               BackgroundSyncCode = bg.SyncCode,
                                               BorderSyncCode = br.SyncCode

                                           }).FirstOrDefault();

                    if (sceneInfo != null)
                    {
                        dataXML = CommonUtility.SerializeObject<SceneInfo>(sceneInfo);
                    }

                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Scene Serialization.", e);
                outerException.Source = "Serialization.SceneSerializer";
                throw outerException;
            }
        }

        private UploadFile uploadFile;
        public UploadFile UploadFile
        {
            get
            {
                return uploadFile;
            }
            set
            {
                uploadFile = value;
            }
        }
        private List<UploadInfo> uploadList;
        public List<UploadInfo> UploadList
        {
            get
            {
                return uploadList;
            }
            set
            {
                uploadList = value;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

    }
}
