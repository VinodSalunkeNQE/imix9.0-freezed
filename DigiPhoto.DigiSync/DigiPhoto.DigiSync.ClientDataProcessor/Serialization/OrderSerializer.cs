﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class OrderSerializer : Serializer, IUploadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    var store = dbContext.DG_Store.FirstOrDefault();
                    var OrderDetails = (
                                        from order in dbContext.DG_Orders
                                        join orderDetails in dbContext.DG_Orders_Details on order.DG_Orders_pkey equals orderDetails.DG_Orders_ID
                                        join substore in dbContext.DG_SubStores on orderDetails.DG_Order_SubStoreId equals substore.DG_SubStore_pkey
                                        join Prod in dbContext.DG_Orders_ProductType on orderDetails.DG_Orders_Details_ProductType_pkey equals Prod.DG_Orders_ProductType_pkey
                                        join ProdPrice in dbContext.DG_Product_Pricing on Prod.DG_Orders_ProductType_pkey equals ProdPrice.DG_Product_Pricing_ProductType
                                        join Currency in dbContext.DG_Currency on order.DG_Orders_Currency_ID equals Currency.DG_Currency_pkey
                                        join user in dbContext.DG_Users on order.DG_Orders_UserID equals user.DG_User_pkey
                                        where orderDetails.DG_Orders_ID == ObjectValueId
                                        orderby orderDetails.DG_Orders_LineItems_pkey
                                        select new OrderDetailsInfo
                                        {
                                            OrderId = order.DG_Orders_pkey,
                                            OrderDetailId = orderDetails.DG_Orders_LineItems_pkey,
                                            OrderNumber = order.DG_Orders_Number,
                                            IdentificationCode = orderDetails.DG_Order_ImageUniqueIdentifier,
                                            SyncCode = order.SyncCode,
                                            OrderMode = order.DG_Order_Mode,
                                            Cost = order.DG_Orders_Cost,
                                            NetCost = order.DG_Orders_NetCost,
                                            CurrencyConversionRate = order.DG_Orders_Currency_Conversion_Rate,
                                            TotalDiscount = order.DG_Orders_Total_Discount,
                                            PaymentDetails = order.DG_Orders_PaymentDetails,
                                            PaymentMode = order.DG_Orders_PaymentMode,
                                            IsCancelled = order.DG_Orders_Canceled,
                                            CancelledDateTime = order.DG_Orders_Canceled_Date,
                                            Reason = order.DG_Orders_Canceled_Reason,
                                            DiscountAmount = orderDetails.DG_Orders_LineItems_DiscountAmount,
                                            UnitPrice = orderDetails.DG_Orders_Details_Items_UniPrice,
                                            Qty = orderDetails.DG_Orders_LineItems_Quantity,
                                            TotalCost = orderDetails.DG_Orders_Details_Items_TotalCost,
                                            NetPrice = orderDetails.DG_Orders_Details_Items_NetPrice,
                                            IsBurned = orderDetails.DG_Photos_Burned,
                                            Photos = orderDetails.DG_Photos_ID,
                                            PhotosUnSold = orderDetails.DG_Photos_ID_UnSold,
                                            OrderDate = order.DG_Orders_Date.HasValue ? order.DG_Orders_Date.Value : DateTime.Now,
                                            ProductType_pkey = orderDetails.DG_Orders_Details_ProductType_pkey,
                                            LineItemParentId = orderDetails.DG_Orders_Details_LineItem_ParentID,
                                            TotalDiscountDetails = orderDetails.DG_Orders_LineItems_DiscountType,
                                            OrderDetailsSyncCode = orderDetails.SyncCode,
                                            //EmpID = order.EmpID, //TODO
                                            PosName=order.PosName,
                                            SubStore = new SubStoreInfo
                                            {
                                                SubStoreCode = substore.DG_SubStore_Name,
                                                SyncCode = substore.SyncCode,
                                                Store = store.DG_Store_Name,
                                                Country = store.Country,
                                            },
                                            CurrencyInfo = new CurrencyInfo
                                            {
                                                SyncCode = Currency.SyncCode
                                            },
                                            UserInfo = new UserInfo
                                            {
                                                SyncCode = user.SyncCode
                                            },
                                        }).ToList();

                    //fill Order Details package Details
                    foreach (OrderDetailsInfo orderDetail in OrderDetails)
                    {

                        List<PackageInfo> DG_Package = (
                                           from p in dbContext.DG_Orders_ProductType
                                           where p.DG_Orders_ProductType_pkey == orderDetail.ProductType_pkey && p.DG_IsPackage == true
                                           select new PackageInfo
                                           {
                                               PackageId = p.DG_Orders_ProductType_pkey,
                                               Name = p.DG_Orders_ProductType_Name,
                                               SyncCode = p.SyncCode
                                           }
                           ).ToList();
                        orderDetail.PackageInfoList = DG_Package;

                        foreach (PackageInfo item in DG_Package)
                        {
                            List<ProductInfo> DG_Product = (
                                             from p in dbContext.DG_Orders_ProductType

                                             where p.DG_Orders_ProductType_pkey == item.PackageId && p.DG_IsPackage == false
                                             select new ProductInfo
                                             {
                                                 Name = p.DG_Orders_ProductType_Name,
                                                 SyncCode = p.SyncCode
                                             }
                             ).ToList();
                            item.Products = DG_Product;
                        }
                    }
                    //fill Order Detail Product Details
                    foreach (OrderDetailsInfo orderDetail in OrderDetails)
                    {
                        List<ProductInfo> DG_Product = (
                                           from p in dbContext.DG_Orders_ProductType
                                           where p.DG_Orders_ProductType_pkey == orderDetail.ProductType_pkey && p.DG_IsPackage == false
                                           select new ProductInfo
                                           {
                                               Name = p.DG_Orders_ProductType_Name,
                                               SyncCode = p.SyncCode
                                           }
                           ).ToList();
                        orderDetail.ProductInfoList = DG_Product;
                    }
                    //fill PrintQueue Details
                    foreach (OrderDetailsInfo orderDetail in OrderDetails)
                    {
                        List<PrinterQueue> dG_PrinterQueues = (
                                           from p in dbContext.DG_PrinterQueue
                                           where p.DG_Order_Details_Pkey == orderDetail.OrderDetailId
                                           select new PrinterQueue
                                           {
                                               DG_Order_Details_Pkey = p.DG_Order_Details_Pkey,
                                               DG_SentToPrinter = p.DG_SentToPrinter,
                                               is_Active = p.is_Active,
                                               DG_Store_pkey = p.DG_Store_pkey,
                                               ReprintCount = p.ReprintCount
                                           }
                           ).ToList();
                        orderDetail.PrinterQueues = dG_PrinterQueues;
                    }

                    //fill Order Detail Photo Details
                    uploadList = new List<UploadInfo>();
                    foreach (OrderDetailsInfo orderDetail in OrderDetails)
                    {
                        long configMasterIdForImageResize = Convert.ToInt64(IMIXConfigurationValues.OnlineImageResize);
                        string photos = orderDetail.Photos;
                        if (!string.IsNullOrEmpty(photos))
                        {
                            int[] photoArray = photos.Split(',').Select(int.Parse).ToArray();
                            var DG_Photos = (
                                                from p in dbContext.DG_Photos
                                                join c in dbContext.DG_Configuration on p.DG_SubStoreId equals c.DG_Substore_Id
                                                join u in dbContext.DG_Users on p.DG_Photos_UserID equals u.DG_User_pkey
                                                join cv in dbContext.iMIXConfigurationValues on p.DG_SubStoreId equals cv.SubstoreId
                                                where cv.IMIXConfigurationMasterId == configMasterIdForImageResize
                                                join Loc in dbContext.DG_Location on p.DG_Location_Id equals Loc.DG_Location_pkey
                                                where photoArray.Contains(p.DG_Photos_pkey)

                                                select new PhotoInfo
                                                {
                                                    PhotoId = p.DG_Photos_pkey,
                                                    FileName = p.DG_Photos_FileName,
                                                    RFID = p.DG_Photos_RFID,
                                                    CreatedOn = p.DG_Photos_CreatedOn,
                                                    Background = p.DG_Photos_Background,
                                                    Frame = p.DG_Photos_Frame,
                                                    Layering = null,//p.DG_Photos_Layering,
                                                    IsCroped = p.DG_Photos_IsCroped,
                                                    IsRedEye = p.DG_Photos_IsRedEye,
                                                    IsGreen = p.DG_Photos_IsGreen,
                                                    MetaData = null,//p.DG_Photos_MetaData,
                                                    LocationId = p.DG_Location_Id,
                                                    HotFolderPath = c.DG_Hot_Folder_Path,
                                                    LocationSyncCode = Loc.SyncCode,
                                                    OnlineImageResize = cv.ConfigurationValue,
                                                    CaptureDate = p.DateTaken,
                                                    MediaType = p.DG_MediaType,
                                                    CreatebBy = p.DG_Photos_UserID,
                                                    VideoLength = p.DG_VideoLength,
                                                    CreatebBySyncCode = u.SyncCode,
                                                    SubstoreId = p.DG_SubStoreId,
                                                    Email = p.Email,
                                                    Nationality = p.Nationality
                                                }
                                            ).ToList();


                            if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                            {
                                foreach (PhotoInfo p in DG_Photos)
                                {
                                    //var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsEnablePartialEditedImage
                                    //    && ).FirstOrDefault();
                                    //if (configValue != null)
                                    //{
                                    //    partialEditedEnabled = Convert.ToBoolean(configValue.ConfigurationValue);
                                    //}
                                    #region Added by ajay on 11 June  for panorama images sync

                                    var EditImagepath = "EditedImages";
                                    var OrderId = orderDetail.OrderId;
                                    var DG_Orders_Details = dbContext.DG_Orders_Details.Where(m => m.DG_Orders_ID == orderDetail.OrderId).FirstOrDefault();
                                    int Product_Type_PKey = Convert.ToInt16(DG_Orders_Details.DG_Orders_Details_ProductType_pkey);
                                    var ProductDetails = dbContext.DG_Orders_ProductType.Where(m => m.DG_Orders_ProductType_pkey == Product_Type_PKey).FirstOrDefault();

                                    if (ProductDetails.IsPanorama)
                                    {
                                        EditImagepath = EditImagepath + "\\Panorama\\";
                                    }                                   
                                    #endregion
                                    uploadList.Add(new UploadInfo
                                    {
                                        MediaType = p.MediaType == null || p.MediaType == 0 ? 1 : (int)p.MediaType,
                                        UploadFilePath = Path.Combine(p.HotFolderPath, p.MediaType == 2 ? "Videos" : (p.MediaType == 3 ? "ProcessedVideos" : string.Empty), p.CreatedOn.ToString("yyyyMMdd"), p.FileName),
                                        PartialEditedUploadFilePath = Path.Combine(p.HotFolderPath, "PartialEditedImages", p.FileName),
                                        EditedUploadFilePath = Path.Combine(p.HotFolderPath, "EditedImages", p.FileName),
                                        UploadThumbnailFilePath = Path.Combine(p.HotFolderPath, "Thumbnails", p.CreatedOn.ToString("yyyyMMdd"), p.MediaType != 1 ? p.FileName.Remove(p.FileName.IndexOf('.') + 1, 3) + "JPG" : p.FileName),
                                        OnlineImageCompression = p.OnlineImageResize,
                                        //SaveFolderPath = Path.Combine(orderDetail.SubStore.SubStoreCode, orderDetail.OrderNumber, orderDetail.IdentificationCode),
                                        //SaveThumbnailFolderPath = Path.Combine(orderDetail.SubStore.SubStoreCode, orderDetail.OrderNumber, orderDetail.IdentificationCode, "Thumbnails"),
                                        SaveFolderPath = Path.Combine(orderDetail.SubStore.Country, orderDetail.SubStore.Store, orderDetail.SubStore.SubStoreCode, orderDetail.OrderDate.ToString("yyyyMMdd")), // Added by Suraj For Order Images path change.
                                        //SaveFolderPath = Path.Combine(orderDetail.SubStore.Country, orderDetail.SubStore.Store, orderDetail.SubStore.SubStoreCode, orderDetail.OrderDate.ToString("yyyyMMdd"), orderDetail.OrderNumber, orderDetail.IdentificationCode), // Commented by Suraj
                                        SaveThumbnailFolderPath = Path.Combine(orderDetail.SubStore.Country, orderDetail.SubStore.Store, orderDetail.SubStore.SubStoreCode, orderDetail.OrderDate.ToString("yyyyMMdd"), orderDetail.OrderNumber, orderDetail.IdentificationCode, "Thumbnails"),

                                        UploadCompressedFilePath = Path.Combine(p.HotFolderPath, "PrintImages", "temp", orderDetail.OrderNumber, p.FileName),
                                        PhotoId = p.PhotoId,
                                        OrderId = orderDetail.OrderId,
                                        IdentificationCode = orderDetail.IdentificationCode,
                                        LocationId = p.LocationId,
                                        SubstoreId = p.SubstoreId

                                    });
                                    p.MediaType = p.MediaType == 2 || p.MediaType == 3 ? 2 : 1;
                                }
                            }
                            orderDetail.PhotosList = DG_Photos;
                        }
                    }
                    foreach (OrderDetailsInfo orderDetail in OrderDetails)
                    {
                        long configMasterIdForImageResize = Convert.ToInt64(IMIXConfigurationValues.OnlineImageResize);
                        string photosUnSold = orderDetail.PhotosUnSold;
                        if (!string.IsNullOrEmpty(photosUnSold))
                        {
                            int[] photoArrayUnSold = photosUnSold.Split(',').Select(int.Parse).ToArray();
                            var DG_PhotosUnSold = (
                                                from p in dbContext.DG_Photos
                                                join c in dbContext.DG_Configuration on p.DG_SubStoreId equals c.DG_Substore_Id
                                                join u in dbContext.DG_Users on p.DG_Photos_UserID equals u.DG_User_pkey
                                                join cv in dbContext.iMIXConfigurationValues on p.DG_SubStoreId equals cv.SubstoreId
                                                where cv.IMIXConfigurationMasterId == configMasterIdForImageResize
                                                join Loc in dbContext.DG_Location on p.DG_Location_Id equals Loc.DG_Location_pkey
                                                where photoArrayUnSold.Contains(p.DG_Photos_pkey)

                                                select new PhotoInfo
                                                {
                                                    PhotoId = p.DG_Photos_pkey,
                                                    FileName = p.DG_Photos_FileName,
                                                    RFID = p.DG_Photos_RFID,
                                                    CreatedOn = p.DG_Photos_CreatedOn,
                                                    Background = p.DG_Photos_Background,
                                                    Frame = p.DG_Photos_Frame,
                                                    Layering = null,//p.DG_Photos_Layering,
                                                    IsCroped = p.DG_Photos_IsCroped,
                                                    IsRedEye = p.DG_Photos_IsRedEye,
                                                    IsGreen = p.DG_Photos_IsGreen,
                                                    MetaData = null,//p.DG_Photos_MetaData,
                                                    LocationId = p.DG_Location_Id,
                                                    HotFolderPath = c.DG_Hot_Folder_Path,
                                                    LocationSyncCode = Loc.SyncCode,
                                                    OnlineImageResize = cv.ConfigurationValue,
                                                    CaptureDate = p.DateTaken,
                                                    MediaType = p.DG_MediaType,
                                                    CreatebBy = p.DG_Photos_UserID,
                                                    VideoLength = p.DG_VideoLength,
                                                    CreatebBySyncCode = u.SyncCode,
                                                    SubstoreId=p.DG_SubStoreId,
                                                    Email = p.Email,
                                                    Nationality = p.Nationality

                                                }
                                            ).ToList();


                            if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                            {
                                foreach (PhotoInfo p in DG_PhotosUnSold)
                                {
                                    
                                    #region Added by ajay on 11 June  for panorama images sync

                                    var EditImagepath = "EditedImages";
                                    var OrderId = orderDetail.OrderId;
                                    var DG_Orders_Details = dbContext.DG_Orders_Details.Where(m => m.DG_Orders_ID == orderDetail.OrderId).FirstOrDefault();
                                    int Product_Type_PKey = Convert.ToInt16(DG_Orders_Details.DG_Orders_Details_ProductType_pkey);
                                    bool IsProductPanoramic = true;//_objPanoramaBusiness.IsProductPanorama(Product_Type_PKey);

                                    if (IsProductPanoramic)
                                    {
                                        EditImagepath = EditImagepath + "\\Panorama\\";
                                    }
                                    #endregion

                                    uploadList.Add(new UploadInfo
                                    {
                                        MediaType = p.MediaType == null || p.MediaType == 0 ? 1 : (int)p.MediaType,
                                        UploadFilePath = Path.Combine(p.HotFolderPath, p.MediaType == 2 ? "Videos" : (p.MediaType == 3 ? "ProcessedVideos" : string.Empty), p.CreatedOn.ToString("yyyyMMdd"), p.FileName),

                                        EditedUploadFilePath = Path.Combine(p.HotFolderPath, "EditedImages", p.FileName),
                                        UploadThumbnailFilePath = Path.Combine(p.HotFolderPath, "Thumbnails", p.CreatedOn.ToString("yyyyMMdd"), p.MediaType != 1 ? p.FileName.Remove(p.FileName.IndexOf('.') + 1, 3) + "JPG" : p.FileName),
                                        OnlineImageCompression = p.OnlineImageResize,
                                        //SaveFolderPath = Path.Combine(orderDetail.SubStore.SubStoreCode, orderDetail.OrderNumber, orderDetail.IdentificationCode),
                                        //SaveThumbnailFolderPath = Path.Combine(orderDetail.SubStore.SubStoreCode, orderDetail.OrderNumber, orderDetail.IdentificationCode, "Thumbnails"),
                                        SaveFolderPath = Path.Combine(orderDetail.SubStore.Country, orderDetail.SubStore.Store, orderDetail.SubStore.SubStoreCode, orderDetail.OrderDate.ToString("yyyyMMdd"), orderDetail.OrderNumber, orderDetail.IdentificationCode),
                                        SaveThumbnailFolderPath = Path.Combine(orderDetail.SubStore.Country, orderDetail.SubStore.Store, orderDetail.SubStore.SubStoreCode, orderDetail.OrderDate.ToString("yyyyMMdd"), orderDetail.OrderNumber, orderDetail.IdentificationCode, "Thumbnails"),

                                        UploadCompressedFilePath = Path.Combine(p.HotFolderPath, "PrintImages", "temp", orderDetail.OrderNumber, p.FileName),
                                        PhotoId = p.PhotoId,
                                        OrderId = orderDetail.OrderId,
                                        IdentificationCode = orderDetail.IdentificationCode,
                                        LocationId=p.LocationId,
                                        SubstoreId=p.SubstoreId

                                    });
                                    p.MediaType = p.MediaType == 2 || p.MediaType == 3 ? 2 : 1;
                                }
                            }
                            orderDetail.PhotosListUnSold = DG_PhotosUnSold;
                        }
                    }
                    if (OrderDetails.Count > 0)
                    {
                        dataXML = CommonUtility.SerializeObject<List<OrderDetailsInfo>>(OrderDetails);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Order Serialization.", e);
                outerException.Source = "Serialization.OrderSerializer";
                throw outerException;
            }
        }

        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
        private List<UploadInfo> uploadList;
        public List<UploadInfo> UploadList
        {
            get
            {
                return uploadList;
            }
            set
            {
                uploadList = value;
            }
        }
        private UploadFile uploadFile;
        public UploadFile UploadFile
        {
            get
            {
                return uploadFile;
            }
            set
            {
                uploadFile = value;
            }
        }

        public override string GetAdditionalInfo(long ObjectValueId)
        {
            string qrCodes = string.Empty;
            var objOrder = GetOrders(ObjectValueId);
            foreach (var order in objOrder)
            {
                qrCodes = string.IsNullOrEmpty(qrCodes) ? order.QrCodes : "," + order.QrCodes;
            }
            return qrCodes;
        }

        private List<OrderDetailsInfo> GetOrders(long ObjectValueId)
        {
            using (DigiPhotoContext dbContext = new DigiPhotoContext())
            {
                var store = dbContext.DG_Store.FirstOrDefault();
                var OrderDetails = (
                                    from order in dbContext.DG_Orders
                                    join orderDetails in dbContext.DG_Orders_Details on order.DG_Orders_pkey equals orderDetails.DG_Orders_ID
                                    join substore in dbContext.DG_SubStores on orderDetails.DG_Order_SubStoreId equals substore.DG_SubStore_pkey
                                    join Prod in dbContext.DG_Orders_ProductType on orderDetails.DG_Orders_Details_ProductType_pkey equals Prod.DG_Orders_ProductType_pkey
                                    join ProdPrice in dbContext.DG_Product_Pricing on Prod.DG_Orders_ProductType_pkey equals ProdPrice.DG_Product_Pricing_ProductType
                                    join Currency in dbContext.DG_Currency on order.DG_Orders_Currency_ID equals Currency.DG_Currency_pkey
                                    join user in dbContext.DG_Users on order.DG_Orders_UserID equals user.DG_User_pkey
                                    where orderDetails.DG_Orders_ID == ObjectValueId
                                    orderby orderDetails.DG_Orders_LineItems_pkey
                                    select new OrderDetailsInfo
                                    {
                                        OrderId = order.DG_Orders_pkey,
                                        OrderDetailId = orderDetails.DG_Orders_LineItems_pkey,
                                        OrderNumber = order.DG_Orders_Number,
                                        IdentificationCode = orderDetails.DG_Order_ImageUniqueIdentifier,
                                        QrCodes = orderDetails.DG_Order_ImageUniqueIdentifier,
                                        SyncCode = order.SyncCode,
                                        OrderMode = order.DG_Order_Mode,
                                        Cost = order.DG_Orders_Cost,
                                        NetCost = order.DG_Orders_NetCost,
                                        CurrencyConversionRate = order.DG_Orders_Currency_Conversion_Rate,
                                        TotalDiscount = order.DG_Orders_Total_Discount,
                                        PaymentDetails = order.DG_Orders_PaymentDetails,
                                        PaymentMode = order.DG_Orders_PaymentMode,
                                        IsCancelled = order.DG_Orders_Canceled,
                                        CancelledDateTime = order.DG_Orders_Canceled_Date,
                                        Reason = order.DG_Orders_Canceled_Reason,
                                        DiscountAmount = orderDetails.DG_Orders_LineItems_DiscountAmount,
                                        UnitPrice = orderDetails.DG_Orders_Details_Items_UniPrice,
                                        Qty = orderDetails.DG_Orders_LineItems_Quantity,
                                        TotalCost = orderDetails.DG_Orders_Details_Items_TotalCost,
                                        NetPrice = orderDetails.DG_Orders_Details_Items_NetPrice,
                                        IsBurned = orderDetails.DG_Photos_Burned,
                                        Photos = orderDetails.DG_Photos_ID,
                                        PhotosUnSold = orderDetails.DG_Photos_ID_UnSold,
                                        OrderDate = order.DG_Orders_Date.HasValue ? order.DG_Orders_Date.Value : DateTime.Now,
                                        ProductType_pkey = orderDetails.DG_Orders_Details_ProductType_pkey,
                                        LineItemParentId = orderDetails.DG_Orders_Details_LineItem_ParentID,
                                        TotalDiscountDetails = orderDetails.DG_Orders_LineItems_DiscountType,
                                        OrderDetailsSyncCode = orderDetails.SyncCode,
                                        //EmpID = order.EmpID, //TODO
                                        PosName = order.PosName,
                                        SubStore = new SubStoreInfo
                                        {
                                            SubStoreCode = substore.DG_SubStore_Name,
                                            SyncCode = substore.SyncCode,
                                            Store = store.DG_Store_Name,
                                            Country = store.Country,
                                        },
                                        CurrencyInfo = new CurrencyInfo
                                        {
                                            SyncCode = Currency.SyncCode
                                        },
                                        UserInfo = new UserInfo
                                        {
                                            SyncCode = user.SyncCode
                                        },
                                    }).ToList();

                return OrderDetails;
            }
        }
    }
}
