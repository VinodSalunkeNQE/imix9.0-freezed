﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class ClosingFormDetailsSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    string FilledBySyncCode = string.Empty;
                    string PackagSyncCode = string.Empty;
                    string errorPhotoId = string.Empty;
                    DG_SubStores subStoreInfo = new DG_SubStores();
                    subStoreInfo = (
                                     from closingFormDetails in dbContext.ClosingFormDetails
                                     join subStore in dbContext.DG_SubStores on closingFormDetails.SubStoreID equals subStore.DG_SubStore_pkey
                                     where closingFormDetails.ClosingFormDetailID == ObjectValueId
                                     select subStore).FirstOrDefault();
                    if (subStoreInfo == null)
                    {
                        subStoreInfo = new DG_SubStores();
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                        subStoreInfo.SyncCode = string.Empty;
                        subStoreInfo.DG_SubStore_Name = string.Empty;
                    }
                    FilledBySyncCode = (
                                     from closingFormDetails in dbContext.ClosingFormDetails
                                     join users in dbContext.DG_Users on closingFormDetails.FilledBy equals users.DG_User_pkey
                                     where closingFormDetails.ClosingFormDetailID == ObjectValueId
                                     select users.SyncCode).FirstOrDefault();

                    var closingFormDetail = dbContext.ClosingFormDetails.Where(r => r.ClosingFormDetailID == ObjectValueId).FirstOrDefault();
                    if (closingFormDetail != null)
                    {
                        List<TransDetails> trasactionDetails = dbContext.TransDetails.Where(r => r.ClosingFormDetailID == ObjectValueId).ToList();
                        List<TransDetailsInfo> transDetailsInfoList = new List<TransDetailsInfo>();
                        foreach (TransDetails transDetails in trasactionDetails)
                        {
                            TransDetailsInfo transDetailsInfoObj = new TransDetailsInfo()
                            {
                                PackageSyncCode = (from p in dbContext.DG_Orders_ProductType
                                                   where p.DG_Orders_ProductType_pkey == transDetails.PackageID && p.DG_IsPackage == true
                                                   where transDetails.ClosingFormDetailID == ObjectValueId
                                                   select p.SyncCode).FirstOrDefault(),
                                TransDetailID = transDetails.TransDetailID,
                                SubstoreID = transDetails.SubstoreID,
                                TransDate = transDetails.TransDate,
                                PackageID = transDetails.PackageID,
                                UnitPrice = transDetails.UnitPrice,
                                Quantity = transDetails.Quantity,
                                Discount = transDetails.Discount,
                                Total = transDetails.Total,
                                ClosingFormDetailID = transDetails.ClosingFormDetailID
                            };
                            transDetailsInfoList.Add(transDetailsInfoObj);
                        }

                        //Inventory Consumables
                        List<InventoryConsumable> inventoryConsumables = dbContext.InventoryConsumables.Where(ic => ic.ClosingFormDetailID == ObjectValueId).ToList();
                        List<InventoryConsumableInfo> inventoryConsumablesList = new List<InventoryConsumableInfo>();
                        foreach (InventoryConsumable consumable in inventoryConsumables)
                        {
                            InventoryConsumableInfo consumableInfo = new InventoryConsumableInfo()
                            {
                                InventoryConsumablesID = consumable.InventoryConsumablesID,
                                AccessoryID = consumable.AccessoryID,
                                ConsumeValue = consumable.ConsumeValue,
                                AccessorySyncCode = (
                                                        from p in dbContext.DG_Orders_ProductType
                                                        where p.DG_Orders_ProductType_pkey == consumable.AccessoryID && p.DG_IsAccessory == true
                                                        select p.SyncCode
                                                    ).FirstOrDefault()
                            };
                            inventoryConsumablesList.Add(consumableInfo);
                        }

                        ClosingFormDetailsInfo closingFormDetailsInfo = new ClosingFormDetailsInfo()
                        {
                            ClosingFormDetailID = closingFormDetail.ClosingFormDetailID,
                            ClosingNumber6X8 = closingFormDetail.ClosingNumber6X8,
                            ClosingNumber8X10 = closingFormDetail.ClosingNumber8X10,
                            PosterClosingNumber = closingFormDetail.PosterClosingNumber,
                            Auto6X8ClosingPrinterCount = closingFormDetail.Auto6X8ClosingNumber,
                            Auto8X10ClosingPrinterCount = closingFormDetail.Auto8X10ClosingNumber,
                            //Folder6x8 = closingFormDetail.Folder6x8,
                            //Folder8x10 = closingFormDetail.Folder8x10,
                            //FolderCase6x8 = closingFormDetail.FolderCase6x8,
                            //FolderCase8x10 = closingFormDetail.FolderCase8x10,
                            //RovingTicket = closingFormDetail.RovingTicket,
                            //RovingBand = closingFormDetail.RovingBand,
                            //PlasticBag = closingFormDetail.PlasticBag,
                            Westage6x8 = closingFormDetail.Westage6x8,
                            Westage8x10 = closingFormDetail.Westage8x10,
                            PosterWestage = closingFormDetail.PosterWestage,
                            Auto6X8Westage = closingFormDetail.Auto6X8Westage,
                            Auto8x10Westage = closingFormDetail.Auto8X10Westage,
                            Attendance = closingFormDetail.Attendance,
                            LaborHour = closingFormDetail.LaborHour,
                            NoOfCapture = closingFormDetail.NoOfCapture,
                            NoOfPreview = closingFormDetail.NoOfPreview,
                            NoOfImageSold = closingFormDetail.NoOfImageSold,
                            Comments = closingFormDetail.Comments,
                            SubStoreID = closingFormDetail.SubStoreID,
                            ClosingDate = closingFormDetail.ClosingDate,
                            FilledBySyncCode = FilledBySyncCode,
                            FilledBy = closingFormDetail.FilledBy,
                            TransDate = closingFormDetail.TransDate,
                            Cash = closingFormDetail.Cash,
                            CreditCard = closingFormDetail.CreditCard,
                            Amex = closingFormDetail.Amex,
                            FCV = closingFormDetail.FCV,
                            RoomCharges = closingFormDetail.RoomCharges,
                            KVL = closingFormDetail.KVL,
                            Vouchers = closingFormDetail.Vouchers,
                            PrintCount6x8 = closingFormDetail.PrintCount6x8,
                            PrintCount8x10 = closingFormDetail.PrintCount8x10,
                            PosterPrintCount = closingFormDetail.PosterPrintCount,
                            SyncCode = closingFormDetail.SyncCode,
                            NoOfTransactions = closingFormDetail.NoOfTransactions,
                            SubStore = new SubStoreInfo
                            {
                                SubStoreCode = subStoreInfo.DG_SubStore_Name,
                                SyncCode = subStoreInfo.SyncCode,
                                Store = subStoreInfo.DG_SubStore_Name
                            },
                            TransDetails = transDetailsInfoList,
                            InventoryConsumables = inventoryConsumablesList
                        };
                        dataXML = CommonUtility.SerializeObject<ClosingFormDetailsInfo>(closingFormDetailsInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in ClosingFormDetails Serialization.", e);
                outerException.Source = "Serialization.ClosingFormDetailsinfoSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
