﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    class ProductSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;
                    var product = dbContext.DG_Orders_ProductType.Where(r => r.DG_Orders_ProductType_pkey == ObjectValueId).FirstOrDefault();
                    ProductInfo productInfo = new ProductInfo();
                    if (product != null)
                    {
                        {
                            productInfo.ProductId = product.DG_Orders_ProductType_pkey;
                            productInfo.Name = product.DG_Orders_ProductType_Name;
                            productInfo.Description = product.DG_Orders_ProductType_Desc;
                            productInfo.Code = product.DG_Orders_ProductCode;
                            productInfo.ProductNumber = product.DG_Orders_ProductNumber.ToString();
                            productInfo.ImagePath = product.DG_Orders_ProductType_Image;
                            productInfo.MaximumQuantity = product.DG_MaxQuantity;
                            productInfo.IsBundled = product.DG_Orders_ProductType_IsBundled;
                            productInfo.IsAccessory = product.DG_IsAccessory;
                            productInfo.IsActive = product.DG_IsActive;
                            productInfo.IsDiscountApplied = product.DG_Orders_ProductType_DiscountApplied;
                            productInfo.IsPrimary = product.DG_IsPrimary;
                            productInfo.IsPackage = product.DG_IsPackage;
                            productInfo.SyncCode = product.SyncCode;
                            productInfo.IsPersonalizedAR = product.IsPersonalizedAR;
                              var siteInfo = dbContext.DG_SubStores.Where(r => r.DG_SubStore_pkey == product.DG_SubStore_pkey).FirstOrDefault();
                              if (siteInfo != null)
                              {
                                  productInfo.LocationSyncCode = siteInfo.SyncCode;
                              }

                            var productprice = dbContext.DG_Product_Pricing.Where(r => r.DG_Product_Pricing_ProductType == product.DG_Orders_ProductType_pkey).FirstOrDefault();
                            if (productprice != null)
                            {
                                productInfo.Price = productprice.DG_Product_Pricing_ProductPrice;
                                //productInfo.CurrencyID = productprice.DG_Product_Pricing_Currency_ID;
                                // to do discussion on currencyid (foreign key issue)
                                productInfo.CreatedDateTime = productprice.DG_Product_Pricing_UpdateDate;
                                productInfo.Storeid = productprice.DG_Product_Pricing_StoreId;
                                productInfo.IsAvailable = productprice.DG_Product_Pricing_IsAvaliable;

                                var users = dbContext.DG_Users.Where(r => r.DG_User_pkey == productprice.DG_Product_Pricing_CreatedBy).FirstOrDefault();
                                if (users != null)
                                {
                                    productInfo.CreatedBy = productprice.DG_Product_Pricing_CreatedBy;
                                    productInfo.UserSyncCode = users.SyncCode;
                                }
                            }
                            var packageproduct = dbContext.DG_PackageDetails.Where(p => p.DG_PackageId == productInfo.ProductId && p.DG_Product_Quantity > 0).ToList();
                            List<PackageProductInfo> pps = new List<PackageProductInfo>();

                            foreach (DG_PackageDetails pp in packageproduct)
                            {
                                PackageProductInfo ppi = new PackageProductInfo();
                                ppi.PackageId = pp.DG_PackageId;
                                ppi.ProductId = pp.DG_ProductTypeId;
                                ppi.ProductMaxImage = pp.DG_Product_MaxImage;
                                ppi.ProductQuantity = pp.DG_Product_Quantity;
                                ppi.ProductSyncCode = dbContext.DG_Orders_ProductType.Where(p => p.DG_Orders_ProductType_pkey == pp.DG_ProductTypeId).FirstOrDefault().SyncCode;
                                pps.Add(ppi);
                            }
                            productInfo.packageProductInfoList = pps;
                        }
                        dataXML = CommonUtility.SerializeObject<ProductInfo>(productInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Product Serialization.", e);
                outerException.Source = "Serialization.ProductSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }


    }
}
