﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public class BackgroundProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    BackgroundInfo backgroundInfo = CommonUtility.DeserializeXML<BackgroundInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        ProductInfo productInfo = (from p in dbContext.DG_Orders_ProductType
                                                   where p.SyncCode == backgroundInfo.Product.SyncCode
                                                   select new ProductInfo
                                                   {
                                                       ProductId = p.DG_Orders_ProductType_pkey
                                                   }).FirstOrDefault();
                        int backGroupId = 1;
                        var maxBackGroup = dbContext.DG_BackGround.OrderByDescending(r => r.DG_BackGround_Group_Id).FirstOrDefault();
                        if (maxBackGroup != null)
                        {
                            backGroupId = (int)maxBackGroup.DG_BackGround_Group_Id + 1;
                        }
                        var existingBackgroundCount = dbContext.DG_BackGround.Count(r => r.SyncCode == backgroundInfo.SyncCode);
                        if (existingBackgroundCount == 0)
                        {
                            var Background = new DG_BackGround();
                            Background.DG_BackGround_Group_Id = backgroundInfo.BackgroundGroupId;
                            Background.DG_BackGround_Image_Display_Name = backgroundInfo.DisplayName;
                            Background.DG_Product_Id = productInfo != null ? Convert.ToInt32(productInfo.ProductId) : 0;
                            Background.DG_BackGround_Image_Name = backgroundInfo.BackgroundImage;
                            Background.DG_BackGround_Group_Id = backGroupId;
                            Background.SyncCode = backgroundInfo.SyncCode;
                            Background.IsSynced = true;
                            Background.DG_Background_IsActive = backgroundInfo.IsActive;
                            Background.CreatedBy = 1;
                            Background.CreatedDate = DateTime.Now;
                            Background.ModifiedBy = 1;
                            Background.ModifiedDate = DateTime.Now;
                            dbContext.DG_BackGround.Add(Background);
                            dbContext.SaveChanges();
                            Background.DG_BackGround_Group_Id = Background.DG_Background_pkey;
                            dbContext.SaveChanges();
                            //var background = dbContext.DG_BackGround.Where(x => x.DG_Background_pkey == Background.DG_Background_pkey).FirstOrDefault();
                            //if (background != null)
                            //{
                            //    background.DG_BackGround_Group_Id = Convert.ToInt16(Background.DG_Background_pkey);
                            //    dbContext.SaveChanges();
                            //}
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Background Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.BackgroundProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    BackgroundInfo backgroundInfo = CommonUtility.DeserializeXML<BackgroundInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        ProductInfo productInfo = (from p in dbContext.DG_Orders_ProductType
                                                   where p.SyncCode == backgroundInfo.Product.SyncCode
                                                   select new ProductInfo
                                                   {
                                                       ProductId = p.DG_Orders_ProductType_pkey
                                                   }).FirstOrDefault();

                        var dbBackground = dbContext.DG_BackGround.Where(r => r.SyncCode == backgroundInfo.SyncCode).FirstOrDefault();
                        if (dbBackground != null)
                        {
                            //Update Border Name
                            //dbBackground.DG_BackGround_Group_Id=backgroundInfo.BackgroundGroupId;
                            dbBackground.DG_BackGround_Image_Display_Name = backgroundInfo.DisplayName;
                            dbBackground.DG_Product_Id = productInfo != null ? Convert.ToInt32(productInfo.ProductId) : 0;
                            dbBackground.DG_BackGround_Image_Name = backgroundInfo.BackgroundImage;
                            dbBackground.SyncCode = backgroundInfo.SyncCode;
                            dbBackground.ModifiedBy = 1;
                            dbBackground.ModifiedDate = DateTime.Now;
                            dbBackground.DG_Background_IsActive = backgroundInfo.IsActive.HasValue ? backgroundInfo.IsActive.Value : false;
                            dbBackground.IsSynced = true;
                            dbContext.SaveChanges();
                            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Background Processor Update.", e);
                outerException.Source = "ClientDataProcessor.BackgroundProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {

                    var dbBackground = dbContext.DG_BackGround.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbBackground != null)
                    {
                        dbContext.DG_BackGround.Remove(dbBackground);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                    }
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Background Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.BackgroundProcessor";
                throw outerException;
            }
        }
    }
}
