﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    [MessageContract]
    public class RemoteFileBytesInfo
    {
        //[MessageHeader(MustUnderstand = true)]
        [MessageBodyMember()]
        public string FileName { get; set; }

        //[MessageHeader(MustUnderstand = true)]
        [MessageBodyMember()]
        public int FileLength { get; set; }

        //[MessageBodyMember(Order = 1)]
        //public byte[] Filebytes;
        [MessageBodyMember()]
        public string bytes { get; set; }

        //[MessageHeader(MustUnderstand = true)]
        [MessageBodyMember()]
        public string SaveFolderPath { get; set; }

        [MessageBodyMember()]
        public byte[] FileBytes { get; set; }
    }
}