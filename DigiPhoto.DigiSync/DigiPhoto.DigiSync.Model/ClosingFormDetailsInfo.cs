﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class ClosingFormDetailsInfo
    {

        public long ClosingFormDetailID { get; set; }

        // BY KCB ON 09 AUG 2019 FOR storing panorama printer inventory
        public long ClosingNumber6900 { get; set; }
        public long Auto6900ClosingPrinterCount { get; set; }
        public long Westage6900 { get; set; }
        //END

        public long ClosingNumber6X8 { get; set; }

        public long ClosingNumber8X10 { get; set; }

        public long PosterClosingNumber { get; set; }

        public long Auto6X8ClosingPrinterCount { get; set; }

        public long Auto8X10ClosingPrinterCount { get; set; }

        public long Westage6x8 { get; set; }

        public long Westage8x10 { get; set; }

        public long PosterWestage { get; set; }

        public long Auto6X8Westage { get; set; }

        public long Auto8x10Westage { get; set; }

        public Int32 Attendance { get; set; }

        public decimal LaborHour { get; set; }

        public long NoOfCapture { get; set; }

        public long NoOfPreview { get; set; }

        public long NoOfImageSold { get; set; }

        public string Comments { get; set; }

        public Int32 SubStoreID { get; set; }

        public DateTime ClosingDate { get; set; }

        public Int32 FilledBy { get; set; }

        public string FilledBySyncCode { get; set; }

        public DateTime TransDate { get; set; }

        public decimal? Cash { get; set; }

        public decimal? CreditCard { get; set; }

        public decimal? Amex { get; set; }

        public decimal? FCV { get; set; }

        public decimal? RoomCharges { get; set; }

        public decimal? KVL { get; set; }

        public decimal? Vouchers { get; set; }

        public long PrintCount6x8 { get; set; }

        public long PrintCount8x10 { get; set; }
        public long? PosterPrintCount { get; set; }

        public string SyncCode { get; set; }

        public SubStoreInfo SubStore { get; set; }

        public List<TransDetailsInfo> TransDetails { get; set; }
        public List<InventoryConsumableInfo> InventoryConsumables { get; set; }
        public int NoOfTransactions { get; set; }
    }
}
