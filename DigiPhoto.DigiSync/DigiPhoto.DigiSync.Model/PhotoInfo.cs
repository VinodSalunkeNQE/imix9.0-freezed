﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class PhotoInfo
    {
        public int PhotoId { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string RFID { get; set; }
        public string HotFolderPath { get; set; }
        public string OnlineImageResize { get; set; }
        public string Background { get; set; }
        public string Frame { get; set; }
        public string Layering { get; set; }
        public bool? IsCroped { get; set; }
        public bool? IsRedEye { get; set; }
        public bool? IsGreen { get; set; }
        public string MetaData { get; set; }
        public int? LocationId { get; set; }

        public int? SubstoreId { get; set; }
        public string LocationSyncCode { get; set; }
        public DateTime? CaptureDate { get; set; }
        public int? MediaType { get; set; }
        public decimal? VideoLength { get; set; }

        public int? CreatebBy { get; set; }

        public string CreatebBySyncCode { get; set; }

        //Added to claim by emailid in dbo.photos by shweta
        public string Email { get; set; }

        public int? Nationality { get; set; }
        //end
    }
}
