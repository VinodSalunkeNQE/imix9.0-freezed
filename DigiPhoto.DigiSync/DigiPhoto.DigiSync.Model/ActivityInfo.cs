﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class ActivityInfo
    {
        public int AcitivityId { get; set; }
        public int? ActionType { get; set; }
        public DateTime? AcitivityDate { get; set; }
        public int AcitivityBy { get; set; }
        public string AcitivityDescrption { get; set; }
        public int? ReferenceID { get; set; }
        public string SyncCode { get; set; }
        public bool IsSynced{ get; set; }
        public string UserSyncCode { get; set; }
    }
}
