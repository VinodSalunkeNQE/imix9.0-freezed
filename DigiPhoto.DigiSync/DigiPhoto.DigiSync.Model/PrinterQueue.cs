using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class PrinterQueue
    {
        [Key]
        public int DG_PrinterQueue_Pkey { get; set; }

        public int? DG_PrinterQueue_ProductID { get; set; }

        public string DG_PrinterQueue_Image_Pkey { get; set; }

        public int? DG_Associated_PrinterId { get; set; }

        public int? DG_Order_Details_Pkey { get; set; }

        public bool? DG_SentToPrinter { get; set; }

        public bool? is_Active { get; set; }

        public int? QueueIndex { get; set; }

        public bool? DG_IsSpecPrint { get; set; }

        public DateTime? DG_Print_Date { get; set; }

        public string RotationAngle { get; set; }

        public int? DG_Store_pkey { get; set; }

        public int? ReprintCount { get; set; }

    }
}
