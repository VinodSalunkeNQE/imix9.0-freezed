﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class InventoryConsumableInfo
    {
        public long InventoryConsumablesID { get; set; }
        public long AccessoryID { get; set; }
        public string AccessorySyncCode { get; set; }
        public long ConsumeValue { get; set; }
    }
}
