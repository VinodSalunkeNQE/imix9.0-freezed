﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class ApplicationClientVersionInfo
    {
        public int ApplicationClientVersionID { get; set; }
        public int ApplicationVersionID { get; set; }
        public string MACAddress { get; set; }
        public string MachineName { get; set; }
        public string IPAddress { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Country { get; set; }
        public string Store { get; set; }
        public string SubStore { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string SyncCode { get; set; }
    }

}
