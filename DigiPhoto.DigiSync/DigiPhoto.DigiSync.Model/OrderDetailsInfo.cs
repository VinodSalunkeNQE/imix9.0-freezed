﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class OrderDetailsInfo
    {
        public int? OrderDetailId { get; set; }
        public string OrderNumber { get; set; }
        public string IdentificationCode { get; set; }
        public List<PackageInfo> PackageInfoList { get; set; }
        public string Photos { get; set; }
        public string PhotosUnSold { get; set; }
        public SubStoreInfo SubStore { get; set; }
        public List<PhotoInfo> PhotosList { get; set; }
        public List<PhotoInfo> PhotosListUnSold { get; set; }
        public DateTime OrderDate { get; set; }
        public string SyncCode { get; set; }
        public UserInfo UserInfo { get; set; }
        public CurrencyInfo CurrencyInfo { get; set; }

        public String OrderMode { get; set; }
        public decimal? NetCost { get; set; }

        public decimal? Cost { get; set; }
        public string CurrencyConversionRate { get; set; }
        public double? TotalDiscount { get; set; }

        public string TotalDiscountDetails { get; set; }

        public int? PaymentMode { get; set; }

        public string PaymentDetails { get; set; }
        public bool? IsCancelled { get; set; }

        public DateTime? CancelledDateTime { get; set; }
        public string Reason { get; set; }

        public long OrderCreatedBy { get; set; }

        public List<ProductInfo> ProductInfoList { get; set; }

        public decimal? DiscountAmount { get; set; }
        public decimal? UnitPrice { get; set; }

        public int? Qty { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? NetPrice { get; set; }

        public bool? IsBurned { get; set; }
        
       public int? ProductType_pkey {get; set;}
       public long? LineItemParentId { get; set; }
       public string OrderDetailsSyncCode { get; set; }

       public Int32 OrderId { get; set; }
       public string PosName { get; set; }

        public string QrCodes { get; set; }

        public List<PrinterQueue> PrinterQueues { get; set; }
    }
}
