﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using System.Data.Entity.Validation;
using log4net;
//using System.Configuration;
namespace DigiPhoto.DigiSync.Business.Processing
{
    public class OrderProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        long OrdetailsId = 0;
        int AlbumId = 0;

        protected override void ProcessInsert()
        {
            bool isError = false;
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    List<OrderDetailsInfo> orderDetailList = CommonUtility.DeserializeXML<List<OrderDetailsInfo>>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        foreach (OrderDetailsInfo orderDetail in orderDetailList)
                        {

                            List<int> imageid = new List<int>();
                            List<string> photoIdList = new List<string>();
                            List<string> photoIdListUnSold = new List<string>();
                            if (orderDetail.Photos != null)
                                photoIdList = orderDetail.Photos.Split(',').ToList();
                            if (orderDetail.PhotosUnSold != null)
                                photoIdListUnSold = orderDetail.PhotosUnSold.Split(',').ToList();
                            int photoCount = 0;
                            int existsOrder = dbContext.OrderMasters.Count(x => x.SyncCode == orderDetail.SyncCode);
                            if (existsOrder == 0)
                            {
                                long userId = 0;
                                long subStoreId = 0;
                                Int32 currencyId = 0;
                                //Added to insert PartnerID in dbo.photos by shweta
                                long? venueId = 0;
                                //end
                                //Int32 currencyId = dbContext.Currencies.Where(x => x.SyncCode == orderDetail.CurrencyInfo.SyncCode).FirstOrDefault().CurrencyId;
                                //here i will check for user and substoreid and currency id
                                if (dbContext.Users.Where(x => x.SyncCode == orderDetail.UserInfo.SyncCode).FirstOrDefault() != null)
                                {
                                    userId = dbContext.Users.Where(x => x.SyncCode == orderDetail.UserInfo.SyncCode).FirstOrDefault().UserId;
                                }
                                else
                                {
                                    throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.UserMissing + "@@@@ Order failed because user with sync code " + orderDetail.UserInfo.SyncCode + " does not exist.");

                                }

                                if (dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault() != null)
                                {
                                    subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    venueId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().ParentDigiMasterLocationId;
                                    //end
                                }
                                else
                                {
                                    throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.SiteMissing + "@@@@ Order failed because site with sync code " + orderDetail.SubStore.SyncCode + " does not exist.");
                                }

                                if (dbContext.Currencies.Where(x => x.SyncCode == orderDetail.CurrencyInfo.SyncCode).FirstOrDefault() != null)
                                {
                                    currencyId = dbContext.Currencies.Where(x => x.SyncCode == orderDetail.CurrencyInfo.SyncCode).FirstOrDefault().CurrencyId;
                                }
                                else
                                {
                                    throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.CurrencyMissing + "@@@@ Order failed because currency with sync code " + orderDetail.CurrencyInfo.SyncCode + " does not exist.");
                                }


                                if (currencyId > 0)
                                {
                                    #region Add OrderMaster

                                    var Order = new OrderMaster();
                                    Order.OrderNumber = orderDetail.OrderNumber;
                                    Order.OrderDate = orderDetail.OrderDate;
                                    Order.AlbumId = AlbumId;
                                    Order.OrderMode = orderDetail.OrderMode;
                                    Order.Cost = orderDetail.Cost;
                                    Order.NetCost = orderDetail.NetCost;
                                    Order.CurrencyId = currencyId;
                                    Order.CurrencyConversionRate = orderDetail.CurrencyConversionRate;
                                    Order.TotalDiscount = orderDetail.TotalDiscount;
                                    Order.PaymentMode = orderDetail.PaymentMode;
                                    Order.PaymentDetails = orderDetail.PaymentDetails;
                                    Order.IsCancelled = orderDetail.IsCancelled;
                                    Order.CancelledDateTime = orderDetail.CancelledDateTime;
                                    Order.Reason = orderDetail.Reason;
                                    Order.OrderCreatedBy = userId;

                                    Order.SyncCode = orderDetail.SyncCode;
                                    Order.SubStoreId = subStoreId;
                                    Order.IsSynced = true;
                                    Order.PosName = orderDetail.PosName;
                                    dbContext.OrderMasters.Add(Order);
                                    dbContext.SaveChanges();
                                    #endregion
                                    long OrderMasterId = Order.OrderMasterId;

                                    foreach (PhotoInfo photo in orderDetail.PhotosList)
                                    {
                                        var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photo.PhotoId && p.SubStoreId == subStoreId && p.FileName == photo.FileName).FirstOrDefault();

                                        //here will check for location
                                        long LocationId = 0;// dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        long PartnerID = 0;
                                        //end
                                        if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault() != null)
                                        {
                                            LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photo.LocationSyncCode + " deos not exist.");
                                        }
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                        {
                                            PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                        }
                                        //end
                                        

                                        if (photoIdList != null && photoIdList.Count > 0)
                                            photoCount = photoIdList.Where(s => s.Contains(photo.PhotoId.ToString())).Count();
                                        #region Add Photo
                                        if (photos == null)
                                        {
                                            var image = new Photo();
                                            decimal? GpsLatitude;
                                            decimal? GpsLongitude;
                                            LoadXml(photo.MetaData, out GpsLatitude, out GpsLongitude);
                                            image.FileName = photo.FileName;
                                            image.RFID = photo.RFID;
                                            image.SubStoreId = subStoreId;
                                            image.CreatedDate = System.DateTime.Now;
                                            image.CreatedOnMIX = photo.CreatedOn;
                                            image.CaptureDate = photo.CaptureDate;
                                            image.IMIXPhotoId = photo.PhotoId;
                                            image.Background = photo.Background;
                                            image.Frame = photo.Frame;
                                            image.IsCroped = photo.IsCroped;
                                            image.IsRedEye = photo.IsRedEye;
                                            //Added to insert PartnerID in dbo.photos by shweta
                                            image.PartnerID = PartnerID;
                                            //end
                                            image.IsGreen = photo.IsGreen;
                                            image.MetaData = photo.MetaData;
                                            image.CreatedBy = userId;
                                            image.LocationId = LocationId;
                                            image.Active = true;
                                            image.GPSLatitude = GpsLatitude;
                                            image.GPSLongitude = GpsLongitude;
                                            if (photo.MediaType != null)
                                            {
                                                image.MediaType = photo.MediaType;
                                            }
                                            else
                                            {
                                                image.MediaType = 1;
                                            }
                                            image.VideoLength = photo.VideoLength;
                                            image.Email = photo.Email;
                                            image.Nationality = photo.Nationality;
                                            dbContext.Photos.Add(image);
                                            dbContext.SaveChanges();
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(image.PhotoId);
                                            photos = new Photo();
                                            photos.PhotoId = image.PhotoId;
                                            photos.FileName = image.FileName;
                                        }
                                        else
                                        {
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(photos.PhotoId);
                                        }
                                        #endregion
                                        if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                        {
                                            var webPhotos = dbContext.WebPhotos.Where(wp => wp.Photo.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                            if (webPhotos == null)
                                            {
                                                var WebPhoto = new WebPhoto();
                                                WebPhoto.Photo.PhotoId = photos.PhotoId;
                                                WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                                WebPhoto.IsPaidImage = true;
                                                WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                                string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                                WebPhoto.CloudSourceImageURL = CloudImageURL;
                                                dbContext.WebPhotos.Add(WebPhoto);
                                                dbContext.SaveChanges();
                                            }
                                        }
                                    }
                                    foreach (PhotoInfo photoUnSold in orderDetail.PhotosListUnSold)
                                    {
                                        var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photoUnSold.PhotoId && p.SubStoreId == subStoreId && p.FileName == photoUnSold.FileName).FirstOrDefault();
                                        long LocationId = 0;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        long PartnerID = 0;
                                        //end
                                        if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault() != null)
                                        {
                                            LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photoUnSold.LocationSyncCode + " deos not exist.");
                                        }
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                        {
                                            PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                        }
                                        //end
                                       
                                        if (photoIdListUnSold != null && photoIdListUnSold.Count > 0)
                                            photoCount = photoIdList.Where(s => s == photoUnSold.PhotoId.ToString()).Count();
                                        #region Add Photo
                                        if (photos == null)
                                        {
                                            var image = new Photo();
                                            decimal? GpsLatitude;
                                            decimal? GpsLongitude;
                                            LoadXml(photoUnSold.MetaData, out GpsLatitude, out GpsLongitude);
                                            image.FileName = photoUnSold.FileName;
                                            image.RFID = photoUnSold.RFID;
                                            image.SubStoreId = subStoreId;
                                            image.CreatedDate = System.DateTime.Now;
                                            image.CreatedOnMIX = photoUnSold.CreatedOn;
                                            image.CaptureDate = photoUnSold.CaptureDate;
                                            image.IMIXPhotoId = photoUnSold.PhotoId;
                                            image.Background = photoUnSold.Background;
                                            image.Frame = photoUnSold.Frame;
                                            image.IsCroped = photoUnSold.IsCroped;
                                            image.IsRedEye = photoUnSold.IsRedEye;
                                            //Added to insert PartnerID in dbo.photos by shweta
                                            image.PartnerID = PartnerID;
                                            //end
                                            image.IsGreen = photoUnSold.IsGreen;
                                            image.MetaData = photoUnSold.MetaData;
                                            image.CreatedBy = userId;
                                            image.LocationId = LocationId;
                                            image.Active = true;
                                            image.GPSLatitude = GpsLatitude;
                                            image.GPSLongitude = GpsLongitude;
                                            if (photoUnSold.MediaType != null)
                                            {
                                                image.MediaType = photoUnSold.MediaType;
                                            }
                                            else
                                            {
                                                image.MediaType = 1;
                                            }
                                            image.VideoLength = photoUnSold.VideoLength;
                                            image.Email = photoUnSold.Email;
                                            image.Nationality = photoUnSold.Nationality;
                                            dbContext.Photos.Add(image);
                                            dbContext.SaveChanges();
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(image.PhotoId);
                                            photos = new Photo();
                                            photos.PhotoId = image.PhotoId;
                                            photos.FileName = image.FileName;
                                        }
                                        else
                                        {
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(photos.PhotoId);
                                        }
                                        #endregion
                                        if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                        {
                                            var webPhotos = dbContext.WebPhotos.Where(wp => wp.Photo.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                            if (webPhotos == null)
                                            {

                                                var WebPhoto = new WebPhoto();
                                                WebPhoto.Photo.PhotoId = photos.PhotoId;
                                                WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                                WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                                string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                                WebPhoto.CloudSourceImageURL = CloudImageURL;
                                                WebPhoto.IsPaidImage = false;
                                                dbContext.WebPhotos.Add(WebPhoto);
                                                dbContext.SaveChanges();
                                            }
                                        }
                                    }
                                    foreach (PackageInfo package in orderDetail.PackageInfoList)
                                    {
                                        long PackageId = 0;
                                        if (dbContext.Packages.Where(x => x.SyncCode == package.SyncCode).FirstOrDefault() != null)
                                        {
                                            PackageId = dbContext.Packages.Where(x => x.SyncCode == package.SyncCode).FirstOrDefault().PackageId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.PackageMissing + "@@@@ Order failed becasue package with sync code " + package.SyncCode + " does not exist.");
                                        }
                                        // var OrderDetails = new OrderDetail();
                                        var chkOrderDetails = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                        if (chkOrderDetails == null)
                                        {
                                            #region Add Order Detail
                                            var OrderDetails = new OrderDetail();
                                            OrderDetails.OrderMasterId = OrderMasterId;
                                            OrderDetails.PackageId = PackageId;
                                            OrderDetails.DiscountAmount = orderDetail.DiscountAmount;
                                            OrderDetails.UnitPrice = orderDetail.UnitPrice;
                                            OrderDetails.Quantity = orderDetail.Qty;
                                            OrderDetails.TotalCost = orderDetail.TotalCost;
                                            OrderDetails.NetPrice = orderDetail.NetPrice;
                                            OrderDetails.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                            OrderDetails.CreatedDateTime = DateTime.Now;
                                            OrderDetails.PhotoId = "";// string.Join(",", imageid);
                                            if (orderDetail.LineItemParentId == -1)
                                            {
                                                OrderDetails.LineItemParentId = -1;
                                            }
                                            else
                                            {
                                                OrderDetails.LineItemParentId = orderDetail.LineItemParentId.HasValue ? orderDetail.LineItemParentId : -1;
                                            }
                                            OrderDetails.DiscountDetails = orderDetail.TotalDiscountDetails;
                                            OrderDetails.SyncCode = orderDetail.OrderDetailsSyncCode;
                                            dbContext.OrderDetails.Add(OrderDetails);
                                            dbContext.SaveChanges();
                                            OrdetailsId = OrderDetails.OrderDetailId;
                                            #endregion
                                        }
                                        else
                                        {
                                            OrdetailsId = chkOrderDetails.OrderDetailId;
                                        }



                                        foreach (ProductInfo prodPackage in package.Products)
                                        {

                                            long ProductId = 0;

                                            if (dbContext.Products.Where(x => x.SyncCode == prodPackage.SyncCode).FirstOrDefault() != null)
                                            {
                                                ProductId = dbContext.Products.Where(x => x.SyncCode == prodPackage.SyncCode).FirstOrDefault().ProductId;
                                            }
                                            else
                                            {
                                                throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.ProductMissing + "@@@@ Order failed becasue product with sync code " + prodPackage.SyncCode + " does not exist.");
                                            }
                                            #region Add OrderDetail
                                            var chkOrderDetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                            if (chkOrderDetail == null)
                                            {
                                                var OrderDetail = new OrderDetail();
                                                OrderDetail.OrderMasterId = OrderMasterId;
                                                OrderDetail.ProductId = ProductId;
                                                OrderDetail.DiscountAmount = orderDetail.DiscountAmount;
                                                OrderDetail.UnitPrice = orderDetail.UnitPrice;
                                                OrderDetail.Quantity = orderDetail.Qty;
                                                OrderDetail.TotalCost = orderDetail.TotalCost;
                                                OrderDetail.NetPrice = orderDetail.NetPrice;
                                                OrderDetail.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                                OrderDetail.CreatedDateTime = DateTime.Now;
                                                OrderDetail.PhotoId = string.Join(",", imageid);
                                                if (orderDetail.LineItemParentId == -1)
                                                {
                                                    OrderDetail.LineItemParentId = -1;
                                                }
                                                else
                                                {
                                                    OrderDetail.LineItemParentId = OrdetailsId;
                                                }

                                                OrderDetail.DiscountDetails = orderDetail.TotalDiscountDetails;
                                                OrderDetail.SyncCode = orderDetail.OrderDetailsSyncCode;
                                                dbContext.OrderDetails.Add(OrderDetail);
                                                dbContext.SaveChanges();
                                            #endregion
                                                if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                                {
                                                    #region Add CustomerQRCodeDetail

                                                    CustomerQRCodeDetail QRCodeOrderDetail = dbContext.CustomerQRCodeDetails.Where(o => o.OrderId == Order.OrderNumber && o.IdentificationCode == orderDetail.IdentificationCode && o.SubStoreId == subStoreId).FirstOrDefault();
                                                    if (QRCodeOrderDetail == null)
                                                    {
                                                        QRCodeOrderDetail = new CustomerQRCodeDetail();
                                                        QRCodeOrderDetail.IdentificationCode = orderDetail.IdentificationCode;
                                                        QRCodeOrderDetail.OrderId = Order.OrderNumber;
                                                        QRCodeOrderDetail.PackageId = Convert.ToInt32(PackageId);
                                                        QRCodeOrderDetail.Photos = string.Join(",", imageid);
                                                        QRCodeOrderDetail.CreatedDateTime = DateTime.Now;
                                                        QRCodeOrderDetail.IsActive = true;
                                                        QRCodeOrderDetail.SubStoreId = subStoreId;
                                                        QRCodeOrderDetail.OrderDate = Order.OrderDate;
                                                        QRCodeOrderDetail.IncomingChangeId = ChangeInfo.IncomingChangeId;
                                                        dbContext.CustomerQRCodeDetails.Add(QRCodeOrderDetail);
                                                        dbContext.SaveChanges();
                                                    #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    foreach (ProductInfo prod in orderDetail.ProductInfoList)
                                    {
                                        long ProductId = 0; //dbContext.Products.Where(x => x.SyncCode == prod.SyncCode).FirstOrDefault().ProductId;
                                        if (dbContext.Products.Where(x => x.SyncCode == prod.SyncCode).FirstOrDefault() != null)
                                        {
                                            ProductId = dbContext.Products.Where(x => x.SyncCode == prod.SyncCode).FirstOrDefault().ProductId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.ProductMissing + "@@@@ Order failed because product with sync code " + prod.SyncCode + " does not exist.");
                                        }


                                        var chkOrderDetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                        if (chkOrderDetail == null)
                                        {
                                            #region Add OrderDetail

                                            var OrderDetail = new OrderDetail();
                                            OrderDetail.OrderMasterId = Order.OrderMasterId;
                                            OrderDetail.ProductId = ProductId;
                                            OrderDetail.DiscountAmount = orderDetail.DiscountAmount;
                                            OrderDetail.UnitPrice = orderDetail.UnitPrice;
                                            OrderDetail.Quantity = orderDetail.Qty;
                                            OrderDetail.TotalCost = orderDetail.TotalCost;
                                            OrderDetail.NetPrice = orderDetail.NetPrice;
                                            OrderDetail.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                            OrderDetail.CreatedDateTime = DateTime.Now;
                                            OrderDetail.PhotoId = string.Join(",", imageid);
                                            if (orderDetail.LineItemParentId == -1)
                                            {
                                                OrderDetail.LineItemParentId = -1;
                                            }
                                            else
                                            {
                                                OrderDetail.LineItemParentId = null; //orderDetail.LineItemParentId;
                                            }
                                            OrderDetail.DiscountDetails = orderDetail.TotalDiscountDetails;
                                            OrderDetail.SyncCode = orderDetail.OrderDetailsSyncCode;
                                            dbContext.OrderDetails.Add(OrderDetail);
                                            dbContext.SaveChanges();
                                            #endregion
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                    {
                                        OrderDetail OrderDetailsInfo = dbContext.OrderDetails.Where(o => o.OrderMasterId == OrderMasterId && o.PackageId > 0).FirstOrDefault();
                                        if (OrderDetailsInfo != null)
                                        {
                                            CustomerQRCodeDetail QRCodeOrderDetail = dbContext.CustomerQRCodeDetails.Where(o => o.OrderId == Order.OrderNumber && o.IdentificationCode == orderDetail.IdentificationCode && o.SubStoreId == subStoreId).FirstOrDefault();
                                            if (QRCodeOrderDetail == null)
                                            {
                                                #region Add QRCodeOrderDetail
                                                QRCodeOrderDetail = new CustomerQRCodeDetail();
                                                QRCodeOrderDetail.IdentificationCode = orderDetail.IdentificationCode;
                                                QRCodeOrderDetail.OrderId = Order.OrderNumber;
                                                QRCodeOrderDetail.PackageId = Convert.ToInt32(OrderDetailsInfo.PackageId);
                                                QRCodeOrderDetail.Photos = string.Join(",", imageid);
                                                QRCodeOrderDetail.CreatedDateTime = DateTime.Now;
                                                QRCodeOrderDetail.IsActive = true;
                                                QRCodeOrderDetail.SubStoreId = subStoreId;
                                                QRCodeOrderDetail.OrderDate = Order.OrderDate;
                                                QRCodeOrderDetail.IncomingChangeId = ChangeInfo.IncomingChangeId;
                                                dbContext.CustomerQRCodeDetails.Add(QRCodeOrderDetail);
                                                dbContext.SaveChanges();
                                                #endregion
                                            }
                                        }
                                    }
                                    foreach (PrinterQueue printerQueue in orderDetail.PrinterQueues)
                                    {
                                        var checkorderdetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();

                                        if (checkorderdetail != null)
                                        {
                                            var queue = new PrinterQueueData();
                                            queue.OrderDetailId = printerQueue.DG_Order_Details_Pkey;
                                            queue.SentToPrinter = printerQueue.DG_SentToPrinter;
                                            queue.isActive = printerQueue.is_Active;
                                            queue.StoreID = printerQueue.DG_Store_pkey;
                                            queue.ReprintCount = printerQueue.ReprintCount;
                                            dbContext.PrinterQueues.Add(queue);
                                            dbContext.SaveChanges();

                                        }
                                    }
                                }
                            }
                            else
                            {
                                var list = dbContext.OrderMasters.Where(x => x.SyncCode == orderDetail.SyncCode).ToList();
                                var orderMaster = list.FirstOrDefault();

                                long subStoreId = 0;
                                //Added to insert PartnerID in dbo.photos by shweta
                                long? venueId = 0;
                                //end
                                if (dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault() != null)
                                {
                                    subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    venueId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().ParentDigiMasterLocationId;
                                    //end
                                }
                                else
                                {
                                    throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.SiteMissing + "@@@@ Order failed because site with sync code " + orderDetail.SubStore.SyncCode + " does not exist.");
                                }


                                if (orderMaster.OrderMasterId > 0)
                                {
                                    foreach (PhotoInfo photo in orderDetail.PhotosList)
                                    {
                                        var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photo.PhotoId && p.SubStoreId == subStoreId && p.FileName == photo.FileName).FirstOrDefault();
                                        long LocationId = 0;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        long PartnerID = 0;
                                        //end
                                        if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault() != null)
                                        {
                                            LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photo.LocationSyncCode + " does not exist.");
                                        }
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                        {
                                            PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                        }
                                        //end

                                        if (photoIdList != null && photoIdList.Count > 0)
                                            photoCount = photoIdList.Where(s => s.Contains(photo.PhotoId.ToString())).Count();
                                        var PhotoUser = dbContext.Users.Where(x => x.SyncCode == photo.CreatebBySyncCode).FirstOrDefault();
                                        if (photos == null)
                                        {
                                            #region Add Photo
                                            var image = new Photo();
                                            decimal? GpsLatitude;
                                            decimal? GpsLongitude;
                                            LoadXml(photo.MetaData, out GpsLatitude, out GpsLongitude);
                                            image.FileName = photo.FileName;
                                            image.RFID = photo.RFID;
                                            image.SubStoreId = subStoreId;
                                            image.CreatedDate = System.DateTime.Now;
                                            image.CreatedOnMIX = photo.CreatedOn;
                                            image.CaptureDate = photo.CaptureDate;
                                            image.IMIXPhotoId = photo.PhotoId;
                                            image.Background = photo.Background;
                                            image.Frame = photo.Frame;
                                            image.IsCroped = photo.IsCroped;
                                            image.IsRedEye = photo.IsRedEye;
                                            //Added to insert PartnerID in dbo.photos by shweta
                                            image.PartnerID = PartnerID;
                                            //end
                                            image.IsGreen = photo.IsGreen;
                                            image.MetaData = photo.MetaData;
                                            image.CreatedBy = PhotoUser == null ? orderMaster.OrderCreatedBy : PhotoUser.UserId; //photo.CreatebBy; //orderMaster.OrderCreatedBy;
                                            //image.CreatebBySyncCode = photo.CreatebBySyncCode;
                                            image.LocationId = LocationId;
                                            image.Active = true;
                                            image.GPSLatitude = GpsLatitude;
                                            image.GPSLongitude = GpsLongitude;
                                            if (photo.MediaType != null)
                                            {
                                                image.MediaType = photo.MediaType;
                                            }
                                            else
                                            {
                                                image.MediaType = 1;
                                            }
                                            image.VideoLength = photo.VideoLength;
                                            image.Email = photo.Email;
                                            image.Nationality = photo.Nationality;
                                            dbContext.Photos.Add(image);
                                            dbContext.SaveChanges();
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(image.PhotoId);
                                            photos = new Photo();
                                            photos.PhotoId = image.PhotoId;
                                            photos.FileName = image.FileName;
                                            #endregion

                                        }
                                        else
                                        {
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(photos.PhotoId);
                                        }

                                        if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                        {
                                            var webPhotos = dbContext.WebPhotos.Where(wp => wp.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                            if (webPhotos == null)
                                            {
                                                var WebPhoto = new WebPhoto();
                                                WebPhoto.PhotoId = photos.PhotoId;
                                                WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                                WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                                WebPhoto.IsPaidImage = true;
                                                string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                                WebPhoto.CloudSourceImageURL = CloudImageURL;
                                                dbContext.WebPhotos.Add(WebPhoto);
                                                dbContext.SaveChanges();
                                            }
                                        }
                                    }
                                    foreach (PhotoInfo photoUnSold in orderDetail.PhotosListUnSold)
                                    {
                                        var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photoUnSold.PhotoId && p.SubStoreId == subStoreId && p.FileName == photoUnSold.FileName).FirstOrDefault();
                                        long LocationId = 0;// dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        long PartnerID = 0;
                                        //end
                                        if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault() != null)
                                        {
                                            LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photoUnSold.LocationSyncCode + " does not exist.");
                                        }
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                        {
                                            PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                        }
                                        //end

                                        photoCount = photoIdListUnSold.Where(s => s == photoUnSold.PhotoId.ToString()).Count();
                                        var PhotoUser = dbContext.Users.Where(x => x.SyncCode == photoUnSold.CreatebBySyncCode).FirstOrDefault();
                                        if (photos == null)
                                        {
                                            #region Add Photo
                                            var image = new Photo();
                                            decimal? GpsLatitude;
                                            decimal? GpsLongitude;
                                            LoadXml(photoUnSold.MetaData, out GpsLatitude, out GpsLongitude);
                                            image.FileName = photoUnSold.FileName;
                                            image.RFID = photoUnSold.RFID;
                                            image.SubStoreId = subStoreId;
                                            image.CreatedDate = System.DateTime.Now;
                                            image.CreatedOnMIX = photoUnSold.CreatedOn;
                                            image.CaptureDate = photoUnSold.CaptureDate;
                                            image.IMIXPhotoId = photoUnSold.PhotoId;
                                            image.Background = photoUnSold.Background;
                                            image.Frame = photoUnSold.Frame;
                                            image.IsCroped = photoUnSold.IsCroped;
                                            image.IsRedEye = photoUnSold.IsRedEye;
                                            //Added to insert PartnerID in dbo.photos by shweta
                                            image.PartnerID = PartnerID;
                                            //end
                                            image.IsGreen = photoUnSold.IsGreen;
                                            image.MetaData = photoUnSold.MetaData;
                                            image.CreatedBy = PhotoUser == null ? orderMaster.OrderCreatedBy : PhotoUser.UserId; //photo.CreatebBy; //orderMaster.OrderCreatedBy;
                                            //image.CreatebBySyncCode = photo.CreatebBySyncCode;
                                            image.LocationId = LocationId;
                                            image.Active = true;
                                            image.GPSLatitude = GpsLatitude;
                                            image.GPSLongitude = GpsLongitude;
                                            if (photoUnSold.MediaType != null)
                                            {
                                                image.MediaType = photoUnSold.MediaType;
                                            }
                                            else
                                            {
                                                image.MediaType = 1;
                                            }
                                            image.VideoLength = photoUnSold.VideoLength;
                                            image.Email = photoUnSold.Email;
                                            image.Nationality = photoUnSold.Nationality;
                                            dbContext.Photos.Add(image);
                                            dbContext.SaveChanges();
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(image.PhotoId);
                                            photos = new Photo();
                                            photos.PhotoId = image.PhotoId;
                                            photos.FileName = image.FileName;
                                            #endregion

                                        }
                                        else
                                        {
                                            for (int i = 0; i < photoCount; i++)
                                                imageid.Add(photos.PhotoId);
                                        }

                                        if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                        {
                                            var webPhotos = dbContext.WebPhotos.Where(wp => wp.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                            if (webPhotos == null)
                                            {
                                                var WebPhoto = new WebPhoto();
                                                WebPhoto.PhotoId = photos.PhotoId;
                                                WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                                WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                                WebPhoto.IsPaidImage = false;
                                                string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                                WebPhoto.CloudSourceImageURL = CloudImageURL;
                                                dbContext.WebPhotos.Add(WebPhoto);
                                                dbContext.SaveChanges();
                                            }
                                        }
                                    }
                                    foreach (PackageInfo package in orderDetail.PackageInfoList)
                                    {
                                        long PackageId = 0;// dbContext.Packages.Where(x => x.SyncCode == package.SyncCode).FirstOrDefault().PackageId;

                                        if (dbContext.Packages.Where(x => x.SyncCode == package.SyncCode).FirstOrDefault() != null)
                                        {
                                            PackageId = dbContext.Packages.Where(x => x.SyncCode == package.SyncCode).FirstOrDefault().PackageId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.PackageMissing + "@@@@ Order failed because package with sync code " + package.SyncCode + " does not exist.");
                                        }

                                        var OrderDetails = new OrderDetail();
                                        OrderDetails.OrderMasterId = orderMaster.OrderMasterId;
                                        //var orderdetails = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderDetails.OrderMasterId && x.PackageId == PackageId && x.SyncCode == OrderDetails.SyncCode).FirstOrDefault();
                                        // OrderDetails.SubStoreId = subStoreId;
                                        var chkorderdetails = dbContext.OrderDetails.Where(x => x.OrderMasterId == orderMaster.OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                        if (chkorderdetails == null)
                                        {
                                            #region Add OrderDetails
                                            var orderdetails = new OrderDetail();
                                            OrderDetails.PackageId = PackageId;
                                            OrderDetails.DiscountAmount = orderDetail.DiscountAmount;
                                            OrderDetails.UnitPrice = orderDetail.UnitPrice;
                                            OrderDetails.Quantity = orderDetail.Qty;
                                            OrderDetails.TotalCost = orderDetail.TotalCost;
                                            OrderDetails.NetPrice = orderDetail.NetPrice;
                                            OrderDetails.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                            OrderDetails.CreatedDateTime = DateTime.Now;
                                            OrderDetails.PhotoId = "";// string.Join(",", imageid);
                                            OrderDetails.DiscountDetails = orderDetail.TotalDiscountDetails;
                                            OrderDetails.SyncCode = orderDetail.OrderDetailsSyncCode;
                                            if (orderDetail.LineItemParentId == -1)
                                            {
                                                OrderDetails.LineItemParentId = -1;
                                            }
                                            else
                                            {
                                                OrderDetails.LineItemParentId = orderDetail.LineItemParentId.HasValue ? orderDetail.LineItemParentId : -1;
                                            }

                                            dbContext.OrderDetails.Add(OrderDetails);
                                            dbContext.SaveChanges();

                                            #endregion

                                            OrdetailsId = OrderDetails.OrderDetailId;
                                        }
                                        else
                                        {
                                            OrdetailsId = chkorderdetails.OrderDetailId;
                                        }
                                        var chkOrderNullLimeItem = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderDetails.OrderMasterId && x.LineItemParentId == null).ToList();
                                        {
                                            if (chkOrderNullLimeItem != null && chkOrderNullLimeItem.Count > 0)
                                            {
                                                chkOrderNullLimeItem.ForEach(x => x.LineItemParentId = OrdetailsId);
                                                dbContext.SaveChanges();
                                            }
                                        }
                                        foreach (ProductInfo prodPackage in package.Products)
                                        {
                                            long ProductId = 0;// ;

                                            if (dbContext.Products.Where(x => x.SyncCode == prodPackage.SyncCode).FirstOrDefault() != null)
                                            {
                                                ProductId = dbContext.Products.Where(x => x.SyncCode == prodPackage.SyncCode).FirstOrDefault().ProductId;
                                            }
                                            else
                                            {
                                                throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.ProductMissing + "@@@@ Order failed because product  with sync code " + prodPackage.SyncCode + " does not exist.");
                                            }

                                            var chkOrderDetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == orderMaster.OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                            if (chkOrderDetail == null)
                                            {
                                                #region Add Order Details
                                                var OrderDetail = new OrderDetail();
                                                OrderDetail.OrderMasterId = orderMaster.OrderMasterId;
                                                // OrderDetail.SubStoreId = subStoreId;
                                                OrderDetail.ProductId = ProductId;
                                                OrderDetail.DiscountAmount = orderDetail.DiscountAmount;
                                                OrderDetail.UnitPrice = orderDetail.UnitPrice;
                                                OrderDetail.Quantity = orderDetail.Qty;
                                                OrderDetail.TotalCost = orderDetail.TotalCost;
                                                OrderDetail.NetPrice = orderDetail.NetPrice;
                                                OrderDetail.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                                OrderDetail.CreatedDateTime = DateTime.Now;
                                                OrderDetail.PhotoId = string.Join(",", imageid);

                                                if (orderDetail.LineItemParentId == -1)
                                                {
                                                    OrderDetail.LineItemParentId = -1;
                                                }
                                                else
                                                {
                                                    OrderDetail.LineItemParentId = OrdetailsId;
                                                }

                                                OrderDetail.DiscountDetails = orderDetail.TotalDiscountDetails;
                                                OrderDetail.SyncCode = orderDetail.OrderDetailsSyncCode;
                                                dbContext.OrderDetails.Add(OrderDetail);
                                                dbContext.SaveChanges();
                                                #endregion
                                            }

                                            if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                            {
                                                CustomerQRCodeDetail QRCodeOrderDetail = dbContext.CustomerQRCodeDetails.Where(o => o.OrderId == orderMaster.OrderNumber && o.IdentificationCode == orderDetail.IdentificationCode && o.SubStoreId == subStoreId).FirstOrDefault();
                                                if (QRCodeOrderDetail == null)
                                                {
                                                    #region Add CustomerQRCode Details
                                                    QRCodeOrderDetail = new CustomerQRCodeDetail();
                                                    QRCodeOrderDetail.IdentificationCode = orderDetail.IdentificationCode;
                                                    QRCodeOrderDetail.OrderId = orderMaster.OrderNumber;
                                                    QRCodeOrderDetail.PackageId = Convert.ToInt32(PackageId);
                                                    QRCodeOrderDetail.Photos = string.Join(",", imageid);
                                                    QRCodeOrderDetail.CreatedDateTime = DateTime.Now;
                                                    QRCodeOrderDetail.IsActive = true;
                                                    QRCodeOrderDetail.SubStoreId = subStoreId;
                                                    QRCodeOrderDetail.OrderDate = orderMaster.OrderDate;
                                                    QRCodeOrderDetail.IncomingChangeId = ChangeInfo.IncomingChangeId;
                                                    dbContext.CustomerQRCodeDetails.Add(QRCodeOrderDetail);
                                                    dbContext.SaveChanges();
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                    foreach (ProductInfo prod in orderDetail.ProductInfoList)
                                    {
                                        long ProductId = 0;// 

                                        if (dbContext.Products.Where(x => x.SyncCode == prod.SyncCode).FirstOrDefault() != null)
                                        {
                                            ProductId = dbContext.Products.Where(x => x.SyncCode == prod.SyncCode).FirstOrDefault().ProductId;
                                        }
                                        else
                                        {
                                            throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.ProductMissing + "@@@@ Order failed because product  with sync code " + prod.SyncCode + " does not exist.");
                                        }
                                        var chkOrderDetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == orderMaster.OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();
                                        if (chkOrderDetail == null)
                                        {
                                            #region Add OrderDeatils

                                            var OrderDetail = new OrderDetail();
                                            OrderDetail.OrderMasterId = orderMaster.OrderMasterId;
                                            OrderDetail.ProductId = ProductId;
                                            OrderDetail.DiscountAmount = orderDetail.DiscountAmount;
                                            OrderDetail.UnitPrice = orderDetail.UnitPrice;
                                            OrderDetail.Quantity = orderDetail.Qty;
                                            OrderDetail.TotalCost = orderDetail.TotalCost;
                                            OrderDetail.NetPrice = orderDetail.NetPrice;
                                            OrderDetail.IsBurned = Convert.ToBoolean(orderDetail.IsBurned);
                                            OrderDetail.CreatedDateTime = DateTime.Now;
                                            OrderDetail.DiscountDetails = orderDetail.TotalDiscountDetails;
                                            OrderDetail.SyncCode = orderDetail.OrderDetailsSyncCode;
                                            //imageid.Clear();
                                            //foreach (PhotoInfo photo in orderDetail.PhotosList)
                                            //{
                                            //    var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photo.PhotoId && p.SubStoreId == subStoreId).FirstOrDefault();
                                            //    if (photos != null)
                                            //    {
                                            //        int photoid = dbContext.Photos.Where(p => p.IMIXPhotoId == photo.PhotoId && p.SubStoreId == subStoreId).FirstOrDefault().PhotoId;
                                            //        imageid.Add(photoid);
                                            //    }
                                            //    OrderDetail.PhotoId = string.Join(",", imageid);
                                            //}
                                            OrderDetail.PhotoId = string.Join(",", imageid);
                                            var orderdetails = dbContext.OrderDetails.Where(p => p.OrderMasterId == OrderDetail.OrderMasterId && p.PackageId != null).OrderByDescending(p => p.OrderDetailId).FirstOrDefault();

                                            if (orderdetails != null && orderDetail.LineItemParentId != -1)
                                            {
                                                OrderDetail.LineItemParentId = orderdetails.OrderDetailId;
                                            }
                                            else
                                            {
                                                OrderDetail.LineItemParentId = -1;
                                            }
                                            if (orderDetail.LineItemParentId == -1)
                                            {
                                                OrderDetail.LineItemParentId = -1;
                                            }

                                            dbContext.OrderDetails.Add(OrderDetail);
                                            dbContext.SaveChanges();
                                            #endregion
                                        }
                                    }
                                }
                                foreach (PrinterQueue printerQueue in orderDetail.PrinterQueues)
                                {
                                    var checkorderdetail = dbContext.OrderDetails.Where(x => x.OrderMasterId == orderMaster.OrderMasterId && x.SyncCode == orderDetail.OrderDetailsSyncCode).FirstOrDefault();

                                    if (checkorderdetail != null)
                                    {
                                        var queue = new PrinterQueueData();
                                        queue.OrderDetailId = printerQueue.DG_Order_Details_Pkey;
                                        queue.SentToPrinter = printerQueue.DG_SentToPrinter;
                                        queue.isActive = printerQueue.is_Active;
                                        queue.StoreID = printerQueue.DG_Store_pkey;
                                        queue.ReprintCount = printerQueue.ReprintCount;
                                        dbContext.PrinterQueues.Add(queue);
                                        dbContext.SaveChanges();

                                    }
                                }

                                if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                {
                                    OrderDetail OrderDetailsInfo = dbContext.OrderDetails.Where(o => o.OrderMasterId == orderMaster.OrderMasterId && o.PackageId > 0).FirstOrDefault();
                                    if (OrderDetailsInfo != null)
                                    {
                                        CustomerQRCodeDetail QRCodeOrderDetail = dbContext.CustomerQRCodeDetails.Where(o => o.OrderId == orderMaster.OrderNumber && o.IdentificationCode == orderDetail.IdentificationCode && o.SubStoreId == subStoreId).FirstOrDefault();
                                        if (QRCodeOrderDetail == null)
                                        {
                                            #region Add CustomerQRCodeDetail
                                            QRCodeOrderDetail = new CustomerQRCodeDetail();
                                            QRCodeOrderDetail.IdentificationCode = orderDetail.IdentificationCode;
                                            QRCodeOrderDetail.OrderId = orderMaster.OrderNumber;
                                            QRCodeOrderDetail.PackageId = Convert.ToInt32(OrderDetailsInfo.PackageId);
                                            QRCodeOrderDetail.Photos = string.Join(",", imageid);
                                            QRCodeOrderDetail.CreatedDateTime = DateTime.Now;
                                            QRCodeOrderDetail.IsActive = true;
                                            QRCodeOrderDetail.SubStoreId = subStoreId;
                                            QRCodeOrderDetail.OrderDate = orderMaster.OrderDate;
                                            QRCodeOrderDetail.IncomingChangeId = ChangeInfo.IncomingChangeId;
                                            dbContext.CustomerQRCodeDetails.Add(QRCodeOrderDetail);
                                            dbContext.SaveChanges();
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                isError = true;
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                isError = true;
                DateTime currentDate = DateTime.Now; ;
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string[] separatingChars = { "@@@@" };
                    string[] StatusAndMessage = ex.Message.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
                    if (StatusAndMessage.Count() > 1)
                    {


                        ChangeTrackingProcessingStatusDtl objChTraProStatus = new ChangeTrackingProcessingStatusDtl();
                        objChTraProStatus.ErrorID = Convert.ToInt32(StatusAndMessage[0]);
                        objChTraProStatus.Message = StatusAndMessage[1];
                        objChTraProStatus.Stage = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStages.CentralServer;
                        try
                        {
                            currentDate = dbContext.Database.SqlQuery<DateTime>("Select GETDATE() AS GetDateTimeNow").FirstOrDefault();
                        }
                        catch (Exception ex1)
                        {
                            log.Error("OrderProcessor:ProcessInsert: catch:: Message: " + ex1.Message + " StackTrace: " + ex1.StackTrace + " InnerException: " + ex1.InnerException + " Source: " + ex1.Source);
                        }
                        objChTraProStatus.CreatedOn = currentDate;
                        objChTraProStatus.ExceptionType = ex.GetType().ToString();
                        objChTraProStatus.ExceptionSource = ex.Source;
                        objChTraProStatus.ExceptionStackTrace = ex.StackTrace;
                        objChTraProStatus.InnerException = Convert.ToString(ex.InnerException);
                        objChTraProStatus.IncomingChangeId = ChangeInfo.IncomingChangeId;
                        objChTraProStatus.SyncCode = CommonUtility.GetUniqueSynccode("26", "00", "000", "00");
                        objChTraProStatus.Status = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStatus.FailedToProcessDataOnCentralServer;

                        dbContext.ChangeTrackingProcessingStatusDtls.Add(objChTraProStatus);
                        dbContext.SaveChanges();
                    }
                }

                log.Error("OrderProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in OrderProcessor Processor Insert.", ex);
                outerException.Source = "Business.Processing.OrderProcessor";
                throw outerException;
            }
            finally
            {
                if (!isError)
                {
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {

                        //string[] separatingChars = { "@@@@" };
                        // string[] StatusAndMessage = ex.Message.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
                        ChangeTrackingProcessingStatusDtl objChTraProStatus = new ChangeTrackingProcessingStatusDtl();
                        objChTraProStatus.ErrorID = 0; //(Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.Success; 
                        objChTraProStatus.Message = "Order Processed Successfully.";
                        objChTraProStatus.Stage = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStages.CentralServer;
                        DateTime currentDate = DateTime.Now;
                        try
                        {
                            currentDate = dbContext.Database.SqlQuery<DateTime>("Select GETDATE() AS GetDateTimeNow").FirstOrDefault();
                        }
                        catch (Exception ex1)
                        {
                            log.Error("OrderProcessor:ProcessInsert: finally:: Message: " + ex1.Message + " StackTrace: " + ex1.StackTrace + " InnerException: " + ex1.InnerException + " Source: " + ex1.Source);
                        }
                        //objChTraProStatus.CreatedOn = DateTime.Now;
                        objChTraProStatus.CreatedOn = currentDate;
                        objChTraProStatus.ExceptionType = "Success";
                        objChTraProStatus.ExceptionSource = "Success";
                        objChTraProStatus.ExceptionStackTrace = "Success";
                        objChTraProStatus.InnerException = "Success";
                        objChTraProStatus.IncomingChangeId = ChangeInfo.IncomingChangeId;
                        objChTraProStatus.SyncCode = CommonUtility.GetUniqueSynccode("26", "00", "000", "00");
                        objChTraProStatus.Status = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStatus.Success;

                        dbContext.ChangeTrackingProcessingStatusDtls.Add(objChTraProStatus);
                        dbContext.SaveChanges();
                    }
                }

            }
        }

        private static string GetImageSourceURL(OrderDetailsInfo orderDetail, long substoreId, string fileName)
        {
            try
            {
                string ImageURL = string.Empty;
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var substore = dbContext.DigiMasterLocations.Where(p => p.DigiMasterLocationId == substoreId).FirstOrDefault();
                    string SubstoreName = substore.Name;
                    DigiMasterLocation store = dbContext.DigiMasterLocations.Where(p => p.DigiMasterLocationId == (substore.ParentDigiMasterLocationId)).FirstOrDefault();
                    string storeName = store.Name;
                    string Country = dbContext.DigiMasterLocations.Where(p => p.DigiMasterLocationId == (dbContext.DigiMasterLocations.Where(q => q.DigiMasterLocationId == store.DigiMasterLocationId).FirstOrDefault().ParentDigiMasterLocationId)).FirstOrDefault().Name;
                    //  ImageURL = ConfigurationManager.AppSettings["ServerImagePath"] + ConfigurationManager.AppSettings["ServerImagePathFolder"] + Country + "/" + storeName + "/" + SubstoreName + "/" + orderDetail.OrderDate.ToString("yyyyMMdd") + "/" + orderDetail.OrderNumber + "/" + orderDetail.IdentificationCode + "/" + fileName;
                    ImageURL = Country + "/" + storeName + "/" + SubstoreName + "/" + orderDetail.OrderDate.ToString("yyyyMMdd") + "/" + orderDetail.OrderNumber + "/" + orderDetail.IdentificationCode + "/" + fileName;
                }
                return ImageURL;
            }
            catch (Exception ex)
            {
                log.Error("OrderProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in OrderProcessor Processor Insert.", ex);
                outerException.Source = "Business.Processing.OrderProcessor";
                return string.Empty;
            }
        }


        protected override void ProcessUpdate()
        {
            bool isError = false;
            try
            {

                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    List<OrderDetailsInfo> orderList = CommonUtility.DeserializeXML<List<OrderDetailsInfo>>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        Int64 OrderMasterID = 0;
                        foreach (OrderDetailsInfo orderDetail in orderList)
                        {
                            List<int> imageid = new List<int>();
                            var list = dbContext.OrderMasters.Where(x => x.SyncCode == orderDetail.SyncCode).ToList();
                            var orderMaster = list.FirstOrDefault();

                            long subStoreId = 0;
                            //Added to insert PartnerID in dbo.photos by shweta
                            long? venueId = 0;
                            //end
                            if (dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault() != null)
                            {
                                subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                                //Added to insert PartnerID in dbo.photos by shweta
                                venueId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().ParentDigiMasterLocationId;
                                //end
                            }
                            else
                            {
                                throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.SiteMissing + "@@@@ Order failed becasue site with sync code " + orderDetail.SubStore.SyncCode + " does not exist.");
                            }


                            string orderNumber = string.Empty;
                            if (orderMaster.OrderMasterId > 0)
                            {
                                OrderMasterID = orderMaster.OrderMasterId;
                                foreach (PhotoInfo photo in orderDetail.PhotosList)
                                {
                                    var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photo.PhotoId && p.SubStoreId == subStoreId && p.FileName == photo.FileName).FirstOrDefault();
                                    long LocationId = 0;// dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    long PartnerID = 0;
                                    //end
                                    if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault() != null)
                                    {
                                        LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photo.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                    }
                                    else
                                    {
                                        throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photo.LocationSyncCode + " does not exist.");
                                    }
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                    {
                                        PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                    }
                                    //end
                                    var PhotoUser = dbContext.Users.Where(x => x.SyncCode == photo.CreatebBySyncCode).FirstOrDefault();
                                    if (photos == null)
                                    {
                                        #region Add Photo
                                        var image = new Photo();
                                        decimal? GpsLatitude;
                                        decimal? GpsLongitude;
                                        LoadXml(photo.MetaData, out GpsLatitude, out GpsLongitude);
                                        image.FileName = photo.FileName;
                                        image.RFID = photo.RFID;
                                        image.SubStoreId = subStoreId;
                                        image.CreatedDate = System.DateTime.Now;
                                        image.CreatedOnMIX = photo.CreatedOn;
                                        image.CaptureDate = photo.CaptureDate;
                                        image.IMIXPhotoId = photo.PhotoId;
                                        image.Background = photo.Background;
                                        image.Frame = photo.Frame;
                                        image.IsCroped = photo.IsCroped;
                                        image.IsRedEye = photo.IsRedEye;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        image.PartnerID = PartnerID;
                                        //end
                                        image.IsGreen = photo.IsGreen;
                                        image.MetaData = photo.MetaData;
                                        image.CreatedBy = PhotoUser == null ? orderMaster.OrderCreatedBy : PhotoUser.UserId; //photo.CreatebBy; //orderMaster.OrderCreatedBy;
                                        //image.CreatebBySyncCode = photo.CreatebBySyncCode;
                                        image.LocationId = LocationId;
                                        image.Active = true;
                                        image.GPSLatitude = GpsLatitude;
                                        image.GPSLongitude = GpsLongitude;
                                        if (photo.MediaType != null)
                                        {
                                            image.MediaType = photo.MediaType;
                                        }
                                        else
                                        {
                                            image.MediaType = 1;
                                        }
                                        image.VideoLength = photo.VideoLength;
                                        image.Email = photo.Email;
                                        image.Nationality = photo.Nationality;
                                        dbContext.Photos.Add(image);
                                        dbContext.SaveChanges();
                                        imageid.Add(image.PhotoId);
                                        photos = new Photo();
                                        photos.PhotoId = image.PhotoId;
                                        photos.FileName = image.FileName;
                                        #endregion
                                    }
                                    else
                                    {
                                        imageid.Add(photos.PhotoId);
                                    }

                                    if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                    {
                                        var webPhotos = dbContext.WebPhotos.Where(wp => wp.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                        if (webPhotos == null)
                                        {
                                            var WebPhoto = new WebPhoto();
                                            WebPhoto.PhotoId = photos.PhotoId;
                                            WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                            WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                            WebPhoto.IsPaidImage = true;
                                            string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                            WebPhoto.CloudSourceImageURL = CloudImageURL;
                                            dbContext.WebPhotos.Add(WebPhoto);
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }
                                foreach (PhotoInfo photoUnSold in orderDetail.PhotosListUnSold)
                                {
                                    var photos = dbContext.Photos.Where(p => p.IMIXPhotoId == photoUnSold.PhotoId && p.SubStoreId == subStoreId && p.FileName == photoUnSold.FileName).FirstOrDefault();
                                    long LocationId = 0;// dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    long PartnerID = 0;
                                    //end
                                    if (dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault() != null)
                                    {
                                        LocationId = dbContext.DigiMasterLocations.Where(p => p.SyncCode == photoUnSold.LocationSyncCode).FirstOrDefault().DigiMasterLocationId;
                                    }
                                    else
                                    {
                                        throw new Exception((Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingError.LocationMissing + "@@@@ Order failed because location with sync code " + photoUnSold.LocationSyncCode + " does not exist.");
                                    }
                                    //Added to insert PartnerID in dbo.photos by shweta
                                    if (dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault() != null)
                                    {
                                        PartnerID = dbContext.PartnerLocations.Where(s => s.StoreID == venueId).FirstOrDefault().PartnerID;
                                    }
                                    //end
                                    var PhotoUser = dbContext.Users.Where(x => x.SyncCode == photoUnSold.CreatebBySyncCode).FirstOrDefault();
                                    if (photos == null)
                                    {
                                        #region Add Photo
                                        var image = new Photo();
                                        decimal? GpsLatitude;
                                        decimal? GpsLongitude;
                                        LoadXml(photoUnSold.MetaData, out GpsLatitude, out GpsLongitude);
                                        image.FileName = photoUnSold.FileName;
                                        image.RFID = photoUnSold.RFID;
                                        image.SubStoreId = subStoreId;
                                        image.CreatedDate = System.DateTime.Now;
                                        image.CreatedOnMIX = photoUnSold.CreatedOn;
                                        image.CaptureDate = photoUnSold.CaptureDate;
                                        image.IMIXPhotoId = photoUnSold.PhotoId;
                                        image.Background = photoUnSold.Background;
                                        image.Frame = photoUnSold.Frame;
                                        image.IsCroped = photoUnSold.IsCroped;
                                        image.IsRedEye = photoUnSold.IsRedEye;
                                        //Added to insert PartnerID in dbo.photos by shweta
                                        image.PartnerID = PartnerID;
                                        //end
                                        image.IsGreen = photoUnSold.IsGreen;
                                        image.MetaData = photoUnSold.MetaData;
                                        image.CreatedBy = PhotoUser == null ? orderMaster.OrderCreatedBy : PhotoUser.UserId; //photo.CreatebBy; //orderMaster.OrderCreatedBy;
                                        //image.CreatebBySyncCode = photo.CreatebBySyncCode;
                                        image.LocationId = LocationId;
                                        image.Active = true;
                                        image.GPSLatitude = GpsLatitude;
                                        image.GPSLongitude = GpsLongitude;
                                        if (photoUnSold.MediaType != null)
                                        {
                                            image.MediaType = photoUnSold.MediaType;
                                        }
                                        else
                                        {
                                            image.MediaType = 1;
                                        }
                                        image.VideoLength = photoUnSold.VideoLength;
                                        image.Email = photoUnSold.Email;
                                        image.Nationality = photoUnSold.Nationality;
                                        dbContext.Photos.Add(image);
                                        dbContext.SaveChanges();
                                        imageid.Add(image.PhotoId);
                                        photos = new Photo();
                                        photos.PhotoId = image.PhotoId;
                                        photos.FileName = image.FileName;
                                        #endregion
                                    }
                                    else
                                    {
                                        imageid.Add(photos.PhotoId);
                                    }

                                    if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                                    {
                                        var webPhotos = dbContext.WebPhotos.Where(wp => wp.PhotoId == photos.PhotoId && wp.IdentificationCode == orderDetail.IdentificationCode && wp.OrderNumber == orderDetail.OrderNumber).FirstOrDefault();
                                        if (webPhotos == null)
                                        {
                                            var WebPhoto = new WebPhoto();
                                            WebPhoto.PhotoId = photos.PhotoId;
                                            WebPhoto.IdentificationCode = orderDetail.IdentificationCode;
                                            WebPhoto.OrderNumber = orderDetail.OrderNumber;
                                            WebPhoto.IsPaidImage = false;
                                            string CloudImageURL = GetImageSourceURL(orderDetail, subStoreId, photos.FileName);
                                            WebPhoto.CloudSourceImageURL = CloudImageURL;
                                            dbContext.WebPhotos.Add(WebPhoto);
                                            dbContext.SaveChanges();
                                        }
                                    }
                                }
                                //Update OrderMaster if cancelled.
                                orderMaster.IsCancelled = orderDetail.IsCancelled;
                                orderMaster.CancelledDateTime = orderDetail.CancelledDateTime;
                                orderMaster.Reason = orderDetail.Reason;
                                orderMaster.IsSynced = true;
                                dbContext.SaveChanges();
                                orderNumber = orderMaster.OrderNumber;
                            }

                            //Update Order Detail Updated Photos Information.
                            var orderData = dbContext.OrderDetails.Where(x => x.SyncCode == orderDetail.OrderDetailsSyncCode && x.OrderMasterId == orderMaster.OrderMasterId).FirstOrDefault();
                            if (orderData != null && imageid.Count > 0)
                            {
                                orderData.PhotoId = string.Join(",", imageid);
                            }
                            if (!string.IsNullOrEmpty(orderDetail.IdentificationCode))
                            {
                                var QRCodeOrderDetail = dbContext.CustomerQRCodeDetails.Where(o => o.OrderId == orderNumber && o.IdentificationCode == orderDetail.IdentificationCode && o.SubStoreId == subStoreId).FirstOrDefault();
                                if (QRCodeOrderDetail != null && imageid.Count > 0)
                                {
                                    QRCodeOrderDetail.Photos = string.Join(",", imageid);
                                }
                            }
                            dbContext.SaveChanges();
                            ////int UpdateorderDetails = dbContext.OrderMasters.Count(x => x.SyncCode == orderDetail.SyncCode);
                            //if (orderMaster != null)
                            //{
                            //    var OrderCancel = dbContext.OrderMasters.Where(x => x.SyncCode == orderDetail.SyncCode).FirstOrDefault();
                            //    OrderCancel.IsCancelled = orderDetail.IsCancelled;
                            //    OrderCancel.CancelledDateTime = orderDetail.CancelledDateTime;
                            //    OrderCancel.Reason = orderDetail.Reason;
                            //    OrderCancel.IsSynced = true;                                
                            //    dbContext.SaveChanges();
                            //}
                        }
                        var OrderDetails = dbContext.OrderDetails.Where(x => x.OrderMasterId == OrderMasterID).ToList();
                        foreach (var obj in OrderDetails)
                        {
                            obj.OrderProcessStatus = 2;
                        }
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string[] separatingChars = { "@@@@" };
                    string[] StatusAndMessage = ex.Message.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
                    if (StatusAndMessage.Count() > 1)
                    {
                        ChangeTrackingProcessingStatusDtl objChTraProStatus = new ChangeTrackingProcessingStatusDtl();
                        objChTraProStatus.ErrorID = Convert.ToInt32(StatusAndMessage[0]);
                        objChTraProStatus.Message = StatusAndMessage[1];
                        objChTraProStatus.Stage = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStages.CentralServer;
                        DateTime currentDate = DateTime.Now;
                        try
                        {
                            currentDate = dbContext.Database.SqlQuery<DateTime>("Select GETDATE() AS GetDateTimeNow").FirstOrDefault();
                        }
                        catch (Exception ex1)
                        {
                            log.Error("OrderProcessor:ProcessUpdate: catch:: Message: " + ex1.Message + " StackTrace: " + ex1.StackTrace + " InnerException: " + ex1.InnerException + " Source: " + ex1.Source);
                        }
                        //objChTraProStatus.CreatedOn = DateTime.Now;
                        objChTraProStatus.CreatedOn = currentDate;
                        objChTraProStatus.ExceptionType = ex.GetType().ToString();
                        objChTraProStatus.ExceptionSource = ex.Source;
                        objChTraProStatus.ExceptionStackTrace = ex.StackTrace;
                        objChTraProStatus.InnerException = Convert.ToString(ex.InnerException);
                        objChTraProStatus.IncomingChangeId = ChangeInfo.IncomingChangeId;
                        objChTraProStatus.SyncCode = CommonUtility.GetUniqueSynccode("26", "00", "000", "00");
                        objChTraProStatus.Status = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStatus.FailedToProcessDataOnCentralServer;

                        dbContext.ChangeTrackingProcessingStatusDtls.Add(objChTraProStatus);
                        dbContext.SaveChanges();
                    }
                }

                log.Error("OrderProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in OrderProcessor Processor Update.", ex);
                outerException.Source = "Business.Processing.OrderProcessor";
                throw outerException;

            }
            finally
            {
                if (!isError)
                {
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        //string[] separatingChars = { "@@@@" };
                        //string[] StatusAndMessage = ex.Message.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
                        ChangeTrackingProcessingStatusDtl objChTraProStatus = new ChangeTrackingProcessingStatusDtl();
                        objChTraProStatus.ErrorID = 0;// (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStatus.Success;
                        objChTraProStatus.Message = "Order Processed Successfully.";
                        objChTraProStatus.Stage = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStages.CentralServer;
                        DateTime currentDate = DateTime.Now;
                        try
                        {
                            currentDate = dbContext.Database.SqlQuery<DateTime>("Select GETDATE() AS GetDateTimeNow").FirstOrDefault();
                        }
                        catch(Exception ex1)
                        {
                            log.Error("OrderProcessor:ProcessUpdate: finally:: Message: " + ex1.Message + " StackTrace: " + ex1.StackTrace + " InnerException: " + ex1.InnerException + " Source: " + ex1.Source);
                        }
                        //objChTraProStatus.CreatedOn = DateTime.Now;
                        objChTraProStatus.CreatedOn = currentDate;
                        objChTraProStatus.ExceptionType = "Success";
                        objChTraProStatus.ExceptionSource = "Success";
                        objChTraProStatus.ExceptionStackTrace = "Success";
                        objChTraProStatus.InnerException = "Success";
                        objChTraProStatus.IncomingChangeId = ChangeInfo.IncomingChangeId;
                        objChTraProStatus.SyncCode = CommonUtility.GetUniqueSynccode("26", "00", "000", "00");
                        objChTraProStatus.Status = (Int32)DigiPhoto.DigiSync.Utility.ChangeTrackingProcessingStatus.Success;
                        dbContext.ChangeTrackingProcessingStatusDtls.Add(objChTraProStatus);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        protected override void ProcessDelete()
        {
            throw new ProcessingException("Not Emplemented");
        }

        /// <summary>
        /// Loads the XML.
        /// </summary>
        /// <param name="xmlMetaData">The image XML.</param>
        public void LoadXml(string xmlMetaData, out decimal? GpsLatitude, out decimal? GpsLongitude)
        {
            GpsLatitude = null;
            GpsLongitude = null;
            if (string.IsNullOrEmpty(xmlMetaData))
                return;
            try
            {
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(xmlMetaData);
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "gpslatitude":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "" && ((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                GpsLatitude = Convert.ToDecimal(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                GpsLatitude = null;
                            }
                            break;
                        case "gpslongitude":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "" && ((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                GpsLongitude = Convert.ToDecimal(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                GpsLongitude = null;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OrderProcessor:LoadXml: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in OrderProcessor LoadXml.", ex);
                outerException.Source = "Business.Processing.OrderProcessor";
                throw outerException;
            }
        }
    }
}

