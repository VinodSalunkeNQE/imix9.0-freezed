﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DigiSync.Business.Processing
{
    //Created By Kumar Gaurav.............
   public class GraphicProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    GraphicInfo graphicInfo = CommonUtility.DeserializeXML<GraphicInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var existingGraphics = dbContext.Graphics.Count(r => r.SyncCode == graphicInfo.SyncCode);
                        var storeId = (from DMStore in dbContext.DigiMasterLocations
                                       join DMSite in dbContext.DigiMasterLocations
                                       on DMStore.DigiMasterLocationId equals DMSite.ParentDigiMasterLocationId
                                       where DMSite.SyncCode == graphicInfo.SiteCodeSyncCode
                                       select DMStore.DigiMasterLocationId).FirstOrDefault();
                        if (existingGraphics == 0)
                        {
                            var graphic = new Graphic();
                            graphic.Name = graphicInfo.Name;
                            graphic.DisplayName = graphicInfo.DisplayName;
                            graphic.CreatedDateTime = graphicInfo.ModifiedDate;
                            graphic.CreatedBy = 1;
                            graphic.ModifiedDateTime = graphicInfo.ModifiedDate;
                            graphic.ModifiedBy = 1;
                            graphic.IsActive = graphicInfo.IsActive;
                            graphic.SyncCode = graphicInfo.SyncCode;
                            graphic.IsSynced = true;
                            dbContext.Graphics.Add(graphic);
                           dbContext.SaveChanges();
                           Int64 graphicId = graphic.GraphicsId;

                            ObjectAuthorization obj = new ObjectAuthorization();
                            obj.ApplicationObjectId = 3;
                            obj.ObjectValueId = graphicId;
                            obj.IsDeleted = 0;
                            obj.DigiMasterLocationId = Convert.ToInt64(storeId);
                            dbContext.ObjectAuthorizations.Add(obj);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GraphicProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Graphic Processor Insert.", ex);
                outerException.Source = "Business.Processing.GraphicProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    GraphicInfo graphicInfo = CommonUtility.DeserializeXML<GraphicInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbGraphics = dbContext.Graphics.Where(r => r.SyncCode == graphicInfo.SyncCode).FirstOrDefault();
                        if (dbGraphics != null)
                        {
                            dbGraphics.Name = graphicInfo.Name;
                            dbGraphics.DisplayName = graphicInfo.DisplayName;
                            dbGraphics.ModifiedDateTime = graphicInfo.ModifiedDate;
                            dbGraphics.ModifiedBy = 1;
                            dbGraphics.IsActive = graphicInfo.IsActive;
                            dbGraphics.SyncCode = graphicInfo.SyncCode;
                            dbGraphics.IsSynced = true;                        
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GraphicProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Graphic Processor Update.", ex);
                outerException.Source = "Business.Processing.GraphicProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbGraphic = dbContext.Graphics.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbGraphic != null)
                    {
                        dbContext.Graphics.Remove(dbGraphic);
                        dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GraphicProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Graphic Processor  Delete.", ex);
                outerException.Source = "Business.Processing.GraphicProcessor";
                throw outerException;
            }
        }
    }
}
