﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using log4net;
using System.Data.Entity.Validation;

namespace DigiPhoto.DigiSync.Business.Processing
{
    class UserProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    UserInfo userInfo = CommonUtility.DeserializeXML<UserInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var subStore = dbContext.DigiMasterLocations.Where(l => l.SyncCode == ChangeInfo.SubStore.SiteCode && l.Level == 4).FirstOrDefault();

                        var existingUserCount = dbContext.Users.Count(r => r.SyncCode == userInfo.SyncCode);
                        if (existingUserCount == 0)
                        {

                            var user = new User();

                            user.UserName = userInfo.UserName;
                            user.PasswordHash = userInfo.Password;
                            user.IsActive = userInfo.IsActive;
                            user.SyncCode = userInfo.SyncCode;
                            user.Emp_Id = userInfo.Emp_Id;
                            var userdetail = new UserDetail();
                            userdetail.FirstName = userInfo.FirstName;
                            if (string.IsNullOrEmpty(userInfo.LastName))
                            {
                                userdetail.LastName = userInfo.FirstName;
                            }
                            else
                            {
                                userdetail.LastName = userInfo.LastName;
                            }
                            userdetail.MiddleName = userInfo.MiddleName;
                            userdetail.EmailAddress = userInfo.EmailAddress;
                            userdetail.PhoneNumber = userInfo.PhoneNumber;
                            dbContext.UserDetails.Add(userdetail);
                            dbContext.SaveChanges();
                            long userdetailid = userdetail.UserDetailId;

                            user.UserDetailId = userdetailid;
                            user.CreatedDateTime = System.DateTime.Now;
                            user.IsImixUser = true;
                            if (subStore != null)
                            {
                                user.StoreId = subStore.ParentDigiMasterLocationId;
                            }

                            user.UserTypeCodeId = 6;
                            var objUser = dbContext.Users.Where(r => r.UserName == "administrator").FirstOrDefault();
                            user.CreatedBy = objUser.UserId;
                            user.IsSynced = true;
                            dbContext.Users.Add(user);
                            dbContext.SaveChanges();
                            long userid = user.UserId;

                            var role = dbContext.Roles.Where(r => r.SyncCode == userInfo.RoleSyncCode).FirstOrDefault();
                            if (role != null)
                            {
                                var roles = new UserRole();
                                roles.RoleId = role.RoleId;
                                roles.UserId = userid;
                                //  roles.UserRoleId = Convert.ToInt32(userid);
                                dbContext.UserRoles.Add(roles);
                                dbContext.SaveChanges();
                            }

                            var userauth = dbContext.DigiMasterLocations.Where(r => r.SyncCode == userInfo.LocationSyncCode).FirstOrDefault();
                            if (userauth != null)
                            {
                                var userauthorization = new UserAuthorization();
                                userauthorization.DigiMasterLocationId = userauth.DigiMasterLocationId;
                                userauthorization.UserId = userid;
                                dbContext.UserAuthorizations.Add(userauthorization);
                                dbContext.SaveChanges();
                            }

                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                log.Error("UserProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                Exception outerException = new ProcessingException("Error occured in User Processor Insert.", ex);
                outerException.Source = "Business.Processing.UserProcessor";
                throw outerException;
            }
        }


        protected override void ProcessUpdate()
        {
            try
            {
                long userdetailid = 0;
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {

                    UserInfo userInfo = CommonUtility.DeserializeXML<UserInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var subStore = dbContext.DigiMasterLocations.Where(l => l.SyncCode == ChangeInfo.SubStore.SiteCode && l.Level == 4).FirstOrDefault();

                        var dbUser = dbContext.Users.Where(r => r.SyncCode == userInfo.SyncCode).FirstOrDefault();
                        if (dbUser != null)
                        {
                            var dbUserdetails = dbContext.UserDetails.Where(r => r.UserDetailId == dbUser.UserDetailId).FirstOrDefault();
                            if (dbUserdetails != null)
                            {
                                dbUserdetails.FirstName = userInfo.FirstName;
                                //dbUserdetails.LastName = userInfo.LastName;
                                if (string.IsNullOrEmpty(userInfo.LastName))
                                {
                                    dbUserdetails.LastName = userInfo.FirstName;
                                }
                                else
                                {
                                    dbUserdetails.LastName = userInfo.LastName;
                                }
                                dbUserdetails.MiddleName = userInfo.MiddleName;
                                dbUserdetails.EmailAddress = userInfo.EmailAddress;
                                dbUserdetails.PhoneNumber = userInfo.PhoneNumber;
                                dbContext.SaveChanges();
                                userdetailid = dbUserdetails.UserDetailId;
                            }

                            dbUser.UserName = userInfo.UserName;
                            dbUser.PasswordHash = userInfo.Password;
                            dbUser.IsActive = userInfo.IsActive;
                            dbUser.CreatedDateTime = System.DateTime.Now;
                            dbUser.SyncCode = userInfo.SyncCode;
                            dbUser.UserTypeCodeId = 6;
                            dbUser.IsImixUser = true;
                            if (subStore != null)
                            {
                                dbUser.StoreId = subStore.ParentDigiMasterLocationId;
                            }

                            dbUser.UserDetailId = userdetailid;
                            dbUser.IsSynced = true;
                            dbContext.SaveChanges();
                            long userid = dbUser.UserId;

                            var role = dbContext.Roles.Where(r => r.SyncCode == userInfo.RoleSyncCode).FirstOrDefault();
                            if (role != null)
                            {
                                var roles = new UserRole();
                                var abc = dbContext.UserRoles.Where(r => r.UserId == userid).FirstOrDefault();
                                if (abc != null)
                                {
                                    abc.RoleId = role.RoleId;
                                    abc.UserId = userid;
                                    dbContext.SaveChanges();
                                }
                            }

                            var userauth = dbContext.DigiMasterLocations.Where(r => r.SyncCode == userInfo.LocationSyncCode).FirstOrDefault();
                            if (userauth != null)
                            {
                                var userauthorization = new UserAuthorization();
                                var Locauthorization = dbContext.UserAuthorizations.Where(r => r.UserId == userid).FirstOrDefault();
                                if (Locauthorization != null)
                                {
                                    Locauthorization.DigiMasterLocationId = userauth.DigiMasterLocationId;
                                    Locauthorization.UserId = userid;
                                    dbContext.SaveChanges();
                                }
                            }
                        }

                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                log.Error("UserProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                Exception outerException = new ProcessingException("Error occured in User Processor Update.", ex);
                outerException.Source = "Business.Processing.UserProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbUser = dbContext.Users.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    var userrole = dbContext.UserRoles.Where(r => r.UserId == dbUser.UserId).FirstOrDefault();

                    if (userrole != null)
                    {
                        dbContext.UserRoles.Remove(userrole);
                        dbContext.SaveChanges();
                    }

                    var userauth = dbContext.UserAuthorizations.Where(r => r.UserId == dbUser.UserId).FirstOrDefault();
                    if (userauth != null)
                    {
                        dbContext.UserAuthorizations.Remove(userauth);
                        dbContext.SaveChanges();
                    }

                    if (dbUser != null)
                    {
                        dbContext.Users.Remove(dbUser);
                        dbContext.SaveChanges();
                    }

                    var UserDetails = dbContext.UserDetails.Where(r => r.UserDetailId == dbUser.UserDetailId).FirstOrDefault();

                    if (UserDetails != null)
                    {
                        dbContext.UserDetails.Remove(UserDetails);
                        dbContext.SaveChanges();
                    }

                }

            }
            catch (Exception ex)
            {
                log.Error("UserProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in User Processor Delete.", ex);
                outerException.Source = "Business.Processing.UserProcessor";
                throw outerException;
            }
        }
    }
}
