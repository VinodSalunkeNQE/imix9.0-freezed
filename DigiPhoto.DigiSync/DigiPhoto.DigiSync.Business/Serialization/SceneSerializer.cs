﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System.Configuration;
namespace DigiPhoto.DigiSync.Business.Serialization
{
    public class SceneSerializer : Serializer, IDownloadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    SceneInfo sceneInfo = (from s in dbContext.Scenes
                                           join p in dbContext.Products on s.ProductId equals p.ProductId
                                           join bg in dbContext.Backgrounds on s.BackgoundId equals bg.BackgoundId
                                           join br in dbContext.Borders on s.BorderId equals br.BorderId
                                           where s.SceneId == ObjectValueId
                                           select new SceneInfo
                                           {
                                               SceneId = s.SceneId,
                                               BackGroundId = s.BackgoundId,
                                               BackgroundName = bg.BackgroundImage,
                                               BorderId = s.BorderId,
                                               BorderName = br.Name,
                                               GraphicName = string.Empty,
                                               GraphicsId = 0,
                                               IsActive = s.IsActive,
                                               IsSynced = s.IsSynced,
                                               ProductId = s.ProductId,
                                               ProductName = p.Name,
                                               SceneName = s.SceneName,
                                               SyncCode = s.SyncCode,
                                               ProductSyncCode = p.SyncCode,
                                               BackgroundSyncCode = bg.SyncCode,
                                               BorderSyncCode = br.SyncCode

                                           }).FirstOrDefault();


                    if (sceneInfo != null)
                    {
                        dataXML = CommonUtility.SerializeObject<SceneInfo>(sceneInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Scene Serialization.", e);
                outerException.Source = "Business.Serialization.SceneSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            return input.DeserializeXML<SceneInfo>();
        }
        private DownloadInfo download;
        public DownloadInfo Download
        {
            get
            {
                return null;
            }
            set
            {
                download = value;
            }
        }
    }
}
