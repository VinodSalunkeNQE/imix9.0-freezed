﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Digiphoto.ManualDownload.Process
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            //System.Diagnostics.Debugger.Break();
            base.Install(stateSaver);

            string targetDirectory = Context.Parameters["targetdir"];

            string servername = Context.Parameters["Servername"];

            string username = Context.Parameters["Username"];

            string password = Context.Parameters["Password"];


            string database = "Digiphoto";
            if (string.IsNullOrEmpty(targetDirectory) || string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return;
            }
            //System.Diagnostics.Debugger.Break();

            string exePath = string.Format("{0}ManualDownloadProcess.exe", targetDirectory);

            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);

            string connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;

            ConnectionStringSettings connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            String conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True;App=EntityFramework";

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn, "System.Data.SqlClient");

            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);

            ConfigurationSection section = config.GetSection("connectionStrings");

            //Ensures that the section is not already protected
            if (!section.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");

            //connectionstring = new ConnectionStringSettings("DigiPhotoEntities", conn, "System.Data.SqlClient");
            //config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
            //config.Save(ConfigurationSaveMode.Modified, true);
            //ConfigurationManager.RefreshSection("connectionStrings");

        } 
    }
}
