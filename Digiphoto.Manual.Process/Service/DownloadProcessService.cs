﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;

namespace Digiphoto.ManualDownload.Process
{
    public partial class DownloadProcessService : ServiceBase
    {
        public DownloadProcessService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
#if DEBUG
            ErrorHandler.ErrorHandler.LogFileWrite("DownloadProcessService in debug mode");
#endif
            try
            {
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
            Digiphoto.ManualDownload.Process.ServiceController.Instance.StartService();
        }
                else
                {
                    throw new Exception("Already Started");                    
                }
            }catch(Exception ex)
            {
                ExitCode = 13816;
                this.Stop();
            }

          

        }

        protected override void OnStop()
        {
            Digiphoto.ManualDownload.Process.ServiceController.Instance.StartService();

            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
    }
}
