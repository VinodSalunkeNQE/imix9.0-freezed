﻿using DigiPhoto.LogEnvelop.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Digiphoto.ManualDownload.Process
{
    public class ServiceController : IDisposable
    {
        /// <summary>
        /// Generic Timer Collection
        /// </summary>
        private GenericTimer<short>[] _dbPollerTimers = null;

        /// <summary>
        /// Expcetion Logger
        /// </summary>
        //private ExceptionLogger _logger = null;

        /// <summary>
        /// Worker Running Flag
        /// </summary>
        private bool _workerRunning;
        //private bool _running = false;

        private short _retryCount = 3;

        private short _storeId = 0;


        #region Singleton

        /// <summary>
        /// Service Controller
        /// </summary>
        private static volatile ServiceController _controller;

        /// <summary>
        /// Sync object that will use for thead safe instance.
        /// </summary>
        private static object syncRoot = new Object();

        /// <summary>
        /// Prviate constructor
        /// </summary>
        private ServiceController()
        {
            _workerRunning = false;
            //_logger = ExceptionLogger.Instance();
            //_running = false;
        }

        /// <summary>
        /// Create single instance of Service Controller
        /// </summary>
        public static ServiceController Instance
        {
            get
            {
                if (_controller == null)
                {
                    lock (syncRoot)
                    {
                        if (_controller == null)
                            _controller = new ServiceController();
                    }
                }

                return _controller;
            }
        }

        #endregion
        /// <summary>
        /// InitializeTimers for each store.
        /// </summary>
        private void InitializeTimers()
        {
            string storeIds = ConfigurationManager.AppSettings["storeIDs"];
            if (String.IsNullOrEmpty(storeIds) || String.IsNullOrWhiteSpace(storeIds))
            {
                //_logger.LogException("Stored list cannot be empty, please check the config file for storeIDs", 
                //    DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                ErrorHandler.ErrorHandler.LogFileWrite("Stored list cannot be empty, please check the config file for storeIDs");

                return;
            }

            string[] strStoreIds = storeIds.Split(',');
            if (strStoreIds.Length == 0)
            {
                //_logger.LogException("Stored list cannot be empty, please check the config file for storeIDs",
                //    DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                ErrorHandler.ErrorHandler.LogFileWrite("Stored list cannot be empty, please check the config file for storeIDs");

                return;
            }
            _dbPollerTimers = new GenericTimer<short>[strStoreIds.Length];

            double interval = Convert.ToDouble(ConfigurationManager.AppSettings["DbPollerTimeInterval"]);
            for (int i = 0; i < strStoreIds.Length; i++)
            {
                short storeId = short.Parse(strStoreIds[i].ToString());
                _dbPollerTimers[i] = new GenericTimer<short>(storeId);
                _dbPollerTimers[i].Interval = interval;
                _dbPollerTimers[i].Elapsed += new ElapsedEventHandler(OnGetPollerTimer);
                _dbPollerTimers[i].Enabled = false;
            }

        }

        /// <summary>
        /// Method that will start the service 
        /// </summary>
        public void StartService()
        {
            _workerRunning = true;
            //_logger.LogException("Start service", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
            //ErrorHandler.ErrorHandler.LogFileWrite("Start service");
            try
            {
                //_logger.LogException("Intialize timers", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                InitializeTimers();
                for (int i = 0; i < _dbPollerTimers.Length; i++)
                {
                    _dbPollerTimers[i].Enabled = true;
                    _dbPollerTimers[i].Start();
                }
                //_logger.LogException("Timer Started", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //ErrorHandler.ErrorHandler.LogFileWrite("End service");
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex.Message, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        /// <summary>
        /// Method that will stop service.
        /// </summary>
        public void StopService()
        {
            lock (_controller)
            {
                if (_workerRunning)
                {
                    //_logger.LogException("Stopping service", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                    //ErrorHandler.ErrorHandler.LogFileWrite("Stopping service");
                    try
                    {
                        _workerRunning = false;
                        for (int i = 0; i < _dbPollerTimers.Length; i++)
                        {
                            _dbPollerTimers[i].Stop();
                            _dbPollerTimers[i].Enabled = false;
                        }
                        //_logger.LogException("Stopped service", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                        //ErrorHandler.ErrorHandler.LogFileWrite("Service stopped.");

                        GC.SuppressFinalize(this);
                    }
                    catch (ApplicationException appExcep)
                    {
                        // throw server exception
                        //_logger.LogException(appExcep.Message, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                        ErrorHandler.ErrorHandler.LogFileWrite(appExcep.Message);
                    }
                }
            }
        }


        #region events
        /// <summary>
        /// Poller timer Event that will be raised by timer.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">ElapsedEventArgs</param>
        private void OnGetPollerTimer(Object sender, ElapsedEventArgs e)
        {
            GenericTimer<short> dbPollerTimer = (GenericTimer<short>)sender;
            //if (dbPollerTimer == null)
            //{
            //    _logger.LogException("Poller timer sender should be GenericTimer", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
            //    return;
            //}

            //short storeId = 0;
            try
            {
                //_logger.LogException("Time elapsed, Processing started", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //ErrorHandler.ErrorHandler.LogFileWrite("Time elapsed, Processing started");
                if (dbPollerTimer != null && _workerRunning)
                {
                    dbPollerTimer.Stop();
                    //_logger.LogException("Timer disabled", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                    //_running = true;
                    //Get messages and send one by one
                    _storeId = dbPollerTimer.Object;

                    lock (_controller)
                    {
                        //_logger.LogException("Timer disabled", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                        //this._running = false;
                        //_logger.LogException("Time to pull data from source", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                        ProcessRequest(_storeId);

                        if (dbPollerTimer!=null)
                            dbPollerTimer.Start();
                    }

                }

            }
            catch (Exception excep)
            {
                // log the error
                //_logger.LogException(excep, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(excep.Message);

                if (dbPollerTimer != null)
                    dbPollerTimer.Start();

            }
            finally
            {
                //if (dbPollerTimer != null)
                //{
                //    dbPollerTimer.Start();
                //    //_logger.LogException("Processing completed, timer started", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //    //ErrorHandler.ErrorHandler.LogFileWrite("Processing completed, timer started");
                //}
            }
        }
        #endregion

        /// <summary>
        /// Copy Image from Source to Destination.
        /// </summary>
        /// <param name="storeId">short</param>
        private void ProcessRequest(short storeId)
        {
            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
            {
                // Redirect the output stream of the child process.
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                
                //ErrorHandler.ErrorHandler.LogFileWrite("Start Loading console application.");

                //process.StartInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Digiphoto.Manual.Console.exe");

                string processName = ConfigurationManager.AppSettings["ProcessConsoleFilePath_" + storeId.ToString()].ToString();
                if (string.IsNullOrEmpty(processName))
                    return;

                process.StartInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, processName);

                ///ErrorHandler.ErrorHandler.LogFileWrite("End Loading console application.");

                //process.StartInfo.FileName = ConfigurationManager.AppSettings["StoreConsoleFilePath_" + storeId.ToString()].ToString();
                //ConfigurationManager.AppSettings["StoreConsoleFilePath_" + storeId.ToString()].ToString();
                process.Start();
                // Do not wait for the child process to exit before reading to the end of its redirected stream.
                // p.WaitForExit();
                // Read the output stream first and then wait.
                string output = process.StandardOutput.ReadToEnd();

                //process.EnableRaisingEvents = true;
                //process.Exited += ReLaunchIfCrashed;
                process.WaitForExit();
            }
        }


        private void ReLaunchIfCrashed(object o, EventArgs e)
        {
            try
            {
                //_logger.LogException("Restart application if get crahsed", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                ErrorHandler.ErrorHandler.LogFileWrite("Restart application if get crashed.");
                System.Diagnostics.Process process = (System.Diagnostics.Process)o;
                if (process.ExitCode != 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Application is restarted.");
                    if (_retryCount-- > 0) // restart at max count times
                        ProcessRequest(_storeId);
                    else
                        Environment.Exit(process.ExitCode);
                }
                else
                {
                    Environment.Exit(0);
                }
            }catch(Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void Initialize()
        {
            _dbPollerTimers = null;
        }
        public void Dispose()
        {
            Initialize();
            //_logger = null;
        }
    }
}
