﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Digiphoto.ManualDownload.Process
{
    partial class GenericTimer<T> : Timer
    {
        private T _obj;

        public GenericTimer()
        {
        }

        public GenericTimer(T obj)
        {
            _obj = obj;
        }

        public T Object
        {
            get { return _obj; }
            set { _obj = value; }
        }
    }
}
