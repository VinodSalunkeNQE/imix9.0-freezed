﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DataLayer
{
    public class PreScanWatcher
    {
        public Int32 PhotographerId { get; set; }
        public string CodeFormat { get; set; }
        public string CodeValue { get; set; }
        public DateTime LastImgTime { get; set; }
    }

    public class ImgInfoPostScanList : ICloneable
    {
        public ImgInfoPostScanList()
        {
            this.ListImgPhotoId = new List<int>();
        }

        public List<int> ListImgPhotoId { get; set; }
        public string Barcode { get; set; }
        public string Format { get; set; }

        public int PhotoGrapherId { get; set; }
        public DateTime LastImgTime { get; set; }
        public object Clone()
        {
            return new ImgInfoPostScanList
            {
                ListImgPhotoId = this.ListImgPhotoId,
                Barcode = this.Barcode,
                Format = this.Format,
                PhotoGrapherId = this.PhotoGrapherId,
                LastImgTime = this.LastImgTime,
            };
        }
    }
}
