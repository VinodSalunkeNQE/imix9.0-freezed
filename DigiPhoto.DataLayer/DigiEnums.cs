﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DataLayer
{
    public enum ConfigParams
    {
        IsBarcodeActive = 1,
        DefaultMappingCode = 2,
        ScanType = 3,
        LostImgTimeGap = 4,
        OnlineImageResize = 5,
        StorageMediaImageResize = 6,
        Online = 7,
        DgServiceURL = 8,
        DbBackupPath = 9,
        CleanUpTable = 10,
        HfBackupPath = 11,
        ScheduleBackup = 12,
        IsBackupScheduled = 13,
        IsRecursive = 14,
        IntervalCount = 15,
        IntervalType = 16,
        CleanUpDaysBackup = 17,
        DigimagicBrightness = 18,
        DigimagicContrast = 19,
        DigimagicSharpen = 20,
        Contrast = 21,
        Brightness = 22,
        IsDeleteFromUSB = 23,
        MktImgPath = 24,
        MktImgTimeInSec = 25,
        #region Start FTP
        FtpFolder = 26,
        FtpIP = 27,
        FtpPwd = 28,
        FtpUid = 29,
        #endregion
        #region ScreenSaverSettings

        SampleImagePath = 30,
        DisplayDuration = 31,
        ScreenStartTime = 32,
        IsScreenSaverActive = 33,

        #endregion

        IsDefaultBackImageEnabled = 34,
        DefaultBackImagePath = 35,
        IsVersionUpdateEnabled = 36,
        UpdateServicePath = 37,
        IsAnonymousQrCodeEnabled = 38,
        ProductPreview = 39,
        IsPrepaidAutoPurchaseActive = 40,
        CleanUpDaysOldBackup = 41,
        ResizeHeight = 42,
        ResizeWidth = 43,
        FrameRate = 44,
        OutputFormat = 45,
        VideoCodecs = 46,
        AudioCodecs = 47,
        IsEnabledVideoEdit = 48,


        #region Baracoda-RFID
        IsEnabledRFID = 49,
        RFIDScanType = 50,
        RFIDFlushInHour = 51,
        #endregion
        EnablePaymentScreenOnClientView = 52,
        Card = 53,
        Cash = 54,
        IsOrderwiseLoadBalancingActive = 55,

        IsPhotographerSerailSearchActive = 67,
        IsSyncedEnabled = 69,
        SyncServiceInterval = 70,
        SyncDataDelay = 71,
        IsEnabledSessionSelect = 72,
        IsEnabledSelectEnd = 73,
        ShowAllSubstorePhotos = 74,
        IsEnableSingleScreenPreview = 75,
        SequencePrefix = 76,
        BackUpEmailAddress = 77,
        BackUpEmailSubject = 78,
        IsEnableSlipPrint = 79,
        ImageBarCodeTextFontSize = 80,
        IsEnablePartialEditedImage = 182,
        SyncImageType = 194,


        RfidFlushDateTime = 81,
        IsRecursiveRfid = 82,
        RfidRecursiveDays = 83,
        RfidFlushReceiverEmail = 84,
        RfidRecursiveExcludeDays = 85,
        RfidFlushType = 86,

        # region video auto-processing
        IsEnableAutoVidProcessing = 56,
        AutoVidProductId = 57,
        VideoWriterSettings = 58,
        ApplyMP4CUDA = 59,
        IsRotatedVideo = 60,
        AutoVideoLength = 61,
        AutoVidDisplayEnabled = 62,
        AutoVidDisplayFolder = 63,
        AutoVidApplyChroma = 64,
        AutoVidChromaColor = 65,
        AutoVidChromaBackgroundID = 66,
        NoOfDisplayScreens = 87,
        IsCyclicDisplay = 88,
        QRCodeAutoPurchase = 89,
        FlipVidAspectRatio = 90,
        RFIDAssociationType = 91,
        RFIDAssociationTimeFrame = 92,
        ImageBarCodeColor = 93,
        BarCodeOrientation = 147,
        BarCodePostion = 148,
        ColorCode = 94,
        ScheduleBackupDaily = 95,
        IsMobileRfidEnabled = 96,
        ResizeImage = 97,
        #endregion
        # region VisioForge License
        VisioForgeLicenseKey = 98,
        VisioForgeUsername = 99,
        VisioForgeEmailId = 100,
        MediaPlayerLicenseKey = 101,
        LiveCaptureLicenseKey = 102,
        CudaCodec = 103,
        DebugVisioForge = 104,
        LogFolderPath = 105,
        # endregion
        IsAutoColorCorrection = 106,
        QRCodeLengthSetting = 107,
        VideoProfileId = 108,
        IsCorrectAtDownloadActive = 109,
        IsContrastCorrection = 110,
        IsGammaCorrection = 111,
        IsNoiseReduction = 112,
        IsEventEmailPackage = 113,
        ChangeTrackingAppObjectIDs = 114,
        IsAppMemoryOverflow = 115,
        CalenderPrintMargin = 116,
        ReSyncDataDelay = 117,
        IsGreenScreenFlow = 119,
        // *** Below Code Added by Anis on 30-Oct-2018 for - Create Magic shot on demand ***//
        IsOnlyMagicShot = 232,
        // *** End Here ***//
        IsAdvancedVideoEditActive = 120,
        AutomatedVideoEditWorkFlow = 123,
        MarginValue4X6_2 = 121,
        SingleScreenPreviewAngle = 126,
        IsOpeningProcMendatory = 122,
        SiteRevenueForSellingLocation = 124,
        ClosingProcTime = 125,
        IsEnableImageEditingMDownload = 179,
        FailedOnlineOrderCleanupDays = 180,
        ArchiveCleanupDays = 218,
        #region Preview Wall

        NoOfScreenPW = 127,
        DelayTimePW = 128,
        PreviewTimePW = 129,
        HighLightTimePW = 130,
        IsMktImgPW = 131,
        MktImgPathPW = 132,
        WaterMarkPathPW = 133,
        IsLogoPW = 134,
        LogoPathPW = 135,
        IsPreviewEnabledPW = 136,
        IsSpecImgPW = 137,
        VerticalBorderPW = 138,
        HorizentalBorderPW = 139,
        TripCamCleanupDays = 140,
        ImixCleanUpDays = 141,
        PreviewWallDummyTag = 142,
        PreviewWallLogoPosition = 149,
        PreviewWallRFIDPostion = 150,
        IsVisibleGumBallZeroScore = 177,
        IsVisibleGumBallZeroScoreOnImage = 178,

        #region PreviewWallEnhancement        
        GuestExplanatoryImgPathPW = 233,
        IsHorizontalGuestExplatory = 234,
        IsLoopPreviewPhotos = 235,
        LoopPreviewPhotosTime = 236,
        RideNoOfCaptures = 237,
        RideNoOfCapturesIterations = 238,
        #endregion

        #endregion

        VideoFlipMode = 151,
        ValidateQRCode = 152,
        DynamicBGFunctionalityActive = 156,
        MaxDelayAllowedInDummyCaptureDownload = 155,
        IsPrintButtonVisible = 154,
        TripCameraPropertyPassword = 157,

        #region Gumball
        GumRideAvailableLocations = 158,
        GumRideFontSize = 159,
        GumRideFontColor = 160,
        GumRideFontWeight = 161,
        GumRideBackgrondColor = 162,
        GumRidePosition = 163,
        GumRideMargin = 164,
        GumRideFilePath = 165,
        IsGumRideActive = 166,
        IsGumPlayerScoreVisible = 167,
        GumRideFontStyle = 168,
        GumRideFontFamily = 169,
        GumRideTextFileTimeOut = 173,
        QrCodeOnlineProductURL = 174,
        QrCodeOnlineProductText = 175,
        GumRideInputPhotoPath = 181,
        GumballScoreSeperater = 201,
        IsPrefixActiveFlow = 202,
        PrefixPhotoName = 203,
        PrefixScoreFileName = 204,
        #endregion

        #region ReportConfiguration
        ExportPath = 143,
        ScheduledTime = 144,
        IsServiceRecursive = 145,
        ExportReportEmailAddress = 146,
        #endregion

        #region Auto Video Frame Extraction
        IsAutoVideoFrameExtEnabled = 170,
        VideoFrameCount = 171,
        VideoFramePositions = 172,
        VideoFrameCropRatio = 176,
        #endregion

        #region  Resync Config
        IsResyncActive = 195,
        InitialResyncDateTime = 196,
        ResyncIntervalCount = 197,
        ResyncIntervalType = 198,
        ResyncMaxRetryCount = 199,
        #endregion
        #region Other Config
        SmallWalletWith4ImageCutter = 200,
        ReceiptLogoPath = 205,
        CountZeroAmtTransinSageSaleTransCnt = 212,

        IncludetaxForNightlyReport = 213,
        #endregion

        #region WaterMark Config
        WaterMarkProduct = 206,
        WaterMarkLocation = 207,
        WaterMarkEnable = 208,
        WaterMarkScheduledOn = 209,
        MarginValueUnique4X6_2 = 210,
        NoOfGroupsToAssociate = 211,
        //-----Nilesh----Start-----Add Three enum for Gumball Dynamic Img Setting----24th Feb 2018----
        GumBGImagePath = 214,
        GumBGImgHeightWidth = 215,
        GumBGImgPosition = 216,
        //-----Nilesh----End-----Add Three enum for Gumball Dynamic Img Setting------24th Feb 2018--
        DontDoDBCleanUp = 217,
        // ------------Delivery Note added by Manoj at 16-Dec-2018---Start-----------------
        CleanupArchiveDays = 218,
        CleanUpArchiveTables = 219,
        DeliveryNoteRequired = 222,
        //--------------------End----------------------------------------------------------
        //-----Start-------Nilesh----Show photo on specific substore--1Nov 2018---
        ShowCheckSubstorePhotos = 221,
        //-----End-------Nilesh----Show photo on specific substore--1Nov 2018-----
        #endregion

        #region added by ajay on 23dec20 for cliew view watermark
        IsClientViewWatermark = 239,
        #endregion

        #region Set3 Config
        IsSet3Flag = 473,
        #endregion
    }
    public enum SettingStatus
    {
        None = 0,
        Spec = 1,
        SpecUpdated = 2,
    }
    public enum VideoFlipModes
    {
        None = 0,
        Horizontal = 1,
        Vertical = 2,
    }
    public enum ScanType
    {
        PreScan = 501,
        PostScan = 502,
    }

    public enum RfidFlushType
    {
        HourWise = 1,
        DayWise = 2,
    }
    public enum BarcodeTypes
    {
        AztecCode = -2147483648,
        Code128 = 1,
        Code39 = 2,
        Interl25 = 4,
        EAN13 = 8,
        EAN8 = 16,
        Codabar = 32,
        Code11 = 64,
        UPCA = 128,
        UPCE = 256,
        Industr25 = 512,
        Code93 = 1024,
        DataBarOmni = 2048,
        DataBarLim = 4096,
        DataBarStacked = 8192,
        DataBarExp = 16384,
        DataBarExpStacked = 32768,
        AztecUnrecognized = 1048576,
        LinearUnrecognized = 16777216,
        PDF417Unrecognized = 33554432,
        DataMatrixUnrecognized = 67108864,
        QRCodeUnrecognized = 134217728,
        DataMatrix = 268435456,
        PDF417 = 536870912,
        QRCode = 1073741824,
    }
    public enum ApplicationObjectEnum
    {
        Border = 1,
        Background = 2,
        Graphic = 3,
        Product = 4,
        Package = 5,
        Discount = 6,
        Camera = 7,
        Currency = 8,
        Configuration = 9,
        Role = 11,
        User = 12,
        Location = 13,
        Order = 14,
        OrderDetails = 15,
        Activity = 16,
        Scene = 17,
        Group = 18,
        PosDetail = 20,
        ChangeTrackingExceptionDtl = 21,
        OpeningFormDetail = 22,
        ClosingFormDetail = 23

    }
    public enum PrinterStatus
    {
        Other = 1,
        Unknown = 2,
        Idle = 3,
        Printing = 4,
        WarmUp = 5,
        StoppedPrinting = 6,
        Offline = 7
    }
    public enum Digibackup
    {
        Completed = 1,
        failed = 2,
        scheduled = 3,
        Inprogress = 4,

    }
    public enum IsProductPrintType
    {
        Print = 0,
        Others = 1
    }
    public enum PaymentMode
    {
        Cash = 0,
        Card = 1,
        RoomCharges = 2,
        GiftVoucher = 3,
    }

    public enum RFIDAssociationType
    {
        PrePostBasedAssociation = 801,
        TimeBasedAssociation = 802,
        GroupBasedAssociation = 803,
        GroupBasedAssociationWithSeperator = 807
    }

    public enum BackupErrorMessage
    {
        DestinationDiskFull = 1,
        DatabaseBackupAlreadyExists = 2,
        Other = 3
    }
    public enum ControlledObject
    {
        Composer = 1,
        Mixer = 2
    }

    public enum TripCamFeatures
    {
        Height = 1,
        HeightMax = 2,
        ImageSize = 3,
        OffsetX = 4,
        OffsetY = 5,
        Width = 6,
        WidthMax = 7,
        Hue = 8,
        Saturation = 9,
        WhiteBalanceRed = 10,
        WhiteBalanceBlue = 11,
        ImagesCaptured = 12,
        ImagesDropped = 13,
        CameraName = 14,
        NoOfImages = 15,
        TriggerDelay = 16,
        ExposureTime = 17,
        CameraRotation = 18,
        DeviceTemperatureSelector = 19,
        DeviceTemperature = 20,
        CCDTemperatureOK = 21,
        StrobeDelay = 22,
        StrobeDuration = 23,
        StrobeDurationMode = 24,
        StrobeSource = 25,
        PacketSize = 26,
        MACAddress = 27,
        IPConfigurationMode = 28,
        IPAddress = 29,
        Subnet = 30,
        Gateway = 31,
        PixelFormat = 32,
        TriggerSource = 33,
        LensFocus = 34,
        LensFocusMax = 35,
        Aperture = 36,
        ApertureMin = 37,
        ApertureMax = 38,
        StreamBytesPerSec = 39,
        BlackLevel = 40,
        Gain = 41,
        Gamma = 42,
        Gain00 = 43,
        Gain01 = 44,
        Gain02 = 45,
        Gain10 = 46,
        Gain11 = 47,
        Gain12 = 48,
        Gain20 = 49,
        Gain21 = 50,
        Gain22 = 51,
        ColorTransformationMode = 52,
        ExposureMode = 53,
        //StrobeDurationModes = 54,
        SyncOutPolarity = 54,
        SyncOutSelector = 55,
        SyncOutSource = 56,
        AcquisitionMode = 57,
    }
    public enum ReportTypes
    {
        ProductionSummaryReport = 1,
        SitePerformanceReport = 2,
        TakingReport = 3,
        PaymentSummary = 4,
        IPPrintTrackingReport = 5,
        NONE,
    }
}
