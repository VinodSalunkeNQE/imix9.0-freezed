﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
namespace DigiPhoto.DataLayer
{
    class SendMailError
    {
        string sendTo = "faraz.hasan@digiphotogulf.com";
        string sendFrom = "faraz.hasan@digiphotogulf.com";
        string subject = "Digi Application Error";
        MailMessage message = new MailMessage();
        public void SendMail(string message)
        {


        }

    }

    public class PhotoPrintPosition
    {
        public PhotoPrintPosition()
        {
        }
        public PhotoPrintPosition(int PageNo, int PhotoPosition, int RotationAngle)
        {
            this.PageNo = PageNo;
            this.PhotoPosition = PhotoPosition;
            this.RotationAngle = RotationAngle;
        }
        public int PageNo { get; set; }
        public int PhotoPosition { get; set; }
        public int RotationAngle { get; set; }
    }


    public class PhotoPrintPositionDic
    {
        public PhotoPrintPositionDic()
        {
            PhotoPrintPositionList = new PhotoPrintPosition();
        }
        public PhotoPrintPositionDic(int PhotoId, PhotoPrintPosition photoPrintPosition)
        {
            this.PhotoPrintPositionList = new PhotoPrintPosition();
            this.PhotoPrintPositionList = photoPrintPosition;
            this.PhotoId = PhotoId;
        }
        public int PhotoId { get; set; }
        public PhotoPrintPosition PhotoPrintPositionList = null;
    }

    public class BurnOrderInfo
    {
        public string OrderNumber { get; set; }
        public int OrderDetailId { get; set; }
        public int ProductType { get; set; }
        public string PhotosId { get; set; }
        public int Status { get; set; }
    }
    public class CardTypeInfo
    {
        public int IMIXImageCardTypeId { get; set; }
        public string Name { get; set; }
        public string CardIdentificationDigit { get; set; }
        public int MaxImages { get; set; }
        public string Status { get; set; }
        public string ImageIdentificationType { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
