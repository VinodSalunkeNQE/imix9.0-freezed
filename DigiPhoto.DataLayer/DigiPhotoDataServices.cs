﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Data.EntityClient;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using System.Data.Objects;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;
namespace DigiPhoto.DataLayer
{


    public class DigiPhotoDataServices
    {
        public string connectionstring = ConfigurationManager.ConnectionStrings["DigiphotoEntities"].ConnectionString;
        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public vw_GetUserDetails GetUserDetails(string username, string password)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Name == username && t.DG_User_Password == password && t.DG_User_Status == true).FirstOrDefault();
                return _objUserDetails;
            }
        }


        /// <summary>
        /// Gets default chroma info from configurations.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, decimal?> GetCromaColor(int SubStoreId)
        {
            Dictionary<string, decimal?> objDic = null;
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objCromaInfo = context.DG_Configuration.Where(o => o.DG_Substore_Id == SubStoreId).FirstOrDefault();
                if (_objCromaInfo != null)
                {
                    objDic = new Dictionary<string, decimal?>();
                    objDic.Add(_objCromaInfo.DG_ChromaColor, _objCromaInfo.DG_ChromaTolerance);
                }
                return objDic;
            }
        }

        /// <summary>
        /// Gets the currency only.
        /// </summary>
        /// <returns></returns>
        public List<DG_Currency> GetCurrencyOnly()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.DG_Currency.ToList().Where(t => t.DG_Currency_IsActive == true).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the queue detail.
        /// </summary>
        /// <param name="QueueID">The queue unique identifier.</param>
        /// <returns></returns>
        public GetPrinterQueueDetails_Result GetQueueDetail(int QueueID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.GetPrinterQueueDetails(QueueID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the data for printing log.
        /// </summary>
        /// <param name="fromdate">The fromdate.</param>
        /// <param name="todate">The todate.</param>
        /// <returns></returns>
        public List<GetPrintedProduct_Result> GetDataForPrintingLog(DateTime fromdate, DateTime todate,string substoreName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var data = context.PrintedProduct(fromdate, todate, substoreName).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the data for printing summary.
        /// </summary>
        /// <param name="fromdate">The fromdate.</param>
        /// <param name="todate">The todate.</param>
        /// <returns></returns>
        public List<PrintSummary_Result> GetDataForPrintingSummary(DateTime fromdate, DateTime todate,string subStoreName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var data = context.PrintSummary(fromdate, todate, subStoreName).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the order detail report.
        /// </summary>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <returns></returns>
        public List<OrderDetailedDiscount_Get_Result> GetOrderDetailReport(DateTime FromDate, DateTime ToDate, String StoreName, String UserName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.OrderDetailedDiscount_Get(FromDate, ToDate, StoreName, UserName).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the version details.
        /// </summary>
        /// <param name="MachineName">Name of the machine.</param>
        /// <returns></returns>
        public List<DG_VersionHistory> GetVersionDetails(string MachineName)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var _objversionDetails = context.DG_VersionHistory.Where(t => t.DG_Machine == MachineName).ToList();
                return _objversionDetails;


            }
        }
        /// <summary>
        /// Gets the user details by user unique identifier.
        /// </summary>
        /// <param name="userId">The user unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetUserDetails> GetUserDetailsByUserId(int userId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                if (Convert.ToInt32(userId) == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.ToList();
                    return _objUserDetails;
                }
                else
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_pkey == userId).ToList();
                    return _objUserDetails;
                }
            }
        }
        /// <summary>
        /// Gets the name of the user details by user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public vw_GetUserDetails GetUserDetailsByUserName(string username)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Name == username).FirstOrDefault();
                return _objUserDetails;
            }
        }
        /// <summary>
        /// Searches the user details.
        /// </summary>
        /// <param name="FName">Name of the configuration.</param>
        /// <param name="LName">Name of the calculate.</param>
        /// <param name="StoreId">The store unique identifier.</param>
        /// <param name="Status">The status.</param>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <param name="MobileNumber">The mobile number.</param>
        /// <param name="EmailId">The email unique identifier.</param>
        /// <param name="locationId">The location unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetUserDetails> SearchUserDetails(string FName, string LName, int StoreId, string Status, int RoleId, string MobileNumber, string EmailId, int locationId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                #region common
                if (!string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Last_Name == LName && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status != "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Status == isActive).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status == "0" && !string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_PhoneNo == MobileNumber && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && !string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Email == EmailId && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId != 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_Location_pkey == locationId && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                if (string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId != 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_Roles_Id == RoleId && t.DG_Store_ID == StoreId).ToList();
                    return _objUserDetails;
                }
                #endregion


                #region main search
                if (!string.IsNullOrEmpty(FName) && string.IsNullOrEmpty(LName) && Status != "0" && !string.IsNullOrEmpty(MobileNumber) && !string.IsNullOrEmpty(EmailId) && locationId != 0 && RoleId != 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName && t.DG_User_Status == isActive && t.DG_User_PhoneNo == MobileNumber && t.DG_User_Email == EmailId && t.DG_User_Roles_Id == RoleId).ToList();
                    return _objUserDetails;
                }
                if (!string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status == "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName).ToList();
                    return _objUserDetails;
                }
                if (!string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status != "0" && string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName && t.DG_User_Status == isActive).ToList();
                    return _objUserDetails;
                }
                if (!string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status != "0" && !string.IsNullOrEmpty(MobileNumber) && string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName && t.DG_User_Status == isActive && t.DG_User_PhoneNo == MobileNumber).ToList();
                    return _objUserDetails;
                }
                if (!string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status != "0" && !string.IsNullOrEmpty(MobileNumber) && !string.IsNullOrEmpty(EmailId) && locationId == 0 && RoleId == 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName && t.DG_User_Status == isActive && t.DG_User_PhoneNo == MobileNumber && t.DG_User_Email == EmailId).ToList();
                    return _objUserDetails;
                }
                if (!string.IsNullOrEmpty(FName) && !string.IsNullOrEmpty(LName) && Status != "0" && !string.IsNullOrEmpty(MobileNumber) && !string.IsNullOrEmpty(EmailId) && locationId != 0 && RoleId == 0)
                {
                    bool? isActive = false;
                    if (Status == "1")
                    {
                        isActive = true;
                    }
                    else if (Status == "2")
                    {
                        isActive = false;
                    }
                    var _objUserDetails = context.vw_GetUserDetails.Where(t => t.DG_User_First_Name == FName && t.DG_Store_ID == StoreId && t.DG_User_Last_Name == LName && t.DG_User_Status == isActive && t.DG_User_PhoneNo == MobileNumber && t.DG_User_Email == EmailId && t.DG_Location_pkey == locationId).ToList();
                    return _objUserDetails;
                }
                return null;


                #endregion
            }
        }
        /// <summary>
        /// Gets the searched photo.
        /// </summary>
        /// <param name="fromTime">From time.</param>
        /// <param name="ToTime">The automatic time.</param>
        /// <param name="UserId">The user unique identifier.</param>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <param name="substoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetPhotoList> GetSearchedPhoto(DateTime fromTime, DateTime ToTime, Int32 UserId, Int32 LocationId, string substoreId)
        {

            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                if (fromTime != null && ToTime != null && UserId != 0 && LocationId != 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => (t.DG_Photos_CreatedOn > fromTime && t.DG_Photos_CreatedOn < ToTime) && t.DG_Photos_UserID == UserId && t.DG_Location_ID == LocationId && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime != null && ToTime != null && UserId == 0 && LocationId == 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => ((t.DG_Photos_CreatedOn > fromTime && t.DG_Photos_CreatedOn < ToTime) && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null))).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime == null && ToTime == null && UserId != 0 && LocationId == 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => t.DG_Photos_UserID == UserId && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime == null && ToTime == null && UserId == 0 && LocationId != 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => t.DG_Location_ID == LocationId && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime == null && ToTime == null && UserId != 0 && LocationId != 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => t.DG_Photos_UserID == UserId && t.DG_Location_ID == LocationId && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime != null && ToTime != null && UserId != 0 && LocationId == 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => t.DG_Photos_UserID == UserId && (t.DG_Photos_CreatedOn > fromTime && t.DG_Photos_CreatedOn < ToTime) && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else if (fromTime != null && ToTime != null && UserId == 0 && LocationId != 0)
                {
                    var _objPhotoDetails = context.vw_GetPhotoList.Where(t => (t.DG_Location_ID == LocationId) && (t.DG_Photos_CreatedOn > fromTime && t.DG_Photos_CreatedOn < ToTime) && (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objPhotoDetails.Where(t => substoreId.Split(',').ToList().Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }
                else
                {
                    return null;
                }

            }
        }
        /// <summary>
        /// Gets the name of the location.
        /// </summary>
        /// <param name="StoreId">The store unique identifier.</param>
        /// <returns></returns>
        public List<DG_Location> GetLocationName(int StoreId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objLocationDetails = context.DG_Location.Where(t => t.DG_Store_ID == StoreId && t.DG_Location_IsActive == true).ToList();
                return _objLocationDetails;
            }
        }
        /// <summary>
        /// Determines whether [is ready for print] [the specified queue unique identifier].
        /// </summary>
        /// <param name="QueueID">The queue unique identifier.</param>
        /// <returns></returns>
        public bool IsReadyForPrint(int QueueID)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPrinterqueue = context.DG_PrinterQueue.Where(t => (t.DG_PrinterQueue_Pkey == QueueID && t.DG_SentToPrinter == false)).FirstOrDefault();
                if (!(bool)_objPrinterqueue.is_Active)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Gets the photo graphers list.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetUserDetails> GetPhotoGraphersList(int storeId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objUsersList = context.vw_GetUserDetails.Where(t => t.DG_Store_ID == storeId && t.DG_User_Roles_Id == 8 && t.DG_User_Status == true).ToList();
                return _objUsersList;
            }
        }
        //public List<> GetPhotoGraphersList(int storeId)
        //{
        //    using (EntityConnection connection = new EntityConnection(connectionstring))
        //    {
        //        DigiphotoEntities context = new DigiphotoEntities(connection);
        //        var _objUsersList = context.vw_GetUserDetails.Where(t => t.DG_Store_ID == storeId && t.DG_User_Roles_Id == 8 && t.DG_User_Status == true).ToList();
        //        return _objUsersList;
        //    }
        //}
        /// <summary>
        /// Gets the location list.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        /// <returns></returns>
        public List<DG_Location> GetLocationList(int storeId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objlocationList = context.DG_Location.Where(t => t.DG_Store_ID == storeId && t.DG_Location_IsActive == true).ToList();
                return _objlocationList;
            }
        }
        /// <summary>
        /// Gets the refunded items.
        /// </summary>
        /// <param name="OrderId">The order unique identifier.</param>
        /// <returns></returns>
        public List<GetRefundedItems_Result> GetRefundedItems(int OrderId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objRefundedList = context.GetRefundedItems(OrderId).ToList();
                return _objRefundedList;
            }
        }
        /// <summary>
        /// Gets the backgound details.
        /// </summary>
        /// <returns></returns>
        public List<DG_BackGround> GetBackgoundDetails()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objBackgroundList = context.DG_BackGround.Where(t => t.DG_BackGround_Group_Id == t.DG_Background_pkey).ToList();
                return _objBackgroundList;
            }
        }
        /// <summary>
        /// Gets the backgound details all.
        /// </summary>
        /// <returns></returns>
        public List<DG_BackGround> GetBackgoundDetailsALL()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objBackgroundList = context.DG_BackGround.ToList();
                return _objBackgroundList;
            }
        }
        /// <summary>
        /// Gets the product typefor backgorund.
        /// </summary>
        /// <param name="productid">The productid.</param>
        /// <returns></returns>
        public int GetProductTypeforBackgorund(string productid)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objBackgroundList = context.DG_BackGround.Where(t => t.DG_BackGround_Image_Name == productid).FirstOrDefault();
                return _objBackgroundList.DG_Product_Id;
            }
        }
        /// <summary>
        /// Gets the backgound details.
        /// </summary>
        /// <param name="productid">The productid.</param>
        /// <returns></returns>
        public List<DG_BackGround> GetBackgoundDetails(int productid)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objBackgroundList = context.DG_BackGround.Where(t => t.DG_Product_Id == productid).ToList();
                if (_objBackgroundList.Count != 0)
                {
                    return _objBackgroundList;
                }
                else return null;
            }
        }
        /// <summary>
        /// Gets all photos.
        /// </summary>
        /// <param name="substoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public List<DG_Photos> GetAllPhotos(string substoreId)
        {
            List<string> _lststr = new List<string>();
            _lststr = substoreId.Split(',').ToList();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objAllPhotos = context.DG_Photos.Where(t => (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                return _objAllPhotos.Where(t => t.DG_SubStoreId == Convert.ToInt32(_lststr[0])).ToList();
            }
        }

        public List<DG_Photos> GetAllPhotosByPage(string substoreId, int startIndex, int pazeSize, bool isNext,out long maxPhotoId, out bool isMoreImage)
        {
            List<string> _lststr = new List<string>();
            if (!String.IsNullOrEmpty(substoreId))
                _lststr = substoreId.Split(',').ToList();
            else
                _lststr = null;

            int? ssInfo = null;
            ssInfo = _lststr == null ? ssInfo : Convert.ToInt32(_lststr[0]);
            //if(ssInfo != null)
            //{
            //    ssInfo = convert
            //}
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                System.Data.Objects.ObjectParameter keyMore = new System.Data.Objects.ObjectParameter("IsMoreImage", typeof(bool));
                System.Data.Objects.ObjectParameter key = new System.Data.Objects.ObjectParameter("LastKeyIndex", typeof(long));
                //var _objAllPhotos = context.GetPhotoDataByPage(startIndex, pazeSize, Convert.ToInt32(_lststr[0]), isNext, key).ToList();
                var _objAllPhotos = context.GetPhotoDataByPage(startIndex, pazeSize, ssInfo, isNext,keyMore, key).ToList();
                maxPhotoId = Convert.ToInt64(key.Value);
                isMoreImage = Convert.ToBoolean(keyMore.Value);
                return _objAllPhotos.ToList();
            }
        }
        public List<vw_GetOrderDetailsforRefund> GetOrderDetailsforReceiptReprint(string OrderNo)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetOrderDetailsforRefund.Where(t => t.DG_Orders_Number == OrderNo);
                    if (item != null)
                        return item.ToList();
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets all photos.
        /// </summary>
        /// <param name="substoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public List<DG_Photos> GetAllPhotosforSearch(string substoreId, long imgRfid, int NoOfImg, bool isAnyRfidSearch)
        {
            List<string> _lststr = new List<string>();
            _lststr = substoreId.Split(',').ToList();

            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                if (!isAnyRfidSearch)
                {
                    return context.GetAllPhotosforSearch(substoreId, imgRfid, NoOfImg).ToList();
                    //int minVal = imgRfid - NoOfImg, 
                    //    MaxVal = imgRfid + NoOfImg;
                    //return (from t in context.DG_Photos where (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)
                    //    && _lststr.Contains(SqlFunctions.StringConvert((double)t.DG_SubStoreId)) && t.DG_IsCodeType != true
                    //    && Int32.Parse(t.DG_Photos_RFID) >= minVal && Int32.Parse(t.DG_Photos_RFID) <= MaxVal
                    //       select t).ToList();
                }
                else
                {
                    var _objAllPhotos = context.DG_Photos.Where(t => (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null)).ToList();
                    return _objAllPhotos.Where(t => _lststr.Contains(t.DG_SubStoreId.ToString()) && t.DG_IsCodeType != true).ToList();
                }

            }
        }

        public List<DG_Photos> GetPhotosBasedonRFID(int substoreId, string RFID)
        {
            List<string> _lstRFID = new List<string>();
            _lstRFID = RFID.Split(',').ToList();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objAllPhotos = context.DG_Photos.Where(t => (t.DG_Photos_Archive == false || t.DG_Photos_Archive == null) && t.DG_SubStoreId == substoreId && _lstRFID.Contains(t.DG_Photos_RFID)).ToList();
                return _objAllPhotos;
            }
        }
        /// <summary>
        /// Gets the moderate photos.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public bool GetModeratePhotos(Int64 PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objModPhoto = context.DG_Moderate_Photos.Where(t => t.DG_Mod_Photo_ID == PhotoId).FirstOrDefault();
                if (_objModPhoto != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<DG_Moderate_Photos> GetModeratePhotos()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                return context.DG_Moderate_Photos.ToList();
            }
        }
        /// <summary>
        /// Checks the is croped photos.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public bool CheckIsCropedPhotos(Int64 PhotoId, string operation)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objModPhoto = context.DG_Photos.Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                if (_objModPhoto != null)
                {
                    if (operation == "RedEye")
                    {
                        if (_objModPhoto.DG_Photos_IsRedEye != null)
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else if (operation == "Crop")
                    {
                        if (_objModPhoto.DG_Photos_IsCroped != null)
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else if (operation == "GreenImage")
                    {
                        if (_objModPhoto.DG_Photos_IsGreen != null)
                        {
                            return (bool)_objModPhoto.DG_Photos_IsGreen;
                        }
                        else
                            return false;
                    }

                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Checks the is visible product for back ground.
        /// </summary>
        /// <param name="BGID">The bgid.</param>
        /// <param name="ProductID">The product unique identifier.</param>
        /// <returns></returns>
        public bool CheckIsVisibleProductForBackGround(int BGID, int ProductID)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objcheckBackground = context.DG_BackGround.Where(t => t.DG_BackGround_Group_Id == BGID && t.DG_Product_Id == ProductID).FirstOrDefault();
                if (_objcheckBackground == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

        }
        /// <summary>
        /// Saves the is croped photos.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="value">The value.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public bool SaveIsCropedPhotos(Int64 PhotoId, object value, string operation)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();

                    if (value != null)
                    {
                        if (operation == "RedEye")
                        {
                            _objnew.DG_Photos_IsRedEye = bool.Parse(value.ToString());
                        }
                        else
                        {
                            _objnew.DG_Photos_IsCroped = bool.Parse(value.ToString());
                        }
                    }

                    else
                    {
                        if (operation == "Restore")
                        {
                            _objnew.DG_Photos_IsRedEye = null;
                            _objnew.DG_Photos_IsCroped = null;
                        }
                        else if (operation == "RedEye")
                        {
                            _objnew.DG_Photos_IsRedEye = null;
                        }
                        else
                        {
                            _objnew.DG_Photos_IsCroped = null;
                        }
                    }

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }

        public bool SaveCroppedPhotoInfo(Int64 PhotoId, object value, string effects)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                    if (value != null)
                    {
                        _objnew.DG_Photos_IsCroped = bool.Parse(value.ToString());
                    }
                    _objnew.DG_Photos_Effects = effects;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }

        /// <summary>
        /// Saves the is green photos.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        /// <returns></returns>
        public bool SaveIsGreenPhotos(Int64 PhotoId, bool value)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();

                    _objnew.DG_Photos_IsGreen = value;

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        /// <summary>
        /// Gets the photo name by photo unique identifier.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public string GetPhotoNameByPhotoID(Int64 PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_FileName;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the photo rfid by photo unique identifier.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public string GetPhotoRFIDByPhotoID(Int64 PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_RFID;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<DG_Photos> GetPhotoRFIDByPhotoID(string[] PhotoIdList)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => PhotoIdList.Contains(t.DG_Photos_pkey.ToString())).ToList();
                if (_objPhotos != null)
                {
                    return _objPhotos;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the refund text.
        /// </summary>
        /// <returns></returns>
        public string GetRefundText()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Bill_Format.ToList().FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Refund_Slogan;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the border name from unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public string GetBorderNameFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Borders.ToList().Where(t => t.DG_Borders_pkey == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Border + "#" + _objPhotos.DG_IsActive + "#" + _objPhotos.DG_ProductTypeID;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the discount details from unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public string GetDiscountDetailsFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objdiscount = context.DG_Orders_DiscountType.ToList().Where(t => t.DG_Orders_DiscountType_Pkey == id).FirstOrDefault();
                if (_objdiscount != null)
                {
                    return _objdiscount.DG_Orders_DiscountType_Name + "#" + _objdiscount.DG_Orders_DiscountType_Desc + "#" + _objdiscount.DG_Orders_DiscountType_Active + "#" + _objdiscount.DG_Orders_DiscountType_Secure + "#" + _objdiscount.DG_Orders_DiscountType_ItemLevel + "#" + _objdiscount.DG_Orders_DiscountType_AsPercentage + "#" + _objdiscount.DG_Orders_DiscountType_Code;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the printer name from unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public string GetPrinterNameFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_AssociatedPrinters.ToList().Where(t => t.DG_AssociatedPrinters_Pkey == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_AssociatedPrinters_Name + "#" + _objPhotos.DG_AssociatedPrinters_IsActive + "#" + _objPhotos.DG_AssociatedPrinters_ProductType_ID + "#" + _objPhotos.DG_AssociatedPrinters_PaperSize;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the type of the associted printer unique identifier by product.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public int GetAssocitedPrinterIDByProductType(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_AssociatedPrinters.ToList().Where(t => t.DG_AssociatedPrinters_ProductType_ID == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_AssociatedPrinters_Pkey;
                }
                else
                {
                    return -1;
                }
            }
        }
        /// <summary>
        /// Gets the product name from unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public string GetProductNameFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Orders_ProductType.ToList().Where(t => t.DG_Orders_ProductType_pkey == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Orders_ProductType_Name;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the currency detail from unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public string GetCurrencyDetailFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Currency.ToList().Where(t => t.DG_Currency_pkey == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Currency_Name + "#" + _objPhotos.DG_Currency_Rate + "#" + _objPhotos.DG_Currency_Symbol + "#" + _objPhotos.DG_Currency_Code;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the name of the border file.
        /// </summary>
        /// <param name="BorderId">The border unique identifier.</param>
        /// <returns></returns>
        public string GetBorderFileName(int BorderId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objborder = context.DG_Borders.ToList().Where(t => t.DG_Borders_pkey == BorderId).FirstOrDefault();
                    if (_objborder != null)
                    {
                        return _objborder.DG_Border;
                    }
                    else
                    {
                        return null;
                    }


                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the name of the back ground file.
        /// </summary>
        /// <param name="productId">The product unique identifier.</param>
        /// <param name="BgGroupId">The debug group unique identifier.</param>
        /// <returns></returns>
        public string GetBackGroundFileName(int productId, int BgGroupId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objbackGround = context.DG_BackGround.ToList().Where(t => t.DG_Product_Id == productId && t.DG_BackGround_Group_Id == BgGroupId).FirstOrDefault();
                    if (_objbackGround != null)
                    {
                        return _objbackGround.DG_BackGround_Image_Name;
                    }
                    else
                    {
                        return null;
                    }


                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }
        /// <summary>
        /// Gets the back ground productwise.
        /// </summary>
        /// <param name="productId">The product unique identifier.</param>
        /// <returns></returns>
        public List<DG_BackGround> GetBackGroundProductwise(int productId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objbackGround = context.DG_BackGround.Where(t => t.DG_Product_Id == productId).ToList();
                    return _objbackGround;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the border productwise.
        /// </summary>
        /// <param name="productId">The product unique identifier.</param>
        /// <returns></returns>
        public List<DG_Borders> GetBorderProductwise(int productId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objBorder = context.DG_Borders.Where(t => t.DG_ProductTypeID == productId).ToList();
                    return _objBorder;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Deletes the currency.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public bool DeleteCurrency(int id)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objcurrency = context.DG_Currency.ToList().Where(t => t.DG_Currency_pkey == id).FirstOrDefault();
                    if (_objcurrency != null)
                    {
                        _objcurrency.DG_Currency_IsActive = false;
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the graphics.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public bool DeleteGraphics(int id)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objgraphics = context.DG_Graphics.ToList().Where(t => t.DG_Graphics_pkey == id).FirstOrDefault();
                    if (_objgraphics != null)
                    {
                        context.DG_Graphics.DeleteObject(_objgraphics);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the manual download.
        /// </summary>
        /// <param name="imgname">The imgname.</param>
        /// <returns></returns>
        public bool DeleteManualDownload(string imgname)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objphotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_FileName == imgname).FirstOrDefault();
                    if (_objphotos != null)
                    {
                        context.DG_Photos.DeleteObject(_objphotos);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the discount.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public bool DeleteDiscount(int id)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objdiscount = context.DG_Orders_DiscountType.ToList().Where(t => t.DG_Orders_DiscountType_Pkey == id).FirstOrDefault();
                    if (_objdiscount != null)
                    {
                        context.DG_Orders_DiscountType.DeleteObject(_objdiscount);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the printer.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public bool DeletePrinter(int id)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objprinters = context.DG_AssociatedPrinters.ToList().Where(t => t.DG_AssociatedPrinters_Pkey == id).FirstOrDefault();
                    if (_objprinters != null)
                    {
                        context.DG_AssociatedPrinters.DeleteObject(_objprinters);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the border.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns></returns>
        public bool DeleteBorder(int id)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objborder = context.DG_Borders.ToList().Where(t => t.DG_Borders_pkey == id).FirstOrDefault();
                    if (_objborder != null)
                    {
                        context.DG_Borders.DeleteObject(_objborder);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the back ground.
        /// </summary>
        /// <param name="productId">The product unique identifier.</param>
        /// <param name="BgGroupId">The debug group unique identifier.</param>
        /// <returns></returns>
        public bool DeleteBackGround(int productId, int BgGroupId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objbackGround = context.DG_BackGround.ToList().Where(t => t.DG_Product_Id == productId && t.DG_BackGround_Group_Id == BgGroupId).FirstOrDefault();
                    if (_objbackGround != null)
                    {
                        context.DG_BackGround.DeleteObject(_objbackGround);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the photo unique identifier by rffid.
        /// </summary>
        /// <param name="RFFId">The RFF unique identifier.</param>
        /// <returns></returns>
        public string GetRFFIDByPhotoID(int PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_RFID;

                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public Int32 GetPhotoIDByRFFID(string RFid)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_RFID == RFid).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_pkey;

                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the rffi dby photo unique identifier.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public string GetRFFIDbyPhotoId(Int64 PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_RFID;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Immoderates the specified photo unique identifier.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        public void immoderate(Int64 PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Moderate_Photos.ToList().Where(t => t.DG_Mod_Photo_ID == PhotoId).FirstOrDefault();
                if (_objPhotos != null)
                {
                    context.DG_Moderate_Photos.DeleteObject(_objPhotos);
                    context.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Sets the moderate image.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="userid">The userid.</param>
        /// <returns></returns>
        public bool SetModerateImage(int PhotoId, int userid)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Moderate_Photos _objnew = new DG_Moderate_Photos();
                    _objnew.DG_Mod_Date = ServerDateTime();
                    _objnew.DG_Mod_Photo_ID = PhotoId;
                    _objnew.DG_Mod_User_ID = userid;
                    context.DG_Moderate_Photos.AddObject(_objnew);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the photo effects.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetPhotoEffects> GetPhotoEffects(int PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.vw_GetPhotoEffects.ToList().Where(t => t.DG_Photos_ID == PhotoId).ToList();
                if (_objPhotos != null)
                    return _objPhotos;
                else
                    return null;
            }
        }
        /// <summary>
        /// Sets the effectson photo.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        /// <returns></returns>
        public bool SetEffectsonPhoto(string value, int photoId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objphoto = context.DG_Photos.Where(t => t.DG_Photos_pkey == photoId).FirstOrDefault();
                    _objphoto.DG_Photos_Effects = value;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Updates the layering.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool UpdateLayering(int PhotoId, string value)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();

                    _objnew.DG_Photos_Layering = value;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateLayeringForSpecPrint(int PhotoId, string value)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Photos _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();

                    _objnew.DG_Photos_Layering = value;
                    _objnew.IsImageProcessed = false;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the photo detailsby photo unique identifier.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <returns></returns>
        public DG_Photos GetPhotoDetailsbyPhotoId(int PhotoId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                    return _objPhotos;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the name of the store.
        /// </summary>
        /// <returns></returns>
        public List<DG_Store> GetStoreName()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objStores = context.DG_Store.ToList();
                    return _objStores;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the role names.
        /// </summary>
        /// <returns></returns>
        public List<DG_User_Roles> GetRoleNames()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objRoles = context.DG_User_Roles.ToList();
                    return _objRoles;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the type of the product.
        /// </summary>
        /// <returns></returns>
        public List<DG_Orders_ProductType> GetProductType()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objProductType = context.DG_Orders_ProductType.Where(t => t.DG_IsPackage == false).ToList();
                return _objProductType;
            }
        }

        public List<DigiPhoto.DataLayer.Model.ValueType> GetReasonType(int valueTypeGroupId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var q = context.ValueType.Where(t => t.IsActive == true && t.ValueTypeGroupId == valueTypeGroupId);
                var _objreasonType = q.ToList();
                return _objreasonType;
            }
        }
        /// <summary>
        /// Gets the archive images.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetRecordsForArchive> GetArchiveImages()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objArchive = context.vw_GetRecordsForArchive.ToList();
                return _objArchive;
            }
        }
        /// <summary>
        /// Gets the product typefor order.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetProductNameforOrder> GetProductTypeforOrder()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objProductType1 = context.vw_GetProductNameforOrder.Where(t => t.DG_IsActive == true && t.DG_IsPackage == false && t.DG_IsAccessory != true).ToList();
                var _objProductType2 = context.vw_GetProductNameforOrder.Where(t => t.DG_IsActive == true && t.DG_IsPackage == true && t.Itemcount > 0).ToList();
                var _objProductType3 = context.vw_GetProductNameforOrder.Where(t => t.DG_IsActive == true && t.DG_IsAccessory == true && t.DG_IsPackage == false).ToList();
                _objProductType1 = _objProductType1.Union(_objProductType2).Concat(_objProductType3).ToList();
                //_objProductType1=_objProductType1.Union(_objProductType2).Concat(_objProductType3).ToList();
                return _objProductType1;
            }
        }
        /// <summary>
        /// Gets the photo grapher.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetPhotographer> GetPhotoGrapher()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objProductType = context.vw_GetPhotographer.ToList();
                return _objProductType;
            }
        }
        /// <summary>
        /// Gets the type of the child product.
        /// </summary>
        /// <param name="parentid">The parentid.</param>
        /// <returns></returns>
        public string GetChildProductType(int parentid)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var result = (from item in context.DG_PackageDetails
                              where item.DG_PackageId == parentid && item.DG_Product_Quantity > 0
                              select item.DG_ProductTypeId).ToList();
                var _objProductType = string.Join(",", result);


                return _objProductType;
            }
        }
        /// <summary>
        /// Gets the child product type quantity.
        /// </summary>
        /// <param name="Child">The child.</param>
        /// <param name="parentId">The parent unique identifier.</param>
        /// <returns></returns>
        public string GetChildProductTypeQuantity(int Child, int parentId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_PackageDetails.Where(t => t.DG_PackageId == parentId && t.DG_ProductTypeId == Child).FirstOrDefault();
                    if (item != null)
                    {
                        return item.DG_Product_Quantity.ToString();
                    }
                    else
                    {
                        return "0";
                    }
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        /// <summary>
        /// Gets the next previous photo.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="Flag">The flag.</param>
        /// <returns></returns>
        public DG_Photos GetNextPreviousPhoto(int PhotoId, string Flag)
        {
            try
            {
                if (Flag == "P")
                {
                    using (EntityConnection connection = new EntityConnection(connectionstring))
                    {
                        DigiphotoEntities context = new DigiphotoEntities(connection);
                        DG_Photos _objPhoto = context.DG_Photos.Where(t => t.DG_Photos_pkey < PhotoId).OrderByDescending(t => t.DG_Photos_pkey).FirstOrDefault();
                        if (_objPhoto != null)
                        {
                            return _objPhoto;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    using (EntityConnection connection = new EntityConnection(connectionstring))
                    {
                        DigiphotoEntities context = new DigiphotoEntities(connection);
                        DG_Photos _objPhoto = context.DG_Photos.Where(t => t.DG_Photos_pkey > PhotoId).FirstOrDefault();
                        if (_objPhoto != null)
                        {
                            return _objPhoto;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the product pricing store wise.
        /// </summary>
        /// <param name="StoreId">The store unique identifier.</param>
        /// <returns></returns>
        public List<DG_Product_Pricing> GetProductPricingStoreWise(int StoreId)
        {

            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objStores = context.DG_Product_Pricing.Where(t => t.DG_Product_Pricing_StoreId == StoreId).ToList();
                    return _objStores;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the currency list.
        /// </summary>
        /// <returns></returns>
        public List<DG_Currency> GetCurrencyList()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    List<DG_Currency> _templist = new List<DG_Currency>();
                    DG_Currency _obj = new DG_Currency();
                    _obj.DG_Currency_Symbol = "--Select--";
                    _obj.DG_Currency_pkey = 0;
                    _templist.Add(_obj);
                    var _objCurrency = context.DG_Currency.ToList();
                    foreach (var item in _objCurrency)
                    {
                        _templist.Add(item);
                    }
                    return _templist;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the currency listforconfig.
        /// </summary>
        /// <returns></returns>
        public List<DG_Currency> GetCurrencyListforconfig()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var itemlist = context.DG_Currency.Where(t => t.DG_Currency_IsActive == true).ToList();
                    return itemlist;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the associated printers.
        /// </summary>
        /// <param name="ProductType">Type of the product.</param>
        /// <returns></returns>
        public List<vw_ProductPrinters> GetAssociatedPrinters(int? ProductType)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPrinters = context.vw_ProductPrinters.Where(t => t.DG_AssociatedPrinters_ProductType_ID == ProductType).ToList();
                return _objPrinters;
            }
        }
        /// <summary>
        /// Gets the default currency.
        /// </summary>
        /// <returns></returns>
        public int GetDefaultCurrency()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objCurrency = context.DG_Currency.Where(t => t.DG_Currency_Default == true).FirstOrDefault();
                    if (_objCurrency != null)
                    {
                        return _objCurrency.DG_Currency_pkey;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Sets the product pricing data.
        /// </summary>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="ProductPrice">The product price.</param>
        /// <param name="CurrencyId">The currency unique identifier.</param>
        /// <param name="CreatedBy">The created by.</param>
        /// <param name="storeId">The store unique identifier.</param>
        /// <param name="Isavailable">The isavailable.</param>
        public void SetProductPricingData(int? ProductTypeId, double? ProductPrice, int? CurrencyId, int? CreatedBy, int? storeId, bool? Isavailable)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Product_Pricing _objProductPricing = context.DG_Product_Pricing.Where(t => t.DG_Product_Pricing_ProductType == ProductTypeId && t.DG_Product_Pricing_StoreId == storeId).FirstOrDefault();
                    if (_objProductPricing != null)
                    {
                        _objProductPricing.DG_Product_Pricing_ProductPrice = Convert.ToDouble(decimal.Round(Convert.ToDecimal(ProductPrice), 3, MidpointRounding.ToEven));
                        _objProductPricing.DG_Product_Pricing_StoreId = storeId; _objProductPricing.DG_Product_Pricing_ProductType = ProductTypeId;
                        _objProductPricing.DG_Product_Pricing_UpdateDate = ServerDateTime();
                        _objProductPricing.DG_Product_Pricing_Currency_ID = CurrencyId;
                        _objProductPricing.DG_Product_Pricing_CreatedBy = CreatedBy;
                        _objProductPricing.DG_Product_Pricing_IsAvaliable = Isavailable;
                    }
                    else
                    {
                        DG_Product_Pricing _objProductPricingnew = new DG_Product_Pricing();
                        _objProductPricingnew.DG_Product_Pricing_ProductPrice = Convert.ToDouble(decimal.Round(Convert.ToDecimal(ProductPrice), 3, MidpointRounding.ToEven));
                        _objProductPricingnew.DG_Product_Pricing_ProductType = ProductTypeId;
                        _objProductPricingnew.DG_Product_Pricing_StoreId = storeId;
                        _objProductPricingnew.DG_Product_Pricing_UpdateDate = ServerDateTime();
                        _objProductPricingnew.DG_Product_Pricing_Currency_ID = CurrencyId;
                        _objProductPricingnew.DG_Product_Pricing_CreatedBy = CreatedBy;
                        _objProductPricingnew.DG_Product_Pricing_IsAvaliable = Isavailable;
                        context.AddToDG_Product_Pricing(_objProductPricingnew);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Function to save backup configuration data in the DG_Configuration table
        /// </summary>
        /// <param name="substoreId"></param>
        /// <param name="dbBackupPath">Path to store the database backup</param>
        /// <param name="cleanupTables">Comma separated table names to be emptied</param>
        /// <param name="hfBackupPath">Path to store the zip backup for hot folder - images</param>
        /// <param name="isScheduled">Is backup scheduled or not</param>
        /// <param name="backupDateTime">Backup scheduled date time</param>
        /// <returns></returns>
        public bool SetBackupConfigurationData(int substoreId, string dbBackupPath, string cleanupTables, string hfBackupPath, bool isScheduled, string backupDateTime, bool isRecursive, int IntervalCount, int IntervalType, int DaysForCleanupBackup)
        {

            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfigList = context.DG_Configuration.ToList();  //.Where(t => t.DG_Substore_Id == substoreId).FirstOrDefault();
                    if (_objConfigList != null)
                    {
                        foreach (DG_Configuration config in _objConfigList)
                        {
                            if (config != null)
                            {
                                config.DG_DbBackupPath = dbBackupPath;
                                config.DG_CleanupTables = cleanupTables;
                                config.DG_HfBackupPath = hfBackupPath;
                                config.DG_IsBackupScheduled = isScheduled;
                                config.DG_ScheduleBackup = backupDateTime;
                                //Recursive back up  Schedule setting
                                config.IsRecursive = isRecursive;
                                config.IntervalCount = IntervalCount;
                                config.intervalType = IntervalType;
                                // cleanBackup days
                                config.DG_CleanUpDaysBackUp = DaysForCleanupBackup;
                                //return true;
                            }
                        }
                        context.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }
        }


        //public bool SetBrightnessContrastData(double configBrightness, double configContrast, int substoreId, bool IsDeleteFromUSB, string mktImgPath, int mktImgTime, string FtpFolder, string FtpIP, string FtpPwd, string FtpUid)
        //{
        //    try
        //    {
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);
        //            var _objConfig = context.DG_Configuration.Where(t => t.DG_Substore_Id == substoreId).FirstOrDefault();
        //            if (_objConfig != null)
        //            {
        //                _objConfig.DG_Brightness = configBrightness;
        //                _objConfig.DG_Contrast = configContrast;
        //                _objConfig.IsDeleteFromUSB = IsDeleteFromUSB;
        //                _objConfig.DG_MktImgPath = mktImgPath;
        //                _objConfig.DG_MktImgTimeInSec = mktImgTime;
        //                _objConfig.FtpFolder = FtpFolder;
        //                _objConfig.FtpIP = FtpIP;
        //                _objConfig.FtpPwd = FtpPwd;
        //                _objConfig.FtpUid = FtpUid;
        //                context.SaveChanges();
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}




        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        /// <returns></returns>
        public vw_GetConfigdata GetConfigurationData(int substoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetConfigdata.Where(t => t.DG_Substore_Id == substoreId).FirstOrDefault();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<vw_GetPhotoList> GetPhoto()
        {
            List<vw_GetPhotoList> Listtable = new List<vw_GetPhotoList>();
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    Listtable = (from c in context.vw_GetPhotoList
                                 select c).ToList<vw_GetPhotoList>();

                    return Listtable;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DG_Configuration GetMktImgInfo()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Configuration.FirstOrDefault();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public vw_GetConfigdata GetDeafultPathData()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetConfigdata.FirstOrDefault();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<vw_GetAllTable> GetAllTable()
        {
            List<vw_GetAllTable> allTable = new List<vw_GetAllTable>();
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    allTable = (from c in context.vw_GetAllTable
                                select c).ToList<vw_GetAllTable>();

                    return allTable;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public int GetsubstoreIdbyLocationId(int LocationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_SubStore_Locations.Where(t => t.DG_Location_ID == LocationId).FirstOrDefault();
                    return _objConfig.DG_SubStore_ID;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the border details.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetBorderDetails> GetBorderDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetBorderDetails.ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the configuration compression.
        /// </summary>
        /// <returns></returns>
        public bool? GetConfigCompression(int SubstoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Configuration.Where(t => t.DG_Substore_Id == SubstoreId).FirstOrDefault();
                    if (_objConfig.DG_IsCompression != null)
                    {
                        return _objConfig.DG_IsCompression;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool? GetConfigEnableGroup(int SubstoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Configuration.Where(t => t.DG_Substore_Id == SubstoreID).FirstOrDefault();
                    if (_objConfig.DG_IsEnableGroup != null)
                    {
                        return _objConfig.DG_IsEnableGroup;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the graphics details.
        /// </summary>
        /// <returns></returns>
        public List<DG_Graphics> GetGraphicsDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Graphics.ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the border details.
        /// </summary>
        /// <param name="productname">The productname.</param>
        /// <returns></returns>
        public List<vw_GetBorderDetails> GetBorderDetails(string productname)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetBorderDetails.Where(t => t.DG_Orders_ProductType_Name == productname && t.DG_IsActive == true).ToList();
                    if (_objConfig.Count != 0)
                    {
                        return _objConfig;
                    }
                    else return null;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the printer details.
        /// </summary>
        /// <param name="substoreID">The substore unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetPrinterDetails> GetPrinterDetails(int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetPrinterDetails.Where(t => t.DG_AssociatedPrinters_SubStoreID == substoreID).ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the print log details.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetPrintDetails> GetPrintLogDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.vw_GetPrintDetails.ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the semi order image.
        /// </summary>
        /// <param name="ImageNumber">The image number.</param>
        /// <returns></returns>
        public DG_Orders_Details GetSemiOrderImage(string PhotoID)// ImageNumber)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                //string PhotoID = GetPhotoIDByRFFID(ImageNumber).ToString();
                if (PhotoID != null)
                {

                    var _objConfig = (from item in context.DG_Orders_Details
                                      where item.DG_Orders_ID == null && item.DG_Photos_ID == PhotoID
                                      select item).FirstOrDefault();
                    return _objConfig;
                }
                return null;
            }
        }




        public bool GetSemiOrderImageforValidation(string ImageNumber)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                string PhotoID = ImageNumber;
                if (PhotoID != null)
                {

                    var _objConfig = (from item in context.DG_Orders_Details
                                      where item.DG_Photos_ID == PhotoID && item.DG_Orders_ID != null
                                      select item).FirstOrDefault();
                    if (_objConfig == null)
                        return false;
                    else
                        return true;
                }
                return false;
            }
        }
        /// <summary>
        /// Sets the semi order image order details.
        /// </summary>
        /// <param name="OrderId">The order unique identifier.</param>
        /// <param name="ImageNumber">The image number.</param>
        /// <returns></returns>
        public bool setSemiOrderImageOrderDetails(int? OrderId, string ImageNumber, Int32? parentId, int substorId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    foreach (var pitem in ImageNumber.Split(','))
                    {
                        if (pitem != null)
                        {
                            var _objConfig = (from item in context.DG_Orders_Details
                                              where item.DG_Orders_ID == null && item.DG_Photos_ID == pitem
                                              select item).FirstOrDefault();
                            _objConfig.DG_Orders_ID = OrderId;
                            _objConfig.DG_Order_SubStoreId = substorId;
                            if (parentId != null)
                            {
                                _objConfig.DG_Orders_Details_LineItem_ParentID = parentId;
                            }

                            context.SaveChanges();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Determines whether [is semi order image] [the specified order details unique identifier].
        /// </summary>
        /// <param name="OrderDetailsID">The order details unique identifier.</param>
        /// <returns></returns>
        public bool IsSemiOrderImage(int OrderDetailsID)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var _objConfig = (from item in context.DG_Orders_Details
                                  where item.DG_Orders_LineItems_pkey == OrderDetailsID
                                  select item).FirstOrDefault();
                if (_objConfig.DG_Orders_ID == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            return false;


        }
        /// <summary>
        /// Gets the currency details.
        /// </summary>
        /// <returns></returns>
        public List<DG_Currency> GetCurrencyDetails()
        {

            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Currency.Where(t => t.DG_Currency_IsActive == true).ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the discount details.
        /// </summary>
        /// <returns></returns>
        public List<DG_Orders_DiscountType> GetDiscountDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Orders_DiscountType.ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the semi order setting.
        /// </summary>
        /// <returns></returns>
        public DG_SemiOrder_Settings GetSemiOrderSetting(int locationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_SemiOrder_Settings.Where(t => t.DG_LocationId == locationId).FirstOrDefault();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DG_SemiOrder_Settings GetSemiOrderSettingOld(int substoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_SemiOrder_Settings.Where(t => t.DG_SubStoreId == substoreId).FirstOrDefault();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DG_SemiOrder_Settings> GetLstSemiOrderSettings(int substoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    //change to load all location settings. to run manual download in all substore when spec printing is enabled 
                    //var _objConfig = context.DG_SemiOrder_Settings.Where(t => t.DG_SubStoreId == substoreId).ToList();
                    var _objConfig = context.DG_SemiOrder_Settings.ToList();
                    return _objConfig;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Sets the configuration data.
        /// </summary>
        /// <param name="hotfolder">The hotfolder.</param>
        /// <param name="framePath">The frame path.</param>
        /// <param name="bgPath">The debug path.</param>
        /// <param name="ModPassword">The mod password.</param>
        /// <param name="graphics">The graphics.</param>
        /// <param name="NumberofImages">The numberof images.</param>
        /// <param name="Iswatermark">The iswatermark.</param>
        /// <param name="IsHighResolution">The is high resolution.</param>
        /// <param name="isSemiorder">The is semiorder.</param>
        /// <param name="isAutoRotate">The is automatic rotate.</param>
        /// <param name="IsLineItemDiscount">The is line item discount.</param>
        /// <param name="IsTotalDiscount">The is total discount.</param>
        /// <param name="IsPosOnOff">The is position configuration off.</param>
        /// <param name="defaultCurrency">The default currency.</param>
        /// <param name="IsSemiOrderMain">The is semi order main.</param>
        /// <param name="ReceiptPrintername">The receipt printername.</param>
        /// <param name="substoreId">The substore unique identifier.</param>
        /// <param name="IsCompression">The is compression.</param>
        /// <returns></returns>

        public bool SetConfigurationData(string SyncCode, string hotfolder, string framePath, string bgPath, string ModPassword, string graphics, int NumberofImages, bool? Iswatermark, bool? IsHighResolution, bool? isSemiorder, bool? isAutoRotate, bool? IsLineItemDiscount, bool? IsTotalDiscount, bool? IsPosOnOff, int defaultCurrency, bool? IsSemiOrderMain, int substoreId, bool? IsCompression, bool? IsEnableGroup, int NoOfReceipt, string ChromaColor, decimal ChromaTolerance, int PageSizeGrid, int PageSizePreview, int NoOfPhotoIdSearch)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Configuration.Where(t => t.DG_Substore_Id == substoreId).FirstOrDefault();
                    if (_objConfig != null)
                    {
                        _objConfig.DG_BG_Path = bgPath;
                        _objConfig.DG_Frame_Path = framePath;
                        _objConfig.DG_Hot_Folder_Path = hotfolder;
                        _objConfig.DG_Mod_Password = ModPassword;
                        _objConfig.DG_Graphics_Path = graphics;
                        _objConfig.DG_NoOfPhotos = NumberofImages;
                        _objConfig.DG_Watermark = Iswatermark;
                        _objConfig.DG_HighResolution = IsHighResolution;
                        _objConfig.DG_SemiOrder = isSemiorder;
                        _objConfig.DG_AllowDiscount = IsLineItemDiscount;
                        _objConfig.DG_EnableDiscountOnTotal = IsTotalDiscount;
                        _objConfig.DG_SemiOrderMain = IsSemiOrderMain;
                        _objConfig.PosOnOff = IsPosOnOff;
                        _objConfig.DG_IsAutoRotate = isAutoRotate;
                        _objConfig.DG_IsCompression = IsCompression;
                        _objConfig.DG_IsEnableGroup = IsEnableGroup;
                        _objConfig.DG_NoOfBillReceipt = NoOfReceipt;
                        _objConfig.DG_ChromaColor = ChromaColor;
                        _objConfig.DG_ChromaTolerance = ChromaTolerance;
                        _objConfig.DG_PageCountGrid = PageSizeGrid;
                        _objConfig.DG_PageCountPreview = PageSizePreview;
                        _objConfig.DG_NoOfPhotoIdSearch = NoOfPhotoIdSearch;
                        _objConfig.IsSynced = false;
                        var receiptItem = context.DG_ReceiptPrinter.Where(t => t.DG_SubStore_Id == substoreId).FirstOrDefault();
                    }
                    else
                    {
                        DG_Configuration _objnew = new DG_Configuration();
                        _objnew.DG_BG_Path = bgPath;
                        _objnew.DG_Frame_Path = framePath;
                        _objnew.DG_Hot_Folder_Path = hotfolder;
                        _objnew.DG_Mod_Password = ModPassword;
                        _objnew.DG_NoOfPhotos = NumberofImages;
                        _objnew.DG_Graphics_Path = graphics;
                        _objnew.DG_Watermark = Iswatermark;
                        _objnew.DG_HighResolution = IsHighResolution;
                        _objnew.DG_SemiOrder = isSemiorder;
                        _objnew.DG_AllowDiscount = IsLineItemDiscount;
                        _objnew.DG_EnableDiscountOnTotal = IsTotalDiscount;
                        _objnew.PosOnOff = IsPosOnOff;
                        _objnew.DG_IsAutoRotate = isAutoRotate;
                        _objnew.DG_IsCompression = IsCompression;
                        _objnew.DG_SemiOrderMain = IsSemiOrderMain;
                        _objnew.DG_IsEnableGroup = IsEnableGroup;
                        _objnew.DG_Substore_Id = substoreId;
                        _objnew.DG_NoOfBillReceipt = NoOfReceipt;
                        _objnew.DG_ChromaColor = ChromaColor;
                        _objnew.DG_ChromaTolerance = ChromaTolerance;
                        _objnew.DG_PageCountGrid = PageSizeGrid;
                        _objnew.DG_PageCountPreview = PageSizePreview;
                        _objnew.DG_NoOfPhotoIdSearch = NoOfPhotoIdSearch;
                        _objnew.SyncCode = SyncCode;
                        _objnew.IsSynced = false;
                        context.AddToDG_Configuration(_objnew);
                    }
                    var _objcurrency = context.DG_Currency.Where(t => t.DG_Currency_pkey == defaultCurrency).FirstOrDefault();
                    if (_objcurrency != null)
                    {
                        _objcurrency.DG_Currency_Default = true;
                        foreach (var item in context.DG_Currency.Where(t => t.DG_Currency_pkey != defaultCurrency).ToList())
                        {
                            item.DG_Currency_Default = false;
                        }
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Adds the update user details.
        /// </summary>
        /// <param name="DG_User_pkey">The command g_ user_pkey.</param>
        /// <param name="DG_User_Name">Name of the command g_ user_.</param>
        /// <param name="DG_User_First_Name">Name of the command g_ user_ first_.</param>
        /// <param name="DG_User_Last_Name">Name of the command g_ user_ last_.</param>
        /// <param name="DG_User_Password">The command g_ user_ password.</param>
        /// <param name="DG_User_Roles_Id">The command g_ user_ roles_ unique identifier.</param>
        /// <param name="DG_Location_ID">The command g_ location_ unique identifier.</param>
        /// <param name="DG_User_Status">The command g_ user_ status.</param>
        /// <param name="DG_User_PhoneNo">The command g_ user_ phone no.</param>
        /// <param name="DG_User_Email">The command g_ user_ email.</param>
        /// <returns></returns>
        public bool AddUpdateUserDetails(int DG_User_pkey, string DG_User_Name, string DG_User_First_Name, string DG_User_Last_Name, string DG_User_Password, int DG_User_Roles_Id, int DG_Location_ID, bool? DG_User_Status, string DG_User_PhoneNo, string DG_User_Email, string SyncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (DG_User_pkey == 0)
                    {
                        DG_Users _objnew = new DG_Users();
                        _objnew.DG_User_Name = DG_User_Name;
                        _objnew.DG_User_First_Name = DG_User_First_Name;
                        _objnew.DG_User_Last_Name = DG_User_Last_Name;
                        _objnew.DG_User_Password = DG_User_Password;
                        _objnew.DG_User_Roles_Id = DG_User_Roles_Id;
                        _objnew.DG_Location_ID = DG_Location_ID;
                        _objnew.DG_User_Status = DG_User_Status;
                        _objnew.DG_User_PhoneNo = DG_User_PhoneNo;
                        _objnew.DG_User_Email = DG_User_Email;
                        _objnew.SyncCode = SyncCode;
                        _objnew.IsSynced = false;
                        context.DG_Users.AddObject(_objnew);
                    }
                    else
                    {
                        var _objConfig = context.DG_Users.ToList().Where(t => t.DG_User_pkey == DG_User_pkey).FirstOrDefault();
                        if (_objConfig != null)
                        {
                            _objConfig.DG_User_Name = DG_User_Name;
                            _objConfig.DG_User_First_Name = DG_User_First_Name;
                            _objConfig.DG_User_Last_Name = DG_User_Last_Name;
                            _objConfig.DG_User_Password = DG_User_Password;
                            _objConfig.DG_User_Roles_Id = DG_User_Roles_Id;
                            _objConfig.DG_Location_ID = DG_Location_ID;
                            _objConfig.DG_User_Status = DG_User_Status;
                            _objConfig.DG_User_PhoneNo = DG_User_PhoneNo;
                            _objConfig.DG_User_Email = DG_User_Email;
                            _objConfig.IsSynced = false;
                        }
                    }
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Adds the update role data.
        /// </summary>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <param name="RoleName">Name of the role.</param>
        /// <returns></returns>
        public string AddUpdateRoleData(int RoleId, string RoleName, string SyncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (RoleId == 0)
                    {
                        DG_User_Roles _objnew = new DG_User_Roles();
                        _objnew.DG_User_Role = RoleName;
                        _objnew.SyncCode = SyncCode;
                        _objnew.IsSynced = false;
                        context.DG_User_Roles.AddObject(_objnew);
                    }
                    else
                    {
                        var _objRoleData = context.DG_User_Roles.ToList().Where(t => t.DG_User_Roles_pkey == RoleId).FirstOrDefault();
                        if (_objRoleData != null)
                        {
                            _objRoleData.DG_User_Role = RoleName;
                            _objRoleData.IsSynced = false;
                        }

                    }
                    context.SaveChanges();
                    return "Saved";
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains("insert duplicate key"))
                {
                    return "Duplicate";
                }
                else
                {
                    return ex.Message;
                }
            }

        }
        /// <summary>
        /// Gets the epx folder path.
        /// </summary>
        /// <returns></returns>
        public string GetEPXFolderPath()
        {
            return "";
        }
        /// <summary>
        /// Deletes the role data.
        /// </summary>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <returns></returns>
        public bool DeleteRoleData(int RoleId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objRole = context.DG_User_Roles.ToList().Where(t => t.DG_User_Roles_pkey == RoleId).FirstOrDefault();
                    if (_objRole != null)
                    {
                        context.DG_User_Roles.DeleteObject(_objRole);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the name of the role.
        /// </summary>
        /// <param name="roleID">The role unique identifier.</param>
        /// <returns></returns>
        public string GetRoleName(int roleID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objRole = context.DG_User_Roles.ToList().Where(t => t.DG_User_Roles_pkey == roleID).FirstOrDefault();
                    if (_objRole != null)
                    {
                        return _objRole.DG_User_Role;
                    }
                    return "No Role";
                }

            }
            catch (Exception ex)
            {
                return "No Role";
            }

        }
        /// <summary>
        /// Gets the permission data.
        /// </summary>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <returns></returns>
        public List<DG_Permission_Role> GetPermissionData(int RoleId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objpermissiondata = context.DG_Permission_Role.Where(t => t.DG_User_Roles_Id == RoleId).ToList();
                    return _objpermissiondata;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the permission names.
        /// </summary>
        /// <returns></returns>
        public List<DG_Permissions> GetPermissionNames()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objpermisson = context.DG_Permissions.ToList();
                    return _objpermisson;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the permission names.
        /// </summary>
        /// <param name="PermissionId">The permission unique identifier.</param>
        /// <returns></returns>
        public string GetPermissionNames(int PermissionId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objpermisson = context.DG_Permissions.Where(t => t.DG_Permission_pkey == PermissionId).FirstOrDefault();
                    return _objpermisson.DG_Permission_Name;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the type of the discount.
        /// </summary>
        /// <returns></returns>
        public List<DG_Orders_DiscountType> GetDiscountType()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objDG_Orders_DiscountType = context.DG_Orders_DiscountType.ToList();
                    return _objDG_Orders_DiscountType;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Sets the permission data.
        /// </summary>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <param name="PermissonId">The permisson unique identifier.</param>
        /// <returns></returns>
        public bool SetPermissionData(int RoleId, int PermissonId)
        {
            bool retvalue = false;
            try
            {

                DG_Permission_Role _objnew = new DG_Permission_Role();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objpermissiondata = context.DG_Permission_Role.Where(t => t.DG_User_Roles_Id == RoleId && t.DG_Permission_Id == PermissonId).FirstOrDefault();
                    if (_objpermissiondata == null)
                    {
                        _objnew.DG_Permission_Id = PermissonId; _objnew.DG_User_Roles_Id = RoleId;
                        context.DG_Permission_Role.AddObject(_objnew);
                        context.SaveChanges();
                        retvalue = true;
                    }
                    return retvalue;

                }
            }
            catch (Exception ex)
            {
                return retvalue;
            }
        }
        /// <summary>
        /// Removes the permission data.
        /// </summary>
        /// <param name="RoleId">The role unique identifier.</param>
        /// <param name="PermissonId">The permisson unique identifier.</param>
        /// <returns></returns>
        public bool RemovePermissionData(int RoleId, int PermissonId)
        {
            try
            {
                bool retvalue = false;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Permission_Role _objpermissiondata = context.DG_Permission_Role.Where(t => t.DG_User_Roles_Id == RoleId && t.DG_Permission_Id == PermissonId).FirstOrDefault();
                    if (_objpermissiondata != null)
                    {
                        context.DG_Permission_Role.DeleteObject(_objpermissiondata);
                        retvalue = true;
                        context.SaveChanges();
                    }
                    return retvalue;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Sets the configurationdata.
        /// </summary>
        /// <param name="iscontrst">The iscontrst.</param>
        /// <param name="conrst">The conrst.</param>
        /// <param name="isbrgt">The isbrgt.</param>
        /// <param name="brgt">The BRGT.</param>
        /// <param name="isframe">The isframe.</param>
        /// <param name="framename">The framename.</param>
        /// <param name="vframename">The vframename.</param>
        /// <param name="pkey">The pkey.</param>
        /// <param name="productType">Type of the product.</param>
        /// <param name="isGreenScreen">The is green screen.</param>
        /// <param name="bg">The debug.</param>
        /// <param name="isbg">The isbg.</param>
        /// <param name="graphicsxml">The graphicsxml.</param>
        /// <param name="ZoomDetails">The zoom details.</param>
        /// <returns></returns>
        public bool SetConfigurationdata(bool? iscontrst, double conrst, bool? isbrgt, double brgt, bool? isframe, string framename, string vframename, int pkey, int? productType, bool? isGreenScreen, string bg, bool? isbg, string graphicsxml, string ZoomDetails, int? SubstoreId, bool? IsPrintActive, bool? IsCropActive, string HorizontalCoordinates, string VerticalCoordinates, int LocationId)
        {
            try
            {
                DG_SemiOrder_Settings _objnew = new DG_SemiOrder_Settings();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (pkey == 0)
                    {
                        _objnew.DG_SemiOrder_Settings_AutoBright = isbrgt;
                        _objnew.DG_SemiOrder_Settings_AutoContrast = iscontrst;
                        _objnew.DG_SemiOrder_Settings_AutoBright_Value = brgt;
                        _objnew.DG_SemiOrder_Settings_AutoContrast_Value = conrst;
                        _objnew.DG_SemiOrder_Settings_IsImageFrame = isframe;
                        _objnew.DG_SemiOrder_Settings_IsImageBG = isbg;
                        _objnew.DG_SemiOrder_Settings_ImageFrame = framename;
                        _objnew.DG_SemiOrder_Settings_ImageFrame_Vertical = vframename;
                        _objnew.DG_SemiOrder_ProductTypeId = productType;
                        _objnew.DG_SemiOrder_Environment = isGreenScreen;
                        _objnew.DG_SemiOrder_BG = bg;
                        _objnew.DG_SemiOrder_Graphics_layer = graphicsxml;
                        _objnew.DG_SemiOrder_Image_ZoomInfo = ZoomDetails;
                        _objnew.DG_SemiOrder_IsPrintActive = IsPrintActive;
                        _objnew.DG_SemiOrder_IsCropActive = IsCropActive;
                        _objnew.HorizontalCropValues = HorizontalCoordinates;
                        _objnew.VerticalCropValues = VerticalCoordinates;
                        _objnew.DG_SubStoreId = SubstoreId;
                        _objnew.DG_LocationId = LocationId;
                        context.DG_SemiOrder_Settings.AddObject(_objnew);
                    }
                    else
                    {
                        var item = context.DG_SemiOrder_Settings.Where(t => t.DG_SemiOrder_Settings_Pkey == pkey).FirstOrDefault();
                        if (item != null)
                        {
                            item.DG_SemiOrder_Settings_AutoBright = isbrgt;
                            item.DG_SemiOrder_Settings_AutoContrast = iscontrst;
                            item.DG_SemiOrder_Settings_AutoBright_Value = brgt;
                            item.DG_SemiOrder_Settings_AutoContrast_Value = conrst;
                            item.DG_SemiOrder_Settings_IsImageFrame = isframe;
                            item.DG_SemiOrder_Settings_IsImageBG = isbg;
                            item.DG_SemiOrder_Settings_ImageFrame = framename;
                            item.DG_SemiOrder_Settings_ImageFrame_Vertical = vframename;
                            item.DG_SemiOrder_Environment = isGreenScreen;
                            item.DG_SemiOrder_ProductTypeId = productType;
                            item.DG_SemiOrder_Graphics_layer = graphicsxml;
                            item.DG_SemiOrder_BG = bg;
                            item.DG_SemiOrder_Image_ZoomInfo = ZoomDetails;
                            item.DG_SemiOrder_IsPrintActive = IsPrintActive;
                            item.DG_SemiOrder_IsCropActive = IsCropActive;
                            item.HorizontalCropValues = HorizontalCoordinates;
                            item.VerticalCropValues = VerticalCoordinates;
                            item.DG_SubStoreId = SubstoreId;
                            item.DG_LocationId = LocationId;
                        }
                    }
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the datafor login.
        /// </summary>
        /// <returns></returns>
        public bool? GetDataforLogin()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objLogindata = context.tblPhotoDetails.FirstOrDefault();
                    if (_objLogindata != null)
                        return _objLogindata.UserName;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        /// <summary>
        /// Gets the color codes.
        /// </summary>
        /// <returns></returns>
        public List<DG_ColorCodes> GetColorCodes()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objLogindata = context.DG_ColorCodes.ToList();
                    if (_objLogindata.Count >= 0)
                        return _objLogindata;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the latest number for wifi.
        /// </summary>
        /// <param name="WifiName">Name of the wifi.</param>
        /// <returns></returns>
        public Int32 GetLatestNumberForWifi(string WifiName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_PhotoNumberConfiguration.Where(t => t.DG_WifiName == WifiName).FirstOrDefault();
                    return Convert.ToInt32(_objconfigdata.DG_StartingNumber);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

        }
        /// <summary>
        /// Sets the photo details.
        /// </summary>
        /// <param name="DG_Photos_RFID">The command g_ photos_ rfid.</param>
        /// <param name="DG_Photos_FileName">Name of the command g_ photos_ file.</param>
        /// <param name="DG_Photos_CreatedOn">The command g_ photos_ created configuration.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="imgmetadata">The imgmetadata.</param>
        /// <param name="locationid">The locationid.</param>
        /// <param name="photolayer">The photolayer.</param>
        /// <returns></returns>
        public Int32 SetPhotoDetails(string DG_Photos_RFID, string DG_Photos_FileName, DateTime DG_Photos_CreatedOn, string userid, string imgmetadata, int locationid, string photolayer, string DG_Photos_Effects, DateTime? DateTaken, int? RfidScanType, bool IsImageProcessed, int? subStoreId, long? videoLength, bool IsVideoProcessed)
        {
            DG_Photos _objnew = new DG_Photos();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                string ext = System.IO.Path.GetExtension(DG_Photos_FileName).ToLower();
                if (ext == ".jpg")
                {
                    _objnew.DG_MediaType = 1;
                }
                else
                {
                    _objnew.DG_MediaType = 2;
                }
                _objnew.DG_Photos_FileName = DG_Photos_FileName;
                _objnew.DG_Photos_CreatedOn = DG_Photos_CreatedOn;
                _objnew.DG_Photos_RFID = DG_Photos_RFID;
                _objnew.DG_Photos_UserID = Convert.ToInt32(userid);
                _objnew.DG_Photos_MetaData = imgmetadata;
                _objnew.DG_Photos_Layering = photolayer;
                _objnew.DG_Location_Id = locationid;
                _objnew.DG_SubStoreId = subStoreId;
                _objnew.DG_Photos_Effects = DG_Photos_Effects;
                _objnew.DG_VideoLength = videoLength;
                _objnew.DateTaken = DateTaken;
                _objnew.RfidScanType = RfidScanType;
                _objnew.IsImageProcessed = IsImageProcessed;
                _objnew.IsVideoProcessed = IsVideoProcessed;
                context.DG_Photos.AddObject(_objnew);
                context.SaveChanges();
                return _objnew.DG_Photos_pkey;
            }
        }

        /// <summary>
        /// Sets the photowise Barcode details.
        /// </summary>
        /// <param name="DG_Photos_RFID">The command g_ photos_ rfid.</param>
        /// <param name="DG_Photos_FileName">Name of the command g_ photos_ file.</param>
        /// <param name="DG_Photos_CreatedOn">The command g_ photos_ created configuration.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="imgmetadata">The imgmetadata.</param>
        /// <param name="locationid">The locationid.</param>
        /// <param name="photolayer">The photolayer.</param>
        /// <returns></returns>
        public long SetImageAssociationInfo(int PhotoId, string Format, string Code, bool IsAnonymousCodeActive)
        {
            try
            {
                iMixImageAssociation _objnew = new iMixImageAssociation();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {

                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (!IsAnonymousCodeActive || (IsAnonymousCodeActive && Code == "1111"))
                    {
                        string CardIdentityCode = Code.Substring(0, 4);
                        var obj = context.iMixImageCardType.ToList().Where(t => t.CardIdentificationDigit.ToUpper() == CardIdentityCode).FirstOrDefault();
                        if (obj != null)
                        {
                            int cardTypeId = obj.IMIXImageCardTypeId;
                            _objnew.PhotoId = PhotoId;
                            _objnew.IMIXCardTypeId = cardTypeId;
                            _objnew.CardUniqueIdentifier = Code;
                            _objnew.MappedIdentifier = Guid.NewGuid().ToString() + PhotoId.ToString();
                            context.iMixImageAssociation.AddObject(_objnew);
                            context.SaveChanges();
                            return _objnew.IMIXImageAssociationId;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else if (IsAnonymousCodeActive && Code != "1111")
                    {
                        _objnew.PhotoId = PhotoId;
                        _objnew.IMIXCardTypeId = 0;
                        _objnew.CardUniqueIdentifier = Code;
                        _objnew.MappedIdentifier = Guid.NewGuid().ToString() + PhotoId.ToString();
                        context.iMixImageAssociation.AddObject(_objnew);
                        context.SaveChanges();
                        return _objnew.IMIXImageAssociationId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch
            {
                return 0;
            }
        }

        public bool SetImageAssociationInfoForPostScan(List<int> PhotoIdList, string Format, string Code, bool IsAnonymousCodeActive)
        {
            try
            {
                iMixImageAssociation _objnew = null;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (!IsAnonymousCodeActive || (IsAnonymousCodeActive && Code == "1111"))
                    {
                        string CardIdentityCode = Code.Substring(0, 4);
                        int cardTypeId = context.iMixImageCardType.ToList().Where(t => t.CardIdentificationDigit.ToUpper() == CardIdentityCode).FirstOrDefault().IMIXImageCardTypeId;
                        foreach (int photoId in PhotoIdList)
                        {
                            _objnew = new iMixImageAssociation();
                            _objnew.PhotoId = photoId;
                            _objnew.IMIXCardTypeId = cardTypeId;
                            _objnew.CardUniqueIdentifier = Code;
                            _objnew.MappedIdentifier = Guid.NewGuid().ToString() + photoId.ToString();
                            context.iMixImageAssociation.AddObject(_objnew);
                        }
                        context.SaveChanges();
                        return true;
                    }
                    else if (IsAnonymousCodeActive && Code != "1111")
                    {
                        foreach (int photoId in PhotoIdList)
                        {
                            _objnew = new iMixImageAssociation();
                            _objnew.PhotoId = photoId;
                            _objnew.IMIXCardTypeId = 0;
                            _objnew.CardUniqueIdentifier = Code;
                            _objnew.MappedIdentifier = Guid.NewGuid().ToString() + photoId.ToString();
                            context.iMixImageAssociation.AddObject(_objnew);
                        }
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePhotoByPhotoId(int PhotoId)
        {
            try
            {
                DG_Photos _objnew = new DG_Photos();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    _objnew = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                    if (_objnew != null)
                    {
                        _objnew.DG_IsCodeType = true;
                    }
                    context.SaveChanges();
                    return true;
                }
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the photo grapher last image unique identifier.
        /// </summary>
        /// <param name="photographerid">The photographerid.</param>
        /// <returns></returns>
        public string GetPhotoGrapherLastImageId(int photographerid)
        {
            DG_Photos _objnew = new DG_Photos();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objphotos = context.GetLastGeneratedNumber(photographerid).FirstOrDefault();
                return _objphotos.NextNumber;
            }
        }
        /// <summary>
        /// Checks the photos.
        /// </summary>
        /// <param name="Photono">The photono.</param>
        /// <param name="PhotographerID">The photographer unique identifier.</param>
        /// <returns></returns>
        public bool CheckPhotos(string Photono, int PhotographerID)
        {
            DG_Photos _objnew = new DG_Photos();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objphotos = context.DG_Photos.Where(t => t.DG_Photos_FileName == Photono && t.DG_Photos_UserID == PhotographerID).FirstOrDefault();
                if (_objphotos != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        /// <summary>
        /// Sets the latest number for wifi.
        /// </summary>
        /// <param name="PhotoNo">The photo no.</param>
        /// <param name="WifiName">Name of the wifi.</param>
        public void SetLatestNumberForWifi(Int32 PhotoNo, string WifiName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_PhotoNumberConfiguration.Where(t => t.DG_WifiName == WifiName).FirstOrDefault();
                    _objconfigdata.DG_StartingNumber = PhotoNo + 1;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

        }
        /// <summary>
        /// Sets the number for image download.
        /// </summary>
        /// <param name="PhotoNo">The photo no.</param>
        /// <param name="WifiName">Name of the wifi.</param>
        public void SetNumberForImageDownload(Int32 PhotoNo, string WifiName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_PhotoNumberConfiguration.Where(t => t.DG_WifiName == WifiName).FirstOrDefault();
                    _objconfigdata.DG_StartingNumber = PhotoNo;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

        }
        /// <summary>
        /// Sets the preview counter.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        public void SetPreviewCounter(int PhotoId)
        {
            DG_Preview_Counter _objnew = new DG_Preview_Counter();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                _objnew.PhotoId = PhotoId;
                _objnew.PreviewDate = ServerDateTime();
                context.DG_Preview_Counter.AddObject(_objnew);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Gets the product unique identifier.
        /// </summary>
        /// <param name="ProductTypeName">Name of the product type.</param>
        /// <returns></returns>
        public int GetProductID(string ProductTypeName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_Orders_ProductType.Where(t => t.DG_Orders_ProductType_Name == ProductTypeName).FirstOrDefault();
                    if (_objconfigdata != null)
                        return _objconfigdata.DG_Orders_ProductType_pkey;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Sets the border details.
        /// </summary>
        /// <param name="Bordername">The bordername.</param>
        /// <param name="productType">Type of the product.</param>
        /// <param name="isactive">if set to <c>true</c> [isactive].</param>
        /// <param name="borderid">The borderid.</param>
        public void SetBorderDetails(string Bordername, int productType, bool isactive, int borderid, string SyncCode)
        {
            DG_Borders _objnew = new DG_Borders();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_Borders.Where(t => t.DG_Borders_pkey == borderid).FirstOrDefault();
                if (_objconfigdata == null)
                {
                    _objnew.DG_Border = Bordername;
                    _objnew.DG_ProductTypeID = productType;
                    _objnew.DG_IsActive = isactive;
                    _objnew.SyncCode = SyncCode;
                    _objnew.IsSynced = false;
                    context.DG_Borders.AddObject(_objnew);
                }
                else
                {
                    _objconfigdata.DG_Border = Bordername;
                    _objconfigdata.DG_ProductTypeID = productType;
                    _objconfigdata.DG_IsActive = isactive;
                    _objconfigdata.IsSynced = false;
                }
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Sets the printer details.
        /// </summary>
        /// <param name="PrinterName">Name of the printer.</param>
        /// <param name="productType">Type of the product.</param>
        /// <param name="isactive">if set to <c>true</c> [isactive].</param>
        /// <param name="Papersize">The papersize.</param>
        /// <param name="SubStoreId">The sub store unique identifier.</param>
        public void SetPrinterDetails(string PrinterName, int productType, bool isactive, string Papersize, int SubStoreId)
        {
            DG_AssociatedPrinters _objnew = new DG_AssociatedPrinters();

            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_AssociatedPrinters.Where(t => t.DG_AssociatedPrinters_Name == PrinterName && t.DG_AssociatedPrinters_ProductType_ID == productType && t.DG_AssociatedPrinters_PaperSize == Papersize && t.DG_AssociatedPrinters_SubStoreID == SubStoreId).FirstOrDefault();
                if (_objconfigdata == null)
                {

                    _objnew.DG_AssociatedPrinters_Name = PrinterName;
                    _objnew.DG_AssociatedPrinters_ProductType_ID = productType;
                    _objnew.DG_AssociatedPrinters_IsActive = isactive;
                    _objnew.DG_AssociatedPrinters_PaperSize = Papersize;
                    _objnew.DG_AssociatedPrinters_SubStoreID = SubStoreId;
                    context.DG_AssociatedPrinters.AddObject(_objnew);
                }
                else
                {
                    _objconfigdata.DG_AssociatedPrinters_Name = PrinterName;
                    _objconfigdata.DG_AssociatedPrinters_ProductType_ID = productType;
                    _objconfigdata.DG_AssociatedPrinters_IsActive = isactive;
                    _objconfigdata.DG_AssociatedPrinters_PaperSize = Papersize;
                    _objconfigdata.DG_AssociatedPrinters_SubStoreID = SubStoreId;
                }
                context.SaveChanges();

            }
        }
        /// <summary>
        /// Sets the currency details.
        /// </summary>
        /// <param name="CurrencyName">Name of the currency.</param>
        /// <param name="rate">The rate.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="id">The unique identifier.</param>
        /// <param name="modifiedby">The modifiedby.</param>
        /// <param name="IsDefault">if set to <c>true</c> [is default].</param>
        /// <param name="UpdateDate">The update date.</param>
        /// <param name="Icon">The icon.</param>
        /// <param name="Currencycode">The currencycode.</param>
        public void SetCurrencyDetails(string CurrencyName, float rate, string symbol, int id, int modifiedby, bool IsDefault, DateTime UpdateDate, string Icon, string Currencycode, string SyncCode)
        {
            DG_Currency _objnew = new DG_Currency();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_Currency.Where(t => t.DG_Currency_pkey == id).FirstOrDefault();
                if (_objconfigdata == null)
                {
                    _objnew.DG_Currency_Name = CurrencyName;
                    _objnew.DG_Currency_Rate = rate;
                    _objnew.DG_Currency_Symbol = symbol;
                    _objnew.DG_Currency_ModifiedBy = modifiedby;
                    _objnew.DG_Currency_Default = IsDefault;
                    _objnew.DG_Currency_UpdatedDate = UpdateDate;
                    _objnew.DG_Currency_Icon = Icon;
                    _objnew.DG_Currency_Code = Currencycode;
                    _objnew.DG_Currency_IsActive = true;
                    _objnew.SyncCode = SyncCode;
                    _objnew.IsSynced = false;
                    context.DG_Currency.AddObject(_objnew);
                }
                else
                {
                    _objconfigdata.DG_Currency_Name = CurrencyName;
                    _objconfigdata.DG_Currency_Rate = rate;
                    _objconfigdata.DG_Currency_Symbol = symbol;
                    _objconfigdata.DG_Currency_Code = Currencycode;
                    _objconfigdata.IsSynced = false;
                }
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Sets the discount details.
        /// </summary>
        /// <param name="DiscountName">Name of the discount.</param>
        /// <param name="DiscountDesc">The discount description.</param>
        /// <param name="isactive">if set to <c>true</c> [isactive].</param>
        /// <param name="issecure">if set to <c>true</c> [issecure].</param>
        /// <param name="isitemlevel">if set to <c>true</c> [isitemlevel].</param>
        /// <param name="isaspercentage">if set to <c>true</c> [isaspercentage].</param>
        /// <param name="Discountcode">The discountcode.</param>
        public void SetDiscountDetails(string DiscountName, string DiscountDesc, bool isactive, bool issecure, bool isitemlevel, bool isaspercentage, string Discountcode, string SyncCode)
        {
            DG_Orders_DiscountType _objnew = new DG_Orders_DiscountType();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_Orders_DiscountType.Where(t => t.DG_Orders_DiscountType_Name == DiscountName).FirstOrDefault();
                if (_objconfigdata == null)
                {
                    _objnew.DG_Orders_DiscountType_Name = DiscountName;
                    _objnew.DG_Orders_DiscountType_Desc = DiscountDesc;
                    _objnew.DG_Orders_DiscountType_Active = isactive;
                    _objnew.DG_Orders_DiscountType_Secure = issecure;
                    _objnew.DG_Orders_DiscountType_ItemLevel = isitemlevel;
                    _objnew.DG_Orders_DiscountType_AsPercentage = isaspercentage;
                    _objnew.DG_Orders_DiscountType_Code = Discountcode;
                    _objnew.SyncCode = SyncCode;
                    _objnew.IsSynced = false;
                    context.DG_Orders_DiscountType.AddObject(_objnew);
                }
                else
                {
                    _objconfigdata.DG_Orders_DiscountType_Name = DiscountName;
                    _objconfigdata.DG_Orders_DiscountType_Desc = DiscountDesc;
                    _objconfigdata.DG_Orders_DiscountType_Active = isactive;
                    _objconfigdata.DG_Orders_DiscountType_Secure = issecure;
                    _objconfigdata.DG_Orders_DiscountType_ItemLevel = isitemlevel;
                    _objconfigdata.DG_Orders_DiscountType_AsPercentage = isaspercentage;
                    _objconfigdata.DG_Orders_DiscountType_Code = Discountcode;
                    _objconfigdata.IsSynced = false;
                }
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Sets the graphics details.
        /// </summary>
        /// <param name="graphicsname">The graphicsname.</param>
        /// <param name="graphicsdisplayname">The graphicsdisplayname.</param>
        /// <param name="isactive">if set to <c>true</c> [isactive].</param>
        public void SetGraphicsDetails(string graphicsname, string graphicsdisplayname, bool isactive, string SyncCode)
        {
            DG_Graphics _objnew = new DG_Graphics();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                _objnew.DG_Graphics_Name = graphicsname;
                _objnew.DG_Graphics_Displayname = graphicsdisplayname;
                _objnew.DG_Graphics_IsActive = isactive;
                _objnew.SyncCode = SyncCode;
                _objnew.IsSynced = false;
                context.DG_Graphics.AddObject(_objnew);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Sets the back ground details.
        /// </summary>
        /// <param name="productid">The productid.</param>
        /// <param name="backgroundname">The backgroundname.</param>
        /// <param name="backgrounddisplayname">The backgrounddisplayname.</param>
        /// <param name="bgid">The bgid.</param>
        public void SetBackGroundDetails(int productid, string backgroundname, string backgrounddisplayname, int bgid, string SyncCode)
        {
            DG_BackGround _objnew = new DG_BackGround();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {

                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_BackGround.Where(t => t.DG_Background_pkey == bgid && t.DG_Product_Id == productid).FirstOrDefault();
                if (_objconfigdata != null)
                {

                    _objconfigdata.DG_BackGround_Image_Name = backgroundname;
                    _objconfigdata.DG_BackGround_Image_Display_Name = backgrounddisplayname;
                    _objconfigdata.DG_Product_Id = productid;
                    _objconfigdata.DG_BackGround_Group_Id = bgid;
                    _objconfigdata.IsSynced = false;
                }
                else
                {
                    _objnew.DG_BackGround_Image_Name = backgroundname;
                    _objnew.DG_BackGround_Image_Display_Name = backgrounddisplayname;
                    _objnew.DG_Product_Id = productid;
                    _objnew.DG_BackGround_Group_Id = bgid;
                    _objnew.SyncCode = SyncCode;
                    _objnew.IsSynced = false;
                    context.DG_BackGround.AddObject(_objnew);
                }
                context.SaveChanges();

            }
        }
        /// <summary>
        /// Sets the back ground details.
        /// </summary>
        /// <param name="productid">The productid.</param>
        /// <param name="backgroundname">The backgroundname.</param>
        /// <param name="backgrounddisplayname">The backgrounddisplayname.</param>
        /// <returns></returns>
        public int SetBackGroundDetails(int productid, string backgroundname, string backgrounddisplayname, string SyncCode)
        {
            DG_BackGround _objnew = new DG_BackGround();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {

                DigiphotoEntities context = new DigiphotoEntities(connection);
                _objnew.DG_BackGround_Image_Name = backgroundname;
                _objnew.DG_BackGround_Image_Display_Name = backgrounddisplayname;
                _objnew.DG_Product_Id = productid;
                _objnew.SyncCode = SyncCode;
                _objnew.IsSynced = false;
                context.DG_BackGround.AddObject(_objnew);
                context.SaveChanges();
                return _objnew.DG_Background_pkey;
            }
        }
        /// <summary>
        /// Sets the archive details.
        /// </summary>
        /// <param name="imgname">The imgname.</param>
        public void SetArchiveDetails(string imgname)
        {
            DG_Photos _objnew = new DG_Photos();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {

                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objconfigdata = context.DG_Photos.Where(t => t.DG_Photos_FileName == imgname).FirstOrDefault();
                if (_objconfigdata != null)
                {

                    _objconfigdata.DG_Photos_Archive = true;

                }
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Sets the printer log.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="UserId">The user unique identifier.</param>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        public void SetPrinterLog(int PhotoId, int UserId, int ProductTypeId)
        {
            DG_PrintLog _objnew = new DG_PrintLog();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                _objnew.PhotoId = PhotoId;
                _objnew.UserID = UserId;
                _objnew.ProductTypeId = ProductTypeId;
                _objnew.PrintTime = ServerDateTime();
                context.DG_PrintLog.AddObject(_objnew);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Gets the name of the printers.
        /// </summary>
        /// <returns></returns>
        public string GetPrintersName()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.tblprinters.FirstOrDefault();
                    return _objconfigdata.Printer1 + "##" + _objconfigdata.Printer2;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the product pricing.
        /// </summary>
        /// <param name="ProductTypeID">The product type unique identifier.</param>
        /// <returns></returns>
        public double GetProductPricing(int ProductTypeID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return (double)context.DG_Product_Pricing.ToList().Where(t => t.DG_Product_Pricing_ProductType == ProductTypeID).FirstOrDefault().DG_Product_Pricing_ProductPrice;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        /// <summary>
        /// Gets the product type list.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetProductTypeData> GetProductTypeList()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var productList = context.vw_GetProductTypeData.ToList();
                    return productList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DG_Orders_ProductType GetProductsynccode(int productid)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Orders_ProductType.ToList().Where(t => t.DG_Orders_ProductType_pkey == productid).FirstOrDefault();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Sets the product type information.
        /// </summary>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="ProductTypeName">Name of the product type.</param>
        /// <param name="ProductTypeDesc">The product type description.</param>
        /// <param name="IsDiscount">The is discount.</param>
        /// <param name="ProductPrice">The product price.</param>
        /// <param name="stroreId">The strore unique identifier.</param>
        /// <param name="UserId">The user unique identifier.</param>
        /// <param name="ispackage">The ispackage.</param>
        /// <param name="Isactive">The isactive.</param>
        /// <param name="IsAccessory">The is accessory.</param>
        /// <param name="Productcode">The productcode.</param>
        /// <returns></returns>
        //public bool SetProductTypeInfo(int ProductTypeId, string ProductTypeName, string ProductTypeDesc, bool? IsDiscount, string ProductPrice, int stroreId, int UserId, bool? ispackage, bool? Isactive, bool? IsAccessory, string Productcode, string SyncCode, string syncodeforPackage, int? IsInvisible)
        //{
        //    try
        //    {
        //        DG_Orders_ProductType _objnew = new DG_Orders_ProductType();
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);

        //            if (ProductTypeId == 0)
        //            {
        //                _objnew.DG_Orders_ProductType_Name = ProductTypeName;
        //                _objnew.DG_Orders_ProductType_Desc = ProductTypeDesc;
        //                _objnew.DG_Orders_ProductType_DiscountApplied = Convert.ToBoolean(IsDiscount);
        //                _objnew.DG_IsPackage = Convert.ToBoolean(ispackage);
        //                _objnew.DG_IsActive = Convert.ToBoolean(Isactive);
        //                _objnew.DG_Orders_ProductType_IsBundled = false;
        //                _objnew.DG_Orders_ProductType_Image = "/images/jpgfloppy.png";
        //                _objnew.DG_IsAccessory = Convert.ToBoolean(IsAccessory);
        //                _objnew.DG_Orders_ProductCode = Productcode;
        //                _objnew.SyncCode = SyncCode;
        //                _objnew.IsSynced = false;
        //                _objnew.DG_Orders_ProductNumber = IsInvisible;
        //                context.DG_Orders_ProductType.AddObject(_objnew);
        //                context.SaveChanges();
        //                ProductTypeId = _objnew.DG_Orders_ProductType_pkey;
        //                if (ispackage == false)
        //                {
        //                    foreach (var packitem in context.DG_Orders_ProductType.Where(t => t.DG_IsPackage == true))
        //                    {
        //                        var packagelist = context.DG_PackageDetails.Where(t => t.DG_PackageId == packitem.DG_Orders_ProductType_pkey).ToList();
        //                        if (packagelist.Count > 0)
        //                        {
        //                            SyncCode = "0";
        //                            SetPackageDetails(packitem.DG_Orders_ProductType_pkey, ProductTypeId, 0, 0);
        //                        }
        //                    }
        //                }

        //            }
        //            else
        //            {
        //                var item = context.DG_Orders_ProductType.ToList().Where(t => t.DG_Orders_ProductType_pkey == ProductTypeId).FirstOrDefault();
        //                item.DG_Orders_ProductType_Name = ProductTypeName;
        //                item.DG_Orders_ProductType_Desc = ProductTypeDesc;
        //                item.DG_Orders_ProductType_DiscountApplied = Convert.ToBoolean(IsDiscount);
        //                item.DG_IsPackage = Convert.ToBoolean(ispackage);
        //                item.DG_IsActive = Convert.ToBoolean(Isactive);
        //                item.DG_IsAccessory = Convert.ToBoolean(IsAccessory);
        //                item.DG_Orders_ProductCode = Productcode;
        //                item.IsSynced = false;
        //                if (item.DG_IsPackage)
        //                {
        //                    item.SyncCode = syncodeforPackage;

        //                }
        //                item.DG_Orders_ProductNumber = IsInvisible;
        //                context.SaveChanges();
        //            }
        //            var priceitem = context.DG_Product_Pricing.ToList().Where(t => t.DG_Product_Pricing_ProductType == ProductTypeId).FirstOrDefault();
        //            if (priceitem != null)
        //            {
        //                priceitem.DG_Product_Pricing_ProductPrice = Convert.ToDouble(ProductPrice);
        //                priceitem.DG_Product_Pricing_UpdateDate = ServerDateTime();
        //                context.SaveChanges();
        //            }
        //            else
        //            {
        //                DG_Product_Pricing _objnewprice = new DG_Product_Pricing();
        //                _objnewprice.DG_Product_Pricing_ProductType = ProductTypeId;
        //                _objnewprice.DG_Product_Pricing_ProductPrice = Convert.ToDouble(ProductPrice);
        //                _objnewprice.DG_Product_Pricing_StoreId = stroreId;
        //                _objnewprice.DG_Product_Pricing_UpdateDate = ServerDateTime();
        //                _objnewprice.DG_Product_Pricing_Currency_ID = 1;
        //                _objnewprice.DG_Product_Pricing_IsAvaliable = true;
        //                _objnewprice.DG_Product_Pricing_CreatedBy = UserId;
        //                context.DG_Product_Pricing.AddObject(_objnewprice);
        //                context.SaveChanges();
        //            }
        //        }


        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        /// <summary>
        /// Deletes the type of the product.
        /// </summary>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <returns></returns>
        public bool DeleteProductType(int ProductTypeId)
        {
            try
            {
                DG_Orders_ProductType _objnew = new DG_Orders_ProductType();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Orders_ProductType.ToList().Where(t => t.DG_Orders_ProductType_pkey == ProductTypeId && t.DG_IsPrimary != true).FirstOrDefault();
                    if (item != null)
                    {
                        context.DeleteObject(item);
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the package names.
        /// </summary>
        /// <returns></returns>
        public List<DG_Orders_ProductType> GetPackageNames()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var packagelist = context.DG_Orders_ProductType.Where(t => t.DG_IsPackage == true).ToList();
                    return packagelist;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the packag details.
        /// </summary>
        /// <param name="PackageId">The package unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetPackageDetails> GetPackagDetails(int PackageId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objPackageDetailsdata = context.vw_GetPackageDetails.Where(t => t.DG_PackageId == PackageId).ToList();
                    return _objPackageDetailsdata;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the product summary.
        /// </summary>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <returns></returns>
        public List<ProductSummary_Result> GetProductSummary(DateTime FromDate, DateTime ToDate, String StoreName, String UserName)
        {
            List<ProductSummary_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    result = context.ProductSummary(ToDate, FromDate, UserName, StoreName).ToList();

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the cash bill summary.
        /// </summary>
        /// <param name="itemdetails">The itemdetails.</param>
        /// <returns></returns>
        public List<GenerateBill_Result> GetCashBillSummary(string itemdetails)
        {
            List<GenerateBill_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    result = context.GenerateBill_Result(itemdetails).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the operator reports.
        /// </summary>
        /// <param name="CurrencyId">The currency unique identifier.</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="secfromDate">The secfrom date.</param>
        /// <param name="SecToDate">The sec automatic date.</param>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Comparision">if set to <c>true</c> [comparision].</param>
        /// <returns></returns>
        public List<OperatorPerformanceReport_Result> GetOperatorReports(int CurrencyId, DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision)
        {
            List<OperatorPerformanceReport_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    result = context.OperatorPerformanceReport(FromDate, ToDate, secfromDate, SecToDate, CurrencyId, StoreName, UserName, Comparision).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the photographer performance reports.
        /// </summary>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="secfromDate">The secfrom date.</param>
        /// <param name="SecToDate">The sec automatic date.</param>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Comparision">if set to <c>true</c> [comparision].</param>
        /// <returns></returns>
        public List<GetPhotgrapherPerformance_Result> GetPhotographerPerformanceReports(DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision)
        {
            List<GetPhotgrapherPerformance_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    result = context.GetPhotgrapherPerformance_Method(FromDate, ToDate, secfromDate, SecToDate, StoreName, UserName, Comparision).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the location performance reports.
        /// </summary>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="secfromDate">The secfrom date.</param>
        /// <param name="SecToDate">The sec automatic date.</param>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Comparision">if set to <c>true</c> [comparision].</param>
        /// <returns></returns>
        public List<GetLocationPerformance_Result> GetLocationPerformanceReports(DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision,string subStore)
        {
            List<GetLocationPerformance_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    result = context.GetLocationPerformance(FromDate, ToDate, secfromDate, SecToDate, StoreName, UserName, Comparision).ToList();
                    result.ForEach(o => { o.selectedSubStore = subStore; o.FromDate = FromDate; o.ToDate = ToDate; });
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the audit trail.
        /// </summary>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <returns></returns>
        public List<OperationalAudit_Result> GetAuditTrail(DateTime FromDate, DateTime ToDate)
        {
            List<OperationalAudit_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    result = context.OperationalAudit(FromDate, ToDate).ToList();

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the financial audit data.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="storename">The storename.</param>
        /// <param name="startdat">The startdat.</param>
        /// <param name="enddate">The enddate.</param>
        /// <returns></returns>
        public List<FinancialAuditTrail_Result> GetFinancialAuditData(string username, string storename, DateTime startdat, DateTime enddate)
        {
            List<FinancialAuditTrail_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    result = context.FinancialAuditTrail(username, storename, startdat, enddate).OrderByDescending(t => t.OrderDate).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// Gets the activity report.
        /// </summary>
        /// <param name="ISFromDate">if set to <c>true</c> [is from date].</param>
        /// <param name="IsToDate">if set to <c>true</c> [is automatic date].</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="UserID">The user unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetActivityReports> GetActivityReport(bool ISFromDate, bool IsToDate, DateTime FromDate, DateTime ToDate, int UserID)
        {
            try
            {
                List<vw_GetActivityReports> objresult = null;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    var _objPackageDetailsdata = context.vw_GetActivityReports;

                    if (UserID != 0 && !ISFromDate && !IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_User_pkey == UserID
                                     select item).Distinct().ToList();
                    }
                    else if (UserID != 0 && ISFromDate && !IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate
                                     select item).Distinct().ToList();
                    }
                    else if (UserID != 0 && !ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_User_pkey == UserID && item.DG_Acitivity_Date <= ToDate
                                     select item).Distinct().ToList();
                    }
                    else if (UserID != 0 && ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                                     select item).Distinct().ToList();
                    }
                    else if (UserID == 0 && ISFromDate && !IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Acitivity_Date >= FromDate
                                     select item).Distinct().ToList();
                    }
                    else if (UserID == 0 && !ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Acitivity_Date <= ToDate
                                     select item).Distinct().ToList();
                    }
                    else if (UserID == 0 && ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                                     select item).Distinct().ToList();
                    }

                    return objresult;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the taking report.
        /// </summary>
        /// <param name="ISFromDate">if set to <c>true</c> [is from date].</param>
        /// <param name="IsToDate">if set to <c>true</c> [is automatic date].</param>
        /// <param name="FromDate">From date.</param>
        /// <param name="ToDate">The automatic date.</param>
        /// <param name="UserID">The user unique identifier.</param>
        /// <returns></returns>
        public List<vw_TakingReport> GetTakingReport(bool ISFromDate, bool IsToDate, DateTime FromDate, DateTime ToDate, int UserID,string SubStore)
        {
            try
            {
                List<vw_TakingReport> objresult = null;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.CommandTimeout = 60000;
                    var _objPackageDetailsdata = context.vw_TakingReport;


                    if (ISFromDate && !IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Orders_Date >= FromDate
                                     select item).Distinct().ToList();
                    }
                    else if (!ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Orders_Date <= ToDate
                                     select item).Distinct().ToList();
                    }
                    else if (ISFromDate && IsToDate)
                    {
                        objresult = (from item in _objPackageDetailsdata
                                     where item.DG_Orders_Date >= FromDate && item.DG_Orders_Date <= ToDate
                                     select item).Distinct().ToList();
                    }
                    objresult.ForEach(o => { o.FromDate = FromDate; o.Todate = ToDate; o.selectedSubStore = SubStore; });
                    return objresult;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the maximum quantityof itemina package.
        /// </summary>
        /// <param name="pkgId">The PKG unique identifier.</param>
        /// <param name="producttypeid">The producttypeid.</param>
        /// <returns></returns>
        public int GetMaxQuantityofIteminaPackage(int pkgId, int producttypeid)
        {
            int MaxImage = 1;
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var _objPackageDetailsdata = (from item in context.DG_PackageDetails
                                              where item.DG_PackageId == pkgId && item.DG_ProductTypeId == producttypeid
                                              select item).FirstOrDefault();
                MaxImage = (int)_objPackageDetailsdata.DG_Product_MaxImage;

                return MaxImage;
            }

        }
        /// <summary>
        /// Sets the package master details.
        /// </summary>
        /// <param name="packageId">The package unique identifier.</param>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="Packageprice">The packageprice.</param>
        /// <returns></returns>
        public bool SetPackageMasterDetails(int packageId, string packageName, string Packageprice)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    foreach (var item in context.DG_PackageDetails.Where(t => t.DG_PackageId == packageId).ToList())
                    {
                        context.DG_PackageDetails.DeleteObject(item);
                    }
                    var packagemasterdetails = context.DG_Orders_ProductType.Where(t => t.DG_Orders_ProductType_pkey == packageId).First();
                    packagemasterdetails.DG_Orders_ProductType_Name = packageName;
                    var packagepricedetails = context.DG_Product_Pricing.Where(t => t.DG_Product_Pricing_ProductType == packageId).First();
                    packagepricedetails.DG_Product_Pricing_ProductPrice = Convert.ToDouble(Packageprice);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Sets the package details.
        /// </summary>
        /// <param name="packageId">The package unique identifier.</param>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="PackageQuantity">The package quantity.</param>
        /// <param name="PackageMaxQuanity">The package maximum quanity.</param>
        /// <returns></returns>
        public bool SetPackageDetails(int packageId, int ProductTypeId, int? PackageQuantity, int? PackageMaxQuanity, int? videoLength)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DG_PackageDetails _objnew = new DG_PackageDetails();
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    _objnew.DG_PackageId = packageId;
                    _objnew.DG_Product_Quantity = PackageQuantity;
                    _objnew.DG_ProductTypeId = ProductTypeId;
                    _objnew.DG_Product_MaxImage = PackageMaxQuanity;
                    _objnew.DG_Video_Length = videoLength;
                    _objnew.IsSynced = false;
                    context.DG_PackageDetails.AddObject(_objnew);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UploadPackagefromExcel(DataTable dt, int userid, int storeid)
        {
            try
            {
                dt.Rows.Remove(dt.Rows[0]);
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    foreach (DataRow row in dt.Rows)
                    {
                        string productName = row.ItemArray[2].ToString();
                        var item = context.DG_Orders_ProductType.Where(r => r.DG_Orders_ProductType_Name == productName).FirstOrDefault();

                        if (item == null)
                        {
                            DG_Orders_ProductType _objnew = new DG_Orders_ProductType();

                            _objnew.DG_Orders_ProductCode = row.ItemArray[1].ToString();
                            _objnew.DG_Orders_ProductType_Desc = row.ItemArray[2].ToString();
                            _objnew.DG_Orders_ProductType_Name = row.ItemArray[2].ToString();
                            _objnew.DG_IsAccessory = Convert.ToBoolean(row.ItemArray[4] == DBNull.Value ? false : row.ItemArray[4]);
                            _objnew.DG_IsActive = Convert.ToBoolean(row.ItemArray[5] == DBNull.Value ? false : row.ItemArray[5]);
                            _objnew.DG_IsPackage = Convert.ToBoolean(row.ItemArray[6] == DBNull.Value ? false : row.ItemArray[6]);
                            _objnew.DG_Orders_ProductType_DiscountApplied = Convert.ToBoolean(row.ItemArray[7] == DBNull.Value ? false : row.ItemArray[7]);
                            _objnew.DG_MaxQuantity = 0;
                            _objnew.DG_Orders_ProductType_IsBundled = false;
                            _objnew.DG_Orders_ProductType_Image = "/images/jpgfloppy.png";
                            _objnew.SyncCode = row.ItemArray[8].ToString();
                            _objnew.IsSynced = false;
                            context.DG_Orders_ProductType.AddObject(_objnew);
                            context.SaveChanges();
                            int result = _objnew.DG_Orders_ProductType_pkey;


                            // insert in product price table

                            if (result > 0)
                            {

                                DG_Product_Pricing __objProduct = new DG_Product_Pricing();
                                string currencySymbol = row.ItemArray[0].ToString();
                                var currencydetails = context.DG_Currency.Where(r => r.DG_Currency_Symbol == currencySymbol).FirstOrDefault();
                                __objProduct.DG_Product_Pricing_ProductType = result;
                                __objProduct.DG_Product_Pricing_ProductPrice = Convert.ToDouble(row.ItemArray[3]);
                                __objProduct.DG_Product_Pricing_Currency_ID = currencydetails.DG_Currency_pkey;
                                __objProduct.DG_Product_Pricing_UpdateDate = ServerDateTime();
                                __objProduct.DG_Product_Pricing_CreatedBy = userid;
                                __objProduct.DG_Product_Pricing_StoreId = storeid;
                                __objProduct.DG_Product_Pricing_IsAvaliable = true;

                                context.DG_Product_Pricing.AddObject(__objProduct);
                                context.SaveChanges();
                            }
                        }
                        else
                        {

                            //update in DG_Orders_ProductType table

                            item.DG_Orders_ProductCode = row.ItemArray[1].ToString();
                            item.DG_Orders_ProductType_Desc = row.ItemArray[2].ToString();
                            item.DG_Orders_ProductType_Name = row.ItemArray[2].ToString();
                            item.DG_IsAccessory = Convert.ToBoolean(row.ItemArray[4] == DBNull.Value ? false : row.ItemArray[4]);
                            item.DG_IsActive = Convert.ToBoolean(row.ItemArray[5] == DBNull.Value ? false : row.ItemArray[5]);
                            item.DG_IsPackage = Convert.ToBoolean(row.ItemArray[6] == DBNull.Value ? false : row.ItemArray[6]);
                            item.DG_Orders_ProductType_DiscountApplied = Convert.ToBoolean(row.ItemArray[7] == DBNull.Value ? false : row.ItemArray[7]);
                            item.DG_MaxQuantity = 0;
                            item.DG_Orders_ProductType_Image = "/images/jpgfloppy.png";
                            if (item.DG_IsPackage)
                            {
                                item.SyncCode = row.ItemArray[8].ToString();

                            }
                            context.SaveChanges();

                            // Update in DG_Product_Pricing table

                            string currencySymbol = row.ItemArray[0].ToString();
                            var currencydetails = context.DG_Currency.Where(r => r.DG_Currency_Symbol == currencySymbol).FirstOrDefault();
                            var productpc = context.DG_Product_Pricing.Where(r => r.DG_Product_Pricing_ProductType == item.DG_Orders_ProductType_pkey).FirstOrDefault();
                            if (productpc != null)
                            {
                                productpc.DG_Product_Pricing_ProductType = item.DG_Orders_ProductType_pkey;
                                productpc.DG_Product_Pricing_ProductPrice = Convert.ToDouble(row.ItemArray[3]);
                                productpc.DG_Product_Pricing_Currency_ID = currencydetails.DG_Currency_pkey;
                                productpc.DG_Product_Pricing_UpdateDate = ServerDateTime();
                                productpc.DG_Product_Pricing_CreatedBy = userid;
                                productpc.DG_Product_Pricing_StoreId = storeid;
                                productpc.DG_Product_Pricing_IsAvaliable = true;
                                context.SaveChanges();
                            }



                        }

                    }


                }


                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Registers the log.
        /// </summary>
        /// <param name="CurrentUser">The current user.</param>
        /// <param name="ActivityType">Type of the activity.</param>
        /// <param name="Description">The description.</param>
        /// <returns></returns>
        public bool RegisterLog(int CurrentUser, int ActivityType, String Description, String syncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Activity _objnewActivity = new DG_Activity();
                    _objnewActivity.DG_Acitivity_By = CurrentUser;
                    _objnewActivity.DG_Acitivity_ActionType = ActivityType;
                    _objnewActivity.DG_Acitivity_Descrption = Description;
                    _objnewActivity.DG_Acitivity_Date = ServerDateTime();
                    _objnewActivity.SyncCode = syncCode;
                    _objnewActivity.IsSynced = false;
                    context.DG_Activity.AddObject(_objnewActivity);
                    context.SaveChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Servers the date time.
        /// </summary>
        /// <returns></returns>
        public DateTime ServerDateTime()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var datetime = context.GetServerDateTime.FirstOrDefault();

                return datetime.CurrentDateTime;
            }
        }
        /// <summary>
        /// Registers the log.
        /// </summary>
        /// <param name="CurrentUser">The current user.</param>
        /// <param name="ActivityType">Type of the activity.</param>
        /// <param name="Description">The description.</param>
        /// <param name="RefID">The preference unique identifier.</param>
        /// <returns></returns>
        public bool RegisterLog(int CurrentUser, int ActivityType, String Description, String SyncCode, int RefID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Activity _objnewActivity = new DG_Activity();
                    _objnewActivity.DG_Acitivity_By = CurrentUser;
                    _objnewActivity.DG_Acitivity_ActionType = ActivityType;
                    _objnewActivity.DG_Acitivity_Descrption = Description;
                    _objnewActivity.DG_Acitivity_Date = ServerDateTime();
                    _objnewActivity.DG_Reference_ID = RefID;
                    _objnewActivity.SyncCode = SyncCode;
                    _objnewActivity.IsSynced = false;
                    context.DG_Activity.AddObject(_objnewActivity);
                    context.SaveChanges();
                    return true;


                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Currents the currency conversion rate.
        /// </summary>
        /// <returns></returns>
        public string CurrentCurrencyConversionRate()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                XElement root = new XElement("Currencies",
                (from row in context.DG_Currency
                 select new
                 {
                     row.DG_Currency_pkey,
                     row.DG_Currency_Rate,
                     row.DG_Currency_Default,
                     row.SyncCode
                 }).ToList().Select(
                    x => new XElement("Currency", new XAttribute("ID", x.DG_Currency_pkey), new XAttribute("Rate", x.DG_Currency_Rate), new XAttribute("DefaultCurrency", x.DG_Currency_Default), new XAttribute("SyncCode", x.SyncCode)
                  )));

                return root.ToString();
            }



        }
        /// <summary>
        /// Generates the order.
        /// </summary>
        /// <param name="OrderNumber">The order number.</param>
        /// <param name="Ordercost">The ordercost.</param>
        /// <param name="OrderNetCost">The order net cost.</param>
        /// <param name="PaymentDetails">The payment details.</param>
        /// <param name="PaymentMode">The payment mode.</param>
        /// <param name="TotalDiscount">The total discount.</param>
        /// <param name="DiscountDetails">The discount details.</param>
        /// <param name="UserID">The user unique identifier.</param>
        /// <param name="CurrencyId">The currency unique identifier.</param>
        /// <param name="OrderMode">The order mode.</param>
        /// <returns></returns>
        //public int GenerateOrder(string OrderNumber, decimal Ordercost, decimal OrderNetCost, String PaymentDetails, int PaymentMode, double TotalDiscount, String DiscountDetails, int UserID, int CurrencyId, String OrderMode, String SyncCode)
        //{
        //    String ConversionRate = string.Empty;

        //    using (EntityConnection connection = new EntityConnection(connectionstring))
        //    {
        //        DG_Orders Order = new DG_Orders();
        //        DigiphotoEntities context = new DigiphotoEntities(connection);
        //        Order.DG_Orders_Cost = decimal.Round(Ordercost, 3, MidpointRounding.ToEven);
        //        Order.DG_Orders_NetCost = decimal.Round(OrderNetCost, 3, MidpointRounding.ToEven);
        //        Order.DG_Orders_PaymentDetails = PaymentDetails;
        //        Order.DG_Orders_Currency_Conversion_Rate = ConversionRate;
        //        Order.DG_Orders_PaymentMode = PaymentMode;
        //        Order.DG_Orders_Total_Discount = Convert.ToDouble(decimal.Round(Convert.ToDecimal(TotalDiscount), 3, MidpointRounding.ToEven));
        //        Order.DG_Orders_Total_Discount_Details = DiscountDetails;
        //        Order.DG_Orders_UserID = UserID;
        //        Order.DG_Orders_Currency_ID = CurrencyId;
        //        Order.DG_Orders_Currency_Conversion_Rate = CurrentCurrencyConversionRate();
        //        Order.DG_Order_Mode = OrderMode;
        //        Order.DG_Orders_Date = ServerDateTime();
        //        Order.DG_Orders_Number = OrderNumber;
        //        Order.SyncCode = SyncCode;
        //        Order.IsSynced = false;
        //        context.DG_Orders.AddObject(Order);

        //        context.SaveChanges();
        //        int OrderID = Order.DG_Orders_pkey;
        //        return OrderID;
        //    }

        //    //  return -1;
        //}
        /// <summary>
        /// Adds the image automatic printer queue.
        /// </summary>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="Images">The images.</param>
        /// <param name="OrderDetailedKey">The order detailed key.</param>
        /// <param name="IsBundled">if set to <c>true</c> [is bundled].</param>
        /// <param name="GreenSpecPrint">if set to <c>true</c> [green spec print].</param>
        /// <returns></returns>
        //public int AddImageToPrinterQueue(int ProductTypeId, List<string> Images, int OrderDetailedKey, bool IsBundled, bool GreenSpecPrint, List<PhotoPrintPositionDic> PhotoPrintPositionDicList)
        //{
        //    DigiPhotoDataServices objDataServices = new DigiPhotoDataServices();
        //    int Printerid = objDataServices.GetAssocitedPrinterIDByProductType(ProductTypeId);
        //    int result = -1;
        //    if (!IsBundled)
        //    {
        //        if (ProductTypeId == 98)
        //        {
        //            using (EntityConnection connection = new EntityConnection(connectionstring))
        //            {
        //                DigiphotoEntities context = new DigiphotoEntities(connection);

        //                if (Images.Count % 2 == 1)
        //                {
        //                    Images.Add(Images[Images.Count - 1]);
        //                }
        //                int counter = 0;
        //                int count = Images.Count;
        //                while (counter <= count / 2)
        //                {
        //                    DG_PrinterQueue objqueue = new DG_PrinterQueue();
        //                    objqueue.DG_PrinterQueue_Image_Pkey = string.Join(",", Images.ToArray());
        //                    string[] objImg = Images.ToArray();
        //                    string str = Images.ToArray()[counter] + "," + Images.ToArray()[counter + 1];
        //                    objqueue.DG_PrinterQueue_Image_Pkey = str;
        //                    objqueue.DG_Associated_PrinterId = Printerid;
        //                    objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
        //                    objqueue.is_Active = true;
        //                    objqueue.DG_SentToPrinter = false;
        //                    objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
        //                    objqueue.DG_IsSpecPrint = GreenSpecPrint;
        //                    var itemlist = context.DG_PrinterQueue.OrderByDescending(t => t.DG_PrinterQueue_Pkey).FirstOrDefault();
        //                    if (itemlist != null)
        //                    {
        //                        objqueue.QueueIndex = itemlist.QueueIndex + 1;
        //                    }
        //                    else
        //                    {
        //                        objqueue.QueueIndex = 1;
        //                    }
        //                    context.DG_PrinterQueue.AddObject(objqueue);
        //                    context.SaveChanges();
        //                    result = objqueue.DG_PrinterQueue_Pkey;
        //                    counter = counter + 2;
        //                }




        //            }


        //        }
        //        //if product type==79 save page wise 
        //        else if (ProductTypeId == 79 && PhotoPrintPositionDicList != null)
        //        {


        //            List<int> SavedImage = new List<int>();

        //            foreach (int PageNo in PhotoPrintPositionDicList.Select(o => o.PhotoPrintPositionList.PageNo).Distinct().OrderBy(o => o).ToList())
        //            {
        //                string imageIds = string.Empty;
        //                //if(SavedImage.Exists(
        //                using (EntityConnection connection = new EntityConnection(connectionstring))
        //                {
        //                    DigiphotoEntities context = new DigiphotoEntities(connection);
        //                    var item = context.DG_Orders_ProductType.Where(t => t.DG_Orders_ProductType_pkey == ProductTypeId).FirstOrDefault();
        //                    if (Convert.ToBoolean(item.DG_IsAccessory) != true)
        //                    {
        //                        DG_PrinterQueue objqueue = new DG_PrinterQueue();
        //                        objqueue.DG_Associated_PrinterId = Printerid;

        //                        int MinPageNo = PhotoPrintPositionDicList.Where(o => SavedImage.Any(c => c == o.PhotoId) == false).Min(j => j.PhotoId);
        //                        string img = string.Empty;
        //                        // img += from o in PhotoPrintPositionDicList where o.PhotoPrintPositionList.Where(p => p.PageNo == MinPageNo).FirstOrDefault().PageNo == MinPageNo select o.PhotoId;

        //                        PhotoPrintPositionDicList.Where(o => o.PhotoPrintPositionList.PageNo == PageNo).OrderBy(r => r.PhotoPrintPositionList.PhotoPosition).ToList().ForEach(p => imageIds += "," + p.PhotoId);
        //                        objqueue.DG_PrinterQueue_Image_Pkey = imageIds.Length > 0 ? imageIds.Substring(1) : string.Empty;

        //                        imageIds = string.Empty;

        //                        PhotoPrintPositionDicList.Where(o => o.PhotoPrintPositionList.PageNo == PageNo).OrderBy(r => r.PhotoPrintPositionList.PhotoPosition).ToList().ForEach(p => imageIds += "," + p.PhotoPrintPositionList.RotationAngle);
        //                        objqueue.RotationAngle = imageIds.Length > 0 ? imageIds.Substring(1) : string.Empty;

        //                        objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
        //                        objqueue.DG_SentToPrinter = false;
        //                        objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
        //                        objqueue.is_Active = true;
        //                        objqueue.DG_IsSpecPrint = GreenSpecPrint;
        //                        objqueue.DG_Print_Date = ServerDateTime();
        //                        var itemlist = context.DG_PrinterQueue.OrderByDescending(t => t.DG_PrinterQueue_Pkey).FirstOrDefault();
        //                        if (itemlist != null)
        //                        {
        //                            objqueue.QueueIndex = itemlist.QueueIndex + 1;
        //                        }
        //                        else
        //                        {
        //                            objqueue.QueueIndex = 1;
        //                        }
        //                        context.DG_PrinterQueue.AddObject(objqueue);
        //                        context.SaveChanges();
        //                        result = objqueue.DG_PrinterQueue_Pkey;
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            foreach (string image in Images)
        //            {
        //                using (EntityConnection connection = new EntityConnection(connectionstring))
        //                {
        //                    DigiphotoEntities context = new DigiphotoEntities(connection);
        //                    var item = context.DG_Orders_ProductType.Where(t => t.DG_Orders_ProductType_pkey == ProductTypeId).FirstOrDefault();
        //                    if (Convert.ToBoolean(item.DG_IsAccessory) != true)
        //                    {
        //                        DG_PrinterQueue objqueue = new DG_PrinterQueue();
        //                        objqueue.DG_Associated_PrinterId = Printerid;
        //                        objqueue.DG_PrinterQueue_Image_Pkey = image;
        //                        objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
        //                        objqueue.DG_SentToPrinter = false;
        //                        objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
        //                        objqueue.is_Active = true;
        //                        objqueue.DG_IsSpecPrint = GreenSpecPrint;
        //                        objqueue.DG_Print_Date = ServerDateTime();
        //                        var itemlist = context.DG_PrinterQueue.OrderByDescending(t => t.DG_PrinterQueue_Pkey).FirstOrDefault();
        //                        if (itemlist != null)
        //                        {
        //                            objqueue.QueueIndex = itemlist.QueueIndex + 1;
        //                        }
        //                        else
        //                        {
        //                            objqueue.QueueIndex = 1;
        //                        }
        //                        context.DG_PrinterQueue.AddObject(objqueue);
        //                        context.SaveChanges();
        //                        result = objqueue.DG_PrinterQueue_Pkey;
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    else
        //    {
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);
        //            DG_PrinterQueue objqueue = new DG_PrinterQueue();
        //            objqueue.DG_Associated_PrinterId = Printerid;
        //            objqueue.DG_PrinterQueue_Image_Pkey = string.Join(",", Images.ToArray());
        //            objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
        //            objqueue.is_Active = true;
        //            objqueue.DG_SentToPrinter = false;
        //            objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
        //            objqueue.DG_IsSpecPrint = GreenSpecPrint;
        //            var itemlist = context.DG_PrinterQueue.OrderByDescending(t => t.DG_PrinterQueue_Pkey).FirstOrDefault();
        //            if (itemlist != null)
        //            {
        //                objqueue.QueueIndex = itemlist.QueueIndex + 1;
        //            }
        //            else
        //            {
        //                objqueue.QueueIndex = 1;
        //            }
        //            context.DG_PrinterQueue.AddObject(objqueue);
        //            context.SaveChanges();
        //            result = objqueue.DG_PrinterQueue_Pkey;
        //        }
        //    }
        //    return result;

        //}
        /// <summary>
        /// Saves the order line items.
        /// </summary>
        /// <param name="ProductType">Type of the product.</param>
        /// <param name="OrderID">The order unique identifier.</param>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="Qty">The qty.</param>
        /// <param name="DisCountDetails">The dis count details.</param>
        /// <param name="TotalDiscount">The total discount.</param>
        /// <param name="UnitPrice">The unit price.</param>
        /// <param name="TotalPrice">The total price.</param>
        /// <param name="NetPrice">The net price.</param>
        /// <param name="ParentID">The parent unique identifier.</param>
        /// <param name="SubStoreID">The sub store unique identifier.</param>
        /// <returns></returns>
        public int SaveOrderLineItems(int ProductType, int? OrderID, String PhotoId, int Qty, string DisCountDetails, decimal TotalDiscount, decimal UnitPrice, decimal TotalPrice, decimal NetPrice, int ParentID, int SubStoreID, int IdentifierType, string UniqueIdentifier, string SyncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DG_Orders_Details Order = new DG_Orders_Details();
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    Order.DG_Orders_ID = OrderID;
                    Order.DG_Photos_ID = PhotoId;
                    Order.DG_Orders_LineItems_Quantity = Qty;
                    Order.DG_Orders_LineItems_DiscountType = DisCountDetails;
                    Order.DG_Orders_LineItems_DiscountAmount = decimal.Round(TotalDiscount, 3, MidpointRounding.ToEven);
                    Order.DG_Orders_LineItems_Created = ServerDateTime();
                    Order.DG_Orders_Details_ProductType_pkey = ProductType;
                    Order.DG_Orders_Details_Items_UniPrice = decimal.Round(UnitPrice, 3, MidpointRounding.ToEven);
                    Order.DG_Orders_Details_LineItem_ParentID = ParentID;
                    Order.DG_Orders_Details_Items_TotalCost = decimal.Round(TotalPrice, 3, MidpointRounding.ToEven);
                    Order.DG_Orders_Details_Items_NetPrice = decimal.Round(NetPrice, 3, MidpointRounding.ToEven);
                    Order.DG_Order_SubStoreId = SubStoreID;
                    Order.DG_Order_IdentifierType = IdentifierType;
                    Order.DG_Order_ImageUniqueIdentifier = UniqueIdentifier;
                    Order.SyncCode = SyncCode;
                    context.DG_Orders_Details.AddObject(Order);
                    context.SaveChanges();
                    int OrderLineItemID = Order.DG_Orders_LineItems_pkey;


                    return OrderLineItemID;

                }

            }
            catch (Exception ex)
            {
                return -1;

            }

        }


        /// <summary>
        /// Used to save the printer order incase of product type Album(ProductTypeId=79)
        /// </summary>
        /// <param name="OrderLineItemId"></param>
        /// <param name="PhotoId"></param>
        /// <param name="PhotoOrder"></param>
        /// <returns></returns>
        public int SaveAlbumPrintPosition(int OrderLineItemId, List<PhotoPrintPositionDic> PrintPhotoOrderIds)
        {


            //string PhotoIds, string PhotoOrder)
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    PhotoAlbumPrintOrder PrintOrder = null;
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    foreach (PhotoPrintPositionDic photoPrintPositionDic in PrintPhotoOrderIds)
                    {
                        //foreach (PhotoPrintPosition photoPosition in PhotoId.PhotoPrintPositionList)
                        //{
                        if (photoPrintPositionDic == null)
                            continue;
                        PrintOrder = new PhotoAlbumPrintOrder();
                        PrintOrder.OrderLineItemId = OrderLineItemId;
                        PrintOrder.PhotoId = photoPrintPositionDic.PhotoId;
                        PrintOrder.PageNo = photoPrintPositionDic.PhotoPrintPositionList.PageNo;
                        PrintOrder.PrintPosition = photoPrintPositionDic.PhotoPrintPositionList.PhotoPosition;
                        PrintOrder.RotationAngle = photoPrintPositionDic.PhotoPrintPositionList.RotationAngle;
                        context.PhotoAlbumPrintOrder.AddObject(PrintOrder);
                        // }//End of inner loop

                    }//End of outer loop
                    context.SaveChanges();

                    int PrintOrderId = (int)PrintOrder.PrintOrderId;
                    return PrintOrderId;
                }
                /*
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    PhotoAlbumPrintOrder PrintOrder = new PhotoAlbumPrintOrder();
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    foreach (int PhotoId in PrintPhotoOrderIds.Keys)
                    {

                        foreach (string PhotoOrderNumber in PrintPhotoOrderIds.Where(o => o.Key == PhotoId && o.Value!=null).FirstOrDefault().Value.Split(','))
                        {
                            if (string.IsNullOrEmpty(PhotoOrderNumber))
                                continue;

                            PrintOrder.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = OrderLineItemId;
                            PrintOrder.DG_Photos.DG_Photos_pkey = PhotoId;
                            PrintOrder.PhotoOrder = int.Parse(PhotoOrderNumber);
                            context.PhotoAlbumPrintOrder.AddObject(PrintOrder);
                        }//End of inner loop
                    }//End of outer loop
                    context.SaveChanges();

                    int PrintOrderId = (int)PrintOrder.PrintOrderId;
                    return PrintOrderId;
                }
                */

            }
            catch (Exception ex)
            {
                return -1;

            }
        }

        /// <summary>
        /// Gets the camera list.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetCameraDetails> GetCameraList()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.vw_GetCameraDetails.OrderBy(t => t.DG_Camera_ID).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        /// <summary>
        /// Gets the camera details by unique identifier.
        /// </summary>
        /// <param name="CameraId">The camera unique identifier.</param>
        /// <returns></returns>
        public DG_CameraDetails GetCameraDetailsByID(int CameraId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.DG_CameraDetails.Where(t => t.DG_Camera_pkey == CameraId).First();
                }
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        /// <summary>
        /// Sets the camera details.
        /// </summary>
        /// <param name="CameraName">Name of the camera.</param>
        /// <param name="CameraMake">The camera make.</param>
        /// <param name="CameraModel">The camera model.</param>
        /// <param name="PhotoSeries">The photo series.</param>
        /// <param name="CameraId">The camera unique identifier.</param>
        /// <param name="PhotoGrapherId">The photo grapher unique identifier.</param>
        /// <param name="LoginId">The login unique identifier.</param>
        /// <returns></returns>
        public void SetCameraDetails(string CameraName, string CameraMake, string CameraModel, string PhotoSeries, int CameraId, int PhotoGrapherId, int LoginId, int LocationId, int substoreId, string SyncCode, bool? isChromaColor, out int cmraId, out int Camerapkey)
        {
            Camerapkey = 0;
            cmraId = 0;
            try
            {

                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (CameraId == 0)
                    {
                        DG_CameraDetails _objnew = new DG_CameraDetails();
                        _objnew.DG_Camera_Make = CameraMake;
                        _objnew.DG_Camera_Model = CameraModel;
                        _objnew.DG_Camera_Name = CameraName;
                        _objnew.DG_AssignTo = PhotoGrapherId;
                        _objnew.DG_Updatedby = LoginId;
                        _objnew.DG_UpdatedDate = ServerDateTime();
                        _objnew.DG_Camera_Start_Series = PhotoSeries;
                        _objnew.DG_Camera_IsDeleted = false;
                        _objnew.SyncCode = SyncCode;
                        _objnew.IsSynced = false;
                        _objnew.Is_ChromaColor = isChromaColor;
                        context.DG_CameraDetails.AddObject(_objnew);
                        context.SaveChanges();
                        CameraId = _objnew.DG_Camera_pkey;
                        cmraId = _objnew.DG_Camera_pkey;
                        Camerapkey = _objnew.DG_Camera_pkey;
                        _objnew.DG_Camera_ID = CameraId;
                        context.SaveChanges();

                        var _obj = context.DG_Users.Where(t => t.DG_User_pkey == PhotoGrapherId).FirstOrDefault();
                        if (_obj != null)
                        {
                            _obj.DG_Location_ID = LocationId;
                            _obj.DG_Substore_ID = substoreId;
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        //add is delete falg to existing one and insert new record.
                        var item = context.DG_CameraDetails.Where(t => t.DG_Camera_pkey == CameraId).FirstOrDefault();
                        if (item != null)
                        {
                            item.DG_Camera_IsDeleted = true;
                            item.DG_Updatedby = LoginId;
                            item.DG_UpdatedDate = ServerDateTime();
                            item.IsSynced = false;
                            context.SaveChanges();

                            //Adding new one.
                            DG_CameraDetails _objnew = new DG_CameraDetails();
                            _objnew.DG_Camera_IsDeleted = false;
                            _objnew.DG_Camera_Make = CameraMake;
                            _objnew.DG_Camera_Model = CameraModel;
                            _objnew.DG_Camera_Name = CameraName;
                            _objnew.DG_AssignTo = PhotoGrapherId;
                            _objnew.DG_Updatedby = LoginId;
                            _objnew.DG_UpdatedDate = ServerDateTime();
                            _objnew.DG_Camera_Start_Series = PhotoSeries;
                            _objnew.IsSynced = false;
                            _objnew.Is_ChromaColor = isChromaColor;
                            context.DG_CameraDetails.AddObject(_objnew);
                            context.SaveChanges();
                            //CameraId = _objnew.DG_Camera_pkey;
                            Camerapkey = _objnew.DG_Camera_pkey;
                            cmraId = (int)item.DG_Camera_ID;
                            _objnew.DG_Camera_ID = cmraId;
                            context.SaveChanges();

                            var _obj = context.DG_Users.Where(t => t.DG_User_pkey == PhotoGrapherId).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.DG_Location_ID = LocationId;
                                _obj.DG_Substore_ID = substoreId;
                            }
                            context.SaveChanges();
                        }
                    }

                    //return CameraId;
                }

            }
            catch (Exception ex)
            {
                return ;
            }

        }
        /// <summary>
        /// Deletes the camera details.
        /// </summary>
        /// <param name="CameraId">The camera unique identifier.</param>
        /// <returns></returns>
        public bool DeleteCameraDetails(int CameraId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_CameraDetails.Where(t => t.DG_Camera_pkey == CameraId).FirstOrDefault();
                    if (item != null)
                    {
                        context.DG_CameraDetails.DeleteObject(item);
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the running services.
        /// </summary>
        /// <returns></returns>
        public List<DG_Services> GetRunningServices()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.DG_Services.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Deletes the users.
        /// </summary>
        /// <param name="userID">The user unique identifier.</param>
        /// <returns></returns>
        public bool DeleteUsers(Int32 userID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_Users.Where(t => t.DG_User_pkey == userID).FirstOrDefault();
                    if (_obj != null)
                    {
                        context.DG_Users.DeleteObject(_obj);
                    }
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool CheckIsCodeType(int PhotoId)
        {
            bool res = false;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_Photos.Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault();
                    if (_obj != null)
                    {
                        if (_obj.DG_IsCodeType == true)
                            res = true;
                    }
                }
            }
            catch
            {
                res = false;
            }
            return res;

        }

        /// <summary>
        /// Sets the order details.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="SubStoreId">The sub store unique identifier.</param>
        /// <returns></returns>
        //public int SetOrderDetails(int? PhotoId, int? ProductTypeId, int SubStoreId, string SyncCode)
        //{
        //    try
        //    {
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);
        //            DG_Orders_Details _objnew = new DG_Orders_Details();
        //            _objnew.DG_Photos_ID = Convert.ToString(PhotoId);
        //            _objnew.DG_Orders_LineItems_Created = ServerDateTime();
        //            _objnew.DG_Orders_Details_ProductType_pkey = ProductTypeId;
        //            _objnew.DG_Order_SubStoreId = SubStoreId;
        //            _objnew.DG_Orders_Details_LineItem_ParentID = -1;
        //            double _itemprice = GetProductPricing(Convert.ToInt32(ProductTypeId));
        //            _objnew.DG_Orders_Details_Items_UniPrice = (decimal)_itemprice;
        //            _objnew.DG_Orders_LineItems_Quantity = 1;
        //            _objnew.DG_Orders_Details_Items_TotalCost = 0;
        //            _objnew.DG_Orders_Details_Items_NetPrice = (decimal)_itemprice;
        //            _objnew.DG_Orders_LineItems_DiscountAmount = 0;
        //            _objnew.SyncCode = SyncCode;
        //            context.DG_Orders_Details.AddObject(_objnew);
        //            context.SaveChanges();
        //            return _objnew.DG_Orders_LineItems_pkey;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }
        //}
        /// <summary>
        /// Sets the data automatic printer queue.
        /// </summary>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="DG_AssocatedPrinterId">The command g_ assocated printer unique identifier.</param>
        /// <param name="DG_SentToPrinter">if set to <c>true</c> [command g_ sent automatic printer].</param>
        /// <param name="orderDetailID">The order detail unique identifier.</param>
        /// <returns></returns>
        public bool SetDataToPrinterQueue(string PhotoId, int? ProductTypeId, int? DG_AssocatedPrinterId, bool DG_SentToPrinter, int orderDetailID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_PrinterQueue _objnew = new DG_PrinterQueue();
                    _objnew.DG_Associated_PrinterId = DG_AssocatedPrinterId;
                    _objnew.DG_Order_Details_Pkey = orderDetailID;
                    _objnew.DG_PrinterQueue_ProductID = ProductTypeId;
                    if (ProductTypeId == 98)
                        _objnew.DG_PrinterQueue_Image_Pkey = PhotoId + "," + PhotoId;
                    else
                        _objnew.DG_PrinterQueue_Image_Pkey = PhotoId;
                    _objnew.DG_SentToPrinter = false;
                    _objnew.is_Active = true; int? countmax;
                    _objnew.DG_Print_Date = ServerDateTime();
                    try
                    {
                        countmax = context.DG_PrinterQueue.Max(t => t.QueueIndex).Value + 1;
                    }
                    catch
                    {
                        countmax = 1;
                    }
                    _objnew.QueueIndex = countmax == null ? 0 : countmax;
                    context.DG_PrinterQueue.AddObject(_objnew);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the associated printer unique identifier from application roduct type unique identifier.
        /// </summary>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <returns></returns>
        public int GetAssociatedPrinterIdFromPRoductTypeId(int? ProductTypeId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_AssociatedPrinters.Where(t => t.DG_AssociatedPrinters_ProductType_ID == ProductTypeId).FirstOrDefault();
                    if (item != null)
                    {
                        return item.DG_AssociatedPrinters_Pkey;
                    }
                    else
                    {
                        return 0;
                    }
                }
                return 0;
            }
            catch (Exception eX)
            {
                return 0;
            }
        }
        /// <summary>
        /// Gets the printer queue.
        /// </summary>
        /// <param name="substoreID">The substore unique identifier.</param>
        /// <param name="productypeId">The productype unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetPrinterQueueforPrint> GetPrinterQueue(int substoreID, ref string productypeId)
        {
            try
            {
                string ptypeid = productypeId;

                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (string.IsNullOrEmpty(productypeId))
                    {
                        var firstitemlist = context.vw_GetPrinterQueueforPrint.ToList().Where(t => t.DG_Order_SubStoreId == substoreID);
                        if (firstitemlist != null)
                        {
                            if (firstitemlist.Count() > 0)
                            {
                                var item = firstitemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                                if (item != null)
                                    productypeId = item.DG_Orders_ProductType_pkey.ToString();
                                else
                                    productypeId = "";
                                return firstitemlist.OrderBy(t => t.QueueIndex).ToList();
                            }
                            else
                            {
                                productypeId = "";
                                return new List<vw_GetPrinterQueueforPrint>();
                            }
                        }
                        else
                        {
                            productypeId = "";
                            return new List<vw_GetPrinterQueueforPrint>();
                        }
                    }
                    else
                    {
                        List<string> _objstr = new List<string>();
                        _objstr = productypeId.Split(',').ToList();
                        var itemlist = context.vw_GetPrinterQueueforPrint.ToList().Where(t => t.DG_Order_SubStoreId == substoreID && !_objstr.Contains(t.DG_Orders_ProductType_pkey.ToString()));

                        if (itemlist != null)
                        {
                            if (itemlist.Count() > 0)
                            {

                                productypeId = productypeId + "," + itemlist.OrderBy(t => t.DG_PrinterQueue_Pkey).FirstOrDefault().DG_Orders_ProductType_pkey.ToString();
                                return itemlist.OrderBy(t => t.QueueIndex).ToList();
                            }
                            else
                            {
                                productypeId = "";
                                return new List<vw_GetPrinterQueueforPrint>();
                            }
                        }
                        else
                        {
                            var otheritem = context.vw_GetPrinterQueueforPrint.ToList().Where(t => t.DG_Order_SubStoreId == substoreID && !ptypeid.Contains(t.DG_Orders_ProductType_pkey.ToString()));
                            if (otheritem != null)
                            {
                                if (otheritem.Count() > 0)
                                {
                                    productypeId = productypeId + "," + otheritem.OrderBy(t => t.QueueIndex).FirstOrDefault().DG_Orders_ProductType_pkey.ToString();
                                    return otheritem.OrderBy(t => t.QueueIndex).ToList();
                                }
                                else
                                {
                                    productypeId = "";
                                    return new List<vw_GetPrinterQueueforPrint>();
                                }
                            }
                            else
                            {
                                productypeId = "";
                                return new List<vw_GetPrinterQueueforPrint>();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<vw_GetPrinterQueueforPrint>();
            }
        }
        /// <summary>
        /// Gets the printer queue details.
        /// </summary>
        /// <param name="ordernumer">The ordernumer.</param>
        /// <returns></returns>
        /// 
        public List<vw_GetFilteredPrinterQueue> GetPrinterQueueForUpdown(int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    //var firstitemlist = context.vw_GetPrinterQueue.ToList().Where(t => t.DG_SentToPrinter == false && t.DG_Order_SubStoreId == substoreID);
                    var firstitemlist = context.vw_GetFilteredPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == substoreID);
                    if (firstitemlist != null)
                    {
                        return firstitemlist.OrderBy(t => t.QueueIndex).ToList();
                    }
                    else
                    {
                        return new List<vw_GetFilteredPrinterQueue>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<vw_GetFilteredPrinterQueue>();
            }
        }
        //public List<vw_GetPrinterQueue> GetPrinterQueueDetails(string ordernumer)
        //{
        //    try
        //    {
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);
        //            var _objdata = context.vw_GetPrinterQueue.ToList().Where(t => t.DG_Orders_Number == ordernumer).Distinct();
        //            if (_objdata != null)
        //            {
        //                return _objdata.OrderBy(t => t.QueueIndex).ToList();
        //            }
        //            else return null;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}

        public List<GetPrinterQueueDetailsByOrderNo_Result> GetPrinterQueueDetails(string ordernumer)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var data = context.GetPrinterQueueDetailsByOrderNo(ordernumer).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the order details by unique identifier.
        /// </summary>
        /// <param name="orderid">The orderid.</param>
        /// <returns></returns>
        public vw_GetOrderDetails GetOrderDetailsByID(int orderid)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objdata = context.vw_GetOrderDetails.ToList().Where(t => t.DG_Orders_LineItems_pkey == orderid).FirstOrDefault();
                    if (_objdata != null)
                    {
                        return _objdata;
                    }
                    else return null;

                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        /// <summary>
        /// Gets the printer queuefor print.
        /// </summary>
        /// <param name="SubStoreID">The sub store unique identifier.</param>
        /// <returns></returns>
        public vw_GetFilteredPrinterQueueForPrint GetPrinterQueueforPrint(int SubStoreID, ref List<int> ProducttypeId)
        {
            try
            {
                if (ProducttypeId.Count == 0)
                {
                    using (EntityConnection connection = new EntityConnection(connectionstring))
                    {
                        DigiphotoEntities context = new DigiphotoEntities(connection);
                        var itemlist = context.vw_GetFilteredPrinterQueueForPrint.ToList().Where(t => t.DG_Order_SubStoreId == SubStoreID);
                        //
                        if (itemlist != null)
                        {
                            if (itemlist.Count() > 0)
                            {
                                var trtitem = itemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                                ProducttypeId.Add(trtitem.DG_Orders_ProductType_pkey);
                                return trtitem;
                            }
                            else
                            {
                                return new vw_GetFilteredPrinterQueueForPrint();
                            }
                        }
                        else
                        {
                            return new vw_GetFilteredPrinterQueueForPrint();
                        }
                    }
                }
                else
                {
                    using (EntityConnection connection = new EntityConnection(connectionstring))
                    {
                        using (DigiphotoEntities context = new DigiphotoEntities(connection))
                        {
                            List<int> _ptypeId = ProducttypeId;
                            if (ProducttypeId != null)
                            {
                                var itemlist = context.vw_GetFilteredPrinterQueueForPrint.ToList().Where(t => t.DG_Order_SubStoreId == SubStoreID
                                    && !_ptypeId.Contains(t.DG_Orders_ProductType_pkey));
                                //
                                if (itemlist != null)
                                {
                                    if (itemlist.Count() > 0)
                                    {
                                        var trtitem = itemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                                        if (trtitem != null)
                                        {
                                            ProducttypeId.Add(trtitem.DG_Orders_ProductType_pkey);
                                            return trtitem;
                                        }
                                        else
                                        {
                                            ProducttypeId = new List<int>();
                                            return new vw_GetFilteredPrinterQueueForPrint();
                                        }
                                    }
                                    else
                                    {
                                        ProducttypeId = new List<int>();
                                        return new vw_GetFilteredPrinterQueueForPrint();
                                    }
                                }
                                else
                                {
                                    ProducttypeId = new List<int>();
                                    return new vw_GetFilteredPrinterQueueForPrint();
                                }
                            }
                            else
                            {
                                ProducttypeId = new List<int>();
                                return new vw_GetFilteredPrinterQueueForPrint();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new vw_GetFilteredPrinterQueueForPrint();
                throw ex;
            }

        }
        /// <summary>
        /// Sets the printer queue.
        /// </summary>
        /// <param name="QueueId">The queue unique identifier.</param>
        public void SetPrinterQueue(int QueueId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_PrinterQueue.Where(t => t.DG_PrinterQueue_Pkey == QueueId).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.DG_SentToPrinter = true;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Sets the printer queue for reprint.
        /// </summary>
        /// <param name="QueueId">The queue unique identifier.</param>
        public void SetPrinterQueueForReprint(int QueueId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_PrinterQueue.Where(t => t.DG_PrinterQueue_Pkey == QueueId).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.DG_SentToPrinter = false;
                        int? countmax = context.DG_PrinterQueue.Max(t => t.QueueIndex).Value + 1;
                        _obj.QueueIndex = countmax == null ? 0 : countmax;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Readies for print.
        /// </summary>
        /// <param name="QueueId">The queue unique identifier.</param>
        public void ReadyForPrint(int QueueId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_PrinterQueue.Where(t => t.DG_PrinterQueue_Pkey == QueueId).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.is_Active = false;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Gets the default name of the currency.
        /// </summary>
        /// <returns></returns>
        public string GetDefaultCurrencyName()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Currency.Where(t => t.DG_Currency_Default == true).FirstOrDefault();
                    if (item != null)
                    {
                        return item.DG_Currency_Name;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /// <summary>
        /// Sets the index of the print queue.
        /// </summary>
        /// <param name="pkey">The pkey.</param>
        /// <param name="flag">The flag.</param>
        public void SetPrintQueueIndex(int pkey, string flag)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _obj = context.DG_PrinterQueue.Where(t => t.DG_PrinterQueue_Pkey == pkey).FirstOrDefault();
                    if (_obj != null)
                    {
                        if (flag == "Up")
                        {
                            if (_obj.QueueIndex - 1 > 0)
                            {
                                var _objdownitem = context.DG_PrinterQueue.Where(t => t.QueueIndex == _obj.QueueIndex - 1).FirstOrDefault();
                                if (_objdownitem != null)
                                {
                                    _objdownitem.QueueIndex = _obj.QueueIndex;
                                }
                                _obj.QueueIndex = _obj.QueueIndex - 1;
                            }
                        }
                        else if (flag == "Down")
                        {

                            if (_obj.QueueIndex + 1 <= context.DG_PrinterQueue.ToList().Count)
                            {
                                var _objdownitem = context.DG_PrinterQueue.Where(t => t.QueueIndex == _obj.QueueIndex + 1).FirstOrDefault();
                                if (_objdownitem != null)
                                {
                                    _objdownitem.QueueIndex = _obj.QueueIndex;
                                }
                                _obj.QueueIndex = _obj.QueueIndex + 1;
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Gets the file name by rfid.
        /// </summary>
        /// <param name="RFID">The rfid.</param>
        /// <returns></returns>
        public string GetFileNameByRFID(string RFID)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_RFID == RFID).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_FileName;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the file name by photo unique identifier.
        /// </summary>
        /// <param name="PhotoID">The photo unique identifier.</param>
        /// <returns></returns>
        public string GetFileNameByPhotoID(string PhotoID)
        {
            if (PhotoID.Contains(","))
                return null;
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == Convert.ToInt32(PhotoID)).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.DG_Photos_FileName;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the order detailsfor refund.
        /// </summary>
        /// <param name="OrderNo">The order no.</param>
        /// <returns></returns>
        public List<vw_GetOrderDetailsforRefund> GetOrderDetailsforRefund(string OrderNo)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetOrderDetailsforRefund.Where(t => t.DG_Orders_Number == OrderNo);
                    if (item != null)
                        return item.ToList();
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the order details.
        /// </summary>
        /// <param name="OrderNo">The order no.</param>
        /// <returns></returns>
        public List<vw_GetOrderDetails> GetOrderDetails(string OrderNo)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetOrderDetails.Where(t => t.DG_Orders_Number == OrderNo);
                    if (item != null)
                        return item.ToList();
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="OrderNo">The order no.</param>
        /// <returns></returns>
        public DG_Orders GetOrder(string OrderNo)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Orders.Where(t => t.DG_Orders_Number == OrderNo);
                    return item.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the order refunded amount.
        /// </summary>
        /// <param name="Orderid">The orderid.</param>
        /// <returns></returns>
        public double GetOrderRefundedAmount(int Orderid)
        {
            double ItemAmount = 0;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Refund.Where(t => t.DG_OrderId == Orderid).FirstOrDefault();
                    if (item != null)
                    {
                        ItemAmount = Convert.ToDouble(item.RefundAmount);
                    }
                    else
                    {
                        ItemAmount = 0;
                    }
                    return ItemAmount;

                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Gets the line item details.
        /// </summary>
        /// <param name="LineItemId">The line item unique identifier.</param>
        /// <returns></returns>
        public DG_Orders_Details GetLineItemDetails(int LineItemId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Orders_Details.Where(t => t.DG_Orders_LineItems_pkey == LineItemId);
                    return item.FirstOrDefault();
                }
            }
            catch
                (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Sets the refund master data.
        /// </summary>
        /// <param name="OrderId">The order unique identifier.</param>
        /// <param name="RefundAmount">The refund amount.</param>
        /// <param name="RefundDate">The refund date.</param>
        /// <param name="UserId">The user unique identifier.</param>
        /// <returns></returns>
        public int SetRefundMasterData(int OrderId, decimal RefundAmount, DateTime RefundDate, int UserId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);



                    var item = context.DG_Refund.Where(t => t.DG_OrderId == OrderId).FirstOrDefault();

                    if (item != null)
                    {
                        var Details = context.DG_RefundDetails.Where(t => t.DG_RefundMaster_ID == item.DG_RefundId).ToList();
                        foreach (var rdetail in Details)
                        {
                            context.DG_RefundDetails.DeleteObject(rdetail);
                        }
                        context.DG_Refund.DeleteObject(item);
                        context.SaveChanges();
                    }


                    DG_Refund _objmaster = new DG_Refund();
                    _objmaster.DG_OrderId = OrderId;
                    _objmaster.RefundAmount = decimal.Round(RefundAmount, 3, MidpointRounding.ToEven);
                    _objmaster.RefundDate = RefundDate;
                    _objmaster.UserId = UserId;
                    context.DG_Refund.AddObject(_objmaster);
                    context.SaveChanges();
                    return _objmaster.DG_RefundId;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Sets the refund details data.
        /// </summary>
        /// <param name="DG_LineItemId">The command g_ line item unique identifier.</param>
        /// <param name="DG_RefundMaster_ID">The command g_ refund master_ unique identifier.</param>
        /// <param name="PhotoId">The photo unique identifier.</param>
        /// <param name="refundprice">The refundprice.</param>
        /// <returns></returns>
        public bool SetRefundDetailsData(int DG_LineItemId, int DG_RefundMaster_ID, string PhotoId, decimal? refundprice, string reason)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_RefundDetails _objDetail = new DG_RefundDetails();
                    _objDetail.DG_LineItemId = DG_LineItemId;
                    _objDetail.DG_RefundMaster_ID = DG_RefundMaster_ID;
                    _objDetail.RefundPhotoId = PhotoId;
                    _objDetail.Refunded_Amount = decimal.Round(Convert.ToDecimal(refundprice), 3, MidpointRounding.ToEven);

                    _objDetail.RefundReason = reason;

                    context.DG_RefundDetails.AddObject(_objDetail);
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SetcashBoxReason(DateTime createddate, int userid, string reason)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_CashBox _objDetail = new DG_CashBox();
                    _objDetail.UserId = userid;
                    _objDetail.CreatedDate = createddate;
                    _objDetail.Reason = reason;
                    context.DG_CashBox.AddObject(_objDetail);
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the semiorder setting.
        /// </summary>
        /// <returns></returns>
        public bool DeleteSemiorderSetting()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_SemiOrder_Settings.FirstOrDefault();
                    if (item != null)
                    {
                        context.DG_SemiOrder_Settings.DeleteObject(item);
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception eX)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the order quantity for argument efund.
        /// </summary>
        /// <param name="OrderId">The order unique identifier.</param>
        /// <returns></returns>
        public Int64? GetOrderQuantityForREfund(int? OrderId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.GetOrderQuantities.Where(t => t.DG_Orders_pkey == OrderId).FirstOrDefault();
                    if (item != null)
                        return item.Quantity;
                    else
                        return 0;
                }
            }
            catch
                (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// Gets the locations.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        /// <returns></returns>
        public List<DG_Location> GetLocations(int storeId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.DG_Location.Where(t => t.DG_Store_ID == storeId && t.DG_Location_IsActive == true).ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the locationsby unique identifier.
        /// </summary>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <returns></returns>
        public DG_Location GetLocationsbyId(int LocationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    return context.DG_Location.Where(t => t.DG_Location_pkey == LocationId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the location idby user unique identifier.
        /// </summary>
        /// <param name="UserId">The user unique identifier.</param>
        /// <returns></returns>
        public DG_Users GetLocationIdbyUserId(string UserId)
        {
            try
            {
                int UserId1 = Convert.ToInt32(UserId);
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Users item = context.DG_Users.Where(t => t.DG_User_pkey == UserId1).FirstOrDefault();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return new DG_Users();
            }
        }
        /// <summary>
        /// Sets the locations.
        /// </summary>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <param name="LocationName">Name of the location.</param>
        /// <param name="StoreId">The store unique identifier.</param>
        /// <returns></returns>
        public bool SetLocations(int LocationId, string LocationName, int StoreId, string SyncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (LocationId == 0)
                    {
                        DG_Location _objmaster = new DG_Location();
                        _objmaster.DG_Location_Name = LocationName;
                        _objmaster.DG_Store_ID = StoreId;
                        _objmaster.DG_Location_IsActive = true;
                        _objmaster.SyncCode = SyncCode;
                        _objmaster.IsSynced = false;
                        context.DG_Location.AddObject(_objmaster);
                    }
                    else
                    {
                        var item = context.DG_Location.Where(t => t.DG_Location_pkey == LocationId).FirstOrDefault();
                        if (item != null)
                        {
                            item.DG_Location_Name = LocationName;
                            item.IsSynced = false;
                        }
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the locations.
        /// </summary>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <returns></returns>
        public bool DeleteLocations(int LocationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Location.Where(t => t.DG_Location_pkey == LocationId).FirstOrDefault();
                    if (item != null)
                    {
                        item.DG_Location_IsActive = false;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Sets the cancel order.
        /// </summary>
        /// <param name="OrderNo">The order no.</param>
        public bool SetCancelOrder(string OrderNo, string CancelReason)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Orders item = context.DG_Orders.ToList().Where(t => t.DG_Orders_Number == OrderNo).FirstOrDefault();
                    item.DG_Orders_Canceled = true;
                    item.DG_Orders_Canceled_Reason = CancelReason;
                    item.DG_Orders_Canceled_Date = ServerDateTime();
                    context.SaveChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;

            }
        }
        /// <summary>
        /// Sets the service path for application.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <returns></returns>
        public bool setServicePathForApplication(string directoryPath)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Services item = new DG_Services();
                    var item1 = context.DG_Services.ToList();
                    bool hasContextChanged = false;
                    if (!ServiceFound("DigiWatcher", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DG Watcher";
                        item.DG_Service_Path = directoryPath + "\\DigiWatcher.exe";
                        item.DG_Sevice_Name = "DigiWatcher";
                        item.IsInterface = true;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiPrintingConsole", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DG Photo Printing Desktop Tool";
                        item.DG_Service_Path = directoryPath + "\\DigiPrintingConsole.exe";
                        item.DG_Sevice_Name = "DigiPrintingConsole";
                        item.IsInterface = true;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiWifiImageProcessing", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DG Image Processing Service";
                        item.DG_Service_Path = directoryPath + "\\DigiWifiImageProcessing.exe";
                        item.DG_Sevice_Name = "DigiWifiImageProcessing";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    //if (!ServiceFound("DigiSyncService", item1))
                    //{
                    //    item = new DG_Services();
                    //    item.DG_Service_Display_Name = "DG Sync Service";
                    //    item.DG_Service_Path = directoryPath + "\\DigiSync.exe";
                    //    item.DG_Sevice_Name = "DigiSyncService";
                    //    item.IsInterface = false;
                    //    context.DG_Services.AddObject(item);
                    //    hasContextChanged = true;
                    //}
                    if (!ServiceFound("DigiEmailService", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DG Email Service";
                        item.DG_Service_Path = directoryPath + "\\EmailService.exe";
                        item.DG_Sevice_Name = "DigiEmailService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiBackupService", item1))
                    {
                        //Added towards including this service in system health monitor
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DigiBackupService";
                        item.DG_Service_Path = directoryPath + "\\DigiBackupService.exe";
                        item.DG_Sevice_Name = "DigiBackupService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("SocialMediaService", item1))
                    {
                        //Social Media File upload service
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "SocialMediaService";
                        item.DG_Service_Path = directoryPath + "\\SocialMediaService.exe";
                        item.DG_Sevice_Name = "SocialMediaService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiphotoDataSyncService", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DigiphotoDataSyncService";
                        item.DG_Service_Path = directoryPath + "\\DataSyncWinService.exe";
                        item.DG_Sevice_Name = "DigiphotoDataSyncService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiRFIDAssociationService", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DigiRFIDAssociationService";
                        item.DG_Service_Path = directoryPath + "\\DigiRFIDAssociationService.exe";
                        item.DG_Sevice_Name = "DigiRFIDAssociationService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (!ServiceFound("DigiRfidService", item1))
                    {
                        item = new DG_Services();
                        item.DG_Service_Display_Name = "DigiRfidService";
                        item.DG_Service_Path = directoryPath + "\\DigiRfidService.exe";
                        item.DG_Sevice_Name = "DigiRfidService";
                        item.IsInterface = false;
                        context.DG_Services.AddObject(item);
                        hasContextChanged = true;
                    }
                    if (hasContextChanged)
                        context.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Check if service already exists in the list.
        /// </summary>
        /// <param name="serviceName">Service Name</param>
        /// <param name="ServiceList">List of already existing items</param>
        /// <returns></returns>
        private bool ServiceFound(string serviceName, List<DG_Services> ServiceList)
        {
            bool found = false;
            foreach (var ServiceItem in ServiceList)
            {
                if (ServiceItem.DG_Sevice_Name == serviceName)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }

        /// <summary>
        /// Gets the store data.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        /// <returns></returns>
        public DG_Store GetStoreData(int storeId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Store.Where(t => t.DG_Store_pkey == storeId).FirstOrDefault();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Sets the store data.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        /// <param name="storeCode">The store code.</param>
        /// <param name="serverIp">The server ip.</param>
        /// <param name="serverUserName">Name of the server user.</param>
        /// <param name="serverPassword">The server password.</param>
        /// <param name="fromtime">The fromtime.</param>
        /// <param name="totime">The totime.</param>
        /// <returns></returns>
        public bool SetStoreData(int storeId, string storeCode, string serverIp, string serverUserName, string serverPassword, string fromtime, string totime)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_Store item = context.DG_Store.ToList().Where(t => t.DG_Store_pkey == storeId).FirstOrDefault();
                    item.DG_CentralServerIP = serverIp;
                    item.DG_CenetralServerUName = serverUserName;
                    item.DG_CenetralServerPassword = serverPassword;
                    item.DG_StoreCode = new Guid(storeCode);
                    item.DG_PreferredTimeToSyncFrom = fromtime.ToDecimal();
                    item.DG_PreferredTimeToSyncTo = totime.ToDecimal();
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool GetServerStatus(string serverIP, string SqlUsername, string SqlPassword)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var result = context.GetConnectionStatus(serverIP, SqlUsername, SqlPassword).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.constatus == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Sets the data automatic central server.
        /// </summary>
        /// <param name="serverIP">The server ip.</param>
        /// <param name="SqlUsername">The SQL username.</param>
        /// <param name="SqlPassword">The SQL password.</param>
        /// <param name="storeName">Name of the store.</param>
        /// <param name="applicationId">The application unique identifier.</param>
        /// <returns></returns>
        public bool SetDataToCentralServer(string serverIP, string SqlUsername, string SqlPassword, string storeName, string applicationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var result = context.SetDataToCentralServer(serverIP, SqlUsername, applicationId, SqlPassword, storeName).ToList();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Sets the trip camera setting.
        /// </summary>
        /// <param name="DG_RideCamera_Id">The command g_ ride camera_ unique identifier.</param>
        /// <param name="AcquisitionFrameCount">The acquisition frame count.</param>
        /// <param name="AcquisitionFrameRateAbs">The acquisition frame rate abs.</param>
        /// <param name="AcquisitionFrameRateLimit">The acquisition frame rate limit.</param>
        /// <param name="AcquisitionMode">The acquisition mode.</param>
        /// <param name="BalanceRatioAbs">The balance ratio abs.</param>
        /// <param name="BalanceRatioSelector">The balance ratio selector.</param>
        /// <param name="BalanceWhiteAuto">The balance white automatic.</param>
        /// <param name="BalanceWhiteAutoAdjustTol">The balance white automatic adjust tol.</param>
        /// <param name="BalanceWhiteAutoRate">The balance white automatic rate.</param>
        /// <param name="EventSelector">The event selector.</param>
        /// <param name="EventsEnable1">The events enable1.</param>
        /// <param name="ExposureAuto">The exposure automatic.</param>
        /// <param name="ExposureAutoAdjustTol">The exposure automatic adjust tol.</param>
        /// <param name="ExposureAutoAlg">The exposure automatic alg.</param>
        /// <param name="ExposureAutoMax">The exposure automatic maximum.</param>
        /// <param name="ExposureAutoMin">The exposure automatic minimum.</param>
        /// <param name="ExposureAutoOutliers">The exposure automatic outliers.</param>
        /// <param name="ExposureAutoRate">The exposure automatic rate.</param>
        /// <param name="ExposureAutoTarget">The exposure automatic target.</param>
        /// <param name="ExposureTimeAbs">The exposure time abs.</param>
        /// <param name="GainAuto">The gain automatic.</param>
        /// <param name="GainAutoAdjustTol">The gain automatic adjust tol.</param>
        /// <param name="GainAutoMax">The gain automatic maximum.</param>
        /// <param name="GainAutoMin">The gain automatic minimum.</param>
        /// <param name="GainAutoOutliers">The gain automatic outliers.</param>
        /// <param name="GainAutoRate">The gain automatic rate.</param>
        /// <param name="GainAutoTarget">The gain automatic target.</param>
        /// <param name="GainRaw">The gain raw.</param>
        /// <param name="GainSelector">The gain selector.</param>
        /// <param name="StrobeDelay">The strobe delay.</param>
        /// <param name="StrobeDuration">Duration of the strobe.</param>
        /// <param name="StrobeDurationMode">The strobe duration mode.</param>
        /// <param name="StrobeSource">The strobe source.</param>
        /// <param name="SyncInGlitchFilter">The asynchronous information glitch filter.</param>
        /// <param name="SyncInLevels">The asynchronous information levels.</param>
        /// <param name="SyncInSelector">The asynchronous information selector.</param>
        /// <param name="SyncOutLevels">The asynchronous out levels.</param>
        /// <param name="SyncOutPolarity">The asynchronous out polarity.</param>
        /// <param name="SyncOutSelector">The asynchronous out selector.</param>
        /// <param name="SyncOutSource">The asynchronous out source.</param>
        /// <param name="TriggerActivation">The trigger activation.</param>
        /// <param name="TriggerDelayAbs">The trigger delay abs.</param>
        /// <param name="TriggerMode">The trigger mode.</param>
        /// <param name="TriggerOverlap">The trigger overlap.</param>
        /// <param name="TriggerSelector">The trigger selector.</param>
        /// <param name="TriggerSource">The trigger source.</param>
        /// <param name="UserSetDefaultSelector">The user set default selector.</param>
        /// <param name="UserSetSelector">The user set selector.</param>
        /// <param name="Width">The width.</param>
        /// <param name="WidthMax">The width maximum.</param>
        /// <param name="Height">The height.</param>
        /// <param name="HeightMax">The height maximum.</param>
        /// <param name="ImageSize">Size of the image.</param>
        /// <returns></returns>
        public bool SetTripCameraSetting(string DG_RideCamera_Id, string AcquisitionFrameCount, string AcquisitionFrameRateAbs, string AcquisitionFrameRateLimit,
                                         string AcquisitionMode, string BalanceRatioAbs, string BalanceRatioSelector, string BalanceWhiteAuto, string BalanceWhiteAutoAdjustTol, string BalanceWhiteAutoRate,
                                         string EventSelector, string EventsEnable1, string ExposureAuto, string ExposureAutoAdjustTol, string ExposureAutoAlg, string ExposureAutoMax, string ExposureAutoMin,
                                         string ExposureAutoOutliers, string ExposureAutoRate, string ExposureAutoTarget, string ExposureTimeAbs, string GainAuto, string GainAutoAdjustTol, string GainAutoMax,
                                         string GainAutoMin, string GainAutoOutliers, string GainAutoRate, string GainAutoTarget, string GainRaw, string GainSelector, string StrobeDelay, string StrobeDuration,
                                         string StrobeDurationMode, string StrobeSource, string SyncInGlitchFilter, string SyncInLevels, string SyncInSelector, string SyncOutLevels, string SyncOutPolarity,
                                         string SyncOutSelector, string SyncOutSource, string TriggerActivation, string TriggerDelayAbs, string TriggerMode, string TriggerOverlap, string TriggerSelector,
                                         string TriggerSource, string UserSetDefaultSelector, string UserSetSelector, string Width, string WidthMax,
                                         string Height, string HeightMax, string ImageSize)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_RideCameraConfiguration.Where(t => t.DG_RideCamera_Id == DG_RideCamera_Id).FirstOrDefault();
                    if (item != null)
                    {
                        item.AcquisitionFrameCount = AcquisitionFrameCount;
                        item.AcquisitionFrameRateAbs = AcquisitionFrameRateAbs;
                        item.AcquisitionFrameRateLimit = AcquisitionFrameRateLimit;
                        item.AcquisitionMode = AcquisitionMode;
                        item.BalanceRatioAbs = BalanceRatioAbs;
                        item.BalanceRatioSelector = BalanceRatioSelector;
                        item.BalanceWhiteAuto = BalanceWhiteAuto;
                        item.BalanceWhiteAutoAdjustTol = BalanceWhiteAutoAdjustTol;
                        item.BalanceWhiteAutoRate = BalanceWhiteAutoRate;
                        item.EventSelector = EventSelector;
                        item.EventsEnable1 = EventsEnable1;
                        item.ExposureAuto = ExposureAuto;
                        item.ExposureAutoAdjustTol = ExposureAutoAdjustTol;
                        item.ExposureAutoAlg = ExposureAutoAlg;
                        item.ExposureAutoMax = ExposureAutoMax;
                        item.ExposureAutoMin = ExposureAutoMin;
                        item.ExposureAutoOutliers = ExposureAutoOutliers;
                        item.ExposureAutoRate = ExposureAutoRate;
                        item.ExposureAutoTarget = ExposureAutoTarget;
                        item.ExposureTimeAbs = ExposureTimeAbs;
                        item.GainAuto = GainAuto;
                        item.GainAutoAdjustTol = GainAutoAdjustTol;
                        item.GainAutoMax = GainAutoMax;
                        item.GainAutoMin = GainAutoMin;
                        item.GainAutoOutliers = GainAutoOutliers;
                        item.GainAutoRate = GainAutoRate;
                        item.GainAutoTarget = GainAutoTarget;
                        item.GainRaw = GainRaw;
                        item.GainSelector = GainSelector;
                        item.StrobeDelay = StrobeDelay;
                        item.StrobeDuration = StrobeDuration;
                        item.StrobeDurationMode = StrobeDurationMode;
                        item.StrobeSource = StrobeSource;
                        item.SyncInGlitchFilter = SyncInGlitchFilter;
                        item.SyncInLevels = SyncInLevels;
                        item.SyncInSelector = SyncInSelector;
                        item.SyncOutLevels = SyncOutLevels;
                        item.SyncOutPolarity = SyncOutPolarity;
                        item.SyncOutSelector = SyncOutSelector;
                        item.SyncOutSource = SyncOutSource;
                        item.TriggerActivation = TriggerActivation;
                        item.TriggerDelayAbs = TriggerDelayAbs;
                        item.TriggerMode = TriggerMode;
                        item.TriggerOverlap = TriggerOverlap;
                        item.TriggerSelector = TriggerSelector;
                        item.TriggerSource = TriggerSource;
                        item.UserSetDefaultSelector = UserSetDefaultSelector;
                        item.UserSetSelector = UserSetSelector;
                        item.Width = Width;
                        item.WidthMax = WidthMax;
                        item.Height = Height;
                        item.HeightMax = HeightMax;
                        item.ImageSize = ImageSize;
                    }
                    else
                    {
                        DG_RideCameraConfiguration _objRide = new DG_RideCameraConfiguration();
                        _objRide.AcquisitionFrameCount = AcquisitionFrameCount;
                        _objRide.DG_RideCamera_Id = DG_RideCamera_Id;
                        _objRide.AcquisitionFrameRateAbs = AcquisitionFrameRateAbs;
                        _objRide.AcquisitionFrameRateLimit = AcquisitionFrameRateLimit;
                        _objRide.AcquisitionMode = AcquisitionMode;
                        _objRide.BalanceRatioAbs = BalanceRatioAbs;
                        _objRide.BalanceRatioSelector = BalanceRatioSelector;
                        _objRide.BalanceWhiteAuto = BalanceWhiteAuto;
                        _objRide.BalanceWhiteAutoAdjustTol = BalanceWhiteAutoAdjustTol;
                        _objRide.BalanceWhiteAutoRate = BalanceWhiteAutoRate;
                        _objRide.EventSelector = EventSelector;
                        _objRide.EventsEnable1 = EventsEnable1;
                        _objRide.ExposureAuto = ExposureAuto;
                        _objRide.ExposureAutoAdjustTol = ExposureAutoAdjustTol;
                        _objRide.ExposureAutoAlg = ExposureAutoAlg;
                        _objRide.ExposureAutoMax = ExposureAutoMax;
                        _objRide.ExposureAutoMin = ExposureAutoMin;
                        _objRide.ExposureAutoOutliers = ExposureAutoOutliers;
                        _objRide.ExposureAutoRate = ExposureAutoRate;
                        _objRide.ExposureAutoTarget = ExposureAutoTarget;
                        _objRide.ExposureTimeAbs = ExposureTimeAbs;
                        _objRide.GainAuto = GainAuto;
                        _objRide.GainAutoAdjustTol = GainAutoAdjustTol;
                        _objRide.GainAutoMax = GainAutoMax;
                        _objRide.GainAutoMin = GainAutoMin;
                        _objRide.GainAutoOutliers = GainAutoOutliers;
                        _objRide.GainAutoRate = GainAutoRate;
                        _objRide.GainAutoTarget = GainAutoTarget;
                        _objRide.GainRaw = GainRaw;
                        _objRide.GainSelector = GainSelector;
                        _objRide.StrobeDelay = StrobeDelay;
                        _objRide.StrobeDuration = StrobeDuration;
                        _objRide.StrobeDurationMode = StrobeDurationMode;
                        _objRide.StrobeSource = StrobeSource;
                        _objRide.SyncInGlitchFilter = SyncInGlitchFilter;
                        _objRide.SyncInLevels = SyncInLevels;
                        _objRide.SyncInSelector = SyncInSelector;
                        _objRide.SyncOutLevels = SyncOutLevels;
                        _objRide.SyncOutPolarity = SyncOutPolarity;
                        _objRide.SyncOutSelector = SyncOutSelector;
                        _objRide.SyncOutSource = SyncOutSource;
                        _objRide.TriggerActivation = TriggerActivation;
                        _objRide.TriggerDelayAbs = TriggerDelayAbs;
                        _objRide.TriggerMode = TriggerMode;
                        _objRide.TriggerOverlap = TriggerOverlap;
                        _objRide.TriggerSelector = TriggerSelector;
                        _objRide.TriggerSource = TriggerSource;
                        _objRide.UserSetDefaultSelector = UserSetDefaultSelector;
                        _objRide.UserSetSelector = UserSetSelector;
                        _objRide.Width = Width;
                        _objRide.WidthMax = WidthMax;
                        _objRide.Height = Height;
                        _objRide.HeightMax = HeightMax;
                        _objRide.ImageSize = ImageSize;
                        context.DG_RideCameraConfiguration.AddObject(_objRide);
                    }
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the ride camera setting.
        /// </summary>
        /// <param name="CameraID">The camera unique identifier.</param>
        /// <returns></returns>
        ////public DataRow GetRideCameraSetting(string CameraID)
        ////{
        ////    try
        ////    {
        ////        using (EntityConnection connection = new EntityConnection(connectionstring))
        ////        {
        ////            DigiphotoEntities context = new DigiphotoEntities(connection);
        ////            var item = context.DG_RideCameraConfiguration.Where(t => t.DG_RideCamera_Id == CameraID).ToList();
        ////            if (item != null)
        ////                return DataTableFromIEnumerable(item).Rows[0];
        ////            else
        ////                return null;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return null;
        ////    }
        ////}
        /// <summary>
        /// Datas the table from attribute enumerable.
        /// </summary>
        /// <param name="ien">The ien.</param>
        /// <returns></returns>
        private DataTable DataTableFromIEnumerable(IEnumerable ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {
                        Type pt = pi.PropertyType;
                        if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                            pt = Nullable.GetUnderlyingType(pt);
                        dt.Columns.Add(pi.Name, pt);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        /// <summary>
        /// Automatics the data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public DataTable ToDataTable<T>(IList<T> data)// T is any generic type
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        /// <summary>
        /// Gets the calculate ast asynchronous date.
        /// </summary>
        /// <returns></returns>
        public string GetLAstSyncDate()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_SyncStatus.ToList().OrderByDescending(t => t.DG_Sync_Date).Take(1);
                    if (item != null)
                        return item.FirstOrDefault().DG_Sync_Date.ToString();
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        /// <summary>
        /// Gets the associated printersfor print.
        /// </summary>
        /// <param name="ProductType">Type of the product.</param>
        /// <param name="SubStoreID">The sub store unique identifier.</param>
        /// <returns></returns>
        public List<DG_AssociatedPrinters> GetAssociatedPrintersforPrint(int? ProductType, int SubStoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objPrinters = context.DG_AssociatedPrinters.Where(t => t.DG_AssociatedPrinters_ProductType_ID == ProductType
                        && t.DG_AssociatedPrinters_IsActive == true && t.DG_AssociatedPrinters_SubStoreID == SubStoreID).ToList();
                    return _objPrinters;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        /// <summary>
        /// Sets the sub store details.
        /// </summary>
        /// <param name="SubstoreName">Name of the substore.</param>
        /// <param name="SubStoreDescription">The sub store description.</param>
        /// <param name="Substore_ID">The substore_ unique identifier.</param>
        /// <returns></returns>
        public bool SetSubStoreDetails(string SubstoreName, string SubStoreDescription, int Substore_ID, string SyncCode)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_SubStores.Where(t => t.DG_SubStore_pkey == Substore_ID).FirstOrDefault();
                    if (item != null)
                    {
                        item.DG_SubStore_Name = SubstoreName;
                        item.DG_SubStore_Description = SubStoreDescription;
                        item.IsSynced = false;
                    }
                    else
                    {
                        DG_SubStores _objnew = new DG_SubStores();
                        _objnew.DG_SubStore_Name = SubstoreName;
                        _objnew.DG_SubStore_Description = SubStoreDescription;
                        _objnew.DG_SubStore_IsActive = true;
                        _objnew.SyncCode = SyncCode;
                        _objnew.IsSynced = false;
                        context.DG_SubStores.AddObject(_objnew);
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the substore data.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetSubStoreData> GetSubstoreData()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetSubStoreData.ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the substore data.
        /// </summary>
        /// <param name="subStoreId">The sub store unique identifier.</param>
        /// <returns></returns>
        public DG_SubStores GetSubstoreData(int subStoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_SubStores.Where(t => t.DG_SubStore_pkey == subStoreId && t.DG_SubStore_IsActive == true).FirstOrDefault();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the substore attribute dby location unique identifier.
        /// </summary>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <returns></returns>
        public int GetSubstoreIDbyLocationId(int LocationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objPrinters = context.DG_SubStore_Locations.Where(t => t.DG_Location_ID == LocationId).FirstOrDefault();
                    return _objPrinters.DG_SubStore_ID;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public List<vw_GetSubStoreLocations> GetSubStoreLocations(int substoreId)
        {
            try
            {
                List<vw_GetSubStoreLocations> _lstmain = new List<vw_GetSubStoreLocations>();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetSubStoreLocations.Where(t => t.DG_SubStore_ID == substoreId).ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the selected locations substore.
        /// </summary>
        /// <param name="SubstoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetSubStoreLocations> GetSelectedLocationsSubstore(int SubstoreId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objsubstorelocationsList = context.vw_GetSubStoreLocations.Where(t => t.DG_SubStore_ID == SubstoreId).ToList();
                if (_objsubstorelocationsList.Count != 0)
                {
                    return _objsubstorelocationsList;
                }
                else return null;
            }
        }
        /// <summary>
        /// Gets the available locations substore.
        /// </summary>
        /// <param name="SubstoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public List<vw_GetListAvailableLocations> GetAvailableLocationsSubstore(int SubstoreId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objsubstorelocationsList = context.vw_GetListAvailableLocations.ToList();
                if (_objsubstorelocationsList.Count != 0)
                {
                    return _objsubstorelocationsList;
                }
                else return null;
            }
        }
        /// <summary>
        /// Gets the available ride cameras.
        /// </summary>
        /// <returns></returns>
        public List<vw_GetRideCamera> GetAvailableRideCameras()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objridecamera = context.vw_GetRideCamera.ToList();
                return _objridecamera;

            }
        }

        /// <summary>
        /// Sets the sub store locations details.
        /// </summary>
        /// <param name="Substore_ID">The substore_ unique identifier.</param>
        /// <param name="LocationId">The location unique identifier.</param>
        /// <returns></returns>
        public bool SetSubStoreLocationsDetails(int Substore_ID, int LocationId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    DG_SubStore_Locations _objnew = new DG_SubStore_Locations();
                    _objnew.DG_Location_ID = LocationId;
                    _objnew.DG_SubStore_ID = Substore_ID;
                    context.DG_SubStore_Locations.AddObject(_objnew);

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes the sub store locations.
        /// </summary>
        /// <param name="SubstoreID">The substore unique identifier.</param>
        /// <returns></returns>
        public bool DeleteSubStoreLocations(int SubstoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var items = context.DG_SubStore_Locations.Where(t => t.DG_SubStore_ID == SubstoreID).ToList();
                    foreach (var item in items)
                    {
                        context.DG_SubStore_Locations.DeleteObject(item);
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #region Methods For EmailSettings

        //public bool SetEmailSettings(string From, string subject, string body, string servername, string serverport, bool defaultcredential, string username, string password, bool enablessl, string mailbcc, string sampleImgPath, int displayDuration, int ScreensaverDelay, bool IsScreenSaverActive, int SsId)
        public bool SetEmailSettings(string From, string subject, string body, string servername, string serverport, bool defaultcredential, string username, string password, bool enablessl, string mailbcc, int SsId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    DG_EmailSettings item = new DG_EmailSettings();
                    item.DG_MailSendFrom = From;
                    item.DG_MailSubject = subject;
                    item.DG_MailBody = body;
                    item.DG_SmtpServername = servername;
                    item.DG_SmtpServerport = serverport;
                    item.DG_SmtpUserDefaultCredentials = defaultcredential;
                    item.DG_SmtpServerUsername = username;
                    item.DG_SmtpServerPassword = password;
                    item.DG_SmtpServerEnableSSL = enablessl;
                    item.DG_MailBCC = mailbcc;
                    context.DG_EmailSettings.AddObject(item);
                    context.SaveChanges();
                    //For fetching screen saver settings
                    /*
                    var _objConfig = context.DG_Configuration.Where(t => t.DG_Substore_Id == SsId).FirstOrDefault();
                    if (_objConfig != null)
                    {
                        _objConfig.EK_SampleImagePath = sampleImgPath;
                        _objConfig.EK_DisplayDuration = displayDuration;
                        _objConfig.EK_ScreenStartTime = ScreensaverDelay;
                        _objConfig.EK_IsScreenSaverActive = IsScreenSaverActive;

                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    */
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool TruncateEmailSettingsTable()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.ExecuteStoreCommand("Truncate Table DG_EmailSettings");
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DG_EmailSettings GetEmailSettingsDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_EmailSettings.ToList().FirstOrDefault();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool DeleteSubstore(int SubStoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_SubStores.Where(t => t.DG_SubStore_pkey == SubStoreId).FirstOrDefault();
                    if (item != null)
                    {
                        context.DG_SubStores.DeleteObject(item);
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SetOrderDetailsForReprint(int LineItemId, int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Orders_Details.Where(t => t.DG_Orders_LineItems_pkey == LineItemId).FirstOrDefault();
                    if (item != null)
                    {
                        item.DG_Order_SubStoreId = substoreID;
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string GetSubstoreNameById(int SubstoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetSubStoreData.Where(t => t.DG_SubStore_pkey == SubstoreId).FirstOrDefault();
                    if (item != null)
                        return item.DG_SubStore_Name;
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<vw_GetSubStoreData> GetAllSubstoreName()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetSubStoreData.ToList();
                    if (item != null)
                        return item;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion


        /// <summary>
        /// Gets the name of the associated printers.
        /// </summary>
        /// <param name="substoreID">The substore unique identifier.</param>
        /// <returns></returns>
        public List<DG_AssociatedPrinters> GetAssociatedPrintersName(int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_AssociatedPrinters.ToList().Where(t => t.DG_AssociatedPrinters_SubStoreID == substoreID).ToList();
                    return _objconfigdata;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the name of the receipt printer.
        /// </summary>
        /// <param name="substoreId">The substore unique identifier.</param>
        /// <returns></returns>
        public string GetReceiptPrinterName(int? substoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objconfigdata = context.DG_ReceiptPrinter.ToList().Where(t => t.DG_SubStore_Id == substoreId).ToList();
                    return _objconfigdata.FirstOrDefault().DG_Receipt_PrinterName;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        /// <summary>
        /// Saves the group data.
        /// </summary>
        /// <param name="_objPhotoDetails">The _obj photo details.</param>
        /// <param name="groupname">The groupname.</param>
        public void SaveGroupData(Dictionary<string, string> _objPhotoDetails, string groupname, int SubStoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    foreach (var existitem in context.DG_PhotoGroup.Where(t => t.DG_Group_Name == groupname).ToList())
                    {
                        existitem.DG_SubstoreId = SubStoreId;
                    }
                    foreach (var item in _objPhotoDetails)
                    {
                        int myphotoid = item.Key.ToInt32();
                        var searchitem = context.DG_PhotoGroup.Where(t => t.DG_Photo_ID == myphotoid && t.DG_Group_Name == groupname).FirstOrDefault();
                        if (searchitem == null)
                        {
                            DG_PhotoGroup _objnew = new DG_PhotoGroup();
                            _objnew.DG_Group_Name = groupname;
                            _objnew.DG_Photo_ID = item.Key.ToInt32();
                            _objnew.DG_Photo_RFID = item.Value;
                            _objnew.DG_CreatedDate = ServerDateTime();
                            _objnew.DG_SubstoreId = SubStoreId;
                            context.DG_PhotoGroup.AddObject(_objnew);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public DG_PhotoGroup GetGroupName(string groupname)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);

                    var searchitem = context.DG_PhotoGroup.Where(t => t.DG_Group_Name == groupname).FirstOrDefault();

                    return searchitem;

                }


            }
            catch (Exception ex)
            {
                return new DG_PhotoGroup();
            }
        }


        public List<vw_GetGroupedImages> GetSavedGroupImages(string GroupName)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.vw_GetGroupedImages.Where(t => t.DG_Group_Name == GroupName).ToList();
                    return item;
                }
            }
            catch (Exception ex)
            {
                return new List<vw_GetGroupedImages>();
            }
        }

        #region Methods for Grouping
        public bool TruncatePhotoGroupTable(int days, int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.ExecuteStoreCommand("delete from DG_PhotoGroup where DG_SubstoreId=" + substoreID.ToString());
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool TruncatePhotoGroupTablefordate(int days, int substoreID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.ExecuteStoreCommand("delete from DG_PhotoGroup where DG_CreatedDate < DATEADD(day," + -days + ", GETDATE()) and DG_SubstoreId=" + substoreID.ToString());
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeletePhotoGroup(string groupname)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.ExecuteStoreCommand("Delete from DG_PhotoGroup where DG_Group_Name='" + groupname + "'");
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        //public enum ValueTypeGroupEnum
        //{
        //    CashBoxOpenReason = 1,
        //    RefundReason = 2,
        //    CancelReason = 3,
        //    BarcodeType = 4,
        //    ScanType = 5,
        //    MediaType = 6,
        //    ScanningDeviceType = 7
        //}

        public bool SaveBorderFor4Images(bool chk4Large, bool chkUniq4, bool chk4small, bool chk3by3)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objConfig = context.DG_Orders_ProductType.Where(t => t.DG_Orders_ProductType_pkey == 3 || t.DG_Orders_ProductType_pkey == 4 || t.DG_Orders_ProductType_pkey == 5 || t.DG_Orders_ProductType_pkey == 98).ToList();
                    if (_objConfig.Count > 0)
                    {
                        foreach (var item in _objConfig)
                        {
                            if (item.DG_Orders_ProductType_pkey == 3)
                                item.DG_IsBorder = chk4Large;
                            if (item.DG_Orders_ProductType_pkey == 4)
                                item.DG_IsBorder = chkUniq4;
                            if (item.DG_Orders_ProductType_pkey == 5)
                                item.DG_IsBorder = chk4small;
                            if (item.DG_Orders_ProductType_pkey == 98)
                                item.DG_IsBorder = chk3by3;
                            context.SaveChanges();
                        }
                        return true;
                    }

                    else
                        return false;


                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public List<GetPhotoToUpload_Result> GetPhotoToUpload()
        {
            List<GetPhotoToUpload_Result> _objNew = new List<GetPhotoToUpload_Result>();
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    _objNew = context.GetPhotoToUpload().ToList();
                    return _objNew;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void UpdatePostedOrder(string orderNumber, int Status)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.UpdatePostedOrder(orderNumber, Status);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GEtCountryName()
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objCountry = context.DG_Store.FirstOrDefault();
                return _objCountry.Country;
            }
        }

        public string GEtOrderDateByOrderNo(string OrderNo)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _obj = context.DG_Orders.Where(x => x.DG_Orders_Number == OrderNo).FirstOrDefault().DG_Orders_Date;
                DateTime dt = _obj.ToDateTime();
                string y = dt.ToString("ddMMyyyy");
                return y;
            }
        }

        /// <summary>
        /// Checks into the local database for any updates
        /// </summary>
        /// <param name="currVersion">#.#.#.#</param>
        /// <returns>true if update is avaliable</returns>
        public string IsUpdateAvailable(string currVersion)
        {
            string avVersion = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(currVersion))
                {
                    using (EntityConnection connection = new EntityConnection(connectionstring))
                    {
                        DigiphotoEntities context = new DigiphotoEntities(connection);
                        var temp = context.uspCheckUpdates(currVersion);

                        if (temp != null && temp.Count() > 0)
                        {
                            foreach (uspCheckUpdates_Result str in temp)
                            {
                                if (str != null)
                                    avVersion += ",";
                                avVersion += str.VERSION;
                            }
                        }
                    }
                }
            }
            catch
            {
                avVersion = string.Empty;
            }
            return avVersion;
        }



        public Hashtable GetStoreDetails()
        {
            Hashtable htStore = new Hashtable();
            string tempStoreName = ""; string tempCountryName = ""; string tempLocationName = "";
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var storename = context.DG_Store.FirstOrDefault().DG_Store_Name;
                if (storename != null)
                    tempStoreName = storename;

                var countryname = context.DG_Store.First().Country.ToString();
                if (countryname != null)
                    tempCountryName = countryname;

                int storeId = context.DG_Store.First().DG_Store_pkey;

                //var location =  context.DG_Location.Where(x => x.DG_Store_ID == storeId).FirstOrDefault();
                //if(location != null)
                //    tempLocationName = location.DG_Location_Name;

                htStore.Add("Store", tempStoreName);
                htStore.Add("Country", tempCountryName);
                //htStore.Add("Location", tempLocationName);

                return htStore;
            }
        }
        /// <summary>
        /// Used to get Image card type.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, int> GetCardTypes()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    //Dictionary<int, string> objDic = (from u in context.iMixImageCardType select new Dictionary<int, string> { u.IMIXImageCardTypeId, u.CardIdentificationDigit }).ToDictionary();
                    Dictionary<string, int> objDic = context.ValueType.Where(o => o.ValueTypeGroupId == 4).ToDictionary(k => k.Name, l => l.ValueTypeId);
                    return objDic;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Dictionary<string, int> GetScanTypes()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    //Dictionary<int, string> objDic = (from u in context.iMixImageCardType select new Dictionary<int, string> { u.IMIXImageCardTypeId, u.CardIdentificationDigit }).ToDictionary();
                    Dictionary<string, int> objDic = context.ValueType.Where(o => o.ValueTypeGroupId == 5).ToDictionary(k => k.Name, l => l.ValueTypeId);
                    return objDic;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<string, int> GetCodeValueTypes(int ValueTypeGroupId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    Dictionary<string, int> objDic = context.ValueType.Where(o => o.ValueTypeGroupId == ValueTypeGroupId && o.ValueTypeId != 405).ToDictionary(k => k.Name, l => l.ValueTypeId);
                    return objDic;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCardCode(int ImageIdentificationType)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    string CardCode = string.Empty;
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var identificationType = context.iMixImageCardType.Where(o => o.ImageIdentificationType == ImageIdentificationType).FirstOrDefault();
                    if (identificationType != null)
                        CardCode = identificationType.CardIdentificationDigit;
                    return CardCode;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool SaveUpdateNewConfig(List<iMIXConfigurationValue> dt)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    foreach (iMIXConfigurationValue val in dt)
                    {
                        iMIXConfigurationValue existing = context.iMIXConfigurationValue.Where(e => e.SubstoreId == val.SubstoreId && e.IMIXConfigurationMasterId == val.IMIXConfigurationMasterId).FirstOrDefault();
                        if (existing != null)
                        {
                            existing.ConfigurationValue = val.ConfigurationValue;
                            existing.IsSynced = false;
                        }
                        else
                        {
                            val.IsSynced = false;
                            context.iMIXConfigurationValue.AddObject(val);
                        }
                    }
                    context.SaveChanges();
                    ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
                    oCache.RemoveFromCache();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<iMIXConfigurationValue> GetNewConfigValues(int SubstoreId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.iMIXConfigurationValue.Where(t => t.SubstoreId == SubstoreId).ToList();
                    return item;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///Checks if the Code(QR/Bar/unique) already exists.
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="CodeType"></param>
        /// <returns></returns>
        public bool IsUniqueCodeExists(string QRCode, bool IsAnonymousQrCodeEnabled)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (IsAnonymousQrCodeEnabled)
                    {
                        var obj = (from a in context.iMixImageAssociation
                                   where a.CardUniqueIdentifier == QRCode && a.IMIXCardTypeId == 0
                                   select a).FirstOrDefault();
                        if (obj != null)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        string CardDigit = QRCode.Length >= 4 ? QRCode.Substring(0, 4) : string.Empty;
                        if (CardDigit == "0000")
                            QRCode = QRCode.Substring(4);
                        var obj = (from a in context.iMixImageAssociation
                                   join t in context.iMixImageCardType on
                                       a.IMIXCardTypeId equals t.IMIXImageCardTypeId
                                   where a.CardUniqueIdentifier == QRCode && t.CardIdentificationDigit == CardDigit
                                   select a).FirstOrDefault();
                        if (obj != null)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch
            {
                return false;
            }

        }



        public bool IsValidCodeType(string QRCode, int CardTypeId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    List<int> objCardTypeIds = new List<int>();
                    if (CardTypeId == 405)
                    {
                        objCardTypeIds.Add(401);
                        objCardTypeIds.Add(402);
                        objCardTypeIds.Add(405);
                    }
                    else
                    {
                        objCardTypeIds.Add(CardTypeId);
                    }

                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var obj = context.iMixImageCardType.Where(o => o.IsActive == true && string.Compare(o.CardIdentificationDigit, QRCode.Substring(0, 4), true) == 0
                        && objCardTypeIds.Contains(o.ImageIdentificationType)).FirstOrDefault();
                    if (obj != null)
                        return true;
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool IsValidPrepaidCodeType(string QRCode, int CardTypeId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    List<int> objCardTypeIds = new List<int>();
                    if (CardTypeId == 405)
                    {
                        objCardTypeIds.Add(401);
                        objCardTypeIds.Add(402);
                        objCardTypeIds.Add(405);
                    }
                    else
                    {
                        objCardTypeIds.Add(CardTypeId);
                    }

                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var obj = context.iMixImageCardType.Where(o => o.IsActive == true && o.IsPrepaid == true && string.Compare(o.CardIdentificationDigit, QRCode.Substring(0, 4), true) == 0
                        && objCardTypeIds.Contains(o.ImageIdentificationType)).FirstOrDefault();
                    if (obj != null)
                        return true;
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Returns "1" when success else error message. 
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="CodeType"></param>
        /// <param name="OverWriteStatus">Status to overrite the existing association</param>
        /// <returns></returns>
        public string AssociateImage(int CodeType, string QRCode, string PhotoIds, int OverWriteStatus, bool IsAnonymousEnabled)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    System.Data.Objects.ObjectParameter obj = new System.Data.Objects.ObjectParameter("Result", string.Empty);
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    context.AssociateImage(QRCode, PhotoIds, OverWriteStatus, IsAnonymousEnabled, obj);
                    return obj.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<GetImagesBYQRCode_Result> GetImagesBYQRCode(string QRCode, bool IsAnonymousQrCodeEnabled)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    List<GetImagesBYQRCode_Result> objImages = context.GetImagesBYQRCode(QRCode, IsAnonymousQrCodeEnabled).ToList();
                    return objImages;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Get all the orders where status is zero i.e. pending to be burned orders
        /// </summary>
        /// <param name="all">true if all records are required, false is only pending items to be shown</param>
        /// <returns>List BurnOrderInfo</returns>
        public List<BurnOrderInfo> GetPendingBurnOrders(bool all)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DateTime dt = DateTime.Now.Date;
                    List<BurnOrderInfo> BurnOrderList = new List<BurnOrderInfo>();
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (all)
                    {
                        int[] CDUSBProducts = new int[] { 35, 36, 96, 97  };
                        BurnOrderList = (from o in context.DG_Orders
                                         join od in context.DG_Orders_Details on o.DG_Orders_pkey equals od.DG_Orders_ID
                                         where (CDUSBProducts.Contains(od.DG_Orders_Details_ProductType_pkey.HasValue ? od.DG_Orders_Details_ProductType_pkey.Value : 0))
                                         && (o.DG_Orders_Date.Value.Year == dt.Year && o.DG_Orders_Date.Value.Month == dt.Month && o.DG_Orders_Date.Value.Day == dt.Day)
                                         select new BurnOrderInfo
                                         {
                                             OrderDetailId = od.DG_Orders_LineItems_pkey,
                                             OrderNumber = o.DG_Orders_Number,
                                             PhotosId = od.DG_Photos_ID,
                                             ProductType = od.DG_Orders_Details_ProductType_pkey.HasValue ? od.DG_Orders_Details_ProductType_pkey.Value : 0,
                                             Status = od.DG_Order_Status.HasValue ? od.DG_Order_Status.Value : 0
                                         }
                                              ).ToList();
                    }
                    else
                    {
                        int[] CDUSBProducts = new int[] { 35, 36, 96, 97 };
                        BurnOrderList = (from o in context.DG_Orders
                                         join od in context.DG_Orders_Details on o.DG_Orders_pkey equals od.DG_Orders_ID
                                         where (CDUSBProducts.Contains(od.DG_Orders_Details_ProductType_pkey.HasValue ? od.DG_Orders_Details_ProductType_pkey.Value : 0)
                                         && (!od.DG_Order_Status.HasValue || od.DG_Order_Status == 0)
                                         && (o.DG_Orders_Date.Value.Year == dt.Year && o.DG_Orders_Date.Value.Month == dt.Month && o.DG_Orders_Date.Value.Day == dt.Day))
                                         select new BurnOrderInfo
                                         {
                                             OrderDetailId = od.DG_Orders_LineItems_pkey,
                                             OrderNumber = o.DG_Orders_Number,
                                             PhotosId = od.DG_Photos_ID,
                                             ProductType = od.DG_Orders_Details_ProductType_pkey.HasValue ? od.DG_Orders_Details_ProductType_pkey.Value : 0,
                                             Status = od.DG_Order_Status.HasValue ? od.DG_Order_Status.Value : 0
                                         }
                                         ).ToList();
                    }


                    return BurnOrderList;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get specific order details
        /// </summary>
        /// <param name="boId">Order detail id</param>
        /// <returns>List</returns>
        public List<BurnOrderInfo> GetBODetails(int boId)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objOrd = (from o in context.DG_Orders
                                   join od in context.DG_Orders_Details on o.DG_Orders_pkey equals od.DG_Orders_ID
                                   where od.DG_Orders_LineItems_pkey == boId
                                   select new BurnOrderInfo
                                   {
                                       OrderDetailId = od.DG_Orders_LineItems_pkey,
                                       OrderNumber = o.DG_Orders_Number,
                                       PhotosId = od.DG_Photos_ID,
                                       ProductType = od.DG_Orders_Details_ProductType_pkey.HasValue ? od.DG_Orders_Details_ProductType_pkey.Value : 0,
                                       Status = od.DG_Order_Status.HasValue ? od.DG_Order_Status.Value : 0
                                   }
                                       ).ToList();
                    return _objOrd;
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Update Order Status
        /// </summary>
        /// <param name="boId">Order detail id</param>
        /// <param name="stat">status</param>
        /// <param name="procBy">User Id - Not used currently</param>
        /// <param name="dateProcessed">Not used currently</param>
        /// <returns></returns>
        public bool UpdateBurnOrderStatus(int boId, int stat, int procBy, DateTime dateProcessed)
        {
            bool succ = false;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objOrd = context.DG_Orders_Details.Where(x => x.DG_Orders_LineItems_pkey == boId).First();
                    _objOrd.DG_Order_Status = stat;
                    context.SaveChanges();
                    succ = true;
                }
                return succ;
            }
            catch
            {
                succ = false;
                return succ;
            }
        }
        /// <summary>
        /// Get image resize/compression level. Between 1-100.
        /// </summary>
        /// <param name="substoreid">User Id</param>
        /// <param name="mediaEnum">6 for Storage Media, 5 for Social Media</param>
        /// <returns></returns>
        public int GetCompressionLevel(int substoreid, long mediaEnum)
        {
            int val = 0;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objOrd = context.iMIXConfigurationValue.Where(x => x.IMIXConfigurationMasterId == mediaEnum).First();
                    val = _objOrd.ConfigurationValue.ToInt32();
                    return val;
                }
            }
            catch
            {
                return val;
            }
        }

        public bool SaveScheduledConfig(int substoreId, DateTime backupDateTime, int status)
        {
            try
            {
                BackupHistory objbackup = new BackupHistory();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.BackupHistory.Where(o => o.ScheduleDate == backupDateTime && o.SubStoreId == substoreId).FirstOrDefault();
                    if (item == null)
                    {
                        objbackup.SubStoreId = substoreId;
                        objbackup.ScheduleDate = backupDateTime.ToDateTime();
                        objbackup.Status = status;
                        context.BackupHistory.AddObject(objbackup);
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        item.SubStoreId = substoreId;
                        item.ScheduleDate = backupDateTime;
                        item.Status = status;
                        //context.BackupHistory.AddObject(item);
                        context.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<BackupHistory> GetBackupHistoryData(int SubstoreId, DateTime dfrom, DateTime dto)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.BackupHistory.Where(t => t.SubStoreId == SubstoreId).ToList();
                    return item.Where(t => t.ScheduleDate.ToDateTime() >= dfrom && t.ScheduleDate.ToDateTime() <= dto).ToList();

                }
            }
            catch
            {
                return null;
            }
        }

        public List<string> GetBackupsubstorename(int SubStoreId)
        {
            List<string> pname = null;
            try
            {

                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    pname = (from c in context.BackupHistory
                             join p in context.DG_SubStores
                             on c.SubStoreId equals p.DG_SubStore_pkey
                             where c.SubStoreId == SubStoreId
                             select p.DG_SubStore_Name).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return pname;
        }
        /// <summary>
        ///Restore DB
        /// </summary>
        /// <param name="_databasename">Restore DB name</param>
        /// <param name="backUpPath">Backup file path</param>
        /// <returns></returns>
        public void RestoreDataBase(string _databasename, string backUpPath)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objUserDetails = context.RestoreDataBase(_databasename, backUpPath);
                }
            }
            catch (Exception ex)
            { }
        }
        /// <summary>
        ///Add missing columns to Old DB 
        /// </summary>        
        /// <returns></returns>
        public void AddMissingColumnsInOldDB()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objUserDetails = context.AddMissingColumnsInOldDB();
                }
            }
            catch (Exception ex)
            { }
        }
        /// <summary>
        ///Data migration
        /// </summary>       
        /// <returns></returns>
         //public string ImportTablesFromOldDBToNew()
        //{
        //    try
        //    {
        //        using (EntityConnection connection = new EntityConnection(connectionstring))
        //        {
        //            DigiphotoEntities context = new DigiphotoEntities(connection);
        //            var _objUserDetails = context.uspImportTablesFromOldDBToNew().FirstOrDefault();

        //            return _objUserDetails.ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        public Dictionary<int, string> GetUsersPwdDetails()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    Dictionary<int, string> itemList = context.DG_Users.ToDictionary(k => k.DG_User_pkey, l => l.DG_User_Password);
                    // Dictionary<int, string> itemList = context.DG_Users.Select(p => new { p.DG_User_pkey, p.DG_User_Password}).ToDictionary();
                    return itemList;
                }

            }
            catch
            {
                return null;
            }
        }
        public bool EncryptUsersPwd(Dictionary<int, string> ltEncryptedpwd)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DG_Users objUser = new DG_Users();
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    foreach (KeyValuePair<int, string> pair in ltEncryptedpwd)
                    {
                        int UId = pair.Key;
                        var item = context.DG_Users.ToList().Where(t => t.DG_User_pkey == pair.Key).FirstOrDefault();
                        item.DG_User_Password = pair.Value;
                        context.SaveChanges();
                    }
                    return true;
                }

            }
            catch
            {
                return false;
            }
        }

        public List<string> GetQRCodeBYImages(string ImageID)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    List<string> objCode = context.GetQrOrBarCodeByPhotoID(ImageID).ToList();
                    return objCode;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public bool UpdateQRCodeWebUrl(string url)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Store.FirstOrDefault();
                    if (item != null)
                    {
                        item.DG_QRCodeWebUrl = url;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public string GetQRCodeWebUrl()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.DG_Store.FirstOrDefault();
                    return item.DG_QRCodeWebUrl;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Used to get the QRcode card limit
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int GetCardImageLimit(string code)
        {
            try
            {
                if (code.Length < 4)
                    return 0;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.iMixImageCardType.Where(o => string.Compare(o.CardIdentificationDigit, code.Substring(0, 4), true) == 0).FirstOrDefault();
                    if (item != null && item.MaxImages != null)
                        return (int)item.MaxImages;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Used to get the QRcode card limit
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int GetCardImageSold(string code)
        {
            try
            {
                int itemCount = 0;
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    List<string> lstImages = context.DG_Orders_Details.Where(o => string.Compare(o.DG_Order_ImageUniqueIdentifier, code, true) == 0).Select(j => j.DG_Photos_ID).ToList();
                    lstImages.ForEach(c => itemCount += c.Split(',').Length);
                    return itemCount;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool IsOnlineOpen(int ConfigurationMasterId, int subStoreId)
        {

            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objOrd = context.iMIXConfigurationValue.Where(x => x.SubstoreId == subStoreId && x.IMIXConfigurationMasterId == ConfigurationMasterId).FirstOrDefault();
                    if (_objOrd != null)
                        return (string.Compare(_objOrd.ConfigurationValue, "True", true) == 0 ? true : false);
                    else
                        return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public string GetOnlineConfigData(ConfigParams objConfigParams, int SubStoreId)
        {

            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var _objOrd = context.iMIXConfigurationValue.Where(x => x.SubstoreId == SubStoreId && x.IMIXConfigurationMasterId == (int)objConfigParams).FirstOrDefault();
                    if (_objOrd != null)
                        return _objOrd.ConfigurationValue;
                    else
                        return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public Dictionary<string, int> GetCardCodeTypes()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    Dictionary<string, int> objDic = new Dictionary<string, int>();
                    objDic.Add("QR/Bar Code", 405);
                    objDic.Add("Unique", 403);
                    objDic.Add("Lost", 404);
                    objDic.Add("RFID", 406);
                    return objDic;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Gets the product type list.
        /// </summary>
        /// <returns></returns>
        public List<iMixImageCardType> GetCardTypeList()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var cardList = context.iMixImageCardType.ToList();
                    return cardList;
                }
            }
            catch
            {
                return null;
            }
        }
        public bool SetCardTypeInfo(int cardId, string strCardTypeName, string strCardSeries, int strcodeType, bool? status, int maxImage, string strDescription, bool IsPrepaid, int PackageId)
        {
            try
            {
                iMixImageCardType objCard = new iMixImageCardType();
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    if (cardId == 0)
                    {
                        objCard.Name = strCardTypeName;
                        objCard.CardIdentificationDigit = strCardSeries;
                        objCard.ImageIdentificationType = strcodeType;
                        objCard.IsActive = status;
                        objCard.MaxImages = maxImage;
                        objCard.Description = strDescription;
                        objCard.CreatedOn = ServerDateTime();
                        objCard.IsPrepaid = IsPrepaid;
                        objCard.PackageId = PackageId;
                        context.iMixImageCardType.AddObject(objCard);
                        context.SaveChanges();
                        cardId = objCard.IMIXImageCardTypeId.ToInt32();
                    }
                    else
                    {
                        var item = context.iMixImageCardType.ToList().Where(t => t.IMIXImageCardTypeId == cardId).FirstOrDefault();
                        item.Name = strCardTypeName;
                        item.CardIdentificationDigit = strCardSeries;
                        item.ImageIdentificationType = strcodeType;
                        item.IsActive = status;
                        item.MaxImages = maxImage;
                        item.Description = strDescription;
                        item.IsPrepaid = IsPrepaid;
                        item.PackageId = PackageId;
                        // item.CreatedOn = DateTime.Now;
                        context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string ss = ex.ToString();
                return false;
            }
        }


        public bool ChangeCardStatus(int cardId)
        {
            iMixImageCardType objCard = new iMixImageCardType();
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var item = context.iMixImageCardType.ToList().Where(t => t.IMIXImageCardTypeId == cardId).FirstOrDefault();
                if (Convert.ToBoolean(item.IsActive))
                {
                    item.IsActive = false;
                }
                else
                {
                    item.IsActive = true;
                }

                context.SaveChanges();
                return true;
            }
        }
        public string GetCardNameFromID(int id)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.iMixImageCardType.ToList().Where(t => t.IMIXImageCardTypeId == id).FirstOrDefault();
                if (_objPhotos != null)
                {
                    return _objPhotos.Name;
                }
                else
                {
                    return null;
                }
            }
        }
        public List<CardTypeInfo> GetCardTypeListview()
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    //var cardtList = context.iMixImageCardType.ToList();
                    //return cardtList;



                    List<CardTypeInfo> cardTypeList = (from ct in context.iMixImageCardType.Where(o => o.ImageIdentificationType != 403 && o.ImageIdentificationType != 404)
                                                       select new CardTypeInfo
                                                       {
                                                           IMIXImageCardTypeId = ct.IMIXImageCardTypeId,
                                                           Name = ct.Name,
                                                           Status = (ct.IsActive.Value == true ? "Active" : "InActive"),
                                                           ImageIdentificationType = (ct.ImageIdentificationType == 401 ? "QRCode" : ct.ImageIdentificationType == 402 ? "Barcode" : ct.ImageIdentificationType == 405 ? "QRCode+Barcode" : null),
                                                           CardIdentificationDigit = ct.CardIdentificationDigit,
                                                           MaxImages = (ct.MaxImages.Value == null ? 0 : ct.MaxImages.Value)
                                                       }
                                                       ).ToList();
                    return cardTypeList;
                }
            }
            catch
            {
                return null;
            }
        }


        public List<OrderDetailedSyncStaus_Result> GetOrdersyncStatus(DateTime? FromDate, DateTime? ToDate)
        {
            //List<OrderDetailedSyncStaus_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var item = context.OrderDetailedSyncStaus(FromDate, ToDate).ToList();
                    return item;

                }

            }
            catch
            {
                return null;
            }
        }

        public bool IsCardSeriesExits(string series)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var _objPhotos = context.iMixImageCardType.ToList().Where(t => t.CardIdentificationDigit == series).FirstOrDefault();
                if (_objPhotos == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public List<GetLocationSubstoreWise_Result> GetLocationSubstoreWise(int SubstoreId)
        {
            List<GetLocationSubstoreWise_Result> result = null;
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    result = context.GetLocationSubstoreWise(SubstoreId).ToList();

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public int GetLocationIdByPhotoId(int PhotoId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);
                var Location = context.DG_Photos.ToList().Where(t => t.DG_Photos_pkey == PhotoId).FirstOrDefault().DG_Location_Id;
                int LocationId = Location == null ? 0 : Location.ToInt32();
                if (LocationId > 0)
                {
                    return LocationId;
                }
                else
                {
                    return 0;
                }
            }
        }
        public bool GetIsResetDPIRequired(int PhotoGrapherId)
        {
            using (EntityConnection connection = new EntityConnection(connectionstring))
            {
                DigiphotoEntities context = new DigiphotoEntities(connection);

                var tempVal = context.DG_CameraDetails.ToList().Where(t => t.DG_AssignTo == PhotoGrapherId && t.DG_Camera_IsDeleted == false).FirstOrDefault().Is_ChromaColor;
                bool? reqVal = tempVal != null ? tempVal : false;
                return (bool)reqVal;
            }
        }
    }



}


