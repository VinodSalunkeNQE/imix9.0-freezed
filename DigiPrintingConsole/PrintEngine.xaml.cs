﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto;
using DigiPhoto.DataLayer;
using System.IO;
using DigiPhoto.DataLayer.Model;
using System.Windows.Threading;
using System.Timers;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using ImageMagick;
using System.Xml;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPrintingConsole
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class PrintEngine : Window
    {
        public static string specproductType = string.Empty;
        bool IsGumballXml = false;
        bool speccase = false;

        //System.Timers.Timer timer;
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        string defaultEffect = @"<image brightness=""0"" contrast=""1"" Crop=""##"" colourvalue=""##"" rotate=""##""><effects sharpen=""##"" greyscale=""0"" digimagic=""0"" sepia=""0"" defog=""0"" underwater=""0"" emboss=""0"" invert=""0"" granite=""0"" hue=""##"" cartoon=""0"" /></image>";
        string defaultSpecPrintLayer = @"<photo zoomfactor=""1"" border="""" bg="""" canvasleft=""-1"" canvastop=""-1"" scalecentrex=""-1"" scalecentrey=""-1"" />";
        bool isFlush = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintEngine"/> class.
        /// </summary>
        public PrintEngine()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Printing Console Initilized.");
                InitializeComponent();
                ErrorHandler.ErrorHandler.LogFileWrite("Print Console Initilization Complete.");
                //timer = new System.Timers.Timer();
                PrintConsole();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Prints the console.
        /// </summary>
        void PrintConsole()
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Printing Console Started.");
            if (MW != null)
            {
                //  ErrorHandler.ErrorHandler.LogFileWrite("Printing Console in MW.");
            }
            else
            {
                try
                {
                    var printImagesPath = System.IO.Path.Combine(DigiPhoto.Common.LoginUser.DigiFolderPath, "\\PrintImages\\");

                    if (!Directory.Exists(printImagesPath))
                        Directory.CreateDirectory(printImagesPath);

                    Thread newWindowThread = new Thread(new ThreadStart(() =>
                    {
                        ImageDispatcher idc = new ImageDispatcher();
                        idc.Show();
                        System.Windows.Threading.Dispatcher.Run();
                    }));

                    newWindowThread.SetApartmentState(ApartmentState.STA);
                    newWindowThread.IsBackground = true;
                    newWindowThread.Start();

                    MW = new PrintManager();
                    MW.Show();

                    while (1 == 1)
                    {
                        StartEngine();
                        //MemoryManagement.FlushMemory();
                        Thread.Sleep(50);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }

        PrintManager MW;

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Printing Console Started.");

            if (MW != null)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Printing Console in MW.");
            }
            else
            {
                try
                {
                    Thread newWindowThread = new Thread(new ThreadStart(() =>
                    {
                        ImageDispatcher idc = new ImageDispatcher();
                        idc.Show();
                        System.Windows.Threading.Dispatcher.Run();
                    }));

                    newWindowThread.SetApartmentState(ApartmentState.STA);
                    newWindowThread.IsBackground = true;
                    newWindowThread.Start();

                    MW = new PrintManager();
                    MW.Show();

                    while (1 == 1)
                    {

                        StartEngine();
                        //MemoryManagement.FlushMemory();
                        Thread.Sleep(100);

                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }
        private bool IsBorderApplied(String inputXml)
        {
            try
            {
                if (string.IsNullOrEmpty(inputXml) || inputXml == "test")
                    return false;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(inputXml);
                string borderValue = ((System.Xml.XmlAttribute)((Xdocument.ChildNodes[0]).Attributes["border"])).Value.ToLower();
                if (!string.IsNullOrEmpty(borderValue))
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        public List<int> ProducttypeId = new List<int>();
        private void StartEngine()
        {
            //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
            PrinterBusniess _objDbLayer = new PrinterBusniess();

            #region Initialize Panorama business
            PanoramaBusiness _objPanoramaBusiness = new PanoramaBusiness();
            #endregion
            var pc = _objDbLayer.GetPrinterQueueforPrint(App.SubStoreId, ref ProducttypeId);
            //var pc = _objDbLayer.GetPrinterQueueforPrint(1, ref ProducttypeId);
            bool IsProductPanoramic = _objPanoramaBusiness.IsProductPanorama(pc.DG_PrinterQueue_Pkey);

            ConfigBusiness configBusiness = new ConfigBusiness();
            //FolderStructureInfo FolderInfo = new FolderStructureInfo();
            MW.FolderPathStructure = configBusiness.GetFolderStructureInfo(App.SubStoreId);
            //pc.DG_Order_SubStoreId
            MW.PrintSizeID = pc.DG_Orders_ProductType_pkey;
            string editedImagePath = string.Empty;
            try
            {
                if (pc != null && pc.DG_Orders_ProductType_pkey != 0)
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif

                    //set true to flush memory
                    isFlush = true;
                    MW.PrintProductType = pc.DG_Orders_ProductType_pkey;
                    String imgName = pc.DG_Photos_ID.ToString();
                    bool children = false;
                    MW.selectedEnglishText = "";
                    MW.selectedChineseText = "";

                    if (imgName.IndexOf(',') >= 0)
                    {
                        Directory.CreateDirectory(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString()));
                        children = true;
                    }
                    string photoRFID = string.Empty;
                    List<SceneInfo> BlankPageSceneDetails = null;
                    foreach (string Name in imgName.Split(','))
                    {
#if DEBUG
                        var watch1 = FrameworkHelper.CommonUtility.Watch();
                        watch1.Start();
#endif
                        if (photoRFID == string.Empty)
                        {
                            photoRFID = Name;//This is for StorybookID to store storybook childs under RFID in print images folder..By VINS_20Oct2021
                        }
                        MW.ProductTypeID = pc.DG_PrinterQueue_ProductID;
                        PhotoBusiness objPhoto = new PhotoBusiness();
                        //PhotoInfo _objphoto = objPhoto.GetPhotoDetailsbyPhotoId(Name.ToInt32());
                        PhotoInfo _objphoto = objPhoto.GetPhotoStoryBookDetailsbyPhotoId(Name.ToInt32());
                        string editedImageFolder = configBusiness.GetFolderStructureInfo(pc.DG_Order_SubStoreId).EditedImagePath;
                        MW.PhotoName = _objphoto.DG_Photos_RFID;
                        MW.PhotoId = Name.ToInt32();


                        if (IsProductPanoramic)
                        {
                            editedImagePath = System.IO.Path.Combine(editedImageFolder + "Panorama", _objphoto.DG_Photos_FileName);
                        }
                        else
                        {

                            editedImagePath = System.IO.Path.Combine(editedImageFolder, _objphoto.DG_Photos_FileName);
                        }
                        MW.editedImagepath = editedImagePath;

                        GetNewConfigLocationValues(_objphoto.DG_SubStoreId.Value, _objphoto.DG_Location_Id.Value);
                        try
                        {
                            string inputXml = string.Empty;
                            if (String.IsNullOrWhiteSpace(_objphoto.DG_Photos_Layering) || _objphoto.DG_Photos_Layering == "NULL")
                                inputXml = null;
                            else
                                inputXml = _objphoto.DG_Photos_Layering;
                            //bool isEffectApplied = IsEffectApplied(_objphoto);
                            bool isBorderApplied = IsBorderApplied(inputXml);
                            bool isGreenScreen = false;
                            bool isCropped = false;
                            if (_objphoto.DG_Photos_IsGreen.HasValue)
                                isGreenScreen = _objphoto.DG_Photos_IsGreen.Value;
                            if (_objphoto.DG_Photos_IsCroped.HasValue)
                                isCropped = _objphoto.DG_Photos_IsCroped.Value;

                            if (inputXml != "test" && !string.IsNullOrWhiteSpace(inputXml))
                            {
                                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                                Xdocument.LoadXml(inputXml);

                                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                                {
                                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                                    {
                                        case "producttype":
                                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                                            {
                                                specproductType = ((System.Xml.XmlAttribute)xn).Value;
                                            }
                                            break;
                                    }
                                }
                                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                                while (rdr.Read())
                                {
                                    if (rdr.NodeType == XmlNodeType.Element)
                                    {
                                        switch (rdr.Name.ToString().ToLower())
                                        {
                                            case "gumball":
                                                IsGumballXml = true;
                                                MW.IsgumBallActive = IsGumballXml;
                                                break;
                                        }
                                    }
                                }
                            }


                            if (children)
                            {
                                if (!string.IsNullOrWhiteSpace(specproductType) && IsGumballXml.Equals(true))
                                {
                                    speccase = true;
                                    IsGumballXml = false;
                                }
                                //if (!isEffectApplied && _objphoto.DG_Photos_IsCroped == true && !isBorderApplied)
                                //    File.Copy(System.IO.Path.Combine(LoginUser.DigiFolderCropedPath, _objphoto.DG_Photos_FileName), LoginUser.DigiFolderPath + "\\PrintImages\\" + pc.DG_PrinterQueue_Pkey.ToString() + "\\" + MW.PhotoId.ToString() + ".jpg", true);
                                //else if (!isEffectApplied)
                                //    File.Copy(System.IO.Path.Combine(LoginUser.DigiFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName), LoginUser.DigiFolderPath + "\\PrintImages\\" + pc.DG_PrinterQueue_Pkey.ToString() + "\\" + MW.PhotoId.ToString() + ".jpg", true);

                                //if (File.Exists(editedImagePath) && ((!isCropped && !isBorderApplied) && isGreenScreen))
                                if (IsAutoColorCorrectionActive && !IsCorrectionAtDownloadActive)
                                {
                                    if (File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        AutoCorrectImageWIthCopy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")));
                                        //File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else
                                        PrepareImageWithCorrection(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")));
                                }
                                else if (pc.DG_Orders_ProductType_pkey == 131) //Storybook product ID
                                {
                                    if (!Directory.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID)))
                                    {
                                        Directory.CreateDirectory(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID));//Create directory with RFID name
                                    }
                                    if (File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID, (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else if (File.Exists(editedImagePath))//Storybook ID case _By Vins
                                    {
                                        File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID, (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else
                                    {
                                        PrepareImage(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID, (MW.PhotoId.ToString() + ".jpg")));
                                    }

                                    if (BlankPageSceneDetails == null)
                                    {
                                        BlankPageSceneDetails = (new SceneBusiness()).GetAllBlankPageByThemeID((Int32)_objphoto.ThemeID);
                                    }
                                    //Check if cascade image page number is blank
                                    string blankPage = "";
                                    if (_objphoto.PageNo % 2 == 0)//Check current even page then look for odd blank page and apply co-ordinates with text
                                    {
                                        if (BlankPageSceneDetails != null)
                                        {
                                            blankPage = Convert.ToString(_objphoto.PageNo - 1);
                                        }
                                    }
                                    else //Check for odd
                                    {
                                        blankPage = Convert.ToString(_objphoto.PageNo + 1);
                                    }
                                    //Check if conscecutive page is blank
                                    if (BlankPageSceneDetails != null && blankPage != "")
                                    {
                                        SceneInfo SceneBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == blankPage).FirstOrDefault();
                                        if (SceneBlankPage != null)
                                        {
                                            string editedBlankPagePath = System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, photoRFID, MW.PhotoId.ToString() + "_" + blankPage + ".jpg");
                                            //Create blank page with text and put in editedimage folder 
                                            //PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, blankPage, editedBlankPagePath, SceneBlankPage.SceneId);
                                            //PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName,blankPage, editedBlankPagePath, SceneBlankPage.SceneId);
                                            PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, blankPage, editedBlankPagePath, SceneBlankPage.SceneId, _objphoto.StoryBookID);
                                        }
                                    }
                                }
                                else
                                {
                                    if (IsProductPanoramic && File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")), true);
                                    }

                                    if (File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else if (File.Exists(editedImagePath))//Storybook ID case _By Vins
                                    {
                                        File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else
                                    {
                                        PrepareImage(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")));
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(specproductType) && IsGumballXml.Equals(true))
                                {
                                    speccase = true;
                                    IsGumballXml = false;
                                }

                                //if (!isEffectApplied && _objphoto.DG_Photos_IsCroped == true)
                                //    File.Copy(System.IO.Path.Combine(LoginUser.DigiFolderCropedPath, _objphoto.DG_Photos_FileName), DigiPhoto.Common.LoginUser.DigiFolderPath + "\\PrintImages\\" + pc.DG_PrinterQueue_Pkey.ToString() + ".jpg", true);
                                //else if (!isEffectApplied)
                                //    File.Copy(System.IO.Path.Combine(LoginUser.DigiFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName), DigiPhoto.Common.LoginUser.DigiFolderPath + "\\PrintImages\\" + pc.DG_PrinterQueue_Pkey.ToString() + ".jpg", true);
                                //if (File.Exists(editedImagePath) && ((!isCropped && !isBorderApplied) && isGreenScreen))
                                if (IsAutoColorCorrectionActive && !IsCorrectionAtDownloadActive)
                                {
                                    if (File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        AutoCorrectImageWIthCopy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")));
                                        //File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString(), (MW.PhotoId.ToString() + ".jpg")), true);
                                    }
                                    else
                                        PrepareImageWithCorrection(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")));
                                }
                                else
                                {

                                    if (File.Exists(editedImagePath) && (isGreenScreen) && speccase.Equals(false))
                                    {
                                        //check if the product is panoramic place in  printimages\panoramic folder
                                        bool IsProductPanoramicCopyToPrintImages = _objPanoramaBusiness.IsProductPanorama(pc.DG_PrinterQueue_Pkey);
                                        if (IsProductPanoramic)
                                        {
                                            if (!Directory.Exists(MW.FolderPathStructure.PrintImagesPath + "\\Panorama"))
                                                Directory.CreateDirectory(MW.FolderPathStructure.PrintImagesPath + "\\Panorama");
                                            File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath + "\\Panorama", (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")), true);
                                        }
                                        else
                                        {
                                            File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")), true);
                                        }

                                    }
                                    else
                                    {
                                        if (IsProductPanoramic)
                                        {
                                            //PrepareImage(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath + "\\Panorama", (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")));
                                            if (File.Exists(editedImagePath))
                                            {
                                                File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath + "\\Panorama", (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")), true);
                                            }
                                        }
                                        else
                                        {
                                            bool isLicenceProduct = false;
                                            if (!string.IsNullOrEmpty(inputXml))
                                            {
                                                isLicenceProduct = inputXml.Contains("isLicenseProduct");
                                            }
                                            //Copy edited image to Printimages folder for Licence product
                                            if (File.Exists(editedImagePath))//By KCB
                                            //if (File.Exists(editedImagePath) && isLicenceProduct)//Commented bY KCB
                                            {
                                                File.Copy(editedImagePath, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")), true);
                                            }
                                            else
                                            {
                                                PrepareImage(_objphoto, System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")));
                                            }

                                        }
                                    }
                                }


                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.ErrorHandler.LogError(ex);
                            _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                            _objDbLayer.SetPrinterQueue(pc.DG_PrinterQueue_Pkey);

                        }
                        finally
                        {
                            ///Added  by Vinod Salunke 1Apr2019
                            int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                            GC.AddMemoryPressure(totMemoryAlloted); //20000

                            MemoryManagement.FlushMemory();

                            //Force garbage collection.// Wait for all finalizers to complete before continuing.
                            //GC.Collect();
                            //GC.WaitForPendingFinalizers();
                        }

                        if (!IsProductPanoramic)
                        {

                            if (pc.DG_Orders_ProductType_pkey != 4 && pc.DG_Orders_ProductType_pkey != 101 && pc.DG_Orders_ProductType_pkey != 79 && pc.DG_Orders_ProductType_pkey != 98
                                && pc.DG_Orders_ProductType_pkey != 120 && pc.DG_Orders_ProductType_pkey != 121 && pc.DG_Orders_ProductType_pkey != 122 && pc.DG_Orders_ProductType_pkey != 123
                                && pc.DG_Orders_ProductType_pkey != 125 && pc.DG_Orders_ProductType_pkey != 131)
                            {
                                if (File.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg"))) || MW.imageNotFound)
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                            }
                            else
                            {
                                if (pc.DG_Orders_ProductType_pkey == 131)
                                {
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                                }

                                if (MW.imageNotFound)
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                            }
                        }
#if DEBUG
                        if (watch1 != null)
                            FrameworkHelper.CommonUtility.WatchStop("Print Engine apply effects 1 image partial", watch1);
#endif
                    }
                    if (pc.DG_Orders_ProductType_pkey == 79)
                    {
                        if (Directory.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString())))
                        {
                            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString()));
                            int count = dir.GetFiles().Length;
                            //Look Wheather both Images on Album page is same--
                            var Photos = pc.DG_Photos_ID;
                            if (Photos != null)
                            {
                                int[] photoArray = Photos.Split(',').Select(int.Parse).ToArray();

                                if (photoArray[0] == photoArray[1] && count == 1)
                                {
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                                }
                                else if (count == 2 || MW.imageNotFound)
                                {
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                                }
                            }
                        }
                        else if (!imgName.Contains(",") && File.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg"))) || MW.imageNotFound)
                            _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                    }
                    else if (pc.DG_Orders_ProductType_pkey == 4 || pc.DG_Orders_ProductType_pkey == 101)
                    {
                        if (Directory.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString())))
                        {
                            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString()));
                            int count = dir.GetFiles().Length;
                            if (count >= 4 || MW.imageNotFound)
                                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                        }
                        else if (!imgName.Contains(",") && File.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg"))) || MW.imageNotFound)
                            _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                    }
                    else if (pc.DG_Orders_ProductType_pkey == 105 || pc.DG_Orders_ProductType_pkey == 131) //Storybook ID 131
                    {
                        if (Directory.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString())))
                        {
                            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString()));
                            int count = dir.GetFiles().Length;
                            if (count > 0)
                                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                        }
                        else if (!imgName.Contains(",") && File.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, (pc.DG_PrinterQueue_Pkey.ToString() + ".jpg"))) || MW.imageNotFound)
                            _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                    }

                    else if (pc.DG_Orders_ProductType_pkey == 98)
                    {
                        if (Directory.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString())))
                        {
                            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, pc.DG_PrinterQueue_Pkey.ToString()));
                            int count = dir.GetFiles().Length;
                            //Look Wheather both Images on Album page is same--
                            var Photos = pc.DG_Photos_ID;
                            if (Photos != null)
                            {
                                int[] photoArray = Photos.Split(',').Select(int.Parse).ToArray();
                                if (photoArray[0] == photoArray[1] && count == 1)
                                {
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                                }
                                else if (count == 2 || MW.imageNotFound)
                                {
                                    _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                                }
                            }
                        }
                        else
                        {
                            if (MW.imageNotFound)
                                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                        }
                    }


                    #region Ajay 18 Jan 18 for Panorama 8 x24 pkg


                    else if (IsProductPanoramic)
                    {

                        if (File.Exists(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath + "Panorama", pc.DG_PrinterQueue_Pkey.ToString() + ".jpg")))
                        {
                            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(MW.FolderPathStructure.PrintImagesPath, "Panorama"));
                            int count = dir.GetFiles().Length;
                            //Look Wheather both Images on Album page is same--
                            var Photos = pc.DG_Photos_ID;
                            if (Photos != null)
                            {
                                int[] photoArray = Photos.Split(',').Select(int.Parse).ToArray();
                                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                            }
                        }
                        else
                        {
                            if (MW.imageNotFound)
                                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                        }
                    }
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Print Engine apply Effects", watch);
#endif
                }
            }
            catch (Exception ex)
            {
                _objDbLayer.ReadyForPrint(pc.DG_PrinterQueue_Pkey);
                if (_objDbLayer != null)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("_objDbLayer is not null");
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_objDbLayer);
                    _objDbLayer = null; // Set each COM Object to null
                }


                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (isFlush)
                {
                    MemoryManagement.FlushMemory();
                    isFlush = false;

                    //Force garbage collection.
                    //GC.Collect();

                    //// Wait for all finalizers to complete before continuing.
                    //GC.WaitForPendingFinalizers();

                }
            }
        }

        private void AutoCorrectImageWIthCopy(string InputFilePath, string OutputFilePath)
        {
            try
            {
                using (FileStream fs = File.OpenRead(InputFilePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fs.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (MagickImage image = new MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        /// <summary>
        /// Added Try catch block by Vinod Salunke 1Apr2019
        /// </summary>
        /// <param name="_objphoto"></param>
        /// <param name="destinationPath"></param>
        private void PrepareImage(PhotoInfo _objphoto, string destinationPath)
        {
            try
            {
                if (_objphoto != null)
                {
                    if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Effects))
                    {
                        MW.ImageEffect = _objphoto.DG_Photos_Effects;
                    }
                    else
                    {
                        MW.ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    }
                }
                MW.WindowStyle = System.Windows.WindowStyle.None;
                MW.ResetForm();
                MW.UpdateLayout();
                if (MW.imageNotFound)
                    return;
                RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
                if (_objphoto.DG_Photos_IsGreen == true)
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 80;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 99;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + " PrepareImage()");
            }
            finally
            {
                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000

                MemoryManagement.FlushMemory();

                //Force garbage collection.// Wait for all finalizers to complete before continuing.
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }

        }


        private void PrepareImageBlankPage(PhotoInfo _objphoto, string themeName,string pageNumber, string destinationPath, int sceneID)
        {
            try
            {               
                MW.WindowStyle = System.Windows.WindowStyle.None;
                //MW.ResetForm();
                //MW.ImageLoader(_objphoto);
                MW.LoadXamlGuestNames(_objphoto.DG_Photos_Layering);//Just to get guest name on photo//chinese and english text 
                MW.ImageLoaderBlankPage(_objphoto, themeName, pageNumber);
                MW.SetTextEnglishGuest("****", Convert.ToInt32(pageNumber), sceneID);
                MW.SetTextChineseGuest("****", Convert.ToInt32(pageNumber), sceneID);
                MW.UpdateLayout();
                if (MW.imageNotFound)
                    return;
                RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
                if (_objphoto.DG_Photos_IsGreen == true)
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 80;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 99;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + " PrepareImage()");
            }
            finally
            {
                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000
                MemoryManagement.FlushMemory();
            }
        }

        private void PrepareImageBlankPage(PhotoInfo _objphoto, string themeName, string pageNumber, string destinationPath, int sceneID, long storyBookID)
        {
            try
            {
                PhotoBusiness objPhoto = new PhotoBusiness();
                MW.WindowStyle = System.Windows.WindowStyle.None;
                //MW.ResetForm();

                MW.ImageLoaderBlankPage(_objphoto, themeName, pageNumber);

                List<PhotoInfo> objPhotoLst = objPhoto.GetAllStoryBookPhotoBYSTID(storyBookID);//list to check page no.
                foreach (PhotoInfo obj in objPhotoLst)
                {
                    if (!String.IsNullOrEmpty(MW.selectedEnglishText) || !String.IsNullOrEmpty(MW.selectedChineseText))
                    {
                        break;
                    }
                    if (String.IsNullOrEmpty(MW.selectedEnglishText) || String.IsNullOrEmpty(MW.selectedChineseText))
                    {
                        MW.LoadXamlGuestNames(obj.DG_Photos_Layering);//Just to get guest name on photo//chinese and english text 
                        MW.isBlankPageRequired = true;
                    }
                    
                }
                MW.SetTextEnglishGuest("******", Convert.ToInt32(pageNumber), sceneID);
                MW.SetTextChineseGuest("******", Convert.ToInt32(pageNumber), sceneID);
                MW.UpdateLayout();
                if (MW.imageNotFound)
                    return;
                RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
                if (_objphoto.DG_Photos_IsGreen == true)
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 80;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 99;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + " PrepareImage()");
            }
            finally
            {
                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000
                MemoryManagement.FlushMemory();
            }
        }

        private void PrepareImageWithCorrection(PhotoInfo _objphoto, string destinationPath)
        {
            if (_objphoto != null)
            {
                if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Effects))
                {
                    MW.ImageEffect = _objphoto.DG_Photos_Effects;
                }
                else
                {
                    MW.ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                }
            }
            MW.WindowStyle = System.Windows.WindowStyle.None;
            MW.ResetForm();
            MW.UpdateLayout();
            RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
            if (_objphoto.DG_Photos_IsGreen == true)
            {
                //using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                //{
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 80;
                encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ApplyAutoCorrectionOnStream(ms, destinationPath);
                }
                //}
            }
            else
            {
                //using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                //{
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 99;
                encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ApplyAutoCorrectionOnStream(ms, destinationPath);
                }
                //}
            }
        }
        private void ApplyAutoCorrectionOnStream(MemoryStream ms, string DestinationFolder)
        {
            try
            {
                ms.Seek(0, SeekOrigin.Begin);
                using (MagickImage image = new MagickImage(ms))
                {
                    if (IsContrastCorrectionActive)
                        image.Contrast();
                    if (IsGammaCorrectionActive)
                        image.AutoGamma();// auto-gamma correction
                    if (IsNoiseReductionActive)
                        image.ReduceNoise();
                    IsAutoColorCorrectionActive = false;
                    image.Write(DestinationFolder);
                    image.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private bool IsEffectApplied(PhotoInfo _objphoto)
        {
            if (string.IsNullOrEmpty(_objphoto.DG_Photos_Effects) || string.Compare(_objphoto.DG_Photos_Effects, defaultEffect.TrimStart().TrimEnd(), true) == 0)
            {
                //No effect applied. Check Layering.
                if (string.IsNullOrEmpty(_objphoto.DG_Photos_Layering) || _objphoto.DG_Photos_Layering == "test" || string.Compare(_objphoto.DG_Photos_Layering, defaultSpecPrintLayer.TrimStart().TrimEnd(), true) == 0)
                    return false;
            }
            return true;
        }
        private bool IsLayerApplied(DG_Photos _objphoto)
        {
            //No effect applied. Check Layering.
            if (string.IsNullOrEmpty(_objphoto.DG_Photos_Layering) || _objphoto.DG_Photos_Layering == "test" || string.Compare(_objphoto.DG_Photos_Layering, defaultSpecPrintLayer.TrimStart().TrimEnd(), true) == 0)
                return false;

            return true;
        }

        bool IsAutoColorCorrectionActive = false;
        bool IsCorrectionAtDownloadActive = false;
        bool IsContrastCorrectionActive = false;
        bool IsGammaCorrectionActive = false;
        bool IsNoiseReductionActive = false;

        private void GetNewConfigLocationValues(int subStoreId, int locationId)
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(locationId, subStoreId);
            foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsAutoColorCorrection://for auto color correction in images
                        IsAutoColorCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsCorrectAtDownloadActive:
                        IsCorrectionAtDownloadActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsContrastCorrection:
                        IsContrastCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsGammaCorrection:
                        IsGammaCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsNoiseReduction:
                        IsNoiseReductionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    default:
                        break;
                }
            }
        }

        private void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        {
            try
            {
                // the resized result bitmap
                decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);

                int width = Convert.ToInt32(height * ratio);


                using (Bitmap result = new Bitmap(width, height))
                {
                    // get the graphics and draw the passed image to the result bitmap
                    using (Graphics grphs = Graphics.FromImage(result))
                    {
                        grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        grphs.DrawImage(image, 0, 0, result.Width, result.Height);
                    }

                    // check the quality passed in
                    if ((quality < 0) || (quality > 100))
                    {
                        string error = string.Format("quality must be 0, 100", quality);
                        throw new ArgumentOutOfRangeException(error);
                    }

                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    string lookupKey = "image/jpeg";
                    var jpegCodec =
                        ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

                    //create a collection of EncoderParameters and set the quality parameter
                    var encoderParams = new EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;
                    //save the image using the codec and the encoder parameter
                    result.Save(pathToSave, jpegCodec, encoderParams);
                    IntPtr deleteObject = result.GetHbitmap();
                    DeleteObject(deleteObject);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                image = null;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
            button1_Click(sender, e);
            this.Topmost = true;
        }
    }
}
