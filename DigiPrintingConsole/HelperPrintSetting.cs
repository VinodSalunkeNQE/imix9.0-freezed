﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Printing;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.IO;
using System.Printing.Interop;

namespace DigiPrintingConsole1
{
    static class SetPrinter
    {
        #region P/Invoke

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalFree(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalLock(IntPtr handle);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GlobalUnlock(IntPtr handle);

        #endregion

        public static void setPrinterCode6(PrintDocument PDoc)
        {
            IntPtr hMode;                     
            IntPtr pMode;                        
            PrinterSettings printerSettings;
            byte[] savedDevMode;                 
            Stream savedDevStream;

            printerSettings = PDoc.PrinterSettings;
            hMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);

            pMode = GlobalLock(hMode);
            savedDevStream = new FileStream("Printer_yitp_yxi.bin", FileMode.Open, FileAccess.Read);
            savedDevMode = new byte[savedDevStream.Length];
            savedDevStream.Read(savedDevMode, 0, savedDevMode.Length);
            savedDevStream.Close();
            savedDevStream.Dispose();

            for (int i = 0; i < savedDevMode.Length; ++i)
            {
                Marshal.WriteByte(pMode, i, savedDevMode[i]);
            }

            GlobalUnlock(hMode);

            printerSettings.SetHdevmode(hMode);

            GlobalFree(hMode);
        }

        public static void setPrinterCode4(PrintDocument pDoc)
        {
            IntPtr hMode;
            IntPtr pMode;                        
            PrinterSettings printerSettings;        
            byte[] savedMode;                   
            Stream savedStream;
            printerSettings = pDoc.PrinterSettings;
            hMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
            pMode = GlobalLock(hMode);

            savedStream = new FileStream("Printer_yitp_rxy.bin", FileMode.Open, FileAccess.Read);
            savedMode = new byte[savedStream.Length];
            savedStream.Read(savedMode, 0, savedMode.Length);
            savedStream.Close();
            savedStream.Dispose();
            
            for (int i = 0; i < savedMode.Length; ++i)
            {
                Marshal.WriteByte(pMode, i, savedMode[i]);
            }
            GlobalUnlock(hMode);

            printerSettings.SetHdevmode(hMode);
            GlobalFree(hMode);
        }

        public static void setPrinterCode(PrintDocument pDoc)
        {
            IntPtr hMode;
            IntPtr pMode;
            PrinterSettings printerSettings;
            byte[] savedMode;
            Stream savedStream;
            printerSettings = pDoc.PrinterSettings;
            hMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
            pMode = GlobalLock(hMode);

            savedStream = new FileStream("Printer_oiqp.bin", FileMode.Open, FileAccess.Read);
            savedMode = new byte[savedStream.Length];
            savedStream.Read(savedMode, 0, savedMode.Length);
            savedStream.Close();
            savedStream.Dispose();

            for (int i = 0; i < savedMode.Length; ++i)
            {
                Marshal.WriteByte(pMode, i, savedMode[i]);
            }
            GlobalUnlock(hMode);

            printerSettings.SetHdevmode(hMode);
            GlobalFree(hMode);
        }
    }

    [StructLayout(LayoutKind.Explicit, CharSet = CharSet.Ansi)]
    struct DEVMODE1
    {
        public const int CCHDEVICENAME = 32;
        public const int CCHFORMNAME = 32;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = CCHDEVICENAME)]
        [System.Runtime.InteropServices.FieldOffset(0)]
        public string dmDeviceName;
        [System.Runtime.InteropServices.FieldOffset(32)]
        public Int16 dmSpecVersion;
        [System.Runtime.InteropServices.FieldOffset(34)]
        public Int16 dmDriverVersion;
        [System.Runtime.InteropServices.FieldOffset(36)]
        public Int16 dmSize;
        [System.Runtime.InteropServices.FieldOffset(38)]
        public Int16 dmDriverExtra;
        //[System.Runtime.InteropServices.FieldOffset(40)]
        //public DM dmFields;

        [System.Runtime.InteropServices.FieldOffset(44)]
        Int16 dmOrientation;
        [System.Runtime.InteropServices.FieldOffset(46)]
        Int16 dmPaperSize;
        [System.Runtime.InteropServices.FieldOffset(48)]
        Int16 dmPaperLength;
        [System.Runtime.InteropServices.FieldOffset(50)]
        Int16 dmPaperWidth;
        [System.Runtime.InteropServices.FieldOffset(52)]
        Int16 dmScale;
        [System.Runtime.InteropServices.FieldOffset(54)]
        Int16 dmCopies;
        [System.Runtime.InteropServices.FieldOffset(56)]
        Int16 dmDefaultSource;
        [System.Runtime.InteropServices.FieldOffset(58)]
        Int16 dmPrintQuality;

        //[System.Runtime.InteropServices.FieldOffset(44)]
        //public POINTL dmPosition;
        [System.Runtime.InteropServices.FieldOffset(52)]
        public Int32 dmDisplayOrientation;
        [System.Runtime.InteropServices.FieldOffset(56)]
        public Int32 dmDisplayFixedOutput;

        [System.Runtime.InteropServices.FieldOffset(60)]
        public short dmColor;
        [System.Runtime.InteropServices.FieldOffset(62)]
        public short dmDuplex;
        [System.Runtime.InteropServices.FieldOffset(64)]
        public short dmYResolution;
        [System.Runtime.InteropServices.FieldOffset(66)]
        public short dmTTOption;
        [System.Runtime.InteropServices.FieldOffset(68)]
        public short dmCollate;
        [System.Runtime.InteropServices.FieldOffset(72)]
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = CCHFORMNAME)]
        public string dmFormName;
        [System.Runtime.InteropServices.FieldOffset(102)]
        public Int16 dmLogPixels;
        [System.Runtime.InteropServices.FieldOffset(104)]
        public Int32 dmBitsPerPel;
        [System.Runtime.InteropServices.FieldOffset(108)]
        public Int32 dmPelsWidth;
        [System.Runtime.InteropServices.FieldOffset(112)]
        public Int32 dmPelsHeight;
        [System.Runtime.InteropServices.FieldOffset(116)]
        public Int32 dmDisplayFlags;
        [System.Runtime.InteropServices.FieldOffset(116)]
        public Int32 dmNup;
        [System.Runtime.InteropServices.FieldOffset(120)]
        public Int32 dmDisplayFrequency;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DEVMODE
    {
        public const int CCHDEVICENAME = 32;
        public const int CCHFORMNAME = 32;

        public unsafe fixed char dmDeviceName[CCHDEVICENAME];
        public Int16 dmSpecVersion;
        public Int16 dmDriverVersion;
        public Int16 dmSize;
        public Int16 dmDriverExtra;
        public DM_FIELD_TYPE dmFields;

        public Int16 dmOrientation;
        public Int16 dmPaperSize;
        public Int16 dmPaperLength;
        public Int16 dmPaperWidth;
        public Int16 dmScale;
        public Int16 dmCopies;
        public Int16 dmDefaultSource;
        public Int16 dmPrintQuality;

        public POINTL dmPosition;
        public Int32 dmDisplayOrientation;
        public Int32 dmDisplayFixedOutput;

        public short dmColor;
        public short dmDuplex;
        public short dmYResolution;
        public short dmTTOption;
        public short dmCollate;

        public unsafe fixed char dmFormName[CCHFORMNAME];
        public Int16 dmLogPixels;
        public Int32 dmBitsPerPel;
        public Int32 dmPelsWidth;
        public Int32 dmPelsHeight;
        public Int32 dmDisplayFlags;
        public Int32 dmNup;
        public Int32 dmDisplayFrequency;
        public Int32 dmICMMethod;
        public Int32 dmICMIntent;
        public Int32 dmMediaType;
        public Int32 dmDitherType;
        public Int32 dmReserved1;
        public Int32 dmReserved2;
        public Int32 dmPanningWidth;
        public Int32 dmPanningHeight;

        public DEVMODE(byte[] data)
        {
            unsafe
            {
                fixed (byte* packet = &data[0])
                {
                    this = *(DEVMODE*)packet;
                }
            }
        }

    }

    [Flags()]
    public enum DM_FIELD_TYPE : int
    {
        /* field selection bits */
        DM_ORIENTATION = 0x00000001,
        DM_PAPERSIZE = 0x00000002,
        DM_PAPERLENGTH = 0x00000004,
        DM_PAPERWIDTH = 0x00000008,
        DM_SCALE = 0x00000010,
        DM_POSITION = 0x00000020,
        DM_NUP = 0x00000040,
        DM_DISPLAYORIENTATION = 0x00000080,
        DM_COPIES = 0x00000100,
        DM_DEFAULTSOURCE = 0x00000200,
        DM_PRINTQUALITY = 0x00000400,
        DM_COLOR = 0x00000800,
        DM_DUPLEX = 0x00001000,
        DM_YRESOLUTION = 0x00002000,
        DM_TTOPTION = 0x00004000,
        DM_COLLATE = 0x00008000,
        DM_FORMNAME = 0x00010000,
        DM_LOGPIXELS = 0x00020000,
        DM_BITSPERPEL = 0x00040000,
        DM_PELSWIDTH = 0x00080000,
        DM_PELSHEIGHT = 0x00100000,
        DM_DISPLAYFLAGS = 0x00200000,
        DM_DISPLAYFREQUENCY = 0x00400000,
        DM_ICMMETHOD = 0x00800000,
        DM_ICMINTENT = 0x01000000,
        DM_MEDIATYPE = 0x02000000,
        DM_DITHERTYPE = 0x04000000,
        DM_PANNINGWIDTH = 0x08000000,
        DM_PANNINGHEIGHT = 0x10000000,
        DM_DISPLAYFIXEDOUTPUT = 0x20000000
    }

    public struct POINTL
    {
        public Int32 x;
        public Int32 y;
    }
}
