﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.Windows.Threading;
using System.IO;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Printing;
using System.Drawing;
using System.Drawing.Printing;
using DigiPhoto;
using DigiPhoto.Common;
using System.Management;
using System.Threading;
using System.ServiceProcess;
using DigiPhoto.IMIX.Business;
using System.Xml;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Configuration;
using ZXing;
using ZXing.Common;
using System.Windows.Interop;
using System.Drawing.Imaging;
using DS820;
using System.Diagnostics;
using System.Runtime.InteropServices;
using K6900Printer;
using System.Threading.Tasks;


//Rajeev 22/11/12 04:56\
namespace DigiPrintingConsole
{
    public partial class ImageDispatcher : Window
    {
        #region Declaration
        public DispatcherTimer PrintTimer;
        //DigiPhotoDataServices objDigiPhotoDataServices;
        public static string lastproductypeID;
        public List<currentTime> _objlastusedprinter;
        public static bool IsOrderwiseLoadBalancingActive = false;
        string subStorePrintPath = string.Empty;
        List<AssociatedPrintersInfo> associatedPrinterList = null;
        int printerErrorGap = 5;
        int printerErrorCheckTimeoutInSec = 20;
        DateTime printerErrorCheckTime = DateTime.Now;
        int remapLastPrinter = 0;
        double CalenderMarginValue = 0;
        double MarginValue4X6_2 = 0;
        double MarginValueUnique4X6_2 = 0;
        string QrCodeOnlineProductText;
        string QrCodeOnlineProductURL;
        string QRCodeWebUrl;
        string QR;
        bool IsSmallWalletwith4ImgCutter = false;

        public string HtmlToDisplay { get; set; }
        #endregion

        #region Constructor
        public ImageDispatcher()
        {
            try
            {
                InitializeComponent();
                PrintTimer = new DispatcherTimer();
                PrintTimer.Interval = new TimeSpan(0, 0, 1);
                PrintTimer.Tick += new EventHandler(PrintTimer_Tick);
                _objlastusedprinter = new List<currentTime>();
                associatedPrinterList = new List<AssociatedPrintersInfo>();
                int.TryParse(ConfigurationManager.AppSettings["printerErrorTimeoutInMnt"].ToString(), out printerErrorGap);
                associatedPrinterList = new PrinterBusniess().GetAssociatedPrintersforPrint(null, App.SubStoreId);

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region LoadBalancing
        public bool isPrinterReady(string PrinterName)
        {
            // Set management scope
            bool Status = IsReadyPrinterNew(PrinterName);
            /*
            if (!Status)
            {
                if (!string.IsNullOrEmpty(PrinterName))
                {
                    bool result = RemapPrinter(PrinterName);
                    Status = IsReadyPrinterNew(PrinterName);
                }
            }
            */
            return Status;
        }

        private static bool IsReadyPrinterNew(string PrinterName)
        {
            // Set management scope
            bool retvalue = false;
            ManagementScope scope = new ManagementScope("\\root\\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            string printerName = string.Empty;
            var results = searcher.Get();
            string[] PrinterStatuses =
            {
                "Other", "Unknown", "Idle", "Printing", "WarmUp", "Stopped Printing", "Offline"
            };

            foreach (ManagementObject printer1 in results)
            {
                printerName = printer1["Name"].ToString().ToLower();
                if (printerName.Equals(PrinterName.ToLower()))
                {
                    if (printer1["WorkOffline"].ToString().ToLower().Equals("true"))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("printer status offline");

                        // printer is offline 
                        ErrorHandler.ErrorHandler.LogFileWrite(printerName + " is Offline.");
                        retvalue = false;
                    }
                    else
                    {
                        Int32 status = printer1["PrinterStatus"].ToInt32();
                        string printerStatus = PrinterStatuses[status];

                        ErrorHandler.ErrorHandler.LogFileWrite("printer and status =" + Convert.ToString(printerName) + " Status, " + printerStatus);

                        if (status == Convert.ToInt32(PrinterStatus.Other) || status == Convert.ToInt32(PrinterStatus.Offline))
                        {
                            retvalue = false;
                            string errorMessage = printerName + " is in error (" + printerStatus + ") status : ";
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                        else
                        {
                            retvalue = true;
                        }
                    }
                    break; //BY KCB ON 08 AUGUST TO AVOID UNWANTED LOOP.
                }
            }
            return retvalue;
        }

        private bool RemapPrinter(string PrinterName)
        {
            PrinterTypeBusiness AssociatedPrinterBusiness = new PrinterTypeBusiness(); ;
            //foreach (string printer in PrinterSettings.InstalledPrinters)
            //{
            bool result = false;
            if (PrinterSettings.InstalledPrinters.Cast<string>().Where(o => string.Compare(o, PrinterName, true) == 0).Count() > 0)
            {

                result = AssociatedPrinterBusiness.RemapNewPrinter(PrinterName, LoginUser.SubStoreId, true);
            }
            //}
            return result;
        }


        public string GetMainQueue(int? productypeId, int? orderDetailId)
        {
            try
            {
                var server = new PrintServer();
                var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

                //DigiPhotoDataServices _objdblayer = new DigiPhotoDataServices();
                PrinterBusniess printBusiness = new PrinterBusniess();
                List<AssociatedPrintersInfo> AssociatedPrinterList = printBusiness.GetAssociatedPrintersforPrint(productypeId, App.SubStoreId);

                Dictionary<string, int> printerques = new Dictionary<string, int> { };
                foreach (var item in AssociatedPrinterList)
                {
                    var printer = queues.Where(t => t.FullName == item.DG_AssociatedPrinters_Name).FirstOrDefault();

                    if (printer != null)
                    {
                        if (isPrinterReady(printer.FullName))
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("printer is ready FullName =" + Convert.ToString(printer.FullName));
                            printerques.Add(printer.FullName, printer.NumberOfJobs);
                            ErrorHandler.ErrorHandler.LogFileWrite("printer.NumberOfJobs =" + Convert.ToString(printer.NumberOfJobs));
                            ErrorHandler.ErrorHandler.LogFileWrite("printerques count =" + Convert.ToString(printerques.Count));

                            try
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("printerques.Add() 1");
                                var lstitem = _objlastusedprinter.Where(t => t.Printername == printer.FullName && t.productId == item.DG_AssociatedPrinters_ProductType_ID).FirstOrDefault();
                                ErrorHandler.ErrorHandler.LogFileWrite("printerques.Add() 2");
                                if (lstitem == null)
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite("lstitem == null");

                                    currentTime _objtitem = new currentTime();
                                    _objtitem.PrintDate = DateTime.Now;
                                    _objtitem.PrintTime = DateTime.Now.TimeOfDay;
                                    _objtitem.Printername = printer.FullName;
                                    _objtitem.productId = item.DG_AssociatedPrinters_ProductType_ID;
                                    _objlastusedprinter.Add(_objtitem);

                                    ErrorHandler.ErrorHandler.LogFileWrite("_objlastusedprinter1 _objtitem.Printername _objtitem.productId=" + Convert.ToString(_objtitem.productId) + "\n" + _objtitem.Printername);
                                }
                                else
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite("lstitem <> null");
                                }
                            }
                            catch
                            {
                                currentTime _objtitem1 = new currentTime();
                                _objtitem1.PrintDate = DateTime.Now;
                                _objtitem1.PrintTime = DateTime.Now.TimeOfDay;
                                _objtitem1.Printername = printer.FullName;
                                _objtitem1.productId = item.DG_AssociatedPrinters_ProductType_ID;
                                _objlastusedprinter.Add(_objtitem1);

                                ErrorHandler.ErrorHandler.LogFileWrite("_objtitem1 catch _objtitem1.Printername _objtitem1.productId=" + Convert.ToString(_objtitem1.productId) + "\n" + _objtitem1.Printername);
                            }
                        }
                        else
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("printer not ready FullName =" + Convert.ToString(printer.FullName));
                        }
                    }
                }
                if (printerques.Count > 0)
                {
                    currentTime target;
                    if (IsOrderwiseLoadBalancingActive == true)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("IsOrderwiseLoadBalancingActive true =" + Convert.ToString(IsOrderwiseLoadBalancingActive));
                        currentTime lstLastPrinterForOrder = _objlastusedprinter.Where(t => t.productId == productypeId && t.IsInError == false && t.OrderDetailId == orderDetailId).LastOrDefault();
                        if (lstLastPrinterForOrder == null || lstLastPrinterForOrder.IsInError)
                        {
                            target = _objlastusedprinter.Where(t => t.productId == productypeId && t.IsInError == false).OrderBy(t => t.PrintDate).OrderBy(t => t.PrintTime).FirstOrDefault();
                        }
                        else
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("lstLastPrinterForOrder final" + lstLastPrinterForOrder.Printername);
                            target = lstLastPrinterForOrder;
                        }
                        target.OrderDetailId = orderDetailId.ToInt32();
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("IsOrderwiseLoadBalancingActive false   =" + Convert.ToString(IsOrderwiseLoadBalancingActive));
                        target = _objlastusedprinter.Where(t => t.productId == productypeId && t.IsInError == false).OrderBy(t => t.PrintDate).OrderBy(t => t.PrintTime).FirstOrDefault();
                    }
                    target.PrintTime = DateTime.Now.TimeOfDay;

                    if (isPrinterReady(target.Printername))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("lstLastPrinterForOrder final printer name =" + target.Printername);
                        return target.Printername;
                    }
                    else
                    {
                        var offlineItem = _objlastusedprinter.Where(t => t.Printername == target.Printername).FirstOrDefault();
                        if (offlineItem != null)
                        {
                            _objlastusedprinter.Remove(offlineItem);
                            ErrorHandler.ErrorHandler.LogFileWrite(target.Printername + " is removed from last used printers list due to some printer error.");
                        }
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //ErrorHandler.ErrorHandler.LogFileWrite(ex.InnerException.Message + "@@@@" + ex.StackTrace.ToString());
                return null;
            }
        }

        #endregion

        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
                this.Left = desktopWorkingArea.Right - this.Width;
                this.Top = desktopWorkingArea.Bottom - this.Height;
                GetConfigData();

                PrintTimer.Start();

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void PrintTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (remapLastPrinter == 0 && DateTime.Now.Hour == 0)
                {
                    remapLastPrinter++;
                    _objlastusedprinter.Clear();
                }
                if (remapLastPrinter != 0 && DateTime.Now.Hour >= 23)
                    remapLastPrinter = 0;

                ServiceController controller = new ServiceController("Spooler");
                switch (controller.Status)
                {
                    case ServiceControllerStatus.Stopped:
                        controller.Start();
                        controller.Refresh();//Vinod
                        break;
                    case ServiceControllerStatus.Paused:
                        controller.Start();
                        controller.Refresh();//Vinod
                        break;
                }
                PrintImages();
            }
            catch (Exception ex)
            {
                int totMemoryAlloted = (int)GC.GetTotalMemory(false);
                GC.RemoveMemoryPressure(totMemoryAlloted); //Vinod

                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        #endregion

        #region Common Methods
        private void GetConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();

            QRCodeWebUrl = (new StoreSubStoreDataBusniess()).GetQRCodeWebUrl();

            if (ConfigValuesList != null)
            {
                var URL = ConfigValuesList.Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.QrCodeOnlineProductURL)).FirstOrDefault();
                if (URL != null)
                    QrCodeOnlineProductURL = URL.ConfigurationValue;

                var Text = ConfigValuesList.Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.QrCodeOnlineProductText)).FirstOrDefault();
                if (Text != null)
                    QrCodeOnlineProductText = Text.ConfigurationValue;

                var SmallWalletConfig = ConfigValuesList.Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.SmallWalletWith4ImageCutter)).FirstOrDefault();
                if (SmallWalletConfig != null)
                    IsSmallWalletwith4ImgCutter = Convert.ToBoolean(SmallWalletConfig.ConfigurationValue);
            }
            // objDigiPhotoDataServices = new DigiPhotoDataServices();
            // vw_GetConfigdata congfiguration = objDigiPhotoDataServices.GetConfigurationData(App.SubStoreId);
            ConfigBusiness confObj = new ConfigBusiness();
            //ConfigurationInfo congfiguration = confObj.GetConfigurationData(App.SubStoreId);
            //if (congfiguration != null)
            //    HotFolderPath = congfiguration.DG_Hot_Folder_Path.ToString();
            iMIXConfigurationInfo lstImixConfigValue = confObj.GetNewConfigValues(App.SubStoreId).Where(t => t.IMIXConfigurationMasterId == (int)ConfigParams.IsOrderwiseLoadBalancingActive).FirstOrDefault();
            //iMIXConfigurationValue lstImixConfigValue = objDigiPhotoDataServices.GetNewConfigValues(App.SubStoreId).Where(t => t.IMIXConfigurationMasterId == (int)ConfigParams.IsOrderwiseLoadBalancingActive).FirstOrDefault();
            if (lstImixConfigValue != null)
                IsOrderwiseLoadBalancingActive = lstImixConfigValue.ConfigurationValue.ToBoolean();
            else
                IsOrderwiseLoadBalancingActive = false;

            iMIXConfigurationInfo lstImixConfigValueMargin = confObj.GetNewConfigValues(App.SubStoreId).Where(t => t.IMIXConfigurationMasterId == (int)ConfigParams.CalenderPrintMargin).FirstOrDefault();
            if (lstImixConfigValueMargin != null && lstImixConfigValueMargin.ConfigurationValue != string.Empty)
                CalenderMarginValue = Convert.ToDouble(lstImixConfigValueMargin.ConfigurationValue);
            else
                CalenderMarginValue = 0;
            iMIXConfigurationInfo ImixConfigMarginValue4X6_2 = confObj.GetNewConfigValues(App.SubStoreId).Where(t => t.IMIXConfigurationMasterId == (int)ConfigParams.MarginValue4X6_2).FirstOrDefault();
            if (ImixConfigMarginValue4X6_2 != null && ImixConfigMarginValue4X6_2.ConfigurationValue != string.Empty)
                MarginValue4X6_2 = Convert.ToDouble(ImixConfigMarginValue4X6_2.ConfigurationValue);
            else
                MarginValue4X6_2 = 0;
            iMIXConfigurationInfo ImixConfigMarginValueUnique4X6_2 = confObj.GetNewConfigValues(App.SubStoreId).Where(t => t.IMIXConfigurationMasterId == (int)ConfigParams.MarginValueUnique4X6_2).FirstOrDefault();
            if (ImixConfigMarginValueUnique4X6_2 != null && ImixConfigMarginValueUnique4X6_2.ConfigurationValue != string.Empty)
                MarginValueUnique4X6_2 = Convert.ToDouble(ImixConfigMarginValueUnique4X6_2.ConfigurationValue);
            else
                MarginValueUnique4X6_2 = 0;
        }
        private bool ReadFile(PrinterQueueforPrint objPrinterImage)
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();            
            //return objDigiPhotoDataServices.IsReadyForPrint(objPrinterImage.DG_PrinterQueue_Pkey);
            PrinterBusniess priObj = new PrinterBusniess();
            return priObj.IsReadyForPrint(objPrinterImage.DG_PrinterQueue_Pkey);
        }
        private void PrintImages()
        {
            string extension = string.Empty;
            string[] filekey = null;
            string[] arrPanoramaPath = null;
            #region By KCB on 10 SEPT 2018 to resolve duplicate image number issue
            Imagebarcode5x7.Text = string.Empty;
            Imagebarcod4x6x2.Text = string.Empty;
            #endregion
            //DigiPhotoDataServices obj_Printer = new DigiPhotoDataServices();
            OrderBusiness orderBusiness = new OrderBusiness();
            PrinterBusniess printBusiness = new PrinterBusniess();


            //////getting printer queues from database
            var Image1 = printBusiness.GetPrinterQueue(App.SubStoreId, ref lastproductypeID);
            //var Image1 = printBusiness.GetPrinterQueue(1, ref lastproductypeID);
            //By KCB ON 23 MAY 20108 to avoid execute codes if no image found
            if (Image1 == null || Image1.Count <= 0)
                return;
            //End
            var Image = Image1.FirstOrDefault();
            if (Image.DG_PrinterQueue_ProductID == 84)
            {
                var image2 = Image1.Where(x => x.DG_Order_Details_Pkey == (Image.DG_Order_Details_Pkey + 1)).FirstOrDefault();
                if ((image2 != null && image2.DG_PrinterQueue_ProductID == 124) || (image2 != null && image2.DG_PrinterQueue_ProductID == 104))
                    Image1 = Image1.Take(2).OrderByDescending(x => x.DG_Order_Details_Pkey).ToList();

                Image = Image1.FirstOrDefault();
            }
            else if (Image.DG_PrinterQueue_ProductID == 104 || Image.DG_PrinterQueue_ProductID == 124)
            {
                Image1 = Image1.Take(2).OrderByDescending(x => x.DG_Order_Details_Pkey).ToList();
            }
            //End

            double borderMargin = 0;

            try
            {

                PrintTimer.Stop();
                bool printerfound = false;
                PrintDialog dlg = new PrintDialog();
                String PrinterName = string.Empty;
                if (Image != null)
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    /////////created by latika for table flow
                    List<iMIXRFIDTableWorkflowInfo> oImixRFID = new List<iMIXRFIDTableWorkflowInfo>();
                    ///////////end
                    subStorePrintPath = configBusiness.GetFolderStructureInfo(App.SubStoreId).PrintImagesPath;
                    var ProductType = orderBusiness.GetOrderDetailsByID((int)Image.DG_Order_Details_Pkey);
                    PanoramaBusiness _objPanoramaBusiness = new PanoramaBusiness();
                    bool IsProductPanoramic = _objPanoramaBusiness.IsProductPanorama_ProductId(Convert.ToInt16(ProductType.DG_Orders_Details_ProductType_pkey));

                    if (IsProductPanoramic)
                    {

                        string PrintImagePanoramaPath = configBusiness.GetFolderStructureInfo(App.SubStoreId).PrintImagesPath + "Panorama";

                        if (!Directory.Exists(PrintImagePanoramaPath))
                            Directory.CreateDirectory(PrintImagePanoramaPath);

                        subStorePrintPath = configBusiness.GetFolderStructureInfo(App.SubStoreId).PrintImagesPath + "Panorama";
                        //ErrorHandler.ErrorHandler.LogFileWrite(" subStorePrintPath Starts  on the line number 430 :-" + subStorePrintPath);
                    }
                    else
                    {
                        subStorePrintPath = configBusiness.GetFolderStructureInfo(App.SubStoreId).PrintImagesPath;
                    }

                    string photoname = Image.DG_Photos_RFID;
                    string TableName = Image.TableName;///created by latika for table flow
                    string orientaion = string.Empty;
                    string position = string.Empty;
                    var configInfoOrientation = configBusiness.GetNewConfigValues(App.SubStoreId).FindAll(x => x.IMIXConfigurationMasterId == (int)ConfigParams.BarCodeOrientation).FirstOrDefault();
                    var configInfoPostion = configBusiness.GetNewConfigValues(App.SubStoreId).FindAll(x => x.IMIXConfigurationMasterId == (int)ConfigParams.BarCodePostion).FirstOrDefault();

                    if (configInfoOrientation != null && configInfoPostion != null)
                    {
                        orientaion = configInfoOrientation.ConfigurationValue.ToString();
                        position = configInfoPostion.ConfigurationValue.ToString();
                    }

                    if (configInfoPostion == null)
                    {
                        ErrorHandler.ErrorHandler.LogError("Print Image 506 :  " + photoname);
                        Imagebarcodenormal.Text = photoname;
                    }
                    ErrorHandler.ErrorHandler.LogError("Print Image 509 :  " + photoname);
                    var configInfo = configBusiness.GetNewConfigValues(App.SubStoreId).FindAll(x => x.IMIXConfigurationMasterId == (int)ConfigParams.ImageBarCodeColor).FirstOrDefault();
                    if (configInfo != null)
                    {
                        System.Windows.Media.Color color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(configInfo.ConfigurationValue);
                        Imagebarcodenormal.Background = new SolidColorBrush(color);
                        ImagebarcodQrPrint.Background = new SolidColorBrush(color);
                    }
                    //////////////created by latika for table flow
                    var lstRfidInfoI = configBusiness.GetTableworkFlowinfo(Image.LocationID);
                    foreach (var _rfidInfo in lstRfidInfoI)
                    {
                        if (TableName != null)
                        {



                            if (_rfidInfo.TypesName == "Table" && (!string.IsNullOrEmpty(Image.TableName)))
                            {

                                // orientaion = "Horizontal";
                                TableFlowSetting(_rfidInfo.Orientation, _rfidInfo.Position, imgTableName, Image.TableName, _rfidInfo.MarginRight, _rfidInfo.MarginTop, _rfidInfo.MarginLeft, _rfidInfo.MarginBottom,
                                 _rfidInfo.BackColor, _rfidInfo.FontFamily, _rfidInfo.FontStyle, _rfidInfo.FontWeight, _rfidInfo.FontSize, _rfidInfo.Font, _rfidInfo.Position);
                            }
                            else if (_rfidInfo.TypesName == "Name" && (!string.IsNullOrEmpty(Image.GuestName)))
                            {

                                TableFlowSetting(_rfidInfo.Orientation, _rfidInfo.Position, imgGuestName, DigiPhoto.CryptorEngine.Decrypt(Image.GuestName, true), _rfidInfo.MarginRight + 10, _rfidInfo.MarginTop, _rfidInfo.MarginLeft, _rfidInfo.MarginBottom,
                                 _rfidInfo.BackColor, _rfidInfo.FontFamily, _rfidInfo.FontStyle, _rfidInfo.FontWeight, _rfidInfo.FontSize, _rfidInfo.Font, _rfidInfo.Position);

                            }
                            else if (_rfidInfo.TypesName == "Email" && (!string.IsNullOrEmpty(Image.EmailID)))
                            {
                                //orientaion = "Horizontal";
                                TableFlowSetting(_rfidInfo.Orientation, _rfidInfo.Position, imgEmailID, DigiPhoto.CryptorEngine.Decrypt(Image.EmailID, true), _rfidInfo.MarginRight, _rfidInfo.MarginTop, _rfidInfo.MarginLeft, _rfidInfo.MarginBottom,
                             _rfidInfo.BackColor, _rfidInfo.FontFamily, _rfidInfo.FontStyle, _rfidInfo.FontWeight, _rfidInfo.FontSize, _rfidInfo.Font, _rfidInfo.Position);

                            }
                            else if (_rfidInfo.TypesName == "Contact" && (!string.IsNullOrEmpty(Image.ContactNo)))
                            {

                                //  orientaion = "Vertical";
                                TableFlowSetting(_rfidInfo.Orientation, _rfidInfo.Position, imgContactNo, DigiPhoto.CryptorEngine.Decrypt(Image.ContactNo, true), _rfidInfo.MarginRight, _rfidInfo.MarginTop, _rfidInfo.MarginLeft, _rfidInfo.MarginBottom,
                             _rfidInfo.BackColor, _rfidInfo.FontFamily, _rfidInfo.FontStyle, _rfidInfo.FontWeight, _rfidInfo.FontSize, _rfidInfo.Font, _rfidInfo.Position);

                            }


                        }
                    }
                    /////////////////////////End by latika
                    if (ReadFile(Image)) // check file is rendered
                    {
                        // Checked small wallet product
                        if (Image.DG_PrinterQueue_ProductID == 131)
                        {
                            string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                            foreach (string fileName in _nameary)
                            {
                                if (!File.Exists(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (fileName + ".jpg"))))
                                {
                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                    ErrorHandler.ErrorHandler.LogFileWrite("Error Line 572 : " + Image.DG_PrinterQueue_Pkey.ToString() + " not found.");
                                    //return;
                                }
                            }
                        }
                        else if (Image.DG_PrinterQueue_ProductID == 79 || Image.DG_PrinterQueue_ProductID == 98 || Image.DG_PrinterQueue_ProductID == 101 || Image.DG_PrinterQueue_ProductID == 4 || Image.DG_PrinterQueue_ProductID == 105)
                        {
                            string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                            foreach (string fileName in _nameary)
                            {
                                if (!File.Exists(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                {
                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                    ErrorHandler.ErrorHandler.LogFileWrite("Error : " + Image.DG_PrinterQueue_Pkey.ToString() + " not found.");
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (!File.Exists(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                            {
                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                ErrorHandler.ErrorHandler.LogFileWrite("Error : " + Image.DG_PrinterQueue_Pkey.ToString() + " not found.");
                                return;
                            }
                        }
                        // check file is rendered
                        #region 

                        var server = new PrintServer();
                        var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
                        if (Image != null)
                        {
                            string quename = GetMainQueue(ProductType.DG_Orders_Details_ProductType_pkey, ProductType.DG_Orders_ID);
                            var queue = queues.Where(t => t.FullName == quename).FirstOrDefault();
                            if (queue != null)
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("Printing Image ID : " + Image.DG_PrinterQueue_Pkey.ToString() + " ");
                                #region Change queue based on product and printers
                                string settingsString = string.Empty;
                                //4*6 Product Type
                                if (ProductType.DG_Orders_Details_ProductType_pkey == 30)
                                {
                                    #region 4*6 Settings
                                    using (var printms = queue.DefaultPrintTicket.GetXmlStream())
                                    {
                                        StreamWriter sw = new StreamWriter(printms);
                                        sw.Flush();
                                        printms.Position = 0;
                                        StreamReader sr = new StreamReader(printms);
                                        settingsString = sr.ReadToEnd();
                                    }
                                    if (settingsString.Contains("_6_0x8_0")) //Kodak Printers 6800, 6850 and 7000
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite("Line No : 612 ");
                                        settingsString = settingsString.Replace("_6_0x8_0", "_4_0x6_0");
                                        settingsString = settingsString.Replace("156100", "104900");
                                        settingsString = settingsString.Replace("206000", "156100");
                                    }
                                    else if (settingsString.Contains("ns0000:PC") && quename.Contains("DS-RX1")) //DNP RX1 Printers with 6*4
                                    {
                                        settingsString = settingsString.Replace("ns0000:PC", "ns0000:PR_4x6");
                                        settingsString = settingsString.Replace("156125", "104987");
                                        settingsString = settingsString.Replace("104987", "156125");
                                    }
                                    else if (settingsString.Contains("ns0000:A5STD") && quename.Contains("DS-RX1")) //DNP RX1 Printers with 6*8 quename
                                    {
                                        settingsString = settingsString.Replace("ns0000:A5STD", "ns0000:PR_4x6");
                                        settingsString = settingsString.Replace("206248", "156125");
                                        settingsString = settingsString.Replace("156125", "104987");
                                    }
                                    else if (settingsString.Contains("A5STD")) //DNP Printers DS40
                                    {
                                        settingsString = settingsString.Replace("A5STD", "PC");
                                        settingsString = settingsString.Replace("156100", "156100");
                                        settingsString = settingsString.Replace("206200", "104900");
                                    }
                                    else if (settingsString.Contains("User0000000505")) // Mitsubishi Printers Mitsubishi CPD80D, Mitsubishi CP70D
                                    {
                                        settingsString = settingsString.Replace("User0000000505", "User0000000501");
                                        settingsString = settingsString.Replace("158000", "105000");
                                        settingsString = settingsString.Replace("206000", "158000");
                                    }
                                    MemoryStream mStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(settingsString));
                                    PrintTicket ticket = new PrintTicket(mStream);
                                    System.Printing.ValidationResult result = queue.MergeAndValidatePrintTicket(queue.DefaultPrintTicket, ticket);
                                    queue.UserPrintTicket = result.ValidatedPrintTicket;
                                    queue.Commit();
                                    #endregion
                                }
                                //For other 6*8 product types    
                                #region 6*8 Settings
                                else if (ProductType.DG_Orders_Details_ProductType_pkey == 1 || ProductType.DG_Orders_Details_ProductType_pkey == 3 || ProductType.DG_Orders_Details_ProductType_pkey == 4
                                    || ProductType.DG_Orders_Details_ProductType_pkey == 5 || ProductType.DG_Orders_Details_ProductType_pkey == 98 || ProductType.DG_Orders_Details_ProductType_pkey == 100
                                    || ProductType.DG_Orders_Details_ProductType_pkey == 101 || ProductType.DG_Orders_Details_ProductType_pkey == 102 || ProductType.DG_Orders_Details_ProductType_pkey == 103 || ProductType.DG_Orders_Details_ProductType_pkey == 104
                    //|| ProductType.DG_Orders_Details_ProductType_pkey == 105 || ProductType.DG_Orders_Details_ProductType_pkey == 1094)
                    || ProductType.DG_Orders_Details_ProductType_pkey == 105 || ProductType.DG_Orders_Details_ProductType_pkey == 124
                    || Image.DG_Orders_ProductType_Name.ToLower() == "licence product")
                                {

                                    using (var printms = queue.DefaultPrintTicket.GetXmlStream())
                                    {
                                        StreamWriter sw = new StreamWriter(printms);
                                        sw.Flush();
                                        printms.Position = 0;
                                        StreamReader sr = new StreamReader(printms);
                                        settingsString = sr.ReadToEnd();
                                    }
                                    ErrorHandler.ErrorHandler.LogFileWrite("Line No : 666");
                                    if (settingsString.Contains("_4_0x6_0")) //Kodak Printers 6800, 6850 and 7000
                                    {
                                        settingsString = settingsString.Replace("_4_0x6_0", "_6_0x8_0");
                                        settingsString = settingsString.Replace("104900", "156100");
                                        settingsString = settingsString.Replace("156100", "206000");
                                    }
                                    else if (settingsString.Contains("ns0000:PC") && quename.Contains("DS-RX1")) //DNP RX1 Printers with 6*4 and 6*8 settings
                                    {
                                        settingsString = settingsString.Replace("ns0000:PC", "ns0000:A5STD");
                                        settingsString = settingsString.Replace("156125", "156125");
                                        settingsString = settingsString.Replace("104987", "206248");
                                    }
                                    else if (settingsString.Contains("ns0000:PR_4x6") && quename.Contains("DS-RX1")) //DNP Printers RX1 with 4*6 settings
                                    {
                                        settingsString = settingsString.Replace("ns0000:PR_4x6", "ns0000:A5STD");
                                        settingsString = settingsString.Replace("156125", "206248");
                                        settingsString = settingsString.Replace("104987", "156125");
                                    }
                                    else if (settingsString.Contains("User0000000501")) // Mitsubishi Printers Mitsubishi CPD80D, Mitsubishi CP70D
                                    {
                                        settingsString = settingsString.Replace("User0000000501", "User0000000505");
                                        settingsString = settingsString.Replace("105000", "158000");
                                        settingsString = settingsString.Replace("158000", "206000");
                                    }
                                    else if (settingsString.Contains("ns0000:PC")) //DNP Printers DS40 
                                    {
                                        settingsString = settingsString.Replace("ns0000:PC", "ns0000:A5STD");
                                        settingsString = settingsString.Replace("156100", "156100");
                                        settingsString = settingsString.Replace("104900", "206200");
                                    }
                                    //else if (settingsString.Contains("ns0000:PC") || settingsString.Contains("ns0000:PR_4x6")) //DNP RX1 Printers 
                                    //{
                                    //    settingsString = settingsString.Replace("ns0000:PC", "ns0000:PR_4x6");
                                    //    settingsString = settingsString.Replace("156125", "104987");
                                    //    settingsString = settingsString.Replace("104987", "156125");
                                    //}
                                    MemoryStream mStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(settingsString));
                                    PrintTicket ticket = new PrintTicket(mStream);
                                    System.Printing.ValidationResult result = queue.MergeAndValidatePrintTicket(queue.DefaultPrintTicket, ticket);
                                    queue.UserPrintTicket = result.ValidatedPrintTicket;
                                    queue.Commit();
                                }
                                #endregion
                                //For 36*24 product types
                                #region 36*24 Settings
                                else if (ProductType.DG_Orders_Details_ProductType_pkey == 6)
                                {
                                    using (var printms = queue.DefaultPrintTicket.GetXmlStream())
                                    {
                                        StreamWriter sw = new StreamWriter(printms);
                                        sw.Flush();
                                        printms.Position = 0;
                                        StreamReader sr = new StreamReader(printms);
                                        settingsString = sr.ReadToEnd();
                                    }
                                    if (settingsString.Contains("ns0000:RollPaperLFP")) //EPSON Stylus Pro 7800 printers
                                    {
                                        settingsString = settingsString.Replace("psk:NorthAmericaLetter", "ns0000:PaperA1Plus");
                                        settingsString = settingsString.Replace("215900", "609600");
                                        settingsString = settingsString.Replace("279400", "914400");
                                    }
                                    MemoryStream mStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(settingsString));
                                    PrintTicket ticket = new PrintTicket(mStream);
                                    ticket.PageBorderless = PageBorderless.Borderless;
                                    System.Printing.ValidationResult result = queue.MergeAndValidatePrintTicket(queue.DefaultPrintTicket, ticket);
                                    queue.UserPrintTicket = result.ValidatedPrintTicket;
                                    queue.Commit();
                                }
                                #endregion
                                //For 24*18 product type
                                #region 24*18 Settings
                                else if (ProductType.DG_Orders_Details_ProductType_pkey == 7)
                                {
                                    using (var printms = queue.DefaultPrintTicket.GetXmlStream())
                                    {
                                        StreamWriter sw = new StreamWriter(printms);
                                        sw.Flush();
                                        printms.Position = 0;
                                        StreamReader sr = new StreamReader(printms);
                                        settingsString = sr.ReadToEnd();
                                    }
                                    if (settingsString.Contains("ns0000:RollPaperLFP")) //EPSON Stylus Pro 7800 printers
                                    {
                                        settingsString = settingsString.Replace("ns0000:PaperA1Plus", "ns0000:EPUserDefinePaper768");
                                        settingsString = settingsString.Replace("609600", "609600");
                                        settingsString = settingsString.Replace("914400", "457200");
                                    }
                                    MemoryStream mStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(settingsString));
                                    PrintTicket ticket = new PrintTicket(mStream);
                                    ticket.PageBorderless = PageBorderless.Borderless;
                                    System.Printing.ValidationResult result = queue.MergeAndValidatePrintTicket(queue.DefaultPrintTicket, ticket);
                                    queue.UserPrintTicket = result.ValidatedPrintTicket;
                                    queue.Commit();
                                }
                                #endregion
                                #endregion
                                dlg.PrintQueue = queue;
                                printerfound = true;
                            }
                            if (printerfound)
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("Line No : 768");
                                int totalPrintCopy = 0;
                                int productParentOrderDetailId = ProductType.DG_Orders_Details_LineItem_ParentID.HasValue ? ProductType.DG_Orders_Details_LineItem_ParentID.Value : 0;
                                if (productParentOrderDetailId > 0)     //Some ParentId may be -1, exclude -1 & 0
                                {
                                    var PackageType = orderBusiness.GetOrderDetailsByID(productParentOrderDetailId);
                                    totalPrintCopy = (ProductType.DG_Orders_LineItems_Quantity * PackageType.DG_Orders_LineItems_Quantity);
                                }
                                else
                                {
                                    totalPrintCopy = ProductType.DG_Orders_LineItems_Quantity;
                                }

                                if (Image.DG_Photos_RFID.Split(',').Length <= 1 && Image.DG_PrinterQueue_ProductID != 131) // if image does not contain comma sperate
                                {
                                    if (Image.DG_PrinterQueue_ProductID == 132) //passport photo
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite("Assign image to grid 786");
                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                        {

                                            BitmapImage bi = new BitmapImage();
                                            MemoryStream ms = new MemoryStream();
                                            fileStream.CopyTo(ms);
                                            ms.Seek(0, SeekOrigin.Begin);
                                            fileStream.Close();
                                            bi.BeginInit();
                                            bi.StreamSource = ms;
                                            bi.EndInit();
                                            string xyz = string.Empty;
                                            //LayoutImage(orientaion, position, null, photoname);
                                            imgwalletsmallp1.Source = bi;
                                            imgwalletsmallp2.Source = bi;
                                            imgwalletsmallp3.Source = bi;
                                            imgwalletsmallp4.Source = bi;
                                            imgwalletsmallp5.Source = bi;
                                            imgwalletsmallp6.Source = bi;

                                            Grd4by6Passport.Width = 600;
                                            Grd4by6Passport.Height = 400;
                                            borderMargin = 0;
                                            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                            //-----------------------------
                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(Grd4by6Passport, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);
                                            Grd4by6Passport.UpdateLayout();
                                            System.Windows.Size size;
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            Grd4by6Passport.Measure(size);
                                            Grd4by6Passport.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));

                                            ErrorHandler.ErrorHandler.LogFileWrite("Passport product is printing:" + Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite("dlg.PrintTicket.PageOrientation:" + dlg.PrintTicket.PageOrientation);
                                            ErrorHandler.ErrorHandler.LogFileWrite("size: Height" + Convert.ToString(size.Height) + " Width:" + Convert.ToString(size.Width));

                                            //--------------
                                            int copy = 1;
                                            while (totalPrintCopy >= copy)
                                            {
                                                dlg.PrintVisual(Grd4by6Passport, Image.DG_PrinterQueue_Pkey.ToString());
                                                copy++;
                                            }

                                            xyz = dlg.PrintQueue.FullName.ToString();
                                            PrinterBusniess priObj = new PrinterBusniess();
                                            priObj.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                    }
                                    else if (Image.DG_PrinterQueue_ProductID == 5 || Image.DG_PrinterQueue_ProductID == 3) // for 4 Large Wallets and 4 Small Wallets
                                    {
                                        #region Commented to resolve border cut issue
                                        //#region for 4 Large Wallets and 4 Small Wallets
                                        //int copy = 1;

                                        //try
                                        //{
                                        //    //Added by Vins
                                        //    //Start
                                        //    if (Image.DG_PrinterQueue_ProductID == 5)
                                        //    {
                                        //        bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());
                                        //        var filepath = Image.DG_PrinterQueue_Pkey.ToString();
                                        //        //start Vins
                                        //        if (isVerticalImage)
                                        //        {
                                        //            ErrorHandler.ErrorHandler.LogFileWrite("Vertical Small wallet");
                                        //            ResizeWPFImageinAspectRatio(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")), 6743, 4500, System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")));//Currently getting 6743*5057
                                        //        }                                                
                                        //    }
                                        //    //End

                                        //    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                        //    {
                                        //        BitmapImage bi = new BitmapImage();
                                        //        MemoryStream ms = new MemoryStream();
                                        //        fileStream.CopyTo(ms);
                                        //        ms.Seek(0, SeekOrigin.Begin);
                                        //        fileStream.Close();

                                        //        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                        //        bi.BeginInit();
                                        //        bi.StreamSource = ms;
                                        //        bi.EndInit();
                                        //        string xyz = string.Empty;
                                        //        // System.Threading.Thread.Sleep(2000);
                                        //        if (bi.Width > bi.Height)
                                        //            dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                        //        else
                                        //            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                        //        if (Image.DG_PrinterQueue_ProductID == 5) //small wallet
                                        //        {
                                        //            LayoutImage(orientaion, position, Imagebarcodenormal_Copy1, photoname);
                                        //            imgwalletsmall1.Source = bi;
                                        //            imgwalletsmall2.Source = bi;
                                        //            imgwalletsmall3.Source = bi;
                                        //            imgwalletsmall4.Source = bi;
                                        //            imgwallet1.Source = null;
                                        //            imgwallet2.Source = null;
                                        //            imgwallet3.Source = null;
                                        //            imgwallet4.Source = null;

                                        //            if (bi.Width > bi.Height)
                                        //            {
                                        //                Grd8by6Small.Width = 800;
                                        //                Grd8by6Small.Height = 600;
                                        //                if (ProductType.DG_IsBorder == true)
                                        //                {
                                        //                    borderMargin = 20;
                                        //                }
                                        //                else
                                        //                    borderMargin = 0;
                                        //                if (IsSmallWalletwith4ImgCutter)
                                        //                {
                                        //                    ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter = true");
                                        //                    imgwalletsmall1.Margin = new Thickness(118, 105, 13, 20);
                                        //                    imgwalletsmall2.Margin = new Thickness(21, 105, 110, 20);
                                        //                    imgwalletsmall3.Margin = new Thickness(118, 2, 13, 122);
                                        //                    imgwalletsmall4.Margin = new Thickness(21, 2, 110, 122);
                                        //                }
                                        //                else
                                        //                {
                                        //                    ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter = false");

                                        //                    prtGrdWalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    prtGrdWalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    prtGrdWalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                        //                    prtGrdWalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));

                                        //                    //imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    //imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                        //                    //imgwalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));
                                        //                }
                                        //                dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                        //            }
                                        //            else
                                        //            {
                                        //                Grd8by6Small.Width = 600;
                                        //                Grd8by6Small.Height = 800;
                                        //                if (ProductType.DG_IsBorder == true)
                                        //                {
                                        //                    borderMargin = 20;
                                        //                }
                                        //                else
                                        //                    borderMargin = 0;

                                        //                ErrorHandler.ErrorHandler.LogFileWrite("borderMargin = " + Convert.ToString(borderMargin));
                                        //                if (IsSmallWalletwith4ImgCutter)
                                        //                {
                                        //                    imgwalletsmall1.Margin = new Thickness(105, 110, 20, 21);
                                        //                    imgwalletsmall2.Margin = new Thickness(2, 110, 122, 21);
                                        //                    imgwalletsmall3.Margin = new Thickness(105, 13, 20, 118);
                                        //                    imgwalletsmall4.Margin = new Thickness(2, 13, 122, 118);
                                        //                }
                                        //                else
                                        //                {
                                        //                    //imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    //imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                        //                    //imgwalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));
                                        //                    //imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    ////imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    //imgwalletsmall2.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall4.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);


                                        //                    //imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (borderMargin), borderMargin, borderMargin);
                                        //                    ////imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    //imgwalletsmall2.Margin = new Thickness(90 + (borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), 90 + (borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall4.Margin = new Thickness(90 + (borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);

                                        //                    //imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    //imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    //imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                        //                    //imgwalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));

                                        //                    prtGrdWalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                    prtGrdWalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                    prtGrdWalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                        //                    prtGrdWalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));



                                        //                }
                                        //                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                        //            }
                                        //            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                        //            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                        //            var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

                                        //            Grd8by6Small.UpdateLayout();
                                        //            Grd8by6Small.Measure(size);
                                        //            Grd8by6Small.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));

                                        //            while (totalPrintCopy >= copy)
                                        //            {
                                        //                dlg.PrintVisual(Grd8by6Small, Image.DG_PrinterQueue_Pkey.ToString());
                                        //                copy++;
                                        //            }
                                        //            xyz = dlg.PrintQueue.FullName.ToString();
                                        //            PrinterBusniess priObj = new PrinterBusniess();
                                        //            priObj.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        //            //obj_Printer.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        //        }
                                        //        else // Large Wallet
                                        //        {
                                        //            if (string.IsNullOrWhiteSpace(position))
                                        //            {
                                        //                position = "Top Left";
                                        //                orientaion = "Vertical";
                                        //            }

                                        //            if (Image.DG_PrinterQueue_ProductID == 126)
                                        //            {
                                        //                position = "Top Right";
                                        //                orientaion = "Horizontal";
                                        //            }

                                        //            ErrorHandler.ErrorHandler.LogFileWrite(" Print photo Name :-" + DateTime.Now.ToLongTimeString() + " PhotoName: " + photoname);
                                        //            LayoutImage(orientaion, position, Imagebarcodenormal_Copy, photoname);
                                        //            imgwalletsmall1.Source = null;
                                        //            imgwalletsmall2.Source = null;
                                        //            imgwalletsmall3.Source = null;
                                        //            imgwalletsmall4.Source = null;
                                        //            imgwallet1.Source = bi;
                                        //            imgwallet2.Source = bi;
                                        //            imgwallet3.Source = bi;
                                        //            imgwallet4.Source = bi;
                                        //            if (bi.Width > bi.Height)
                                        //            {
                                        //                Grd8by6.Width = 800;
                                        //                Grd8by6.Height = 600;
                                        //                if (ProductType.DG_IsBorder == true)
                                        //                {
                                        //                    borderMargin = 20;
                                        //                }
                                        //                else
                                        //                    borderMargin = 0;
                                        //                prtGrdWallet1.Margin = new Thickness(90 + (20 - borderMargin), 40 + (20 - borderMargin), borderMargin, borderMargin);
                                        //                prtGrdWallet2.Margin = new Thickness(borderMargin, 40 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                        //                prtGrdWallet3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 40 + (20 - borderMargin));
                                        //                prtGrdWallet4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 40 + (20 - borderMargin));
                                        //            }
                                        //            else
                                        //            {
                                        //                Grd8by6.Width = 600;
                                        //                Grd8by6.Height = 800;
                                        //                if (ProductType.DG_IsBorder == true)
                                        //                {
                                        //                    prtGrdWallet1.Margin = new Thickness(50, 95, 10, 20);
                                        //                    prtGrdWallet2.Margin = new Thickness(10, 95, 50, 20);
                                        //                    prtGrdWallet3.Margin = new Thickness(50, 20, 10, 95);
                                        //                    prtGrdWallet4.Margin = new Thickness(10, 20, 50, 95);
                                        //                }
                                        //                else
                                        //                {
                                        //                    prtGrdWallet1.Margin = new Thickness(60, 115, 0, 0);
                                        //                    prtGrdWallet2.Margin = new Thickness(0, 115, 60, 0);
                                        //                    prtGrdWallet3.Margin = new Thickness(60, 0, 0, 115);
                                        //                    prtGrdWallet4.Margin = new Thickness(0, 0, 60, 115);
                                        //                }
                                        //            }
                                        //            Grd8by6.UpdateLayout();
                                        //            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                        //            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                        //            var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                        //            Grd8by6.Measure(size);
                                        //            Grd8by6.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                        //            while (totalPrintCopy >= copy)
                                        //            {
                                        //                dlg.PrintVisual(Grd8by6, Image.DG_PrinterQueue_Pkey.ToString());
                                        //                copy++;
                                        //            }
                                        //            xyz = dlg.PrintQueue.FullName.ToString();
                                        //            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        //            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        //        }
                                        //    }
                                        //}
                                        //catch (Exception ex)
                                        //{
                                        //    ErrorHandler.ErrorHandler.LogError(ex);
                                        //    GC.AddMemoryPressure(10000);
                                        //    MemoryManagement.FlushMemory();
                                        //    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                        //}
                                        //#endregion for 4 Large Wallets and 4 Small Wallets 
                                        #endregion

                                        #region for 4 Large Wallets and 4 Small Wallets
                                        int copy = 1;

                                        try
                                        {
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();
                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                bi.EndInit();
                                                string xyz = string.Empty;
                                                // System.Threading.Thread.Sleep(2000);
                                                if (bi.Width > bi.Height)
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                else
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                                if (Image.DG_PrinterQueue_ProductID == 5) //small wallet
                                                {
                                                    LayoutImage(orientaion, position, Imagebarcodenormal_Copy1, photoname);
                                                    imgwalletsmall1.Source = bi;
                                                    imgwalletsmall2.Source = bi;
                                                    imgwalletsmall3.Source = bi;
                                                    imgwalletsmall4.Source = bi;
                                                    imgwallet1.Source = null;
                                                    imgwallet2.Source = null;
                                                    imgwallet3.Source = null;
                                                    imgwallet4.Source = null;

                                                    if (bi.Width > bi.Height)
                                                    {
                                                        Grd8by6Small.Width = 800;
                                                        Grd8by6Small.Height = 600;
                                                        if (ProductType.DG_IsBorder == true)
                                                        {
                                                            borderMargin = 20;
                                                        }
                                                        else
                                                            borderMargin = 0;
                                                        if (IsSmallWalletwith4ImgCutter)
                                                        {
                                                            imgwalletsmall1.Margin = new Thickness(118, 105, 13, 20);
                                                            imgwalletsmall2.Margin = new Thickness(21, 105, 110, 20);
                                                            imgwalletsmall3.Margin = new Thickness(118, 2, 13, 122);
                                                            imgwalletsmall4.Margin = new Thickness(21, 2, 110, 122);
                                                        }
                                                        else
                                                        {
                                                            imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                                            imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                            imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                                            imgwalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));
                                                        }
                                                        dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                    }
                                                    else
                                                    {
                                                        Grd8by6Small.Width = 600;
                                                        Grd8by6Small.Height = 800;
                                                        if (ProductType.DG_IsBorder == true)
                                                        {
                                                            borderMargin = 20;
                                                        }
                                                        else
                                                            borderMargin = 0;
                                                        if (IsSmallWalletwith4ImgCutter)
                                                        {
                                                            imgwalletsmall1.Margin = new Thickness(105, 110, 20, 21);
                                                            imgwalletsmall2.Margin = new Thickness(2, 110, 122, 21);
                                                            imgwalletsmall3.Margin = new Thickness(105, 13, 20, 118);
                                                            imgwalletsmall4.Margin = new Thickness(2, 13, 122, 118);
                                                        }
                                                        else
                                                        {
                                                            imgwalletsmall1.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                                            imgwalletsmall2.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                            imgwalletsmall3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));
                                                            imgwalletsmall4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));
                                                        }
                                                        dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    }
                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

                                                    Grd8by6Small.UpdateLayout();
                                                    Grd8by6Small.Measure(size);
                                                    Grd8by6Small.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));

                                                    while (totalPrintCopy >= copy)
                                                    {
                                                        dlg.PrintVisual(Grd8by6Small, Image.DG_PrinterQueue_Pkey.ToString());
                                                        copy++;
                                                    }
                                                    xyz = dlg.PrintQueue.FullName.ToString();
                                                    PrinterBusniess priObj = new PrinterBusniess();
                                                    priObj.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                    //obj_Printer.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                }
                                                else // Large Wallet
                                                {
                                                    if (string.IsNullOrWhiteSpace(position))
                                                    {
                                                        position = "Top Left";
                                                        orientaion = "Vertical";
                                                    }
                                                    LayoutImage(orientaion, position, Imagebarcodenormal_Copy, photoname);
                                                    imgwalletsmall1.Source = null;
                                                    imgwalletsmall2.Source = null;
                                                    imgwalletsmall3.Source = null;
                                                    imgwalletsmall4.Source = null;
                                                    imgwallet1.Source = bi;
                                                    imgwallet2.Source = bi;
                                                    imgwallet3.Source = bi;
                                                    imgwallet4.Source = bi;
                                                    if (bi.Width > bi.Height)
                                                    {
                                                        Grd8by6.Width = 800;
                                                        Grd8by6.Height = 600;
                                                        if (ProductType.DG_IsBorder == true)
                                                        {
                                                            borderMargin = 20;
                                                        }
                                                        else
                                                            borderMargin = 0;
                                                        prtGrdWallet1.Margin = new Thickness(90 + (20 - borderMargin), 40 + (20 - borderMargin), borderMargin, borderMargin);
                                                        prtGrdWallet2.Margin = new Thickness(borderMargin, 40 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                        prtGrdWallet3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 40 + (20 - borderMargin));
                                                        prtGrdWallet4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 40 + (20 - borderMargin));
                                                    }
                                                    else
                                                    {
                                                        Grd8by6.Width = 600;
                                                        Grd8by6.Height = 800;
                                                        if (ProductType.DG_IsBorder == true)
                                                        {
                                                            prtGrdWallet1.Margin = new Thickness(50, 95, 10, 20);
                                                            prtGrdWallet2.Margin = new Thickness(10, 95, 50, 20);
                                                            prtGrdWallet3.Margin = new Thickness(50, 20, 10, 95);
                                                            prtGrdWallet4.Margin = new Thickness(10, 20, 50, 95);
                                                        }
                                                        else
                                                        {
                                                            prtGrdWallet1.Margin = new Thickness(60, 115, 0, 0);
                                                            prtGrdWallet2.Margin = new Thickness(0, 115, 60, 0);
                                                            prtGrdWallet3.Margin = new Thickness(60, 0, 0, 115);
                                                            prtGrdWallet4.Margin = new Thickness(0, 0, 60, 115);
                                                        }
                                                    }
                                                    Grd8by6.UpdateLayout();
                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                    Grd8by6.Measure(size);
                                                    Grd8by6.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                                    while (totalPrintCopy >= copy)
                                                    {
                                                        dlg.PrintVisual(Grd8by6, Image.DG_PrinterQueue_Pkey.ToString());
                                                        copy++;
                                                    }
                                                    xyz = dlg.PrintQueue.FullName.ToString();
                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                        }
                                        #endregion for 4 Large Wallets and 4 Small Wallets
                                    }
                                    else if (Image.DG_PrinterQueue_ProductID == 100)
                                    {
                                        #region calendar
                                        var queueId = Image.DG_PrinterQueue_Pkey;
                                        CalenderBusiness objBizz = new CalenderBusiness();
                                        List<ItemTemplateDetailModel> lstItemtemplatePath = objBizz.GetItemTemplatePath(queueId);
                                        var masterTemplateId = lstItemtemplatePath.FirstOrDefault().MasterTemplateId;
                                        var itemTemplateId = lstItemtemplatePath.FirstOrDefault().Id;
                                        List<ItemTemplateDetailModel> lstTemplateDetails = objBizz.GetItemTemplateDetail();
                                        List<ItemTemplateDetailModel> lstMasterFilter = lstTemplateDetails.Where(x => x.MasterTemplateId == masterTemplateId).ToList();
                                        var filePath = lstMasterFilter.Where(x => x.TemplateType == itemTemplateId).FirstOrDefault().FilePath;
                                        ImagebarcodCalender.Text = Image.DG_Photos_RFID.ToString();
                                        // Position & Orienation layout
                                        LayoutImage(orientaion, position, ImagebarcodCalender, ImagebarcodCalender.Text);
                                        int copy;
                                        try
                                        {
                                            //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                //BitmapImage bi = new BitmapImage();
                                                //MemoryStream ms = new MemoryStream();
                                                //fileStream.CopyTo(ms);
                                                //ms.Seek(0, SeekOrigin.Begin);
                                                //fileStream.Close();
                                                //bi.BeginInit();
                                                //bi.StreamSource = ms;
                                                //bi.EndInit();
                                                //string xyz = "";
                                                //// System.Threading.Thread.Sleep(2000);
                                                //if (bi.Width > bi.Height)
                                                //    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                //else
                                                //    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                var filepath = Image.DG_PrinterQueue_Pkey.ToString();


                                                // using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                                // using (FileStream fileStream = File.OpenRead(@"D:\DigiImages\20151015\1510678_4.jpg"))
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                                {
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();
                                                    imgCalendar6x8_1.Source = bi;
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    //if (bi.Width > bi.Height)
                                                    //    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                    //else
                                                    //    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    imgCalendar6x8_1.Margin = new Thickness(10, 10, 10, CalenderMarginValue);
                                                }

                                                ////have to define path here for image 2
                                                var calenderpath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "ItemTemplate", "Calendar");
                                                var finalpath = calenderpath + "\\" + filePath;
                                                // using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                                using (FileStream fileStream = File.OpenRead(finalpath))
                                                {
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();
                                                    imgCalendar6x8_2.Source = bi;
                                                    // imgwallet2.Margin = new Thickness(borderMargin, 40 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                    imgCalendar6x8_2.Margin = new Thickness(1, 1, 1, 5);
                                                }

                                                Calendar6x8.UpdateLayout();
                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                Calendar6x8.Measure(size);
                                                Calendar6x8.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                                //while (totalPrintCopy >= copy)

                                                {
                                                    dlg.PrintVisual(Calendar6x8, Image.DG_PrinterQueue_Pkey.ToString());
                                                    //copy++;
                                                }
                                                string xyz = dlg.PrintQueue.FullName.ToString();
                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                            }
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                        }
                                        #endregion calendar
                                    }
                                    else if (Image.DG_PrinterQueue_ProductID == 102)
                                    {
                                        #region product 4*6(2) Vinod start                                        
                                        var queueId = Image.DG_PrinterQueue_Pkey;
                                        Imagebarcod4x6x2.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                        // Position & Orienation layout
                                        LayoutImage(orientaion, position, Imagebarcod4x6x2, Imagebarcod4x6x2.Text); //1 comment
                                        //LayoutImage4by6b2(orientaion, position, Imagebarcod4x6x2, Imagebarcod4x6x2.Text);// By KCB ON 06 SEPT 2018 to remove text 'Image' exist with
                                        try
                                        {
                                            bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());
                                            var filepath = Image.DG_PrinterQueue_Pkey.ToString();
                                            //start Vins
                                            ////if (isVerticalImage)
                                            ////{
                                            ////    ErrorHandler.ErrorHandler.LogFileWrite("Vertical 4*6(2)");
                                            ////    ResizeWPFImage(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")), 6743, 4500, System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")));
                                            ////}
                                            ////else
                                            ////{
                                            ////    ErrorHandler.ErrorHandler.LogFileWrite("Horizontal 4*6(2)");
                                            ////    ResizeWPFImage(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")), 2996, 4496, System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg")));
                                            ////}
                                            //End
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            ///using (FileStream fileStream = File.OpenRead(subStorePrintPath + "FixedSize.png"))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                //bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());
                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                if (isVerticalImage)
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                bi.EndInit();
                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                                //MarginValue4X6_2 = -15;
                                                if (isVerticalImage)
                                                {
                                                    img4x6_1.Source = bi;
                                                    //img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2); //Existing
                                                    img4x6_1.Margin = new Thickness(10, -MarginValue4X6_2, 0, 0);
                                                    prtGrdWallet4by6by2.Margin = new Thickness(0, -MarginValue4X6_2, 0, 0);
                                                    img4x6_2.Source = bi;
                                                    img4x6_2.Margin = new Thickness(11, -5 - MarginValue4X6_2, 0, 0);
                                                    prtGrd2_4by6by2.Margin = new Thickness(0, -5 - MarginValue4X6_2, 0, 0);
                                                }
                                                else
                                                {
                                                    //img4x6_1.Source = bi;
                                                    //img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2);
                                                    //img4x6_2.Source = bi;
                                                    //img4x6_2.Margin = new Thickness(0, 0, 0, 0);

                                                    //MarginValue4X6_2 = -5

                                                    //img4x6_1.Source = bi;
                                                    /////img4x6_1.Margin = new Thickness(0, 0, 0, 0);//Existing
                                                    ////img4x6_1.Margin = new Thickness(0, -MarginValue4X6_2, 0, 0);//Existing
                                                    //img4x6_1.Margin = new Thickness(0, 18, 0, 0);
                                                    //img4x6_2.Source = bi;
                                                    ////img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                                    ////img4x6_2.Margin = new Thickness(0, 0 - MarginValue4X6_2, 0, 0);//By VinS  

                                                    ////prtGrd2_4by6by2.Margin = new Thickness(40, 50, 40, 65);
                                                    //prtGrd2_4by6by2.Margin = new Thickness(0, 0 - MarginValue4X6_2, 0, 0);


                                                    img4x6_1.Source = bi;
                                                    ///img4x6_1.Margin = new Thickness(0, 0, 0, 0);//Existing
                                                    //img4x6_1.Margin = new Thickness(0, -MarginValue4X6_2, 0, 0);//Existing
                                                    img4x6_1.Margin = new Thickness(0, -22, 0, 0);
                                                    img4x6_2.Source = bi;
                                                    //img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                                    //img4x6_2.Margin = new Thickness(0, 0 - MarginValue4X6_2, 0, 0);//By VinS
                                                    //prtGrd2_4by6by2.Margin = new Thickness(40, 50, 40, 65);
                                                    prtGrd2_4by6by2.Margin = new Thickness(0, -40 - MarginValue4X6_2, 0, 0);

                                                }
                                            }
                                            grd4x6x2.UpdateLayout();
                                            //Print(grd4x6x2);
                                            //Below is existing...
                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            //var size = new System.Windows.Size(600,800);
                                            grd4x6x2.Measure(size);
                                            //var size1 = new System.Windows.Size(600, 779);//By Vins  
                                            //grd4x6x2.Measure(size1);
                                            grd4x6x2.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            dlg.PrintVisual(grd4x6x2, Image.DG_PrinterQueue_Pkey.ToString());

                                            string xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                        }

                                        #endregion product 4*6(2) Vinod End
                                    }
                                    else if (Image.DG_PrinterQueue_ProductID == 104)// 4*6 & QR
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 104");
                                        #region 4*6 & QR
                                        try
                                        {
                                            var queueId = Image.DG_PrinterQueue_Pkey;
                                            ImagebarcodQrPrint.Text = " " + Image.DG_Photos_RFID.ToString() + " ";

                                            if (position == "Bottom Right")
                                            {
                                                position = "Bottom Left";
                                            }
                                            else if (position == "Bottom Left")
                                            {
                                                position = "Top Left";
                                            }
                                            else if (position == "Top Left")
                                            {
                                                position = "Top Right";
                                            }
                                            else if (position == "Top Right")
                                            {
                                                position = "Bottom Right";
                                            }

                                            if (orientaion == "Horizontal")
                                            {
                                                orientaion = "Vertical";
                                            }
                                            else if (orientaion == "Vertical")
                                            {
                                                orientaion = "Horizontal";
                                            }
                                            LayoutImageQrCode(orientaion, position, ImagebarcodQrPrint, ImagebarcodQrPrint.Text);

                                            bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());
                                            List<int> ImageIds = new List<int>();
                                            List<int> RFIds = new List<int>();

                                            //isVerticalImage = true;
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                if (!isVerticalImage)
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate180; //big images
                                                    bi.EndInit();

                                                    img4x6And2x3_2_1.Source = bi;
                                                }
                                                else
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    img4x6And2x3_2_1.Source = bi;
                                                }
                                            }
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019


                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                if (!isVerticalImage)
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (Image.DG_PrinterQueue_ProductID == 104) //(4x6) & QR _ 07Sep2018_Anisur_Vinod_Ajay
                                                    {
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;///180 change made by Latika for horizontal image
                                                    }
                                                    else
                                                    {
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate180;///180 change made by Latika for horizontal image
                                                    }
                                                    bi.EndInit();
                                                    img4x6And2x3_2_3.Source = bi;

                                                    if (ProductType.DG_IsBorder == true)
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(50, 70, 50, 85);
                                                    }
                                                    else
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(40, 50, 40, 65);
                                                    }
                                                }
                                                else
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();
                                                    img4x6And2x3_2_3.Source = bi;

                                                    if (ProductType.DG_IsBorder == true)
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(50, 70, 50, 85);
                                                    }
                                                    else
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(40, 50, 40, 65);
                                                    }
                                                }
                                                txtQrCodeUrl.Text = string.Empty;
                                                #region Added by Ajay on 21 Sept 18

                                                ImagebarcodQrPrintSmall.Text = string.Empty;

                                                #endregion
                                                //added by ajit dhama
                                                BitmapImage biqr = new BitmapImage();
                                                //need to embed qr code
                                                FillText(QrCodeOnlineProductText);
                                                string specValue = printBusiness.CheckSpecSetting(Convert.ToInt32(Image.DG_Order_Details_Pkey));
                                                if (string.IsNullOrWhiteSpace(QRCodeWebUrl))
                                                {
                                                    if (!specValue.Equals("SpecOrderNull")) // if(!Image.DG_IsSpecPrint)
                                                        biqr = GetImage(Image.DG_Order_ImageUniqueIdentifier);
                                                    else
                                                    {
                                                        QR = printBusiness.SelectQRCode(Convert.ToInt32(Image.DG_PrinterQueue_Image_Pkey));
                                                        biqr = GetImage(QR);
                                                    }
                                                }
                                                else
                                                {
                                                    if (!specValue.Equals("SpecOrderNull")) // if(!Image.DG_IsSpecPrint)
                                                        biqr = GetImage(QRCodeWebUrl + "/" + Image.DG_Order_ImageUniqueIdentifier);
                                                    else
                                                    {
                                                        QR = printBusiness.SelectQRCode(Convert.ToInt32(Image.DG_PrinterQueue_Image_Pkey));
                                                        biqr = GetImage(QRCodeWebUrl + "/" + QR);
                                                    }
                                                }

                                                img4x6And2x3_2_2.Source = biqr;
                                                FillBottomText(QrCodeOnlineProductURL);
                                                txtImageCode.Text = Image.DG_Order_ImageUniqueIdentifier;
                                                if (specValue.Equals("SpecOrderNull")) //if (Image.DG_IsSpecPrint)
                                                    txtImageCode.Text = QR;
                                                //END
                                            }
                                            string xyz = string.Empty;
                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(grd4x6And2x3_2, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_1, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_2, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_3, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);

                                            grd4x6And2x3_2.UpdateLayout();
                                            ///var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            System.Windows.Size size;
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            grd4x6And2x3_2.Measure(size);
                                            grd4x6And2x3_2.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            int copy = 1;

                                            while (totalPrintCopy >= copy)
                                            {

                                                dlg.PrintVisual(grd4x6And2x3_2, Image.DG_PrinterQueue_Pkey.ToString());
                                                copy++;
                                            }
                                            xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                        #endregion
                                    }

                                    //By KCB ON 23 MAY 2018 To print Unique with QR Code
                                    else if (Image.DG_PrinterQueue_ProductID == 124)// Unique 4*6 & QR
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 124");
                                        #region Unique 4*6 & QR
                                        try
                                        {
                                            var queueId = Image.DG_PrinterQueue_Pkey;
                                            ImagebarcodQrPrint.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                            //ImagebarcodQrPrintSmall.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                            if (position == "Bottom Right")
                                            {
                                                position = "Bottom Left";
                                            }
                                            else if (position == "Bottom Left")
                                            {
                                                position = "Top Left";
                                            }
                                            else if (position == "Top Left")
                                            {
                                                position = "Top Right";
                                            }
                                            else if (position == "Top Right")
                                            {
                                                position = "Bottom Right";
                                            }

                                            if (orientaion == "Horizontal")
                                            {
                                                orientaion = "Vertical";
                                            }
                                            else if (orientaion == "Vertical")
                                            {
                                                orientaion = "Horizontal";
                                            }
                                            //LayoutImageQrCode(orientaion, position, ImagebarcodQrPrint, ImagebarcodQrPrint.Text);
                                            LayoutImageUniqu4by6QR(orientaion, position, ImagebarcodQrPrint, ImagebarcodQrPrint.Text);//By KCB ON 06 SEPT 2018 to remove text 'Image' exist with 
                                            bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());

                                            int bothimages = Image.DG_Order_Details_Pkey;


                                            List<int> ImageIds = new List<int>();
                                            List<int> RFIds = new List<int>();

                                            foreach (PrinterQueueforPrint item in Image1)
                                            {
                                                ImageIds.Add(item.DG_PrinterQueue_Pkey);
                                                RFIds.Add(Convert.ToInt32(item.DG_Photos_RFID));
                                            }

                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (ImageIds[0] + ".jpg")))) //Image.DG_PrinterQueue_Pkey
                                            {
                                                ErrorHandler.ErrorHandler.LogFileWrite((ImageIds[0] + ".jpg") + " send to Print at  (ImageIds[0]:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 124");
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                if (!isVerticalImage)
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate180; //big images
                                                    bi.EndInit();

                                                    img4x6And2x3_2_1.Source = bi;
                                                }
                                                else
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    img4x6And2x3_2_1.Source = bi;
                                                }
                                            }
                                            //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (ImageIds[1] + ".jpg")))) //Image.DG_PrinterQueue_Pkey
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (ImageIds[0] + ".jpg")))) //updated by 
                                            {
                                                ErrorHandler.ErrorHandler.LogFileWrite((ImageIds[0] + ".jpg") + " send to Print at  (ImageIds[0]:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 124");
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                if (!isVerticalImage)
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    //ImagebarcodQrPrintSmall.Text = " " + RFIds[1].ToString() + " ";
                                                    ImagebarcodQrPrintSmall.Text = " " + RFIds[0].ToString() + " ";
                                                    img4x6And2x3_2_3.Source = bi;

                                                    //System.Windows.Controls.Image finalImage = new System.Windows.Controls.Image();
                                                    //finalImage.Width = 80;
                                                    //BitmapImage logo = new BitmapImage();
                                                    //logo.BeginInit();
                                                    //logo.UriSource = new Uri(@"D:\DigiImages\PrintImages\mahesh.jpg");
                                                    //logo.EndInit();
                                                    //img4x6And2x3_2_3.Source = logo;

                                                    if (ProductType.DG_IsBorder == true)
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(50, 70, 50, 85);
                                                    }
                                                    else
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(40, 50, 40, 65);
                                                    }
                                                }
                                                else
                                                {
                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();
                                                    img4x6And2x3_2_3.Source = bi;

                                                    if (ProductType.DG_IsBorder == true)
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(50, 70, 50, 85);
                                                    }
                                                    else
                                                    {
                                                        prt4x6And2x3_2_3.Margin = new Thickness(40, 50, 40, 65);
                                                    }
                                                }
                                                txtQrCodeUrl.Text = string.Empty;

                                                //added by ajit dhama
                                                BitmapImage biqr = new BitmapImage();
                                                //need to embed qr code
                                                FillText(QrCodeOnlineProductText);
                                                string specValue = printBusiness.CheckSpecSetting(Convert.ToInt32(Image.DG_Order_Details_Pkey));
                                                if (string.IsNullOrWhiteSpace(QRCodeWebUrl))
                                                {
                                                    if (!specValue.Equals("SpecOrderNull")) // if(!Image.DG_IsSpecPrint)
                                                        biqr = GetImage(Image.DG_Order_ImageUniqueIdentifier);
                                                    else
                                                    {
                                                        QR = printBusiness.SelectQRCode(Convert.ToInt32(Image.DG_PrinterQueue_Image_Pkey));
                                                        biqr = GetImage(QR);
                                                    }
                                                }
                                                else
                                                {
                                                    if (!specValue.Equals("SpecOrderNull")) // if(!Image.DG_IsSpecPrint)
                                                        biqr = GetImage(QRCodeWebUrl + "/" + Image.DG_Order_ImageUniqueIdentifier);
                                                    else
                                                    {
                                                        QR = printBusiness.SelectQRCode(Convert.ToInt32(Image.DG_PrinterQueue_Image_Pkey));
                                                        biqr = GetImage(QRCodeWebUrl + "/" + QR);
                                                    }
                                                }

                                                img4x6And2x3_2_2.Source = biqr;
                                                FillBottomText(QrCodeOnlineProductURL);
                                                //txtImageCode.Text = Image.DG_Order_ImageUniqueIdentifier;
                                                if (specValue.Equals("SpecOrderNull")) //if (Image.DG_IsSpecPrint)
                                                    txtImageCode.Text = QR;
                                                else
                                                {
                                                    //if (!String.IsNullOrEmpty(Image1[1].DG_Order_ImageUniqueIdentifier))
                                                    //    txtImageCode.Text = Image1[1].DG_Order_ImageUniqueIdentifier;
                                                    //else
                                                    //    txtImageCode.Text = QR;

                                                    if (!String.IsNullOrEmpty(Image1[0].DG_Order_ImageUniqueIdentifier))
                                                        txtImageCode.Text = Image1[0].DG_Order_ImageUniqueIdentifier;
                                                    else
                                                        txtImageCode.Text = QR;
                                                }

                                                //txtImageCode.Text = Image.DG_Order_ImageUniqueIdentifier;
                                                //if (specValue.Equals("SpecOrderNull")) //if (Image.DG_IsSpecPrint)
                                                //    txtImageCode.Text = QR;
                                            }

                                            string xyz = "";
                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(grd4x6And2x3_2, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_1, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_2, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img4x6And2x3_2_3, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);

                                            grd4x6And2x3_2.UpdateLayout();
                                            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            System.Windows.Size size;
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            grd4x6And2x3_2.Measure(size);
                                            grd4x6And2x3_2.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            int copy = 1;

                                            while (totalPrintCopy >= copy)
                                            {
                                                ErrorHandler.ErrorHandler.LogFileWrite((Image.DG_PrinterQueue_Pkey.ToString()) + " send to Print at  (:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 124");
                                                dlg.PrintVisual(grd4x6And2x3_2, Image.DG_PrinterQueue_Pkey.ToString());
                                                copy++;
                                                #region Added by Ajay
                                                ImagebarcodQrPrintSmall.Text = string.Empty;
                                                #endregion
                                            }
                                            xyz = dlg.PrintQueue.FullName.ToString();

                                            foreach (PrinterQueueforPrint item in Image1)
                                            {
                                                printBusiness.SetPrinterQueue(item.DG_PrinterQueue_Pkey);
                                                ErrorHandler.ErrorHandler.LogFileWrite(item.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                        #endregion
                                    }
                                    //End                                    
                                    else if (Image.DG_PrinterQueue_ProductID == 103)//5*7
                                    {
                                        #region product 5*7
                                        try
                                        {
                                            //Imagebarcode5x7.Text = "Image: " + Image.DG_Photos_RFID.ToString();
                                            Imagebarcode5x7.Text = Image.DG_Photos_RFID.ToString();//By KCB ON 06 SEPT 2018 to remove text 'Image' exist with photo number.
                                            if (string.IsNullOrWhiteSpace(position))
                                            {
                                                position = "Top Left";
                                                orientaion = "Vertical";
                                            }
                                            //Position & Orienation layout
                                            //LayoutImage(orientaion, position, Imagebarcode5x7, Imagebarcode5x7.Text);
                                            LayoutImage5By7(orientaion, position, Imagebarcode5x7, Imagebarcode5x7.Text);// By KCB ON 06 SEPT 2018 to remove text 'Image' exist with

                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                bi.EndInit();
                                                img5x7.Source = bi;
                                                //imgwallet1.Source = null;
                                                //imgwallet2.Source = null;
                                                //imgwallet3.Source = null;
                                                //imgwallet4.Source = null;

                                                if (bi.Width > bi.Height)
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                    grd5x7.Width = 700;
                                                    grd5x7.Height = 500;
                                                    grd5x7main.Width = 800;
                                                    grd5x7main.Height = 600;
                                                }
                                                else
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    grd5x7.Width = 500;
                                                    grd5x7.Height = 700;
                                                    grd5x7main.Width = 600;
                                                    grd5x7main.Height = 800;
                                                }
                                            }

                                            string xyz = string.Empty;

                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(grd5x7, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img5x7, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);

                                            grd5x7main.UpdateLayout();
                                            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            System.Windows.Size size;
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            grd5x7main.Measure(size);
                                            grd5x7main.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            int copy = 1;

                                            while (totalPrintCopy >= copy)
                                            {
                                                dlg.PrintVisual(grd5x7main, Image.DG_PrinterQueue_Pkey.ToString());
                                                copy++;
                                            }
                                            xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                        #endregion 5*7
                                    }
                                    else if (IsProductPanoramic)
                                    {
                                        #region Panoramic product 
                                        #region image
                                        try
                                        {

                                            LayoutImage(orientaion, position, Imagebarcodenormal, photoname);
                                            Imagebarcodenormal.Margin = new Thickness(130, 100, 0, 0);
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                bi.EndInit();
                                                imgmain.Source = bi;
                                                imgwallet1.Source = null;
                                                imgwallet2.Source = null;
                                                imgwallet3.Source = null;
                                                imgwallet4.Source = null;

                                                if (bi.Width > bi.Height)
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    if (ProductType.DG_Orders_Details_ProductType_pkey == 121) // 121 8 * 26  --7800 x 2400 pixels 
                                                    {
                                                        semiorder8x10.Width = 7800;
                                                        semiorder8x10.Height = 2400;
                                                        Imagebarcodenormal.FontSize = 25;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 122) // 122 8 * 18 --5400 x 2400 pixels 
                                                    {
                                                        semiorder8x10.Width = 5400;
                                                        semiorder8x10.Height = 2400;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }

                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 123) //123 6 * 14  --4200 x 1800 pixels 
                                                    {
                                                        semiorder8x10.Width = 4200;
                                                        semiorder8x10.Height = 1800;
                                                        Imagebarcodenormal.FontSize = 25;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 125) //125 6 * 20 --6000 x 1800 pixels 
                                                    {
                                                        semiorder8x10.Width = 6000;
                                                        semiorder8x10.Height = 1800;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }
                                                    //Below else condition added by Vins_10Aug2021 for 8*16 Size print on KODAK 8810
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 131) // 8 * 16 --4800 x 2400 pixels 
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("Product 8*16 size setting here");
                                                        semiorder8x10.Width = 4800;
                                                        semiorder8x10.Height = 2400;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }
                                                }
                                                else
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    if (ProductType.DG_Orders_Details_ProductType_pkey == 121) // 121 8 * 26  --7800 x 2400 pixels 
                                                    {
                                                        semiorder8x10.Height = 7800;
                                                        semiorder8x10.Width = 2400;
                                                        Imagebarcodenormal.FontSize = 25;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 122) // 122 8 * 18 --5400 x 2400 pixels 
                                                    {
                                                        semiorder8x10.Height = 5400;
                                                        semiorder8x10.Width = 2400;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }

                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 123) //123 6 * 14  --4200 x 1800 pixels 
                                                    {
                                                        semiorder8x10.Height = 4200;
                                                        semiorder8x10.Width = 1800;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 125) //125 6 * 20 --6000 x 1800 pixels 
                                                    {
                                                        semiorder8x10.Height = 6000;
                                                        semiorder8x10.Width = 1800;
                                                        Imagebarcodenormal.FontSize = 25;
                                                    }
                                                    //Below else condition added by Vins_10Aug2021 for 8*16 Size print on KODAK 8810
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 130) // 8 * 16 --4800 x 2400 pixels 
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("Product 8*16 size setting here");
                                                        semiorder8x10.Width = 4800;
                                                        semiorder8x10.Height = 2400;
                                                        Imagebarcodenormal.FontSize = 20;
                                                    }
                                                }
                                            }
                                            string xyz = string.Empty;
                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(semiorder8x10, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(imgmain, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);
                                            semiorder8x10.UpdateLayout();
                                            var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            System.Windows.Size size;
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            size.Height = semiorder8x10.Height;
                                            size.Width = semiorder8x10.Width;
                                            semiorder8x10.Measure(size);
                                            semiorder8x10.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            int copy = 1;
                                            while (totalPrintCopy >= copy)
                                            {
                                                //dlg.PrintVisual(semiorder8x10, Image.DG_PrinterQueue_Pkey.ToString());
                                                //dlg.PrintVisual(grdPanmain, Image.DG_PrinterQueue_Pkey.ToString());
                                                DS820Printer objPanoramaDs820 = null;
                                                DS820Printer objPanoramaDs620 = null;
                                                if (quename.Contains("DP-DS820"))
                                                {
                                                    objPanoramaDs820 = new DS820Printer(quename);
                                                }
                                                else if (quename.Contains("DP-DS620"))
                                                {
                                                    objPanoramaDs620 = new DS820Printer(quename);
                                                }
                                                DS820.Media Mediatype = DS820.Media.PAPER_8x18;
                                                #region get extension and media size
                                                switch (Image.DG_Orders_ProductType_Name)
                                                {
                                                    case "8 * 24":
                                                        Mediatype = DS820.Media.PAPER_8x24;
                                                        break;
                                                    case "8 * 26":
                                                        Mediatype = DS820.Media.PAPER_8x26;
                                                        break;
                                                    case "6 * 20":
                                                        Mediatype = DS820.Media.PAPER_6x20;
                                                        break;
                                                    case "8 * 18":
                                                        Mediatype = DS820.Media.PAPER_8x18;
                                                        break;
                                                    case "6 * 14":
                                                        Mediatype = DS820.Media.PAPER_6x14;
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                arrPanoramaPath = Directory.GetFiles(subStorePrintPath);
                                                foreach (string name in arrPanoramaPath)
                                                {
                                                    var result = System.IO.Path.GetFileName(name);
                                                    filekey = result.Split('.');
                                                    if (filekey[0] == Convert.ToString(Image.DG_PrinterQueue_Pkey))
                                                    {
                                                        extension = System.IO.Path.GetExtension(name);
                                                    }
                                                }
                                                #endregion
                                                string printImagePath = System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + extension));
                                                #region save grid as image
                                                int Height1 = (int)this.semiorder8x10.ActualHeight;
                                                int Width1 = (int)this.semiorder8x10.ActualWidth;
                                                RenderTargetBitmap bmp1 = new RenderTargetBitmap(Width1, Height1, 96, 96, PixelFormats.Pbgra32);
                                                bmp1.Render(this.semiorder8x10);
                                                string file1 = printImagePath;
                                                string Extension1 = System.IO.Path.GetExtension(file1).ToLower();
                                                BitmapEncoder encoder1;
                                                if (Extension1 == ".gif")
                                                    encoder1 = new GifBitmapEncoder();
                                                else if (Extension1 == ".png")
                                                    encoder1 = new PngBitmapEncoder();
                                                else if (Extension1 == ".jpg")
                                                    encoder1 = new JpegBitmapEncoder();
                                                else
                                                    return;
                                                encoder1.Frames.Add(BitmapFrame.Create(bmp1));

                                                using (Stream stm1 = File.Create(file1))
                                                {
                                                    encoder1.Save(stm1);
                                                }
                                                #endregion
                                                bool isPrint = false;
                                                if (quename.Contains("DP-DS820"))
                                                {
                                                    isPrint = objPanoramaDs820.Print(printImagePath, 1, Mediatype, DS820.Resolution.R300DPI, DS820.HighDensity.ENABLE_HIGH_DENSITY, DS820.OVERCOAT.GLOSSY);
                                                }
                                                else if (quename.Contains("DP-DS620"))
                                                {
                                                    isPrint = objPanoramaDs620.Print(printImagePath, 1, Mediatype, Resolution.R300DPI, HighDensity.ENABLE_HIGH_DENSITY, OVERCOAT.GLOSSY);
                                                }
                                                else if (quename.Contains("6900"))//KAILASH ON 07 AUG 2019
                                                {
                                                    ErrorHandler.ErrorHandler.LogFileWrite("6900 KODAK CODE STARTED");
                                                    PrintSize printSize;
                                                    switch (Image.DG_Orders_ProductType_Name)
                                                    {

                                                        case "6 * 20":
                                                            printSize = PrintSize.SixByTwentyPanoramic;
                                                            break;
                                                        case "6 * 14":
                                                            printSize = PrintSize.SixByFourteenPanoramic;
                                                            break;
                                                        default:
                                                            printSize = PrintSize.FourBySix;
                                                            break;
                                                    }
                                                    int rs = 0;
                                                    int PageID = 0;
                                                    K6900Driver k6900Driver = new K6900Driver();
                                                    Task task = Task.Run(() =>
                                                    {

                                                        int str = -1;
                                                        bool iswait = true;
                                                        do
                                                        {
                                                            Thread.Sleep(5000);
                                                            if (k6900Driver.GetStatus(out str) == true)
                                                            {
                                                                if (str == 0)
                                                                {
                                                                    if (this.needTowaitPrinter6900(str) == true)
                                                                    {
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("Kodak 6900 Print Status code:-" + str.ToString());
                                                                        Thread.Sleep(5000);
                                                                    }
                                                                    else
                                                                    {
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("Kodak 6900 Print Status code:-" + str.ToString());
                                                                        iswait = false;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ErrorHandler.ErrorHandler.LogFileWrite("Kodak 6900 Print Status code:-" + str.ToString());
                                                                iswait = false;
                                                            }
                                                        }
                                                        while (iswait == true);
                                                        if (str == 0)
                                                        {
                                                            if (k6900Driver.Print(printImagePath, printSize, 1, out rs, out PageID, "", "") == true)
                                                            {
                                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Kodak 6900 Print at:-" + DateTime.Now.ToLongTimeString());
                                                                Thread.Sleep(50000);
                                                            }
                                                            else
                                                                ErrorHandler.ErrorHandler.LogFileWrite("Kodak 6900 Print Error:-" + rs);
                                                        }

                                                    });
                                                    task.Wait();
                                                    ErrorHandler.ErrorHandler.LogFileWrite("Printer resumed");

                                                }
                                                else if (quename.Contains("8810"))
                                                {
                                                    if (System.Configuration.ConfigurationManager.AppSettings["IsPrintEnable"] == "true")
                                                    {
                                                        while (totalPrintCopy >= copy)
                                                        {
                                                            dlg.PrintVisual(semiorder8x10, Image.DG_PrinterQueue_Pkey.ToString());
                                                            copy++;
                                                        }
                                                    }
                                                    //xyz = dlg.PrintQueue.FullName.ToString();
                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                }//END

                                                copy++;
                                            }
                                            xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());


                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogFileWrite("exception");
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                        #endregion

                                        #endregion Pan
                                    }
                                    else // normal 4 by 6, 6 by 8 , 8 by 10, 36 by 24, 24 by 18 image
                                    {
                                        #region normal 4 by 6, 6 by 8 , 8 by 10, 36 by 24, 24 by 18 image
                                        try
                                        {
                                            //#region modified by ajay on 6 Sep 2018
                                            //Imagebarcodenormal.FontSize = 8;////
                                            //#endregion
                                            //#region modified by latika on 30 Aug 2019

                                            Imagebarcodenormal.FontSize = Global.FontSizeNew;////
                                            //#endregion
                                            if (Image.DG_Orders_ProductType_Name.ToLower() == "licence product")
                                            {
                                                if (string.IsNullOrWhiteSpace(position))
                                                {
                                                    position = "Top Right";
                                                    orientaion = "Horizontal";
                                                }
                                                if (orientaion.Equals("Horizontal"))
                                                {
                                                    RotateTransform RotateTrsfrm = new RotateTransform();
                                                    RotateTrsfrm.Angle = 0;
                                                    Imagebarcodenormal.RenderTransform = RotateTrsfrm;
                                                    Thickness margin = Imagebarcodenormal.Margin;
                                                    int len = Imagebarcodenormal.Text.Length;
                                                    margin.Right = len + 16;
                                                    Imagebarcodenormal.Margin = margin;
                                                }
                                                Imagebarcodenormal.HorizontalAlignment = HorizontalAlignment.Right;
                                                Imagebarcodenormal.VerticalAlignment = VerticalAlignment.Top;
                                                Imagebarcodenormal.Text = photoname;
                                                ErrorHandler.ErrorHandler.LogError("Is Print Images 2240: " + photoname);
                                            }
                                            else
                                            {
                                                LayoutImage(orientaion, position, Imagebarcodenormal, photoname);
                                            }

                                            //LayoutImage(orientaion, position, Imagebarcodenormal, photoname);
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (Image.DG_PrinterQueue_Pkey + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                bi.EndInit();
                                                imgmain.Source = bi;
                                                imgwallet1.Source = null;
                                                imgwallet2.Source = null;
                                                imgwallet3.Source = null;
                                                imgwallet4.Source = null;

                                                if (bi.Width > bi.Height)
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                    if (ProductType.DG_Orders_Details_ProductType_pkey == 1) // 8 * 6
                                                    {
                                                        semiorder8x10.Width = 800;
                                                        semiorder8x10.Height = 600;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 2) // 10 * 8
                                                    {
                                                        semiorder8x10.Width = 1000;
                                                        semiorder8x10.Height = 800;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 30) //4*6
                                                    {
                                                        if (dlg.PrintQueue.FullName.ToLower().Contains("ds40"))
                                                            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                                        #region Added by Ajay
                                                        if (dlg.PrintQueue.FullName.ToLower().Contains("ds620"))
                                                        {
                                                            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                            Thickness margin = Imagebarcodenormal.Margin;
                                                            int len = Imagebarcodenormal.Text.Length;
                                                            margin.Left = 35;
                                                            Imagebarcodenormal.Margin = margin;
                                                        }
                                                        #endregion

                                                        semiorder8x10.Width = 600;
                                                        semiorder8x10.Height = 400;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 6) //36*24
                                                    {
                                                        semiorder8x10.Width = 3456;
                                                        semiorder8x10.Height = 2304;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 7) //24*18
                                                    {
                                                        dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                        semiorder8x10.Width = 2304;
                                                        semiorder8x10.Height = 1728;
                                                    }
                                                }
                                                else
                                                {
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                    if (ProductType.DG_Orders_Details_ProductType_pkey == 1) // 6*8
                                                    {
                                                        semiorder8x10.Width = 600;
                                                        semiorder8x10.Height = 800;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 2) //8*10
                                                    {
                                                        semiorder8x10.Width = 800;
                                                        semiorder8x10.Height = 1000;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 30)//6*4
                                                    {
                                                        if (dlg.PrintQueue.FullName.ToLower().Contains("ds40"))
                                                            dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                        #region Added by Ajay
                                                        if (dlg.PrintQueue.FullName.ToLower().Contains("ds620"))
                                                        {
                                                            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                            Thickness margin = Imagebarcodenormal.Margin;
                                                            int len = Imagebarcodenormal.Text.Length;
                                                            margin.Left = 35;
                                                            Imagebarcodenormal.Margin = margin;
                                                        }
                                                        #endregion
                                                        semiorder8x10.Width = 400;
                                                        semiorder8x10.Height = 600;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 6)//36*24
                                                    {
                                                        semiorder8x10.Width = 2304;
                                                        semiorder8x10.Height = 3456;
                                                    }
                                                    else if (ProductType.DG_Orders_Details_ProductType_pkey == 7) //24*18
                                                    {
                                                        dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                        semiorder8x10.Width = 1728;
                                                        semiorder8x10.Height = 2304;
                                                    }
                                                }
                                            }

                                            //string xyz = string.Empty;

                                            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                            RenderOptions.SetEdgeMode(semiorder8x10, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(imgmain, EdgeMode.Aliased);
                                            RenderOptions.SetEdgeMode(img, EdgeMode.Aliased);

                                            semiorder8x10.UpdateLayout();
                                            ///var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                            System.Windows.Size size;

                                            //if (ProductType.DG_Orders_Details_ProductType_pkey == 1 && orientaion.Equals("Vertical")) //by Vins
                                            //{
                                            //    size = new System.Windows.Size(591, 800);
                                            //}
                                            //else
                                            //{
                                            size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                            //}
                                            semiorder8x10.Measure(size);

                                            semiorder8x10.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                            int copy = 1;

                                            ErrorHandler.ErrorHandler.LogFileWrite("IsPrintEnabled =" + System.Configuration.ConfigurationManager.AppSettings["IsPrintEnable"]);

                                            if (System.Configuration.ConfigurationManager.AppSettings["IsPrintEnable"] == "true")
                                            {
                                                while (totalPrintCopy >= copy)
                                                {
                                                    dlg.PrintVisual(semiorder8x10, Image.DG_PrinterQueue_Pkey.ToString());
                                                    copy++;
                                                }
                                            }
                                            //xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);///Need to remove the DG_SentToPrinter flag set =1//Vins
                                        }

                                        #endregion
                                    }
                                }
                                else
                                {
                                    if (Image.DG_PrinterQueue_ProductID == 131) // for Storybook product
                                    {
                                        List<SceneInfo> BlankPageSceneDetails = null;

                                        //LayoutImage(orientaion, position, Imagebarcodenormal_Copy, photoname);
                                        ErrorHandler.ErrorHandler.LogFileWrite("Line 2511 Storybook Pkeys:" + Image.DG_PrinterQueue_Image_Pkey);
                                        string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                        //string[] _namearyPrinted = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                        List<string> _namearyPrinted = new List<string>();
                                        long SBID = 0;
                                        if (_nameary.Count() > 0)
                                        {
                                            bool Landscape = false;
                                            isBlankPageRequired = true;//first time it will be true, once values got then will make it false
                                            ////have to define path here for image 1
                                            try
                                            {
                                                if (ProductType.DG_IsBorder == true)
                                                    borderMargin = 20;

                                                PhotoBusiness objPhoto = new PhotoBusiness();
                                                SBID = Convert.ToInt64(_nameary[0]);
                                                PhotoInfo _objSBphoto = objPhoto.GetPhotoStoryBookDetailsbyPhotoId(_nameary[0].ToInt32());
                                                SBID = _objSBphoto.StoryBookID;
                                                if (_objSBphoto.StoryBookID == 0)
                                                {
                                                    for (int i = 0; i < _nameary.Count(); i++)
                                                    {
                                                        _objSBphoto = objPhoto.GetPhotoStoryBookDetailsbyPhotoId(_nameary[i].ToInt32());

                                                        if (_objSBphoto.StoryBookID > 0)
                                                        {
                                                            SBID = _objSBphoto.StoryBookID;
                                                            break;
                                                        }
                                                    }
                                                }

                                                //bool isBlankPageRequired = false;
                                                List<PhotoInfo> objPhotoLst = objPhoto.GetAllStoryBookPhotoBYSTID(SBID);//list to check page no.
                                                //foreach (PhotoInfo obj in objPhotoLst)
                                                //{
                                                //    if (String.IsNullOrEmpty(MW.selectedEnglishText) || String.IsNullOrEmpty(MW.selectedChineseText))
                                                //    {
                                                //        MW.LoadXamlGuestNames(obj.DG_Photos_Layering);//Just to get guest name on photo//chinese and english text 
                                                //        isBlankPageRequired = true;
                                                //    }
                                                //    if(!String.IsNullOrEmpty(MW.selectedEnglishText) || !String.IsNullOrEmpty(MW.selectedChineseText))
                                                //    {
                                                //        break;
                                                //    }
                                                //}

                                                for (int i = 0; i < _nameary.Count(); i++)
                                                {
                                                    ErrorHandler.ErrorHandler.LogFileWrite("Storybook print started for " + _nameary[i].ToString());
                                                    PhotoInfo _objphoto = objPhoto.GetPhotoStoryBookDetailsbyPhotoId(_nameary[i].ToInt32());
                                                    if (_namearyPrinted.Contains(Convert.ToString(_objphoto.PageNo)))
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("Storybook page already printed " + _objphoto.PageNo);
                                                        //it's already printed
                                                        continue;
                                                    }
                                                    if (BlankPageSceneDetails == null)
                                                    {
                                                        BlankPageSceneDetails = (new SceneBusiness()).GetAllBlankPageByThemeID((Int32)_objphoto.ThemeID);
                                                    }

                                                    if (_objphoto.PageNo % 2 == 0 || _objphoto.PageNo == 0)//Check for even page number
                                                    {
                                                        //Current Page is even 
                                                        int currentPage = Convert.ToInt32(_objphoto.PageNo);
                                                        //Assign odd page no here
                                                        int prevPage = Convert.ToInt32(_objphoto.PageNo - 1);//check for previous odd page number is blank or not

                                                        if (!_namearyPrinted.Contains(Convert.ToString(currentPage)))
                                                        {
                                                            _namearyPrinted.Add(Convert.ToString(currentPage));
                                                        }
                                                        if (!_namearyPrinted.Contains(Convert.ToString(prevPage)))
                                                        {
                                                            _namearyPrinted.Add(Convert.ToString(prevPage));
                                                        }
                                                        //Check if next page is blank
                                                        SceneInfo SceneBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == currentPage.ToString()).FirstOrDefault();

                                                        ErrorHandler.ErrorHandler.LogFileWrite("Page no " + currentPage + " is even");
                                                        if (SceneBlankPage != null)
                                                        {
                                                            ErrorHandler.ErrorHandler.LogFileWrite("Page no " + currentPage + " is blank " + " for image ID :" + _nameary[i]);
                                                            //found even page is blank
                                                            BitmapImage Nextbi = new BitmapImage();
                                                            ErrorHandler.ErrorHandler.LogFileWrite("Page no " + currentPage + " for image ID :" + _nameary[i]);
                                                            //Chekc if page exists
                                                            string editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + currentPage + ".jpg");
                                                            if (!File.Exists(editedBlankPagePath))
                                                            {
                                                                //If blank page not exists then create blank blank for the same 
                                                                ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                //Create blank page with text and put in editedimage folder 
                                                                PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(currentPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                            }
                                                            //if(isBlankPageRequired)
                                                            //{
                                                            //    PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(currentPage), editedBlankPagePath, SceneBlankPage.SceneId,SBID);
                                                            //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                            //}
                                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + currentPage + ".jpg"))))
                                                            {
                                                                //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));

                                                                MemoryStream ms = new MemoryStream();
                                                                fileStream.CopyTo(ms);
                                                                ms.Seek(0, SeekOrigin.Begin);
                                                                fileStream.Close();

                                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                                Nextbi.BeginInit();
                                                                Nextbi.StreamSource = ms;
                                                                //if (isVerticalImage)
                                                                //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                Nextbi.EndInit();
                                                                //imgwalletSB1.Source = bi;
                                                                if (Nextbi.Width >= Nextbi.Height)
                                                                    Landscape = true;
                                                                else
                                                                    Landscape = false;
                                                            }
                                                            imgwalletSB2.Source = Nextbi;

                                                            if (!_namearyPrinted.Contains(Convert.ToString(currentPage)))
                                                            {
                                                                _namearyPrinted.Add(Convert.ToString(currentPage));//Add if not contains
                                                            }
                                                            //Assign content to previous page
                                                            if (objPhotoLst != null)
                                                            {
                                                                SceneInfo IsPrevBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == prevPage.ToString()).FirstOrDefault();

                                                                if (!_namearyPrinted.Contains(Convert.ToString(prevPage)))
                                                                {
                                                                    _namearyPrinted.Add(Convert.ToString(prevPage));//Add previous page if not contains
                                                                }
                                                                BitmapImage Prevbi = new BitmapImage();

                                                                if (IsPrevBlankPage != null)
                                                                {
                                                                    //is blank
                                                                    editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + prevPage + ".jpg");
                                                                    if (!File.Exists(editedBlankPagePath))
                                                                    {
                                                                        //If blank page not exists then create blank blank for the same 
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                        //Create blank page with text and put in editedimage folder 
                                                                        PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(prevPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                    }
                                                                    //if (isBlankPageRequired)
                                                                    //{
                                                                    //    PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(prevPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                    //}
                                                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + prevPage + ".jpg"))))
                                                                    {
                                                                        //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                        MemoryStream ms = new MemoryStream();
                                                                        fileStream.CopyTo(ms);
                                                                        ms.Seek(0, SeekOrigin.Begin);
                                                                        fileStream.Close();
                                                                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                        Prevbi.BeginInit();
                                                                        Prevbi.StreamSource = ms;
                                                                        //if (isVerticalImage)
                                                                        //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                        Prevbi.EndInit();
                                                                        //imgwalletSB1.Source = bi;
                                                                        if (Nextbi.Width >= Nextbi.Height)
                                                                            Landscape = true;
                                                                        else
                                                                            Landscape = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    //Not blank                                                                        
                                                                    PhotoInfo objPhotoInfo = objPhotoLst.Where(g => g.PageNo == prevPage).FirstOrDefault();
                                                                    if (objPhotoInfo != null)
                                                                    {
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("no blank Page no " + prevPage + " for image ID :" + objPhotoInfo.DG_Photos_FileName);
                                                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                        {
                                                                            //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                            MemoryStream ms = new MemoryStream();
                                                                            fileStream.CopyTo(ms);
                                                                            ms.Seek(0, SeekOrigin.Begin);
                                                                            fileStream.Close();
                                                                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                            Prevbi.BeginInit();
                                                                            Prevbi.StreamSource = ms;
                                                                            //if (isVerticalImage)
                                                                            //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                            Prevbi.EndInit();
                                                                            //imgwalletSB1.Source = bi;
                                                                            if (Prevbi.Width >= Prevbi.Height)
                                                                                Landscape = true;
                                                                            else
                                                                                Landscape = false;
                                                                        }

                                                                    }
                                                                    imgwalletSB1.Source = Prevbi;//Previous page content assigned
                                                                }

                                                                dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;

                                                                string xyz = string.Empty;
                                                                RenderOptions.SetEdgeMode(Grd8by16, EdgeMode.Aliased);
                                                                RenderOptions.SetBitmapScalingMode(Grd8by16, BitmapScalingMode.HighQuality);
                                                                Grd8by16.UpdateLayout();
                                                                Grd8by16.SnapsToDevicePixels = true;
                                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                                var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                                Grd8by16.Measure(size);
                                                                Grd8by16.Arrange(new Rect(
                                                                  new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                                  size));
                                                                try
                                                                {
                                                                    int copy = 1;
                                                                    while (totalPrintCopy >= copy)
                                                                    {
                                                                        //PrintDialog dialog = new PrintDialog();
                                                                        //if (dialog.ShowDialog() == true)
                                                                        //{
                                                                        //    dialog.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                        //}

                                                                        dlg.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                        copy++;
                                                                    }
                                                                }
                                                                catch (OutOfMemoryException ex)
                                                                {
                                                                    GC.AddMemoryPressure(10000);
                                                                    MemoryManagement.FlushMemory();
                                                                }
                                                                xyz = dlg.PrintQueue.FullName.ToString();

                                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                                //Remove printed item from list in order to not print it again
                                                                if (_namearyPrinted.Contains(_nameary[i]))
                                                                {
                                                                    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + _nameary[i]);
                                                                    try
                                                                    {
                                                                        _namearyPrinted.Remove(_nameary[i].ToString());
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        ErrorHandler.ErrorHandler.LogError(ex);
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("2729 Removing item exception");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //Get even number guest photo
                                                            if (objPhotoLst != null)
                                                            {
                                                                PhotoInfo objPhotoInfo = objPhotoLst.Where(g => g.PageNo == currentPage).FirstOrDefault();
                                                                if (objPhotoInfo != null)
                                                                {
                                                                    if (!_namearyPrinted.Contains(Convert.ToString(currentPage)))
                                                                    {
                                                                        _namearyPrinted.Add(Convert.ToString(currentPage));
                                                                    }
                                                                    ErrorHandler.ErrorHandler.LogFileWrite("not blank Page  " + currentPage + " for image ID :" + objPhotoInfo.DG_Photos_pkey);
                                                                    // objPhotoInfo.DG_Photos_FileName
                                                                    BitmapImage Nextbi = new BitmapImage();

                                                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                    {
                                                                        //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));

                                                                        MemoryStream ms = new MemoryStream();
                                                                        fileStream.CopyTo(ms);
                                                                        ms.Seek(0, SeekOrigin.Begin);
                                                                        fileStream.Close();

                                                                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                                        Nextbi.BeginInit();
                                                                        Nextbi.StreamSource = ms;
                                                                        //if (isVerticalImage)
                                                                        //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                        Nextbi.EndInit();
                                                                        //imgwalletSB1.Source = bi;
                                                                        if (Nextbi.Width >= Nextbi.Height)
                                                                            Landscape = true;
                                                                        else
                                                                            Landscape = false;
                                                                    }
                                                                    imgwalletSB2.Source = Nextbi;


                                                                    //Assign to previous page

                                                                    SceneInfo IsPrevBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == prevPage.ToString()).FirstOrDefault();
                                                                    BitmapImage Prevbi = new BitmapImage();

                                                                    if (IsPrevBlankPage != null)
                                                                    {
                                                                        if (!_namearyPrinted.Contains(Convert.ToString(prevPage)))
                                                                        {
                                                                            _namearyPrinted.Add(Convert.ToString(prevPage));
                                                                        }
                                                                        //is blank
                                                                        string editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + prevPage + ".jpg");
                                                                        if (!File.Exists(editedBlankPagePath))
                                                                        {
                                                                            //If blank page not exists then create blank blank for the same 
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                            //Create blank page with text and put in editedimage folder 
                                                                            PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(prevPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        }
                                                                        //if (isBlankPageRequired)
                                                                        //{
                                                                        //    PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(prevPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                        //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        //}
                                                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + prevPage + ".jpg"))))
                                                                        {
                                                                            //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                            MemoryStream ms = new MemoryStream();
                                                                            fileStream.CopyTo(ms);
                                                                            ms.Seek(0, SeekOrigin.Begin);
                                                                            fileStream.Close();
                                                                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                            Prevbi.BeginInit();
                                                                            Prevbi.StreamSource = ms;
                                                                            //if (isVerticalImage)
                                                                            //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                            Prevbi.EndInit();
                                                                            //imgwalletSB1.Source = bi;
                                                                            if (Prevbi.Width >= Prevbi.Height)
                                                                                Landscape = true;
                                                                            else
                                                                                Landscape = false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (!_namearyPrinted.Contains(Convert.ToString(prevPage)))
                                                                        {
                                                                            _namearyPrinted.Add(Convert.ToString(prevPage));
                                                                        }
                                                                        //Not blank                                                                       
                                                                        PhotoInfo objPrevPhotoInfo = objPhotoLst.Where(g => g.PageNo == prevPage).FirstOrDefault();
                                                                        if (objPrevPhotoInfo != null)
                                                                        {
                                                                            ErrorHandler.ErrorHandler.LogFileWrite(" Page is not blank Page no " + prevPage + " for image ID :" + objPrevPhotoInfo.DG_Photos_FileName);
                                                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objPrevPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                            {
                                                                                //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                                MemoryStream ms = new MemoryStream();
                                                                                fileStream.CopyTo(ms);
                                                                                ms.Seek(0, SeekOrigin.Begin);
                                                                                fileStream.Close();
                                                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                                Prevbi.BeginInit();
                                                                                Prevbi.StreamSource = ms;
                                                                                //if (isVerticalImage)
                                                                                //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                                Prevbi.EndInit();
                                                                                //imgwalletSB1.Source = bi;
                                                                                if (Prevbi.Width >= Prevbi.Height)
                                                                                    Landscape = true;
                                                                                else
                                                                                    Landscape = false;
                                                                            }
                                                                        }
                                                                    }
                                                                    imgwalletSB1.Source = Prevbi;//Previous page content assigned    

                                                                    //Now print as page is ready to print
                                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;

                                                                    string xyz = string.Empty;
                                                                    RenderOptions.SetEdgeMode(Grd8by16, EdgeMode.Aliased);
                                                                    RenderOptions.SetBitmapScalingMode(Grd8by16, BitmapScalingMode.HighQuality);
                                                                    Grd8by16.UpdateLayout();
                                                                    Grd8by16.SnapsToDevicePixels = true;
                                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                                    Grd8by16.Measure(size);
                                                                    Grd8by16.Arrange(new Rect(
                                                                      new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                                      size));
                                                                    try
                                                                    {
                                                                        int copy = 1;
                                                                        while (totalPrintCopy >= copy)
                                                                        {
                                                                            //PrintDialog dialog = new PrintDialog();
                                                                            //if (dialog.ShowDialog() == true)
                                                                            //{
                                                                            //    dialog.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            //}
                                                                            dlg.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            copy++;
                                                                        }
                                                                    }
                                                                    catch (OutOfMemoryException ex)
                                                                    {
                                                                        GC.AddMemoryPressure(10000);
                                                                        MemoryManagement.FlushMemory();
                                                                    }
                                                                    xyz = dlg.PrintQueue.FullName.ToString();

                                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                                    //Remove printed item from list in order to not print it again
                                                                    //if (_namearyPrinted.Contains(objPhotoInfo.DG_Photos_pkey.ToString()))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + objPhotoInfo.DG_Photos_pkey);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(objPhotoInfo.DG_Photos_pkey.ToString());
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("2730 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                    //if (_namearyPrinted.Contains(_nameary[i]))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + _nameary[i]);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(_nameary[i].ToString());
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("2932 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else //Check for odd page number
                                                    {
                                                        //Objphoto is odd 
                                                        //imgwalletSB1.Source = bi;

                                                        int nextPage = Convert.ToInt32(_objphoto.PageNo + 1);
                                                        //Assign current page no is odd
                                                        int currentPage = Convert.ToInt32(_objphoto.PageNo);

                                                        if (!_namearyPrinted.Contains(Convert.ToString(nextPage)))
                                                        {
                                                            _namearyPrinted.Add(Convert.ToString(nextPage));
                                                        }
                                                        if (!_namearyPrinted.Contains(Convert.ToString(currentPage)))
                                                        {
                                                            _namearyPrinted.Add(Convert.ToString(currentPage));
                                                        }

                                                        ErrorHandler.ErrorHandler.LogFileWrite("Line 2945 Odd page no. " + currentPage);

                                                        //Check if next page is blank
                                                        SceneInfo SceneBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == currentPage.ToString()).FirstOrDefault();

                                                        if (SceneBlankPage != null)
                                                        {
                                                            //found odd page is blank
                                                            BitmapImage Prevbi = new BitmapImage();
                                                            ErrorHandler.ErrorHandler.LogFileWrite("Page no " + currentPage + " for image ID :" + _nameary[i]);
                                                            //Chekc if page exists
                                                            string editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + currentPage + ".jpg");
                                                            if (!File.Exists(editedBlankPagePath))
                                                            {
                                                                //If blank page not exists then create blank blank for the same 
                                                                ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                //Create blank page with text and put in editedimage folder 
                                                                PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(currentPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                            }
                                                            //if (isBlankPageRequired)
                                                            //{
                                                            //    PrepareImageBlankPage(_objphoto, SceneBlankPage.ThemeName, Convert.ToString(currentPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                            //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                            //}
                                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + currentPage + ".jpg"))))
                                                            {
                                                                //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));

                                                                MemoryStream ms = new MemoryStream();
                                                                fileStream.CopyTo(ms);
                                                                ms.Seek(0, SeekOrigin.Begin);
                                                                fileStream.Close();

                                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                                Prevbi.BeginInit();
                                                                Prevbi.StreamSource = ms;
                                                                //if (isVerticalImage)
                                                                //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                Prevbi.EndInit();
                                                                //imgwalletSB1.Source = bi;
                                                                if (Prevbi.Width >= Prevbi.Height)
                                                                    Landscape = true;
                                                                else
                                                                    Landscape = false;
                                                            }
                                                            imgwalletSB1.Source = Prevbi;

                                                            //Assign content to next even page
                                                            if (objPhotoLst != null)
                                                            {
                                                                PhotoInfo objNextPhotoInfo = objPhotoLst.Where(g => g.PageNo == nextPage).FirstOrDefault();
                                                                if (objNextPhotoInfo != null)
                                                                {
                                                                    ErrorHandler.ErrorHandler.LogFileWrite("no blank Page no " + nextPage + " for image ID :" + objNextPhotoInfo.DG_Photos_FileName);
                                                                    SceneInfo IsNextBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == nextPage.ToString()).FirstOrDefault();
                                                                    BitmapImage Nextbi = new BitmapImage();

                                                                    if (IsNextBlankPage != null)
                                                                    {
                                                                        //is blank
                                                                        editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + nextPage + ".jpg");
                                                                        if (!File.Exists(editedBlankPagePath))
                                                                        {
                                                                            //If blank page not exists then create blank blank for the same 
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                            //Create blank page with text and put in editedimage folder 
                                                                            PrepareImageBlankPage(objNextPhotoInfo, SceneBlankPage.ThemeName, Convert.ToString(nextPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        }
                                                                        //if (isBlankPageRequired)
                                                                        //{
                                                                        //    PrepareImageBlankPage(objNextPhotoInfo, SceneBlankPage.ThemeName, Convert.ToString(nextPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                        //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        //}
                                                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + nextPage + ".jpg"))))
                                                                        {
                                                                            //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                            MemoryStream ms = new MemoryStream();
                                                                            fileStream.CopyTo(ms);
                                                                            ms.Seek(0, SeekOrigin.Begin);
                                                                            fileStream.Close();
                                                                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                            Nextbi.BeginInit();
                                                                            Nextbi.StreamSource = ms;
                                                                            //if (isVerticalImage)
                                                                            //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                            Nextbi.EndInit();
                                                                            //imgwalletSB1.Source = bi;
                                                                            if (Nextbi.Width >= Nextbi.Height)
                                                                                Landscape = true;
                                                                            else
                                                                                Landscape = false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        //Not blank                                                                        

                                                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objNextPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                        {
                                                                            //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                            MemoryStream ms = new MemoryStream();
                                                                            fileStream.CopyTo(ms);
                                                                            ms.Seek(0, SeekOrigin.Begin);
                                                                            fileStream.Close();
                                                                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                            Nextbi.BeginInit();
                                                                            Nextbi.StreamSource = ms;
                                                                            //if (isVerticalImage)
                                                                            //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                            Nextbi.EndInit();
                                                                            //imgwalletSB1.Source = bi;
                                                                            if (Nextbi.Width >= Nextbi.Height)
                                                                                Landscape = true;
                                                                            else
                                                                                Landscape = false;
                                                                        }

                                                                    }
                                                                    imgwalletSB2.Source = Nextbi;//Next page content assigned

                                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;

                                                                    string xyz = string.Empty;
                                                                    RenderOptions.SetEdgeMode(Grd8by16, EdgeMode.Aliased);
                                                                    RenderOptions.SetBitmapScalingMode(Grd8by16, BitmapScalingMode.HighQuality);
                                                                    Grd8by16.UpdateLayout();
                                                                    Grd8by16.SnapsToDevicePixels = true;
                                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                                    Grd8by16.Measure(size);
                                                                    Grd8by16.Arrange(new Rect(
                                                                      new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                                      size));
                                                                    try
                                                                    {
                                                                        int copy = 1;
                                                                        while (totalPrintCopy >= copy)
                                                                        {
                                                                            //PrintDialog dialog = new PrintDialog();
                                                                            //if (dialog.ShowDialog() == true)
                                                                            //{
                                                                            //    dialog.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            //}
                                                                            dlg.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            copy++;
                                                                        }
                                                                    }
                                                                    catch (OutOfMemoryException ex)
                                                                    {
                                                                        GC.AddMemoryPressure(10000);
                                                                        MemoryManagement.FlushMemory();
                                                                    }
                                                                    xyz = dlg.PrintQueue.FullName.ToString();

                                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                                    //Remove printed item from list in order to not print it again
                                                                    //if (_namearyPrinted.Contains(Image.DG_PrinterQueue_Pkey.ToString()))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + Image.DG_PrinterQueue_Pkey);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(Image.DG_PrinterQueue_Pkey.ToString());
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("3102 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                    //if (_namearyPrinted.Contains(objNextPhotoInfo.DG_Photos_pkey.ToString()))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + objNextPhotoInfo.DG_Photos_pkey);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(objNextPhotoInfo.DG_Photos_pkey.ToString());
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("2730 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {

                                                            //Get odd number guest photo
                                                            if (objPhotoLst != null)
                                                            {
                                                                PhotoInfo objPhotoInfo = objPhotoLst.Where(g => g.PageNo == currentPage).FirstOrDefault();
                                                                if (objPhotoInfo != null)
                                                                {
                                                                    ErrorHandler.ErrorHandler.LogFileWrite("Guest photo " + currentPage + " for image ID :" + objPhotoInfo.DG_Photos_pkey);
                                                                    // objPhotoInfo.DG_Photos_FileName
                                                                    BitmapImage Nextbi = new BitmapImage();

                                                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                    {
                                                                        //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));

                                                                        MemoryStream ms = new MemoryStream();
                                                                        fileStream.CopyTo(ms);
                                                                        ms.Seek(0, SeekOrigin.Begin);
                                                                        fileStream.Close();

                                                                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                                        Nextbi.BeginInit();
                                                                        Nextbi.StreamSource = ms;
                                                                        //if (isVerticalImage)
                                                                        //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                        Nextbi.EndInit();
                                                                        //imgwalletSB1.Source = bi;
                                                                        if (Nextbi.Width >= Nextbi.Height)
                                                                            Landscape = true;
                                                                        else
                                                                            Landscape = false;
                                                                    }
                                                                    imgwalletSB1.Source = Nextbi;


                                                                    //Assign to previous page
                                                                    ErrorHandler.ErrorHandler.LogFileWrite("Check if Page no " + nextPage + " for blank");
                                                                    SceneInfo IsNextBlankPage = BlankPageSceneDetails.Where(x => x.PageNo == nextPage.ToString()).FirstOrDefault();

                                                                    BitmapImage Prevbi = new BitmapImage();

                                                                    if (IsNextBlankPage != null)
                                                                    {
                                                                        SceneBlankPage = IsNextBlankPage;
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("Page no " + nextPage + " is blank");
                                                                        //is blank
                                                                        string editedBlankPagePath = System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), _nameary[i] + "_" + nextPage + ".jpg");
                                                                        if (!File.Exists(editedBlankPagePath))
                                                                        {
                                                                            //If blank page not exists then create blank blank for the same 
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File not exists :" + editedBlankPagePath);
                                                                            //Create blank page with text and put in editedimage folder 
                                                                            PrepareImageBlankPage(objPhotoInfo, SceneBlankPage.ThemeName, Convert.ToString(nextPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                            ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        }
                                                                        //if (isBlankPageRequired)
                                                                        //{
                                                                        //    PrepareImageBlankPage(objPhotoInfo, SceneBlankPage.ThemeName, Convert.ToString(nextPage), editedBlankPagePath, SceneBlankPage.SceneId, SBID);
                                                                        //    ErrorHandler.ErrorHandler.LogFileWrite("File created for :" + editedBlankPagePath);
                                                                        //}
                                                                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), (_nameary[i] + "_" + nextPage + ".jpg"))))
                                                                        {
                                                                            //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                            MemoryStream ms = new MemoryStream();
                                                                            fileStream.CopyTo(ms);
                                                                            ms.Seek(0, SeekOrigin.Begin);
                                                                            fileStream.Close();
                                                                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                            Prevbi.BeginInit();
                                                                            Prevbi.StreamSource = ms;
                                                                            //if (isVerticalImage)
                                                                            //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                            Prevbi.EndInit();
                                                                            //imgwalletSB1.Source = bi;
                                                                            if (Nextbi.Width >= Nextbi.Height)
                                                                                Landscape = true;
                                                                            else
                                                                                Landscape = false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ErrorHandler.ErrorHandler.LogFileWrite("Page no " + nextPage + " is not blank");
                                                                        //Not blank                                                                       
                                                                        PhotoInfo objNextPhotoInfo = objPhotoLst.Where(g => g.PageNo == nextPage).FirstOrDefault();
                                                                        if (objNextPhotoInfo != null)
                                                                        {
                                                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, _nameary[0].ToString(), objNextPhotoInfo.DG_Photos_pkey.ToString() + ".jpg")))
                                                                            {
                                                                                //bool isVerticalImage = IsVerticalImage(_nameary[0].ToString() + "\\" + (_nameary[i]));
                                                                                MemoryStream ms = new MemoryStream();
                                                                                fileStream.CopyTo(ms);
                                                                                ms.Seek(0, SeekOrigin.Begin);
                                                                                fileStream.Close();
                                                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019
                                                                                Prevbi.BeginInit();
                                                                                Prevbi.StreamSource = ms;
                                                                                //if (isVerticalImage)
                                                                                //    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                                                Prevbi.EndInit();
                                                                                //imgwalletSB1.Source = bi;
                                                                                if (Prevbi.Width >= Prevbi.Height)
                                                                                    Landscape = true;
                                                                                else
                                                                                    Landscape = false;
                                                                            }

                                                                        }
                                                                    }
                                                                    imgwalletSB2.Source = Prevbi;//Previous page content assigned 

                                                                    //Now print as page is ready to print
                                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;

                                                                    string xyz = string.Empty;
                                                                    RenderOptions.SetEdgeMode(Grd8by16, EdgeMode.Aliased);
                                                                    RenderOptions.SetBitmapScalingMode(Grd8by16, BitmapScalingMode.HighQuality);
                                                                    Grd8by16.UpdateLayout();
                                                                    Grd8by16.SnapsToDevicePixels = true;
                                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                                    Grd8by16.Measure(size);
                                                                    Grd8by16.Arrange(new Rect(
                                                                      new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                                      size));
                                                                    try
                                                                    {
                                                                        int copy = 1;
                                                                        while (totalPrintCopy >= copy)
                                                                        {
                                                                            //PrintDialog dialog = new PrintDialog();
                                                                            //if (dialog.ShowDialog() == true)
                                                                            //{
                                                                            //    dialog.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            //}
                                                                            dlg.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                                            copy++;
                                                                        }
                                                                    }
                                                                    catch (OutOfMemoryException ex)
                                                                    {
                                                                        GC.AddMemoryPressure(10000);
                                                                        MemoryManagement.FlushMemory();
                                                                    }
                                                                    xyz = dlg.PrintQueue.FullName.ToString();

                                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                                    //Remove printed item from list in order to not print it again
                                                                    //if (_namearyPrinted.Contains(objPhotoInfo.DG_Photos_pkey.ToString()))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + objPhotoInfo.DG_Photos_pkey);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(objPhotoInfo.DG_Photos_pkey.ToString());
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("3271 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                    //if (_namearyPrinted.Contains(_nameary[i]))
                                                                    //{
                                                                    //    ErrorHandler.ErrorHandler.LogFileWrite("Removing item:" + _nameary[i]);
                                                                    //    try
                                                                    //    {
                                                                    //        _namearyPrinted.Remove(_nameary[i]);
                                                                    //    }
                                                                    //    catch (Exception ex)
                                                                    //    {
                                                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                                                    //        ErrorHandler.ErrorHandler.LogFileWrite("3256 Removing item exception");
                                                                    //    }
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                _namearyPrinted.Clear();
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorHandler.ErrorHandler.LogError(ex);
                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            }
                                        }
                                    }
                                    #region Unique 4x5
                                    else if (Image.DG_PrinterQueue_ProductID == 4) // for unique 
                                    {
                                        LayoutImage(orientaion, position, Imagebarcodenormal_Copy, photoname);
                                        if (orientaion.Equals("Vertical"))
                                        {
                                            int len = Imagebarcodenormal_Copy.Text.Length;
                                            Thickness margin = Imagebarcodenormal_Copy.Margin;
                                            if (position.Equals("Top Right"))
                                            {
                                                if (len <= 20)
                                                    margin.Right = -50;
                                                else if (len <= 30)
                                                    margin.Right = -75;
                                                else if (len <= 40)
                                                    margin.Right = -100;
                                                else if (len <= 50)
                                                    margin.Right = -125;
                                                else if (len <= 60)
                                                    margin.Right = -150;
                                                else
                                                    margin.Right = -170;
                                            }
                                            else if (position.Equals("Bottom Right"))
                                            {
                                                margin.Bottom = 22;
                                                if (len <= 20)
                                                    margin.Right = -25;
                                                else if (len <= 30)
                                                    margin.Right = -50;
                                                else if (len <= 40)
                                                    margin.Right = -75;
                                                else if (len <= 50)
                                                    margin.Right = -100;
                                                else if (len <= 60)
                                                    margin.Right = -125;
                                                else
                                                    margin.Right = -150;
                                            }
                                            Imagebarcodenormal_Copy.Margin = margin;
                                        }
                                        //string[] _nameary = Image.DG_Photos_RFID.Split(',');
                                        string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                        if (_nameary.Count() >= 4)
                                        {
                                            bool Landscape = false;
                                            imgmain.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;
                                            ////have to define path here for image 1
                                            try
                                            {
                                                if (ProductType.DG_IsBorder == true)
                                                    borderMargin = 20;

                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwallet1.Source = bi;
                                                    if (bi.Width > bi.Height)
                                                        Landscape = true;
                                                    else
                                                        Landscape = false;
                                                    //commented below by ajay and updated on 6 aug to solve the images size issue.
                                                    //imgwallet1.Margin = new Thickness(90 + (20 - borderMargin), 40 + (20 - borderMargin), borderMargin, borderMargin);
                                                    prtGrdWallet1.Margin = new Thickness(90 + (20 - borderMargin), 40 + (20 - borderMargin), borderMargin, borderMargin);
                                                }

                                                ////have to define path here for image 2

                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[1]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwallet2.Source = bi;
                                                    //commented below by ajay and updated on 6 aug to solve the images size issue.
                                                    //imgwallet2.Margin = new Thickness(borderMargin, 40 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                    prtGrdWallet2.Margin = new Thickness(borderMargin, 40 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                }

                                                ////have to define path here for image 3
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[2] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[2]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwallet3.Source = bi;
                                                    //imgwallet3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 40 + (20 - borderMargin));
                                                    prtGrdWallet3.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 40 + (20 - borderMargin));
                                                }

                                                ////have to define path here for image 4
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[3] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[3]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwallet4.Source = bi;
                                                    //commented below by ajay and updated on 6 aug to solve the images size issue.
                                                    //imgwallet4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 40 + (20 - borderMargin));
                                                    prtGrdWallet4.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 40 + (20 - borderMargin));
                                                }


                                                if (Landscape)
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                else
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                string xyz = string.Empty;
                                                RenderOptions.SetEdgeMode(Grd8by6, EdgeMode.Aliased);
                                                RenderOptions.SetBitmapScalingMode(Grd8by6, BitmapScalingMode.HighQuality);
                                                Grd8by6.UpdateLayout();
                                                Grd8by6.SnapsToDevicePixels = true;
                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                Grd8by6.Measure(size);
                                                Grd8by6.Arrange(new Rect(
                                                  new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                  size));
                                                try
                                                {
                                                    int copy = 1;
                                                    while (totalPrintCopy >= copy)
                                                    {
                                                        //PrintDialog dialog = new PrintDialog();
                                                        //if (dialog.ShowDialog() == true)
                                                        //{
                                                        //    dialog.PrintVisual(Grd8by16, Image.DG_PrinterQueue_Pkey.ToString());
                                                        //}
                                                        dlg.PrintVisual(Grd8by6, Image.DG_PrinterQueue_Pkey.ToString());
                                                        copy++;
                                                    }
                                                }
                                                catch (OutOfMemoryException ex)
                                                {
                                                    GC.AddMemoryPressure(10000);
                                                    MemoryManagement.FlushMemory();
                                                }
                                                xyz = dlg.PrintQueue.FullName.ToString();

                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorHandler.ErrorHandler.LogError(ex);
                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Unique 4 Small Wallet
                                    else if (Image.DG_PrinterQueue_ProductID == 101) // for unique 4 Small Wallet
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite("unique 4 Small Wallet");
                                        //string[] _nameary = Image.DG_Photos_RFID.Split(',');
                                        LayoutImage(orientaion, position, Imagebarcodenormal_CopyNew, photoname);

                                        if (position.Equals("Top Right") || position.Equals("Bottom Right"))
                                        {
                                            if (orientaion.Equals("Vertical"))
                                            {
                                                int len = Imagebarcodenormal_CopyNew.Text.Length;
                                                Thickness margin = Imagebarcodenormal_CopyNew.Margin;
                                                if (len <= 20)
                                                    margin.Right = -25;
                                                else if (len <= 30)
                                                    margin.Right = -50;
                                                else if (len <= 40)
                                                    margin.Right = -75;
                                                else if (len <= 50)
                                                    margin.Right = -100;
                                                else if (len <= 60)
                                                    margin.Right = -125;
                                                else
                                                    margin.Right = -150;

                                                Imagebarcodenormal_CopyNew.Margin = margin;
                                            }
                                        }

                                        string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                        if (_nameary.Count() >= 4)
                                        {
                                            bool Landscape = false;
                                            imgmain.Source = null;
                                            imgwalletsmall1SW.Source = null;
                                            imgwalletsmall2SW.Source = null;
                                            imgwalletsmall3SW.Source = null;
                                            imgwalletsmall4SW.Source = null;
                                            ////have to define path here for image 1
                                            try
                                            {
                                                if (ProductType.DG_IsBorder == true)
                                                    borderMargin = 20;

                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwalletsmall1SW.Source = bi;
                                                    if (bi.Width > bi.Height)
                                                        Landscape = true;
                                                    else
                                                        Landscape = false;
                                                    if (IsSmallWalletwith4ImgCutter)
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter true");
                                                        imgwalletsmall1SW.Margin = new Thickness(118, 105, 13, 20);
                                                    }
                                                    else
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter false");
                                                        imgwalletsmall1SW.Margin = new Thickness(90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin, borderMargin);
                                                    }


                                                }

                                                ////have to define path here for image 2

                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwalletsmall2SW.Source = bi;
                                                    if (IsSmallWalletwith4ImgCutter)
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter true1");
                                                        imgwalletsmall2SW.Margin = new Thickness(21, 105, 110, 20);
                                                    }
                                                    else
                                                    {
                                                        ErrorHandler.ErrorHandler.LogFileWrite("IsSmallWalletwith4ImgCutter true2");
                                                        imgwalletsmall2SW.Margin = new Thickness(borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin), borderMargin);
                                                    }
                                                }

                                                ////have to define path here for image 3
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[2] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwalletsmall3SW.Source = bi;

                                                    if (IsSmallWalletwith4ImgCutter)
                                                    {
                                                        imgwalletsmall3SW.Margin = new Thickness(118, 2, 13, 122);
                                                    }
                                                    else
                                                    {
                                                        imgwalletsmall3SW.Margin = new Thickness(90 + (20 - borderMargin), borderMargin, borderMargin, 90 + (20 - borderMargin));

                                                    }
                                                }

                                                ////have to define path here for image 4
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[3] + ".jpg"))))
                                                {
                                                    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    if (isVerticalImage)
                                                        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                    bi.EndInit();
                                                    imgwalletsmall4SW.Source = bi;
                                                    if (IsSmallWalletwith4ImgCutter)
                                                    {
                                                        imgwalletsmall4SW.Margin = new Thickness(21, 2, 110, 122);
                                                    }
                                                    else
                                                    {
                                                        imgwalletsmall4SW.Margin = new Thickness(borderMargin, borderMargin, 90 + (20 - borderMargin), 90 + (20 - borderMargin));
                                                    }
                                                }


                                                if (Landscape)
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                else
                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                string xyz = string.Empty;
                                                RenderOptions.SetEdgeMode(Grd8by6Unique4SW, EdgeMode.Aliased);
                                                RenderOptions.SetBitmapScalingMode(Grd8by6Unique4SW, BitmapScalingMode.HighQuality);
                                                Grd8by6Unique4SW.UpdateLayout();
                                                Grd8by6Unique4SW.SnapsToDevicePixels = true;
                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                Grd8by6Unique4SW.Measure(size);
                                                Grd8by6Unique4SW.Arrange(new Rect(
                                                  new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                  size));
                                                try
                                                {
                                                    int copy = 1;
                                                    while (totalPrintCopy >= copy)
                                                    {
                                                        dlg.PrintVisual(Grd8by6Unique4SW, Image.DG_PrinterQueue_Pkey.ToString());
                                                        copy++;
                                                    }
                                                }
                                                catch (OutOfMemoryException ex)
                                                {
                                                    GC.AddMemoryPressure(10000);
                                                    MemoryManagement.FlushMemory();
                                                }
                                                xyz = dlg.PrintQueue.FullName.ToString();

                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorHandler.ErrorHandler.LogError(ex);
                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            }
                                        }
                                    }
                                    #endregion

                                    #region 3 by 3
                                    else if (Image.DG_PrinterQueue_ProductID == 98)
                                    {
                                        try
                                        {
                                            string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                            if (_nameary.Count() >= 2)
                                            {
                                                bool Landscape = false;
                                                imgmain.Source = null;
                                                img3by3One.Source = null;
                                                img3by3Two.Source = null;

                                                ////have to define path here for image 1
                                                try
                                                {
                                                    if (ProductType.DG_IsBorder == true)
                                                        borderMargin = 33;

                                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                                    {
                                                        BitmapImage bi = new BitmapImage();
                                                        MemoryStream ms = new MemoryStream();
                                                        fileStream.CopyTo(ms);
                                                        ms.Seek(0, SeekOrigin.Begin);
                                                        fileStream.Close();

                                                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                        bi.BeginInit();
                                                        bi.StreamSource = ms;
                                                        bi.EndInit();
                                                        img3by3One.Source = bi;
                                                        img3by3One.Margin = new Thickness(66 + (33 - borderMargin), 150, borderMargin, 150);
                                                    }

                                                    ////have to define path here for image 2

                                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                                    {
                                                        BitmapImage bi = new BitmapImage();
                                                        MemoryStream ms = new MemoryStream();
                                                        fileStream.CopyTo(ms);
                                                        ms.Seek(0, SeekOrigin.Begin);
                                                        fileStream.Close();

                                                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                        bi.BeginInit();
                                                        bi.StreamSource = ms;
                                                        bi.EndInit();
                                                        img3by3Two.Source = bi;
                                                        img3by3Two.Margin = new Thickness(borderMargin, 150, 66 + (33 - borderMargin), 150);
                                                    }

                                                    dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                    string xyz = string.Empty;
                                                    RenderOptions.SetEdgeMode(Grd8By6For3by3, EdgeMode.Aliased);
                                                    RenderOptions.SetBitmapScalingMode(Grd8By6For3by3, BitmapScalingMode.HighQuality);
                                                    Grd8By6For3by3.UpdateLayout();
                                                    Grd8By6For3by3.SnapsToDevicePixels = true;
                                                    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                    var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                    var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                    Grd8By6For3by3.Measure(size);
                                                    Grd8By6For3by3.Arrange(new Rect(
                                                      new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                      size));
                                                    try
                                                    {
                                                        int copy = 1;
                                                        while (totalPrintCopy >= copy)
                                                        {
                                                            dlg.PrintVisual(Grd8By6For3by3, Image.DG_PrinterQueue_Pkey.ToString());
                                                            copy++;
                                                        }
                                                    }
                                                    catch (OutOfMemoryException ex)
                                                    {
                                                        GC.AddMemoryPressure(10000);
                                                        MemoryManagement.FlushMemory();
                                                    }
                                                    xyz = dlg.PrintQueue.FullName.ToString();
                                                    PrinterBusniess priObj = new PrinterBusniess();
                                                    priObj.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorHandler.ErrorHandler.LogError(ex);
                                                    printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                        }
                                    }
                                    #endregion

                                    #region Unique product 4*6(2)
                                    //By KCB ON 23 MAY 20108 to avoid execute codes if no image found
                                    else if (Image.DG_PrinterQueue_ProductID == 105)
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString() + "and printer entered in 105");
                                        string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                        var queueId = Image.DG_PrinterQueue_Pkey;
                                        ImagebarcodUniq4x6x2.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                        //Imagebarcod4x6x2_1.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                        // Position & Orienation layout

                                        //LayoutImage(orientaion, position, Imagebarcod4x6x2, Imagebarcod4x6x2.Text);
                                        ///LayoutImage4by6b2(orientaion, position, Imagebarcod4x6x2, Imagebarcod4x6x2.Text);//Existing
                                        LayoutImage4by6by2Unique(orientaion, position, ImagebarcodUniq4x6x2, ImagebarcodUniq4x6x2.Text);

                                        ErrorHandler.ErrorHandler.LogFileWrite("MarginValueUnique4X6_2 Vins =" + Convert.ToString(MarginValueUnique4X6_2));

                                        //LayoutImage(orientaion, position, Imagebarcod4x6x2_1, Imagebarcod4x6x2.Text);
                                        try
                                        {
                                            var filepath = Image.DG_PrinterQueue_Pkey.ToString();
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[0]));
                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                if (isVerticalImage)
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90; ;
                                                bi.EndInit();
                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                                if (isVerticalImage)
                                                {
                                                    img4x6Unique_1.Source = bi;
                                                    img4x6Unique_1.Margin = new Thickness(-15, 0, 0, MarginValueUnique4X6_2);//vins
                                                    //prtimg4x6_1.Margin = new Thickness(0, 0, 0, MarginValueUnique4X6_2);
                                                }
                                                else
                                                {
                                                    ErrorHandler.ErrorHandler.LogFileWrite("Vins2650");
                                                    img4x6Unique_1.Source = bi;
                                                    //prtimg4x6_1.Margin = new Thickness(0, 0, 0, MarginValueUnique4X6_2);
                                                    img4x6Unique_1.Margin = new Thickness(-15, 0, 0, MarginValueUnique4X6_2);
                                                }
                                            }
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                            {
                                                BitmapImage bi = new BitmapImage();
                                                MemoryStream ms = new MemoryStream();
                                                fileStream.CopyTo(ms);
                                                ms.Seek(0, SeekOrigin.Begin);
                                                fileStream.Close();

                                                fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString() + "\\" + (_nameary[1]));
                                                bi.BeginInit();
                                                bi.StreamSource = ms;
                                                if (isVerticalImage)
                                                    bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90;
                                                bi.EndInit();
                                                dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

                                                ErrorHandler.ErrorHandler.LogFileWrite("MarginValueUnique4X6_2 Vins =" + Convert.ToString(MarginValueUnique4X6_2));

                                                if (isVerticalImage)
                                                {
                                                    img4x6Unique_2.Source = bi;
                                                    img4x6Unique_2.Margin = new Thickness(-15, 0, 0, 0);
                                                }
                                                else
                                                {
                                                    img4x6Unique_2.Source = bi;
                                                    //img4x6_2.Margin = new Thickness(0, 0, 0, MarginValueUnique4X6_2);
                                                    img4x6Unique_2.Margin = new Thickness(-15, -20, 0, MarginValueUnique4X6_2);
                                                }
                                            }
                                            grd4x6x2Unique.UpdateLayout();

                                            ErrorHandler.ErrorHandler.LogFileWrite("IsPrintEnabled =" + System.Configuration.ConfigurationManager.AppSettings["IsPrintEnable"]);

                                            if (System.Configuration.ConfigurationManager.AppSettings["IsPrintEnable"] == "true")
                                            {
                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                //var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                                var size = new System.Windows.Size(600, 790);
                                                grd4x6x2Unique.Measure(size);
                                                grd4x6x2Unique.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                                dlg.PrintVisual(grd4x6x2Unique, Image.DG_PrinterQueue_Pkey.ToString());
                                            }

                                            //string xyz = dlg.PrintQueue.FullName.ToString();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite("PrintImages() method exception");
                                            GC.AddMemoryPressure(10000);
                                            MemoryManagement.FlushMemory();
                                            printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                        }

                                    }

                                    //else if (Image.DG_PrinterQueue_ProductID == 105)
                                    //{
                                    //    #region product 4*6(2)
                                    //    var queueId = Image.DG_PrinterQueue_Pkey;
                                    //    Imagebarcod4x6x2.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                    //    //Imagebarcod4x6x2_1.Text = " " + Image.DG_Photos_RFID.ToString() + " ";
                                    //    // Position & Orienation layout
                                    //    LayoutImage(orientaion, position, Imagebarcod4x6x2, Imagebarcod4x6x2.Text);
                                    //    //LayoutImage(orientaion, position, Imagebarcod4x6x2_1, Imagebarcod4x6x2_1.Text);
                                    //    try
                                    //    {
                                    //        var filepath = Image.DG_PrinterQueue_Pkey.ToString();
                                    //        //By KCB ON 23 MAY 2018 To print two uniqu images
                                    //        List<int> ImageIds = new List<int>();
                                    //        List<int> RFIds = new List<int>();


                                    //        string[] images = Image.DG_PrinterQueue_Image_Pkey.Split(',');
                                    //        foreach (string photo in images)
                                    //        {
                                    //            ImageIds.Add(Convert.ToInt32(photo));
                                    //        }
                                    //        string[] rfids = Image.DG_Photos_RFID.Split(',');
                                    //        foreach (string frid in images)
                                    //        {
                                    //            RFIds.Add(Convert.ToInt32(frid));
                                    //        }

                                    //        //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (ImageIds[0] + ".jpg"))))
                                    //        //{
                                    //        //    BitmapImage bi = new BitmapImage();
                                    //        //    MemoryStream ms = new MemoryStream();
                                    //        //    fileStream.CopyTo(ms);
                                    //        //    ms.Seek(0, SeekOrigin.Begin);
                                    //        //    fileStream.Close();

                                    //        //    bool isVerticalImage = IsVerticalImage(Image.DG_PrinterQueue_Pkey.ToString());
                                    //        //    bi.BeginInit();
                                    //        //    bi.StreamSource = ms;
                                    //        //    if (isVerticalImage)
                                    //        //        bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90; ;
                                    //        //    bi.EndInit();
                                    //        //    dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                    //        //    if (isVerticalImage)
                                    //        //    {
                                    //        //        img4x6_1.Source = bi;
                                    //        //        img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2);
                                    //        //        img4x6_2.Source = bi;
                                    //        //        img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                    //        //    }
                                    //        //    else
                                    //        //    {
                                    //        //        img4x6_1.Source = bi;
                                    //        //        img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2);
                                    //        //        img4x6_2.Source = bi;
                                    //        //        img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                    //        //    }
                                    //        //}
                                    //        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (ImageIds[0] + ".jpg"))))
                                    //        {
                                    //            BitmapImage bi = new BitmapImage();
                                    //            MemoryStream ms = new MemoryStream();
                                    //            fileStream.CopyTo(ms);
                                    //            ms.Seek(0, SeekOrigin.Begin);
                                    //            fileStream.Close();

                                    //            bool isVerticalImage = IsVerticalImage(System.IO.Path.Combine(Image.DG_PrinterQueue_Pkey.ToString(), ImageIds[0].ToString()));
                                    //            bi.BeginInit();
                                    //            bi.StreamSource = ms;
                                    //            if (isVerticalImage)
                                    //                bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate90; ;
                                    //            bi.EndInit();
                                    //            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                    //            if (isVerticalImage)
                                    //            {
                                    //                img4x6_1.Source = bi;
                                    //                img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2);
                                    //                //img4x6_2.Source = bi;
                                    //                //img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                    //            }
                                    //            else
                                    //            {
                                    //                img4x6_1.Source = bi;
                                    //                img4x6_1.Margin = new Thickness(0, 0, 0, MarginValue4X6_2);
                                    //            }
                                    //        }
                                    //        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(),(ImageIds[1] + ".jpg"))))
                                    //        {
                                    //            BitmapImage bi = new BitmapImage();
                                    //            MemoryStream ms = new MemoryStream();
                                    //            fileStream.CopyTo(ms);
                                    //            ms.Seek(0, SeekOrigin.Begin);
                                    //            fileStream.Close();

                                    //            bool isVerticalImage = IsVerticalImage(System.IO.Path.Combine(Image.DG_PrinterQueue_Pkey.ToString(), ImageIds[1].ToString()));
                                    //            bi.BeginInit();
                                    //            bi.StreamSource = ms;
                                    //            if (isVerticalImage)
                                    //                bi.Rotation = System.Windows.Media.Imaging.Rotation.Rotate180; ;
                                    //            bi.EndInit();
                                    //            dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;
                                    //            if (isVerticalImage)
                                    //            {
                                    //                img4x6_2.Source = bi;
                                    //                img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                    //            }
                                    //            else
                                    //            {
                                    //                img4x6_2.Source = bi;
                                    //                img4x6_2.Margin = new Thickness(0, 0, 0, 0);
                                    //            }
                                    //        }
                                    //        //end
                                    //        grd4x6x2.UpdateLayout();
                                    //        System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                    //        var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                    //        var size = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                                    //        grd4x6x2.Measure(size);
                                    //        grd4x6x2.Arrange(new Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), size));
                                    //        dlg.PrintVisual(grd4x6x2, Image.DG_PrinterQueue_Pkey.ToString());

                                    //        string xyz = dlg.PrintQueue.FullName.ToString();
                                    //        printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                    //        ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                    //        //foreach (PrinterQueueforPrint item in Image1)
                                    //        //{
                                    //        //    printBusiness.SetPrinterQueue(item.DG_PrinterQueue_Pkey);
                                    //        //    ErrorHandler.ErrorHandler.LogFileWrite(item.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                    //        //}
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        ErrorHandler.ErrorHandler.LogError(ex);
                                    //        GC.AddMemoryPressure(10000);
                                    //        MemoryManagement.FlushMemory();
                                    //        printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);

                                    //    }
                                    //    #endregion product 4*6(2)
                                    //}
                                    //end
                                    #endregion product 4*6(2)

                                    #region For Photo Album Product
                                    else if (Image.DG_PrinterQueue_ProductID == 79)           //Photo Album product
                                    {
                                        string[] _nameary = Image.DG_PrinterQueue_Image_Pkey.Split(',');

                                        int[] _rotationAngle = Image.RotationAngle.Split(',').Select(n => n.ToInt32()).ToArray();

                                        if (_nameary.Count() == 2)    //2 Photos will be printed on Photo Album
                                        {
                                            imgmain.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;
                                            imgwalletsmall1.Source = null;

                                            try
                                            {
                                                ////have to define path here for image 1
                                                string[] _DisplayAray = Image.DG_Photos_RFID.Split(',');
                                                ImagebarcodePhotoAlbum.Text = _DisplayAray.Length > 0 ? _DisplayAray[0] : string.Empty;
                                                LayoutImage(orientaion, position, ImagebarcodePhotoAlbum, ImagebarcodePhotoAlbum.Text);
                                                ImagebarcodePhotoAlbum2.Text = _DisplayAray.Length > 1 ? _DisplayAray[1] : string.Empty;
                                                LayoutImage(orientaion, position, ImagebarcodePhotoAlbum2, ImagebarcodePhotoAlbum2.Text);
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[0] + ".jpg"))))
                                                {
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019


                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();

                                                    if (_rotationAngle[0] != null && _rotationAngle[0] > 0)
                                                    {
                                                        TransformedBitmap tb = new TransformedBitmap();
                                                        tb.BeginInit();
                                                        tb.Source = bi;
                                                        // Set image rotation.
                                                        RotateTransform transform = new RotateTransform(_rotationAngle[0]);
                                                        tb.Transform = transform;
                                                        tb.EndInit();
                                                        imgPhotoAlbum1.Source = tb;
                                                    }
                                                    else
                                                    {
                                                        imgPhotoAlbum1.Source = bi;
                                                    }
                                                }

                                                ////have to define path here for image 2
                                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, Image.DG_PrinterQueue_Pkey.ToString(), (_nameary[1] + ".jpg"))))
                                                {
                                                    BitmapImage bi = new BitmapImage();
                                                    MemoryStream ms = new MemoryStream();
                                                    fileStream.CopyTo(ms);
                                                    ms.Seek(0, SeekOrigin.Begin);
                                                    fileStream.Close();

                                                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                                                    bi.BeginInit();
                                                    bi.StreamSource = ms;
                                                    bi.EndInit();
                                                    if (_rotationAngle[1] != null && _rotationAngle[1] > 0)
                                                    {
                                                        TransformedBitmap tb = new TransformedBitmap();
                                                        tb.BeginInit();
                                                        tb.Source = bi;
                                                        // Set image rotation.
                                                        RotateTransform transform = new RotateTransform(_rotationAngle[1]);
                                                        tb.Transform = transform;
                                                        tb.EndInit();
                                                        imgPhotoAlbum2.Source = tb;
                                                    }
                                                    else
                                                    {
                                                        imgPhotoAlbum2.Source = bi;
                                                    }
                                                }

                                                //A4-Landscape Printing - Borderless

                                                PageMediaSize pageSize = new PageMediaSize(PageMediaSizeName.ISOA4);
                                                dlg.PrintTicket.PageMediaSize = pageSize;
                                                dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                                                dlg.PrintTicket.PageBorderless = PageBorderless.Borderless;

                                                string xyz = string.Empty;
                                                RenderOptions.SetEdgeMode(GrdPhotoAlbum, EdgeMode.Aliased);
                                                RenderOptions.SetBitmapScalingMode(GrdPhotoAlbum, BitmapScalingMode.HighQuality);
                                                GrdPhotoAlbum.UpdateLayout();
                                                GrdPhotoAlbum.SnapsToDevicePixels = true;
                                                System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);
                                                var pageAspect = capabilities.PageImageableArea.ExtentWidth / capabilities.PageImageableArea.ExtentHeight;
                                                var size = new System.Windows.Size(1123, 794);
                                                GrdPhotoAlbum.Measure(size);
                                                GrdPhotoAlbum.Arrange(new Rect(
                                                  new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight),
                                                  size));

                                                try
                                                {
                                                    int copy = 1;
                                                    while (totalPrintCopy >= copy)
                                                    {
                                                        dlg.PrintVisual(GrdPhotoAlbum, Image.DG_PrinterQueue_Pkey.ToString());
                                                        copy++;
                                                    }
                                                }
                                                catch (OutOfMemoryException ex)
                                                {
                                                    GC.AddMemoryPressure(10000);
                                                    MemoryManagement.FlushMemory();
                                                }
                                                xyz = dlg.PrintQueue.FullName.ToString();

                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                                ErrorHandler.ErrorHandler.LogFileWrite(Image.DG_PrinterQueue_Pkey.ToString() + " send to Print at:-" + DateTime.Now.ToLongTimeString());
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorHandler.ErrorHandler.LogError(ex);
                                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                            }
                                        }
                                    }
                                    #endregion
                                }

                            }
                            else
                            {
                                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
                                ErrorHandler.ErrorHandler.LogFileWrite("No Suitable Printer not found.");
                                try
                                {
                                    ServiceController controller = new ServiceController("Spooler");
                                    switch (controller.Status)
                                    {
                                        case ServiceControllerStatus.Stopped:
                                            controller.Start();
                                            controller.Refresh();//Vinod
                                            break;
                                        case ServiceControllerStatus.Paused:
                                            controller.Start();
                                            controller.Refresh();//Vinod
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                }
                            }
                        }

                        server = null; //Vinod
                    }
                    #endregion
                    //else
                    //{
                    //    ErrorHandler.ErrorHandler.LogFileWrite("No Image Rendered");
                    //}
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Print Image", watch);
#endif
                }
                if (printerErrorCheckTime.AddSeconds(printerErrorCheckTimeoutInSec) < DateTime.Now)
                {
                    printerErrorCheckTime = DateTime.Now;
                    //CheckForPrinterError();
                }
            }
            catch (OutOfMemoryException ex)
            {
                int totMemoryAlloted = (int)GC.GetTotalMemory(false);
                GC.RemoveMemoryPressure(totMemoryAlloted); //Vinod

                ErrorHandler.ErrorHandler.LogError(ex);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                printBusiness.SetPrinterQueue(Image.DG_PrinterQueue_Pkey);
            }
            finally
            {
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000

                MemoryManagement.FlushMemory();
                Thread.Sleep(1000);


                var memory = 0.0;
                using (Process proc = Process.GetCurrentProcess())
                {
                    // The proc.PrivateMemorySize64 will returns the private memory usage in byte.
                    // Would like to Convert it to Megabyte? divide it by 1e+6
                    memory = proc.PrivateMemorySize64 / 1e+6;
                }

                int maxMemSize = 5000;
                if (ConfigurationManager.AppSettings.AllKeys.Contains("MaxMemoryAllowed") == true)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["MaxMemoryAllowed"] != "")
                    {
                        maxMemSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxMemoryAllowed"]);
                    }
                }

                if (IsMemoryOverflow() || memory >= maxMemSize)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Printconsole restarted at memory: " + Convert.ToString(memory));

                    KillMserverProcesses();
                    Process thisProc = Process.GetCurrentProcess();
                    System.Windows.Forms.Application.Restart(); ///
                    thisProc.Kill();
                    Application.Current.Shutdown();
                }

                PrintTimer.Start();

                //Force garbage collection.// Wait for all finalizers to complete before continuing.
                //GC.Collect();                
                //GC.WaitForPendingFinalizers();

                Image = null; //Vinod



            }
        }


        PrintManager MW;
        public bool isBlankPageRequired = false;

        private void PrepareImageBlankPage(PhotoInfo _objphoto, string themeName, string pageNumber, string destinationPath, int sceneID)
        {
            try
            {
                MW.WindowStyle = System.Windows.WindowStyle.None;
                //MW.ResetForm();
                MW.ImageLoaderBlankPage(_objphoto, themeName, pageNumber);
                MW.SetTextEnglishGuest("***", Convert.ToInt32(pageNumber), sceneID);
                MW.SetTextChineseGuest("***", Convert.ToInt32(pageNumber), sceneID);
                MW.UpdateLayout();
                if (MW.imageNotFound)
                    return;
                RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
                if (_objphoto.DG_Photos_IsGreen == true)
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 80;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 99;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + " PrepareImage()");
            }
            finally
            {
                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000
                MemoryManagement.FlushMemory();
            }
        }


        private void PrepareImageBlankPage(PhotoInfo _objphoto, string themeName, string pageNumber, string destinationPath, int sceneID, long storyBookID)
        {
            try
            {
                PhotoBusiness objPhoto = new PhotoBusiness();
                MW.WindowStyle = System.Windows.WindowStyle.None;
                //MW.ResetForm();

                MW.ImageLoaderBlankPage(_objphoto, themeName, pageNumber);

                List<PhotoInfo> objPhotoLst = objPhoto.GetAllStoryBookPhotoBYSTID(storyBookID);//list to check page no.
                foreach (PhotoInfo obj in objPhotoLst)
                {
                    if (String.IsNullOrEmpty(MW.selectedEnglishText) || String.IsNullOrEmpty(MW.selectedChineseText))
                    {
                        MW.LoadXamlGuestNames(obj.DG_Photos_Layering);//Just to get guest name on photo//chinese and english text 
                        isBlankPageRequired = true;
                    }
                    if (!String.IsNullOrEmpty(MW.selectedEnglishText) || !String.IsNullOrEmpty(MW.selectedChineseText))
                    {
                        break;
                    }
                }
                MW.SetTextEnglishGuest("***", Convert.ToInt32(pageNumber), sceneID);
                MW.SetTextChineseGuest("***", Convert.ToInt32(pageNumber), sceneID);
                MW.UpdateLayout();
                if (MW.imageNotFound)
                    return;
                RenderTargetBitmap _objeditedImage = MW.jCaptureScreen();
                if (_objphoto.DG_Photos_IsGreen == true)
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 80;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
                else
                {
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 99;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + " PrepareImageBlankPage() exception");
            }
            finally
            {
                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000
                MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, int maxWidth, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    //bitmapImage.DecodePixelWidth = maxWidth;// newWidth;
                    bitmapImage.DecodePixelWidth = maxWidth;// newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImageinAspectRatio(string sourceImage, int maxHeight, int maxWidth, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }


        /// <summary>
        ///By KCB ON 29 AUG 2019 For Checking 6900 whether thread should wait for printer before giving command to print
        /// </summary>
        /// <param name="code">status code, intiger value</param>
        /// <returns>bool value</returns>
        private bool needTowaitPrinter6900(int code)
        {
            switch (code)
            {
                case 1500:
                case 2100:
                case 2101:
                case 2102:
                case 2103:
                case 2104:
                case 2105:
                case 2106:
                case 2107:
                case 2304:
                case 2600:
                    return true;
                default:
                    return false;
            }
        }
        ////////////created by latika for table flow
        private void TableFlowSetting(string orientaion, string position, TextBlock txtblockNameTable, string sName,
           int MarginRight, int MarginTop, int MarginLeft, int MarginBottom, string BackColor, string FontFamily, string FontStyle, string FontWeight
            , int FontSizes, string Font, string sposition)
        {
            txtblockNameTable.Text = sName;
            System.Windows.Media.Color icolor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(BackColor);
            txtblockNameTable.Background = new SolidColorBrush(icolor);
            txtblockNameTable.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(FontFamily);
            txtblockNameTable.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(FontStyle);
            txtblockNameTable.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString(FontWeight);
            txtblockNameTable.FontSize = Convert.ToInt32(FontSizes);
            txtblockNameTable.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(Font.ToString());

            RotateTransform RotateTrsfrm = new RotateTransform();

            txtblockNameTable.RenderTransform = RotateTrsfrm;
            //Margin="45,25,0,25"                                                
            Thickness margin = txtblockNameTable.Margin;
            int len = imgTableName.Text.Length;
            margin.Right = MarginRight;
            margin.Top = MarginTop;
            margin.Left = MarginLeft;
            margin.Bottom = MarginBottom;
            txtblockNameTable.Margin = margin;

            txtblockNameTable.HorizontalAlignment = HorizontalAlignment.Right;
            txtblockNameTable.VerticalAlignment = VerticalAlignment.Top;

            if (sposition.Equals("Top-Left"))
            {

                // orientaion = "Horizontal";
                txtblockNameTable.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockNameTable.VerticalAlignment = VerticalAlignment.Top;
                //txtblockNameTable.Text = photoname;
                //Thickness margin = txtblockName.Margin;
                //margin.Left = 30;
                //txtblockNameTable.Margin = margin;
            }
            else if (sposition.Equals("Top-Right"))
            {

                // orientaion = "Horizontal";
                txtblockNameTable.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockNameTable.VerticalAlignment = VerticalAlignment.Top;

                //if (orientaion.Equals("Horizontal"))
                //{


                //    //margin.Right = len + 16;
                //    //txtblockNameTable.Margin = margin;
                //}
                //else if (orientaion.Equals("Vertical"))
                //{

                //    if (len < 6)
                //        margin.Right = 15;
                //    else if (len < 12)
                //        margin.Right = 10;
                //    else if (len <= 14)
                //        margin.Right = -len - 5;
                //    else
                //        margin.Right = -len - 20;

                //    txtblockNameTable.Margin = margin;
                //}
            }
            else if (position.Equals("Bottom-Left"))
            {

                // orientaion = "Vertical";
                txtblockNameTable.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockNameTable.VerticalAlignment = VerticalAlignment.Bottom;


                //if (orientaion.Equals("Vertical"))
                //{

                //    margin.Bottom = 70;
                //    margin.Left = 25;
                //    txtblockNameTable.Margin = margin;
                //}
                //else
                //{

                //    margin.Bottom = 22;
                //    margin.Left = 25;
                //    txtblockNameTable.Margin = margin;
                //}

            }
            else if (position.Equals("Bottom-Right"))
            {

                // orientaion = "Vertical";
                txtblockNameTable.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockNameTable.VerticalAlignment = VerticalAlignment.Bottom;

                int len1 = txtblockNameTable.Text.Length;
                //if (orientaion.Equals("Horizontal"))
                //{

                //    margin.Bottom = 22;
                //    margin.Right = len + 16;
                //    txtblockNameTable.Margin = margin;
                //}
                //else if (orientaion.Equals("Vertical"))
                //{
                //    int length = txtblockNameTable.Text.Length;

                //    margin.Bottom = 70;
                //    if (length < 6)
                //        margin.Right = 15;
                //    else if (length < 12)
                //        margin.Right = 10;
                //    else if (length <= 14)
                //        margin.Right = -length + 5;
                //    else
                //        margin.Right = -length - 10;

                //    txtblockNameTable.Margin = margin;
                //}
            }

            if (orientaion.Equals("Horizontal"))
            {

                RotateTrsfrm.Angle = 0;

            }
            else if (orientaion.Equals("Vertical"))
            {
                RotateTrsfrm.Angle = 270;
                //if (position.Equals("Top-Left") || position.Equals("Top-Right"))
                //{

                //    RotateTrsfrm.Angle = 90;
                //    txtblockNameTable.RenderTransform = RotateTrsfrm;
                //}
                //else if (position.Equals("Bottom-Left") || position.Equals("Bottom-Right"))
                //{

                //    RotateTrsfrm.Angle = 270;
                //    txtblockNameTable.RenderTransform = RotateTrsfrm;
                //}
            }
            txtblockNameTable.RenderTransform = RotateTrsfrm;




        }

        private static void KillMserverProcesses()
        {
            try
            {
                Process[] MserverRunning = Process.GetProcessesByName("MServer");
                if (MserverRunning.Length > 1)
                {
                    foreach (Process mserver in MserverRunning)
                        mserver.Kill();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        int physicalMemoryThreshold = 0;
        static long availableMemoryPercentage;

        /// <summary>
        /// Added by Vins
        /// Method to check available memory
        /// </summary>
        /// <returns>Returning the avail memory</returns>
        private bool IsMemoryOverflow()
        {
            bool retValue = false;
            try
            {
                availableMemoryPercentage = (PerformanceInfo.GetPhysicalAvailableMemoryInMiB() * 100) / PerformanceInfo.GetTotalMemoryInMiB();
                if (availableMemoryPercentage <= physicalMemoryThreshold)
                {
                    retValue = true;
                    ErrorHandler.ErrorHandler.LogFileWrite("Total avail memory: " + availableMemoryPercentage.ToString());
                }
                return retValue;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return retValue;
            }
        }

        /// <summary>
        /// This method is used to restart the console application
        /// Added by Vinod Salunke
        /// </summary>
        private static void RestartApplication()
        {
            Process thisProc = Process.GetCurrentProcess();
            System.Windows.Forms.Application.Restart();
            thisProc.Kill();
            Application.Current.Shutdown();
        }


        private void Print(Visual v)
        {

            System.Windows.FrameworkElement e = v as System.Windows.FrameworkElement;
            if (e == null)
                return;

            PrintDialog pd = new PrintDialog();

            //store original scale
            Transform originalScale = e.LayoutTransform;
            //get selected printer capabilities
            System.Printing.PrintCapabilities capabilities = pd.PrintQueue.GetPrintCapabilities(pd.PrintTicket);

            //get scale of the print wrt to screen of WPF visual
            //double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight /
            //               e.ActualHeight);
            double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.Width, capabilities.PageImageableArea.ExtentHeight /
                           e.Height);

            //scale = 0.98;
            //Transform the Visual to scale
            e.LayoutTransform = new ScaleTransform(scale, scale);//0.98

            //get the size of the printer page
            System.Windows.Size sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

            //update the layout of the visual to the printer page size.
            e.Measure(sz);
            e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), sz));

            //now print the visual to printer to fit on the one page.
            pd.PrintVisual(v, "My Print");

            //apply the original transform.
            e.LayoutTransform = originalScale;

        }

        /// <summary>
        /// Position & Orienation layout
        /// </summary>
        /// <param name="orientaion"></param>
        /// <param name="position"></param>
        /// <param name="txtblockName"></param>
        /// <param name="photoname"></param>
        private void LayoutImage(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 270;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                ErrorHandler.ErrorHandler.LogError("Is Print Images 3847: " + photoname);
                Thickness margin = txtblockName.Margin;
                margin.Left = 30;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    if (len < 6)
                        margin.Right = 15;
                    else if (len < 12)
                        margin.Right = 10;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 20;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                ErrorHandler.ErrorHandler.LogError("Is Print Images 3884: " + photoname);
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Bottom = 22;
                margin.Left = 25;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                ErrorHandler.ErrorHandler.LogError("Is Print Images 3895: " + photoname);
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }

        /// <summary>
        /// Position & Orienation layout 4*6 By KCB ON 06 SEPT 2018 to remove text 'Image' exist with 
        /// </summary>
        /// <param name="orientaion"></param>
        /// <param name="position"></param>
        /// <param name="txtblockName"></param>
        /// <param name="photoname"></param>
        private void LayoutImage4by6by2Unique(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("orientaion position photoname" + orientaion + "," + position + "," + photoname);
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 270;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                if (orientaion.Equals("Horizontal"))
                    margin.Left = 10;
                else
                    //margin.Left = 20;
                    #region Added by Ajay on 21 Sep18
                    margin.Left = 30;
                #endregion
                margin.Top = 45;
                //margin.Left = 10;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 10;
                    margin.Top = 45;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Top = 45;
                    if (len < 6)
                        margin.Right = 5;
                    else if (len < 12)
                        margin.Right = 20;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 30;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;

                if (orientaion.Equals("Horizontal"))
                    margin.Bottom = 45;
                else
                    margin.Bottom = 35;
                margin.Left = 10;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 45;
                    margin.Right = len + 10;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 45;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }

        /// <summary>
        /// Position & Orienation layout 4*6 By KCB ON 06 SEPT 2018 to remove text 'Image' exist with 
        /// </summary>
        /// <param name="orientaion"></param>
        /// <param name="position"></param>
        /// <param name="txtblockName"></param>
        /// <param name="photoname"></param>
        private void LayoutImage4by6b2(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 270;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                if (orientaion.Equals("Horizontal"))
                    margin.Left = 10;
                else
                    //margin.Left = 20;
                    #region Added by Ajay on 21 Sep18
                    margin.Left = 30;
                #endregion


                margin.Top = 45;
                //margin.Left = 10;

                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;

                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 10;
                    margin.Top = 45;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Top = 45;
                    if (len < 6)
                        margin.Right = 5;
                    else if (len < 12)
                        margin.Right = 20;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 30;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;

                if (orientaion.Equals("Horizontal"))
                    margin.Bottom = 45;
                else
                    margin.Bottom = 35;
                margin.Left = 10;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 45;
                    margin.Right = len + 10;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 45;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }
        /// <summary>
        /// Position & Orienation layout for 5*7 By KCB ON 06 SEPT 2018 to remove text 'Image' exist with 
        /// </summary>
        /// <param name="orientaion"></param>
        /// <param name="position"></param>
        /// <param name="txtblockName"></param>
        /// <param name="photoname"></param>
        private void LayoutImage5By7(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 270;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Left = 30;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 50;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    if (len < 6)
                        margin.Right = 15;
                    else if (len < 12)
                        margin.Right = 10;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 20;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Bottom = 22;
                margin.Left = 50;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Right = len + 50;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }
        /// <summary>
        /// Position & Orienation layout
        /// </summary>
        /// <param name="orientaion"></param>
        /// <param name="position"></param>
        /// <param name="txtblockName"></param>
        /// <param name="photoname"></param>
        private void LayoutImageForUniq6by4by2(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 270;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Left = 30;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    if (len < 6)
                        margin.Right = 15;
                    else if (len < 12)
                        margin.Right = 10;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 20;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Bottom = 22;
                margin.Left = 25;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }
        /// Position & Orienation layout Unique 4*6 QR
        private void LayoutImageUniqu4by6QR(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion == string.Empty && position == string.Empty)
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;

                int len = txtblockName.Text.Length;
                Thickness margin = txtblockName.Margin;
                margin.Right = len + 16;
                txtblockName.Margin = margin;

            }
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                    //Thickness margin = txtblockName.Margin;
                    //margin.Bottom = 50;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Left = 30;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    if (len < 6)
                        margin.Right = 15;
                    else if (len < 12)
                        margin.Right = 10;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 20;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;

                if (orientaion.Equals("Vertical"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 70;
                    margin.Left = 25;
                    txtblockName.Margin = margin;
                }
                else
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Left = 25;
                    txtblockName.Margin = margin;
                }

            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 70;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }
        private void LayoutImageQrCode(string orientaion, string position, TextBlock txtblockName, string photoname)
        {
            if (orientaion == string.Empty && position == string.Empty)
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;

                int len = txtblockName.Text.Length;
                Thickness margin = txtblockName.Margin;
                margin.Right = len + 16;
                txtblockName.Margin = margin;

            }
            if (orientaion.Equals("Horizontal"))
            {
                RotateTransform RotateTrsfrm = new RotateTransform();
                RotateTrsfrm.Angle = 0;
                txtblockName.RenderTransform = RotateTrsfrm;
            }
            else if (orientaion.Equals("Vertical"))
            {
                if (position.Equals("Top Left") || position.Equals("Top Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                }

                else if (position.Equals("Bottom Left") || position.Equals("Bottom Right"))
                {
                    RotateTransform RotateTrsfrm = new RotateTransform();
                    RotateTrsfrm.Angle = 90;
                    txtblockName.RenderTransform = RotateTrsfrm;
                    //Thickness margin = txtblockName.Margin;
                    //margin.Bottom = 50;
                }
            }

            if (position.Equals("Top Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                Thickness margin = txtblockName.Margin;
                margin.Left = 30;
                txtblockName.Margin = margin;
            }
            else if (position.Equals("Top Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Top;
                txtblockName.Text = photoname;
                if (orientaion.Equals("Horizontal"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int len = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    if (len < 6)
                        margin.Right = 15;
                    else if (len < 12)
                        margin.Right = 10;
                    else if (len <= 14)
                        margin.Right = -len - 5;
                    else
                        margin.Right = -len - 20;

                    txtblockName.Margin = margin;
                }
            }
            else if (position.Equals("Bottom Left"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Left;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;

                if (orientaion.Equals("Vertical"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 70;
                    margin.Left = 25;
                    txtblockName.Margin = margin;
                }
                else
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Left = 25;
                    txtblockName.Margin = margin;
                }

            }
            else if (position.Equals("Bottom Right"))
            {
                txtblockName.HorizontalAlignment = HorizontalAlignment.Right;
                txtblockName.VerticalAlignment = VerticalAlignment.Bottom;
                txtblockName.Text = photoname;
                int len = txtblockName.Text.Length;
                if (orientaion.Equals("Horizontal"))
                {
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 22;
                    margin.Right = len + 16;
                    txtblockName.Margin = margin;
                }
                else if (orientaion.Equals("Vertical"))
                {
                    int length = txtblockName.Text.Length;
                    Thickness margin = txtblockName.Margin;
                    margin.Bottom = 70;
                    if (length < 6)
                        margin.Right = 15;
                    else if (length < 12)
                        margin.Right = 10;
                    else if (length <= 14)
                        margin.Right = -length + 5;
                    else
                        margin.Right = -length - 10;

                    txtblockName.Margin = margin;
                }
            }
        }

        #endregion
        void CheckForPrinterError()
        {
            try
            {
                var server = new PrintServer();
                PrinterBusniess printBuss = null;
                var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
                int printQueueId = 0;
                foreach (PrintQueue PQ in queues)
                {
                    //Check printer is in Error state
                    if ((PQ.IsInError || PQ.IsOutOfPaper || PQ.IsPaperJammed) && PQ.NumberOfJobs > 0)
                    {
                        string statusReport = string.Empty;
                        if (PQ.IsInError)
                        {
                            statusReport = "Is in an error state.";
                        }
                        else if (PQ.IsOutOfPaper)
                        {
                            statusReport = "Is out of paper.";
                        }
                        else if (PQ.IsPaperJammed)
                        {
                            statusReport = "Has a paper jam.";
                        }
                        ErrorHandler.ErrorHandler.LogFileWrite(PQ.FullName + " Printer " + statusReport + " status, contains total " + PQ.NumberOfJobs + " jobs, logged on " + DateTime.Now.ToLongTimeString());

                        printBuss = new PrinterBusniess();
                        //List<AssociatedPrintersInfo> associatedPrinterList = printBuss.GetAssociatedPrintersforPrint(null, App.SubStoreId);
                        //check if the printer is associated in our app.
                        if (PQ.FullName.ToLower().Contains("epson stylus pro 7800"))
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Printer in error state is " + PQ.FullName + ".Continuing to check other print queue.");
                            continue;
                        }

                        if (associatedPrinterList.Where(o => string.Compare(System.IO.Path.Combine(o.DG_AssociatedPrinters_Name), System.IO.Path.Combine(PQ.FullName), true) == 0).Count() > 0)
                        {
                            if (_objlastusedprinter != null)
                            {
                                var item = _objlastusedprinter.Where(o => string.Compare(System.IO.Path.Combine(o.Printername), System.IO.Path.Combine(PQ.FullName), true) == 0).FirstOrDefault();
                                if (item != null)
                                {
                                    item.IsInError = true;
                                    item.printerErrorRaiseDateTime = DateTime.Now;
                                }
                            }
                            PQ.Refresh();
                            List<int> listIds = new List<int>();
                            foreach (PrintSystemJobInfo job in PQ.GetPrintJobInfoCollection())
                            {
                                if (string.Compare(job.Submitter, Environment.UserName, true) == 0)
                                {
                                    bool isNumeric = int.TryParse(((PrintSystemObject)job).Name, out printQueueId);
                                    if (isNumeric == true)
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite("Cancelling job from Printer : " + PQ.FullName + ", Printer Queue Id : " + printQueueId.ToString());
                                        job.Cancel();
                                        if (!listIds.Contains(printQueueId))
                                        {
                                            ErrorHandler.ErrorHandler.LogFileWrite("Setting Printer Queue Id : " + printQueueId.ToString() + " for Reprint.");
                                            printBuss.SetPrinterQueueForReprint(printQueueId);
                                            listIds.Add(printQueueId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_objlastusedprinter != null)
                        {
                            var item = _objlastusedprinter.Where(o => string.Compare(System.IO.Path.Combine(o.Printername), System.IO.Path.Combine(PQ.FullName), true) == 0).FirstOrDefault();
                            if (item != null && item.IsInError == true && item.printerErrorRaiseDateTime != null && Convert.ToDateTime(item.printerErrorRaiseDateTime).AddMinutes(printerErrorGap) < DateTime.Now)
                                item.IsInError = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        bool IsVerticalImage(string imageKey)
        {
            bool IsimageVertical = false;
            try
            {
                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(subStorePrintPath, (imageKey + ".jpg"))))
                {
                    BitmapImage bi = new BitmapImage();
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();

                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    if (bi.Width < bi.Height)
                    {
                        IsimageVertical = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Exception in IsVerticalImage()" + ex.Message);
            }
            return IsimageVertical;
        }

        private BitmapImage GetImage(string id)
        {
            Bitmap bm = null;
            try
            {
                var qrValue = id;
                var barcodeWriter = new BarcodeWriter
                {
                    Options = new EncodingOptions
                    {
                        Height = 80,
                        Width = 80
                    }
                };

                barcodeWriter.Options.Height = 100;
                barcodeWriter.Options.Width = 100;
                barcodeWriter.Format = BarcodeFormat.QR_CODE;
                //string QRCodeWebURL = System.Configuration.ConfigurationManager.AppSettings["QRCodeWebURL"];
                // if (!string.IsNullOrEmpty(QRCodeWebURL))
                //{
                //     qrValue = QRCodeWebURL + qrValue;
                // }

                bm = barcodeWriter.Write(qrValue);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("id is empty" + id);
            }

            return ToBitmapImage(bm);
        }

        public static BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Jpeg);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }


        private void FillText(string textValue)
        {
            try
            {
                txtQrCodeText1.Text = string.Empty;
                txtQrCodeText2.Text = string.Empty;
                txtQrCodeText3.Text = string.Empty;

                if (!string.IsNullOrEmpty(textValue))
                {
                    string[] TextSeparators = { "@@##" };
                    string[] strTextArr = textValue.Split(TextSeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] PropertySeparators = { "@@@@" };
                    string[] strPropertyArr1 = strTextArr[0].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPropertyArr2 = strTextArr[1].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    string[] strPropertyArr3 = strTextArr[2].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);

                    if (Convert.ToBoolean(strTextArr[3]))
                    {
                        hline.Visibility = Visibility.Collapsed;
                    }
                    //chkshowHideLine.IsChecked = Convert.ToBoolean(strTextArr[3]);

                    string txt1 = strPropertyArr1[0].Replace("\r\n", string.Empty);

                    //txtQrCodeText1.Text = txt1;
                    FillBold(txtQrCodeText1, txt1);

                    txtQrCodeText1.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr1[1]);
                    txtQrCodeText1.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strPropertyArr1[2]);
                    txtQrCodeText1.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strPropertyArr1[3]);
                    if (Convert.ToBoolean(strPropertyArr1[4]))
                        txtQrCodeText1.TextDecorations = TextDecorations.Underline;
                    txtQrCodeText1.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr1[5]);
                    txtQrCodeText1.FontSize = Convert.ToInt16(strPropertyArr1[6]);
                    if (Convert.ToBoolean(strPropertyArr1[7]))
                        txtQrCodeText1.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");
                    AlignText(txtQrCodeText1, strPropertyArr1[8].ToString());


                    string txt2 = strPropertyArr2[0].Replace("\r\n", string.Empty);
                    //txtQrCodeText2.Text = txt2;
                    FillBold(txtQrCodeText2, txt2);
                    txtQrCodeText2.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr2[1]);
                    txtQrCodeText2.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strPropertyArr2[2]);
                    txtQrCodeText2.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strPropertyArr2[3]);
                    if (Convert.ToBoolean(strPropertyArr2[4]))
                        txtQrCodeText2.TextDecorations = TextDecorations.Underline;
                    txtQrCodeText2.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr2[5]);
                    txtQrCodeText2.FontSize = Convert.ToInt16(strPropertyArr2[6]);
                    if (Convert.ToBoolean(strPropertyArr2[7]))
                        txtQrCodeText2.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");

                    AlignText(txtQrCodeText2, strPropertyArr2[8].ToString());

                    string txt3 = strPropertyArr3[0].Replace("\r\n", string.Empty);
                    FillBold(txtQrCodeText3, txt3);
                    //txtQrCodeText3.Text = txt3;
                    txtQrCodeText3.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr3[1]);
                    txtQrCodeText3.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strPropertyArr3[2]);
                    txtQrCodeText3.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strPropertyArr3[3]);
                    if (Convert.ToBoolean(strPropertyArr3[4]))
                        txtQrCodeText3.TextDecorations = TextDecorations.Underline;
                    txtQrCodeText3.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strPropertyArr3[5]);
                    txtQrCodeText3.FontSize = Convert.ToInt16(strPropertyArr3[6]);
                    if (Convert.ToBoolean(strPropertyArr3[7]))
                        txtQrCodeText3.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");
                    AlignText(txtQrCodeText3, strPropertyArr3[8].ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        private void FillBottomText(string textValue)
        {
            try
            {
                txtQrCodeUrl.Text = string.Empty;


                if (!string.IsNullOrEmpty(textValue))
                {
                    string[] TextSeparators = { "@@@@" };
                    string[] strTextArr = textValue.Split(TextSeparators, StringSplitOptions.RemoveEmptyEntries);
                    // string[] PropertySeparators = { "@@@@" };
                    // string[] strPropertyArr1 = strTextArr[0].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    //  string[] strPropertyArr2 = strTextArr[1].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);
                    //  string[] strPropertyArr3 = strTextArr[2].Split(PropertySeparators, StringSplitOptions.RemoveEmptyEntries);

                    string txt1 = strTextArr[0].Replace("\r\n", string.Empty);
                    //txtQrCodeUrl.Text =
                    //txtQrCodeText1.Text = txt1;
                    FillBold(txtQrCodeUrl, txt1);

                    txtQrCodeUrl.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(strTextArr[1]);
                    txtQrCodeUrl.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(strTextArr[2]);
                    txtQrCodeUrl.FontStyle = (System.Windows.FontStyle)new FontStyleConverter().ConvertFromString(strTextArr[3]);
                    if (Convert.ToBoolean(strTextArr[4]))
                        txtQrCodeUrl.TextDecorations = TextDecorations.Underline;
                    txtQrCodeUrl.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(strTextArr[5]);
                    txtQrCodeUrl.FontSize = Convert.ToInt16(strTextArr[6]);
                    if (Convert.ToBoolean(strTextArr[7]))
                        txtQrCodeUrl.FontWeight = (System.Windows.FontWeight)new FontWeightConverter().ConvertFromString("Bold");

                    AlignText(txtQrCodeUrl, strTextArr[8].ToString());

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void FillBold(TextBlock txt, string textValue)
        {
            var parts = textValue.Split(new[] { "<b>", "</b>" }, StringSplitOptions.None);
            if (parts.Length > 1)
            {
                bool isbold = false; // Start in normal mode
                foreach (var part in parts)
                {
                    if (isbold)
                        txt.Inlines.Add(new Bold(new Run(part)));
                    else
                    {
                        var ipart = part.Split(new[] { "<i>", "</i>" }, StringSplitOptions.None);
                        var bipart = part.Split(new[] { "<bi>", "</bi>" }, StringSplitOptions.None);
                        if (ipart.Length > 1)
                        {
                            FillItalic(txt, part);
                        }
                        else if (bipart.Length > 1)
                        {
                            FillBoldItalic(txt, part);
                        }
                        else
                        {
                            txt.Inlines.Add(new Run(part));
                        }
                    }
                    //txt.Inlines.Add(new Run(part));

                    isbold = !isbold; // toggle between bold and not bold

                }
                return;

            }
            var iparts = textValue.Split(new[] { "<i>", "</i>" }, StringSplitOptions.None);
            bool isItalic = false; // Start in normal mode
            if (iparts.Length > 1)
            {
                foreach (var part in iparts)
                {
                    if (isItalic)
                        txt.Inlines.Add(new Italic(new Run(part)));
                    else
                    {
                        var ibipart = part.Split(new[] { "<bi>", "</bi>" }, StringSplitOptions.None);
                        if (ibipart.Length > 1)
                        {
                            //txt.Inlines.Add(new Run(part));
                            FillBoldItalic(txt, part);
                        }
                        else
                        {
                            txt.Inlines.Add(new Run(part));
                        }
                    }


                    isItalic = !isItalic; // toggle between bold and not bold

                }
                return;
            }

            var biparts = textValue.Split(new[] { "<bi>", "</bi>" }, StringSplitOptions.None);
            if (biparts.Length > 1)
            {
                bool isboldItalic = false; // Start in normal mode
                foreach (var part in biparts)
                {
                    if (isboldItalic)
                    {
                        //txt.Inlines.Add(new Bold(new Run(part)));
                        txt.Inlines.Add(new Bold(new Italic(new Run(part))));
                        //txt.Inlines.Add(new Italic(new Run(part)));
                    }
                    else
                        txt.Inlines.Add(new Run(part));

                    isboldItalic = !isboldItalic; // toggle between bold and not bold

                }
                return;
            }
            if (biparts.Length == 1 && iparts.Length == 1 && parts.Length == 1)
            {
                txt.Inlines.Add(textValue);
            }

        }
        private void FillItalic(TextBlock txt, string textValue)
        {
            var iparts = textValue.Split(new[] { "<i>", "</i>" }, StringSplitOptions.None);
            bool isItalic = false; // Start in normal mode
            if (iparts.Length > 1)
            {
                foreach (var part in iparts)
                {
                    if (isItalic)
                        txt.Inlines.Add(new Italic(new Run(part)));
                    else
                    {
                        FillBoldItalic(txt, part);
                        //txt.Inlines.Add(new Run(part));
                    }

                    isItalic = !isItalic; // toggle between bold and not bold

                }
            }
            else
            {
                txt.Inlines.Add(textValue);
            }
        }
        private void FillBoldItalic(TextBlock txt, string textValue)
        {
            var biparts = textValue.Split(new[] { "<bi>", "</bi>" }, StringSplitOptions.None);
            if (biparts.Length > 1)
            {
                bool isboldItalic = false; // Start in normal mode
                foreach (var part in biparts)
                {
                    if (isboldItalic)
                    {
                        txt.Inlines.Add(new Bold(new Italic(new Run(part))));
                    }
                    else
                        txt.Inlines.Add(new Run(part));

                    isboldItalic = !isboldItalic; // toggle between bold and not bold

                }
            }
            else
            {
                txt.Inlines.Add(textValue);
            }
        }

        private void AlignText(TextBlock txt, string textValue)
        {
            if (textValue == "Center")
            {
                txt.TextAlignment = TextAlignment.Center;
            }
            else if (textValue == "Left")
            {
                txt.TextAlignment = TextAlignment.Left;
            }
            else if (textValue == "Right")
            {
                txt.TextAlignment = TextAlignment.Right;
            }
            else if (textValue == "Justify")
            {
                txt.TextAlignment = TextAlignment.Justify;
            }
            else
            {
                txt.TextAlignment = TextAlignment.Center;
            }
        }
    }
    public class currentTime
    {
        public string Printername { get; set; }
        public DateTime PrintDate { get; set; }
        public TimeSpan PrintTime { get; set; }
        public int productId { get; set; }
        public int OrderDetailId { get; set; }
        public bool IsInError { get; set; }
        public DateTime? printerErrorRaiseDateTime = null;
        public currentTime()
        {
            IsInError = false;
        }
    }
    public static class Global
    {
        public static double FontSize
        {
            get
            {
                //return 40; 
                //Get ImageBarCodeTextFontSize setting value for current Substore.
                ConfigBusiness buss = new ConfigBusiness();
                Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
                iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, App.SubStoreId);
                string ImageBarCodeTextFontSize = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.ImageBarCodeTextFontSize).FirstOrDefault().Value;

                try
                {
                    if (string.IsNullOrEmpty(ImageBarCodeTextFontSize))
                    {
                        return 7;      //Default Value.
                    }
                    else
                    {
                        return Convert.ToDouble(ImageBarCodeTextFontSize);
                    }
                }
                catch (Exception ex)
                {
                    return Convert.ToDouble(7);
                }

            }
        }
        public static double FontSizeNew
        {
            get
            {
                //return 40; 
                //Get ImageBarCodeTextFontSize setting value for current Substore.
                ConfigBusiness buss = new ConfigBusiness();
                Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
                iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, App.SubStoreId);
                string ImageBarCodeTextFontSize = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.ImageBarCodeTextFontSize).FirstOrDefault().Value;

                try
                {
                    if (string.IsNullOrEmpty(ImageBarCodeTextFontSize))
                    {
                        return 7;      //Default Value.
                    }
                    else
                    {
                        return Convert.ToDouble(ImageBarCodeTextFontSize);
                    }
                }
                catch (Exception ex)
                {
                    return Convert.ToDouble(7);
                }

            }
        }
    }
    public static class PerformanceInfo
    {
        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetPerformanceInfo([Out] out PerformanceInformation PerformanceInformation, [In] int Size);

        [StructLayout(LayoutKind.Sequential)]
        public struct PerformanceInformation
        {
            public int Size;
            public IntPtr CommitTotal;
            public IntPtr CommitLimit;
            public IntPtr CommitPeak;
            public IntPtr PhysicalTotal;
            public IntPtr PhysicalAvailable;
            public IntPtr SystemCache;
            public IntPtr KernelTotal;
            public IntPtr KernelPaged;
            public IntPtr KernelNonPaged;
            public IntPtr PageSize;
            public int HandlesCount;
            public int ProcessCount;
            public int ThreadCount;
        }

        public static Int64 GetPhysicalAvailableMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }
        }

        public static Int64 GetTotalMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }
        }

    }
}

