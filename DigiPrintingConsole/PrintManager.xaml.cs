﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPrintingConsole.Shader;
using DigiPhoto.DataLayer.Model;
using DigiPhoto;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.ValueType;
using System.Globalization;
using DigiPhoto.IMIX.Model.Base;
using DigiPhoto.IMIX.Business.StoryBook;
using System.Configuration;

namespace DigiPrintingConsole
{
    public partial class PrintManager : Window
    {
        #region Properties
        public Collection<string> ImageArray
        {
            get;
            set;
        }
        public string ImageEffect
        {
            get;
            set;
        }
        public int ProductTypeID
        {
            get;
            set;
        }
        public bool PrintJob
        {
            get;
            set;
        }
        public FolderStructureInfo FolderPathStructure { get; set; }
        public string PhotoName { get; set; }
        private int _photoId;
        public int PrintSizeID = 0;
        public int PhotoId
        {
            get { return _photoId; }
            set
            {
                _photoId = value;
            }
        }
        public static bool DynamicBGFunctionalityActive = false;
        public bool imageNotFound = false;
        #endregion
        public PrintManager()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Printing Manager in Initialization Start.");
                InitializeComponent();
                ErrorHandler.ErrorHandler.LogFileWrite("Printing Manger in MW2.");
                mainImage.SnapsToDevicePixels = true;
                mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
                mainImage.OverridesDefaultStyle = true;
                RobotImageLoader.GetConfigData();
                ErrorHandler.ErrorHandler.LogFileWrite("Printing Manger in Completed Read Config.");
                GetStoreConfigData();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        double attributeWidth = 10;
        string _centerX = string.Empty;
        string _centerY = string.Empty;
        bool _isLicenseProduct = false;
        private int FlipMode;
        private int FlipModeY;
        private bool isGreenImage;
        ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
        ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
        MonochromeEffect _colorfiltereff = new MonochromeEffect();
        ShiftHueEffect _shifthueeff = new ShiftHueEffect();
        ShEffect _sharpeff = new ShEffect();
        BloomEffect _bloomeff = new BloomEffect();
        ToneMappingEffect _defog = new ToneMappingEffect();
        ColorKeyAlphaEffect _greenscreen = new ColorKeyAlphaEffect();
        MultiChannelContrastEffect _under = new MultiChannelContrastEffect();
        RedEyeEffect redEyeEffect = new RedEyeEffect();
        RedEyeEffect redEyeEffectSecond = new RedEyeEffect();
        redeyeMultiple redEyeEffectMultiple = new redeyeMultiple();
        redeyeMultiple redEyeEffectMultiple1 = new redeyeMultiple();
        public int PrintProductType = 0;
        public bool IsgumBallActive = false;
        public string editedImagepath = string.Empty;
        double hueshift = 0;
        double sharpen = 0;
        double currenthueshift = 0;
        double currentsharpen = 0;
        double cont = 1;
        double bright = 0;
        /// <summary>
        /// Add storybook vatiables
        /// </summary>
        ///
        public string selectedEnglishText = "";
        public string selectedChineseText = "";
        public string selectedChineseFont = "";
        public string selectedEnglishFont = "";
        public string selectedChineseFontSize = "";
        public string selectedEnglishFontSize = "";
        public bool isBlankPageRequired = false;


        private void FlipLoad()
        {

            try
            {
                if (FlipModeY == 0)
                {
                    if (FlipMode == 1)
                    {
                        zoomTransform.ScaleX = 1;
                    }
                    else
                    {
                        zoomTransform.ScaleX = 1;
                    }
                }

                zoomTransform.CenterX = _centerX.ToDouble();
                zoomTransform.CenterY = _centerY.ToDouble();

                transformGroup = new TransformGroup();
                transformGroup.Children.Add(zoomTransform);
                transformGroup.Children.Add(translateTransform);
                transformGroup.Children.Add(rotateTransform);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
            ResetForm();
        }

        /// <summary>
        /// Method updated to catchup Gumball efects_By Vinod_12Apr2018
        /// </summary>
        public void ResetForm()
        {
            translateTransform = new TranslateTransform();
            rotateTransform = new RotateTransform();
            transformGroup = new TransformGroup();
            imageNotFound = false;
            //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
            PhotoBusiness photoBusiness = new PhotoBusiness();
            ErrorHandler.ErrorHandler.LogFileWrite("Load Photo " + PhotoId);
            if (PhotoId > 0)
            {

                PhotoInfo _objphoto = photoBusiness.GetPhotoDetailsbyPhotoId(PhotoId);
                ErrorHandler.ErrorHandler.LogFileWrite(PhotoId + " Loaded successfully");
                ImageEffect = _objphoto.DG_Photos_Effects;

                ///Hide/Show Gumball score on the basis of Applied photo effects..3Apr2018..Vinod
                ///Start new code
                //Check if photo is from Gumball substore
                if (_objphoto.IsGumRideShow)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("_objphoto.IsGumRideShow =" + Convert.ToString(_objphoto.IsGumRideShow));
                    ///Hide/Show Gumball score on the basis of Applied photo effects..3Apr2018..Vinod
                    ///Start new code
                    System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                    if (!string.IsNullOrWhiteSpace(_objphoto.DG_Photos_Layering))
                    {
                        Xdocument.LoadXml(_objphoto.DG_Photos_Layering);
                    }
                    //By KCB ON 24 JUL 2018 to handle null exception of layering
                    //System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(_objphoto.DG_Photos_Layering.ToString()));
                    //while (rdr.Read())
                    //{
                    //    if (rdr.NodeType == XmlNodeType.Element)
                    //    {
                    //        switch (rdr.Name.ToString().ToLower())
                    //        {
                    //            case "gumball":
                    //                System.Xml.XmlNodeList list = Xdocument.SelectNodes("//Gumball");
                    //                foreach (System.Xml.XmlElement XElement in list)
                    //                {
                    //                    if (XElement.GetAttribute("Visibility") != null && rdr.GetAttribute("Visibility") == "Collapsed")
                    //                    {
                    //                        XElement.RemoveAllAttributes();
                    //                    }
                    //                }
                    //                break;
                    //        }
                    //    }
                    //}

                    if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Layering.ToString()))
                    {
                        System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(_objphoto.DG_Photos_Layering.ToString()));
                        while (rdr.Read())
                        {
                            if (rdr.NodeType == XmlNodeType.Element)
                            {
                                switch (rdr.Name.ToString().ToLower())
                                {
                                    case "gumball":
                                        System.Xml.XmlNodeList list = Xdocument.SelectNodes("//Gumball");
                                        foreach (System.Xml.XmlElement XElement in list)
                                        {
                                            if (XElement.GetAttribute("Visibility") != null && rdr.GetAttribute("Visibility") == "Collapsed")
                                            {
                                                XElement.RemoveAllAttributes();
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    //END

                    _objphoto.DG_Photos_Layering = Xdocument.InnerXml.ToString();
                }
                ///End new code...Vinod
                ///
                ImageLoader(_objphoto);
            }
        }


        public void SetTextEnglishGuest(string EnglishGuestName, int selectedPageno, int sceneID)
        {
            try
            {                
                Int32 selectedSceneVal = sceneID;
                var SceneDetails = (new SceneBusiness()).GetSceneDetailsSceneID(selectedSceneVal);
                var converter = new System.Windows.Media.BrushConverter();
                ThicknessConverter myThicknessConverter = new ThicknessConverter();
                List<StoryInfo> storyinfo = new List<StoryInfo>();
                storyinfo = (new StoryBookBusiness()).GetStoryBookDetails();
                TextBox txtChinese = new TextBox();
                TextBox txtEnglish = new TextBox();
                txtChinese.ContextMenu = dragCanvas.ContextMenu;

                StoryInfo si = storyinfo.Where(x => x.DG_PageNo == Convert.ToString(selectedPageno)).FirstOrDefault();
                if (si != null)
                {
                    string englishName = si.DG_Text_English;
                    if (EnglishGuestName != "")
                    {
                        if (englishName.Contains("******"))
                        {
                            englishName = englishName.Replace("******", EnglishGuestName);
                        }

                        ///Added logic for guest name
                        if (selectedEnglishText != "" && selectedEnglishText != null)
                        {   
                            if (englishName.Contains("******"))
                            {
                                englishName = englishName.Replace("******", selectedEnglishText);
                            }
                        }
                        ///
                        txtEnglish.Text = englishName;                       
                    }
                    else
                    {
                        txtEnglish.Text = si.DG_Text_English;
                    }

                   
                    txtEnglish.Foreground = (Brush)converter.ConvertFromString(si.DG_FontColor_English);
                    txtEnglish.FontSize = Convert.ToDouble(si.DG_FontSize_English);
                    txtEnglish.Margin = (Thickness)myThicknessConverter.ConvertFromString(si.DG_CoOrdinate_EnglishText);
                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(si.DG_FontEnglish);
                    txtEnglish.FontFamily = fw;
                }

                txtEnglish.Background = new SolidColorBrush(Colors.Transparent);
                txtEnglish.FontWeight = FontWeights.Bold;
                txtEnglish.FontStyle = FontStyles.Italic;
                txtEnglish.Uid = "txtEnglishblock";
                txtEnglish.Tag = "txtEnglishblock";
                txtEnglish.Name = "txtEnglishblock";
                txtEnglish.Style = (Style)FindResource("SearchIDTB");
                txtEnglish.LayoutTransform = new ScaleTransform(1.5, 1.5);

                FrameworkElement fe = dragCanvas.Children.Cast<FrameworkElement>()
                                               .Where(x => x.Tag != null &&
                                                      x.Tag.ToString() == "txtEnglishblock")
                                               .FirstOrDefault();

                if (fe != null)
                {
                    dragCanvas.Children.Remove(fe);
                    dragCanvas.Children.Add(txtEnglish);
                }
                else
                {
                    dragCanvas.Children.Add(txtEnglish);
                }
                Canvas.SetLeft(txtEnglish, 10);
                Canvas.SetTop(txtEnglish, 20);
                Canvas.SetZIndex(txtEnglish, 400);

                graphicsTextBoxCount++;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Error in SetTextEnglishGuest() Line 337");
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        public void SetTextChineseGuest(String ChineseGuestName, int selectedPageno, int sceneID)
        {
            try
            {
                if (selectedChineseText != "" && selectedChineseText != null)
                {
                    ChineseGuestName = selectedChineseText;
                }

                var SceneDetails = (new SceneBusiness()).GetSceneDetailsSceneID(sceneID);
                var converter = new System.Windows.Media.BrushConverter();
                ThicknessConverter myThicknessConverter = new ThicknessConverter();
                List<StoryInfo> storyinfo = new List<StoryInfo>();
                storyinfo = (new StoryBookBusiness()).GetStoryBookDetails();
                TextBox txtChinese = new TextBox();
                txtChinese.ContextMenu = dragCanvas.ContextMenu;

                StoryInfo si = storyinfo.Where(x => x.DG_PageNo == Convert.ToString(selectedPageno)).FirstOrDefault();
                if (si != null)
                {
                    string chineseName = si.DG_Text_Chinese;
                    if (ChineseGuestName != "")
                    {
                        if (chineseName.Contains("******"))
                        {
                            chineseName = chineseName.Replace("******", ChineseGuestName);
                        }
                        ///Added logic for guest name
                        if (selectedChineseText != "" && selectedChineseText != null)
                        {
                            if (chineseName.Contains("******"))
                            {
                                chineseName = chineseName.Replace("******", selectedChineseText);
                            }
                        }
                        ///
                        txtChinese.Text = chineseName;
                    }
                    else
                    {
                        txtChinese.Text = si.DG_Text_Chinese;
                    }

                    txtChinese.Foreground = (Brush)converter.ConvertFromString(si.DG_FontColor_Chinese);
                    txtChinese.FontSize = Convert.ToDouble(si.DG_FontSize_Chinese);
                    txtChinese.Margin = (Thickness)myThicknessConverter.ConvertFromString(si.DG_CoOrdinate_ChineseText);
                }

                RotateTransform rtm = new RotateTransform();

                txtChinese.Background = new SolidColorBrush(Colors.Transparent);
                //txtChinese.FontWeight = FontWeights.Bold;
                txtChinese.FontStyle = FontStyles.Italic;
                txtChinese.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString("YUANTI_1.TTF");
                txtChinese.Uid = "txtChineseblock";
                txtChinese.Tag = "txtChineseblock";
                txtChinese.BorderBrush = null;
                txtChinese.Style = (Style)FindResource("SearchIDTB");
                txtChinese.LayoutTransform = new ScaleTransform(1.7, 1.7);

                FrameworkElement fe = dragCanvas.Children.Cast<FrameworkElement>()
                                              .Where(x => x.Tag != null &&
                                                     x.Tag.ToString() == "txtChineseblock")
                                              .FirstOrDefault();

                if (fe != null)
                {
                    dragCanvas.Children.Remove(fe);
                    dragCanvas.Children.Add(txtChinese);
                }
                else
                {
                    dragCanvas.Children.Add(txtChinese);
                }

                Canvas.SetLeft(txtChinese, 10);
                Canvas.SetTop(txtChinese, 20);
                Canvas.SetZIndex(txtChinese, 400);
                graphicsTextBoxCount++;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Error in SetTextChineseGuest() Line 410");
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        public void ResetForm_Old()
        {
            translateTransform = new TranslateTransform();
            rotateTransform = new RotateTransform();
            transformGroup = new TransformGroup();
            imageNotFound = false;
            //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
            PhotoBusiness photoBusiness = new PhotoBusiness();
            ErrorHandler.ErrorHandler.LogFileWrite("Load Photo " + PhotoId);
            if (PhotoId > 0)
            {

                PhotoInfo _objphoto = photoBusiness.GetPhotoDetailsbyPhotoId(PhotoId);
                ErrorHandler.ErrorHandler.LogFileWrite(PhotoId + " Loaded successfully");
                ImageEffect = _objphoto.DG_Photos_Effects;
                ImageLoader(_objphoto);
            }
        }
        /// <summary>
        /// Below function commented by Latika for production issue i.e. 4/6 image cutting while printing Latika.ruke@digiphotoglobal.com
        /// </summary>
        /// <param name="_objphoto"></param>
        //public RenderTargetBitmap jCaptureScreen()
        //{
        //    this.InvalidateVisual();
        //    BitmapImage bi = mainImage.Source as BitmapImage;
        //    double dpiX = bi.DpiX;
        //    double dpiY = bi.DpiY;

        //    RenderTargetBitmap renderBitmap = null;
        //    RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
        //    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
        //    try
        //    {
        //        if (_ZoomFactor > 1.4)
        //        {
        //            Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
        //            renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
        //                dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
        //            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
        //            //RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
        //            forWdht.SnapsToDevicePixels = true;
        //            forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
        //            forWdht.Measure(size);
        //            forWdht.Arrange(new Rect(size));
        //            renderBitmap.Render(forWdht);
        //            forWdht.RenderTransform = null;
        //        }
        //        else
        //        {

        //            Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
        //            renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0 * 1.5),
        //                (int)(size.Height * dpiY / 96.0 * 1.5), dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
        //            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
        //            //RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
        //            forWdht.SnapsToDevicePixels = true;
        //            forWdht.RenderTransform = new ScaleTransform(1.5, 1.5, 0.5, 0.5);
        //            forWdht.Measure(size);
        //            forWdht.Arrange(new Rect(size));
        //            renderBitmap.Render(forWdht);
        //            forWdht.RenderTransform = null;
        //        }
        //        return renderBitmap;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //        Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
        //        DrawingVisual dv = new DrawingVisual();
        //        renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
        //                                                      (int)(bounds.Height * dpiY / 96.0),
        //                                                      dpiX,
        //                                                      dpiY,
        //                                                      PixelFormats.Default);


        //        using (DrawingContext ctx = dv.RenderOpen())
        //        {
        //            VisualBrush vb = new VisualBrush(forWdht);
        //            ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
        //        }

        //License Product  Vishal 13-04-2018
        TextBlock GetLicenseProductTextBlock(string text, FontFamily fontFamily, double fontSize, Brush brush = null)
        {
            TextBlock txtBlock = new TextBlock() { Text = text };
            txtBlock.FontFamily = fontFamily;
            txtBlock.FontSize = fontSize;
            txtBlock.FontWeight = FontWeights.Bold;
            if (brush != null)
                txtBlock.Foreground = brush;
            return (txtBlock);
        }

        //License Product  Vishal 13-04-2018
        void SetPositionInCanvas(UIElement control, int left, int top, int zIndex = 6)
        {
            dragCanvas.Children.Add(control);
            Canvas.SetZIndex(control, zIndex);
            Canvas.SetLeft(control, left);
            Canvas.SetTop(control, top);
        }

        //License Product  Vishal 13-04-2018
        private void AddLicenseDetailsToCanvas(int photoId)
        {
            string textFont = "Arial Bold", licNoTextFont = "Arial Black";
            int fontSize = 30, licNoFontSize = 40;
            int addToLeft = -30, addToTop = -30;

            LicenseProductBusiness lpb = new LicenseProductBusiness();
            LicenseUserDetails lud = lpb.GetLicenseDetails(photoId);
            if (lud != null)
            {
                TextBlock txtName = GetLicenseProductTextBlock(lud.Name, new FontFamily(textFont), fontSize);
                SetPositionInCanvas(txtName, 569 + addToLeft, 190 - 18);
                TextBlock txtAge = GetLicenseProductTextBlock(lud.Age.ToString(), new FontFamily(textFont), fontSize);
                SetPositionInCanvas(txtAge, 569 + addToLeft, 263 - 32);
                TextBlock txtIssueDate = GetLicenseProductTextBlock(lud.IssueDate.ToString("dd-MM-yyyy"), new FontFamily(textFont), fontSize);
                SetPositionInCanvas(txtIssueDate, 569 + addToLeft, 336 - 45);
                TextBlock txtExpireDate = GetLicenseProductTextBlock(lud.ExpiryDate.ToString("dd-MM-yyyy"), new FontFamily(textFont), fontSize);
                SetPositionInCanvas(txtExpireDate, 569 + addToLeft, 402 - 50);
                TextBlock txtLicenseNo = GetLicenseProductTextBlock(LicenseProductBusiness.GetLiceseNumberFromPrefixSufix(lud.LicensePrefix, lud.LicenseSufix), new FontFamily(licNoTextFont), licNoFontSize, Brushes.Red);
                SetPositionInCanvas(txtLicenseNo, 630 + addToLeft, 562 - 66);
            }
        }

        public RenderTargetBitmap jCaptureScreen()
        {
            this.InvalidateVisual();
            BitmapImage bi = mainImage.Source as BitmapImage;
            double dpiX = bi.DpiX;
            double dpiY = bi.DpiY;

            RenderTargetBitmap renderBitmap = null;
            RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
            try
            {
                if (_ZoomFactor > 1.4)
                {
                    Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
                        dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    //RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    forWdht.RenderTransform = null;
                }
                else
                {
                    Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);

                    if (PrintSizeID == 30) //4*6 Size print cut setting (IMG 09-Nove-2017) 
                    {

                        renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0 * 1.49),
                      (int)(size.Height * dpiY / 96.0 * 1.46), dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1.47, 1.47, 0.5, 0.5);
                    }
                    else
                    {

                        renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0 * 1.5),
                            (int)(size.Height * dpiY / 96.0 * 1.5), dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1.5, 1.5, 0.5, 0.5);
                    }
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    forWdht.RenderTransform = null;

                }
                return renderBitmap;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                DrawingVisual dv = new DrawingVisual();
                renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                              (int)(bounds.Height * dpiY / 96.0),
                                                              dpiX,
                                                              dpiY,
                                                              PixelFormats.Default);


                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(forWdht);
                    ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                }

                renderBitmap.Render(dv);
                return renderBitmap;
            }
        }

        /// <summary>
        /// ImageLoader Method updated to catch the gumball effects chaged from Digiphoto.exe
        /// </summary>
        /// <param name="_objphoto"></param>
        public void ImageLoader(PhotoInfo _objphoto)
        {
            double height = 0;
            double width = 0;
            grdZoomCanvas.UpdateLayout();
            GrdSize.UpdateLayout();
            Clear();
            attributeWidth = 10;
            _centerX = string.Empty;
            _centerY = string.Empty;
            FlipMode = 0;
            FlipModeY = 0;
            bool redeye = false;
            bool crop = false;
            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            forWdht.RenderTransform = new RotateTransform();

            try
            {
                BitmapImage _tempimage = new BitmapImage();

                if (_objphoto.DG_Photos_IsRedEye == null)
                    redeye = false;
                else
                    redeye = (bool)_objphoto.DG_Photos_IsRedEye;

                if (_objphoto.DG_Photos_IsCroped == null)
                    crop = false;
                else
                    crop = (bool)_objphoto.DG_Photos_IsCroped;

                if (_objphoto.DG_Photos_IsGreen == null)
                    isGreenImage = false;
                else
                    isGreenImage = (bool)_objphoto.DG_Photos_IsGreen;
                ConfigBusiness configBusiness = new ConfigBusiness();
                FolderStructureInfo fInfo = configBusiness.GetFolderStructureInfo(_objphoto.DG_SubStoreId.Value);

                if (IsgumBallActive && File.Exists(editedImagepath))
                {
                    if (!isGreenImage)
                    {
                        //using (FileStream fileStream = File.OpenRead(editedImagepath))
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();

                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            height = bi.Height;
                            width = bi.Width;
                        }
                    }
                    else
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName.Replace("jpg", "png"))))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            fileStream.Close();

                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                            bi.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            height = bi.Height;
                            width = bi.Width;
                        }
                    }
                }
                else if (crop && !isGreenImage)
                {
                    //using (FileStream fileStream = File.OpenRead(LoginUser.DigiFolderCropedPath + _objDbLayer.GetFileNameByPhotoID(Convert.ToString(PhotoId))))
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.CroppedPath, _objphoto.DG_Photos_FileName)))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }
                else if (isGreenImage)
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName.Replace("jpg", "png"))))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }
                else
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName)))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }

                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                if (ImageEffect != null && ImageEffect != string.Empty)
                {
                    ///Below commented code is original old code and Added below if else condition to handle ImageEffect Dated on..3Apr2018 //Vinod
                    ///if (!IsgumBallActive || !File.Exists(editedImagepath))
                    /// LoadXml(ImageEffect);

                    if (!IsgumBallActive || !File.Exists(editedImagepath))
                    {
                        LoadXml(ImageEffect);
                    }
                    else if (IsgumBallActive || File.Exists(editedImagepath))
                    {
                        LoadXml(ImageEffect);
                    }

                }

                dragCanvas.AllowDragOutOfView = true;

                GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                if (IsgumBallActive && File.Exists(editedImagepath))
                    Zomout(true);
                LoadFromDB(_objphoto);
                if (!IsgumBallActive || !File.Exists(editedImagepath))
                    Zomout(true);

                ErrorHandler.ErrorHandler.LogFileWrite("Islicence product");

                //License Product  Vishal 13-04-2018
                if (_isLicenseProduct)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Islicence product :" + _isLicenseProduct);

                    AddLicenseDetailsToCanvas(_objphoto.DG_Photos_pkey);


                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName)))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.CacheOption = BitmapCacheOption.None;
                        bi.StreamSource = ms;
                        bi.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                        bi.Rotation = Rotation.Rotate0;
                        bi.DecodePixelWidth = (int)LicenseProductBusiness.PrintImageWidthInDIP;
                        bi.DecodePixelHeight = (int)LicenseProductBusiness.PrintImageHeightInDIP;
                        bi.EndInit();

                        //mainImage.Source = bi;
                        //widthimg.Source = bi;
                        //height = bi.Height;
                        //width = bi.Width;
                    }
                    forWdht.Height = widthimg.Source.Height;
                    forWdht.Width = widthimg.Source.Width;
                    forWdht.VerticalAlignment = VerticalAlignment.Center;
                    ErrorHandler.ErrorHandler.LogFileWrite("forWdht.Height & forWdht.Width  :" + Convert.ToString(forWdht.Height) + " & " + Convert.ToString(forWdht.Width));
                    ErrorHandler.ErrorHandler.LogFileWrite("PrintImageHeightInDIP & PrintImageWidthInDIP  :" + Convert.ToString(LicenseProductBusiness.PrintImageHeightInDIP) + " & " + Convert.ToString(LicenseProductBusiness.PrintImageWidthInDIP));
                }

                int Sharpenfactor = 3;

                if (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"] != null)
                {
                    Sharpenfactor = (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"]).ToInt32();
                }

                // Apply Default Sharpen
                if (Sharpenfactor > 0)
                {
                    currentsharpen = currentsharpen + (0.025 * Sharpenfactor);
                    _sharpeff.Strength = currentsharpen;
                    _sharpeff.PixelWidth = 0.0015;
                    _sharpeff.PixelHeight = 0.0015;
                    GrdSharpen.Effect = _sharpeff;
                }
                forWdht.InvalidateArrange();
                forWdht.UpdateLayout();
                currentsharpen = 0;
                dragCanvas.AllowDragging = false;
                //dragCanvas.IsEnabled = false;

            }
            catch (Exception ex)
            {
                imageNotFound = true;
                ErrorHandler.ErrorHandler.LogError(ex);
                ErrorHandler.ErrorHandler.LogError(new Exception(_objphoto.DG_Photos_FileName + " not found in the hot folder."));
            }
            finally
            {

                ///Added  by Vinod Salunke 1Apr2019
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000

                MemoryManagement.FlushMemory();

                //Force garbage collection.// Wait for all finalizers to complete before continuing.
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }
        }

        public void ImageLoaderBlankPage(PhotoInfo _objphoto, string themeName, string pageNumber)
        {
            double height = 0;
            double width = 0;
            grdZoomCanvas.UpdateLayout();
            GrdSize.UpdateLayout();
            Clear();
            attributeWidth = 10;
            _centerX = string.Empty;
            _centerY = string.Empty;
            FlipMode = 0;
            FlipModeY = 0;
            bool redeye = false;
            bool crop = false;
            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            forWdht.RenderTransform = new RotateTransform();

            try
            {
                BitmapImage _tempimage = new BitmapImage();

                if (_objphoto.DG_Photos_IsRedEye == null)
                    redeye = false;
                else
                    redeye = (bool)_objphoto.DG_Photos_IsRedEye;

                if (_objphoto.DG_Photos_IsCroped == null)
                    crop = false;
                else
                    crop = (bool)_objphoto.DG_Photos_IsCroped;

                if (_objphoto.DG_Photos_IsGreen == null)
                    isGreenImage = false;
                else
                    isGreenImage = (bool)_objphoto.DG_Photos_IsGreen;
                ConfigBusiness configBusiness = new ConfigBusiness();
                FolderStructureInfo fInfo = configBusiness.GetFolderStructureInfo(_objphoto.DG_SubStoreId.Value);

                //Assign blank page template photo
                string blankPagePath = System.IO.Path.Combine(fInfo.HotFolderPath, "Storybook\\Theme\\", themeName, pageNumber + ".jpg");
                using (FileStream fileStream = File.OpenRead(blankPagePath))
                {
                    BitmapImage bi = new BitmapImage();
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    fileStream.Close();

                    fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                    bi.BeginInit();
                    ms.Seek(0, SeekOrigin.Begin);
                    bi.StreamSource = ms;
                    bi.EndInit();
                    mainImage.Source = bi;
                    widthimg.Source = bi;
                    height = bi.Height;
                    width = bi.Width;
                }

                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                dragCanvas.AllowDragOutOfView = true;
                GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                int Sharpenfactor = 3;
                if (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"] != null)
                {
                    Sharpenfactor = (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"]).ToInt32();
                }

                // Apply Default Sharpen
                if (Sharpenfactor > 0)
                {
                    currentsharpen = currentsharpen + (0.025 * Sharpenfactor);
                    _sharpeff.Strength = currentsharpen;
                    _sharpeff.PixelWidth = 0.0015;
                    _sharpeff.PixelHeight = 0.0015;
                    GrdSharpen.Effect = _sharpeff;
                }
                forWdht.InvalidateArrange();
                forWdht.UpdateLayout();
                currentsharpen = 0;
                dragCanvas.AllowDragging = false;
                //LoadXamlGuestNames(_objphoto.DG_Photos_Layering);//Just to get guest name on photo//chinese and english text 
            }
            catch (Exception ex)
            {
                imageNotFound = true;
                ErrorHandler.ErrorHandler.LogError(ex);
                ErrorHandler.ErrorHandler.LogError(new Exception(_objphoto.DG_Photos_FileName + " not found in the hot folder."));
            }
            finally
            {
                int totMemoryAlloted = (int)GC.GetTotalMemory(false); //Get approax unmanaged memory allocated
                GC.AddMemoryPressure(totMemoryAlloted); //20000
                MemoryManagement.FlushMemory();
            }
        }

        public void ImageLoader_Old(PhotoInfo _objphoto)
        {
            double height = 0;
            double width = 0;
            grdZoomCanvas.UpdateLayout();
            GrdSize.UpdateLayout();
            Clear();
            attributeWidth = 10;
            _centerX = string.Empty;
            _centerY = string.Empty;
            FlipMode = 0;
            FlipModeY = 0;
            bool redeye = false;
            bool crop = false;
            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            forWdht.RenderTransform = new RotateTransform();

            try
            {
                BitmapImage _tempimage = new BitmapImage();

                if (_objphoto.DG_Photos_IsRedEye == null)
                    redeye = false;
                else
                    redeye = (bool)_objphoto.DG_Photos_IsRedEye;

                if (_objphoto.DG_Photos_IsCroped == null)
                    crop = false;
                else
                    crop = (bool)_objphoto.DG_Photos_IsCroped;

                if (_objphoto.DG_Photos_IsGreen == null)
                    isGreenImage = false;
                else
                    isGreenImage = (bool)_objphoto.DG_Photos_IsGreen;
                ConfigBusiness configBusiness = new ConfigBusiness();
                FolderStructureInfo fInfo = configBusiness.GetFolderStructureInfo(_objphoto.DG_SubStoreId.Value);

                if (IsgumBallActive && File.Exists(editedImagepath))
                {
                    if (!isGreenImage)
                    {
                        //using (FileStream fileStream = File.OpenRead(editedImagepath))
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();

                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            height = bi.Height;
                            width = bi.Width;
                        }
                    }
                    else
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName.Replace("jpg", "png"))))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            fileStream.Close();

                            fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                            bi.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            height = bi.Height;
                            width = bi.Width;
                        }
                    }
                }
                else if (crop && !isGreenImage)
                {
                    //using (FileStream fileStream = File.OpenRead(LoginUser.DigiFolderCropedPath + _objDbLayer.GetFileNameByPhotoID(Convert.ToString(PhotoId))))
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.CroppedPath, _objphoto.DG_Photos_FileName)))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }
                else if (isGreenImage)
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName.Replace("jpg", "png"))))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }
                else
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(fInfo.HotFolderPath, _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphoto.DG_Photos_FileName)))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();

                        fileStream.Dispose();//Added by Vins to release the resources used by stream_2Apr2019

                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        widthimg.Source = bi;
                        height = bi.Height;
                        width = bi.Width;
                    }
                }

                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                if (ImageEffect != null && ImageEffect != string.Empty)
                {
                    if (!IsgumBallActive || !File.Exists(editedImagepath))
                        LoadXml(ImageEffect);
                }

                dragCanvas.AllowDragOutOfView = true;

                GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                if (IsgumBallActive && File.Exists(editedImagepath))
                    Zomout(true);
                LoadFromDB(_objphoto);
                if (!IsgumBallActive || !File.Exists(editedImagepath))
                    Zomout(true);

                int Sharpenfactor = 3;

                if (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"] != null)
                {
                    Sharpenfactor = (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"]).ToInt32();
                }

                // Apply Default Sharpen
                if (Sharpenfactor > 0)
                {
                    currentsharpen = currentsharpen + (0.025 * Sharpenfactor);
                    _sharpeff.Strength = currentsharpen;
                    _sharpeff.PixelWidth = 0.0015;
                    _sharpeff.PixelHeight = 0.0015;
                    GrdSharpen.Effect = _sharpeff;
                }
                forWdht.InvalidateArrange();
                forWdht.UpdateLayout();
                currentsharpen = 0;
                dragCanvas.AllowDragging = false;
                //dragCanvas.IsEnabled = false;

            }
            catch (Exception ex)
            {
                imageNotFound = true;
                ErrorHandler.ErrorHandler.LogError(ex);
                ErrorHandler.ErrorHandler.LogError(new Exception(_objphoto.DG_Photos_FileName + " not found in the hot folder."));
            }
            finally
            {

            }
        }

        ColorKeyAlphaEffect _greenscreendefault3 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefault4 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection1 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection2 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection3 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection4 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection5 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection6 = new ColorKeyAlphaEffect();
        ColorKeyAlphaEffect _greenscreendefaultCorrection7 = new ColorKeyAlphaEffect();
        public void LoadXml(string ImageXml)
        {
            try
            {
                System.Windows.Media.Color color;
                bool checkluminiosity = false;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                bool checkDigimagic = false;
                Xdocument.LoadXml(ImageXml);
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    var attributevalue = ((System.Xml.XmlAttribute)xn).Value.ToString();
                    if (string.IsNullOrEmpty(attributevalue))
                        continue;

                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "brightness":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                bright = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value.ToString()));
                                _brighteff.Brightness = bright;
                                _brighteff.Contrast = 1;
                                GrdBrightness.Effect = _brighteff;
                                checkluminiosity = true;
                            }
                            else
                            {
                                GrdBrightness.Effect = null;
                            }
                            break;

                        case "firstredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffect.Radius = .0105;
                                redEyeEffect.RedeyeTrue = 1;
                                GrdRedEyeFirst.Effect = redEyeEffect;
                                redEyeEffect.Center = new Point(.5, .5);
                            }
                            else
                            {
                                GrdRedEyeFirst.Effect = null;
                            }
                            break;
                        case "secondredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectSecond.Radius = .0105;
                                redEyeEffectSecond.RedeyeTrue = 1;
                                GrdRedEyeSecond.Effect = redEyeEffectSecond;
                                redEyeEffectSecond.Center = new Point(.5, .5);
                            }
                            else
                            {
                                GrdRedEyeSecond.Effect = null;
                            }
                            break;
                        case "firstradius":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Radius = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "aspectratiofirstredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.AspectRatio = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "aspectratiosecondredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.AspectRatio = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                                redEyeEffectMultiple.AspectRatio = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                                redEyeEffectMultiple1.AspectRatio = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "secondradius":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Radius = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "firstcenterx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Center = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "firstcentery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Center = new Point(redEyeEffect.Center.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "secondcenterx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Center = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "secondcentery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Center = new Point(redEyeEffectSecond.Center.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "multipleredeye1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius = .0105;
                                redEyeEffectMultiple.RedeyeTrue = 1;
                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center = new Point(.5, .5);
                            }
                            else
                            {
                                GrdRedEyeMultiple.Effect = null;
                            }
                            break;
                        case "multipleradius1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }

                            break;
                        case "multiplecenterx1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "multiplecentery1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center = new Point(redEyeEffectMultiple.Center.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "multipleredeye2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius1 = .0105;
                                redEyeEffectMultiple.RedeyeTrue1 = 1;
                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center1 = new Point(.5, .5);
                            }
                            break;
                        case "multipleradius2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius1 = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "multiplecenterx2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center1 = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "multiplecentery2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center1 = new Point(redEyeEffectMultiple.Center1.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "multipleredeye3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius2 = .0105;
                                redEyeEffectMultiple.RedeyeTrue2 = 1;
                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center2 = new Point(.5, .5);
                            }
                            break;
                        case "multipleradius3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius2 = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "multiplecenterx3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center2 = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "multiplecentery3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center2 = new Point(redEyeEffectMultiple.Center2.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "multipleredeye4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius = .0105;
                                redEyeEffectMultiple1.RedeyeTrue = 1;
                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center = new Point(.5, .5);
                            }
                            else
                            {
                                GrdRedEyeMultiple1.Effect = null;
                            }
                            break;
                        case "multipleradius4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "multiplecenterx4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "multiplecentery4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center = new Point(redEyeEffectMultiple1.Center.X, (((System.Xml.XmlAttribute)xn).Value).ToDouble());
                            }
                            break;
                        case "multipleredeye5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius1 = .0105;
                                redEyeEffectMultiple1.RedeyeTrue1 = 1;
                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center1 = new Point(.5, .5);
                            }
                            break;
                        case "multipleradius5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius1 = (((System.Xml.XmlAttribute)xn).Value).ToDouble();
                            }
                            break;
                        case "multiplecenterx5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center1 = new Point((((System.Xml.XmlAttribute)xn).Value).ToDouble(), .5);
                            }
                            break;
                        case "multiplecentery5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center1 = new Point(redEyeEffectMultiple1.Center1.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;
                        case "multipleredeye6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius2 = .0105;
                                redEyeEffectMultiple1.RedeyeTrue2 = 1;
                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center2 = new Point(.5, .5);
                            }
                            break;
                        case "multipleradius6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius2 = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }
                            break;
                        case "multiplecenterx6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center2 = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center2 = new Point(redEyeEffectMultiple1.Center2.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;
                        case "contrast":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "1")
                            {
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() == "##")
                                {
                                    cont = 1;
                                }
                                else
                                {
                                    cont = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                }

                                _conteff.Brightness = bright;
                                _conteff.Contrast = cont;
                                GrdBrightness.Effect = _conteff;
                                checkluminiosity = true;
                            }
                            break;
                        case "rotate":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                rotateangle = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                                Rotate(rotateangle);
                            }
                            break;
                        case "flipmode":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                FlipMode = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                FlipMode = 0;
                            }
                            break;
                        case "flipmodey":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                FlipModeY = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                FlipModeY = 0;
                            }
                            break;
                        case "_centerx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerX = string.Empty;
                            }
                            break;
                        case "_centery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerY = string.Empty;
                            }
                            break;
                        case "colourvalue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                switch (((System.Xml.XmlAttribute)xn).Value.ToString())
                                {
                                    case "red":
                                        _colorfiltereff.FilterColor = Colors.Red;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "blue":
                                        _colorfiltereff.FilterColor = Colors.Blue;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "green":
                                        _colorfiltereff.FilterColor = Colors.Green;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "magenta":
                                        _colorfiltereff.FilterColor = Colors.Magenta;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "lime":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFFF00");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "orange":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFA81D");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "yellow":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFFF96");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "skyblue":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF7CF5F5");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "lightred":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFCACB");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                }
                            }
                            else
                            {
                                Grdcolorfilter.Effect = null;
                            }
                            break;

                    }
                }
                bool coloreffect = false;
                foreach (var xn in (Xdocument.ChildNodes[0]).FirstChild.Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "sharpen":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                sharpen = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                currentsharpen = sharpen;
                                GrdSharpen.Effect = _sharpeff;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdSharpen.Effect = null;
                            }
                            break;

                        case "greyscale":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                GreyScaleEffect _greyscaleeff = new GreyScaleEffect();
                                _greyscaleeff.Desaturation = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _greyscaleeff.Toned = 0;
                                GrdGreyScale.Effect = _greyscaleeff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdGreyScale.Effect = null;
                            }
                            break;
                        case "digimagic":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _brighteff.Brightness = bright;
                                _brighteff.Contrast = cont;
                                GrdBrightness.Effect = _brighteff;
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                GrdSharpen.Effect = _sharpeff;
                                checkDigimagic = true;
                            }
                            else
                            {
                                if (!checkluminiosity)
                                {
                                    GrdBrightness.Effect = null;
                                }
                            }
                            break;
                        case "sepia":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _colorfiltereff.FilterColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFE6B34D");
                                GrdSepia.Effect = _colorfiltereff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSepia.Effect = null;
                            }
                            break;

                        case "defog":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _brighteff.Brightness = -0.10;
                                _brighteff.Contrast = 1.09;
                                GrdBrightness.Effect = _brighteff;
                                bright = -0.10;
                                cont = 1.09;
                                coloreffect = true;
                            }
                            else
                            {
                                if (!checkluminiosity && !checkDigimagic)
                                {
                                    GrdBrightness.Effect = null;
                                }
                            }
                            break;
                        case "underwater":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _under.FogColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF4F9CEF");
                                _under.Defog = .40;
                                _under.Contrastr = .67;
                                _under.Contrastg = 1;
                                _under.Contrastb = 1;
                                _under.Exposure = .77;
                                _under.Gamma = .91;
                                GrdUnderWater.Effect = _under;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdUnderWater.Effect = null;
                            }
                            break;
                        case "emboss":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                EmbossedEffect emboseff = new EmbossedEffect();
                                emboseff.Amount = 0.7;
                                emboseff.Width = 0.002;
                                GrdEmboss.Effect = emboseff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdEmboss.Effect = null;
                            }
                            break;
                        case "invert":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                InvertColorEffect _inverteff = new InvertColorEffect();
                                GrdInvert.Effect = _inverteff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdInvert.Effect = null;
                            }
                            break;
                        case "granite":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                SketchGraniteEffect sketcheff = new SketchGraniteEffect();
                                sketcheff.BrushSize = .005;
                                GrdSketchGranite.Effect = sketcheff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSketchGranite.Effect = null;
                            }
                            break;
                        case "hue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                hueshift = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _shifthueeff.HueShift = hueshift;
                                GrdHueShift.Effect = _shifthueeff;
                                currenthueshift = hueshift;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdHueShift.Effect = null;
                            }
                            break;
                        case "cartoon":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                Cartoonize carteff = new Cartoonize();
                                carteff.Width = 150;
                                carteff.Height = 150;
                                Grdcartoonize.Effect = carteff;
                                coloreffect = true;
                            }
                            else
                            {
                                Grdcartoonize.Effect = null;
                            }
                            break;

                    }
                }

                zoomTransform = new ScaleTransform();
                if (FlipMode != 0 || FlipModeY != 0)
                {
                    FlipLoad();
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }


        }
        void objCurrent_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(frm, (dragCanvas.ActualWidth - frm.ActualWidth) / 2); Canvas.SetTop(frm, (dragCanvas.ActualHeight - frm.ActualHeight) / 2);
        }
        private void LoadXaml(String inputXml)
        {
            if (string.IsNullOrEmpty(inputXml))
                return;
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(inputXml);

            double translateX = 0;
            double translateY = 0;
            transformGroup = new TransformGroup();
            rotateTransform = new RotateTransform();
            try
            {
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "border":
                            #region Border
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                BitmapImage img = new BitmapImage();
                                BitmapImage img1 = new BitmapImage();
                                img1.BeginInit();
                                img1.UriSource = new Uri(DigiPhoto.Common.LoginUser.DigiFolderFramePath + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString());
                                img1.EndInit();
                                img.BeginInit();
                                img.UriSource = new Uri(DigiPhoto.Common.LoginUser.DigiFolderFramePath + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString());
                                double ratio = 1;


                                #region Added by Ajay for the Panoramic Product on 17 Mar 2018
                                if (ProductTypeID == 120)//8 * 24 7200 x 2400 
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 7200;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 7200;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }

                                if (ProductTypeID == 121)//8 * 26 7800 x 2400 
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 7800;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 7800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                if (ProductTypeID == 122)//8 * 18 5400 x 2400 
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 5400;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 5400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                if (ProductTypeID == 123)//6 * 14 4200 x 1800 
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 4200;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 4200;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }

                                if (ProductTypeID == 125)//6 * 20 6000 x 1800 
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 6000;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 6000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                #endregion




                                else if (ProductTypeID == 1)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (ProductTypeID == 2 || ProductTypeID == 3)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 3000;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 3000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (ProductTypeID == 30 || ProductTypeID == 5 || ProductTypeID == 104)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 3000;
                                        img.DecodePixelWidth = 2000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2000;
                                        img.DecodePixelWidth = 3000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (ProductTypeID == 98)
                                {
                                    img.DecodePixelHeight = 900;
                                    img.DecodePixelWidth = 900;
                                    img.EndInit();
                                    ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else
                                {
                                    img.EndInit();
                                    if (img.Height > img.Width)
                                    {
                                        ratio = (double)(img.Width / img.Height);
                                    }
                                    else
                                    {
                                        ratio = (double)(img.Height / img.Width);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }

                                string[] tempFileName = img.ToString().Split('/');
                                OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                                objCurrent.Uid = "frame";
                                objCurrent.Source = img;
                                objCurrent.IsHitTestVisible = false;
                                objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                                objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);

                                frm.Width = forWdht.Width;
                                frm.Height = forWdht.Height;
                                forWdht.InvalidateArrange();
                                forWdht.InvalidateMeasure();
                                forWdht.InvalidateVisual();
                                frm.Children.Add(objCurrent);
                            }
                            #endregion
                            break;
                        #region  cases bg / canvasleft / canvastop / rotatetransform / scalecentrex / scalecentrey / zoomfactor
                        case "bg":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                BitmapImage objCurrent = new BitmapImage(new Uri(DigiPhoto.Common.LoginUser.DigiFolderBackGroundPath + "\\8x10" + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString()));
                                canbackground.Background = new ImageBrush { ImageSource = objCurrent };
                                LoadBackgroundInGrid(objCurrent);
                            }
                            break;
                        case "canvasleft":
                            Canvas.SetLeft(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            break;
                        case "canvastop":
                            Canvas.SetTop(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            break;
                        case "rotatetransform":
                            RotateTransform rtm = new RotateTransform();
                            rtm.CenterX = 0;
                            rtm.CenterY = 0;
                            rtm.Angle = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value));
                            GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                            if (rtm.Angle != -1 && rtm.Angle != 0)
                            {
                                GrdBrightness.RenderTransform = rtm;
                            }
                            break;
                        case "scalecentrex":
                            _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "scalecentrey":
                            _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "zoomfactor":
                            _ZoomFactor = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                            break;
                        //Licence Product  Vinod Salunke 13-04-2018
                        case "islicenseproduct":
                            _isLicenseProduct = Convert.ToBoolean(((System.Xml.XmlAttribute)xn).Value);
                            break;
                        case "chinesetext":
                            try
                            {
                                string ChineseText = Convert.ToString((((System.Xml.XmlAttribute)xn).Value));
                                if (!string.IsNullOrEmpty(selectedChineseText))
                                {
                                    ChineseText = selectedChineseText;
                                }
                                else
                                {
                                    selectedChineseText = ChineseText;
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            break;
                        case "englishtext":
                            try
                            {
                                string EnglishText = Convert.ToString((((System.Xml.XmlAttribute)xn).Value));
                                if (string.IsNullOrEmpty(EnglishText))
                                {
                                    EnglishText = selectedEnglishText;
                                }
                                else
                                {
                                    selectedEnglishText = EnglishText;
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            break;
                            #endregion
                    }
                }

                #region _centerX / translateX

                if (_centerX != "-1")
                {
                    ScaleTransform st = new ScaleTransform();
                    st.CenterX = Convert.ToDouble(_centerX);
                    st.CenterY = Convert.ToDouble(_centerY);

                    if (_ZoomFactor != -1)
                    {
                        st.ScaleX = _ZoomFactor;
                        st.ScaleY = _ZoomFactor;
                    }

                    if (FlipMode != 0 || FlipModeY != 0)
                    {
                        st.ScaleX = st.ScaleX;
                    }

                    zoomTransform = st;
                    transformGroup.Children.Add(st);
                }

                if (translateX != 0)
                {
                    TranslateTransform st = new TranslateTransform();
                    st.X = Convert.ToDouble(translateX);
                    st.Y = Convert.ToDouble(translateY);
                    translateTransform = st;
                    transformGroup.Children.Add(st);
                }

                if (GrdGreenScreenDefault3.RenderTransform == null)
                    GrdGreenScreenDefault3.RenderTransform = transformGroup;
                #endregion

                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                XmlNodeList xmlNL = Xdocument.GetElementsByTagName("Gumball");
                int count = 0;
                actualHgt = 0;
                actualWdth = 0;
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "graphics":
                                #region Graphics
                                Button btngrph = new Button();
                                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                                btngrph.Style = defaultStyle;
                                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                                BitmapImage img = new BitmapImage(new Uri(DigiPhoto.Common.LoginUser.DigiFolderGraphicsPath + @"\" + rdr.GetAttribute("source").ToString()));
                                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                imgctrl.Source = img;
                                btngrph.Width = 90;
                                btngrph.Height = 90;

                                btngrph.Content = imgctrl;
                                dragCanvas.Children.Add(btngrph);
                                var left = String.Format("{0:0.00}", (rdr.GetAttribute("left").ToString()).ToDouble());
                                var top = String.Format("{0:0.00}", (rdr.GetAttribute("top").ToString()).ToDouble());
                                Canvas.SetLeft(btngrph, left.ToDouble());
                                Canvas.SetTop(btngrph, top.ToDouble());

                                Double ZoomFactor = Convert.ToDouble(rdr.GetAttribute("zoomfactor").ToString());
                                btngrph.Width = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Height = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Tag = ZoomFactor.ToString();
                                TransformGroup tg = new TransformGroup();
                                RotateTransform rotation = new RotateTransform();
                                ScaleTransform scale = new ScaleTransform();
                                scale.CenterX = 0;
                                scale.CenterY = 0;
                                if (rdr.GetAttribute("scalex") != null)
                                {
                                    scale.ScaleX = Convert.ToDouble(rdr.GetAttribute("scalex").ToString());
                                }
                                if (rdr.GetAttribute("scaley") != null)
                                {
                                    scale.ScaleY = Convert.ToDouble(rdr.GetAttribute("scaley").ToString());
                                }

                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(scale);
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(rotation);

                                btngrph.RenderTransform = tg;

                                graphicsCount++;
                                #endregion
                                break;
                            case "textlogo":
                                #region TextLogo
                                TextBox txtLogo = new TextBox();
                                txtLogo.ContextMenu = dragCanvas.ContextMenu;
                                txtLogo.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("FontColor").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("FontColor").ToString());
                                    txtLogo.Foreground = brush;
                                }
                                else
                                {
                                    txtLogo.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("FontSize").ToString() != string.Empty)
                                {
                                    txtLogo.FontSize = (rdr.GetAttribute("FontSize").ToString()).ToDouble();
                                }
                                else
                                {
                                    txtLogo.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("FontFamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("FontFamily").ToString());
                                    txtLogo.FontFamily = fw;
                                }

                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.Uid = "txtLogoBlock";
                                txtLogo.FontWeight = FontWeights.Bold;
                                txtLogo.BorderBrush = null;
                                txtLogo.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtLogo);
                                Canvas.SetLeft(txtLogo, (rdr.GetAttribute("Left").ToString()).ToDouble());
                                Canvas.SetTop(txtLogo, (rdr.GetAttribute("Top").ToString()).ToDouble());
                                Canvas.SetZIndex(txtLogo, Convert.ToInt32(rdr.GetAttribute("ZIndex").ToString()));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = (rdr.GetAttribute("Angle").ToString()).ToDouble();
                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.RenderTransform = rotation;
                                txtLogo.Text = rdr.GetAttribute("Content").ToString();
                                graphicsTextBoxCount++;
                                #endregion TextLogo
                                break;
                            case "textbox":
                                #region Textbox
                                TextBox txtTest = new TextBox();
                                txtTest.ContextMenu = dragCanvas.ContextMenu;
                                txtTest.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtTest.Foreground = brush;
                                }
                                else
                                {
                                    txtTest.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtTest.FontSize = (rdr.GetAttribute("fontsize").ToString()).ToDouble();
                                }
                                else
                                {
                                    txtTest.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("_fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("_fontfamily").ToString());
                                    txtTest.FontFamily = fw;
                                }

                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.Uid = "txtblock";
                                txtTest.FontWeight = FontWeights.Bold;
                                txtTest.BorderBrush = null;
                                txtTest.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtTest);
                                Canvas.SetLeft(txtTest, (rdr.GetAttribute("left").ToString()).ToDouble());
                                Canvas.SetTop(txtTest, (rdr.GetAttribute("top").ToString()).ToDouble());
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = (rdr.GetAttribute("angle").ToString()).ToDouble();
                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.RenderTransform = rotation;

                                txtTest.Text = rdr.GetAttribute("text").ToString();

                                graphicsTextBoxCount++;
                                #endregion
                                break;
                            case "gumball":
                                #region Gumball
                                //StackPanel spGBall = new StackPanel();
                                //TextBox txtGBallPlayerScore = new TextBox();
                                //txtGBallPlayerScore.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;
                                //TextBox txtGumball = new TextBox();
                                //txtGumball.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;
                                //grdGumball.ContextMenu = dragCanvas.ContextMenu;
                                //if (rdr.GetAttribute("background").ToString() != string.Empty)
                                //{
                                //    var converter = new System.Windows.Media.BrushConverter();
                                //    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("background").ToString());
                                //    txtGumball.Background = brush;
                                //    txtGBallPlayerScore.Background = brush;
                                //}
                                //else
                                //{
                                //    txtGumball.Background = new SolidColorBrush(Colors.Transparent);
                                //    txtGBallPlayerScore.Background = new SolidColorBrush(Colors.Transparent);
                                //}

                                //if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                //{
                                //    var converter = new System.Windows.Media.BrushConverter();
                                //    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                //    txtGumball.Foreground = brush;
                                //    txtGBallPlayerScore.Foreground = brush;
                                //}
                                //else
                                //{
                                //    txtGumball.Foreground = new SolidColorBrush(Colors.DarkRed);
                                //    txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.DarkRed);
                                //}

                                //if (rdr.GetAttribute("zindex").ToString() != string.Empty)
                                //    Canvas.SetZIndex(spGBall, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                //else
                                //    Canvas.SetZIndex(spGBall, 4);

                                //if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                //{
                                //    txtGumball.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                //    txtGBallPlayerScore.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                //}
                                //else
                                //{
                                //    txtGumball.FontSize = 36.00;
                                //    txtGBallPlayerScore.FontSize = 36.00;
                                //}
                                //if (rdr.GetAttribute("fontfamily").ToString() != string.Empty)
                                //{
                                //    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("fontfamily").ToString());
                                //    txtGumball.FontFamily = fw;
                                //    txtGBallPlayerScore.FontFamily = fw;
                                //}

                                //if (rdr.GetAttribute("fontweight").ToString() != string.Empty)
                                //{
                                //    txtGumball.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                //    txtGBallPlayerScore.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                //}
                                //else
                                //{
                                //    txtGumball.FontWeight = FontWeights.Bold;
                                //    txtGBallPlayerScore.FontWeight = FontWeights.Bold;
                                //}

                                //if (rdr.GetAttribute("fontStyle").ToString() != string.Empty)
                                //{
                                //    txtGumball.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                //    txtGBallPlayerScore.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                //}
                                //else
                                //{
                                //    txtGumball.FontStyle = FontStyles.Normal;
                                //    txtGBallPlayerScore.FontStyle = FontStyles.Normal;
                                //}
                                ////Player Number
                                //string pn = string.Empty; string ps = string.Empty;
                                //string ft = rdr.GetAttribute("text").ToString();
                                //if (!String.IsNullOrEmpty(ft))
                                //{ 
                                //    pn = ft.Split('=')[0].Trim() + " Score";
                                //    //Player Score
                                //    ps = ft.Split('=')[1].Trim();
                                // }
                                //txtGumball.Uid = "txtgumball";
                                //txtGumball.Text = pn;
                                //txtGumball.Name = !String.IsNullOrEmpty(ft) ? ft.Split('=')[0].Trim() : "";
                                //txtGBallPlayerScore.Text = ps;
                                //grdGumball.Height = dragCanvas.ActualHeight;
                                //grdGumball.Width = dragCanvas.ActualWidth;
                                //string position = rdr.GetAttribute("position").ToString();
                                //string margin = rdr.GetAttribute("margin").ToString();
                                //spGBall.Uid = "spgumball";
                                //spGBall.Orientation = Orientation.Vertical;
                                //spGBall.Children.Add(txtGumball);
                                //spGBall.Children.Add(txtGBallPlayerScore);
                                ////dragCanvas.Children.Add(spGBall);
                                //Canvas.SetZIndex(grdGumball, 7);
                                ////Position Related Change
                                //if (count == xmlNL.Count)
                                //SetGumballPosition(position, margin, spGBall);
                                #endregion
                                break;
                        }
                        spGumball.InvalidateArrange();
                        spGumball.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        public void LoadXamlGuestNames(String inputXml)
        {
            if (string.IsNullOrEmpty(inputXml))
                return;
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(inputXml);

            try
            {
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "chinesetext":
                            try
                            {
                                string ChineseText = Convert.ToString((((System.Xml.XmlAttribute)xn).Value));
                                if (!string.IsNullOrEmpty(selectedChineseText))
                                {
                                    ChineseText = selectedChineseText;
                                }
                                else
                                {
                                    selectedChineseText = ChineseText;
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            break;
                        case "englishtext":
                            try
                            {
                                string EnglishText = Convert.ToString((((System.Xml.XmlAttribute)xn).Value));
                                if (string.IsNullOrEmpty(EnglishText))
                                {
                                    EnglishText = selectedEnglishText;
                                }
                                else
                                {
                                    selectedEnglishText = EnglishText;
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void LoadGumball(String inputXml)
        {
            if (string.IsNullOrEmpty(inputXml))
                return;
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(inputXml);
            XmlNodeList xmlNL = Xdocument.GetElementsByTagName("Gumball");
            int count = 0;
            actualHgt = 0;
            actualWdth = 0;
            try
            {
                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "gumball":
                                #region Gumball
                                //count++;
                                //txtGumBall.Visibility = Visibility.Visible;
                                //btnGumBall.Visibility = Visibility.Visible;
                                StackPanel spGBall = new StackPanel();
                                TextBox txtGBallPlayerScore = new TextBox();
                                txtGBallPlayerScore.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;
                                TextBox txtGumball1 = new TextBox();
                                // txtGumball1.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;-----Commented By Nilesh
                                grdGumball.ContextMenu = dragCanvas.ContextMenu;
                                if (rdr.GetAttribute("background").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("background").ToString());
                                    //txtGumball1.Background = brush;//--------Commented By Nilesh
                                    //txtGBallPlayerScore.Background = brush;-----Commented By Nilesh
                                    txtGBallPlayerScore.Background = new SolidColorBrush(Colors.Transparent);
                                }
                                else
                                {
                                    //  txtGumball1.Background = new SolidColorBrush(Colors.Transparent); ------Commented By Nilesh
                                    txtGBallPlayerScore.Background = new SolidColorBrush(Colors.Transparent);
                                }

                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtGumball1.Foreground = brush;
                                    //txtGBallPlayerScore.Foreground = brush;
                                    txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.DarkRed);////For Ani-mayhem
                                }
                                else
                                {
                                    txtGumball1.Foreground = new SolidColorBrush(Colors.DarkRed);
                                    txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("zindex").ToString() != string.Empty)
                                    Canvas.SetZIndex(spGBall, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                else
                                    Canvas.SetZIndex(spGBall, 4);

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtGumball1.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                    txtGBallPlayerScore.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString()) + 6;
                                    //txtGumball1.FontSize = 14;
                                    //txtGBallPlayerScore.FontSize = 20;
                                }
                                else
                                {
                                    txtGumball1.FontSize = 36.00;
                                    txtGBallPlayerScore.FontSize = 36.00;
                                }
                                if (rdr.GetAttribute("fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("fontfamily").ToString());
                                    txtGumball1.FontFamily = fw;
                                    txtGBallPlayerScore.FontFamily = fw;
                                }

                                if (rdr.GetAttribute("fontweight").ToString() != string.Empty)
                                {
                                    txtGumball1.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                    txtGBallPlayerScore.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                }
                                else
                                {
                                    txtGumball1.FontWeight = FontWeights.Bold;
                                    txtGBallPlayerScore.FontWeight = FontWeights.Bold;
                                }

                                if (rdr.GetAttribute("fontStyle").ToString() != string.Empty)
                                {
                                    txtGumball1.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                    txtGBallPlayerScore.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                }
                                else
                                {
                                    txtGumball1.FontStyle = FontStyles.Normal;
                                    txtGBallPlayerScore.FontStyle = FontStyles.Normal;
                                }
                                //Player Number
                                string pn = string.Empty; string ps = string.Empty;
                                string ft = rdr.GetAttribute("text").ToString();
                                if (!String.IsNullOrEmpty(ft))
                                {
                                    pn = ft.Split('=')[0].Trim() + " Score";
                                    //Player Score
                                    ps = ft.Split('=')[1].Trim();
                                }

                                //------Start--------Nilesh---Gummball---------
                                StackPanel spImg = new StackPanel();
                                Canvas objcan = new Canvas();
                                BitmapImage btm = null;
                                string bgimgPath = rdr.GetAttribute("BGImgPath");

                                if (pn == "Player1 Score")
                                {
                                    pn = "Player 1 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(1);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(1);
                                }
                                if (pn == "Player2 Score")
                                {
                                    pn = "Player 2 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(20);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(20);
                                }
                                if (pn == "Player3 Score")
                                {
                                    pn = "Player 3 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(2);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(2);
                                }
                                if (pn == "Player4 Score")
                                {
                                    pn = "Player 4 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(10);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(10);
                                }
                                if (pn == "Player5 Score")
                                {
                                    pn = "Player 5 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(-10);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(-10);
                                }
                                if (pn == "Player6 Score")
                                {
                                    pn = "Player 6 Score";
                                    if (bgimgPath != "")
                                    {
                                        btm = new BitmapImage(new Uri(bgimgPath));
                                    }
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(15);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(15);
                                }

                                string[] bgImgWidth = rdr.GetAttribute("BGImgHeightWidth").Split(':');
                                string[] bgImgPosition = rdr.GetAttribute("BGImgScorePosition").Split(':');

                                Image img1 = new Image();
                                img1.Source = btm;
                                img1.Width = Convert.ToDouble(bgImgWidth[0]);//170;
                                img1.Height = Convert.ToDouble(bgImgWidth[1]);//170;
                                img1.VerticalAlignment = VerticalAlignment.Top;
                                //spGBall.Children.Add(img1);
                                //------End--------Nilesh---Gummball---------

                                txtGumball1.Uid = "txtgumball";
                                pn = pn.Replace("Score", ""); //For Ani-Mayhem
                                //txtGumball1.Text = pn + Environment.NewLine + ps;
                                txtGumball1.Text = pn + Environment.NewLine;//By Vins for Ani-mayhem
                                txtGumball1.IsReadOnly = true;
                                txtGumball1.Background = Brushes.Transparent;
                                Thickness thick = new Thickness(0, 0, 0, 0);
                                txtGumball1.BorderThickness = thick;
                                spGBall.Name = !String.IsNullOrEmpty(ft) ? ft.Split('=')[0].Trim() : "";

                                txtGBallPlayerScore.Text = Environment.NewLine + ps + " ";//By Vins
                                txtGBallPlayerScore.TextAlignment = TextAlignment.Center;

                                txtGBallPlayerScore.IsReadOnly = true;
                                grdGumball.Height = dragCanvas.ActualHeight;
                                grdGumball.Width = dragCanvas.ActualWidth;
                                string position = rdr.GetAttribute("position").ToString();
                                string margin = rdr.GetAttribute("margin").ToString();
                                spGBall.Uid = "spgumball";
                                spGBall.Orientation = Orientation.Vertical;
                                //------Start--------Nilesh---Gummball---------


                                //Check the visibility for score and background image
                                if (rdr.GetAttribute("Visibility").ToString() != string.Empty)
                                {
                                    if (rdr.GetAttribute("Visibility").ToString().ToLower() == "visible")
                                    {
                                        txtGumball1.Visibility = Visibility.Visible;
                                        txtGBallPlayerScore.Visibility = Visibility.Visible;
                                    }
                                    else
                                    {
                                        txtGumball1.Visibility = Visibility.Collapsed;
                                        txtGBallPlayerScore.Visibility = Visibility.Collapsed;
                                    }
                                }
                                else
                                {
                                    //By Default
                                    txtGumball1.Visibility = Visibility.Visible;
                                    txtGBallPlayerScore.Visibility = Visibility.Visible;
                                }


                                objcan.Children.Add(img1);
                                objcan.Children.Add(txtGumball1);
                                Canvas.SetTop(txtGumball1, Convert.ToDouble(bgImgPosition[0]));
                                Canvas.SetLeft(txtGumball1, Convert.ToDouble(bgImgPosition[1]));
                                //objcan.Children.Add(txtGBallPlayerScore);  

                                //Start
                                //Added by Vins
                                objcan.Children.Add(txtGBallPlayerScore); //Added by Vins Ani-Mayhem
                                Canvas.SetTop(txtGBallPlayerScore, Convert.ToDouble(bgImgPosition[0]));
                                Canvas.SetLeft(txtGBallPlayerScore, Convert.ToDouble(bgImgPosition[1]));
                                //End

                                spGBall.Children.Add(objcan);
                                //------End--------Nilesh---Gummball---------

                                //spGBall.Children.Add(txtGumball1);--Commented--By-Nilesh------------
                                // spGBall.Children.Add(txtGBallPlayerScore);--Commented--By-Nilesh------------

                                dragCanvas.Children.Add(spGBall);
                                spGBall.UpdateLayout();
                                spGBall.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                                Size size = spGBall.DesiredSize; //MeasureString(txtGumball.Text, txtGumball);
                                //actualHgt = size.Height; //actualHgt + size.Height;
                                //if (size.Width > actualWdth)
                                //    actualWdth = size.Width;
                                Canvas.SetZIndex(grdGumball, 7);
                                //Position Related Change
                                SetGumballPosition(position, margin, spGBall);
                                //GumBallAppend.Append("<Gumball player = '" + rdr.GetAttribute("player").ToString() + "' zindex='" + rdr.GetAttribute("zindex").ToString() + "' text= '" + rdr.GetAttribute("text").ToString() + "' foreground='" + rdr.GetAttribute("foreground").ToString() + "' fontfamily='" + rdr.GetAttribute("fontfamily").ToString() + "' fontsize= '" + rdr.GetAttribute("fontsize").ToString() + "' fontweight= '" + rdr.GetAttribute("fontweight").ToString() + "' fontStyle='" + rdr.GetAttribute("fontStyle").ToString() + "' background='" + rdr.GetAttribute("background").ToString() + "' position='" + position + "' margin='" + margin + "'/>");
                                spGumball.RenderTransform = null;
                                DragCanvas.SetCanBeDragged(spGBall, false);
                                #endregion
                                break;
                        }
                        spGumball.InvalidateArrange();
                        spGumball.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        double actualHgt = 0;
        double actualWdth = 0;
        private void SetGumballPosition1(string position, string margin, StackPanel txtbox)
        {
            int extratopmargin;
            int extraleftmarginSpec4by6;
            SetExtraMargin(position, out extratopmargin, out extraleftmarginSpec4by6);
            double ratio = mainImage.Source.Height / mainImage.Source.Width;
            double gumgridHeight = (mainImage.Source.Width / 3.25) * 0.75;
            double gumgridWidth = mainImage.Source.Width / 3.25;
            if (position == "Top-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                if (ratio < 0.67 && ratio > 0.65)    // For 4x6 input file
                {
                    if (PrintProductType == 1)  // for 6x8 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)  // For 6x8 input file
                {
                    if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 30)  // for 4x6 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)   // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                    else if (PrintProductType == 30)  // for 4x6 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, 10);
            }
            else if (position == "Top-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                if (ratio < 0.67 && ratio > 0.65)    // For 4x6 input file
                {
                    if (PrintProductType == 1)  // for 6x8 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)  // For 6x8 input file
                {
                    if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 30)  // for 4x6 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)   // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                    else if (PrintProductType == 30)  // for 4x6 output
                    {
                        double topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                //Canvas.SetLeft(txtbox, 10);
                double left = (mainImage.Source.Width - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);
                Canvas.SetTop(txtbox, 10);
            }
            else if (position == "Top-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(0, Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                double rightExtraMargin = 0;
                double topExtraMargin = 0;
                if (ratio < 0.67 && ratio > 0.65)  // For 4x6 input file
                {
                    if (PrintProductType == 1)     // for 6x8 output
                    {
                        rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 2)   // for 8x10 output
                    {
                        rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Top = mg.Top;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)   // For 6x8 input file
                {
                    if (PrintProductType == 2)   // for 8x10 output
                    {
                        rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Top = mg.Top;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Right = mg.Right;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)  // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Right = mg.Right;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                    else if (PrintProductType == 30)    // for 4x6 output
                    {
                        topExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Right = mg.Right;
                        mg.Top = mg.Top + topExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                Canvas.SetTop(txtbox, 10);
                Canvas.SetLeft(txtbox, mainImage.Source.Width - actualWdth - mg.Right - 20);
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
            }
            else if (position == "Bottom-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                if (ratio < 0.67 && ratio > 0.65)    // For 4x6 input file
                {
                    if (PrintProductType == 1)    // for 6x8 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 2)    // for 8x10  output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)     // For 6x8 input file
                {
                    if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)    // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, mainImage.Source.Height - actualHgt - mg.Bottom - 10);

                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
            }
            else if (position == "Bottom-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                if (ratio < 0.67 && ratio > 0.65)    // For 4x6 input file
                {
                    if (PrintProductType == 1)    // for 6x8 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 2)    // for 8x10  output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)     // For 6x8 input file
                {
                    if (PrintProductType == 2)   // for 8x10 output
                    {
                        double leftExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Left = mg.Left + leftExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)    // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        double BottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Left = mg.Left;
                        mg.Bottom = mg.Bottom + BottomExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                //Canvas.SetLeft(txtbox, 10);
                double left = (mainImage.Source.Width - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);
                Canvas.SetTop(txtbox, mainImage.Source.Height - actualHgt - mg.Bottom - 10);

                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
            }
            else if (position == "Bottom-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                if (ratio < 0.67 && ratio > 0.65)   // for 4x6 input file
                {
                    if (PrintProductType == 1)    // for 6x8 output
                    {
                        double rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.75) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 2)     // for 8x10 output
                    {
                        double rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                }
                else if (ratio < 0.76 && ratio > 0.74)    // For 6x8 input file
                {
                    if (PrintProductType == 2)    // for 8x10 output
                    {
                        double rightExtraMargin = (mainImage.Source.Width - mainImage.Source.Height / 0.8) / 2;
                        mg.Right = mg.Right + rightExtraMargin;
                        mg.Bottom = mg.Bottom;
                    }
                    else if (PrintProductType == 30)    // for 4x6 output
                    {
                        double bottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Right = mg.Right;
                        mg.Bottom = mg.Bottom + bottomExtraMargin;
                    }
                }
                else if (ratio < 0.81 && ratio > 0.79)    // For 8x10 input file
                {
                    if (PrintProductType == 1)   // for 6x8 output
                    {
                        double bottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.75) / 2;
                        mg.Right = mg.Right;
                        mg.Bottom = mg.Bottom + bottomExtraMargin;
                    }
                    else if (PrintProductType == 30)   // for 4x6 output
                    {
                        double bottomExtraMargin = (mainImage.Source.Height - mainImage.Source.Width * 0.67) / 2;
                        mg.Right = mg.Right;
                        mg.Bottom = mg.Bottom + bottomExtraMargin;
                    }
                }
                txtbox.Margin = mg;
                Canvas.SetLeft(txtbox, mainImage.Source.Width - actualWdth - mg.Right - 10);
                Canvas.SetTop(txtbox, mainImage.Source.Height - actualHgt - mg.Bottom - 10);
            }
            else
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, mainImage.Source.Width - actualWdth);
                Canvas.SetTop(txtbox, mainImage.Source.Height - actualHgt);
            }
        }
        private void SetGumballPosition(string position, string margin, StackPanel txtbox)
        {
            if (position == "Top-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, 10);
            }
            else if (position == "Top-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();

                double left = (forWdht.ActualWidth - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);

                //double top = (forWdht.ActualHeight - txtbox.ActualHeight) / 2;
                //Canvas.SetTop(txtbox, top);

                Canvas.SetTop(txtbox, 10);

            }
            else if (position == "Top-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(0, Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetTop(txtbox, 10);
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - (txtbox.ActualWidth) - mg.Right - 10);

            }
            else if (position == "Bottom-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom - 10);
                //Canvas.SetLeft(grdGumball, 10);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
            else if (position == "Bottom-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                //Canvas.SetLeft(txtbox, 10);
                //Canvas.SetTop(txtbox, forWdht.Height - actualHgt - mg.Bottom - 10);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom - 10);
                double left = (forWdht.ActualWidth - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);
                //txtbox.Margin = mg;
                //txtbox.UpdateLayout();
                //Canvas.SetLeft(grdGumball, 10);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
                //txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                //txtbox.VerticalAlignment = VerticalAlignment.Bottom;
            }
            else if (position == "Bottom-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - txtbox.ActualWidth - mg.Right - 10);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom - 10);
                //Canvas.SetLeft(grdGumball, forWdht.ActualWidth - grdGumball.Width);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
            else
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - txtbox.ActualWidth);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight);
                //Canvas.SetLeft(grdGumball, forWdht.ActualWidth - grdGumball.Width);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
        }
        private Size MeasureString(string candidate, object textBx)
        {
            TextBox txt = (TextBox)textBx;
            var formattedText = new FormattedText(
                candidate,
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(txt.FontFamily, txt.FontStyle, txt.FontWeight, txt.FontStretch),
                txt.FontSize,
                txt.Foreground);

            return new Size(formattedText.Width, formattedText.Height);
        }

        private void SetExtraMargin(string position, out int extratopmargin, out int extraleftmarginSpec4by6)
        {
            extratopmargin = 0;
            extraleftmarginSpec4by6 = 0;
            if (!string.IsNullOrWhiteSpace(PrintEngine.specproductType))
            {
                if (PrintEngine.specproductType.Equals("2"))
                {
                    if (ProductTypeID.Equals(1) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = 35;
                    else if (ProductTypeID.Equals(1) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 45;
                    else if (ProductTypeID.Equals(2) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = 5;
                    else if (ProductTypeID.Equals(2) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 10;
                    else if (ProductTypeID.Equals(30) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = 70;
                    else if (ProductTypeID.Equals(30) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 10;
                }
                else if (PrintEngine.specproductType.Equals("1"))
                {
                    if (ProductTypeID.Equals(1) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = -10;
                    else if (ProductTypeID.Equals(1) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 45;
                    else if (ProductTypeID.Equals(2) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = -40;
                    else if (ProductTypeID.Equals(2) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 50;
                    else if (ProductTypeID.Equals(30) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = 30;
                    else if (ProductTypeID.Equals(30) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                        extratopmargin = 10;
                }
                else if (PrintEngine.specproductType.Equals("30"))
                {
                    if (ProductTypeID.Equals(1) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = -80;
                    else if (ProductTypeID.Equals(1) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                    {
                        extratopmargin = 45;
                        extraleftmarginSpec4by6 = 10;
                    }
                    else if (ProductTypeID.Equals(2) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = -110;
                    else if (ProductTypeID.Equals(2) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                    {
                        extratopmargin = 50;
                        extraleftmarginSpec4by6 = 10;
                    }
                    else if (ProductTypeID.Equals(30) && (position.Equals("Bottom-Left") || position.Equals("Bottom-Right")))
                        extratopmargin = -50;
                    else if (ProductTypeID.Equals(30) && (position.Equals("Top-Left") || position.Equals("Top-Right")))
                    {
                        extratopmargin = 10;
                        extraleftmarginSpec4by6 = 10;
                    }
                }
            }
        }

        private void LoadBackgroundInGrid(BitmapImage objCurrent)
        {
            if (frm.Children.Count == 0)
            {

                double ratio = 1;
                if (objCurrent.Height > objCurrent.Width)
                {
                    ratio = (double)(objCurrent.Width / objCurrent.Height);
                }
                else
                {
                    ratio = (double)(objCurrent.Height / objCurrent.Width);
                }

                if (forWdht.Height > forWdht.Width)
                {
                    forWdht.Width = forWdht.Height * ratio;
                }
                else
                {
                    forWdht.Height = forWdht.Width * ratio;
                }

                forWdht.InvalidateArrange();
                forWdht.InvalidateMeasure();
                forWdht.InvalidateVisual();
                Zomout(true);
            }
            objCurrent.Freeze();
        }

        int rotateangle = 0;
        private void Rotate(int angle)
        {

            GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            RotateTransform RoatateTrans = new RotateTransform();
            int previousvalue = rotateangle;
            if (angle != -1)
            {
                RoatateTrans.Angle = angle;
                GrdBrightness.RenderTransform = RoatateTrans;
                rotateangle = 360 - angle;
                if (rotateangle == 360)
                    rotateangle = 0;
            }
            else
            {
                try
                {
                    switch (rotateangle)
                    {
                        case 0:
                            {
                                RoatateTrans.Angle = 90;
                                rotateangle = 90;


                                //CurrentImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                //FlipModeY = 1;
                                break;
                            }
                        case 90:
                            {
                                RoatateTrans.Angle = 180;
                                rotateangle = 180;
                                break;
                            }
                        case 180:
                            {
                                RoatateTrans.Angle = 270;
                                rotateangle = 270;
                                break;
                            }
                        case 270:
                            {
                                RoatateTrans.Angle = 360;
                                rotateangle = 0;
                                //FlipModeY = 0;
                                break;
                            }

                    }
                    GrdBrightness.RenderTransform = RoatateTrans;

                    if (RoatateTrans.Angle / 90 == 1 || RoatateTrans.Angle / 90 == 3)
                    {
                        GrdBrightness.Margin = new Thickness(-(forWdht.Width - forWdht.Height) / 2, (forWdht.Width - forWdht.Height) / 2, 0, 0);

                        double w = forWdht.ActualWidth;
                        double h = forWdht.ActualHeight;

                        forWdht.Width = h;
                        forWdht.Height = w;

                        forWdht.RenderSize = new Size(h, w);

                        dragCanvas.RenderSize = new Size(h, w);
                        canbackground.RenderSize = new Size(h, w);


                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();
                    }
                    else
                    {
                        GrdBrightness.Margin = new Thickness(0, 0, 0, 0);

                        double w = forWdht.ActualWidth;
                        double h = forWdht.ActualHeight;

                        forWdht.Width = h;
                        forWdht.Height = w;

                        forWdht.RenderSize = new Size(h, w);

                        dragCanvas.RenderSize = new Size(h, w);
                        canbackground.RenderSize = new Size(h, w);


                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
                finally
                {
                }
            }
        }

        int graphicsTextBoxCount = 0;
        int graphicsCount = 0;
        private TransformGroup transformGroup;
        private TranslateTransform translateTransform;
        private ScaleTransform zoomTransform;
        private RotateTransform rotateTransform;
        private double _ZoomFactor = 1;
        private void RemoveAllGraphicsEffect()
        {
            try
            {
                canbackground.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                //Clean old Gumball Data
                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in spGumball.Children)
                {
                    if (ui is TextBox)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    spGumball.Children.Remove(ui);
                }
                Canvas.SetLeft(grdGumball, 0);
                Canvas.SetTop(grdGumball, 0);
                grdGumball.UpdateLayout();

                _ZoomFactor = 1;
                if (zoomTransform != null)
                {
                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    zoomTransform.ScaleX = _ZoomFactor;
                    zoomTransform.ScaleY = _ZoomFactor;
                    zoomTransform = new ScaleTransform();
                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform();
                    //transformGroup.Children.Add(zoomTransform);
                    //transformGroup.Children.Add(rotateTransform);
                    //GrdGreenScreenDefault3.RenderTransform = null;

                    //GrdBrightness.RenderTransform = transformGroup;
                    GrdBrightness.RenderTransform = null;

                    //mainImage.Source = new BitmapImage(new Uri(LoginUser.DigiFolderPath + PhotoName + ".jpg"));

                    // BitmapImage _tempimage = new BitmapImage();
                    //_tempimage = new BitmapImage(new Uri(LoginUser.DigiFolderPath + PhotoName + ".jpg"));
                    //_objMainImage = BitmapImage2Bitmap(_tempimage);
                    //_CurrentBitMap = new WriteableBitmap(_tempimage);
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void ReloadAllGraphicsEffect()
        {
            try
            {
                canbackground.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                if (FlipMode != 0 || FlipModeY != 0)
                {
                    _ZoomFactor = 1;

                    zoomTransform.CenterX = _centerX.ToDouble();
                    zoomTransform.CenterY = _centerY.ToDouble();

                    zoomTransform.ScaleX = 1;
                    zoomTransform.ScaleY = 1;

                    translateTransform = new TranslateTransform();
                    rotateTransform = new RotateTransform();
                    transformGroup = new TransformGroup();

                    GrdBrightness.RenderTransform = null;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    FlipLoad();
                }
                else
                {
                    _ZoomFactor = 1;

                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    zoomTransform.ScaleX = _ZoomFactor;
                    zoomTransform.ScaleY = _ZoomFactor;


                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform();
                    zoomTransform = new ScaleTransform();


                    //GrdBrightness.RenderTransform = transformGroup;
                    GrdBrightness.RenderTransform = null;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void RemoveAllShaderEffects()
        {
            try
            {
                GrdInvert.Effect = null;
                GrdSharpen.Effect = null;
                GrdSketchGranite.Effect = null;
                GrdEmboss.Effect = null;
                Grdcartoonize.Effect = null;
                GrdGreyScale.Effect = null;
                GrdHueShift.Effect = null;
                Grdcolorfilter.Effect = null;
                GrdBrightness.Effect = null;
                GrdSepia.Effect = null;
                GrdRedEyeFirst.Effect = null;
                GrdRedEyeSecond.Effect = null;
                GrdRedEyeMultiple.Effect = null;
                GrdRedEyeMultiple1.Effect = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void Clear()
        {
            RemoveAllShaderEffects();
            RemoveAllGraphicsEffect();
        }
        private void LoadFromDB(PhotoInfo _objPhotoDetails)
        {
            try
            {
                graphicsTextBoxCount = 0;
                graphicsCount = 0;

                if (_objPhotoDetails.DG_Photos_Layering != null && !_objPhotoDetails.DG_Photos_Layering.Equals("test", StringComparison.CurrentCultureIgnoreCase))
                {
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                    if (IsgumBallActive && File.Exists(editedImagepath) && _objPhotoDetails.IsGumRideShow)///changed by latika 
                    {
                        LoadXaml(_objPhotoDetails.DG_Photos_Layering);
                        LoadGumball(_objPhotoDetails.DG_Photos_Layering);
                    }
                    else
                        LoadXaml(_objPhotoDetails.DG_Photos_Layering);
                }
                else
                {
                    forWdht.Width = widthimg.Source.Width;
                    forWdht.Height = widthimg.Source.Height;
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                }
                if (!_objPhotoDetails.IsGumRideShow)
                {
                    grdGumball.Visibility = Visibility.Hidden;
                }
                else
                {
                    grdGumball.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        public void Zomout(bool orignal)
        {
            try
            {
                grdZoomCanvas.UpdateLayout();
                double currentwidth = widthimg.Source.Width;
                double currentheight = widthimg.Source.Height;

                double ratiowidth = currentwidth / 600;
                double ratioheight = currentheight / 600;
                ratiowidth = 100 / ratiowidth / 100;
                ratioheight = 100 / ratioheight / 100;

                if (frm.Children.Count == 1)
                {
                    currentwidth = forWdht.Width;
                    currentheight = forWdht.Height;
                    ratiowidth = currentwidth / 600;
                    ratioheight = currentheight / 600;
                    ratiowidth = 100 / ratiowidth / 100;
                    ratioheight = 100 / ratioheight / 100;
                }

                ScaleTransform zoomTransform = new ScaleTransform(); ;
                TransformGroup transformGroup = new TransformGroup();
                if (currentheight > currentwidth)
                {
                    zoomTransform.ScaleX = ratioheight - .01;
                    zoomTransform.ScaleY = ratioheight - .01;
                }

                if (currentheight < currentwidth)
                {
                    zoomTransform.ScaleX = ratiowidth - .01;
                    zoomTransform.ScaleY = ratiowidth - .01;
                }
                zoomTransform.CenterX = forWdht.ActualWidth / 2;
                zoomTransform.CenterY = forWdht.ActualHeight / 2;
                transformGroup.Children.Add(zoomTransform);
                if (orignal)
                {
                    MyInkCanvas.RenderTransform = null;

                    grdZoomCanvas.LayoutTransform = transformGroup;
                }
                else
                {

                    GrdGreenScreenDefault3.RenderTransform = null;
                    MyInkCanvas.RenderTransform = transformGroup;

                    MyInkCanvas.DefaultDrawingAttributes.StylusTipTransform = new Matrix(zoomTransform.ScaleX, 0, 0, zoomTransform.ScaleY, 0, 0);

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                DigiPhoto.MemoryManagement.FlushMemory();
            }
        }
        private void GetStoreConfigData()
        {

            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();
            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.DynamicBGFunctionalityActive:
                        DynamicBGFunctionalityActive = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                        break;
                }
            }
        }
    }
}