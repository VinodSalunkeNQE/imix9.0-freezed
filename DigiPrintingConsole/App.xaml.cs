﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;
using DigiPhoto;
using Hardcodet.Wpf.TaskbarNotification;
using DigiPrintingConsole.Properties;
using System.IO;
using System.Windows.Media.Animation;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Business;
using System.Web.Script.Serialization;
using System.Windows.Documents;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace DigiPrintingConsole
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : Application
    {
        #region Declaration
        private TaskbarIcon notifyIcon;
        public static int SubStoreId;
        #region ADDED bY AJAY
        private List jsonAsList = new List();
        #endregion
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {



            DataCacheFactory.Register();
            // MessageBox.Show("Test");
            // Get Reference to the current Process
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                              new FrameworkPropertyMetadata { DefaultValue = 5 });
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If there is more than one, then it is already running.
                MessageBox.Show("DigiPrintingConsole is already running.");
                Application.Current.Shutdown();
                return;

            }


            #region Add Keys Dynamically by Anis on August 17
            //READ THE CONFIG FILE.
            string ConfigPath = @"C:\Program Files (x86)\iMix";

            var directory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-DigiPrintConsole.json";
            string jrf = System.IO.File.ReadAllText(directory);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            jsonAsList = serializer.Deserialize<List>(jrf.ToString());
            var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS820LUTFilename") == false)
            {
                string DS820LUTFilename = Convert.ToString(jsonObj["DS820LUTFilename"]);
                config.AppSettings.Settings.Add("DS820LUTFilename", DS820LUTFilename.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS820CorrFilename") == false)
            {
                string DS820CorrFilename = Convert.ToString(jsonObj["DS820CorrFilename"]);
                config.AppSettings.Settings.Add("DS820CorrFilename", DS820CorrFilename.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS820ICCTblFilename20") == false)
            {
                string DS820ICCTblFilename20 = Convert.ToString(jsonObj["DS820ICCTblFilename20"]);
                config.AppSettings.Settings.Add("DS820ICCTblFilename20", DS820ICCTblFilename20.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS820ICCTblFilename21") == false)
            {
                string DS820ICCTblFilename21 = Convert.ToString(jsonObj["DS820ICCTblFilename21"]);
                config.AppSettings.Settings.Add("DS820ICCTblFilename21", DS820ICCTblFilename21.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS620ICCTblData_0010") == false)
            {
                string DS620ICCTblData_001010 = Convert.ToString(jsonObj["DS620ICCTblData_0010"]);
                config.AppSettings.Settings.Add("DS620ICCTblData_0010",DS620ICCTblData_001010.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS620LUTFilename") == false)
            {
                string DS620LUTFilename = Convert.ToString(jsonObj["DS620LUTFilename"]);
                config.AppSettings.Settings.Add("DS620LUTFilename", DS620LUTFilename.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DS620CorrFilename") == false)
            {
                string DS620CorrFilename = Convert.ToString(jsonObj["DS620CorrFilename"]);
                config.AppSettings.Settings.Add("DS620CorrFilename", DS620CorrFilename.Replace("-", "\\"));
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsPrintEnable") == false)
            {
                string IsPrintEnable = Convert.ToString(jsonObj["IsPrintEnable"]);
                config.AppSettings.Settings.Add("IsPrintEnable", IsPrintEnable);
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MaxMemoryAllowed") == false)
            {
                string MaxMemoryAllowed = Convert.ToString(jsonObj["MaxMemoryAllowed"]);
                config.AppSettings.Settings.Add("MaxMemoryAllowed", MaxMemoryAllowed);
            }

            //Save the changes in App.config file.
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");

            #endregion


            SetSubStoreId();
            RobotImageLoader.GetConfigData();
            notifyIcon = new TaskbarIcon();
            notifyIcon.Icon = DigiPrintingConsole.Properties.Resources.PrintConsole;
            notifyIcon.ToolTipText = "DigiPrinting";
            notifyIcon.Visibility = Visibility.Visible;
            notifyIcon.TrayPopup = new mypopup();
           
        }

        private static void SetSubStoreId()
        {
            try
            {
                string pathtosave = Environment.CurrentDirectory;
                //pathtosave = pathtosave.Replace("DigiPrintingConsole", "DigiPhoto");
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;

                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                        SubStoreId = (subID.Split(',')[0]).ToInt32();

                    }
                }
                else
                {
                    MessageBox.Show("Please select substore from Configuration Section in Imix for this machine.");
                    Application.Current.Shutdown();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            MessageBox.Show(e.Message);
        }
        void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ShowUnhandeledException(e);
        }
        void ShowUnhandeledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError:{0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",
            e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
            e.Exception.InnerException.Message : null));
            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}
