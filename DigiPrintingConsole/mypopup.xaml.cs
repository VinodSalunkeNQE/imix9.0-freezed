﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;
using DigiPhoto.IMIX.Business;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;

namespace DigiPrintingConsole
{
    /// <summary>
    /// Interaction logic for mypopup.xaml
    /// </summary>
    public partial class mypopup : UserControl
    {
        public mypopup()
        {

            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            string ret = svcPosinfoBusiness.ServiceStart(true);

            //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            // int ret = svcPosinfoBusiness.ServiceStart(false);

            if (!string.IsNullOrEmpty(ret))
            {
                MessageBox.Show("This application is already running on " + ret);
                Environment.Exit(0);
                // return;
            }
            else
            { 
            //here i will put the code to update the imixposdetail table

                SetClearPrintServer(false);
                
            }
          

            InitializeComponent();
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {
                PrintEngine objprintEngine = new PrintEngine();
                objprintEngine.Show();
                System.Windows.Threading.Dispatcher.Run();
            }));

            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();

           
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(true);



            SetClearPrintServer(true);

            //MessageBox.Show("hello");
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
               
                Application.Current.Shutdown();
                thisProc.Kill();
                
            }

        }
        public void SetClearPrintServer(bool isStop )
        {
            try
            {
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST                      
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemName = queryObj["Name"];
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                svcPosinfoBusiness.setPrintServer(mac, SystemName.ToString(), isStop);
            }
            catch (Exception ex)
            {

                
            }
        }

    }
}
