﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace DigiPrintingConsole.Shader
{
    public class ShiftHueEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(ShiftHueEffect), 0);
        public static readonly DependencyProperty HueShiftProperty = DependencyProperty.Register("HueShift", typeof(double), typeof(ShiftHueEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(0)));
        public ShiftHueEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri(@"/Shader/ShiftHue.ps", UriKind.Relative);
           
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(HueShiftProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>Hue shift</summary>
        public double HueShift
        {
            get
            {
                return ((double)(this.GetValue(HueShiftProperty)));
            }
            set
            {
                this.SetValue(HueShiftProperty, value);
            }
        }
    }
}
