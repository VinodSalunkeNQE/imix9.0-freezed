﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;


namespace DigiPrintingConsole.Shader
{

    public class MultiChannelContrastEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(MultiChannelContrastEffect), 0);
        public static readonly DependencyProperty BrightnessProperty = DependencyProperty.Register("Brightness", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty ContrastrProperty = DependencyProperty.Register("Contrastr", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(1)));
        public static readonly DependencyProperty ContrastgProperty = DependencyProperty.Register("Contrastg", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(2)));
        public static readonly DependencyProperty ContrastbProperty = DependencyProperty.Register("Contrastb", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(3)));
        public static readonly DependencyProperty DefogProperty = DependencyProperty.Register("Defog", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(4)));
        public static readonly DependencyProperty FogColorProperty = DependencyProperty.Register("FogColor", typeof(Color), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(Color.FromArgb(255, 255, 255, 255), PixelShaderConstantCallback(5)));
        public static readonly DependencyProperty ExposureProperty = DependencyProperty.Register("Exposure", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0.2D)), PixelShaderConstantCallback(6)));
        public static readonly DependencyProperty GammaProperty = DependencyProperty.Register("Gamma", typeof(double), typeof(MultiChannelContrastEffect), new UIPropertyMetadata(((double)(0.8D)), PixelShaderConstantCallback(7)));
        public MultiChannelContrastEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri(@"/Shader/multi.ps", UriKind.Relative);
            this.PixelShader = pixelShader;
            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(BrightnessProperty);
            this.UpdateShaderValue(ContrastrProperty);
            this.UpdateShaderValue(ContrastgProperty);
            this.UpdateShaderValue(ContrastbProperty);
            this.UpdateShaderValue(DefogProperty);
            this.UpdateShaderValue(FogColorProperty);
            this.UpdateShaderValue(ExposureProperty);
            this.UpdateShaderValue(GammaProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        public double Brightness
        {
            get
            {
                return ((double)(this.GetValue(BrightnessProperty)));
            }
            set
            {
                this.SetValue(BrightnessProperty, value);
            }
        }
        public double Contrastr
        {
            get
            {
                return ((double)(this.GetValue(ContrastrProperty)));
            }
            set
            {
                this.SetValue(ContrastrProperty, value);
            }
        }
        public double Contrastg
        {
            get
            {
                return ((double)(this.GetValue(ContrastgProperty)));
            }
            set
            {
                this.SetValue(ContrastgProperty, value);
            }
        }
        public double Contrastb
        {
            get
            {
                return ((double)(this.GetValue(ContrastbProperty)));
            }
            set
            {
                this.SetValue(ContrastbProperty, value);
            }
        }
        public double Defog
        {
            get
            {
                return ((double)(this.GetValue(DefogProperty)));
            }
            set
            {
                this.SetValue(DefogProperty, value);
            }
        }
        /// <summary>The fog color.</summary>
        public Color FogColor
        {
            get
            {
                return ((Color)(this.GetValue(FogColorProperty)));
            }
            set
            {
                this.SetValue(FogColorProperty, value);
            }
        }
        /// <summary>The exposure adjustment.</summary>
        public double Exposure
        {
            get
            {
                return ((double)(this.GetValue(ExposureProperty)));
            }
            set
            {
                this.SetValue(ExposureProperty, value);
            }
        }
        /// <summary>The gamma correction exponent.</summary>
        public double Gamma
        {
            get
            {
                return ((double)(this.GetValue(GammaProperty)));
            }
            set
            {
                this.SetValue(GammaProperty, value);
            }
        }
    }
}
