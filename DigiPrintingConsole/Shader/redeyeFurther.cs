﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace DigiPrintingConsole.Shader
{
    public class RedEyeEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(RedEyeEffect), 0);
        public static readonly DependencyProperty CenterProperty = DependencyProperty.Register("Center", typeof(Point), typeof(RedEyeEffect), new UIPropertyMetadata(new Point(0.5D, 0.5D), PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty RadiusProperty = DependencyProperty.Register("Radius", typeof(double), typeof(RedEyeEffect), new UIPropertyMetadata(((double)(0.1D)), PixelShaderConstantCallback(1)));
        public static readonly DependencyProperty RedeyeTrueProperty = DependencyProperty.Register("RedeyeTrue", typeof(double), typeof(RedEyeEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(2)));
        public static readonly DependencyProperty AspectRatioProperty = DependencyProperty.Register("AspectRatio", typeof(double), typeof(RedEyeEffect), new UIPropertyMetadata(((double)(1D)), PixelShaderConstantCallback(4)));
        public RedEyeEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri(@"/Shader/redeyeFurther.ps", UriKind.RelativeOrAbsolute);
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(CenterProperty);
            this.UpdateShaderValue(RadiusProperty);
            this.UpdateShaderValue(RedeyeTrueProperty);
            this.UpdateShaderValue(AspectRatioProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>The center point of the magnified region.</summary>
        public Point Center
        {
            get
            {
                return ((Point)(this.GetValue(CenterProperty)));
            }
            set
            {
                this.SetValue(CenterProperty, value);
            }
        }
        /// <summary>The radius of the magnified region.</summary>
        public double Radius
        {
            get
            {
                return ((double)(this.GetValue(RadiusProperty)));
            }
            set
            {
                this.SetValue(RadiusProperty, value);
            }
        }
        /// <summary>The Redeye factor.</summary>
        public double RedeyeTrue
        {
            get
            {
                return ((double)(this.GetValue(RedeyeTrueProperty)));
            }
            set
            {
                this.SetValue(RedeyeTrueProperty, value);
            }
        }
        /// <summary>The aspect ratio (width / height) of the input.</summary>
        public double AspectRatio
        {
            get
            {
                return ((double)(this.GetValue(AspectRatioProperty)));
            }
            set
            {
                this.SetValue(AspectRatioProperty, value);
            }
        }
    }
}

