﻿using DigiPhoto.LogEnvelop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.LogEnvelop.ILogger
{
    public interface ILogWrapper
    {
        void LogException(Exception ex, LoggingLevel level, string format, params object[] vars);
        void LogException(Exception ex, LoggingLevel level);
        void LogException(string message, LoggingLevel level);
        void LogAndThrowException(Exception ex, LoggingLevel level);

    }
}
