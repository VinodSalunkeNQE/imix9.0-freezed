﻿using DigiPhoto.LogEnvelop.Data;
using DigiPhoto.LogEnvelop.Exceptions;
using DigiPhoto.LogEnvelop.ILogger;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DigiPhoto.LogEnvelop.Logger
{
    public class ExceptionLogger : ILogWrapper
    {
        #region Members
        /// <summary>
        /// Reference to the log4net logging object
        /// </summary>
        private static log4net.ILog logger;
        /// <summary>
        /// The category under which this object will log messages
        /// </summary>
        private static string category;
        private static bool isInitialized;
        private static volatile ExceptionLogger instance;
        private static object syncRoot = new Object();


        #endregion

        #region Private and Singleton

        private ExceptionLogger() { }

        /// <summary>
        /// Singleton Class
        /// </summary>
        /// <param name="oAppUserInfo"></param>
        /// <returns></returns>
        public static ExceptionLogger Instance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        instance = new ExceptionLogger();
                        string logName = ConfigurationManager.AppSettings.Get("LoggerName");

                        logger = LogManager.GetLogger(logName);

                        if (logger == null)
                            throw new InvalidException("The log \"" + logName + "\" does not exist or is invalid.", logName);

                        category = logName;
                        isInitialized = false;
                        Initialize();

                    }
                }
            }
            return instance;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Default initialization of the log4net library based on
        /// entries in the config file.
        /// </summary>
        public static void Initialize()
        {
            if (!isInitialized)
            {
                XmlConfigurator.Configure();
                isInitialized = true;
            }
            else
                throw new InitializationException("Logging has already been initialized.");
        }

        /// <summary>
        /// Initialization of the log4net library based on a separate
        /// configuration file.
        /// </summary>
        /// <param name="configFile"></param>
        public void Initialize(string configFile)
        {
            if (!isInitialized)
            {
                XmlConfigurator.ConfigureAndWatch(new FileInfo(configFile));
            }
            else
                throw new InitializationException("Logging has already been initialized.");
        }

        #endregion

        #region ILogEnvelop
        /// <summary>
        /// Log an exception to the ERROR log with the specified message
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="format"></param>
        /// <param name="vars"></param>
        public void LogException(Exception ex, LoggingLevel level, string format, params object[] vars)
        {
            logger.Error(string.Format(format, vars), ex);
        }

        public void LogException(Exception ex, LoggingLevel level)
        {
            logger.Error(ex.Message, ex);
            Thread.Sleep(1000);
        }

        public void LogException(string message, LoggingLevel level)
        {
            logger.Error(message);
        }

        public void LogAndThrowException(Exception ex, LoggingLevel level)
        {
            this.LogException(ex, level);
            throw new CustomException(ex.Message);
        }

        #endregion
    }
}
