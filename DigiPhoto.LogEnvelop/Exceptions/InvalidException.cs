﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.LogEnvelop.Exceptions
{
    internal class InvalidException : Exception
    {
        /// <summary>
        /// Constructs a new <see cref="InvalidException"/> object.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="logName">The name of the log.</param>
        /// <param name="innerException">The exception which caused this exception to be thrown.</param>
        public InvalidException(string message, string logName, Exception innerException)
            : base(message, innerException)
        {
            this.LogName = logName;
        }

        /// <summary>
        /// Constructs a new <see cref="InvalidException"/> object.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="logName">The name of the log.</param>
        public InvalidException(string message, string logName)
            : this(message, logName, null)
        {
        }

        /// <summary>
        /// The name or category of the log.
        /// </summary>
        public string LogName { get; private set; }
    }
}
