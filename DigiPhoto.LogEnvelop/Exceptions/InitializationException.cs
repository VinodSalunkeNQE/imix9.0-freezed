﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.LogEnvelop.Exceptions
{
    internal class InitializationException : Exception
    {
        /// <summary>
        /// Constructs a new <see cref="InitializationException"/>.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="innerException">The exception which caused this exception to be thrown.</param>
        public InitializationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Constructs a new <see cref="InitializationException"/>.
        /// </summary>
        /// <param name="message">The error message.</param>
        public InitializationException(string message)
            : this(message, null)
        {
        }
    }
}
