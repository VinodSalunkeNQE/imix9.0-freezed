﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.LogEnvelop.Logger;
using ExifLib;
using FrameworkHelper;
using LevDan.Exif;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;
using System.Configuration;
using ImageMagick;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Windows.Documents;
using Newtonsoft.Json;
using DigiPhoto.IMIX.Model.Base;

namespace Digiphoto.ManualDownload.Console
{
    public class ManualDownLoad
    {
        private List jsonAsList = new List();
        private static volatile object _object = new object();
        string OriginalFileName = string.Empty;
        int ParentImageID = -1;
        int imagenameCntr = 0;
        int photoId;
        Int32 _QRCodeLenth = 8;
        string ProductNamePreference = string.Empty;
        PhotoBusiness photoBusiness = new PhotoBusiness();

        public ManualDownLoad()
        {
            //_logger = ExceptionLogger.Instance();
        }
        public string _QRCode = null;
        #region Fields and members

        private int _SubStoreId = -1;
        private string _SubStoreName = String.Empty;

        private List<SemiOrderSettings> _lstSemiSettings = null;

        private List<SemiOrderSettings> _lstSemiSettingsLoc = null;

        private string _defaultBrightness = string.Empty;
        private string _defaultContrast = string.Empty;
        private bool _isrotated = false;
        private string _thumbnailFilePathDate;
        private string _bigThumbnailFilePathDate;
        private string _minifiedFilePathDate = string.Empty; // By Suraj

        private int? _rfidScanType = null;
        private bool _isRFIDEnabled = false;



        /// <summary>
        /// The photo layer
        /// </summary>
        private string _photoLayer = null;

        private string _networkPath = string.Empty;//@"\\192.168.29.179\DigiImages\ManualDownload";
        private string _networkBackupPath = string.Empty;//@"\\192.168.29.179\DigiImages\ManualDownlaodBackup";

        private string _filePathDate = string.Empty;
        private string _filePath = string.Empty;

        //private ManualDownloadInfo _objInfo = null;
        private ConfigurationInfo _configInfo = null;
        private bool _isResetDPIRequired = false;
        private bool _is_SemiOrder = false;
        private Nullable<bool> _IsAutoRotate = false;
        /// <summary>
        /// The count images download
        /// </summary>
        private int CountImagesDownload = 0;
        private int ImagesDownloadSequence = 0;
        bool IsMobileRfidActive = false;
        /// <summary>
        /// The needrotaion
        /// </summary>
        private int _needRotation = 0;

        bool _isVideoProcessed = true;
        bool IsGreenScreenWorkFlow = false;
        bool RunManualDownloadLocWise = false;
        List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = new List<ManualDownloadPosLocationAssociation>();

        /// <summary>
        /// Gets or sets the image meta data.
        /// </summary>
        /// <value>
        /// The image meta data.
        /// </value>
        public string ImageMetaData
        {
            get;
            set;
        }

        public string DefaultEffects
        {
            get;
            set;
        }
        bool IsSpecSettingsUpdated = false;
        public static List<String> FilesToDelete = new List<string>();
        private const string _metaDataFormat = "<image Dimensions={0} CameraManufacture={1} HorizontalResolution={2} VerticalResolution={3} CameraModel={4} ISO-SpeedRating={5} DateTaken={6} ExposureMode={7} Sharpness={8} Orientation={9} GPSLatitude={10} GPSLongitude={11} ExposureTime={12} ApertureValue={13}></image>";
        private const string _defaultEffects = "<image brightness = '{0}' contrast = '{1}' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
        private const string _layeringdata = "<photo zoomfactor='{0}' border='{1}' bg= '{2}' canvasleft='{3}' canvastop='{4}' scalecentrex='{5}' scalecentrey='{6}' producttype='{9}'>{7}{8}</photo>";
        // string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];
        string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];

        #endregion

        void SetSubStoreId()
        {

            //Start of changes by Bhavin Udani Bug: ss.dat for Manual running from code is in DigiPhoto thus all values are null  Date: 26112020
            //string pathtosave = AppDomain.CurrentDomain.BaseDirectory;
            string pathtosave = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            pathtosave = pathtosave.Replace("Digiphoto.Manual.Console", "DigiPhoto");
            // end of changes by Bhavin Udani Bug: ss.dat for Manual running from code is in DigiPhoto thus all values are null  Date: 26112020
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Path-> " + pathtosave + "\\ss.dat");
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;

                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        string subID = CryptorEngine.Decrypt(line, true);
                        if (!String.IsNullOrEmpty(subID))
                        {
                            _SubStoreId = (subID.Split(',')[0]).ToInt32();
                            _SubStoreName = (new StoreSubStoreDataBusniess()).GetLoginUserDefaultSubstores(_SubStoreId).FirstOrDefault().DG_SubStore_Name;
                        }
                        else
                            ErrorHandler.ErrorHandler.LogFileWrite("Invalid data in ss.dat file.");
                        QRCodeLength(_SubStoreId);
                    }
                }
            }
            catch (Exception ex)
            {
                _SubStoreId = -1;
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        public void StartMaualDownloadProcess()
        {
            try
            {
                #region Add Keys Dynamically by Anis on  1 Aug
              
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                if (ConfigurationManager.AppSettings.AllKeys.Contains("jpgFileExtension") == false)
                {   
                    config.AppSettings.Settings.Add("jpgFileExtension", ".jpg");
                }
                if (ConfigurationManager.AppSettings.AllKeys.Contains("pngFileExtension") == false)
                {   
                    config.AppSettings.Settings.Add("pngFileExtension", ".png");
                }
                config.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection("appSettings");

                #endregion

                //_objInfo = (new ManualDownloadBusiness()).GetManualDownloadInfo();

                //if (_objInfo == null)
                //{
                //    _logger.LogException("No data avialable for download.", DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                //    return;
                //}

                //_objInfo = new ManualDownloadInfo();
                //_lstSemiSettings = (new ManualDownloadBusiness()).GetSemiSettings();
                ///Now Collect the file information from ManualDownLoad Folder

                //_logger.LogException("Start collecting files", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //ErrorHandler.ErrorHandler.LogFileWrite("Start collecting files");

                lock (_object)
                {
                    SetSubStoreId();
                    if (GetManualDownloadLocationWisesettings())
                        lstPosLocationAssociation = GetPosLocationAssociation();

                    CollectFilesForProcessing();
                }

                //_logger.LogException("Collecting files is completed", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                //ErrorHandler.ErrorHandler.LogFileWrite("Collecting files is completed");


            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                SetSubStoreId();
            }
        }

        ///by latika to implement locationwise settings
        private bool GetManualDownloadLocationWisesettings()
        {
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigDataBySystemID(Environment.MachineName);////changed by latika for locationwise settings

            return RunManualDownloadLocWise = store.RunManualDownloadLocationWise;
        }
        private List<ManualDownloadPosLocationAssociation> GetPosLocationAssociation()
        {
            List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = new List<ManualDownloadPosLocationAssociation>();
            lstPosLocationAssociation = new ConfigBusiness().GetPosLocationMapping(Environment.MachineName);

            return lstPosLocationAssociation;
        }

        private bool SetNetWorkAndFilePath(int subStoreId)
        {
            _configInfo = (new ConfigBusiness()).GetConfigurationData(subStoreId);

            if (_configInfo == null)
            {
                //_logger.LogException("Invalid sub store id.", DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite("Invalid sub store id.");
                return false;
            }

            _filePath = _configInfo.DG_Hot_Folder_Path;
            _filePathDate = Path.Combine(_filePath, DateTime.Now.ToString("yyyyMMdd"));

            _thumbnailFilePathDate = Path.Combine(_filePath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
            _bigThumbnailFilePathDate = Path.Combine(_filePath, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));

            #region Minified Images Path Create  // Suraj
            _minifiedFilePathDate = System.IO.Path.Combine(_filePath, "Minified_Images", DateTime.Now.ToString("yyyyMMdd"));
            if (!Directory.Exists(_minifiedFilePathDate))
                Directory.CreateDirectory(_minifiedFilePathDate);
            #endregion

            if (!Directory.Exists(_filePathDate))
                Directory.CreateDirectory(_filePathDate);

            if (!Directory.Exists(_thumbnailFilePathDate))
                Directory.CreateDirectory(_thumbnailFilePathDate);

            if (!Directory.Exists(_bigThumbnailFilePathDate))
                Directory.CreateDirectory(_bigThumbnailFilePathDate);

            //_BorderFolder = _configInfo.DG_Frame_Path;


            //_networkPath = _configInfo.DG_Hot_Folder_Path + "\\ManualDownload";
            //_networkBackupPath = _configInfo.DG_Hot_Folder_Path + "\\ManualDownloadBackup";
            _IsAutoRotate = _configInfo.DG_IsAutoRotate;

            return true;
        }
        bool IsAutoColorCorrectionActive = false;
        bool IsCorrectionAtDownloadActive = false;
        bool IsContrastCorrectionActive = false;
        bool IsGammaCorrectionActive = false;
        bool IsNoiseReductionActive = false;
        private void GetRfidConfigInfo(int subStoreId)
        {
            var configList = (new ConfigBusiness()).GetNewConfigValues(subStoreId);
            if (configList == null)
                return;
            // var configList = new ConfigBusiness().GetNewConfigValues(subStoreId);
            iMIXConfigurationInfo configValue = configList.FirstOrDefault
                                    (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsAnonymousQrCodeEnabled);

            iMIXConfigurationInfo configValue3 = configList.FirstOrDefault
                                (o => o.IMIXConfigurationMasterId == (long)ConfigParams.RFIDScanType);

            iMIXConfigurationInfo configValue2 = configList.FirstOrDefault
                                            (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsEnabledRFID);
            iMIXConfigurationInfo configValue4 = configList.FirstOrDefault
                                            (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsEnableAutoVidProcessing);
            iMIXConfigurationInfo configValue5 = configList.FirstOrDefault
                                           (o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsGreenScreenFlow);
            if (configValue2 != null && !string.IsNullOrEmpty(configValue2.ConfigurationValue))
            {
                _isRFIDEnabled = Convert.ToBoolean(configValue2.ConfigurationValue);
            }
            else
            {
                _isRFIDEnabled = false;
            }

            if (_isRFIDEnabled == true && configValue3 != null)
            {
                _rfidScanType = Convert.ToInt32(configValue3.ConfigurationValue);
            }
            else
                _rfidScanType = null;
            if (configValue4 != null)
            {
                if (Convert.ToBoolean(configValue4.ConfigurationValue) == true)
                    _isVideoProcessed = false;
            }
            if (configValue5 != null && !string.IsNullOrEmpty(configValue5.ConfigurationValue))
            {
                IsGreenScreenWorkFlow = Convert.ToBoolean(configValue5.ConfigurationValue);
            }

        }

        private void CollectFilesForProcessing()
        {
            //Start of Changes by Bhavin Udani for Bug: _SubStoreId takes value of -1 in  CollectFilesForProcessing() thus method returns in first statement without further processing Date: 26112020
            //if (_SubStoreId <= 0)
            //{
            //    ErrorHandler.ErrorHandler.LogFileWrite("Invalid sub store id.");
            //    return;
            //}
            //End of Change by Bhavin Udani Date:26112020

            ConfigurationInfo config = (new ConfigBusiness()).GetConfigurationData(1); //bhavin (new ConfigBusiness()).GetConfigurationData(_SubStoreId);
            _networkBackupPath = config.DG_Hot_Folder_Path + "ManualDownloadCorrupt";
            //Author Bhavin Udani. Created on 18 Feb 2021. Reverting to original settings to fix normal workflow not working bug
            _networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload");
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 353 " + _networkPath);
            DirectoryInfo info = new DirectoryInfo(_networkPath);
            string[] extensions = new[] { jpgExtension, pngExtension };

            FileInfo[] files =
            info.EnumerateFiles()
            .Where(f => extensions.Contains(f.Extension.ToLower()))
            .ToArray();

            if (files.Count() > 0)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 364 ");
                ProcessPhotos(config);
            }

            //ErrorHandler.ErrorHandler.LogFileWrite(String.Format("Hotfolder path: {0} and sub store id {1} -->", config.DG_Hot_Folder_Path, _SubStoreId));
            if (lstPosLocationAssociation.Count == 0)
            {
                // in this case service will work substore wise.
                _networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload", _SubStoreName);
                ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 373 " + _networkPath);
                ProcessPhotos(config);
            }

            else
            {
                //service will work location wise.
                foreach (string locationName in lstPosLocationAssociation.Select(x => x.LocationName))
                {
                    _networkPath = Path.Combine(config.DG_Hot_Folder_Path + "ManualDownload", _SubStoreName, locationName);
                    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 382 " + _networkPath);
                    ProcessPhotos(config);
                }
            }
        }

        public void ProcessPhotos(ConfigurationInfo config)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(_networkPath);
                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 392 " + _networkPath);
                string[] extensions = new[] { jpgExtension, pngExtension };

                FileInfo[] files =
                info.EnumerateFiles()
                .Where(f => extensions.Contains(f.Extension.ToLower()))
                .ToArray();

                //FileInfo[] files = info.GetFiles("*.jpg").ToArray();
                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 401  File Count" + files.Count().ToString());
                if (files == null || files.Count() <= 0)
                    return;

                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 405  ");
                var imageList = new List<MyImageClass>();
                var count = files.Count();//(files.Count() == 1) ? files.Count() + 1 : files.Count();
                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 407  ");
                foreach (var file in files)//.Take(count - 1))
                {
                    if (IsFileLocked(file))
                    {
                        var title = file.Name.Substring(file.Name.LastIndexOf("\\") + 1).Replace(jpgExtension, "").Replace(".jpeg", "").Replace(pngExtension, "");
                        ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 413  ");
                        string[] infoCol = title.Split('_');
                        var imageClass = new MyImageClass
                        {
                            //Title = file.Name.Substring(file.Name.LastIndexOf("\\") + 1).Replace(FileExt, "").Replace(".jpeg", ""),
                            Title = file.Name.Substring(file.Name.LastIndexOf("\\") + 1).Replace(jpgExtension, "").Replace(".jpeg", "").Replace(pngExtension, ""),
                            ImagePath = file.FullName.Substring(0, file.FullName.LastIndexOf("\\")),
                            IsChecked = false,
                            PhotoNumber = Convert.ToInt64(infoCol[1]),
                            PhotoGrapherID = Convert.ToInt32(infoCol[2]),
                            //PhotoNumber = Convert.ToInt64(infoCol[0]),
                            //PhotoGrapherID = Convert.ToInt32(infoCol[1]),
                            ActFileExtension = file.Extension,
                            //Veerendra
                            //commented by ajay sinha on 24Dec18 to resolve the issues if a image comes in Digiimages\ManuallDownlaod 
                            //folder as "2112_212438140_6_1_22_0_2112212404702(2) _0.jpg which contains bracket in name"
                            //SettingStatus = (SettingStatus)Enum.ToObject(typeof(SettingStatus), Convert.ToInt32(infoCol[6]))

                            SettingStatus = (SettingStatus)Enum.ToObject(typeof(SettingStatus), !infoCol[6].Contains('(') ? Convert.ToInt32(infoCol[6]) : 0)
                            //SettingStatus = (SettingStatus)Enum.ToObject(typeof(SettingStatus),0)

                            //SettingStatus = (SettingStatus)Enum.ToObject(typeof(SettingStatus), Convert.ToInt32(infoCol[1]))

                        };
                        //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 438  ");
                        imageList.Add(imageClass);
                        ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 439  ");
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("We are unable to open the file " + file.Name);
                    }
                }
                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 446  ");
                imageList = imageList.OrderBy(o => o.PhotoGrapherID).ThenBy(p => p.PhotoNumber).ToList();
                if (config.DG_SemiOrderMain.HasValue && config.DG_SemiOrderMain.Value)
                    _lstSemiSettings = (new ManualDownloadBusiness()).GetSemiSettings();
                //ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 450  ");
                List<long> PhotoNumbers = new List<long>();
                foreach (var item in imageList)
                {
                    PhotoNumbers.Add(item.PhotoNumber);
                }

                List<ManualDisplayOrder> MDOList = new List<ManualDisplayOrder>();
                MDOList = photoBusiness.GetPhotosDisplayNumber(PhotoNumbers);
                PhotoNumbers.Clear();
                List<MyImagesList> ImagesList = new List<MyImagesList>();
                foreach (var item in imageList)
                {
                    foreach (var item1 in MDOList)
                    {
                        if (item.PhotoNumber == item1.PhotoNumber)
                        {
                            ImagesList.Add(new MyImagesList() { MyImageClass = item, DisplayOrder = item1.DisplayOrder });
                        }
                    }
                }
                //ProcessFile(imageList);   Commented for Display Order
                ProcessFile(ImagesList);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Process Photos : Error Msg :  " + ex.Message + "    : Error Stack : " + ex.InnerException.ToString());
            }
        }

        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return true;
        }

        /// <summary>
        /// ProcessFile, pick the file from download folder and apply effects.
        /// </summary>
        /// <param name="imageList">List of MyImageClass</param>
        private void ProcessFile(List<MyImagesList> imageList)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 479  ");
            if (imageList == null || imageList.Count == 0)
                return;

            //_logger.LogException("Start download flow", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
            //ErrorHandler.ErrorHandler.LogFileWrite("Start normal download process.");

            if (!Directory.Exists(_networkBackupPath))
            {
                DirectoryInfo di = new DirectoryInfo(_networkBackupPath);
                di.Create();
            }
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 491  ");
            foreach (var item in imageList)
            {
                var info = new List<MyImageClass>();
                info.Add(item.MyImageClass);
                ManualDownloadInfo downloadInfo = PopulateManualDownloadInfo(info[0]);
                //NormalDownloadFlow(info, downloadInfo);
                NormalDownloadFlow(info, downloadInfo, item.DisplayOrder);
            }
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 499  ");
            for (int j = 0; j < FilesToDelete.Count; j++)
            {
                if (File.Exists(FilesToDelete[j]))
                {
                    try
                    {
                        File.Delete(FilesToDelete[j]);
                        FilesToDelete.RemoveAt(j);
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        //Commented for DisplayOrder by Suraj
        //private void ProcessFile(List<MyImageClass> imageList)
        //{
        //    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 479  ");
        //    if (imageList == null || imageList.Count == 0)
        //        return;

        //    //_logger.LogException("Start download flow", DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
        //    //ErrorHandler.ErrorHandler.LogFileWrite("Start normal download process.");

        //    if (!Directory.Exists(_networkBackupPath))
        //    {
        //        DirectoryInfo di = new DirectoryInfo(_networkBackupPath);
        //        di.Create();
        //    }
        //    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 491  ");
        //    foreach (var item in imageList)
        //    {
        //        var info = new List<MyImageClass>();
        //        info.Add(item);
        //        ManualDownloadInfo downloadInfo = PopulateManualDownloadInfo(info[0]);
        //        NormalDownloadFlow(info, downloadInfo);
        //    }
        //    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 499  ");
        //    for (int j = 0; j < FilesToDelete.Count; j++)
        //    {
        //        if (File.Exists(FilesToDelete[j]))
        //        {
        //            try
        //            {
        //                File.Delete(FilesToDelete[j]);
        //                FilesToDelete.RemoveAt(j);
        //            }
        //            catch (Exception)
        //            { }
        //        }
        //    }
        //}

        /// <summary>
        /// PopulateManualDownloadInfo
        /// </summary>
        /// <param name="info"></param>
        private ManualDownloadInfo PopulateManualDownloadInfo(MyImageClass info)
        {

            string[] infoCol = info.Title.Split('_');
            //0-PhotoNo, 1 -PhotographerId, 2- SubstoreId, 3- LocationId
            ManualDownloadInfo objInfo = new ManualDownloadInfo();
            objInfo.PhotoNo = Convert.ToInt64(infoCol[1]);
            objInfo.PhotographerId = Convert.ToInt32(infoCol[2]);
            objInfo.SubStoreId = Convert.ToInt32(infoCol[3]);
            objInfo.LocationId = Convert.ToInt32(infoCol[4]);
            objInfo.CharacterId = Convert.ToInt32(infoCol[5]);

            if (SetNetWorkAndFilePath(objInfo.SubStoreId) == false)
                return null;

            GetRfidConfigInfo(objInfo.SubStoreId);
            return objInfo;
        }

        /// <summary>
        /// NormalDownloadFlow
        /// </summary>
        /// <param name="imagelist"></param>
        private void NormalDownloadFlow(List<MyImageClass> imagelist, ManualDownloadInfo objInfo, long DisplayOrder)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 544  ");
            String s = String.Empty;
            int i = 0;
            List<MyImageClassList> lstMyImgClassList = new List<MyImageClassList>();

            MyImageClassList myImgClassList = new MyImageClassList();
            string barcode = string.Empty;
            string format = string.Empty;

            int getitem = 0;
            DateTime? CaptureDate = new DateTime();

            //long photoNo = -1;
            string photographerId = string.Empty;
            //int locationid = -1;
            //int subStoreid = -1;
            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 560  ");
            foreach (var item in imagelist)
            {
                try
                {
                    if (IsGreenScreenWorkFlow)
                        _isVideoProcessed = IsGreenScreenWorkFlow;
                    //FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + FileExt);
                    FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + item.ActFileExtension);
                    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 569  ");
                    try
                    {
                        ExifTagCollection exif = null;

                        if (objInfo == null)
                            objInfo = PopulateManualDownloadInfo(item);

                        _is_SemiOrder = Convert.ToBoolean(_configInfo.DG_SemiOrderMain);

                        i++;
                        string CameraManufacture = "'" + "##" + "'";
                        string CameraModel = "'" + "##" + "'";
                        string orientation = "'" + "##" + "'";
                        string HorizontalResolution = "'" + "##" + "'";
                        string VerticalResolution = "'" + "##" + "'";
                        string Datetaken = "'" + "##" + "'";
                        string dimension = "'" + "##" + "'";
                        string ISOSpeedRatings = "'" + "##" + "'";
                        string ExposureMode = "'" + "##" + "'";
                        string Sharpness = "'" + "##" + "'";
                        string GPSLatitude = "'" + "##" + "'";
                        string GPSLongitude = "'" + "##" + "'";
                        string ExposureTime = "'" + "##" + "'";         //shutter speed
                        string ApertureValue = "'" + "##" + "'";
                        getitem = imagelist.Count;
                        string name = item.Title + item.ActFileExtension;
                        if (name != "Thumbs.db")
                        {
                            string picname = name;
                            s += ", " + picname;
                            try
                            {
                                // exif = new ExifTagCollection(Path.Combine(item.ImagePath, item.Title) + FileExt);
                                exif = new ExifTagCollection(Path.Combine(item.ImagePath, item.Title) + item.ActFileExtension);

                                #region Auto-Rotate
                                //Start:Code for Auto-Rotate
                                if ((bool)_IsAutoRotate)
                                {
                                    //ExifReader reader = null;
                                    string renderedTag = "";
                                    try
                                    {
                                        using (var reader = new ExifReader(Path.Combine(item.ImagePath, item.Title) + item.ActFileExtension))
                                        {
                                            foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                            {
                                                object val;
                                                if (reader.GetTagValue(tagID, out val))
                                                {
                                                    renderedTag = val.ToString();
                                                }
                                            }
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        item.IsCorrupt = true;
                                        ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                                    }

                                    if (!string.IsNullOrEmpty(renderedTag))
                                    {
                                        _needRotation = GetRotationValue(renderedTag);
                                    }
                                    else
                                    {
                                        _needRotation = 0;
                                    }
                                }
                                //End:Code for Auto-Rotate
                                #endregion

                                if (exif.Where(o => o.Id == 36867).Count() > 0)
                                {
                                    try
                                    {
                                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                        objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                        string datePart = exif[36867].Value.Split(' ').First();
                                        datePart = datePart.Replace(':', '/');
                                        string timePart = exif[36867].Value.Split(' ').Last();
                                        CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                    }
                                    catch (Exception ex)
                                    {
                                        CaptureDate = null;
                                        //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                                        ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                                    }
                                }
                                else
                                    CaptureDate = null;

                                foreach (LevDan.Exif.ExifTag tag in exif)
                                {
                                    if (tag.Id == 271)
                                    {
                                        CameraManufacture = "'" + exif[271].Value + "'";
                                    }
                                    else if (tag.Id == 272)
                                    {
                                        CameraModel = "'" + exif[272].Value + "'";
                                    }
                                    else if (tag.Id == 274)
                                    {
                                        orientation = "'" + exif[274].Value + "'";
                                    }
                                    else if (tag.Id == 282)
                                    {
                                        HorizontalResolution = "'" + exif[282].Value + "'";
                                    }
                                    else if (tag.Id == 283)
                                    {
                                        VerticalResolution = "'" + exif[283].Value + "'";
                                    }
                                    else if (tag.Id == 36867)
                                    {
                                        Datetaken = "'" + exif[36867].Value + "'";
                                    }
                                    else if (tag.Id == 40962 || tag.Id == 40963)
                                    {
                                        dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                    }
                                    else if (tag.Id == 34855)
                                    {
                                        ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                    }
                                    else if (tag.Id == 41986)
                                    {
                                        ExposureMode = "'" + exif[41986].Value + "'";
                                    }
                                    else if (tag.Id == 41994)
                                    {
                                        Sharpness = "'" + exif[41994].Value + "'";
                                    }
                                    else if (tag.Id == 2)
                                    {
                                        GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 4)
                                    {
                                        GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 33434)
                                    {
                                        ExposureTime = "'" + exif[33434].Value + "'";
                                    }
                                    else if (tag.Id == 37381)
                                    {
                                        ApertureValue = "'" + exif[37381].Value + "'";
                                    }
                                }
                                ImageMetaData = String.Format(_metaDataFormat, dimension, CameraManufacture,
                                                              HorizontalResolution, VerticalResolution, CameraModel,
                                                              ISOSpeedRatings, Datetaken, ExposureMode, Sharpness,
                                                              orientation, GPSLatitude, GPSLongitude, ExposureTime, ApertureValue);

                                //ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + "></image>";
                            }
                            catch (Exception ex)
                            {
                                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);

                            }
                            finally { exif = null; }

                            PhotoBusiness photoBiz = new PhotoBusiness();
                            int photoCount = photoBiz.PhotoCountCurrentDate(objInfo.PhotoNo.ToString(), objInfo.PhotographerId, 1);
                            DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                            string filename = string.Empty;
                            string title = imagelist.Select(x => x.Title).FirstOrDefault();
                            if (!title.Contains('#'))
                                filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                                + "_" + objInfo.PhotographerId.ToString() + item.ActFileExtension;
                            else
                            {
                                filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
                                           + "_" + objInfo.PhotographerId.ToString() + "_" + title.Split('#')[1] + item.ActFileExtension;
                                _isVideoProcessed = true;
                            }
                            if (photoCount > 0)
                                filename = Path.GetFileNameWithoutExtension(filename) + "(" + photoCount + ")" + item.ActFileExtension;

                            //End new config data get
                            if (string.IsNullOrWhiteSpace(_defaultBrightness))
                                _defaultBrightness = "0";
                            if (string.IsNullOrWhiteSpace(_defaultContrast))
                                _defaultContrast = "1";
                            DefaultEffects = String.Format(_defaultEffects, _defaultBrightness, _defaultContrast);
                            if (item.SettingStatus == SettingStatus.SpecUpdated)
                            {
                                IsSpecSettingsUpdated = true;
                                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                                xmlDoc.Load(Path.Combine(item.ImagePath, item.Title + ".xml"));
                                System.Xml.XmlNode effectsNode = xmlDoc.GetElementsByTagName("Effects")[0];
                                DefaultEffects = effectsNode.InnerText;
                                System.Xml.XmlNode layringNode = xmlDoc.GetElementsByTagName("Layering")[0];
                                if (layringNode.InnerText == "NULL" || String.IsNullOrEmpty(layringNode.InnerText))
                                    _photoLayer = "test";
                                else
                                    _photoLayer = layringNode.InnerText;
                            }
                            int isImageProcessed = 1;
                            bool isCroped = false;
                            if (IsSpecSettingsUpdated)
                                isImageProcessed = 0;
                            string cropedImagePath = Path.Combine(item.ImagePath, "Croped", picname);
                            if (File.Exists(cropedImagePath))
                            {
                                isCroped = true;
                                File.Move(cropedImagePath, Path.Combine(_filePath, "Croped", filename));
                            }


                            //from here needs to put data 


                            if (photoBiz.CheckPhotos(filename, objInfo.PhotographerId))
                            {
                                if (_lstSemiSettings != null && _lstSemiSettings.Count != 0)
                                {
                                    _lstSemiSettingsLoc =
                                            _lstSemiSettings.Where(x => x.DG_LocationId == objInfo.LocationId).ToList();
                                }

                                if (_lstSemiSettingsLoc == null || _lstSemiSettingsLoc.Count == 0)
                                {
                                    _isResetDPIRequired = (new CameraBusiness()).CheckResetDPIRequired(HorizontalResolution,
                                                                VerticalResolution, objInfo.PhotographerId);

                                    GetNewConfigLocationValues(objInfo.SubStoreId, objInfo.LocationId);

                                    int PhotoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename,
                                                            serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
                                                            objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
                                                            objInfo.CharacterId, 0, _isVideoProcessed, 1, -1, filename, 0, isCroped, DisplayOrder);
                                    _photoLayer = null;

                                    if (IsMobileRfidActive)
                                    {
                                        MobileRfidAssociationCheck(PhotoId, item.Title);
                                    }
                                    if (_IsAutoRotate.HasValue && _IsAutoRotate.Value == true)
                                    {
                                        //if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(".jpg", ".xml"))))
                                        if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(jpgExtension, ".xml").Replace(pngExtension, ".xml"))))
                                        {
                                            rotateImage(Path.Combine(item.ImagePath, picname));
                                        }
                                    }
                                    if (_isResetDPIRequired)
                                        ResetLowDPI(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
                                    else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                        AutoCorrectImageWIthCopy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
                                    else
                                        File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename), true);

                                    SemiOrderSettings objDG_SemiOrder_Settings = null;
                                    if (_lstSemiSettingsLoc != null && _lstSemiSettingsLoc.Count > 0)
                                    {
                                        objDG_SemiOrder_Settings =
                                            _lstSemiSettings.Where(x => x.DG_LocationId == objInfo.LocationId).FirstOrDefault();
                                    }
                                    if (_is_SemiOrder && objDG_SemiOrder_Settings != null)
                                    {
                                        SaveSpecPrintEffectsIntoDb(PhotoId, objInfo.SubStoreId, objInfo.LocationId,
                                                                    objDG_SemiOrder_Settings, _filePathDate, filename);
                                    }
                                    else
                                    {
                                        // Code by Suraj For Images Resize Logic for without spec printing.
                                        //ResizeWPFImage(Path.Combine(_filePathDate, filename), 210, Path.Combine(_thumbnailFilePathDate, filename),Path.Combine(_filePathDate, filename));
                                        //ResizeWPFImage(Path.Combine(_filePathDate, filename), 1200, Path.Combine(_bigThumbnailFilePathDate, filename));
                                        ResizeImages(0.2, Path.Combine(_filePathDate, filename), Path.Combine(_thumbnailFilePathDate, filename));
                                        ResizeImages(0.6, Path.Combine(_filePathDate, filename), Path.Combine(_bigThumbnailFilePathDate, filename));
                                        ResizeImages(0.6, Path.Combine(_filePathDate, filename), Path.Combine(_minifiedFilePathDate, "m_" + filename));
                                    }

                                    _isResetDPIRequired = false;
                                    //Interlocked.Add(ref CountImagesDownload, 1);

                                    Interlocked.Add(ref ImagesDownloadSequence, 1);

                                    //CountImagesDownload = CountImagesDownload + 1;

                                    //Interlocked.Add(ref PhotoNo, 1);
                                    //objInfo.PhotoNo = objInfo.PhotoNo + 1;
                                    if (fileInfo.Exists)
                                    {
                                        if (fileInfo.IsReadOnly)
                                            File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                                        try
                                        {
                                            fileInfo.Delete();
                                            string settngsXmlPath = (fileInfo.FullName).Replace(fileInfo.Extension, ".xml");
                                            if (File.Exists(settngsXmlPath))
                                                File.Delete(settngsXmlPath);
                                        }
                                        catch { }

                                    }
                                }
                                else
                                {

                                    foreach (SemiOrderSettings objDG_SemiOrder_Settings in _lstSemiSettingsLoc)
                                    {
                                        imagenameCntr = imagenameCntr + 1;
                                        if (ParentImageID == -1)
                                        {
                                            string filenametobesaved = filename;
                                            OriginalFileName = filename;
                                            photoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filenametobesaved,
                                                             serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
                                                             objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
                                                             objInfo.CharacterId, 0, _isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, isCroped, DisplayOrder);

                                            //photoId = photoBiz.SetPhotoDetails(substoreId, filename, picname.ToString() + "_" + imagenameCntr, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), ParentImageID, OriginalFileName);
                                            ParentImageID = photoId;
                                            _photoLayer = null;
                                            DefaultEffects = String.Format(_defaultEffects, _defaultBrightness, _defaultContrast);
                                        }
                                        else
                                        {
                                            // photoId = phBusiness.SetPhotoDetails(substoreId, filename, picname.ToString() + "_" + imagenameCntr, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), ParentImageID, OriginalFileName);
                                            string filenametobesaved = filename;//.Substring(0, filename.Length - 4) + "_" + imagenameCntr + ".jpg";
                                            photoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filenametobesaved,
                                                               serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
                                                               objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
                                                               objInfo.CharacterId, 0, _isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, isCroped, DisplayOrder);

                                            _photoLayer = null;
                                        }
                                        _isResetDPIRequired = (new CameraBusiness()).CheckResetDPIRequired(HorizontalResolution,
                                                                    VerticalResolution, objInfo.PhotographerId);

                                        //int PhotoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename,
                                        //                       serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
                                        //                       objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, 1,
                                        //                       objInfo.CharacterId);

                                        GetNewConfigLocationValues(objInfo.SubStoreId, objInfo.LocationId);



                                        if (IsMobileRfidActive)
                                        {
                                            MobileRfidAssociationCheck(photoId, item.Title);
                                        }
                                        if (_IsAutoRotate.HasValue && _IsAutoRotate.Value == true)
                                        {
                                            if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(jpgExtension, ".xml").Replace(pngExtension, ".xml"))))
                                            {
                                                rotateImage(Path.Combine(item.ImagePath, picname));
                                            }
                                        }
                                        if (_isResetDPIRequired)
                                            ResetLowDPI(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
                                        else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                            AutoCorrectImageWIthCopy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
                                        else
                                            File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));

                                        if (_is_SemiOrder && objDG_SemiOrder_Settings != null)
                                        {
                                            SaveSpecPrintEffectsIntoDb(photoId, objInfo.SubStoreId, objInfo.LocationId,
                                                                        objDG_SemiOrder_Settings, _filePathDate, filename);
                                        }
                                        else
                                        {
                                            ResizeWPFImage(Path.Combine(_filePathDate, filename), 210, Path.Combine(_thumbnailFilePathDate, filename),
                                                                                    Path.Combine(_filePathDate, filename));
                                            ResizeWPFImage(Path.Combine(_filePathDate, filename), 1200, Path.Combine(_bigThumbnailFilePathDate, filename));
                                        }

                                        _isResetDPIRequired = false;
                                        //Interlocked.Add(ref CountImagesDownload, 1);

                                        Interlocked.Add(ref ImagesDownloadSequence, 1);

                                        //CountImagesDownload = CountImagesDownload + 1;

                                        //Interlocked.Add(ref PhotoNo, 1);
                                        //objInfo.PhotoNo = objInfo.PhotoNo + 1;

                                        filename = filename.Substring(0, filename.Length - 4) + "_" + imagenameCntr + jpgExtension;
                                    }
                                    //_photoLayer = null;
                                }
                                if (fileInfo.Exists)
                                {
                                    if (fileInfo.IsReadOnly)
                                        File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                                    try
                                    {
                                        fileInfo.Delete();
                                        string settngsXmlPath = (fileInfo.FullName).Replace(fileInfo.Extension, ".xml");
                                        if (File.Exists(settngsXmlPath))
                                            File.Delete(settngsXmlPath);
                                    }
                                    catch { }
                                }
                                ParentImageID = -1;
                                OriginalFileName = string.Empty;
                                imagenameCntr = 0;

                            }
                            else
                            {
                                //MessageBox.Show(PhotoNo + " already exists for today");
                                //_logger.LogException(objInfo.PhotoNo + " already exists for today",DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
                                ErrorHandler.ErrorHandler.LogFileWrite(objInfo.PhotoNo + " already exists for today");
                                if (fileInfo.IsReadOnly)
                                    File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                                if (fileInfo.Exists)
                                {
                                    if (fileInfo.IsReadOnly)
                                        File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
                                    if (File.Exists(_networkBackupPath + "\\" + fileInfo.Name))
                                    {
                                        try
                                        {
                                            File.Delete(_networkBackupPath + "\\" + fileInfo.Name);
                                            fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
                                    }
                                }
                            }
                        }
                    }
                    catch (IOException IOex)
                    {
                        string errorMessage = "Got IO Exception after Archived movement for file " + fileInfo.FullName;
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        FilesToDelete.Add(fileInfo.FullName.ToLower());
                        for (int j = 0; j < FilesToDelete.Count; j++)
                        {
                            if (File.Exists(FilesToDelete[j]))
                            {
                                try
                                {
                                    File.Delete(FilesToDelete[j]);
                                    FilesToDelete.RemoveAt(j);
                                }
                                catch { }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                        ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                    }

                }
                catch (Exception x)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite(x.Message);
                }
            }
        }


        #region Commented By Suraj For DisplayOrder 
        //private void NormalDownloadFlow(List<MyImageClass> imagelist, ManualDownloadInfo objInfo)
        //{
        //    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 544  ");
        //    String s = String.Empty;
        //    int i = 0;
        //    List<MyImageClassList> lstMyImgClassList = new List<MyImageClassList>();

        //    MyImageClassList myImgClassList = new MyImageClassList();
        //    string barcode = string.Empty;
        //    string format = string.Empty;

        //    int getitem = 0;
        //    DateTime? CaptureDate = new DateTime();

        //    //long photoNo = -1;
        //    string photographerId = string.Empty;
        //    //int locationid = -1;
        //    //int subStoreid = -1;
        //    ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 560  ");
        //    foreach (var item in imagelist)
        //    {
        //        try
        //        {
        //            if (IsGreenScreenWorkFlow)
        //                _isVideoProcessed = IsGreenScreenWorkFlow;
        //            //FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + FileExt);
        //            FileInfo fileInfo = new FileInfo(item.ImagePath + "\\" + item.Title + item.ActFileExtension);
        //            ErrorHandler.ErrorHandler.LogFileWrite("Mannual Download : Line Number : 569  ");
        //            try
        //            {
        //                ExifTagCollection exif = null;

        //                if (objInfo == null)
        //                    objInfo = PopulateManualDownloadInfo(item);

        //                _is_SemiOrder = Convert.ToBoolean(_configInfo.DG_SemiOrderMain);

        //                i++;
        //                string CameraManufacture = "'" + "##" + "'";
        //                string CameraModel = "'" + "##" + "'";
        //                string orientation = "'" + "##" + "'";
        //                string HorizontalResolution = "'" + "##" + "'";
        //                string VerticalResolution = "'" + "##" + "'";
        //                string Datetaken = "'" + "##" + "'";
        //                string dimension = "'" + "##" + "'";
        //                string ISOSpeedRatings = "'" + "##" + "'";
        //                string ExposureMode = "'" + "##" + "'";
        //                string Sharpness = "'" + "##" + "'";
        //                string GPSLatitude = "'" + "##" + "'";
        //                string GPSLongitude = "'" + "##" + "'";
        //                string ExposureTime = "'" + "##" + "'";         //shutter speed
        //                string ApertureValue = "'" + "##" + "'";
        //                getitem = imagelist.Count;
        //                string name = item.Title + item.ActFileExtension;
        //                if (name != "Thumbs.db")
        //                {
        //                    string picname = name;
        //                    s += ", " + picname;
        //                    try
        //                    {
        //                        // exif = new ExifTagCollection(Path.Combine(item.ImagePath, item.Title) + FileExt);
        //                        exif = new ExifTagCollection(Path.Combine(item.ImagePath, item.Title) + item.ActFileExtension);

        //                        #region Auto-Rotate
        //                        //Start:Code for Auto-Rotate
        //                        if ((bool)_IsAutoRotate)
        //                        {
        //                            //ExifReader reader = null;
        //                            string renderedTag = "";
        //                            try
        //                            {
        //                                using (var reader = new ExifReader(Path.Combine(item.ImagePath, item.Title) + item.ActFileExtension))
        //                                {
        //                                    foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
        //                                    {
        //                                        object val;
        //                                        if (reader.GetTagValue(tagID, out val))
        //                                        {
        //                                            renderedTag = val.ToString();
        //                                        }
        //                                    }
        //                                }

        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                item.IsCorrupt = true;
        //                                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
        //                            }

        //                            if (!string.IsNullOrEmpty(renderedTag))
        //                            {
        //                                _needRotation = GetRotationValue(renderedTag);
        //                            }
        //                            else
        //                            {
        //                                _needRotation = 0;
        //                            }
        //                        }
        //                        //End:Code for Auto-Rotate
        //                        #endregion

        //                        if (exif.Where(o => o.Id == 36867).Count() > 0)
        //                        {
        //                            try
        //                            {
        //                                System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
        //                                objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
        //                                string datePart = exif[36867].Value.Split(' ').First();
        //                                datePart = datePart.Replace(':', '/');
        //                                string timePart = exif[36867].Value.Split(' ').Last();
        //                                CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                CaptureDate = null;
        //                                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
        //                                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
        //                            }
        //                        }
        //                        else
        //                            CaptureDate = null;

        //                        foreach (LevDan.Exif.ExifTag tag in exif)
        //                        {
        //                            if (tag.Id == 271)
        //                            {
        //                                CameraManufacture = "'" + exif[271].Value + "'";
        //                            }
        //                            else if (tag.Id == 272)
        //                            {
        //                                CameraModel = "'" + exif[272].Value + "'";
        //                            }
        //                            else if (tag.Id == 274)
        //                            {
        //                                orientation = "'" + exif[274].Value + "'";
        //                            }
        //                            else if (tag.Id == 282)
        //                            {
        //                                HorizontalResolution = "'" + exif[282].Value + "'";
        //                            }
        //                            else if (tag.Id == 283)
        //                            {
        //                                VerticalResolution = "'" + exif[283].Value + "'";
        //                            }
        //                            else if (tag.Id == 36867)
        //                            {
        //                                Datetaken = "'" + exif[36867].Value + "'";
        //                            }
        //                            else if (tag.Id == 40962 || tag.Id == 40963)
        //                            {
        //                                dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
        //                            }
        //                            else if (tag.Id == 34855)
        //                            {
        //                                ISOSpeedRatings = "'" + exif[34855].Value + "'";
        //                            }
        //                            else if (tag.Id == 41986)
        //                            {
        //                                ExposureMode = "'" + exif[41986].Value + "'";
        //                            }
        //                            else if (tag.Id == 41994)
        //                            {
        //                                Sharpness = "'" + exif[41994].Value + "'";
        //                            }
        //                            else if (tag.Id == 2)
        //                            {
        //                                GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
        //                            }
        //                            else if (tag.Id == 4)
        //                            {
        //                                GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
        //                            }
        //                            else if (tag.Id == 33434)
        //                            {
        //                                ExposureTime = "'" + exif[33434].Value + "'";
        //                            }
        //                            else if (tag.Id == 37381)
        //                            {
        //                                ApertureValue = "'" + exif[37381].Value + "'";
        //                            }
        //                        }
        //                        ImageMetaData = String.Format(_metaDataFormat, dimension, CameraManufacture,
        //                                                      HorizontalResolution, VerticalResolution, CameraModel,
        //                                                      ISOSpeedRatings, Datetaken, ExposureMode, Sharpness,
        //                                                      orientation, GPSLatitude, GPSLongitude, ExposureTime, ApertureValue);

        //                        //ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + "></image>";
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
        //                        ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);

        //                    }
        //                    finally { exif = null; }

        //                    PhotoBusiness photoBiz = new PhotoBusiness();
        //                    int photoCount = photoBiz.PhotoCountCurrentDate(objInfo.PhotoNo.ToString(), objInfo.PhotographerId, 1);
        //                    DateTime serverDate = (new CustomBusineses()).ServerDateTime();
        //                    string filename = string.Empty;
        //                    string title = imagelist.Select(x => x.Title).FirstOrDefault();
        //                    if (!title.Contains('#'))
        //                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
        //                                        + "_" + objInfo.PhotographerId.ToString() + item.ActFileExtension;
        //                    else
        //                    {
        //                        filename = serverDate.Day.ToString() + serverDate.Month.ToString() + objInfo.PhotoNo.ToString()
        //                                   + "_" + objInfo.PhotographerId.ToString() + "_" + title.Split('#')[1] + item.ActFileExtension;
        //                        _isVideoProcessed = true;
        //                    }
        //                    if (photoCount > 0)
        //                        filename = Path.GetFileNameWithoutExtension(filename) + "(" + photoCount + ")" + item.ActFileExtension;

        //                    //End new config data get
        //                    if (string.IsNullOrWhiteSpace(_defaultBrightness))
        //                        _defaultBrightness = "0";
        //                    if (string.IsNullOrWhiteSpace(_defaultContrast))
        //                        _defaultContrast = "1";
        //                    DefaultEffects = String.Format(_defaultEffects, _defaultBrightness, _defaultContrast);
        //                    if (item.SettingStatus == SettingStatus.SpecUpdated)
        //                    {
        //                        IsSpecSettingsUpdated = true;
        //                        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        //                        xmlDoc.Load(Path.Combine(item.ImagePath, item.Title + ".xml"));
        //                        System.Xml.XmlNode effectsNode = xmlDoc.GetElementsByTagName("Effects")[0];
        //                        DefaultEffects = effectsNode.InnerText;
        //                        System.Xml.XmlNode layringNode = xmlDoc.GetElementsByTagName("Layering")[0];
        //                        if (layringNode.InnerText == "NULL" || String.IsNullOrEmpty(layringNode.InnerText))
        //                            _photoLayer = "test";
        //                        else
        //                            _photoLayer = layringNode.InnerText;
        //                    }
        //                    int isImageProcessed = 1;
        //                    bool isCroped = false;
        //                    if (IsSpecSettingsUpdated)
        //                        isImageProcessed = 0;
        //                    string cropedImagePath = Path.Combine(item.ImagePath, "Croped", picname);
        //                    if (File.Exists(cropedImagePath))
        //                    {
        //                        isCroped = true;
        //                        File.Move(cropedImagePath, Path.Combine(_filePath, "Croped", filename));
        //                    }


        //                    //from here needs to put data 


        //                    if (photoBiz.CheckPhotos(filename, objInfo.PhotographerId))
        //                    {
        //                        if (_lstSemiSettings != null && _lstSemiSettings.Count != 0)
        //                        {
        //                            _lstSemiSettingsLoc =
        //                                    _lstSemiSettings.Where(x => x.DG_LocationId == objInfo.LocationId).ToList();
        //                        }

        //                        if (_lstSemiSettingsLoc == null || _lstSemiSettingsLoc.Count == 0)
        //                        {
        //                            _isResetDPIRequired = (new CameraBusiness()).CheckResetDPIRequired(HorizontalResolution,
        //                                                        VerticalResolution, objInfo.PhotographerId);

        //                            GetNewConfigLocationValues(objInfo.SubStoreId, objInfo.LocationId);

        //                            int PhotoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename,
        //                                                    serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
        //                                                    objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
        //                                                    objInfo.CharacterId, 0, _isVideoProcessed, 1, -1, filename, 0, isCroped);
        //                            _photoLayer = null;

        //                            if (IsMobileRfidActive)
        //                            {
        //                                MobileRfidAssociationCheck(PhotoId, item.Title);
        //                            }
        //                            if (_IsAutoRotate.HasValue && _IsAutoRotate.Value == true)
        //                            {
        //                                //if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(".jpg", ".xml"))))
        //                                if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(jpgExtension, ".xml").Replace(pngExtension, ".xml"))))
        //                                {
        //                                    rotateImage(Path.Combine(item.ImagePath, picname));
        //                                }
        //                            }
        //                            if (_isResetDPIRequired)
        //                                ResetLowDPI(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
        //                            else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
        //                                AutoCorrectImageWIthCopy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
        //                            else
        //                                File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename), true);

        //                            SemiOrderSettings objDG_SemiOrder_Settings = null;
        //                            if (_lstSemiSettingsLoc != null && _lstSemiSettingsLoc.Count > 0)
        //                            {
        //                                objDG_SemiOrder_Settings =
        //                                    _lstSemiSettings.Where(x => x.DG_LocationId == objInfo.LocationId).FirstOrDefault();
        //                            }
        //                            if (_is_SemiOrder && objDG_SemiOrder_Settings != null)
        //                            {
        //                                SaveSpecPrintEffectsIntoDb(PhotoId, objInfo.SubStoreId, objInfo.LocationId,
        //                                                            objDG_SemiOrder_Settings, _filePathDate, filename);
        //                            }
        //                            else
        //                            {
        //                                // Code by Suraj For Images Resize Logic for without spec printing.
        //                                //ResizeWPFImage(Path.Combine(_filePathDate, filename), 210, Path.Combine(_thumbnailFilePathDate, filename),Path.Combine(_filePathDate, filename));
        //                                //ResizeWPFImage(Path.Combine(_filePathDate, filename), 1200, Path.Combine(_bigThumbnailFilePathDate, filename));
        //                                ResizeImages(0.2, Path.Combine(_filePathDate, filename), Path.Combine(_thumbnailFilePathDate, filename));
        //                                ResizeImages(0.6, Path.Combine(_filePathDate, filename), Path.Combine(_bigThumbnailFilePathDate, filename));
        //                                ResizeImages(0.6, Path.Combine(_filePathDate, filename), Path.Combine(_minifiedFilePathDate, "m_" + filename));
        //                            }

        //                            _isResetDPIRequired = false;
        //                            //Interlocked.Add(ref CountImagesDownload, 1);

        //                            Interlocked.Add(ref ImagesDownloadSequence, 1);

        //                            //CountImagesDownload = CountImagesDownload + 1;

        //                            //Interlocked.Add(ref PhotoNo, 1);
        //                            //objInfo.PhotoNo = objInfo.PhotoNo + 1;
        //                            if (fileInfo.Exists)
        //                            {
        //                                if (fileInfo.IsReadOnly)
        //                                    File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
        //                                try
        //                                {
        //                                    fileInfo.Delete();
        //                                    string settngsXmlPath = (fileInfo.FullName).Replace(fileInfo.Extension, ".xml");
        //                                    if (File.Exists(settngsXmlPath))
        //                                        File.Delete(settngsXmlPath);
        //                                }
        //                                catch { }

        //                            }
        //                        }
        //                        else
        //                        {

        //                            foreach (SemiOrderSettings objDG_SemiOrder_Settings in _lstSemiSettingsLoc)
        //                            {
        //                                imagenameCntr = imagenameCntr + 1;
        //                                if (ParentImageID == -1)
        //                                {
        //                                    string filenametobesaved = filename;
        //                                    OriginalFileName = filename;
        //                                    photoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filenametobesaved,
        //                                                     serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
        //                                                     objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
        //                                                     objInfo.CharacterId, 0, _isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, isCroped);

        //                                    //photoId = photoBiz.SetPhotoDetails(substoreId, filename, picname.ToString() + "_" + imagenameCntr, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), ParentImageID, OriginalFileName);
        //                                    ParentImageID = photoId;
        //                                    _photoLayer = null;
        //                                    DefaultEffects = String.Format(_defaultEffects, _defaultBrightness, _defaultContrast);
        //                                }
        //                                else
        //                                {
        //                                    // photoId = phBusiness.SetPhotoDetails(substoreId, filename, picname.ToString() + "_" + imagenameCntr, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), ParentImageID, OriginalFileName);
        //                                    string filenametobesaved = filename;//.Substring(0, filename.Length - 4) + "_" + imagenameCntr + ".jpg";
        //                                    photoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filenametobesaved,
        //                                                       serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
        //                                                       objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, isImageProcessed,
        //                                                       objInfo.CharacterId, 0, _isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, isCroped);

        //                                    _photoLayer = null;
        //                                }
        //                                _isResetDPIRequired = (new CameraBusiness()).CheckResetDPIRequired(HorizontalResolution,
        //                                                            VerticalResolution, objInfo.PhotographerId);

        //                                //int PhotoId = photoBiz.SetPhotoDetails(objInfo.SubStoreId, objInfo.PhotoNo.ToString(), filename,
        //                                //                       serverDate, objInfo.PhotographerId.ToString(), ImageMetaData,
        //                                //                       objInfo.LocationId, _photoLayer, DefaultEffects, CaptureDate, _rfidScanType, 1,
        //                                //                       objInfo.CharacterId);

        //                                GetNewConfigLocationValues(objInfo.SubStoreId, objInfo.LocationId);



        //                                if (IsMobileRfidActive)
        //                                {
        //                                    MobileRfidAssociationCheck(photoId, item.Title);
        //                                }
        //                                if (_IsAutoRotate.HasValue && _IsAutoRotate.Value == true)
        //                                {
        //                                    if (!File.Exists(Path.Combine(item.ImagePath, picname.Replace(jpgExtension, ".xml").Replace(pngExtension, ".xml"))))
        //                                    {
        //                                        rotateImage(Path.Combine(item.ImagePath, picname));
        //                                    }
        //                                }
        //                                if (_isResetDPIRequired)
        //                                    ResetLowDPI(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
        //                                else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
        //                                    AutoCorrectImageWIthCopy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));
        //                                else
        //                                    File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filePathDate, filename));

        //                                if (_is_SemiOrder && objDG_SemiOrder_Settings != null)
        //                                {
        //                                    SaveSpecPrintEffectsIntoDb(photoId, objInfo.SubStoreId, objInfo.LocationId,
        //                                                                objDG_SemiOrder_Settings, _filePathDate, filename);
        //                                }
        //                                else
        //                                {
        //                                    ResizeWPFImage(Path.Combine(_filePathDate, filename), 210, Path.Combine(_thumbnailFilePathDate, filename),
        //                                                                            Path.Combine(_filePathDate, filename));
        //                                    ResizeWPFImage(Path.Combine(_filePathDate, filename), 1200, Path.Combine(_bigThumbnailFilePathDate, filename));
        //                                }

        //                                _isResetDPIRequired = false;
        //                                //Interlocked.Add(ref CountImagesDownload, 1);

        //                                Interlocked.Add(ref ImagesDownloadSequence, 1);

        //                                //CountImagesDownload = CountImagesDownload + 1;

        //                                //Interlocked.Add(ref PhotoNo, 1);
        //                                //objInfo.PhotoNo = objInfo.PhotoNo + 1;

        //                                filename = filename.Substring(0, filename.Length - 4) + "_" + imagenameCntr + jpgExtension;
        //                            }
        //                            //_photoLayer = null;
        //                        }
        //                        if (fileInfo.Exists)
        //                        {
        //                            if (fileInfo.IsReadOnly)
        //                                File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
        //                            try
        //                            {
        //                                fileInfo.Delete();
        //                                string settngsXmlPath = (fileInfo.FullName).Replace(fileInfo.Extension, ".xml");
        //                                if (File.Exists(settngsXmlPath))
        //                                    File.Delete(settngsXmlPath);
        //                            }
        //                            catch { }
        //                        }
        //                        ParentImageID = -1;
        //                        OriginalFileName = string.Empty;
        //                        imagenameCntr = 0;

        //                    }
        //                    else
        //                    {
        //                        //MessageBox.Show(PhotoNo + " already exists for today");
        //                        //_logger.LogException(objInfo.PhotoNo + " already exists for today",DigiPhoto.LogEnvelop.Data.LoggingLevel.Info);
        //                        ErrorHandler.ErrorHandler.LogFileWrite(objInfo.PhotoNo + " already exists for today");
        //                        if (fileInfo.IsReadOnly)
        //                            File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
        //                        if (fileInfo.Exists)
        //                        {
        //                            if (fileInfo.IsReadOnly)
        //                                File.SetAttributes(fileInfo.FullName, FileAttributes.Normal);
        //                            if (File.Exists(_networkBackupPath + "\\" + fileInfo.Name))
        //                            {
        //                                try
        //                                {
        //                                    File.Delete(_networkBackupPath + "\\" + fileInfo.Name);
        //                                    fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
        //                                }
        //                                catch { }
        //                            }
        //                            else
        //                            {
        //                                fileInfo.MoveTo(_networkBackupPath + "\\" + fileInfo.Name);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            catch (IOException IOex)
        //            {
        //                string errorMessage = "Got IO Exception after Archived movement for file " + fileInfo.FullName;
        //                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //                FilesToDelete.Add(fileInfo.FullName.ToLower());
        //                for (int j = 0; j < FilesToDelete.Count; j++)
        //                {
        //                    if (File.Exists(FilesToDelete[j]))
        //                    {
        //                        try
        //                        {
        //                            File.Delete(FilesToDelete[j]);
        //                            FilesToDelete.RemoveAt(j);
        //                        }
        //                        catch { }
        //                    }
        //                }

        //            }
        //            catch (Exception ex)
        //            {
        //                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
        //                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
        //            }

        //        }
        //        catch (Exception x)
        //        {
        //            ErrorHandler.ErrorHandler.LogFileWrite(x.Message);
        //        }
        //    }
        //}
        #endregion
        /// <summary>
        /// Gets the rotation value.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;
                default:
                    return 0;
            }
        }

        private string ConvertDegreeAngleToDouble(string degrees, string minutes, string seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return (degrees.ToDouble() + (minutes.ToDouble() / 60) + (seconds.ToDouble() / 3600)).ToString();
        }

        private void AutoCorrectImageWIthCopy(string InputFilePath, string OutputFilePath)
        {
            try
            {
                //System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);

                using (FileStream fs = File.OpenRead(InputFilePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fs.CopyTo(ms);
                        //newImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (MagickImage image = new MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void ResetLowDPI(string InputFilePath, string OutputFilePath)
        {
            try
            {
                System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);
                System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);

                if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        outImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (MagickImage image = new MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
                else
                {
                    outImage.Save(OutputFilePath);
                }
                newImage.Dispose();
                outImage.Dispose();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void rotateImage(string rotatePath)
        {
            try
            {

                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                if (_needRotation > 0)
                {
                    if (_needRotation == 90)
                    {
                        object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                    else if (_needRotation == 180)
                    {
                        object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                    else if (_needRotation == 270)
                    {
                        object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                }

                jmagic = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private bool CheckResetDPIRequired(string HR, string VR, string PhotographerId)
        {
            try
            {
                int hr = Convert.ToInt32(HR.Replace("'", ""));
                int vr = Convert.ToInt32(VR.Replace("'", ""));
                //bool cameraSetting = _objDataLayer.GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                bool cameraSetting = (new CameraBusiness()).GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                if ((hr != 300 || vr != 300) && cameraSetting)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private void RotateImageIfRequired(string sourceImage)
        {
            try
            {
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    fileStream.Close();
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                    if (_needRotation > 0)
                    {
                        if (_needRotation == 0)
                        {
                            bitmapImage.Rotation = Rotation.Rotate0;
                            _isrotated = false;
                        }
                        else if (_needRotation == 90)
                        {
                            bitmapImage.Rotation = Rotation.Rotate90;

                            object[] o = new object[] { "-rotate", " 90 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            _isrotated = true;
                        }
                        else if (_needRotation == 180)
                        {
                            bitmapImage.Rotation = Rotation.Rotate180;

                            object[] o = new object[] { "-rotate", " 180 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            _isrotated = false;
                        }
                        else if (_needRotation == 270)
                        {
                            bitmapImage.Rotation = Rotation.Rotate270;

                            object[] o = new object[] { "-rotate", " 270 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            _isrotated = true;
                        }
                    }

                    jmagic = null;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        /// <param name="rotatePath">The rotate path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath, string rotatePath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.CreateOptions = BitmapCreateOptions.PreservePixelFormat | BitmapCreateOptions.IgnoreColorProfile;
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.CreateOptions = BitmapCreateOptions.PreservePixelFormat | BitmapCreateOptions.IgnoreColorProfile;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    #region Commented code for image rotation
                    //ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                    //if (_needRotation > 0)
                    //{
                    //    if (_needRotation == 0)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate0;
                    //        _isrotated = false;
                    //    }
                    //    else if (_needRotation == 90)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate90;

                    //        object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                    //        jmagic.Mogrify(o);
                    //        o = null;
                    //        _isrotated = true;
                    //    }
                    //    else if (_needRotation == 180)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate180;

                    //        object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                    //        jmagic.Mogrify(o);
                    //        o = null;
                    //        _isrotated = false;
                    //    }
                    //    else if (_needRotation == 270)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate270;

                    //        object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                    //        jmagic.Mogrify(o);
                    //        o = null;
                    //        _isrotated = true;
                    //    }
                    //}

                    //jmagic = null;
                    #endregion

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);

            }
        }

        #region Minified Images Creation.   // Suraj
        /// <summary>
        /// This Function used for the get Different Resize images configuration.
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <returns></returns>
        private bool ResizeImages(double scaleFactor, string sourcePath, string targetPath)
        {
            try
            {
                CompressToImage(scaleFactor, sourcePath, targetPath);
                return false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("ResizeImages:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                return true;//for corrupted image
            }
            finally
            {

            }
        }
        /// <summary>
        /// This Function used for the to create Resize Images
        /// </summary>
        /// <param name="scaleFactor"></param>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        private bool CompressToImage(double scaleFactor, string sourcePath, string targetPath)
        {
            try
            {
                using (FileStream fileStream = File.OpenRead(sourcePath))
                {
                    using (var image = System.Drawing.Image.FromStream(fileStream))
                    {
                        var newWidth = (int)(image.Width * scaleFactor);
                        var newHeight = (int)(image.Height * scaleFactor);
                        var thumbnailImg = new System.Drawing.Bitmap(newWidth, newHeight);
                        var thumbGraph = System.Drawing.Graphics.FromImage(thumbnailImg);
                        thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imageRectangle);
                        thumbnailImg.Save(targetPath, image.RawFormat);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("CompressToImage:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                return true;//for corrupted image
            }
            finally
            {

            }
        }
        #endregion

        private void CreateThumbnail(string filepath, string filepathdate, string filename)
        {
            try
            {
                decimal ratio = 0;
                MemoryStream ms = null;
                using (FileStream fileStream = File.OpenRead(Path.Combine(filepathdate, filename).ToString()))
                {
                    ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                }
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.CreateOptions = BitmapCreateOptions.PreservePixelFormat | BitmapCreateOptions.IgnoreColorProfile;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.StreamSource = ms;
                bi.EndInit();
                bi.Freeze();
                if (bi.Width >= bi.Height)
                {
                    ratio = bi.Width.ToDecimal() / bi.Height.ToDecimal();
                }
                else
                {
                    ratio = bi.Height.ToDecimal() / bi.Width.ToDecimal();
                }

                ResizeAndSave(210, ratio, filepath, filename, filepath + "\\Thumbnails\\" + filename, bi, ms, true);
                ResizeAndSave(900, ratio, filepath, filename, filepath + "\\Thumbnails_Big\\" + filename, bi, ms, false);
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void ResizeAndSave(int maxHeight, decimal ratio, string filepath, string filename, string saveToPath, BitmapImage bi, MemoryStream ms, bool needRotation)
        {

            BitmapImage bitmapImage = new BitmapImage();
            int newWidth = 0;
            int newHeight = 0;

            if (bi.Width >= bi.Height)
            {
                newWidth = maxHeight;
                newHeight = (maxHeight / ratio).ToInt32();
            }
            else
            {
                newHeight = maxHeight;
                newWidth = (maxHeight / ratio).ToInt32();
            }
            ms.Seek(0, SeekOrigin.Begin);

            bitmapImage.BeginInit();
            bitmapImage.CreateOptions = BitmapCreateOptions.PreservePixelFormat | BitmapCreateOptions.IgnoreColorProfile;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.StreamSource = ms;
            bitmapImage.DecodePixelWidth = newWidth;
            bitmapImage.DecodePixelHeight = newHeight;
            if (needRotation)
            {
                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();
                if (_needRotation > 0)
                {
                    if (_needRotation == 0)
                    {
                        bitmapImage.Rotation = Rotation.Rotate0;
                        _isrotated = false;
                    }
                    else if (_needRotation == 90)
                    {
                        bitmapImage.Rotation = Rotation.Rotate90;
                        object[] o = new object[] { "-rotate", " 90 ", Path.Combine(_filePathDate, filename) };
                        jmagic.Mogrify(o);
                        o = null;
                        _isrotated = true;
                    }
                    else if (_needRotation == 180)
                    {
                        bitmapImage.Rotation = Rotation.Rotate180;
                        object[] o = new object[] { "-rotate", " 180 ", Path.Combine(_filePathDate, filename) };
                        jmagic.Mogrify(o);
                        o = null;
                        _isrotated = false;
                    }
                    else if (_needRotation == 270)
                    {
                        bitmapImage.Rotation = Rotation.Rotate270;
                        object[] o = new object[] { "-rotate", " 270 ", Path.Combine(_filePathDate, filename) };
                        jmagic.Mogrify(o);
                        o = null;
                        _isrotated = true;
                    }
                }

                jmagic = null;
            }

            bitmapImage.EndInit();
            bitmapImage.Freeze();

            using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 94;
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                encoder.Save(fileStreamForSave);
                fileStreamForSave.Close();
            }
        }
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            BitmapImage bi = new BitmapImage();
            BitmapImage bitmapImage = new BitmapImage();
            try
            {
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            finally
            {
                bi = null;
                bitmapImage = null;
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Applies the settings.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="photoname">The photoname.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        //private void ApplySettings(string path, string filename, string photoname, int photoId, int substoreId)
        private void SaveSpecPrintEffectsIntoDb(int photoId, int substoreId, int locationId, SemiOrderSettings objDG_SemiOrder_Settings, string dateFolderPath, string fileName)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            PhotoBusiness phBiz = new PhotoBusiness();
            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright == false)
            {
                if (!string.IsNullOrEmpty(_defaultBrightness))
                    objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(_defaultBrightness);
                else
                    objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = 0.0;
            }

            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast == false)
            {
                if (!string.IsNullOrEmpty(_defaultContrast))
                    objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(_defaultContrast);
                else
                    objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = 1;
            }

            if (!IsSpecSettingsUpdated)
            {
                SaveXml(objDG_SemiOrder_Settings, photoId, dateFolderPath, fileName);
            }
            else
            {
                SetQRForManualEditCase(objDG_SemiOrder_Settings, photoId);
            }
            IsSpecSettingsUpdated = false;
            if (!IsCodeType(photoId))
            {
                if (_is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                {
                    StoreSubStoreDataBusniess objStoreBiz = new StoreSubStoreDataBusniess();
                    StoreInfo objStore = objStoreBiz.SelectStores().FirstOrDefault();
                    string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');
                    foreach (string prodList in prdIdList)
                    {
                        string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                        int OrderDetailId = (new OrderBusiness()).AddSemiOrderDetails(photoId, prodList, locationId, SyncCode, false, substoreId);
                    }
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <param name="bvalue">The bvalue.</param>
        /// <param name="childnode">if set to <c>true</c> [childnode].</param>
        /// <param name="rfid">The rfid.</param>
        /// <param name="cvalue">The cvalue.</param>
        /// <param name="fvalue">The fvalue.</param>
        /// <param name="fvvalue">The fvvalue.</param>
        /// <param name="BG">The debug.</param>
        /// <param name="glayer">The glayer.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        /// <param name="ZoomDetails">The zoom details.</param>
        public void SaveXml(string bvalue, string cvalue, string fvalue, string fvvalue, string BG, string glayer, int photoId, string ZoomDetails, bool isCropActive, int ProductId, string filedatepath)
        {

            string layeringdata = "";
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(DefaultEffects);

            System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("brightness", bvalue);
            }

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("contrast", cvalue);
            }

            if (isCropActive)
            {
                foreach (System.Xml.XmlElement XElement in list)
                {
                    if (ProductId == 1)
                        XElement.SetAttribute("Crop", "6 * 8");
                    else if (ProductId == 2 || ProductId == 3)
                        XElement.SetAttribute("Crop", "8 * 10");
                    else if (ProductId == 30 || ProductId == 5)
                        XElement.SetAttribute("Crop", "4 * 6");
                    else if (ProductId == 98)
                        XElement.SetAttribute("Crop", "3 * 3");
                }
            }
            PhotoBusiness photoBiz = new PhotoBusiness();
            string fname = string.Empty;
            BitmapImage _objPhoto = GetBitmapImageFromPath(Path.Combine(filedatepath, photoBiz.GetFileNameByPhotoID(Convert.ToString(photoId))));
            if (_objPhoto.Height > _objPhoto.Width)
                fname = fvvalue;
            else
                fname = fvalue;

            string[] zoomdetails = ZoomDetails.Split(',');
            layeringdata = String.Format(_layeringdata, zoomdetails[0], fname, BG, zoomdetails[1], zoomdetails[2], zoomdetails[3], zoomdetails[4], glayer);
            photoBiz.UpdateLayeringForSpecPrint(photoId, layeringdata);
            DefaultEffects = Xdocument.InnerXml.ToString();
            photoBiz.SetEffectsonPhoto(DefaultEffects, photoId, false);

        }

        public void SetQRForManualEditCase(SemiOrderSettings semiOrderSettings, int photoId)
        {
            PhotoBusiness photoBiz = new PhotoBusiness();
            _QRCode = null;
            int productId = 0;
            if (semiOrderSettings.DG_SemiOrder_ProductTypeId.Contains(','))
                productId = Convert.ToInt32(semiOrderSettings.DG_SemiOrder_ProductTypeId.Split(',')[0]);
            else
                productId = Convert.ToInt32(semiOrderSettings.DG_SemiOrder_ProductTypeId);

            if (productId == 104 || semiOrderSettings.ProductName.Contains("(4x6) & QR"))
                _QRCode = DigiPhoto.GenerateQRCode.GetNextQRCode(_QRCodeLenth);

            photoBiz.UpdateOnlineQRCodeForPhoto(photoId, _QRCode);
        }

        public void SaveXml(SemiOrderSettings semiOrderSettings, int photoId, string filedatepath, string fileName)
        {
            _QRCode = null;
            int productId = 0;
            if (semiOrderSettings.DG_SemiOrder_ProductTypeId.Contains(','))
                productId = 1;
            else
                productId = Convert.ToInt32(semiOrderSettings.DG_SemiOrder_ProductTypeId);
            //int productId = (Int32)semiOrderSettings.DG_SemiOrder_ProductTypeId;
            string layeringdata = "";
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(DefaultEffects);

            System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("brightness", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoBright_Value));
            }

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("contrast", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoContrast_Value));
            }

            if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean())
            {
                foreach (System.Xml.XmlElement XElement in list)
                {
                    if (productId == 1)
                        XElement.SetAttribute("Crop", "6 * 8");
                    else if (productId == 2 || productId == 3)
                        XElement.SetAttribute("Crop", "8 * 10");
                    else if (productId == 30 || productId == 5 || productId == 104)
                        XElement.SetAttribute("Crop", "4 * 6");
                    else if (productId == 98)
                        XElement.SetAttribute("Crop", "3 * 3");
                    else if (productId == 120)
                        XElement.SetAttribute("Crop", "8 * 24");
                    else if (productId == 121)
                        XElement.SetAttribute("Crop", "8 * 26");
                    else if (productId == 122)
                        XElement.SetAttribute("Crop", "8 * 18");
                    else if (productId == 123)
                        XElement.SetAttribute("Crop", "6 * 14");
                    else if (productId == 125)
                        XElement.SetAttribute("Crop", "6 * 20");


                }
            }

            if (semiOrderSettings.ProductName.Contains("6 * 8"))
                ProductNamePreference = "1";
            else if (semiOrderSettings.ProductName.Contains("8 * 10"))
                ProductNamePreference = "2";
            else if (semiOrderSettings.ProductName.Contains("4 * 6") || semiOrderSettings.ProductName.Contains("(4x6) & QR") || semiOrderSettings.ProductName.Contains("4 Small Wallets"))
                ProductNamePreference = "30";


            else if (semiOrderSettings.ProductName.Contains("6 * 20"))
                ProductNamePreference = "125";
            else if (semiOrderSettings.ProductName.Contains("6 * 14"))
                ProductNamePreference = "123";
            else if (semiOrderSettings.ProductName.Contains("8 * 18"))
                ProductNamePreference = "122";
            else if (semiOrderSettings.ProductName.Contains("8 * 26"))
                ProductNamePreference = "121";
            else if (semiOrderSettings.ProductName.Contains("8 * 24"))
                ProductNamePreference = "120";

            else
                ProductNamePreference = "2";

            PhotoBusiness photoBiz = new PhotoBusiness();
            string fname = string.Empty;
            string[] zoomdetails;
            string graphicLayer = string.Empty;
            string textLogo = string.Empty;
            string background = string.Empty;
            bool IsCropActive = false;
            bool IsChromaActive = false;
            BitmapImage _objPhoto = GetBitmapImageFromPath(Path.Combine(filedatepath, fileName));
            if (_objPhoto.Height > _objPhoto.Width)
            {
                fname = Convert.ToString(semiOrderSettings.ImageFrame_Vertical);
                zoomdetails = semiOrderSettings.ZoomInfo_Vertical.Split(',');
                graphicLayer = semiOrderSettings.Graphics_layer_Vertical;
                textLogo = semiOrderSettings.TextLogo_Vertical;
                background = semiOrderSettings.Background_Vertical;
                if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                    IsChromaActive = true;

                if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                    IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.VerticalCropValues) == true ? false : true;
            }
            else
            {
                fname = Convert.ToString(semiOrderSettings.ImageFrame_Horizontal);
                zoomdetails = semiOrderSettings.ZoomInfo_Horizontal.Split(',');
                graphicLayer = semiOrderSettings.Graphics_layer_Horizontal;
                textLogo = semiOrderSettings.TextLogo_Horizontal;
                background = semiOrderSettings.Background_Horizontal;
                if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                    IsChromaActive = true;
                if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                    IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.HorizontalCropValues) == true ? false : true;
            }
            string zoomFactor = "1";
            string canvasleft = "0";
            string canvastop = "0";
            string scalecentrex = "-1";
            string scalecentrey = "-1";
            // Vertical Layering Effects
            if (zoomdetails.Count() > 1)
            {
                zoomFactor = zoomdetails[0];
                canvasleft = zoomdetails[1];
                canvastop = zoomdetails[2];
                scalecentrex = zoomdetails[3];
                scalecentrey = zoomdetails[4];
            }

            layeringdata = String.Format(_layeringdata, zoomFactor, fname, background, canvasleft, canvastop, scalecentrex, scalecentrey, graphicLayer, textLogo, ProductNamePreference);
            //photoBiz.UpdateLayeringForSpecPrint(photoId, layeringdata);
            DefaultEffects = Xdocument.InnerXml.ToString();
            // photoBiz.SetEffectsonPhoto(DefaultEffects, photoId);
            if (productId == 104 || semiOrderSettings.ProductName.Contains("(4x6) & QR"))
                _QRCode = DigiPhoto.GenerateQRCode.GetNextQRCode(_QRCodeLenth);
            photoBiz.UpdateEffectsSpecPrint(photoId, layeringdata, DefaultEffects, IsChromaActive, IsCropActive, false, "", _QRCode);
        }

        private Int32 QRCodeLength(Int32 subStoreId)
        {
            List<long> filterValues = new List<long>();
            filterValues.Add((Int32)ConfigParams.QRCodeLengthSetting);
            ConfigBusiness conBiz = new ConfigBusiness();
            iMIXConfigurationInfo ConfigValuesList = conBiz.GetNewConfigValues(subStoreId).Where(o => o.IMIXConfigurationMasterId == (Int32)ConfigParams.QRCodeLengthSetting).FirstOrDefault();
            if (ConfigValuesList != null)
                _QRCodeLenth = ConfigValuesList.ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList.ConfigurationValue) : 0;
            return _QRCodeLenth;
        }

        private bool IsCodeType(int PhotoId)
        {
            return (new PhotoBusiness()).CheckIsCodeType(PhotoId);
        }

        /// <summary>
        /// Gets the bitmap image from path.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public BitmapImage GetBitmapImageFromPath(string value)
        {
            BitmapImage bi = new BitmapImage();
            try
            {
                if (value != null)
                {
                    using (FileStream fileStream = File.OpenRead(value.ToString()))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                        }
                    }
                }
                else
                {
                    bi = new BitmapImage();
                }
                return bi;
            }
            catch (Exception ex)
            {
                return bi;
            }
        }
        private void UpdateDataAfterProcessing()
        {
            //bool isUpdated = (new ManualDownloadBusiness()).UpdateData(_objInfo);
            throw new NotImplementedException();
        }

        private void MobileRfidAssociationCheck(int PhtoId, string imageName)
        {
            try
            {
                string tagId = string.Empty;
                if (imageName.Split('_').Length > 6)
                    tagId = imageName.Split('_')[6];
                if (!string.IsNullOrEmpty(tagId))
                {
                    tagId = "2222" + tagId;
                    new AssociateImageBusiness().AssociateMobileImage(tagId, PhtoId);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetNewConfigLocationValues(int subStoreId, int locationId)
        {
            IsMobileRfidActive = false;
            //Start Author Bhavin Udani. Created on 16 Feb 2021. Manual Download Error in Vertical Spec Print 
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(locationId, subStoreId);
            try
            {
                foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
                {

                    //Start Author Bhavin Udani 19 Feb 2021 Changes for SpecPrinting not working from manual download flow
                    //Changes: Code revert to previous version as issue was in database
                    switch (_objNewConfig.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.IsMobileRfidEnabled:
                            IsMobileRfidActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsAutoColorCorrection://for auto color correction in images
                            IsAutoColorCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsCorrectAtDownloadActive:
                            IsCorrectionAtDownloadActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsContrastCorrection:
                            IsContrastCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsGammaCorrection:
                            IsGammaCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsNoiseReduction:
                            IsNoiseReductionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            break;
                        case (int)ConfigParams.IsAdvancedVideoEditActive:
                            bool IsAdvancedVideoEditActive;
                            IsAdvancedVideoEditActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);

                            if (IsAdvancedVideoEditActive != null && IsAdvancedVideoEditActive == true)
                            {
                                if (IsGreenScreenWorkFlow == true)
                                    _isVideoProcessed = true;
                                else
                                    _isVideoProcessed = false;
                            }
                            break;
                        default:
                            break;
                            //End Author Bhavin Udani. Created on 19 Feb 2021. Changes for SpecPrinting not working from manual download flow
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                //throw;
            }




        }

    }


}
