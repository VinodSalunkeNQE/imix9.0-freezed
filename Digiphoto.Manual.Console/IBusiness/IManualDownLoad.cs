﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digiphoto.ManualDownload.Console.IBusiness
{
    public interface IManualDownLoad
    {

        ManualDownloadInfo GetManualDownloadInfo();

        bool SaveManualDownloadInfo();

        bool CheckManualDownloadInfo();

        bool CheckResetDPIRequired(int photographerId);

        bool UpdateData(ManualDownloadInfo objInfo);
    }
}
