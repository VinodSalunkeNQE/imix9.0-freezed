﻿using DigiPhoto.Cache.DataCache;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Digiphoto.ManualDownload.Console
{
    class Program
    {
        /// <summary>
        /// Main Method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
#if DEBUG
            (new ManualDownLoad()).StartMaualDownloadProcess();
            ErrorHandler.ErrorHandler.LogFileWrite("Digiphoto.ManualDownload.Console service started");
#endif
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
            DataCacheFactory.Register();
            
            //while (true)
            //{
#if DEBUG
                var watch = FrameworkHelper.CommonUtility.Watch();
                watch.Start();
#endif

                (new ManualDownLoad()).StartMaualDownloadProcess();

                //System.Threading.Thread.Sleep(1000);
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop("Total" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            //}
        }

        /// <summary>
        /// Rasied when Unhandled Exception comes
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">UnhandledExceptionEventArgs</param>
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            ErrorHandler.ErrorHandler.LogFileWrite(e.ExceptionObject.ToString());
            Environment.Exit(1);
        }
        
    }
}
