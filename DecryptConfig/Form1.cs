﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace DecryptConfig
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnbrowseconfig_Click(object sender, EventArgs e)
        {
            Stream checkstream = null;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "EXE Files | *.exe*";

            ofd.ShowDialog();
            
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        txtpathconfig.Text = ofd.FileName.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please select an exe file");
                }
            
        }

        private void btndecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                UnprotectConnectionString();
                System.IO.StreamReader oStream = new System.IO.StreamReader(txtpathconfig.Text + ".config", System.Text.Encoding.UTF8);
                txtData.Text = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }



        }
        private void UnprotectConnectionString()
        {
            ToggleConnectionStringProtection(txtpathconfig.Text, false);
        }
        private  void ProtectConnectionString()
        {
            ToggleConnectionStringProtection(txtpathconfig.Text, true);
        }
        private static void ToggleConnectionStringProtection(string pathName, bool protect)
        {
            // Define the Dpapi provider name.
            string strProvider = "DataProtectionConfigurationProvider";
            // string strProvider = "RSAProtectedConfigurationProvider";

            System.Configuration.Configuration oConfiguration = null;
            System.Configuration.ConnectionStringsSection oSection = null;

            try
            {
                // Open the configuration file and retrieve the connectionStrings section.

                // For Web!
                // oConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

                // For Windows!
                // Takes the executable file name without the config extension.
                oConfiguration = System.Configuration.ConfigurationManager.OpenExeConfiguration(pathName);

                if (oConfiguration != null)
                {
                    bool blnChanged = false;
                    oSection = oConfiguration.GetSection("connectionStrings") as System.Configuration.ConnectionStringsSection;

                    if (oSection != null)
                    {
                        if ((!(oSection.ElementInformation.IsLocked)) && (!(oSection.SectionInformation.IsLocked)))
                        {
                            if (protect)
                            {
                                if (!(oSection.SectionInformation.IsProtected))
                                {
                                    blnChanged = true;

                                    // Encrypt the section.
                                    oSection.SectionInformation.ProtectSection(strProvider);
                                }
                            }
                            else
                            {
                                if (oSection.SectionInformation.IsProtected)
                                {
                                    blnChanged = true;

                                    // Remove encryption.
                                    oSection.SectionInformation.UnprotectSection();
                                }
                            }
                        }

                        if (blnChanged)
                        {
                            // Indicates whether the associated configuration section will be saved even if it has not been modified.
                            oSection.SectionInformation.ForceSave = true;

                            // Save the current configuration.
                            oConfiguration.Save();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        private void btnencrypt_Click(object sender, EventArgs e)
        {
            try
            {
                ProtectConnectionString();

                System.IO.StreamReader oStream = new System.IO.StreamReader(txtpathconfig.Text + ".config", System.Text.Encoding.UTF8);
                txtData.Text = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                    StreamWriter b = new StreamWriter(File.Open(txtpathconfig.Text + ".config", FileMode.Create));
                    b.Write(txtData.Text);
                    MessageBox.Show("Changes saved successfully");
                    b.Close();
                
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            
        }
    }
}
