﻿namespace DecryptConfig
{
    partial class SelectExes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxDigiPhoto = new System.Windows.Forms.CheckBox();
            this.checkBoxPrintConsole = new System.Windows.Forms.CheckBox();
            this.checkBoxWatcher = new System.Windows.Forms.CheckBox();
            this.checkBoxEmailApp = new System.Windows.Forms.CheckBox();
            this.checkBoxSyncService = new System.Windows.Forms.CheckBox();
            this.checkBoxRideCaptureUtility = new System.Windows.Forms.CheckBox();
            this.checkBoxRideCameraSetting = new System.Windows.Forms.CheckBox();
            this.checkBoxWifi = new System.Windows.Forms.CheckBox();
            this.checkBoxSyncApp = new System.Windows.Forms.CheckBox();
            this.checkBoxEmailService = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxUserID = new System.Windows.Forms.TextBox();
            this.textBoxInitialCatalog = new System.Windows.Forms.TextBox();
            this.textBoxDataSource = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxDigiPhoto
            // 
            this.checkBoxDigiPhoto.AutoSize = true;
            this.checkBoxDigiPhoto.Enabled = false;
            this.checkBoxDigiPhoto.Location = new System.Drawing.Point(5, 20);
            this.checkBoxDigiPhoto.Name = "checkBoxDigiPhoto";
            this.checkBoxDigiPhoto.Size = new System.Drawing.Size(115, 17);
            this.checkBoxDigiPhoto.TabIndex = 2;
            this.checkBoxDigiPhoto.Text = "DigiPhoto (Primary)";
            this.checkBoxDigiPhoto.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrintConsole
            // 
            this.checkBoxPrintConsole.AutoSize = true;
            this.checkBoxPrintConsole.Enabled = false;
            this.checkBoxPrintConsole.Location = new System.Drawing.Point(5, 46);
            this.checkBoxPrintConsole.Name = "checkBoxPrintConsole";
            this.checkBoxPrintConsole.Size = new System.Drawing.Size(88, 17);
            this.checkBoxPrintConsole.TabIndex = 3;
            this.checkBoxPrintConsole.Text = "Print Console";
            this.checkBoxPrintConsole.UseVisualStyleBackColor = true;
            // 
            // checkBoxWatcher
            // 
            this.checkBoxWatcher.AutoSize = true;
            this.checkBoxWatcher.Enabled = false;
            this.checkBoxWatcher.Location = new System.Drawing.Point(6, 15);
            this.checkBoxWatcher.Name = "checkBoxWatcher";
            this.checkBoxWatcher.Size = new System.Drawing.Size(67, 17);
            this.checkBoxWatcher.TabIndex = 4;
            this.checkBoxWatcher.Text = "Watcher";
            this.checkBoxWatcher.UseVisualStyleBackColor = true;
            // 
            // checkBoxEmailApp
            // 
            this.checkBoxEmailApp.AutoSize = true;
            this.checkBoxEmailApp.Enabled = false;
            this.checkBoxEmailApp.Location = new System.Drawing.Point(6, 47);
            this.checkBoxEmailApp.Name = "checkBoxEmailApp";
            this.checkBoxEmailApp.Size = new System.Drawing.Size(73, 17);
            this.checkBoxEmailApp.TabIndex = 15;
            this.checkBoxEmailApp.Text = "Email App";
            this.checkBoxEmailApp.UseVisualStyleBackColor = true;
            // 
            // checkBoxSyncService
            // 
            this.checkBoxSyncService.AutoSize = true;
            this.checkBoxSyncService.Enabled = false;
            this.checkBoxSyncService.Location = new System.Drawing.Point(7, 44);
            this.checkBoxSyncService.Name = "checkBoxSyncService";
            this.checkBoxSyncService.Size = new System.Drawing.Size(89, 17);
            this.checkBoxSyncService.TabIndex = 16;
            this.checkBoxSyncService.Text = "Sync Service";
            this.checkBoxSyncService.UseVisualStyleBackColor = true;
            // 
            // checkBoxRideCaptureUtility
            // 
            this.checkBoxRideCaptureUtility.AutoSize = true;
            this.checkBoxRideCaptureUtility.Enabled = false;
            this.checkBoxRideCaptureUtility.Location = new System.Drawing.Point(6, 16);
            this.checkBoxRideCaptureUtility.Name = "checkBoxRideCaptureUtility";
            this.checkBoxRideCaptureUtility.Size = new System.Drawing.Size(116, 17);
            this.checkBoxRideCaptureUtility.TabIndex = 17;
            this.checkBoxRideCaptureUtility.Text = "Ride Capture Utility";
            this.checkBoxRideCaptureUtility.UseVisualStyleBackColor = true;
            // 
            // checkBoxRideCameraSetting
            // 
            this.checkBoxRideCameraSetting.AutoSize = true;
            this.checkBoxRideCameraSetting.Enabled = false;
            this.checkBoxRideCameraSetting.Location = new System.Drawing.Point(6, 42);
            this.checkBoxRideCameraSetting.Name = "checkBoxRideCameraSetting";
            this.checkBoxRideCameraSetting.Size = new System.Drawing.Size(123, 17);
            this.checkBoxRideCameraSetting.TabIndex = 18;
            this.checkBoxRideCameraSetting.Text = "Ride Camera Setting";
            this.checkBoxRideCameraSetting.UseVisualStyleBackColor = true;
            // 
            // checkBoxWifi
            // 
            this.checkBoxWifi.AutoSize = true;
            this.checkBoxWifi.Enabled = false;
            this.checkBoxWifi.Location = new System.Drawing.Point(6, 39);
            this.checkBoxWifi.Name = "checkBoxWifi";
            this.checkBoxWifi.Size = new System.Drawing.Size(44, 17);
            this.checkBoxWifi.TabIndex = 19;
            this.checkBoxWifi.Text = "Wifi";
            this.checkBoxWifi.UseVisualStyleBackColor = true;
            // 
            // checkBoxSyncApp
            // 
            this.checkBoxSyncApp.AutoSize = true;
            this.checkBoxSyncApp.Enabled = false;
            this.checkBoxSyncApp.Location = new System.Drawing.Point(7, 19);
            this.checkBoxSyncApp.Name = "checkBoxSyncApp";
            this.checkBoxSyncApp.Size = new System.Drawing.Size(72, 17);
            this.checkBoxSyncApp.TabIndex = 20;
            this.checkBoxSyncApp.Text = "Sync App";
            this.checkBoxSyncApp.UseVisualStyleBackColor = true;
            // 
            // checkBoxEmailService
            // 
            this.checkBoxEmailService.AutoSize = true;
            this.checkBoxEmailService.Enabled = false;
            this.checkBoxEmailService.Location = new System.Drawing.Point(6, 19);
            this.checkBoxEmailService.Name = "checkBoxEmailService";
            this.checkBoxEmailService.Size = new System.Drawing.Size(90, 17);
            this.checkBoxEmailService.TabIndex = 21;
            this.checkBoxEmailService.Text = "Email Service";
            this.checkBoxEmailService.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxSyncApp);
            this.groupBox1.Controls.Add(this.checkBoxSyncService);
            this.groupBox1.Location = new System.Drawing.Point(6, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(130, 64);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DigiSync";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxEmailApp);
            this.groupBox2.Controls.Add(this.checkBoxEmailService);
            this.groupBox2.Location = new System.Drawing.Point(141, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(130, 64);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Email";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxRideCaptureUtility);
            this.groupBox3.Controls.Add(this.checkBoxRideCameraSetting);
            this.groupBox3.Location = new System.Drawing.Point(141, 40);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(130, 64);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ride Camera";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBoxWatcher);
            this.groupBox4.Controls.Add(this.checkBoxWifi);
            this.groupBox4.Location = new System.Drawing.Point(6, 173);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(130, 64);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Wifi";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxDigiPhoto);
            this.groupBox5.Controls.Add(this.checkBoxPrintConsole);
            this.groupBox5.Location = new System.Drawing.Point(7, 40);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(130, 64);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "DigiPhoto";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.buttonSave);
            this.groupBox6.Controls.Add(this.textBoxPassword);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.textBoxUserID);
            this.groupBox6.Controls.Add(this.textBoxInitialCatalog);
            this.groupBox6.Controls.Add(this.textBoxDataSource);
            this.groupBox6.Location = new System.Drawing.Point(277, 41);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(185, 196);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Preview";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(76, 148);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 36;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(76, 121);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "User ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Initial Catalog";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Data Source";
            // 
            // textBoxUserID
            // 
            this.textBoxUserID.Location = new System.Drawing.Point(76, 87);
            this.textBoxUserID.Name = "textBoxUserID";
            this.textBoxUserID.Size = new System.Drawing.Size(100, 20);
            this.textBoxUserID.TabIndex = 30;
            // 
            // textBoxInitialCatalog
            // 
            this.textBoxInitialCatalog.Location = new System.Drawing.Point(76, 53);
            this.textBoxInitialCatalog.Name = "textBoxInitialCatalog";
            this.textBoxInitialCatalog.Size = new System.Drawing.Size(100, 20);
            this.textBoxInitialCatalog.TabIndex = 29;
            // 
            // textBoxDataSource
            // 
            this.textBoxDataSource.Location = new System.Drawing.Point(76, 19);
            this.textBoxDataSource.Name = "textBoxDataSource";
            this.textBoxDataSource.Size = new System.Drawing.Size(100, 20);
            this.textBoxDataSource.TabIndex = 28;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.textBox1);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Location = new System.Drawing.Point(6, -1);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(456, 35);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(417, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(135, 10);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(273, 20);
            this.textBox1.TabIndex = 14;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Select the working folder.";
            // 
            // SelectExes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 257);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "SelectExes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration Update";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxDigiPhoto;
        private System.Windows.Forms.CheckBox checkBoxPrintConsole;
        private System.Windows.Forms.CheckBox checkBoxWatcher;
        private System.Windows.Forms.CheckBox checkBoxEmailApp;
        private System.Windows.Forms.CheckBox checkBoxSyncService;
        private System.Windows.Forms.CheckBox checkBoxRideCaptureUtility;
        private System.Windows.Forms.CheckBox checkBoxRideCameraSetting;
        private System.Windows.Forms.CheckBox checkBoxWifi;
        private System.Windows.Forms.CheckBox checkBoxSyncApp;
        private System.Windows.Forms.CheckBox checkBoxEmailService;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxUserID;
        private System.Windows.Forms.TextBox textBoxInitialCatalog;
        private System.Windows.Forms.TextBox textBoxDataSource;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
    }
}