﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DecryptConfig
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (!File.Exists(".\\DigiPasword.digi"))
                {
                    MessageBox.Show("Unable to locate DigiPasword.digi file. please configure path correctly.", "Digi");
                    return;
                }
                string[] text = File.ReadAllLines(".\\DigiPasword.digi");

                if (cleanPassword(Passwordmatcher(text)).Equals(textBox1.Text.Trim()))
                {
                    SelectExes form1 = new SelectExes();
                    form1.FormClosing += new FormClosingEventHandler(fr_FormClosing);
                    form1.ShowDialog();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Please try again.", "Digi");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please try again.", "Digi");
            }
            finally
            {
            }
        }

        private string cleanPassword(string rawPassword)
        {
            string strfirst = rawPassword.Substring(0, 4);
            strfirst = Reverse(strfirst);

            string strsecond = rawPassword.Substring(4);

            return strfirst + strsecond;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buttonLogin_Click(sender, new EventArgs());
            }
        }

        private string Passwordmatcher(string[] pswrd)
        {
            string savedPassword = string.Empty;
            pswrd = processPaswrd(pswrd);

            if (pswrd == null)
            {
                return null;
            }

            Int32 fakeId = 0;
            foreach (var item in pswrd)
            {
                //if (fakeId == 6 || fakeId == 8 || fakeId == 10 || fakeId == 12 || fakeId == 14)

                if (fakeId >= 6 && fakeId % 2 == 0)
                {
                    fakeId += 1;
                    continue;
                }
                fakeId += 1;

                string[] str = item.Split(' ');

                var firstString = Reverse(str[0]);

                switch (firstString.ToString().ToUpper())
                {
                    case "LOW":
                        if (Convert.ToInt32(str[1]) >= 97 && Convert.ToInt32(str[1]) <= 122)
                            savedPassword += char.ConvertFromUtf32(Convert.ToInt32(str[1]));
                        break;

                    case "CAP":
                        if (Convert.ToInt32(str[1]) >= 65 && Convert.ToInt32(str[1]) <= 90)
                            savedPassword += char.ConvertFromUtf32(Convert.ToInt32(str[1]));
                        break;

                    case "DIG":
                        if (Convert.ToInt32(str[1]) >= 48 && Convert.ToInt32(str[1]) <= 57)
                            savedPassword += char.ConvertFromUtf32(Convert.ToInt32(str[1]));
                        break;

                    case "SPL":
                        if ((Convert.ToInt32(str[1]) >= 33 && Convert.ToInt32(str[1]) <= 47)
                            || (Convert.ToInt32(str[1]) >= 58 && Convert.ToInt32(str[1]) <= 64)
                            || (Convert.ToInt32(str[1]) >= 91 && Convert.ToInt32(str[1]) <= 96)
                            || (Convert.ToInt32(str[1]) >= 123 && Convert.ToInt32(str[1]) <= 126))
                            savedPassword += char.ConvertFromUtf32(Convert.ToInt32(str[1]));
                        break;

                    // case "ASC":
                    case "CSA":
                    // case "ASCII":
                    // case "IICSA":
                        savedPassword += char.ConvertFromUtf32(Convert.ToInt32(str[1]));
                        break;

                    // case "CHARACTER":
                    // case "RETCARAHC":
                    case "CHAR":
                    case "RAHC":
                        savedPassword += str[1];
                        break;
                }
            }

            return savedPassword;
        }

        private string[] processPaswrd(string[] pswrd)
        {
            if (!ValidatePaswrd(pswrd))
            {
                return null;
            }
            return pswrd;
        }

        private bool ValidatePaswrd(string[] pswrd)
        {
            if (!pswrd[0].StartsWith("**#**"))
            {
                return false;
            }
            if (!pswrd[pswrd.Length - 1].EndsWith("##*##"))
            {
                return false;
            }

            if (!(pswrd.Count() >= 6))
            {
                return false;
            }
            return true;
        }

        public string Reverse(string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        void fr_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }
    }
}
