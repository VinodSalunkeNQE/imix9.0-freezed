﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace DecryptConfig
{
    public partial class SelectExes : Form
    {
        public SelectExes()
        {
            InitializeComponent();
        }

        private string FilePath = string.Empty;
        string xmlData = string.Empty;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

                folderBrowserDialog.SelectedPath = Environment.CurrentDirectory;
                folderBrowserDialog.ShowNewFolderButton = false;

                DialogResult dgResult = folderBrowserDialog.ShowDialog();

                if (dgResult == System.Windows.Forms.DialogResult.OK)
                {
                    textBox1.Text = folderBrowserDialog.SelectedPath;
                    //load data from xml digiphoto

                    FilePath = folderBrowserDialog.SelectedPath;
                    LoadExeFromSelectedFolder(FilePath);
                    if (File.Exists(FilePath + "\\DigiPhoto.exe") && File.Exists(FilePath + "\\DigiPhoto.exe.config"))
                    {
                        decrypt(FilePath + "\\DigiPhoto.exe");
                        ShowXmlData(string.Concat(folderBrowserDialog.SelectedPath, "\\DigiPhoto.exe.config"));
                        encrypt(FilePath + "\\DigiPhoto.exe");
                    }
                    else
                    {
                        textBoxDataSource.Text = string.Empty;
                        textBoxInitialCatalog.Text = string.Empty;
                        textBoxPassword.Text = string.Empty;
                        textBoxUserID.Text = string.Empty;

                        MessageBox.Show("DigiPhoto.exe file not found.please provide input for working with other exes.", "Digi");
                    }


                    // FilePath = "C:\\Temp";

                    //decrypt(FilePath + "\\DigiPhoto.exe");
                    //ShowXmlData(FilePath + "\\DigiPhoto.exe.config");
                    //encrypt(FilePath + "\\DigiPhoto.exe");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select an exe folder", "Digi");
            }
        }

        private void ShowXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);
                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings"); // You can also use XPath here
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');

                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');
                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "Data Source":
                                            textBoxDataSource.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Initial Catalog":
                                            textBoxInitialCatalog.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "User ID":
                                            textBoxUserID.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Password":
                                            textBoxPassword.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }
        }

        static string ConvertStringArrayToString(string[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
            }
            return builder.ToString();
        }

        static string ConvertStringArrayToString(string[] array, string seprator)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(seprator);
            }
            return builder.ToString();
        }

        private void WriteXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');
                                StringBuilder strColNew = new StringBuilder();
                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');


                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "'Data Source":
                                        case "Data Source":
                                            strColChild[strColChild.Length - 1] = textBoxDataSource.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");


                                            break;
                                        case "Initial Catalog":

                                            strColChild[strColChild.Length - 1] = textBoxInitialCatalog.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");

                                            //strColNew[strColChild.Length - 1] = textBoxInitialCatalog.Text;
                                            break;
                                        case "User ID":
                                            strColChild[strColChild.Length - 1] = textBoxUserID.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxUserID.Text;
                                            break;
                                        case "Password":
                                            strColChild[strColChild.Length - 1] = textBoxPassword.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxPassword.Text;
                                            break;
                                        default:
                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            break;
                                    }
                                }
                                //strColNew.Append(ConvertStringArrayToString(strColChild, ";"));
                                //stringData = ConvertStringArrayToString(strColNew);

                                attrColection.Value = strColNew.ToString();

                            }
                        }
                    }
                }

                doc.Save(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saved = false;

                if (checkBoxDigiPhoto.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveDigiPhoto(FilePath);
                    saved = true;
                }

                if (checkBoxPrintConsole.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SavePrintConsole(FilePath);
                    saved = true;
                }

                if (checkBoxWatcher.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveWatcher(FilePath);
                    saved = true;
                }

                if (checkBoxWifi.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveWifi(FilePath);
                    saved = true;
                }

                if (checkBoxRideCaptureUtility.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveRideCaptureUtility(FilePath);
                    saved = true;
                }

                if (checkBoxRideCameraSetting.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveRideCameraSetting(FilePath);
                    saved = true;
                }

                if (checkBoxSyncApp.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveSyncApp(FilePath);
                    saved = true;
                }

                if (checkBoxSyncService.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveSyncService(FilePath);
                    saved = true;
                }

                if (checkBoxEmailApp.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveEmailApp(FilePath);
                    saved = true;
                }

                if (checkBoxEmailService.Checked && !string.IsNullOrEmpty(FilePath))
                {
                    SaveEmailService(FilePath);
                    saved = true;
                }

                if (saved)
                    MessageBox.Show("Changes saved successfully", "Digi");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }
        }

        private void SaveEmailService(string fileName)
        {
            fileName += "\\EmailService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\EmailService.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\EmailService.exe");
        }

        private void SaveEmailApp(string fileName)
        {
            fileName += "\\EmailKiosk.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\EmailKiosk.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\EmailKiosk.exe");
        }

        private void SaveSyncService(string fileName)
        {
            fileName += "\\DigiSyncService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiSyncService.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiSyncService.exe");
        }

        private void SaveSyncApp(string fileName)
        {
            fileName += "\\DigiSync.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiSync.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiSync.exe");
           
        }

        private void SaveWifi(string fileName)
        {
            fileName += "\\DigiWifiImageProcessing.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiWifiImageProcessing.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiWifiImageProcessing.exe");
        }

        private void SaveRideCaptureUtility(string fileName)
        {
            fileName += "\\RideCaptureUtility.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\RideCaptureUtility.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\RideCaptureUtility.exe");
        }

        private void SaveRideCameraSetting(string fileName)
        {
            fileName += "\\RideCameraSetting.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\RideCameraSetting.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\RideCameraSetting.exe");
        }

        private void SaveDigiPhoto(string fileName)
        {
            fileName += "\\DigiPhoto.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiPhoto.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiPhoto.exe");
        }

        private void SavePrintConsole(string fileName)
        {
            fileName += "//DigiPrintingConsole.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiPrintingConsole.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiPrintingConsole.exe");
        }

        private void SaveWatcher(string fileName)
        {
            fileName += "//DigiWatcher.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiWatcher.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiWatcher.exe");
        }

        private void decrypt(string configfileName)
        {
            try
            {
                UnprotectConnectionString(configfileName);
                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }
        }

        private void UnprotectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, false);
        }

        private void ProtectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, true);
        }

        private static void ToggleConnectionStringProtection(string pathName, bool protect)
        {
            // Define the Dpapi provider name.
            string strProvider = "DataProtectionConfigurationProvider";
            // string strProvider = "RSAProtectedConfigurationProvider";

            System.Configuration.Configuration oConfiguration = null;
            System.Configuration.ConnectionStringsSection oSection = null;

            try
            {
                // Open the configuration file and retrieve the connectionStrings section.

                // For Web!
                // oConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

                // For Windows!
                // Takes the executable file name without the config extension.
                oConfiguration = System.Configuration.ConfigurationManager.OpenExeConfiguration(pathName);

                if (oConfiguration != null)
                {
                    bool blnChanged = false;
                    oSection = oConfiguration.GetSection("connectionStrings") as System.Configuration.ConnectionStringsSection;

                    if (oSection != null)
                    {
                        if ((!(oSection.ElementInformation.IsLocked)) && (!(oSection.SectionInformation.IsLocked)))
                        {
                            if (protect)
                            {
                                if (!(oSection.SectionInformation.IsProtected))
                                {
                                    blnChanged = true;

                                    // Encrypt the section.
                                    oSection.SectionInformation.ProtectSection(strProvider);
                                }
                            }
                            else
                            {
                                if (oSection.SectionInformation.IsProtected)
                                {
                                    blnChanged = true;

                                    // Remove encryption.
                                    oSection.SectionInformation.UnprotectSection();
                                }
                            }
                        }

                        if (blnChanged)
                        {
                            // Indicates whether the associated configuration section will be saved even if it has not been modified.
                            oSection.SectionInformation.ForceSave = true;

                            // Save the current configuration.
                            oConfiguration.Save();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        private void encrypt(string configfileName)
        {
            try
            {
                ProtectConnectionString(configfileName);

                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }
        }

        private void Save(string configfileName)
        {
            try
            {
                StreamWriter b = new StreamWriter(File.Open(configfileName, FileMode.Create));
                b.Write(xmlData);

                b.Close();

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Digi");
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FilePath = textBox1.Text;
                LoadExeFromSelectedFolder(FilePath);
                if (File.Exists(FilePath + "\\DigiPhoto.exe") && File.Exists(FilePath + "\\DigiPhoto.exe.config"))
                {
                    decrypt(FilePath + "\\DigiPhoto.exe");
                    ShowXmlData(string.Concat(FilePath, "\\DigiPhoto.exe.config"));
                    encrypt(FilePath + "\\DigiPhoto.exe");
                }
                else
                {
                    MessageBox.Show("File not found.", "Digi");
                }
            }
        }

        private void LoadExeFromSelectedFolder(string fileName)
        {
            //Enable-Disable check box.
            if (File.Exists(fileName + "\\DigiPhoto.exe"))
            {
                checkBoxDigiPhoto.Enabled = true;
            }
            else
            {
                checkBoxDigiPhoto.Enabled = false;
            }

            if (File.Exists(fileName + "\\DigiPrintingConsole.exe"))
            {
                checkBoxPrintConsole.Enabled = true; 
            }
            else
            {
                checkBoxPrintConsole.Enabled = false; 
            }
            if (File.Exists(fileName + "\\DigiWatcher.exe"))
            {
                checkBoxWatcher.Enabled = true;
            }
            else
            {
                checkBoxWatcher.Enabled = false;
            }

            if (File.Exists(fileName + "\\DigiSync.exe"))
            {
                checkBoxSyncApp.Enabled = true;
            }
            else
            {
                checkBoxSyncApp.Enabled = false;
            }

            if (File.Exists(fileName + "\\DigiSyncService.exe"))
            {
                checkBoxSyncService.Enabled = true;
            }
            else
            {
                checkBoxSyncService.Enabled = false;
            }

            if (File.Exists(fileName + "\\EmailKiosk.exe"))
            {
                checkBoxEmailApp.Enabled = true;
            }
            else
            {
                checkBoxEmailApp.Enabled = false;
            }
            if (File.Exists(fileName + "\\EmailService.exe"))
            {
                checkBoxEmailService.Enabled = true;
            }
            else
            {
                checkBoxEmailService.Enabled = false;
            }
            if (File.Exists(fileName + "\\RideCameraSetting.exe"))
            {
                checkBoxRideCameraSetting.Enabled = true;
            }
            else
            {
                checkBoxRideCameraSetting.Enabled = false;
            }
            if (File.Exists(fileName + "\\RideCaptureUtility.exe"))
            {
                checkBoxRideCaptureUtility.Enabled = true;
            }
            else
            {
                checkBoxRideCaptureUtility.Enabled = false;
            }
            if (File.Exists(fileName + "\\DigiWifiImageProcessing.exe"))
            {
                checkBoxWifi.Enabled = true;
            }
            else
            {
                checkBoxWifi.Enabled = false;
            }
        }
    }
}
