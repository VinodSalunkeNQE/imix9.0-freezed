﻿namespace DecryptConfig
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtpathconfig = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnbrowseconfig = new System.Windows.Forms.Button();
            this.btndecrypt = new System.Windows.Forms.Button();
            this.txtData = new System.Windows.Forms.TextBox();
            this.btnencrypt = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtpathconfig
            // 
            this.txtpathconfig.Location = new System.Drawing.Point(128, 27);
            this.txtpathconfig.Margin = new System.Windows.Forms.Padding(4);
            this.txtpathconfig.Name = "txtpathconfig";
            this.txtpathconfig.Size = new System.Drawing.Size(434, 23);
            this.txtpathconfig.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Exe file:";
            // 
            // btnbrowseconfig
            // 
            this.btnbrowseconfig.Location = new System.Drawing.Point(569, 27);
            this.btnbrowseconfig.Name = "btnbrowseconfig";
            this.btnbrowseconfig.Size = new System.Drawing.Size(75, 23);
            this.btnbrowseconfig.TabIndex = 2;
            this.btnbrowseconfig.Text = "Browse..";
            this.btnbrowseconfig.UseVisualStyleBackColor = true;
            this.btnbrowseconfig.Click += new System.EventHandler(this.btnbrowseconfig_Click);
            // 
            // btndecrypt
            // 
            this.btndecrypt.Location = new System.Drawing.Point(244, 87);
            this.btndecrypt.Name = "btndecrypt";
            this.btndecrypt.Size = new System.Drawing.Size(75, 25);
            this.btndecrypt.TabIndex = 3;
            this.btndecrypt.Text = "Decrypt";
            this.btndecrypt.UseVisualStyleBackColor = true;
            this.btndecrypt.Click += new System.EventHandler(this.btndecrypt_Click);
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(57, 128);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtData.Size = new System.Drawing.Size(587, 169);
            this.txtData.TabIndex = 4;
            // 
            // btnencrypt
            // 
            this.btnencrypt.Location = new System.Drawing.Point(358, 87);
            this.btnencrypt.Name = "btnencrypt";
            this.btnencrypt.Size = new System.Drawing.Size(75, 25);
            this.btnencrypt.TabIndex = 5;
            this.btnencrypt.Text = "Encrypt";
            this.btnencrypt.UseVisualStyleBackColor = true;
            this.btnencrypt.Click += new System.EventHandler(this.btnencrypt_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(128, 87);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 322);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnencrypt);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.btndecrypt);
            this.Controls.Add(this.btnbrowseconfig);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtpathconfig);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Decyrpt Config";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtpathconfig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnbrowseconfig;
        private System.Windows.Forms.Button btndecrypt;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Button btnencrypt;
        private System.Windows.Forms.Button btnSave;
    }
}

