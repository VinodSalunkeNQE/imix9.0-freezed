﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.Business.Serialization
{
    class ProductSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var product = (from p in dbContext.Products
                                   join OA in dbContext.ObjectAuthorizations on p.ProductId equals OA.ObjectValueId where p.ProductId==ObjectValueId
                                   select new { myproduct = p, myobjectauth = OA }).FirstOrDefault();
                    //  var product = dbContext.Products join .Where(r => r.ProductId == ObjectValueId).FirstOrDefault();
                    if (product != null)
                    {
                        ProductInfo productInfo = new ProductInfo()
                           {
                               ProductId = product.myproduct.ProductId,
                               Name = product.myproduct.Name,
                               Description = product.myproduct.Description,
                               Code = product.myproduct.Code,
                               ProductNumber = product.myproduct.ProductNumber,
                               ImagePath = product.myproduct.ImagePath,
                               MaximumQuantity = product.myproduct.MaximumQuantity,
                               IsBundled = product.myproduct.IsBundled,
                               IsAccessory = product.myproduct.IsAccessory,
                               IsActive = product.myproduct.IsActive,
                               IsDiscountApplied = product.myproduct.IsDiscountApplied,
                               IsPrimary = product.myproduct.IsPrimary,
                               Price=product.myproduct.Price,
                               CreatedDateTime=product.myproduct.CreatedDateTime,
                               SyncCode = product.myproduct.SyncCode,
                              IsPersonalizedAR = product.myproduct.IsPersonalizedAR,
                            // CurrencySyncCode = dbContext.Currencies.Where(c => c.CurrencyId == product.myproduct.CurrencyID).FirstOrDefault().SyncCode,
                            UserSyncCode = dbContext.Users.Where(u => u.UserId == product.myproduct.CreatedBy).FirstOrDefault().SyncCode,
                               LocationSyncCode = dbContext.DigiMasterLocations.Where(d => d.DigiMasterLocationId == product.myobjectauth.DigiMasterLocationId).FirstOrDefault().SyncCode




                           };

                        dataXML = CommonUtility.SerializeObject<ProductInfo>(productInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Product Serialization.", e);
                outerException.Source = "Business.Serialization.ProductSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
