﻿using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Business.Serialization
{
    public interface IDownloadable
    {
        DownloadInfo Download { get; set; }
    }
}
