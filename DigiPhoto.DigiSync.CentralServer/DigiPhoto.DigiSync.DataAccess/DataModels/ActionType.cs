namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActionType")]
    public partial class ActionType
    {
        public ActionType()
        {
            UserActivityLogs = new HashSet<UserActivityLog>();
        }

        public int ActionTypeId { get; set; }

        [Required]
        [StringLength(200)]
        public string ActionName { get; set; }

        public virtual ICollection<UserActivityLog> UserActivityLogs { get; set; }
    }
}
