namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RoleModule
    {
        public long RoleId { get; set; }

        public long ModuleId { get; set; }

        [Key]
        public int RoleModulesId { get; set; }

        public virtual Module Module { get; set; }

        public virtual Role Role { get; set; }
    }
}
