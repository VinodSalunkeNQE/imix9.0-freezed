namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Graphic
    {
        [Key]
        public long GraphicsId { get; set; }

        [StringLength(1000)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string DisplayName { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public bool IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }
    }
}
