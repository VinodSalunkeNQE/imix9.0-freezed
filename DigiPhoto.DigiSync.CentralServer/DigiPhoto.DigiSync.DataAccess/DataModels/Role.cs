namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Role")]
    public partial class Role
    {
        public Role()
        {
            Modules = new HashSet<Module>();
            Users = new HashSet<User>();
        }

        public long RoleId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public long? ParentRoleId { get; set; }

        public bool IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual ICollection<Module> Modules { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
