namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Background")]
    public partial class Background
    {
        [Key]
        public long BackgoundId { get; set; }

        public long? ProductId { get; set; }

        [StringLength(2000)]
        public string BackgroundImage { get; set; }

        [Required]
        [StringLength(2000)]
        public string DisplayName { get; set; }

        public int? BackgroundGroupId { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public virtual Product Product { get; set; }
    }
}
