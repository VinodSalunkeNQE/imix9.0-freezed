﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Activity")]
    public partial class Activity
    {
        [Key]
        public int AcitivityId { get; set; }
        public int? ActionType { get; set; }
        public DateTime? AcitivityDate { get; set; }
        public long AcitivityBy { get; set; }
        public string AcitivityDescrption { get; set; }
        public int? ReferenceID { get; set; }
        
        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
    }
}
