namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductRefund")]
    public partial class ProductRefund
    {
        public ProductRefund()
        {
            ProductRefundDetails = new HashSet<ProductRefundDetail>();
        }

        public long ProductRefundId { get; set; }

        public long OrderMasterId { get; set; }

        [Column(TypeName = "money")]
        public decimal? RefundAmount { get; set; }

        public int? RefundMode { get; set; }

        public DateTime RefundDateTime { get; set; }

        public long RefundBy { get; set; }

        public virtual OrderMaster OrderMaster { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<ProductRefundDetail> ProductRefundDetails { get; set; }
    }
}
