﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    class UserProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    UserInfo userInfo = CommonUtility.DeserializeXML<UserInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingUserCount = dbContext.DG_Users.Count(r => r.SyncCode == userInfo.SyncCode);
                        if (existingUserCount == 0)
                        {
                            var user = new DG_Users();

                            user.DG_User_Name = userInfo.UserName;
                            user.DG_User_CreatedDate = userInfo.CreatedDateTime;
                            user.DG_User_Email = userInfo.EmailAddress;
                            user.DG_User_First_Name = userInfo.FirstName;
                            user.DG_User_Last_Name = userInfo.LastName;
                            user.DG_User_PhoneNo = userInfo.PhoneNumber;
                            user.DG_User_Password = userInfo.Password;
                            user.DG_User_Status = userInfo.IsActive;
                            user.DG_User_CreatedDate = System.DateTime.Now;
                            int RoleId = 0;
                            if (userInfo.RoleSyncCode != null)
                            {
                                int countrole = dbContext.DG_User_Roles.Count(s => s.SyncCode == userInfo.RoleSyncCode);
                                if (countrole > 0)
                                {
                                    RoleId = dbContext.DG_User_Roles.Where(s => s.SyncCode == userInfo.RoleSyncCode).FirstOrDefault().DG_User_Roles_pkey;
                                    user.DG_User_Roles_Id = RoleId;
                                }
                            }

                            int LocationId = 0;
                            if (userInfo.LocationSyncCode != null)
                            {
                                int countLocation = dbContext.DG_Location.Count(s => s.SyncCode == userInfo.LocationSyncCode);
                                if (countLocation > 0)
                                {
                                    LocationId = dbContext.DG_Location.Where(s => s.SyncCode == userInfo.LocationSyncCode).FirstOrDefault().DG_Location_pkey;
                                    user.DG_Location_ID = LocationId;
                                }
                                else
                                {

                                   int substoreid = dbContext.DG_SubStores.Where(s => s.SyncCode == ChangeInfo.SubStore.SiteCode).FirstOrDefault().DG_SubStore_pkey;
                                   LocationId = dbContext.DG_SubStore_Locations.Where(s => s.DG_SubStore_ID == substoreid).FirstOrDefault().DG_Location_ID;
                                   if (LocationId > 0)
                                   {
                                       user.DG_Location_ID = LocationId;
                                   }
                                   else
                                   {
                                       user.DG_Location_ID = 0;
                                   }
                                }
                                
                            }
                            else
                            {
                                user.DG_Location_ID = 0;
                            }

                            user.SyncCode = userInfo.SyncCode;
                            user.IsSynced = true;
                            dbContext.DG_Users.Add(user);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in User Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.UserProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    UserInfo userInfo = CommonUtility.DeserializeXML<UserInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbUser = dbContext.DG_Users.Where(r => r.SyncCode == userInfo.SyncCode).FirstOrDefault();
                        if (dbUser != null)
                        {


                            dbUser.DG_User_Name = userInfo.UserName;

                            dbUser.DG_User_CreatedDate = userInfo.CreatedDateTime;
                            dbUser.DG_User_Email = userInfo.EmailAddress;
                            dbUser.DG_User_First_Name = userInfo.FirstName;
                            dbUser.DG_User_Last_Name = userInfo.LastName;
                            dbUser.DG_User_PhoneNo = userInfo.PhoneNumber;
                            dbUser.DG_User_Password = userInfo.Password;
                            dbUser.DG_User_Status = userInfo.IsActive;
                            dbUser.DG_User_CreatedDate = System.DateTime.Now;
                            int RoleId = 0;

                            if (userInfo.RoleSyncCode != null)
                            {
                                int countrole = dbContext.DG_User_Roles.Count(s => s.SyncCode == userInfo.RoleSyncCode);
                                if (countrole > 0)
                                {
                                    RoleId = dbContext.DG_User_Roles.Where(s => s.SyncCode == userInfo.RoleSyncCode).FirstOrDefault().DG_User_Roles_pkey;
                                }
                                else
                                {
                                    RoleId = 0;
                                }
                            }
                            else
                            {
                                RoleId = 0;
                            }
                            dbUser.DG_User_Roles_Id = RoleId;


                            int LocationId = 0;

                            if (userInfo.LocationSyncCode != null)
                            {
                                int countLocation = dbContext.DG_Location.Count(s => s.SyncCode == userInfo.LocationSyncCode);
                                if (countLocation > 0)
                                {
                                    LocationId = dbContext.DG_Location.Where(s => s.SyncCode == userInfo.LocationSyncCode).FirstOrDefault().DG_Location_pkey;
                                }
                                else
                                {

                                    int substoreid = dbContext.DG_SubStores.Where(s => s.SyncCode == ChangeInfo.SubStore.SiteCode).FirstOrDefault().DG_SubStore_pkey;
                                   int UserLocationId = dbContext.DG_SubStore_Locations.Where(s => s.DG_SubStore_ID == substoreid).FirstOrDefault().DG_Location_ID;
                                   if (UserLocationId > 0)
                                    {
                                        LocationId = UserLocationId;
                                    }
                                    else
                                    {
                                        LocationId = 0;
                                    }
                                }
                                
                            }
                            else
                            {
                                LocationId = 0;
                            }
                            dbUser.DG_Location_ID = LocationId;

                            dbUser.SyncCode = userInfo.SyncCode;
                            dbUser.IsSynced = true;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in User Processor Update.", e);
                outerException.Source = "ClientDataProcessor.UserProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbUser = dbContext.DG_Users.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbUser != null)
                    {
                        dbContext.DG_Users.Remove(dbUser);
                        dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in User Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.UserProcessor";
                throw outerException;
            }
        }
    }
}
