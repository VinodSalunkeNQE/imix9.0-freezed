﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    class PackageProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    PackageInfo packageInfo = CommonUtility.DeserializeXML<PackageInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingProductCount = dbContext.DG_Orders_ProductType.Count(r => r.SyncCode == packageInfo.SyncCode);
                        if (existingProductCount == 0)
                        {
                            var product = new DG_Orders_ProductType();
                            product.DG_Orders_ProductType_Name = packageInfo.Name;
                            product.DG_Orders_ProductType_Desc = packageInfo.Description;
                            product.DG_Orders_ProductCode = packageInfo.Code;
                            product.DG_IsActive = packageInfo.IsActive;
                            product.DG_Orders_ProductType_DiscountApplied = packageInfo.IsDiscountApplied;
                            product.SyncCode = packageInfo.SyncCode;
                            product.DG_Orders_ProductType_Image = "/images/jpgfloppy.png";
                            product.DG_Orders_ProductType_IsBundled = false;
                            product.DG_IsPackage = packageInfo.IsPackage;
                            product.DG_IsAccessory = packageInfo.IsAccessory.HasValue ? packageInfo.IsAccessory.Value : false;
                            product.IsSynced = true;

                            int SubstoreId = 0;
                            if (dbContext.DG_SubStores.Where(c => c.SyncCode == packageInfo.LocationSyncCode).FirstOrDefault() != null)
                                SubstoreId = dbContext.DG_SubStores.Where(c => c.SyncCode == packageInfo.LocationSyncCode).FirstOrDefault().DG_SubStore_pkey;
                            product.DG_SubStore_pkey = SubstoreId;

                            dbContext.DG_Orders_ProductType.Add(product);
                            dbContext.SaveChanges();
                            //adding data to packagedetails table
                            DG_PackageDetails packagedetails = new DG_PackageDetails();
                            int productid = 0;
                            int packageid = 0;
                            foreach (PackageProductInfo p in packageInfo.packageProductInfoList)
                            {
                                if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault() != null)
                                    packageid = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                                if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault() != null)
                                
                                    productid = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                                if (productid > 0)
                                {
                                    packagedetails.DG_Product_MaxImage = p.ProductMaxImage;
                                    packagedetails.DG_Product_Quantity = p.ProductQuantity;
                                    packagedetails.DG_PackageId = packageid;
                                    packagedetails.DG_ProductTypeId = productid;
                                    packagedetails.IsSynced = true;
                                    dbContext.DG_PackageDetails.Add(packagedetails);
                                    dbContext.SaveChanges();
                                    productid = 0;
                                }
                            }

                            var abc = dbContext.DG_PackageDetails.Where(r => r.DG_PackageId == product.DG_Orders_ProductType_pkey).Select(o => o.DG_ProductTypeId).ToList();
                            var products = (from p in dbContext.DG_Orders_ProductType
                                            where p.DG_IsPackage == false && !abc.Contains(p.DG_Orders_ProductType_pkey)
                                            select p).ToList();

                            foreach (var p in products)
                            {

                                packagedetails.DG_Product_MaxImage = 0;
                                packagedetails.DG_Product_Quantity = 0;
                                packagedetails.DG_ProductTypeId = p.DG_Orders_ProductType_pkey;
                                packagedetails.DG_PackageId = product.DG_Orders_ProductType_pkey;
                                packagedetails.IsSynced = true;
                                dbContext.DG_PackageDetails.Add(packagedetails);
                                dbContext.SaveChanges();

                            }



                            //adding Data in ProductPricing table

                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                            int StoreId = 0;
                            int UserId = 0;
                            int CurrencyId = 0;
                            int ProductId = 0;

                            //if (dbContext.DG_Location.Where(r => r.SyncCode == packageInfo.LocationSyncCode).FirstOrDefault() != null)
                               StoreId = dbContext.DG_Store.FirstOrDefault().DG_Store_pkey;

                            if (dbContext.DG_Users.Where(u => u.SyncCode == packageInfo.UserSyncCode).FirstOrDefault() != null)
                                UserId = dbContext.DG_Users.Where(u => u.SyncCode == packageInfo.UserSyncCode).FirstOrDefault().DG_User_pkey;

                            if (dbContext.DG_Currency.Where(c => c.SyncCode == packageInfo.CurrencySyncCode).FirstOrDefault() != null)
                                CurrencyId = dbContext.DG_Currency.Where(c => c.SyncCode == packageInfo.CurrencySyncCode).FirstOrDefault().DG_Currency_pkey;

                            if (dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == packageInfo.SyncCode).FirstOrDefault() != null)
                                ProductId = dbContext.DG_Orders_ProductType.Where(p => p.SyncCode == packageInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                            if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault() != null)
                                packageid = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                            ProductPricing.DG_Product_Pricing_CreatedBy = UserId;
                            ProductPricing.DG_Product_Pricing_Currency_ID = 1;

                            //   ProductPricing.DG_Product_Pricing_Currency_ID = CurrencyId;

                            ProductPricing.DG_Product_Pricing_IsAvaliable = true;
                            ProductPricing.DG_Product_Pricing_ProductPrice = packageInfo.Price;
                            ProductPricing.DG_Product_Pricing_ProductType = packageid;
                            ProductPricing.DG_Product_Pricing_CreatedBy = UserId;
                            ProductPricing.DG_Product_Pricing_StoreId = StoreId;
                            ProductPricing.DG_Product_Pricing_UpdateDate = packageInfo.CreatedDateTime;
                            dbContext.DG_Product_Pricing.Add(ProductPricing);
                            dbContext.SaveChanges();


                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Package Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.PackageProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    PackageInfo packageInfo = CommonUtility.DeserializeXML<PackageInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbProduct = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault();
                        if (dbProduct != null)
                        {
                            //Update Product Details
                            dbProduct.DG_Orders_ProductType_Name = packageInfo.Name;
                            dbProduct.DG_Orders_ProductType_Desc = packageInfo.Description;
                            dbProduct.DG_Orders_ProductCode = packageInfo.Code;
                            //  dbProduct.DG_Orders_ProductNumber = Convert.ToInt32(packageInfo.ProductNumber);
                            dbProduct.DG_Orders_ProductType_Image = packageInfo.ImagePath;
                            dbProduct.DG_MaxQuantity = packageInfo.MaximumQuantity;
                            // dbProduct.DG_Orders_ProductType_IsBundled = packageInfo.IsBundled;
                            dbProduct.DG_IsAccessory = packageInfo.IsAccessory.HasValue ? packageInfo.IsAccessory.Value: false ;
                            dbProduct.DG_IsActive = packageInfo.IsActive;
                            dbProduct.DG_Orders_ProductType_DiscountApplied = packageInfo.IsDiscountApplied;
                            dbProduct.DG_IsPrimary = packageInfo.IsPrimary;
                            dbProduct.DG_Orders_ProductType_Image = "/images/jpgfloppy.png";
                            dbProduct.DG_Orders_ProductType_IsBundled = false;
                            dbProduct.DG_IsPackage = packageInfo.IsPackage;
                            dbProduct.SyncCode = packageInfo.SyncCode;
                            dbProduct.IsSynced = true;

                            int SubstoreId = 0;
                            if (dbContext.DG_SubStores.Where(c => c.SyncCode == packageInfo.LocationSyncCode).FirstOrDefault() != null)
                                SubstoreId = dbContext.DG_SubStores.Where(c => c.SyncCode == packageInfo.LocationSyncCode).FirstOrDefault().DG_SubStore_pkey;
                            dbProduct.DG_SubStore_pkey = SubstoreId;

                            dbContext.SaveChanges();
                            DG_PackageDetails packagedetails = new DG_PackageDetails();

                            int productid = 0;
                            int packageid = 0;
                            int cnt = dbContext.DG_PackageDetails.Count(p => p.DG_PackageId == dbProduct.DG_Orders_ProductType_pkey);
                            for (int i = 0; i <= cnt - 1; i++)
                            {
                                var dbPackageDetails = dbContext.DG_PackageDetails.Where(r => r.DG_PackageId == dbProduct.DG_Orders_ProductType_pkey).FirstOrDefault();
                                if (dbPackageDetails != null)
                                {
                                    dbContext.DG_PackageDetails.Remove(dbPackageDetails);
                                    dbContext.SaveChanges();
                                }
                            }
                            foreach (PackageProductInfo p in packageInfo.packageProductInfoList)
                            {
                                if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault() != null)
                                    packageid = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == packageInfo.SyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;

                                if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault() != null)
                                    productid = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault().DG_Orders_ProductType_pkey;
                                if (productid > 0)
                                {
                                    // packagedetails.DG_ProductTypeId = Convert.ToInt32(p.ProductId);
                                    packagedetails.DG_Product_MaxImage = p.ProductMaxImage;
                                    packagedetails.DG_Product_Quantity = p.ProductQuantity;
                                    packagedetails.DG_PackageId = packageid;
                                    packagedetails.DG_ProductTypeId = productid;
                                    packagedetails.IsSynced = true;
                                    dbContext.DG_PackageDetails.Add(packagedetails);
                                    dbContext.SaveChanges();
                                    productid = 0;
                                }
                            }


                            var abc = dbContext.DG_PackageDetails.Where(r => r.DG_PackageId == dbProduct.DG_Orders_ProductType_pkey).Select(o => o.DG_ProductTypeId).ToList();
                            var products = (from p in dbContext.DG_Orders_ProductType
                                            where p.DG_IsPackage == false && !abc.Contains(p.DG_Orders_ProductType_pkey)
                                            select p).ToList();


                            foreach (var p in products)
                            {

                                packagedetails.DG_Product_MaxImage = 0;
                                packagedetails.DG_Product_Quantity = 0;
                                packagedetails.DG_ProductTypeId = p.DG_Orders_ProductType_pkey;
                                packagedetails.DG_PackageId = dbProduct.DG_Orders_ProductType_pkey;
                                packagedetails.IsSynced = true;
                                dbContext.DG_PackageDetails.Add(packagedetails);
                                dbContext.SaveChanges();

                            }
                            int UserId = 0;
                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                            int ProductPricingId = dbContext.DG_Product_Pricing.Where(p => p.DG_Product_Pricing_ProductType == dbProduct.DG_Orders_ProductType_pkey).FirstOrDefault().DG_Product_Pricing_Pkey;
                            ProductPricing = dbContext.DG_Product_Pricing.Where(p => p.DG_Product_Pricing_Pkey == ProductPricingId).FirstOrDefault();
                            int StoreId = dbContext.DG_Store.FirstOrDefault().DG_Store_pkey;
                            if (dbContext.DG_Users.Where(u => u.SyncCode == packageInfo.UserSyncCode).FirstOrDefault() != null)
                                UserId = dbContext.DG_Users.Where(u => u.SyncCode == packageInfo.UserSyncCode).FirstOrDefault().DG_User_pkey;

                            ProductPricing.DG_Product_Pricing_CreatedBy = UserId;
                            ProductPricing.DG_Product_Pricing_Currency_ID = 1;
                            ProductPricing.DG_Product_Pricing_IsAvaliable = true;
                            ProductPricing.DG_Product_Pricing_ProductType = dbProduct.DG_Orders_ProductType_pkey;
                            ProductPricing.DG_Product_Pricing_StoreId = StoreId;
                            ProductPricing.DG_Product_Pricing_UpdateDate = packageInfo.CreatedDateTime;
                            ProductPricing.DG_Product_Pricing_ProductPrice = packageInfo.Price;
                            //dbContext.DG_Product_Pricing.Add(ProductPricing);
                            dbContext.SaveChanges();

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Package Processor Update.", e);
                outerException.Source = "ClientDataProcessor.PackageProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbProduct = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbProduct != null)
                    {
                        int PackageID = 0;
                        if (dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault() != null)
                            PackageID = dbContext.DG_Orders_ProductType.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault().DG_Orders_ProductType_pkey;
                        if (PackageID != 0)
                        {
                            DG_Product_Pricing ProductPricing = new DG_Product_Pricing();
                            ProductPricing = dbContext.DG_Product_Pricing.Where(p => p.DG_Product_Pricing_ProductType == PackageID).FirstOrDefault();
                            if (ProductPricing != null)
                            {
                                dbContext.DG_Product_Pricing.Remove(ProductPricing);
                                dbContext.SaveChanges();
                            }
                            int cnt = dbContext.DG_PackageDetails.Count(p => p.DG_PackageId == PackageID);
                            for (int i = 0; i <= cnt - 1; i++)
                            {
                                var dbPackageDetails = dbContext.DG_PackageDetails.Where(r => r.DG_PackageId == dbProduct.DG_Orders_ProductType_pkey).FirstOrDefault();
                                if (dbPackageDetails != null)
                                {
                                    dbContext.DG_PackageDetails.Remove(dbPackageDetails);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        if (dbProduct != null)
                        {
                            dbContext.DG_Orders_ProductType.Remove(dbProduct);
                            dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                        }


                    }

                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Package Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.PackageProcessor";
                throw outerException;
            }
        }
    }
}
