﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public class CurrencyProfileProcessor : BaseProcessor
    {

        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    CurrencyProfileInfo currencyProfileInfo = CommonUtility.DeserializeXML<CurrencyProfileInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        using (var dbTran = dbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                bool isdeleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                    {
                                        isdeleted = true;
                                    }
                                    else if (currencyProfileInfo.IsDeleted != null)
                                    {
                                        if (currencyProfileInfo.IsDeleted == true)
                                            isdeleted = true;
                                    }
                                }
                                else if (currencyProfileInfo.IsDeleted != null)
                                {
                                    if (currencyProfileInfo.IsDeleted == true)
                                        isdeleted = true;
                                }
                                

                                var existingCurrencyProfiles = dbContext.CurrencyProfiles.Count(r => r.SyncCode == currencyProfileInfo.SyncCode);

                                if (existingCurrencyProfiles == 0)
                                {
                                    var vCurrencyProfileInfo = new CurrencyProfile();
                                    vCurrencyProfileInfo.CreatedBy = currencyProfileInfo.CreatedBy;

                                    vCurrencyProfileInfo.ProfileName = currencyProfileInfo.ProfileName;
                                    vCurrencyProfileInfo.StartDate = currencyProfileInfo.startDate;
                                    vCurrencyProfileInfo.EndDate = currencyProfileInfo.Enddate;
                                    vCurrencyProfileInfo.IsActive = currencyProfileInfo.IsActive;
                                    vCurrencyProfileInfo.CreatedOn = currencyProfileInfo.CreatedOn;
                                    vCurrencyProfileInfo.SyncCode = currencyProfileInfo.SyncCode;
                                    vCurrencyProfileInfo.IsDeleted = isdeleted;
                                    vCurrencyProfileInfo.PublishedOn = currencyProfileInfo.PublishedOn;
                                    dbContext.CurrencyProfiles.Add(vCurrencyProfileInfo);
                                    dbContext.SaveChanges();
                                    Int64 id = vCurrencyProfileInfo.ProfileAuditID;


                                    foreach (var item in currencyProfileInfo.lstCurrencyProfileRate)
                                    {
                                        var vCurrencyProfileRate = new CurrencyProfileRate();

                                        vCurrencyProfileRate.ProfileAuditID = id;
                                        vCurrencyProfileRate.ExchangeRate = item.ExchangeRate;
                                        vCurrencyProfileRate.CurrencyCode = item.CurrencyCode;
                                        vCurrencyProfileRate.CurrencyName = item.CurrencyName;
                                        vCurrencyProfileRate.IsActive = item.IsActive;
                                        vCurrencyProfileRate.CreatedOn = item.CreatedOn;
                                        vCurrencyProfileRate.IsDeleted = item.IsDeleted;
                                        dbContext.CurrencyProfileRates.Add(vCurrencyProfileRate);
                                    }
                                    dbContext.SaveChanges();
                                }

                                bool deleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                        deleted = true;
                                }
                                var SubStore = dbContext.DG_SubStores.Where(r => r.DG_SubStore_pkey == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();

                                CurrencySyncStatusInfo objCurrencySyncStatus = new CurrencySyncStatusInfo();
                                objCurrencySyncStatus.CurrencySyncStatusID = 0;
                                objCurrencySyncStatus.CurrencyProfileID = currencyProfileInfo.ProfileAuditID;
                                objCurrencySyncStatus.SubStoreSyncCode = SubStore.SyncCode;
                                objCurrencySyncStatus.VenueID = 0;
                                objCurrencySyncStatus.SyncStatus = 1;
                                objCurrencySyncStatus.CreatedBy = currencyProfileInfo.CreatedBy;
                                objCurrencySyncStatus.CreatedOn = currencyProfileInfo.CreatedOn;
                                objCurrencySyncStatus.CurrencyProfileStatus = deleted;


                                bool success = false;
                                DServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    success = client.SaveCurrencySyncStatusInfo(objCurrencySyncStatus);
                                });

                                dbTran.Commit();
                            }
                            catch (Exception e)
                            {
                                bool deleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                        deleted = true;
                                }
                                var SubStore = dbContext.DG_SubStores.Where(r => r.DG_SubStore_pkey == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();

                                CurrencySyncStatusInfo objCurrencySyncStatus = new CurrencySyncStatusInfo();
                                objCurrencySyncStatus.CurrencySyncStatusID = 0;
                                objCurrencySyncStatus.CurrencyProfileID = currencyProfileInfo.ProfileAuditID;
                                objCurrencySyncStatus.SubStoreSyncCode = SubStore.SyncCode;
                                objCurrencySyncStatus.VenueID = 0;
                                objCurrencySyncStatus.SyncStatus = 2;
                                objCurrencySyncStatus.CreatedBy = currencyProfileInfo.CreatedBy;
                                objCurrencySyncStatus.CreatedOn = currencyProfileInfo.CreatedOn;
                                objCurrencySyncStatus.CurrencyProfileStatus = deleted;


                                bool success = false;
                                DServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    success = client.SaveCurrencySyncStatusInfo(objCurrencySyncStatus);
                                });
                                dbTran.Rollback();
                                throw e;
                            }
                        }
                    }
                    //here i will insert into table that sync has been completed
                }
            }
            catch (Exception e)
            {
                //here i will insert into table that sync has been failed
                Exception outerException = new ProcessingException("Error occured in Currency Profile Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.CurrencyProfileProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {

                    CurrencyProfileInfo currencyProfileInfo = CommonUtility.DeserializeXML<CurrencyProfileInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {

                        using (var dbTran = dbContext.Database.BeginTransaction())
                        {
                            try
                            {

                                bool isdeleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                    {
                                        isdeleted = true;
                                    }
                                    else if (currencyProfileInfo.IsDeleted != null)
                                    {
                                        if (currencyProfileInfo.IsDeleted == true)
                                            isdeleted = true;
                                    }
                                }
                                else if (currencyProfileInfo.IsDeleted != null)
                                {
                                    if (currencyProfileInfo.IsDeleted == true)
                                        isdeleted = true;
                                }
                                

                                //System.Data.Entity.DbContextTransaction dbTran = context.Database.BeginTransaction( )
                                //var existingCurrencyProfiles = dbContext.CurrencyProfiles.Count(r => r.SyncCode == currencyProfileInfo.SyncCode);
                                var existingCurrencyProfiles = dbContext.CurrencyProfiles.Where(r => r.SyncCode == currencyProfileInfo.SyncCode).FirstOrDefault();

                                if (existingCurrencyProfiles != null)
                                {
                                    existingCurrencyProfiles.ProfileName = currencyProfileInfo.ProfileName;
                                    existingCurrencyProfiles.StartDate = currencyProfileInfo.startDate;
                                    existingCurrencyProfiles.EndDate = currencyProfileInfo.Enddate;
                                    existingCurrencyProfiles.IsActive = currencyProfileInfo.IsActive;
                                    existingCurrencyProfiles.CreatedBy = currencyProfileInfo.CreatedBy;
                                    existingCurrencyProfiles.CreatedOn = currencyProfileInfo.CreatedOn;
                                    existingCurrencyProfiles.SyncCode = currencyProfileInfo.SyncCode;
                                    existingCurrencyProfiles.IsDeleted = isdeleted;
                                    dbContext.SaveChanges();
                                    Int64 id = existingCurrencyProfiles.ProfileAuditID;


                                    var existingCurrencyProfileRates = dbContext.CurrencyProfileRates.Where(r => r.ProfileAuditID == id).ToList();

                                    foreach (var item in existingCurrencyProfileRates)
                                    {
                                        dbContext.CurrencyProfileRates.Remove(item);
                                    }
                                    dbContext.SaveChanges();


                                    foreach (var item in currencyProfileInfo.lstCurrencyProfileRate)
                                    {
                                        var vCurrencyProfileRate = new CurrencyProfileRate();
                                        vCurrencyProfileRate.ProfileAuditID = id;
                                        vCurrencyProfileRate.ExchangeRate = item.ExchangeRate;
                                        vCurrencyProfileRate.CurrencyCode = item.CurrencyCode;
                                        vCurrencyProfileRate.CurrencyName = item.CurrencyName;
                                        vCurrencyProfileRate.IsActive = item.IsActive;
                                        vCurrencyProfileRate.CreatedOn = item.CreatedOn;
                                        vCurrencyProfileRate.IsDeleted = item.IsDeleted;
                                        dbContext.CurrencyProfileRates.Add(vCurrencyProfileRate);
                                    }
                                    dbContext.SaveChanges();
                                }

                                bool deleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                        deleted = true;
                                }
                                var SubStore = dbContext.DG_SubStores.Where(r => r.DG_SubStore_pkey == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();

                                CurrencySyncStatusInfo objCurrencySyncStatus = new CurrencySyncStatusInfo();
                                objCurrencySyncStatus.CurrencySyncStatusID = 0;
                                objCurrencySyncStatus.CurrencyProfileID = currencyProfileInfo.ProfileAuditID;
                                objCurrencySyncStatus.SubStoreSyncCode = SubStore.SyncCode;
                                objCurrencySyncStatus.VenueID = 0;
                                objCurrencySyncStatus.SyncStatus = 1;
                                objCurrencySyncStatus.CreatedBy = currencyProfileInfo.CreatedBy;
                                objCurrencySyncStatus.CreatedOn = currencyProfileInfo.CreatedOn;
                                objCurrencySyncStatus.CurrencyProfileStatus = deleted;


                                bool success = false;
                                DServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    success = client.SaveCurrencySyncStatusInfo(objCurrencySyncStatus);
                                });

                                dbTran.Commit();
                            }
                            catch (Exception e)
                            {
                                bool deleted = false;
                                if (ChangeInfo.IsDeleted != null)
                                {
                                    if (ChangeInfo.IsDeleted == true)
                                        deleted = true;
                                }
                                var SubStore = dbContext.DG_SubStores.Where(r => r.DG_SubStore_pkey == ChangeInfo.SubStore.SubStoreId).FirstOrDefault();

                                CurrencySyncStatusInfo objCurrencySyncStatus = new CurrencySyncStatusInfo();
                                objCurrencySyncStatus.CurrencySyncStatusID = 0;
                                objCurrencySyncStatus.CurrencyProfileID = currencyProfileInfo.ProfileAuditID;
                                objCurrencySyncStatus.SubStoreSyncCode = SubStore.SyncCode;
                                objCurrencySyncStatus.VenueID = 0;
                                objCurrencySyncStatus.SyncStatus = 2;
                                objCurrencySyncStatus.CreatedBy = currencyProfileInfo.CreatedBy;
                                objCurrencySyncStatus.CreatedOn = currencyProfileInfo.CreatedOn;
                                objCurrencySyncStatus.CurrencyProfileStatus = deleted;


                                bool success = false;
                                DServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    success = client.SaveCurrencySyncStatusInfo(objCurrencySyncStatus);
                                });
                                dbTran.Rollback();
                                throw e;
                            }
                        }
                    }

                    //here i will insert into table that sync has been completed
                }
            }
            catch (Exception e)
            {
                //dbTran.Rollback();
                //here i will insert into table that sync has been failed
                Exception outerException = new ProcessingException("Error occured in Border Processor Update.", e);
                outerException.Source = "ClientDataProcessor.CurrencyProfileProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            throw new ProcessingException("Not Emplemented");
        }

    }
}
