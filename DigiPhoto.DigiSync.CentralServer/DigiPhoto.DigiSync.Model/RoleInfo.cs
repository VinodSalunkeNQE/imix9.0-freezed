﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class RoleInfo
    {
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public string SyncCode { get; set; }
        public List<string > ModulesSyncCodes { get; set; }
 
    }
}
