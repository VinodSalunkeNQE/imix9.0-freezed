﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class SyncSessionInfo
    {
        public long SyncSessionId { get; set; }
        public Guid SessionTokenId { get; set; }
        public string ClientStoreCode { get; set; }
        public string ClientSiteCode { get; set; }
        public string ClientVenueCode { get; set; }
        public DateTime SignInDate { get; set; }
        public DateTime? SignOutDate { get; set; }
        public long StartChangeTrackingId { get; set; }
        public long? EndChangeTrackingId { get; set; }
        public int Status { get; set; }
    }
}
