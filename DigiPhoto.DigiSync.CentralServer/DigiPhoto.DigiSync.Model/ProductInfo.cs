﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
   public class ProductInfo
    {
        public long ProductId { get; set; }  
        public string Name { get; set; }     
        public string Description { get; set; }
        public string Code { get; set; }
        public double? Price { get; set; }

       public int? CurrencyID { get; set; }

     
        public string ProductNumber { get; set; }

       
        public string ImagePath { get; set; }
      
        public int MaximumQuantity { get; set; }

        public bool ?IsBundled { get; set; }

        public bool? IsDiscountApplied { get; set; }

        public bool? IsAccessory { get; set; }

        public bool? IsPrimary { get; set; }
        

        public bool ?IsActive { get; set; }

       public DateTime ?CreatedDateTime { get; set; }

      //  public DateTime? ModifiedDateTime { get; set; }

      //  public long CreatedBy { get; set; }

       // public long? ModifiedBy { get; set; }
        public bool IsPackage { get; set; }
        public bool? IsBorder { get; set; }
        public bool? Orders_ProductType_Active { get; set; }
        public string SyncCode { get; set; }
        public string UserSyncCode { get; set; }
        public string LocationSyncCode { get; set; }
        public string CurrencySyncCode { get; set; }
      
        public bool? IsAvailable { get; set; }
        public Int32? Storeid { get; set; }
        public Int32? CreatedBy { get; set; }
        public bool? IsPersonalizedAR { get; set; }////latika for AR Personalised changed
        public List<PackageProductInfo> packageProductInfoList { get; set; }
    }
}
