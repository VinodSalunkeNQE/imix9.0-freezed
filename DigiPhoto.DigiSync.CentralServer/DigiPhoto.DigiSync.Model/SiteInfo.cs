﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class SiteInfo
    {
        public int SubStoreId { get; set; }
        public string SiteName { get; set; }
        public string SiteCode { get; set; }
        public StoreInfo Store { get; set; }

        public LocationInfo Location { get; set; }

        public List<LocationInfo> SubstorelocationDetails { get; set; }

        public string HotFolderPath { get; set; }

        public string DG_SiteCode { get; set; }

        public SiteInfo LogicalSubStoreDetails { get; set; }

        public bool IsLogicalSubStore { get; set; }


    }
}
