﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class CardLimitInfo
    {
        //public bool IsCardPurchased { get; set; }
        public int Allowed { get; set; }
        public int Associated { get; set; }
        public bool ValidCard { get; set; }
    }
}
