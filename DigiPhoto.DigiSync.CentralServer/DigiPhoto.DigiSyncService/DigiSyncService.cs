﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ClientDataProcessor;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using System.Configuration;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.IMIX.Business;


namespace DataSyncService
{
    public partial class DigiSyncService : ServiceBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer timer;
        private static bool IsSyncEnabled = false;
        private static int SyncServiceInterval = 5;
        private static int SyncDataDelay = 1;
        private static bool IsSyncServerOnline = false;
        private static List<iMIXconfigurationLocationValue> iMIXconfigurationLocationWiseValue;
        private static bool IsPartialEdited = false;



        public DigiSyncService()
        {
            InitializeComponent();
        }
//////change made by latika for AR Personalised
        public void SyncOrdertEST()
        {
            SyncOrder();
        }
		//////end 
        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {
                    SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
                    CheckServerIsOnline();
                    IsSyncEnabled = SyncController.IsSynceEnabled();
                    SyncServiceInterval = SyncController.SyncServiceInterval();
                    SyncDataDelay = SyncController.SyncDataDelay();
                    iMIXconfigurationLocationWiseValue = SyncController.PartialEditedEnabled();

                    SyncController.FailedOrderConfigValue();
                    SyncController.SetSyncStatusZero();
                    this.timer = new System.Timers.Timer();
                    this.timer.Interval = 1000 * SyncServiceInterval;
                    this.timer.AutoReset = true;
                    this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    this.timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }

                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);

            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                //ExitCode = 13816;
                this.Stop();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                this.timer.Stop();
                // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // svcPosinfoBusiness.ServiceStart(false);
                SyncOrder();

            }
            catch (Exception ex)
            {

                log.Error("timer_Elapsed:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.timer.Start();
            }
            finally
            {
                this.timer.Start();
            }
        }
        public static void SyncOrder()
        {
            //SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
            //CheckServerIsOnline();
            //IsSyncEnabled = SyncController.IsSynceEnabled();
            //SyncServiceInterval = SyncController.SyncServiceInterval();
            //SyncDataDelay = SyncController.SyncDataDelay();
            //iMIXconfigurationLocationWiseValue = SyncController.PartialEditedEnabled();
            if (!IsSyncEnabled)
            {
                log.Info("Sync is not Enabled !");
            }
            CheckServerIsOnline();
            if (IsSyncEnabled && IsSyncServerOnline)
            {
                Pull();
                Push();
            }

        }

        static void CheckServerIsOnline()
        {
            IsSyncServerOnline = false;
            string serverdate = string.Empty;
            try
            {
                DServiceProxy<IDataSyncService>.Use(client =>
                {
                    serverdate = client.GetServerDate();
                });
            }
            catch (Exception ex)
            {
                SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
                serverdate = string.Empty;
                log.Error("CheckServerIsOnline:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            if (!String.IsNullOrEmpty(serverdate))
            {
                IsSyncServerOnline = true;
            }

        }
        static void Pull()
        {
            if (log.IsInfoEnabled)
                log.Info("Pull Start");
            try
            {
                List<SiteInfo> sites = SyncController.GetSites();
                foreach (var site in sites)
                {
                    if (string.IsNullOrEmpty(site.SiteCode))
                    {
                        log.Error("SubStore SyncCode is not set, For Syncing set the SubStore SyncCode");
                        continue;
                    }
                    if (log.IsInfoEnabled)
                        log.Info("SubStore Name/Code:" + site.SiteName + "/" + site.SiteCode);
                    //Get Last ChnageDetails By SubStore
                    ChangeInfo lastChangeInfo = SyncController.GetLastChangeDetail(site);
                    ChangeInfo changeInfo = null;
                    do
                    {
                        if (lastChangeInfo != null)
                        {
                            changeInfo = SyncController.GetLastChangeDetail(site);  //lastChangeInfo;
                        }
                        else
                        {
                            lastChangeInfo = new ChangeInfo
                            {
                                ChangeTrackingId = 0,
                                SubStore = site
                            };
                        }
                        if (changeInfo != null)
                        {
                            lastChangeInfo = changeInfo;
                        }

                        //requestHandler = new SyncRequestHandler();
                        //changeInfo = requestHandler.GetNextChangeInfo(lastChangeInfo);

                        DServiceProxy<IDataSyncService>.Use(client =>
                        {
                            changeInfo = client.GetNextChangeInfo(lastChangeInfo);
                        });
                        if (changeInfo == null)
                        {
                            if (log.IsInfoEnabled)
                                log.Info("Change Tracking Not Found");
                        }
                        if (changeInfo != null)
                        {
                            if (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId || (
                                (changeInfo.ChangeTrackingId <= lastChangeInfo.ChangeTrackingId)
                                && (changeInfo.ApplicationObjectId == 1 || changeInfo.ApplicationObjectId == 2 || changeInfo.ApplicationObjectId == 3)
                                && changeInfo.ChangeAction == 3))
                            {
                                changeInfo.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    SubStoreId = site.SubStoreId,
                                    HotFolderPath = site.HotFolderPath
                                };
                                Int32 proceesingStatus = 0;
                                bool success = false;
                                try
                                {
                                    IProcesser processor = ProcessorFactory.Create(changeInfo.ApplicationObjectId);
                                    processor.Process(changeInfo);
                                    //if (changeInfo.ChangeAction != 3 && changeInfo.DownloadDetail != null)
                                    if (changeInfo.ChangeAction == 1 && changeInfo.DownloadDetail != null)
                                    {
                                        DownloadManager.Downloadfile(changeInfo.DownloadDetail.DownloadFilePath, changeInfo.ApplicationObjectId, site);
                                    }
                                    proceesingStatus = 1;
                                }
                                catch (Exception ex)
                                {
                                    proceesingStatus = -1;
                                    log.Error("Pull:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                }
                                finally
                                {
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.updateChangeTrackingHistory(changeInfo, site, proceesingStatus);
                                    });
                                }
                            }
                        }
                        //If location is updated from centralserver then reinitialize 
                        if (changeInfo.ApplicationObjectId == 13)
                            SyncOrder();
                    } while (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId);

                }
            }
            catch (Exception ex)
            {
                log.Error("Pull:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            if (log.IsInfoEnabled)
                log.Info("Pull End");

        }
        #region Sync Service Enhancement - Ashirwad
        static void Push()
        {
            string errorPhotoId = string.Empty;
            bool flag = true;
            bool SuccessFlag = true;
            if (log.IsInfoEnabled)
                log.Info("Push Start");
            List<ChangeInfo> changeList = null;
            try
            {
                List<SiteInfo> sites = SyncController.GetSites();
                foreach (var site in sites)
                {
                    if (log.IsInfoEnabled)
                        log.Info("SubStore Name/Code:" + site.SiteName + "/" + site.SiteCode);
                    //Get the ChangeInfo from database.
                    changeList = SyncController.GetNextChangeInfo(SyncDataDelay);



                    if (changeList == null)
                    {
                        if (log.IsInfoEnabled)
                            log.Info("Change Tracking Not Found");
                    }
                    if (changeList != null && changeList.Count > 0)
                    {
                        foreach (ChangeInfo change in changeList)
                        {
                            //check if order is cancel -- Clear Upload LIST (do not load photos to cloudinary)-- ASHIRWAD
                            SuccessFlag = true;//important
                            flag = true;
                            bool? IsOrderCancel = false;
                            IsOrderCancel = SyncController.GetOrderCancelStatus(change.ObjectValueId);
                            if (IsOrderCancel != null && IsOrderCancel == true)
                            {
                                change.UploadList = null;
                            }

                            try
                            {
                                if (log.IsInfoEnabled)
                                    log.Info("Change Tracking Id:" + change.ChangeTrackingId);
                                change.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    Store = new StoreInfo
                                    {
                                        StoreCode = site.Store.StoreCode,
                                        StoreName = site.Store.StoreName
                                    }
                                };

                                if (!string.IsNullOrEmpty(change.dataXML))
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    try
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressSavingIncomingChangeUploadedDataOn);
                                        DServiceProxy<IDataSyncService>.Use(client =>
                                        {
                                            success = client.SaveChangeInfo(change);
                                        });

                                        log.Info("Data saved in IncomingChange : " + success);

                                    }
                                    catch
                                    {
                                        log.Error("Execption while saving Data in IncomingChange : ");
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.ErrorSavingIncomingChange);
                                        SuccessFlag = false;
                                        continue;
                                    }
                                    if (success)
                                    {
                                        //Start Compressing photos of this Order.
                                        if (change.UploadList != null)
                                        {
                                            ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                if (upload.MediaType == 1)
                                                {
                                                    try
                                                    {
                                                        IsPartialEdited = false;
                                                        if (!Directory.Exists(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath)))
                                                        {
                                                            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                        }
                                                        if (iMIXconfigurationLocationWiseValue != null && iMIXconfigurationLocationWiseValue.Count > 0)
                                                        {
                                                            var res = iMIXconfigurationLocationWiseValue.Where(x => x.SubstoreId == upload.SubstoreId && x.LocationId == upload.LocationId).FirstOrDefault();
                                                            if (res != null)
                                                                IsPartialEdited = true;
                                                        }
                                                        if (IsPartialEdited)
                                                        {
                                                            if (File.Exists(upload.PartialEditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.PartialEditedUploadFilePath;
                                                            }
                                                            else if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }

                                                        }

                                                        if (!File.Exists(upload.UploadFilePath))
                                                        {
                                                            flag = false;
                                                            log.Info("Image Not found : " + upload.UploadFilePath);
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedImageNotFound);
                                                            SuccessFlag = false;
                                                            continue;
                                                        }
                                                        imgCompress.GetImage = new System.Drawing.Bitmap(upload.UploadFilePath);
                                                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Save(System.IO.Path.GetFileName(upload.UploadFilePath), System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));

                                                        log.Info("Compressing Image : " + upload.UploadFilePath);
                                                        if (SuccessFlag)
                                                        {
                                                            log.Info("status updated InProgressCompressingImages");
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressCompressingImages);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        flag = false;
                                                        log.Error("Push:: Error Compressing Image: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedCompressingImages);
                                                        SuccessFlag = false;
                                                        //continue;
                                                    }
                                                }


                                            }

                                            int uploadCount = 0;
                                            //Upload Photos.
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                try
                                                {
                                                    log.Info("Upload Main Photo");
                                                    string uploadFilePath = string.Empty;
                                                    //Upload Main Photo
                                                    if (upload.MediaType == 1)
                                                    {
                                                        uploadFilePath = upload.UploadCompressedFilePath;
                                                    }
                                                    else
                                                    {
                                                        uploadFilePath = upload.UploadFilePath;
                                                    }
                                                    if (File.Exists(uploadFilePath))
                                                    {
                                                        //System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadCompressedFilePath);
                                                        //RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        //using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadCompressedFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        //{
                                                        //    uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadCompressedFilePath);
                                                        //    uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                        //    uploadRequestInfo.Length = fileInfo.Length;
                                                        //    uploadRequestInfo.FileByteStream = stream;
                                                        //    DServiceProxy<IFileService>.Use(client =>
                                                        //    {
                                                        //        client.UploadFile(uploadRequestInfo);
                                                        //    });
                                                        //}



                                                        //Move below lines before loop.
                                                        //string cloundApiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"].ToString();

                                                        //Search Cloudinary Record for OrderID, PhotoID, QRCode
                                                        var cloudinaryDetails = SyncController.GetCloudinaryDetails(upload);
                                                        if (cloudinaryDetails != null && cloudinaryDetails.CloudinaryStatusID == 1)
                                                        {
                                                            //ALREADY SUCCESSFULLY UPLOADED
                                                            log.Info("ALREADY SUCCESSFULLY UPLOADED");
                                                            uploadCount++;
                                                            continue;
                                                        }
                                                        else
                                                        {

                                                            string filePath = ConfigurationManager.AppSettings["DestinationSiteDirectoryPath"];
                                                            int defaultWidth = ImageHelper.GetDefaultWidth;
                                                            int defaultHeight = ImageHelper.GetDefaultHeight;

                                                            UploadFileInfo obj = new UploadFileInfo
                                                            {
                                                                Title = System.IO.Path.GetFileName(uploadFilePath),
                                                                ImagePath = uploadFilePath,
                                                                TargetDirectory = filePath + upload.SaveFolderPath,
                                                                ImageDefaultHeight = defaultHeight,
                                                                ImageDefaultWidth = defaultWidth,
                                                                MediaType = upload.MediaType,
                                                                PhotoId = upload.PhotoId

                                                            };
                                                            CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, out errorPhotoId);

                                                            if (cloudinaryDetails != null)
                                                            {
                                                                //Update Cloudinary upload status in CloudinaryDtl table for same CloudinaryInfoID
                                                                cloudinaryDetails.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                                cloudinaryDetails.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                                cloudinaryDetails.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                                cloudinaryDetails.RetryCount += 1;
                                                                cloudinaryDetails.ModifiedDateTime = DateTime.Now;
                                                                cloudinaryDetails.Height = objCloudinaryUploadInfo.Height;
                                                                cloudinaryDetails.Width = objCloudinaryUploadInfo.Width;
                                                                cloudinaryDetails.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                                SyncController.UpdateCloudinaryUploadDetails(cloudinaryDetails);
                                                            }
                                                            else
                                                            {
                                                                //update cloudinary upload status in CloudinaryDtl table
                                                                CloudinaryDtl objCloudinaryDtl = new CloudinaryDtl();
                                                                objCloudinaryDtl.PhotoID = upload.PhotoId;
                                                                objCloudinaryDtl.SourceImageID = obj.Title;
                                                                objCloudinaryDtl.AddedBy = "sa";
                                                                objCloudinaryDtl.CreatedDateTime = DateTime.Now;
                                                                objCloudinaryDtl.ModifiedDateTime = DateTime.Now;
                                                                objCloudinaryDtl.ModifiedBy = "webusers";
                                                                objCloudinaryDtl.IsActive = true;
                                                                objCloudinaryDtl.SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Cloudinary).ToString().PadLeft(2, '0'), site.Store.Country.CountryCode, site.Store.StoreCode, "00");
                                                                objCloudinaryDtl.OrderId = upload.OrderId;
                                                                objCloudinaryDtl.IdentificationCode = upload.IdentificationCode;
                                                                objCloudinaryDtl.UploadCompressedFilePath = uploadFilePath;
                                                                objCloudinaryDtl.TargetDirectory = obj.TargetDirectory;

                                                                objCloudinaryDtl.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                                objCloudinaryDtl.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                                objCloudinaryDtl.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                                objCloudinaryDtl.Width = objCloudinaryUploadInfo.Width;
                                                                objCloudinaryDtl.Height = objCloudinaryUploadInfo.Height;
                                                                objCloudinaryDtl.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                                SyncController.InsertCloudinaryUploadDetails(objCloudinaryDtl);
                                                            }


                                                            //change regarding partial sync showing wrong status --12 march 19
                                                            if (objCloudinaryUploadInfo.CloudinaryStatusID == 1)
                                                            {
                                                                uploadCount++;
                                                            }
                                                            else
                                                            {
                                                                SuccessFlag = false;
                                                                flag = false;
                                                                log.Error("Push:: Error Uploading Image to Cloudinary: CloudinaryDtl.PhotoID :" + upload.PhotoId);
                                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadPhotosCloudinary);
                                                            }
                                                        }


                                                        log.Info("Uploading Image to Cloudinary : " + upload.UploadFilePath);
                                                       
                                                        if (SuccessFlag)
                                                        {
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressUploadPhotosCloudinary);
                                                        }

                                                    }
                                                    else
                                                    {

                                                        flag = false;
                                                        SuccessFlag = false;
                                                        log.Info("Image Not found - Failed CloudinaryUploading: " + upload.UploadFilePath);
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedImageNotFound);
                                                    }


                                                }
                                                catch (Exception ex)
                                                {
                                                    SuccessFlag = false;
                                                    flag = false;
                                                    log.Error("Push:: Error Uploading Image to Cloudinary: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadPhotosCloudinary);
                                                    //continue;
                                                }
                                            }

                                            log.Info("After continue ALREADY SUCCESSFULLY UPLOADED");
                                            if (uploadCount > 0 && uploadCount < change.UploadList.Count)
                                            {
                                                log.Info("Partially Uploaded Image to Cloudinary : ");
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.PartillyFailedUploading);
                                            }
                                            //Delete Order Number folder.
                                            if (change.UploadList.Count > 0)
                                            {
                                                if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)))
                                                    Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                            }

                                        }
                                        try
                                        {
                                            // Upload Border , BackGround and Graphic
                                            log.Info("Upload Border , BackGround and Graphic");
                                            if (change.UploadFile != null)
                                            {
                                                string Folderpath = string.Empty;
                                                UploadFile upload = new UploadFile();
                                                upload = change.UploadFile;

                                                if (upload.FileType == 1)
                                                {
                                                    upload.SaveFolderPath = "Border";
                                                    upload.SaveThumbnailFolderPath = ("BorderThumbnails");
                                                }
                                                else if (upload.FileType == 2)
                                                {
                                                    upload.SaveFolderPath = "BackGround";
                                                    upload.SaveThumbnailFolderPath = "BackGroundThumbnails";
                                                }
                                                else if (upload.FileType == 3)
                                                {
                                                    upload.SaveFolderPath = "Graphics";
                                                    upload.SaveThumbnailFolderPath = "";
                                                }
                                                if (File.Exists(upload.UploadFilePath))
                                                {
                                                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadFilePath);
                                                    RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                    using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                    {
                                                        uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadFilePath);
                                                        uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                        uploadRequestInfo.Length = fileInfo.Length;
                                                        uploadRequestInfo.FileByteStream = stream;
                                                        DServiceProxy<IFileService>.Use(client =>
                                                        {
                                                            client.UploadFile(uploadRequestInfo);
                                                        });
                                                    }
                                                }
                                                //Upload Thumbnails
                                                if (upload.FileType == 1 || upload.FileType == 2)
                                                {
                                                    if (File.Exists(upload.UploadThumbnailFilePath))
                                                    {
                                                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadThumbnailFilePath);
                                                        RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadThumbnailFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        {
                                                            uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadThumbnailFilePath);
                                                            uploadRequestInfo.SaveFolderPath = upload.SaveThumbnailFolderPath;
                                                            uploadRequestInfo.Length = fileInfo.Length;
                                                            uploadRequestInfo.FileByteStream = stream;
                                                            DServiceProxy<IFileService>.Use(client =>
                                                            {
                                                                client.UploadFile(uploadRequestInfo);
                                                            });
                                                        }
                                                    }
                                                }
                                                flag = true;

                                                log.Info("Uploaded " + upload.SaveFolderPath + "to Cloudinary : " + upload.UploadFilePath);
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedGraphicsToCloud);
                                            }

                                        }
                                        catch
                                        {
                                            log.Error("Error in Graphics Uploading: " + change.UploadFile);
                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadedGraphicsToCloud);
                                            continue;
                                        }
                                        //Delete Order Number folder.
                                        if (change.UploadList != null && change.UploadList.Count > 0)
                                        {
                                            if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)))
                                                Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                        }

                                        log.Info("Towards final Status");
                                        if (SuccessFlag)
                                        {
                                            if (flag == true)
                                            {
                                                log.Info("Final Status=flag="+ flag);
                                                //photo uploaded to cloudinary 3
                                                if (change.ApplicationObjectId == 14)
                                                {
                                                    if (IsOrderCancel != null && IsOrderCancel == true)
                                                    {
                                                        log.Info("Final Status=5");
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerandCancel);
                                                    }
                                                    else
                                                    {
                                                        log.Info("Final Status=3");
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerandCloudinary);
                                                    }
                                                }
                                                else
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                                }
                                                if (log.IsInfoEnabled)
                                                    log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                            }
                                            else
                                            {
                                                //photo not uploaded -3
                                                if (change.ApplicationObjectId == 14)
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerButFailedonCloudinary);
                                                }
                                                else
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                                }
                                                //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                                //Exception ex = new Exception("Error in uploadloading photos to cloudinary");
                                                //SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                                if (log.IsInfoEnabled)
                                                    log.Error("Sync failed for ChangeTrackingId : " + change.ChangeTrackingId);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }

                                }
                                else
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                        if (log.IsInfoEnabled)
                                            log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.ToString().ToUpper().Contains("NO ENDPOINT") || ex.Message.ToString().ToUpper().Contains("LONGER TIMEOUT"))
                                {
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                    break;
                                }
                                else
                                {
                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                    //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                    //SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TBD
                //Mark this change as sync error
                //SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            log.Info("Push End");            
        }

        #endregion
        static void PushOLD()
        {
            string errorPhotoId = string.Empty;
            bool flag = true;
            if (log.IsInfoEnabled)
                log.Info("Push Start");
            List<ChangeInfo> changeList = null;
            try
            {
                List<SiteInfo> sites = SyncController.GetSites();
                foreach (var site in sites)
                {
                    if (log.IsInfoEnabled)
                        log.Info("SubStore Name/Code:" + site.SiteName + "/" + site.SiteCode);
                    //Get the ChangeInfo from database.
                    changeList = SyncController.GetNextChangeInfo(SyncDataDelay);
                    if (changeList == null)
                    {
                        if (log.IsInfoEnabled)
                            log.Info("Change Tracking Not Found");
                    }
                    if (changeList != null && changeList.Count > 0)
                    {
                        foreach (ChangeInfo change in changeList)
                        {
                            try
                            {
                                if (log.IsInfoEnabled)
                                    log.Info("Change Tracking Id:" + change.ChangeTrackingId);
                                change.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    Store = new StoreInfo
                                    {
                                        StoreCode = site.Store.StoreCode,
                                        StoreName = site.Store.StoreName
                                    }
                                };

                                if (!string.IsNullOrEmpty(change.dataXML))
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        //Start Compressing photos of this Order.
                                        if (change.UploadList != null)
                                        {
                                            ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                if (upload.MediaType == 1)
                                                {
                                                    try
                                                    {
                                                        IsPartialEdited = false;
                                                        if (!Directory.Exists(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath)))
                                                        {
                                                            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                        }
                                                        if (iMIXconfigurationLocationWiseValue != null && iMIXconfigurationLocationWiseValue.Count > 0)
                                                        {
                                                            var res = iMIXconfigurationLocationWiseValue.Where(x => x.SubstoreId == upload.SubstoreId && x.LocationId == upload.LocationId).FirstOrDefault();
                                                            if (res != null)
                                                                IsPartialEdited = true;
                                                        }
                                                        if (IsPartialEdited)
                                                        {
                                                            if (File.Exists(upload.PartialEditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.PartialEditedUploadFilePath;
                                                            }
                                                            else if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }
                                                        }
                                                        imgCompress.GetImage = new System.Drawing.Bitmap(upload.UploadFilePath);
                                                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Save(System.IO.Path.GetFileName(upload.UploadFilePath), System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        flag = false;
                                                        log.Error("Push:: Corrupt image file: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                                    }
                                                }
                                            }

                                            //Upload Photos.
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                string uploadFilePath = string.Empty;
                                                //Upload Main Photo
                                                if (upload.MediaType == 1)
                                                {
                                                    uploadFilePath = upload.UploadCompressedFilePath;
                                                }
                                                else
                                                {
                                                    uploadFilePath = upload.UploadFilePath;
                                                }
                                                if (File.Exists(uploadFilePath))
                                                {
                                                    //System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadCompressedFilePath);
                                                    //RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                    //using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadCompressedFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                    //{
                                                    //    uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadCompressedFilePath);
                                                    //    uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                    //    uploadRequestInfo.Length = fileInfo.Length;
                                                    //    uploadRequestInfo.FileByteStream = stream;
                                                    //    DServiceProxy<IFileService>.Use(client =>
                                                    //    {
                                                    //        client.UploadFile(uploadRequestInfo);
                                                    //    });
                                                    //}



                                                    //Move below lines before loop.
                                                    //string cloundApiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"].ToString();

                                                    //Search Cloudinary Record for OrderID, PhotoID, QRCode
                                                    int strisPersonalized = 0; //  //////change made by latika for AR pesonalised
                                                    DServiceProxy<IDataSyncService>.Use(client =>
                                                    {
                                                        strisPersonalized = client.CheckIsPersonalized(upload.OrderId);
                                                    });
													//END by latika
                                                    var cloudinaryDetails = SyncController.GetCloudinaryDetails(upload);
                                                    if (cloudinaryDetails != null && cloudinaryDetails.CloudinaryStatusID == 1)
                                                    {
                                                        //ALREADY SUCCESSFULLY UPLOADED
                                                        continue;
                                                    }
                                                    else
                                                    {

                                                        string filePath = ConfigurationManager.AppSettings["DestinationSiteDirectoryPath"];
                                                        int defaultWidth = ImageHelper.GetDefaultWidth;
                                                        int defaultHeight = ImageHelper.GetDefaultHeight;

                                                        UploadFileInfo obj = new UploadFileInfo
                                                        {
                                                            Title = System.IO.Path.GetFileName(uploadFilePath),
                                                            ImagePath = uploadFilePath,
                                                            TargetDirectory = filePath + upload.SaveFolderPath,
                                                            ImageDefaultHeight = defaultHeight,
                                                            ImageDefaultWidth = defaultWidth,
                                                            MediaType = upload.MediaType,
                                                            PhotoId = upload.PhotoId

                                                        };
                                                        CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, out errorPhotoId);

                                                        if (cloudinaryDetails != null)
                                                        {
                                                            //Update Cloudinary upload status in CloudinaryDtl table for same CloudinaryInfoID
                                                            cloudinaryDetails.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                            cloudinaryDetails.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                            cloudinaryDetails.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                            cloudinaryDetails.RetryCount += 1;
                                                            cloudinaryDetails.ModifiedDateTime = DateTime.Now;
                                                            cloudinaryDetails.Height = objCloudinaryUploadInfo.Height;
                                                            cloudinaryDetails.Width = objCloudinaryUploadInfo.Width;
                                                            cloudinaryDetails.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                            SyncController.UpdateCloudinaryUploadDetails(cloudinaryDetails);
                                                        }
                                                        else
                                                        {
                                                            //update cloudinary upload status in CloudinaryDtl table
                                                            CloudinaryDtl objCloudinaryDtl = new CloudinaryDtl();
                                                            objCloudinaryDtl.PhotoID = upload.PhotoId;
                                                            objCloudinaryDtl.SourceImageID = obj.Title;
                                                            objCloudinaryDtl.AddedBy = "sa";
                                                            objCloudinaryDtl.CreatedDateTime = DateTime.Now;
                                                            objCloudinaryDtl.ModifiedDateTime = DateTime.Now;
                                                            objCloudinaryDtl.ModifiedBy = "webusers";
                                                            objCloudinaryDtl.IsActive = true;
                                                            objCloudinaryDtl.SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Cloudinary).ToString().PadLeft(2, '0'), site.Store.Country.CountryCode, site.Store.StoreCode, "00");
                                                            objCloudinaryDtl.OrderId = upload.OrderId;
                                                            objCloudinaryDtl.IdentificationCode = upload.IdentificationCode;
                                                            objCloudinaryDtl.UploadCompressedFilePath = uploadFilePath;
                                                            objCloudinaryDtl.TargetDirectory = obj.TargetDirectory;

                                                            objCloudinaryDtl.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                            objCloudinaryDtl.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                            objCloudinaryDtl.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                            objCloudinaryDtl.Width = objCloudinaryUploadInfo.Width;
                                                            objCloudinaryDtl.Height = objCloudinaryUploadInfo.Height;
                                                            objCloudinaryDtl.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                            SyncController.InsertCloudinaryUploadDetails(objCloudinaryDtl);
                                                        }
                                                        //if (errorPhotoId == "")
                                                        //{
                                                        //    if (File.Exists(uploadFilePath))
                                                        //        File.Delete(uploadFilePath);
                                                        //}
                                                        //else
                                                        //{
                                                        //    flag = false;
                                                        //}
                                                        if (!string.IsNullOrEmpty(errorPhotoId))
                                                            flag = false;
                                                    }
                                                }
                                                //}
                                                //else
                                                //{
                                                //    //Upload Main Videos
                                                //    if (File.Exists(upload.UploadFilePath))
                                                //    {
                                                //        System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadFilePath);
                                                //        RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                //        using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                //        {
                                                //            uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadFilePath);
                                                //            uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                //            uploadRequestInfo.Length = fileInfo.Length;
                                                //            uploadRequestInfo.FileByteStream = stream;
                                                //            DServiceProxy<IFileService>.Use(client =>
                                                //            {
                                                //                client.UploadFile(uploadRequestInfo);
                                                //            });
                                                //        }
                                                //    }
                                                //}
                                                //Upload Thumbnails
                                                //if (File.Exists(upload.UploadThumbnailFilePath))
                                                //{
                                                //    System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadThumbnailFilePath);
                                                //    RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                //    using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadThumbnailFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                //    {
                                                //        uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadThumbnailFilePath);
                                                //        uploadRequestInfo.SaveFolderPath = upload.SaveThumbnailFolderPath;
                                                //        uploadRequestInfo.Length = fileInfo.Length;
                                                //        uploadRequestInfo.FileByteStream = stream;
                                                //        DServiceProxy<IFileService>.Use(client =>
                                                //        {
                                                //            client.UploadFile(uploadRequestInfo);
                                                //        });
                                                //    }
                                                //}
                                            }
                                            //Delete Order Number folder.
                                            if (change.UploadList.Count > 0)
                                            {
                                                if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)) && flag == true)
                                                    Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                            }

                                        }
                                        // Upload Border , BackGround and Graphic
                                        if (change.UploadFile != null)
                                        {
                                            string Folderpath = string.Empty;
                                            UploadFile upload = new UploadFile();
                                            upload = change.UploadFile;

                                            if (upload.FileType == 1)
                                            {
                                                upload.SaveFolderPath = "Border";
                                                upload.SaveThumbnailFolderPath = ("BorderThumbnails");
                                            }
                                            else if (upload.FileType == 2)
                                            {
                                                upload.SaveFolderPath = "BackGround";
                                                upload.SaveThumbnailFolderPath = "BackGroundThumbnails";
                                            }
                                            else if (upload.FileType == 3)
                                            {
                                                upload.SaveFolderPath = "Graphics";
                                                upload.SaveThumbnailFolderPath = "";
                                            }
                                            if (File.Exists(upload.UploadFilePath))
                                            {
                                                System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadFilePath);
                                                RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                {
                                                    uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadFilePath);
                                                    uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                    uploadRequestInfo.Length = fileInfo.Length;
                                                    uploadRequestInfo.FileByteStream = stream;
                                                    DServiceProxy<IFileService>.Use(client =>
                                                    {
                                                        client.UploadFile(uploadRequestInfo);
                                                    });
                                                }
                                            }
                                            //Upload Thumbnails
                                            if (upload.FileType == 1 || upload.FileType == 2)
                                            {
                                                if (File.Exists(upload.UploadThumbnailFilePath))
                                                {
                                                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadThumbnailFilePath);
                                                    RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                    using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadThumbnailFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                    {
                                                        uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadThumbnailFilePath);
                                                        uploadRequestInfo.SaveFolderPath = upload.SaveThumbnailFolderPath;
                                                        uploadRequestInfo.Length = fileInfo.Length;
                                                        uploadRequestInfo.FileByteStream = stream;
                                                        DServiceProxy<IFileService>.Use(client =>
                                                        {
                                                            client.UploadFile(uploadRequestInfo);
                                                        });
                                                    }
                                                }
                                            }
                                            flag = true;
                                        }


                                        //Delete Order Number folder.
                                        if (change.UploadList != null && change.UploadList.Count > 0)
                                        {
                                            if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)) && flag == true)
                                                Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                        }

                                        if (flag == true)
                                        {
                                            //photo uploaded to cloudinary 3
                                            if (change.ApplicationObjectId == 14)
                                            {
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerandCloudinary);
                                            }
                                            else
                                            {
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                            }
                                            if (log.IsInfoEnabled)
                                                log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                        }
                                        else
                                        {
                                            //photo not uploaded -3
                                            if (change.ApplicationObjectId == 14)
                                            {
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerButFailedonCloudinary);
                                            }
                                            else
                                            {
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                            }
                                            //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                            Exception ex = new Exception("Error in uploadloading photos to cloudinary");
                                            SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                            if (log.IsInfoEnabled)
                                                log.Info("Sync failed for ChangeTrackingId : " + change.ChangeTrackingId);
                                        }
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }

                                }
                                else
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                        if (log.IsInfoEnabled)
                                            log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.ToString().ToUpper().Contains("NO ENDPOINT") || ex.Message.ToString().ToUpper().Contains("LONGER TIMEOUT"))
                                {
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                    break;
                                }
                                else
                                {
                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                    //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                    SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TBD
                //Mark this change as sync error
                //SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            log.Info("Push End");
        }
        protected override void OnStop()
        {
            SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
        //public void startservice()
        //{

        //    try
        //    {

        //        //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
        //        //string ret = svcPosinfoBusiness.ServiceStart(false);
        //        ////ret = 1;
        //        //if (string.IsNullOrEmpty(ret))
        //        //{
        //        SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
        //        CheckServerIsOnline();
        //        IsSyncEnabled = SyncController.IsSynceEnabled();
        //        SyncServiceInterval = SyncController.SyncServiceInterval();
        //        SyncDataDelay = SyncController.SyncDataDelay();
        //        //IsFailedOrderResyncActive = SyncController.IsFailedOrderResyncActive();
        //        //FailedOrderResyncDateTime = SyncController.FailedOrderResyncDateTime();
        //        //FailedOrderIntervalCount = SyncController.FailedOrderIntervalCount();
        //        //FailedOrderIntervalType = SyncController.FailedOrderIntervalType();
        //        //FailedOrderMaxRetryCount = SyncController.FailedOrderMaxRetryCount();
        //        SyncController.FailedOrderConfigValue();
        //        SyncController.SetSyncStatusZero();
        //        SyncOrder();


        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
        //        //ExitCode = 13816;
        //        this.Stop();
        //    }
        //}
    }
}





