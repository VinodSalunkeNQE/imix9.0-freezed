namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Orders_Details
    {
        public DG_Orders_Details()
        {
            PhotoAlbumPrintOrders = new HashSet<PhotoAlbumPrintOrder>();
        }

        [Key]
        public int DG_Orders_LineItems_pkey { get; set; }

        public int? DG_Orders_ID { get; set; }

        public string DG_Photos_ID { get; set; }

        public string DG_Photos_ID_UnSold { get; set; }

        public DateTime? DG_Orders_LineItems_Created { get; set; }

        public string DG_Orders_LineItems_DiscountType { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_LineItems_DiscountAmount { get; set; }

        public int? DG_Orders_LineItems_Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_Details_Items_UniPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_Details_Items_TotalCost { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_Details_Items_NetPrice { get; set; }

        public int? DG_Orders_Details_ProductType_pkey { get; set; }

        public int? DG_Orders_Details_LineItem_ParentID { get; set; }

        public int? DG_Orders_Details_LineItem_PrinterReferenceID { get; set; }

        public bool? DG_Photos_Burned { get; set; }

        public int? DG_Order_SubStoreId { get; set; }

        public int? IsPostedToServer { get; set; }

        public int? DG_Order_IdentifierType { get; set; }

        [StringLength(50)]
        public string DG_Order_ImageUniqueIdentifier { get; set; }

        public int? DG_Order_Status { get; set; }

        public string SyncCode { get; set; }
        public virtual ICollection<PhotoAlbumPrintOrder> PhotoAlbumPrintOrders { get; set; }
    }
}
