namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_BackGround
    {
        [Key]
        public int DG_Background_pkey { get; set; }

        public int DG_Product_Id { get; set; }

        public string DG_BackGround_Image_Name { get; set; }

        public string DG_BackGround_Image_Display_Name { get; set; }

        public int? DG_BackGround_Group_Id { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public bool? DG_Background_IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
