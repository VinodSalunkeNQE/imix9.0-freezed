namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Albums
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DG_Albums_pkey { get; set; }

        public int? DG_Albums_Name { get; set; }

        public DateTime? DG_Albums_CreatedOn { get; set; }

        public int? DG_Albums_CreatedBy { get; set; }
    }
}
