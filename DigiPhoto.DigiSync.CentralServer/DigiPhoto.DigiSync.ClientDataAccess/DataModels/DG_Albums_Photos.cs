namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Albums_Photos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DG_Albums_Photos_pkey { get; set; }

        public int? DG_Albums_pkey { get; set; }

        public int? DG_Photos_pkey { get; set; }

        public DateTime? DG_Albums_Photos_CreatedOn { get; set; }

        public int? DG_Albums_Photos_CreatedBy { get; set; }
    }
}
