namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_AssociatedPrinters
    {
        [Key]
        public int DG_AssociatedPrinters_Pkey { get; set; }

        [Required]
        public string DG_AssociatedPrinters_Name { get; set; }

        public int DG_AssociatedPrinters_ProductType_ID { get; set; }

        public bool DG_AssociatedPrinters_IsActive { get; set; }

        [StringLength(100)]
        public string DG_AssociatedPrinters_PaperSize { get; set; }

        public int? DG_AssociatedPrinters_SubStoreID { get; set; }
    }
}
