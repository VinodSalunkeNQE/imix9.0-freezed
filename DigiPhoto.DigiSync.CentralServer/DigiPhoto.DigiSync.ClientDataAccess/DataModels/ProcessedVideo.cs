namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcessedVideo")]
    public partial class ProcessedVideo
    {
        public ProcessedVideo()
        {
            ProcessedVideoDetails = new HashSet<ProcessedVideoDetail>();
        }

        public long ProcessedVideoId { get; set; }

        public int? VideoId { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string Effects { get; set; }

        [Required]
        [StringLength(150)]
        public string OutputVideoFileName { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int ProductId { get; set; }

        public virtual ICollection<ProcessedVideoDetail> ProcessedVideoDetails { get; set; }
    }
}
