namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Photos_Changes
    {
        [Key]
        public int DG_Photos_Changes_pkey { get; set; }

        public int? DG_Photos_ID { get; set; }

        public int? DG_Photos_Changes_TypeID { get; set; }

        [StringLength(50)]
        public string DG_Photos_Changes_Value { get; set; }

        public DateTime? DG_Photos_Changes_CreatedON { get; set; }
    }
}
