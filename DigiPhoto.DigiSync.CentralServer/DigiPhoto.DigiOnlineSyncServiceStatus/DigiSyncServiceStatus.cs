﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ClientDataProcessor;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using System.Configuration;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.IMIX.Business;

namespace DataSyncService
{
    public partial class DigiSyncServiceStatus : ServiceBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private System.Timers.Timer timer;
  

        public DigiSyncServiceStatus()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {
                    
                                        
                    this.timer = new System.Timers.Timer();
                    this.timer.Interval = 1000 * 100;
                    this.timer.AutoReset = true;
                    this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    this.timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }

                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);

            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                //ExitCode = 13816;
                this.Stop();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                this.timer.Stop();
                // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // svcPosinfoBusiness.ServiceStart(false);
                GetOnlineOrdersStatus();

            }
            catch (Exception ex)
            {

                log.Error("timer_Elapsed:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.timer.Start();
            }
            finally
            {
                this.timer.Start();
            }
        }


        private void GetOnlineOrdersStatus()
        {
            try
            {
                List<QrCodeStatus> QrCodeList = new List<QrCodeStatus>();

                QrCodeList = SyncController.GetPendingOnlineOrders(10);
                log.Info("QR code status started");
                foreach (QrCodeStatus item in QrCodeList)
                {
                    log.Info("Started for QR code:" + item.IdentificationCode);
                    OnlineOrderStatus qrCodeInfo = new OnlineOrderStatus();
                    DServiceProxy<IDataSyncService>.Use(client =>
                    {
                        qrCodeInfo = client.GetOnlineOrderStatus(item);
                    });

                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var changeTrack = (from DOD in dbContext.DG_Orders_Details
                                           join DO in dbContext.DG_Orders
                                           on DOD.DG_Orders_ID equals DO.DG_Orders_pkey
                                           join QCT in dbContext.ChangeTrackings
                                           on DO.DG_Orders_pkey equals QCT.ObjectValueId
                                           where
                                           QCT.ApplicationObjectId == 14
                                           && DOD.DG_Order_ImageUniqueIdentifier == item.IdentificationCode
                                           && (QCT.SyncStatus == 3 || QCT.SyncStatus == 11 || QCT.SyncStatus == 12 || QCT.SyncStatus == 13 || QCT.SyncStatus == 14)
                                           orderby QCT.ChangeTrackingId descending
                                           select QCT).FirstOrDefault();

                        changeTrack.SyncStatus = qrCodeInfo.OnlineOrderIMSStatus;

                        dbContext.SaveChanges();

                    }

                    log.Info("Completed for QR code:" + item.IdentificationCode + "=" + qrCodeInfo.OnlineOrderIMSStatus);
                }

            }
            catch (Exception ex)
            {

                log.Error("Error:GetOnlineOrdersStatus-" + ex.Message);
            }


        }

    }
}





