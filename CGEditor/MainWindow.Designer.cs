﻿using CGEditor_WinForms.CustomPropertyEditors;

namespace CGEditor_WinForms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.LoadConfig = new System.Windows.Forms.RibbonOrbMenuItem();
            this.SaveConfig = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbOptionButton1 = new System.Windows.Forms.RibbonOrbOptionButton();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.VisibleHost = new System.Windows.Forms.RibbonHost();
            this.BasicVisibilityEditor = new CGEditor_WinForms.CustomPropertyEditors.VisibilityEditor();
            this.positionGroup = new System.Windows.Forms.RibbonPanel();
            this.PositionHost = new System.Windows.Forms.RibbonHost();
            this.BasicPositionEditor = new CGEditor_WinForms.CustomPropertyEditors.PositionEditor();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.AlphaBackColorHost = new System.Windows.Forms.RibbonHost();
            this.BasicColorEditor = new CGEditor_WinForms.CustomPropertyEditors.SimpleColorEditor();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.MiscHost = new System.Windows.Forms.RibbonHost();
            this.BasicMiscEditor = new CGEditor_WinForms.CustomPropertyEditors.MiscEditor();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.SpeedHost = new System.Windows.Forms.RibbonHost();
            this.EffectsSpeedEditor = new CGEditor_WinForms.CustomPropertyEditors.SpeedEditor();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.ShadowHost = new System.Windows.Forms.RibbonHost();
            this.EffectsShadowEditor = new CGEditor_WinForms.CustomPropertyEditors.ShadowEditor();
            this.ribbonPanel7 = new System.Windows.Forms.RibbonPanel();
            this.BlurHost = new System.Windows.Forms.RibbonHost();
            this.EffectsBlurEditor = new CGEditor_WinForms.CustomPropertyEditors.BlurEditor();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.GlowHost = new System.Windows.Forms.RibbonHost();
            this.EffectsGlowEditor = new CGEditor_WinForms.CustomPropertyEditors.GlowEditor();
            this.TabText = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.TextContentHost = new System.Windows.Forms.RibbonHost();
            this.TextContEditor = new CGEditor_WinForms.CustomPropertyEditors.TextContentEditor();
            this.ribbonPanel10 = new System.Windows.Forms.RibbonPanel();
            this.TextColorHost = new System.Windows.Forms.RibbonHost();
            this.TextColorEditor = new CGEditor_WinForms.CustomPropertyEditors.GradientEditor();
            this.ribbonPanel11 = new System.Windows.Forms.RibbonPanel();
            this.TextOutlineHost = new System.Windows.Forms.RibbonHost();
            this.TextOutlineEditor = new CGEditor_WinForms.CustomPropertyEditors.OutlineColorEditor();
            this.ribbonPanel12 = new System.Windows.Forms.RibbonPanel();
            this.TextFontHost = new System.Windows.Forms.RibbonHost();
            this.TextFontEditor = new CGEditor_WinForms.CustomPropertyEditors.FontEditor();
            this.TabGraphics = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel13 = new System.Windows.Forms.RibbonPanel();
            this.GraphicShapeHost = new System.Windows.Forms.RibbonHost();
            this.GraphicsShapeEditor = new CGEditor_WinForms.CustomPropertyEditors.ShapeEditor();
            this.ribbonPanel14 = new System.Windows.Forms.RibbonPanel();
            this.GraphicColorHost = new System.Windows.Forms.RibbonHost();
            this.GraphicsColorEditor = new CGEditor_WinForms.CustomPropertyEditors.GradientEditor();
            this.ribbonPanel15 = new System.Windows.Forms.RibbonPanel();
            this.GraphicOutlineHost = new System.Windows.Forms.RibbonHost();
            this.GraphicsOutlineEditor = new CGEditor_WinForms.CustomPropertyEditors.OutlineColorEditor();
            this.TabTicker = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel16 = new System.Windows.Forms.RibbonPanel();
            this.TickerNewContentHost = new System.Windows.Forms.RibbonHost();
            this.TickerNewContentEditor = new CGEditor_WinForms.CustomPropertyEditors.TickerContentEditor();
            this.ribbonPanel17 = new System.Windows.Forms.RibbonPanel();
            this.TickerPropsHost = new System.Windows.Forms.RibbonHost();
            this.TickerPropsEditor = new CGEditor_WinForms.CustomPropertyEditors.TickerEditor();
            this.ribbonPanel18 = new System.Windows.Forms.RibbonPanel();
            this.TickerShapeHost = new System.Windows.Forms.RibbonHost();
            this.TickerShapeEditor = new CGEditor_WinForms.CustomPropertyEditors.ShapeEditor();
            this.ribbonPanel19 = new System.Windows.Forms.RibbonPanel();
            this.TickerColorHost = new System.Windows.Forms.RibbonHost();
            this.TickerColorEditor = new CGEditor_WinForms.CustomPropertyEditors.GradientEditor();
            this.ribbonPanel20 = new System.Windows.Forms.RibbonPanel();
            this.TickerOutlineHost = new System.Windows.Forms.RibbonHost();
            this.TickerOutlineEditor = new CGEditor_WinForms.CustomPropertyEditors.OutlineColorEditor();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.tickerTextHost = new System.Windows.Forms.RibbonHost();
            this.TickerTextColorEditor = new CGEditor_WinForms.CustomPropertyEditors.TextGradientEditor();
            this.ribbonPanel23 = new System.Windows.Forms.RibbonPanel();
            this.tickerTextOutlineHost = new System.Windows.Forms.RibbonHost();
            this.TickerTextOutlineEditor = new CGEditor_WinForms.CustomPropertyEditors.TextOutlineColorEditor();
            this.ribbonPanel24 = new System.Windows.Forms.RibbonPanel();
            this.TickerTextFontHost = new System.Windows.Forms.RibbonHost();
            this.TickerTextFontEditor = new CGEditor_WinForms.CustomPropertyEditors.FontEditor();
            this.TabImage = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel21 = new System.Windows.Forms.RibbonPanel();
            this.ImageSourceHost = new System.Windows.Forms.RibbonHost();
            this.TabFlash = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel22 = new System.Windows.Forms.RibbonPanel();
            this.FlashSourceHost = new System.Windows.Forms.RibbonHost();
            this.FlashEditor = new CGEditor_WinForms.CustomPropertyEditors.OpenFlashEditor();
            this.ribbonTab3 = new System.Windows.Forms.RibbonTab();
            this.GroupActions = new System.Windows.Forms.RibbonPanel();
            this.GroupHost = new System.Windows.Forms.RibbonHost();
            this.BtnGroup = new System.Windows.Forms.Button();
            this.UngroupHost = new System.Windows.Forms.RibbonHost();
            this.BtnUnGroup = new System.Windows.Forms.Button();
            this.GroupZOrder = new System.Windows.Forms.RibbonPanel();
            this.LayerUpHost = new System.Windows.Forms.RibbonHost();
            this.BtnUp = new System.Windows.Forms.Button();
            this.LayerDownHost = new System.Windows.Forms.RibbonHost();
            this.BtnDown = new System.Windows.Forms.Button();
            this.FrontHost = new System.Windows.Forms.RibbonHost();
            this.BtnToFront = new System.Windows.Forms.Button();
            this.BackHost = new System.Windows.Forms.RibbonHost();
            this.BtnToBack = new System.Windows.Forms.Button();
            this.GroupAlign = new System.Windows.Forms.RibbonPanel();
            this.ALeftsHost = new System.Windows.Forms.RibbonHost();
            this.AlignLefts = new System.Windows.Forms.Button();
            this.ACenterHost = new System.Windows.Forms.RibbonHost();
            this.AlignCenters = new System.Windows.Forms.Button();
            this.ARightsHost = new System.Windows.Forms.RibbonHost();
            this.AlignRights = new System.Windows.Forms.Button();
            this.ATopHost = new System.Windows.Forms.RibbonHost();
            this.AlignTops = new System.Windows.Forms.Button();
            this.AMiddleHost = new System.Windows.Forms.RibbonHost();
            this.AlignMiddles = new System.Windows.Forms.Button();
            this.ABottomHost = new System.Windows.Forms.RibbonHost();
            this.AlignBottoms = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConfigFileName = new System.Windows.Forms.TextBox();
            this.btnSaveCGConfig = new System.Windows.Forms.Button();
            this.lblAddTextLogo = new System.Windows.Forms.Label();
            this.txtAddTextLogo = new System.Windows.Forms.TextBox();
            this.btnAddTextLogo = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dockPanel1
            // 
            this.dockPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dockPanel1.Location = new System.Drawing.Point(0, 135);
            this.dockPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(913, 377);//Changed by Shailee 
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel1.Skin = dockPanelSkin1;
            this.dockPanel1.TabIndex = 4;
            // 
            // ribbon1
            // 
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Margin = new System.Windows.Forms.Padding(2);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.LoadConfig);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.SaveConfig);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.OptionItems.Add(this.ribbonOrbOptionButton1);
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 160);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.ribbon1.OrbText = "File";
            this.ribbon1.OrbVisible = false;
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.DropDownButtonVisible = false;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon1.Size = new System.Drawing.Size(913, 130);
            this.ribbon1.TabIndex = 7;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.Tabs.Add(this.TabText);
            this.ribbon1.Tabs.Add(this.TabGraphics);
            this.ribbon1.Tabs.Add(this.TabTicker);
            this.ribbon1.Tabs.Add(this.TabImage);
            this.ribbon1.Tabs.Add(this.TabFlash);
            this.ribbon1.Tabs.Add(this.ribbonTab3);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // LoadConfig
            // 
            this.LoadConfig.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.LoadConfig.Image = ((System.Drawing.Image)(resources.GetObject("LoadConfig.Image")));
            this.LoadConfig.SmallImage = ((System.Drawing.Image)(resources.GetObject("LoadConfig.SmallImage")));
            this.LoadConfig.Text = "Load CG Config";
            this.LoadConfig.Click += new System.EventHandler(this.LoadConfig_Click);
            // 
            // SaveConfig
            // 
            this.SaveConfig.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.SaveConfig.Image = ((System.Drawing.Image)(resources.GetObject("SaveConfig.Image")));
            this.SaveConfig.SmallImage = ((System.Drawing.Image)(resources.GetObject("SaveConfig.SmallImage")));
            this.SaveConfig.Text = "Save CG Config";
            this.SaveConfig.Click += new System.EventHandler(this.SaveConfig_Click);
            // 
            // ribbonOrbOptionButton1
            // 
            this.ribbonOrbOptionButton1.Image = global::CGEditor_WinForms.Properties.Resources.close;
            this.ribbonOrbOptionButton1.SmallImage = global::CGEditor_WinForms.Properties.Resources.close;
            this.ribbonOrbOptionButton1.Text = "Close";
            this.ribbonOrbOptionButton1.Click += new System.EventHandler(this.ribbonOrbOptionButton1_Click);
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.positionGroup);
            this.ribbonTab1.Panels.Add(this.ribbonPanel3);
            this.ribbonTab1.Panels.Add(this.ribbonPanel4);
            this.ribbonTab1.Text = "Common properties";
            this.ribbonTab1.Visible = false;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ButtonMoreVisible = false;
            this.ribbonPanel1.Items.Add(this.VisibleHost);
            this.ribbonPanel1.Text = "Visibility";
            // 
            // VisibleHost
            // 
            this.VisibleHost.HostedControl = this.BasicVisibilityEditor;
            // 
            // BasicVisibilityEditor
            // 
            this.BasicVisibilityEditor.BackColor = System.Drawing.Color.White;
            this.BasicVisibilityEditor.Enabled = false;
            this.BasicVisibilityEditor.Location = new System.Drawing.Point(8, 141);
            this.BasicVisibilityEditor.Margin = new System.Windows.Forms.Padding(1);
            this.BasicVisibilityEditor.Name = "BasicVisibilityEditor";
            this.BasicVisibilityEditor.Size = new System.Drawing.Size(72, 55);
            this.BasicVisibilityEditor.TabIndex = 14;
            // 
            // positionGroup
            // 
            this.positionGroup.ButtonMoreVisible = false;
            this.positionGroup.Items.Add(this.PositionHost);
            this.positionGroup.Text = "Position/Size";
            // 
            // PositionHost
            // 
            this.PositionHost.HostedControl = this.BasicPositionEditor;
            // 
            // BasicPositionEditor
            // 
            this.BasicPositionEditor.BackColor = System.Drawing.Color.White;
            this.BasicPositionEditor.Enabled = false;
            this.BasicPositionEditor.Location = new System.Drawing.Point(84, 141);
            this.BasicPositionEditor.Margin = new System.Windows.Forms.Padding(1);
            this.BasicPositionEditor.Name = "BasicPositionEditor";
            this.BasicPositionEditor.Size = new System.Drawing.Size(152, 55);
            this.BasicPositionEditor.TabIndex = 15;
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ButtonMoreVisible = false;
            this.ribbonPanel3.Items.Add(this.AlphaBackColorHost);
            this.ribbonPanel3.Text = "Alpha/BackColor";
            // 
            // AlphaBackColorHost
            // 
            this.AlphaBackColorHost.HostedControl = this.BasicColorEditor;
            // 
            // BasicColorEditor
            // 
            this.BasicColorEditor.BackColor = System.Drawing.Color.White;
            this.BasicColorEditor.Enabled = false;
            this.BasicColorEditor.Location = new System.Drawing.Point(240, 141);
            this.BasicColorEditor.Margin = new System.Windows.Forms.Padding(1);
            this.BasicColorEditor.Name = "BasicColorEditor";
            this.BasicColorEditor.Size = new System.Drawing.Size(135, 55);
            this.BasicColorEditor.TabIndex = 16;
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ButtonMoreVisible = false;
            this.ribbonPanel4.Items.Add(this.MiscHost);
            this.ribbonPanel4.Text = "Miscellaneous";
            // 
            // MiscHost
            // 
            this.MiscHost.HostedControl = this.BasicMiscEditor;
            // 
            // BasicMiscEditor
            // 
            this.BasicMiscEditor.BackColor = System.Drawing.Color.White;
            this.BasicMiscEditor.Enabled = false;
            this.BasicMiscEditor.Location = new System.Drawing.Point(387, 141);
            this.BasicMiscEditor.Margin = new System.Windows.Forms.Padding(1);
            this.BasicMiscEditor.Name = "BasicMiscEditor";
            this.BasicMiscEditor.Size = new System.Drawing.Size(335, 55);
            this.BasicMiscEditor.TabIndex = 17;
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel5);
            this.ribbonTab2.Panels.Add(this.ribbonPanel6);
            this.ribbonTab2.Panels.Add(this.ribbonPanel7);
            this.ribbonTab2.Panels.Add(this.ribbonPanel8);
            this.ribbonTab2.Text = "Effects";
            this.ribbonTab2.Visible = false;
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ButtonMoreVisible = false;
            this.ribbonPanel5.Items.Add(this.SpeedHost);
            this.ribbonPanel5.Text = "Speed";
            // 
            // SpeedHost
            // 
            this.SpeedHost.HostedControl = this.EffectsSpeedEditor;
            // 
            // EffectsSpeedEditor
            // 
            this.EffectsSpeedEditor.BackColor = System.Drawing.Color.White;
            this.EffectsSpeedEditor.Enabled = false;
            this.EffectsSpeedEditor.Location = new System.Drawing.Point(8, 226);
            this.EffectsSpeedEditor.Margin = new System.Windows.Forms.Padding(1);
            this.EffectsSpeedEditor.Name = "EffectsSpeedEditor";
            this.EffectsSpeedEditor.Size = new System.Drawing.Size(147, 56);
            this.EffectsSpeedEditor.TabIndex = 18;
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ButtonMoreVisible = false;
            this.ribbonPanel6.Items.Add(this.ShadowHost);
            this.ribbonPanel6.Text = "Shadow";
            // 
            // ShadowHost
            // 
            this.ShadowHost.HostedControl = this.EffectsShadowEditor;
            // 
            // EffectsShadowEditor
            // 
            this.EffectsShadowEditor.BackColor = System.Drawing.Color.White;
            this.EffectsShadowEditor.Enabled = false;
            this.EffectsShadowEditor.Location = new System.Drawing.Point(159, 226);
            this.EffectsShadowEditor.Margin = new System.Windows.Forms.Padding(1);
            this.EffectsShadowEditor.Name = "EffectsShadowEditor";
            this.EffectsShadowEditor.Size = new System.Drawing.Size(279, 56);
            this.EffectsShadowEditor.TabIndex = 19;
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ButtonMoreVisible = false;
            this.ribbonPanel7.Items.Add(this.BlurHost);
            this.ribbonPanel7.Text = "Blur";
            // 
            // BlurHost
            // 
            this.BlurHost.HostedControl = this.EffectsBlurEditor;
            // 
            // EffectsBlurEditor
            // 
            this.EffectsBlurEditor.BackColor = System.Drawing.Color.White;
            this.EffectsBlurEditor.Enabled = false;
            this.EffectsBlurEditor.Location = new System.Drawing.Point(443, 226);
            this.EffectsBlurEditor.Margin = new System.Windows.Forms.Padding(2);
            this.EffectsBlurEditor.Name = "EffectsBlurEditor";
            this.EffectsBlurEditor.Size = new System.Drawing.Size(149, 56);
            this.EffectsBlurEditor.TabIndex = 20;
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ButtonMoreVisible = false;
            this.ribbonPanel8.Items.Add(this.GlowHost);
            this.ribbonPanel8.Text = "Glow";
            // 
            // GlowHost
            // 
            this.GlowHost.HostedControl = this.EffectsGlowEditor;
            // 
            // EffectsGlowEditor
            // 
            this.EffectsGlowEditor.BackColor = System.Drawing.Color.White;
            this.EffectsGlowEditor.Enabled = false;
            this.EffectsGlowEditor.Location = new System.Drawing.Point(595, 226);
            this.EffectsGlowEditor.Margin = new System.Windows.Forms.Padding(1);
            this.EffectsGlowEditor.Name = "EffectsGlowEditor";
            this.EffectsGlowEditor.Size = new System.Drawing.Size(149, 56);
            this.EffectsGlowEditor.TabIndex = 21;
            // 
            // TabText
            // 
            this.TabText.Panels.Add(this.ribbonPanel9);
            this.TabText.Panels.Add(this.ribbonPanel10);
            this.TabText.Panels.Add(this.ribbonPanel11);
            this.TabText.Panels.Add(this.ribbonPanel12);
            this.TabText.Text = "Text Properties";
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ButtonMoreVisible = false;
            this.ribbonPanel9.Items.Add(this.TextContentHost);
            this.ribbonPanel9.Text = "Text";
            // 
            // TextContentHost
            // 
            this.TextContentHost.HostedControl = this.TextContEditor;
            // 
            // TextContEditor
            // 
            this.TextContEditor.BackColor = System.Drawing.Color.White;
            this.TextContEditor.Enabled = false;
            this.TextContEditor.Location = new System.Drawing.Point(8, 305);
            this.TextContEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TextContEditor.Name = "TextContEditor";
            this.TextContEditor.Size = new System.Drawing.Size(356, 54);
            this.TextContEditor.TabIndex = 8;
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ButtonMoreVisible = false;
            this.ribbonPanel10.Items.Add(this.TextColorHost);
            this.ribbonPanel10.Text = "Color";
            // 
            // TextColorHost
            // 
            this.TextColorHost.HostedControl = this.TextColorEditor;
            // 
            // TextColorEditor
            // 
            this.TextColorEditor.BackColor = System.Drawing.Color.White;
            this.TextColorEditor.Enabled = false;
            this.TextColorEditor.Location = new System.Drawing.Point(368, 305);
            this.TextColorEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TextColorEditor.Name = "TextColorEditor";
            this.TextColorEditor.Size = new System.Drawing.Size(243, 49);
            this.TextColorEditor.TabIndex = 22;
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.ButtonMoreVisible = false;
            this.ribbonPanel11.Items.Add(this.TextOutlineHost);
            this.ribbonPanel11.Text = "Outline";
            this.ribbonPanel11.Visible = false;
            // 
            // TextOutlineHost
            // 
            this.TextOutlineHost.HostedControl = this.TextOutlineEditor;
            // 
            // TextOutlineEditor
            // 
            this.TextOutlineEditor.BackColor = System.Drawing.Color.White;
            this.TextOutlineEditor.Enabled = false;
            this.TextOutlineEditor.Location = new System.Drawing.Point(615, 303);
            this.TextOutlineEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TextOutlineEditor.Name = "TextOutlineEditor";
            this.TextOutlineEditor.Size = new System.Drawing.Size(254, 51);
            this.TextOutlineEditor.TabIndex = 23;
            // 
            // ribbonPanel12
            // 
            this.ribbonPanel12.ButtonMoreVisible = false;
            this.ribbonPanel12.Items.Add(this.TextFontHost);
            this.ribbonPanel12.Text = "Font";
            // 
            // TextFontHost
            // 
            this.TextFontHost.HostedControl = this.TextFontEditor;
            // 
            // TextFontEditor
            // 
            this.TextFontEditor.BackColor = System.Drawing.Color.White;
            this.TextFontEditor.Enabled = false;
            this.TextFontEditor.Location = new System.Drawing.Point(873, 303);
            this.TextFontEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TextFontEditor.Name = "TextFontEditor";
            this.TextFontEditor.Size = new System.Drawing.Size(308, 51);
            this.TextFontEditor.TabIndex = 11;
            // 
            // TabGraphics
            // 
            this.TabGraphics.Panels.Add(this.ribbonPanel13);
            this.TabGraphics.Panels.Add(this.ribbonPanel14);
            this.TabGraphics.Panels.Add(this.ribbonPanel15);
            this.TabGraphics.Text = "Graphics Properties";
            // 
            // ribbonPanel13
            // 
            this.ribbonPanel13.ButtonMoreVisible = false;
            this.ribbonPanel13.Items.Add(this.GraphicShapeHost);
            this.ribbonPanel13.Text = "Shape";
            // 
            // GraphicShapeHost
            // 
            this.GraphicShapeHost.HostedControl = this.GraphicsShapeEditor;
            // 
            // GraphicsShapeEditor
            // 
            this.GraphicsShapeEditor.BackColor = System.Drawing.Color.White;
            this.GraphicsShapeEditor.Enabled = false;
            this.GraphicsShapeEditor.Location = new System.Drawing.Point(8, 378);
            this.GraphicsShapeEditor.Margin = new System.Windows.Forms.Padding(1);
            this.GraphicsShapeEditor.Name = "GraphicsShapeEditor";
            this.GraphicsShapeEditor.Size = new System.Drawing.Size(185, 44);
            this.GraphicsShapeEditor.TabIndex = 24;
            // 
            // ribbonPanel14
            // 
            this.ribbonPanel14.ButtonMoreVisible = false;
            this.ribbonPanel14.Items.Add(this.GraphicColorHost);
            this.ribbonPanel14.Text = "Color";
            // 
            // GraphicColorHost
            // 
            this.GraphicColorHost.HostedControl = this.GraphicsColorEditor;
            // 
            // GraphicsColorEditor
            // 
            this.GraphicsColorEditor.BackColor = System.Drawing.Color.White;
            this.GraphicsColorEditor.Enabled = false;
            this.GraphicsColorEditor.Location = new System.Drawing.Point(195, 378);
            this.GraphicsColorEditor.Margin = new System.Windows.Forms.Padding(1);
            this.GraphicsColorEditor.Name = "GraphicsColorEditor";
            this.GraphicsColorEditor.Size = new System.Drawing.Size(244, 49);
            this.GraphicsColorEditor.TabIndex = 25;
            // 
            // ribbonPanel15
            // 
            this.ribbonPanel15.ButtonMoreVisible = false;
            this.ribbonPanel15.Items.Add(this.GraphicOutlineHost);
            this.ribbonPanel15.Text = "Outline";
            // 
            // GraphicOutlineHost
            // 
            this.GraphicOutlineHost.HostedControl = this.GraphicsOutlineEditor;
            // 
            // GraphicsOutlineEditor
            // 
            this.GraphicsOutlineEditor.BackColor = System.Drawing.Color.White;
            this.GraphicsOutlineEditor.Enabled = false;
            this.GraphicsOutlineEditor.Location = new System.Drawing.Point(443, 378);
            this.GraphicsOutlineEditor.Margin = new System.Windows.Forms.Padding(1);
            this.GraphicsOutlineEditor.Name = "GraphicsOutlineEditor";
            this.GraphicsOutlineEditor.Size = new System.Drawing.Size(254, 51);
            this.GraphicsOutlineEditor.TabIndex = 26;
            // 
            // TabTicker
            // 
            this.TabTicker.Panels.Add(this.ribbonPanel16);
            this.TabTicker.Panels.Add(this.ribbonPanel17);
            this.TabTicker.Panels.Add(this.ribbonPanel18);
            this.TabTicker.Panels.Add(this.ribbonPanel19);
            this.TabTicker.Panels.Add(this.ribbonPanel20);
            this.TabTicker.Panels.Add(this.ribbonPanel2);
            this.TabTicker.Panels.Add(this.ribbonPanel23);
            this.TabTicker.Panels.Add(this.ribbonPanel24);
            this.TabTicker.Text = "Ticker Properties";
            // 
            // ribbonPanel16
            // 
            this.ribbonPanel16.ButtonMoreVisible = false;
            this.ribbonPanel16.Items.Add(this.TickerNewContentHost);
            this.ribbonPanel16.Text = "New content";
            // 
            // TickerNewContentHost
            // 
            this.TickerNewContentHost.HostedControl = this.TickerNewContentEditor;
            // 
            // TickerNewContentEditor
            // 
            this.TickerNewContentEditor.BackColor = System.Drawing.Color.White;
            this.TickerNewContentEditor.Enabled = false;
            this.TickerNewContentEditor.Location = new System.Drawing.Point(8, 448);
            this.TickerNewContentEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerNewContentEditor.Name = "TickerNewContentEditor";
            this.TickerNewContentEditor.Size = new System.Drawing.Size(359, 53);
            this.TickerNewContentEditor.TabIndex = 27;
            // 
            // ribbonPanel17
            // 
            this.ribbonPanel17.ButtonMoreVisible = false;
            this.ribbonPanel17.Items.Add(this.TickerPropsHost);
            this.ribbonPanel17.Text = "Ticker properties";
            // 
            // TickerPropsHost
            // 
            this.TickerPropsHost.HostedControl = this.TickerPropsEditor;
            // 
            // TickerPropsEditor
            // 
            this.TickerPropsEditor.BackColor = System.Drawing.Color.White;
            this.TickerPropsEditor.Enabled = false;
            this.TickerPropsEditor.Location = new System.Drawing.Point(371, 448);
            this.TickerPropsEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerPropsEditor.Name = "TickerPropsEditor";
            this.TickerPropsEditor.Size = new System.Drawing.Size(209, 53);
            this.TickerPropsEditor.TabIndex = 28;
            // 
            // ribbonPanel18
            // 
            this.ribbonPanel18.ButtonMoreVisible = false;
            this.ribbonPanel18.Items.Add(this.TickerShapeHost);
            this.ribbonPanel18.Text = "Shape type";
            // 
            // TickerShapeHost
            // 
            this.TickerShapeHost.HostedControl = this.TickerShapeEditor;
            // 
            // TickerShapeEditor
            // 
            this.TickerShapeEditor.BackColor = System.Drawing.Color.White;
            this.TickerShapeEditor.Enabled = false;
            this.TickerShapeEditor.Location = new System.Drawing.Point(584, 448);
            this.TickerShapeEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerShapeEditor.Name = "TickerShapeEditor";
            this.TickerShapeEditor.Size = new System.Drawing.Size(187, 53);
            this.TickerShapeEditor.TabIndex = 29;
            // 
            // ribbonPanel19
            // 
            this.ribbonPanel19.ButtonMoreVisible = false;
            this.ribbonPanel19.Items.Add(this.TickerColorHost);
            this.ribbonPanel19.Text = "Shape color";
            // 
            // TickerColorHost
            // 
            this.TickerColorHost.HostedControl = this.TickerColorEditor;
            // 
            // TickerColorEditor
            // 
            this.TickerColorEditor.BackColor = System.Drawing.Color.White;
            this.TickerColorEditor.Enabled = false;
            this.TickerColorEditor.Location = new System.Drawing.Point(775, 448);
            this.TickerColorEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerColorEditor.Name = "TickerColorEditor";
            this.TickerColorEditor.Size = new System.Drawing.Size(243, 53);
            this.TickerColorEditor.TabIndex = 30;
            // 
            // ribbonPanel20
            // 
            this.ribbonPanel20.ButtonMoreVisible = false;
            this.ribbonPanel20.Items.Add(this.TickerOutlineHost);
            this.ribbonPanel20.Text = "Shape outline";
            // 
            // TickerOutlineHost
            // 
            this.TickerOutlineHost.HostedControl = this.TickerOutlineEditor;
            // 
            // TickerOutlineEditor
            // 
            this.TickerOutlineEditor.BackColor = System.Drawing.Color.White;
            this.TickerOutlineEditor.Enabled = false;
            this.TickerOutlineEditor.Location = new System.Drawing.Point(1022, 448);
            this.TickerOutlineEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerOutlineEditor.Name = "TickerOutlineEditor";
            this.TickerOutlineEditor.Size = new System.Drawing.Size(254, 53);
            this.TickerOutlineEditor.TabIndex = 31;
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.tickerTextHost);
            this.ribbonPanel2.Text = "Text";
            // 
            // tickerTextHost
            // 
            this.tickerTextHost.HostedControl = this.TickerTextColorEditor;
            // 
            // TickerTextColorEditor
            // 
            this.TickerTextColorEditor.BackColor = System.Drawing.Color.White;
            this.TickerTextColorEditor.Enabled = false;
            this.TickerTextColorEditor.Location = new System.Drawing.Point(387, 508);
            this.TickerTextColorEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerTextColorEditor.Name = "TickerTextColorEditor";
            this.TickerTextColorEditor.Size = new System.Drawing.Size(241, 49);
            this.TickerTextColorEditor.TabIndex = 52;
            // 
            // ribbonPanel23
            // 
            this.ribbonPanel23.Items.Add(this.tickerTextOutlineHost);
            this.ribbonPanel23.Text = "Outline";
            // 
            // tickerTextOutlineHost
            // 
            this.tickerTextOutlineHost.HostedControl = this.TickerTextOutlineEditor;
            // 
            // TickerTextOutlineEditor
            // 
            this.TickerTextOutlineEditor.BackColor = System.Drawing.Color.White;
            this.TickerTextOutlineEditor.Enabled = false;
            this.TickerTextOutlineEditor.Location = new System.Drawing.Point(632, 508);
            this.TickerTextOutlineEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerTextOutlineEditor.Name = "TickerTextOutlineEditor";
            this.TickerTextOutlineEditor.Size = new System.Drawing.Size(252, 50);
            this.TickerTextOutlineEditor.TabIndex = 53;
            // 
            // ribbonPanel24
            // 
            this.ribbonPanel24.Items.Add(this.TickerTextFontHost);
            this.ribbonPanel24.Text = "Font";
            // 
            // TickerTextFontHost
            // 
            this.TickerTextFontHost.HostedControl = this.TickerTextFontEditor;
            // 
            // TickerTextFontEditor
            // 
            this.TickerTextFontEditor.BackColor = System.Drawing.Color.White;
            this.TickerTextFontEditor.Enabled = false;
            this.TickerTextFontEditor.Location = new System.Drawing.Point(888, 509);
            this.TickerTextFontEditor.Margin = new System.Windows.Forms.Padding(1);
            this.TickerTextFontEditor.Name = "TickerTextFontEditor";
            this.TickerTextFontEditor.Size = new System.Drawing.Size(309, 49);
            this.TickerTextFontEditor.TabIndex = 54;
            // 
            // TabImage
            // 
            this.TabImage.Panels.Add(this.ribbonPanel21);
            this.TabImage.Text = "Image Properties";
            // 
            // ribbonPanel21
            // 
            this.ribbonPanel21.Items.Add(this.ImageSourceHost);
            this.ribbonPanel21.Text = "Image source";
            // 
            // ImageSourceHost
            // 
            this.ImageSourceHost.HostedControl = null;
            // 
            // TabFlash
            // 
            this.TabFlash.Panels.Add(this.ribbonPanel22);
            this.TabFlash.Text = "Flash Properties";
            // 
            // ribbonPanel22
            // 
            this.ribbonPanel22.ButtonMoreVisible = false;
            this.ribbonPanel22.Items.Add(this.FlashSourceHost);
            this.ribbonPanel22.Text = "Flash Source";
            // 
            // FlashSourceHost
            // 
            this.FlashSourceHost.HostedControl = this.FlashEditor;
            // 
            // FlashEditor
            // 
            this.FlashEditor.BackColor = System.Drawing.Color.White;
            this.FlashEditor.Location = new System.Drawing.Point(8, 575);
            this.FlashEditor.Margin = new System.Windows.Forms.Padding(1);
            this.FlashEditor.Name = "FlashEditor";
            this.FlashEditor.Size = new System.Drawing.Size(329, 29);
            this.FlashEditor.TabIndex = 35;
            this.FlashEditor.Value = null;
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.Panels.Add(this.GroupActions);
            this.ribbonTab3.Panels.Add(this.GroupZOrder);
            this.ribbonTab3.Panels.Add(this.GroupAlign);
            this.ribbonTab3.Text = "Actions";
            this.ribbonTab3.Visible = false;
            // 
            // GroupActions
            // 
            this.GroupActions.ButtonMoreVisible = false;
            this.GroupActions.Items.Add(this.GroupHost);
            this.GroupActions.Items.Add(this.UngroupHost);
            this.GroupActions.Text = "Grouping";
            // 
            // GroupHost
            // 
            this.GroupHost.HostedControl = this.BtnGroup;
            // 
            // BtnGroup
            // 
            this.BtnGroup.BackColor = System.Drawing.Color.White;
            this.BtnGroup.Image = ((System.Drawing.Image)(resources.GetObject("BtnGroup.Image")));
            this.BtnGroup.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnGroup.Location = new System.Drawing.Point(53, 630);
            this.BtnGroup.Margin = new System.Windows.Forms.Padding(2);
            this.BtnGroup.Name = "BtnGroup";
            this.BtnGroup.Size = new System.Drawing.Size(75, 51);
            this.BtnGroup.TabIndex = 38;
            this.BtnGroup.Text = "Group";
            this.BtnGroup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnGroup.UseVisualStyleBackColor = false;
            this.BtnGroup.Click += new System.EventHandler(this.BtnGroup_Click);
            // 
            // UngroupHost
            // 
            this.UngroupHost.HostedControl = this.BtnUnGroup;
            // 
            // BtnUnGroup
            // 
            this.BtnUnGroup.BackColor = System.Drawing.Color.White;
            this.BtnUnGroup.Image = ((System.Drawing.Image)(resources.GetObject("BtnUnGroup.Image")));
            this.BtnUnGroup.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnUnGroup.Location = new System.Drawing.Point(133, 630);
            this.BtnUnGroup.Margin = new System.Windows.Forms.Padding(2);
            this.BtnUnGroup.Name = "BtnUnGroup";
            this.BtnUnGroup.Size = new System.Drawing.Size(77, 51);
            this.BtnUnGroup.TabIndex = 39;
            this.BtnUnGroup.Text = "Ungroup";
            this.BtnUnGroup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnUnGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnUnGroup.UseVisualStyleBackColor = false;
            this.BtnUnGroup.Click += new System.EventHandler(this.BtnUnGroup_Click);
            // 
            // GroupZOrder
            // 
            this.GroupZOrder.ButtonMoreVisible = false;
            this.GroupZOrder.Items.Add(this.LayerUpHost);
            this.GroupZOrder.Items.Add(this.LayerDownHost);
            this.GroupZOrder.Items.Add(this.FrontHost);
            this.GroupZOrder.Items.Add(this.BackHost);
            this.GroupZOrder.Text = "Z Order";
            // 
            // LayerUpHost
            // 
            this.LayerUpHost.HostedControl = this.BtnUp;
            // 
            // BtnUp
            // 
            this.BtnUp.BackColor = System.Drawing.Color.White;
            this.BtnUp.Image = ((System.Drawing.Image)(resources.GetObject("BtnUp.Image")));
            this.BtnUp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnUp.Location = new System.Drawing.Point(251, 630);
            this.BtnUp.Margin = new System.Windows.Forms.Padding(2);
            this.BtnUp.Name = "BtnUp";
            this.BtnUp.Size = new System.Drawing.Size(79, 51);
            this.BtnUp.TabIndex = 40;
            this.BtnUp.Text = "Layer Up";
            this.BtnUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnUp.UseVisualStyleBackColor = false;
            this.BtnUp.Click += new System.EventHandler(this.BtnUp_Click);
            // 
            // LayerDownHost
            // 
            this.LayerDownHost.HostedControl = this.BtnDown;
            // 
            // BtnDown
            // 
            this.BtnDown.BackColor = System.Drawing.Color.White;
            this.BtnDown.Image = ((System.Drawing.Image)(resources.GetObject("BtnDown.Image")));
            this.BtnDown.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnDown.Location = new System.Drawing.Point(334, 630);
            this.BtnDown.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDown.Name = "BtnDown";
            this.BtnDown.Size = new System.Drawing.Size(79, 51);
            this.BtnDown.TabIndex = 41;
            this.BtnDown.Text = "Layer Down";
            this.BtnDown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnDown.UseVisualStyleBackColor = false;
            this.BtnDown.Click += new System.EventHandler(this.BtnDown_Click);
            // 
            // FrontHost
            // 
            this.FrontHost.HostedControl = this.BtnToFront;
            // 
            // BtnToFront
            // 
            this.BtnToFront.BackColor = System.Drawing.Color.White;
            this.BtnToFront.Image = ((System.Drawing.Image)(resources.GetObject("BtnToFront.Image")));
            this.BtnToFront.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnToFront.Location = new System.Drawing.Point(415, 630);
            this.BtnToFront.Margin = new System.Windows.Forms.Padding(2);
            this.BtnToFront.Name = "BtnToFront";
            this.BtnToFront.Size = new System.Drawing.Size(93, 51);
            this.BtnToFront.TabIndex = 42;
            this.BtnToFront.Text = "Bring To Front";
            this.BtnToFront.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnToFront.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnToFront.UseVisualStyleBackColor = false;
            this.BtnToFront.Click += new System.EventHandler(this.BtnToFront_Click);
            // 
            // BackHost
            // 
            this.BackHost.HostedControl = this.BtnToBack;
            // 
            // BtnToBack
            // 
            this.BtnToBack.BackColor = System.Drawing.Color.White;
            this.BtnToBack.Image = ((System.Drawing.Image)(resources.GetObject("BtnToBack.Image")));
            this.BtnToBack.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnToBack.Location = new System.Drawing.Point(498, 630);
            this.BtnToBack.Margin = new System.Windows.Forms.Padding(2);
            this.BtnToBack.Name = "BtnToBack";
            this.BtnToBack.Size = new System.Drawing.Size(93, 51);
            this.BtnToBack.TabIndex = 43;
            this.BtnToBack.Text = "Send To Back";
            this.BtnToBack.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnToBack.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnToBack.UseVisualStyleBackColor = false;
            this.BtnToBack.Click += new System.EventHandler(this.BtnToBack_Click);
            // 
            // GroupAlign
            // 
            this.GroupAlign.ButtonMoreVisible = false;
            this.GroupAlign.Items.Add(this.ALeftsHost);
            this.GroupAlign.Items.Add(this.ACenterHost);
            this.GroupAlign.Items.Add(this.ARightsHost);
            this.GroupAlign.Items.Add(this.ATopHost);
            this.GroupAlign.Items.Add(this.AMiddleHost);
            this.GroupAlign.Items.Add(this.ABottomHost);
            this.GroupAlign.Text = "Align";
            // 
            // ALeftsHost
            // 
            this.ALeftsHost.HostedControl = this.AlignLefts;
            // 
            // AlignLefts
            // 
            this.AlignLefts.BackColor = System.Drawing.Color.White;
            this.AlignLefts.Image = ((System.Drawing.Image)(resources.GetObject("AlignLefts.Image")));
            this.AlignLefts.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignLefts.Location = new System.Drawing.Point(599, 630);
            this.AlignLefts.Margin = new System.Windows.Forms.Padding(2);
            this.AlignLefts.Name = "AlignLefts";
            this.AlignLefts.Size = new System.Drawing.Size(79, 51);
            this.AlignLefts.TabIndex = 44;
            this.AlignLefts.Text = "Align Lefts";
            this.AlignLefts.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignLefts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignLefts.UseVisualStyleBackColor = false;
            this.AlignLefts.Click += new System.EventHandler(this.AlignLefts_Click);
            // 
            // ACenterHost
            // 
            this.ACenterHost.HostedControl = this.AlignCenters;
            // 
            // AlignCenters
            // 
            this.AlignCenters.BackColor = System.Drawing.Color.White;
            this.AlignCenters.Image = ((System.Drawing.Image)(resources.GetObject("AlignCenters.Image")));
            this.AlignCenters.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignCenters.Location = new System.Drawing.Point(680, 630);
            this.AlignCenters.Margin = new System.Windows.Forms.Padding(2);
            this.AlignCenters.Name = "AlignCenters";
            this.AlignCenters.Size = new System.Drawing.Size(79, 51);
            this.AlignCenters.TabIndex = 45;
            this.AlignCenters.Text = "Align Centers";
            this.AlignCenters.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignCenters.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignCenters.UseVisualStyleBackColor = false;
            this.AlignCenters.Click += new System.EventHandler(this.AlignCenters_Click);
            // 
            // ARightsHost
            // 
            this.ARightsHost.HostedControl = this.AlignRights;
            // 
            // AlignRights
            // 
            this.AlignRights.BackColor = System.Drawing.Color.White;
            this.AlignRights.Image = ((System.Drawing.Image)(resources.GetObject("AlignRights.Image")));
            this.AlignRights.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignRights.Location = new System.Drawing.Point(763, 630);
            this.AlignRights.Margin = new System.Windows.Forms.Padding(2);
            this.AlignRights.Name = "AlignRights";
            this.AlignRights.Size = new System.Drawing.Size(79, 51);
            this.AlignRights.TabIndex = 46;
            this.AlignRights.Text = "Align Rights";
            this.AlignRights.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignRights.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignRights.UseVisualStyleBackColor = false;
            this.AlignRights.Click += new System.EventHandler(this.AlignRights_Click);
            // 
            // ATopHost
            // 
            this.ATopHost.HostedControl = this.AlignTops;
            // 
            // AlignTops
            // 
            this.AlignTops.BackColor = System.Drawing.Color.White;
            this.AlignTops.Image = ((System.Drawing.Image)(resources.GetObject("AlignTops.Image")));
            this.AlignTops.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignTops.Location = new System.Drawing.Point(857, 630);
            this.AlignTops.Margin = new System.Windows.Forms.Padding(2);
            this.AlignTops.Name = "AlignTops";
            this.AlignTops.Size = new System.Drawing.Size(79, 51);
            this.AlignTops.TabIndex = 47;
            this.AlignTops.Text = "Align Tops";
            this.AlignTops.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignTops.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignTops.UseVisualStyleBackColor = false;
            this.AlignTops.Click += new System.EventHandler(this.AlignTops_Click);
            // 
            // AMiddleHost
            // 
            this.AMiddleHost.HostedControl = this.AlignMiddles;
            // 
            // AlignMiddles
            // 
            this.AlignMiddles.BackColor = System.Drawing.Color.White;
            this.AlignMiddles.Image = ((System.Drawing.Image)(resources.GetObject("AlignMiddles.Image")));
            this.AlignMiddles.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignMiddles.Location = new System.Drawing.Point(940, 630);
            this.AlignMiddles.Margin = new System.Windows.Forms.Padding(2);
            this.AlignMiddles.Name = "AlignMiddles";
            this.AlignMiddles.Size = new System.Drawing.Size(79, 51);
            this.AlignMiddles.TabIndex = 48;
            this.AlignMiddles.Text = "Align Middles";
            this.AlignMiddles.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignMiddles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignMiddles.UseVisualStyleBackColor = false;
            this.AlignMiddles.Click += new System.EventHandler(this.AlignMiddles_Click);
            // 
            // ABottomHost
            // 
            this.ABottomHost.HostedControl = this.AlignBottoms;
            // 
            // AlignBottoms
            // 
            this.AlignBottoms.BackColor = System.Drawing.Color.White;
            this.AlignBottoms.Image = ((System.Drawing.Image)(resources.GetObject("AlignBottoms.Image")));
            this.AlignBottoms.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlignBottoms.Location = new System.Drawing.Point(1023, 630);
            this.AlignBottoms.Margin = new System.Windows.Forms.Padding(2);
            this.AlignBottoms.Name = "AlignBottoms";
            this.AlignBottoms.Size = new System.Drawing.Size(79, 51);
            this.AlignBottoms.TabIndex = 49;
            this.AlignBottoms.Text = "Align Bottoms";
            this.AlignBottoms.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AlignBottoms.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AlignBottoms.UseVisualStyleBackColor = false;
            this.AlignBottoms.Click += new System.EventHandler(this.AlignBottoms_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(416, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Enter File Name";
            // 
            // txtConfigFileName
            // 
            this.txtConfigFileName.Location = new System.Drawing.Point(504, 5);
            this.txtConfigFileName.Name = "txtConfigFileName";
            this.txtConfigFileName.Size = new System.Drawing.Size(160, 20);
            this.txtConfigFileName.TabIndex = 58;
            // 
            // btnSaveCGConfig
            // 
            this.btnSaveCGConfig.Location = new System.Drawing.Point(680, 3);
            this.btnSaveCGConfig.Name = "btnSaveCGConfig";
            this.btnSaveCGConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveCGConfig.TabIndex = 59;
            this.btnSaveCGConfig.Text = "Save";
            this.btnSaveCGConfig.UseVisualStyleBackColor = true;
            this.btnSaveCGConfig.Click += new System.EventHandler(this.btnSaveCGConfig_Click);
            // 
            // lblAddTextLogo
            // 
            this.lblAddTextLogo.AutoSize = true;
            this.lblAddTextLogo.Location = new System.Drawing.Point(102, 9);
            this.lblAddTextLogo.Name = "lblAddTextLogo";
            this.lblAddTextLogo.Size = new System.Drawing.Size(55, 13);
            this.lblAddTextLogo.TabIndex = 62;
            this.lblAddTextLogo.Text = "Text Logo";
            // 
            // txtAddTextLogo
            // 
            this.txtAddTextLogo.Location = new System.Drawing.Point(162, 6);
            this.txtAddTextLogo.Name = "txtAddTextLogo";
            this.txtAddTextLogo.Size = new System.Drawing.Size(100, 20);
            this.txtAddTextLogo.TabIndex = 63;
            // 
            // btnAddTextLogo
            // 
            this.btnAddTextLogo.Location = new System.Drawing.Point(274, 6);
            this.btnAddTextLogo.Name = "btnAddTextLogo";
            this.btnAddTextLogo.Size = new System.Drawing.Size(46, 23);
            this.btnAddTextLogo.TabIndex = 64;
            this.btnAddTextLogo.Text = "Add";
            this.btnAddTextLogo.UseVisualStyleBackColor = true;
            this.btnAddTextLogo.Click += new System.EventHandler(this.btnAddTextLogo_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.BackgroundImage = global::CGEditor_WinForms.Properties.Resources.close_btn;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(867, -3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(50, 52);
            this.btnClose.TabIndex = 67;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 487);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddTextLogo);
            this.Controls.Add(this.txtAddTextLogo);
            this.Controls.Add(this.lblAddTextLogo);
            this.Controls.Add(this.btnSaveCGConfig);
            this.Controls.Add(this.txtConfigFileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TickerTextFontEditor);
            this.Controls.Add(this.TickerTextOutlineEditor);
            this.Controls.Add(this.TickerTextColorEditor);
            this.Controls.Add(this.AlignBottoms);
            this.Controls.Add(this.AlignMiddles);
            this.Controls.Add(this.AlignTops);
            this.Controls.Add(this.AlignRights);
            this.Controls.Add(this.AlignCenters);
            this.Controls.Add(this.AlignLefts);
            this.Controls.Add(this.BtnToBack);
            this.Controls.Add(this.BtnToFront);
            this.Controls.Add(this.BtnDown);
            this.Controls.Add(this.BtnUp);
            this.Controls.Add(this.BtnUnGroup);
            this.Controls.Add(this.BtnGroup);
            this.Controls.Add(this.FlashEditor);
            this.Controls.Add(this.TickerOutlineEditor);
            this.Controls.Add(this.TickerColorEditor);
            this.Controls.Add(this.TickerShapeEditor);
            this.Controls.Add(this.TickerPropsEditor);
            this.Controls.Add(this.TickerNewContentEditor);
            this.Controls.Add(this.GraphicsOutlineEditor);
            this.Controls.Add(this.GraphicsColorEditor);
            this.Controls.Add(this.GraphicsShapeEditor);
            this.Controls.Add(this.TextOutlineEditor);
            this.Controls.Add(this.TextColorEditor);
            this.Controls.Add(this.EffectsGlowEditor);
            this.Controls.Add(this.EffectsBlurEditor);
            this.Controls.Add(this.EffectsShadowEditor);
            this.Controls.Add(this.EffectsSpeedEditor);
            this.Controls.Add(this.BasicMiscEditor);
            this.Controls.Add(this.BasicColorEditor);
            this.Controls.Add(this.BasicPositionEditor);
            this.Controls.Add(this.BasicVisibilityEditor);
            this.Controls.Add(this.TextFontEditor);
            this.Controls.Add(this.TextContEditor);
            this.Controls.Add(this.ribbon1);
            this.Controls.Add(this.dockPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Imix";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel positionGroup;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonPanel ribbonPanel7;
        private System.Windows.Forms.RibbonPanel ribbonPanel8;
        private System.Windows.Forms.RibbonTab TabText;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.RibbonHost TextContentHost;
        private TextContentEditor TextContEditor;
        private CustomPropertyEditors.FontEditor TextFontEditor;
        private System.Windows.Forms.RibbonHost VisibleHost;
        private CustomPropertyEditors.VisibilityEditor BasicVisibilityEditor;
        private System.Windows.Forms.RibbonHost PositionHost;
        private CustomPropertyEditors.PositionEditor BasicPositionEditor;
        private System.Windows.Forms.RibbonHost AlphaBackColorHost;
        private CustomPropertyEditors.SimpleColorEditor BasicColorEditor;
        private System.Windows.Forms.RibbonHost MiscHost;
        private CustomPropertyEditors.MiscEditor BasicMiscEditor;
        private System.Windows.Forms.RibbonHost SpeedHost;
        private CustomPropertyEditors.SpeedEditor EffectsSpeedEditor;
        private System.Windows.Forms.RibbonHost ShadowHost;
        private CustomPropertyEditors.ShadowEditor EffectsShadowEditor;
        private System.Windows.Forms.RibbonHost BlurHost;
        private CustomPropertyEditors.BlurEditor EffectsBlurEditor;
        private System.Windows.Forms.RibbonHost GlowHost;
        private GlowEditor EffectsGlowEditor;
        private CustomPropertyEditors.GradientEditor TextColorEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel10;
        private System.Windows.Forms.RibbonHost TextColorHost;
        private System.Windows.Forms.RibbonPanel ribbonPanel11;
        private System.Windows.Forms.RibbonHost TextOutlineHost;
        private CustomPropertyEditors.OutlineColorEditor TextOutlineEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel12;
        private System.Windows.Forms.RibbonHost TextFontHost;
        private System.Windows.Forms.RibbonTab TabGraphics;
        private System.Windows.Forms.RibbonPanel ribbonPanel13;
        private System.Windows.Forms.RibbonHost GraphicShapeHost;
        private CustomPropertyEditors.ShapeEditor GraphicsShapeEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel14;
        private System.Windows.Forms.RibbonHost GraphicColorHost;
        private CustomPropertyEditors.GradientEditor GraphicsColorEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel15;
        private System.Windows.Forms.RibbonHost GraphicOutlineHost;
        private CustomPropertyEditors.OutlineColorEditor GraphicsOutlineEditor;
        private System.Windows.Forms.RibbonTab TabTicker;
        private System.Windows.Forms.RibbonPanel ribbonPanel16;
        private System.Windows.Forms.RibbonHost TickerNewContentHost;
        private CustomPropertyEditors.TickerContentEditor TickerNewContentEditor;
        private CustomPropertyEditors.TickerEditor TickerPropsEditor;
        private CustomPropertyEditors.ShapeEditor TickerShapeEditor;
        private CustomPropertyEditors.GradientEditor TickerColorEditor;
        private CustomPropertyEditors.OutlineColorEditor TickerOutlineEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel17;
        private System.Windows.Forms.RibbonHost TickerPropsHost;
        private System.Windows.Forms.RibbonPanel ribbonPanel18;
        private System.Windows.Forms.RibbonHost TickerShapeHost;
        private System.Windows.Forms.RibbonPanel ribbonPanel19;
        private System.Windows.Forms.RibbonHost TickerColorHost;
        private System.Windows.Forms.RibbonPanel ribbonPanel20;
        private System.Windows.Forms.RibbonHost TickerOutlineHost;
        private System.Windows.Forms.RibbonTab TabImage;
        private System.Windows.Forms.RibbonPanel ribbonPanel21;
        private System.Windows.Forms.RibbonHost ImageSourceHost;
        //private CustomPropertyEditors.OpenImageEditor ImageEditor;
        private System.Windows.Forms.RibbonTab TabFlash;
        private System.Windows.Forms.RibbonPanel ribbonPanel22;
        private System.Windows.Forms.RibbonHost FlashSourceHost;
        private CustomPropertyEditors.OpenFlashEditor FlashEditor;
        private System.Windows.Forms.RibbonTab ribbonTab3;
        private System.Windows.Forms.RibbonPanel GroupActions;
        private System.Windows.Forms.RibbonHost GroupHost;
        private System.Windows.Forms.Button BtnGroup;
        private System.Windows.Forms.RibbonHost UngroupHost;
        private System.Windows.Forms.Button BtnUnGroup;
        private System.Windows.Forms.RibbonPanel GroupZOrder;
        private System.Windows.Forms.RibbonHost LayerUpHost;
        private System.Windows.Forms.Button BtnUp;
        private System.Windows.Forms.RibbonHost LayerDownHost;
        private System.Windows.Forms.Button BtnDown;
        private System.Windows.Forms.RibbonHost FrontHost;
        private System.Windows.Forms.Button BtnToFront;
        private System.Windows.Forms.RibbonHost BackHost;
        private System.Windows.Forms.Button BtnToBack;
        private System.Windows.Forms.RibbonPanel GroupAlign;
        private System.Windows.Forms.RibbonHost ALeftsHost;
        private System.Windows.Forms.Button AlignLefts;
        private System.Windows.Forms.RibbonHost ACenterHost;
        private System.Windows.Forms.Button AlignCenters;
        private System.Windows.Forms.RibbonHost ARightsHost;
        private System.Windows.Forms.Button AlignRights;
        private System.Windows.Forms.RibbonHost ATopHost;
        private System.Windows.Forms.Button AlignTops;
        private System.Windows.Forms.RibbonHost AMiddleHost;
        private System.Windows.Forms.Button AlignMiddles;
        private System.Windows.Forms.RibbonHost ABottomHost;
        private System.Windows.Forms.Button AlignBottoms;
        private System.Windows.Forms.RibbonOrbMenuItem LoadConfig;
        private System.Windows.Forms.RibbonOrbMenuItem SaveConfig;
        private System.Windows.Forms.RibbonOrbOptionButton ribbonOrbOptionButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonHost tickerTextHost;
        private TextGradientEditor TickerTextColorEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel23;
        private System.Windows.Forms.RibbonHost tickerTextOutlineHost;
        private TextOutlineColorEditor TickerTextOutlineEditor;
        private System.Windows.Forms.RibbonPanel ribbonPanel24;
        private System.Windows.Forms.RibbonHost TickerTextFontHost;
        private FontEditor TickerTextFontEditor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtConfigFileName;
        private System.Windows.Forms.Button btnSaveCGConfig;
        private System.Windows.Forms.Label lblAddTextLogo;
        private System.Windows.Forms.TextBox txtAddTextLogo;
        private System.Windows.Forms.Button btnAddTextLogo;
        private System.Windows.Forms.Button btnClose;
      

    }
}

