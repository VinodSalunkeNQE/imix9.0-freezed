﻿namespace CGEditor_WinForms
{
    partial class XMLDescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.XmlDescriptionControl = new XmlEditor();
            this.ApplyXMLButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // XmlDescriptionControl
            // 
            this.XmlDescriptionControl.AllowXmlFormatting = true;
            this.XmlDescriptionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XmlDescriptionControl.Location = new System.Drawing.Point(0, 0);
            this.XmlDescriptionControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.XmlDescriptionControl.Name = "XmlDescriptionControl";
            this.XmlDescriptionControl.ReadOnly = false;
            this.XmlDescriptionControl.Size = new System.Drawing.Size(813, 520);
            this.XmlDescriptionControl.TabIndex = 0;
            this.XmlDescriptionControl.Visible = false;
            // 
            // ApplyXMLButton
            // 
            this.ApplyXMLButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyXMLButton.Location = new System.Drawing.Point(631, 475);
            this.ApplyXMLButton.Name = "ApplyXMLButton";
            this.ApplyXMLButton.Size = new System.Drawing.Size(170, 33);
            this.ApplyXMLButton.TabIndex = 1;
            this.ApplyXMLButton.Text = "APPLY";
            this.ApplyXMLButton.UseVisualStyleBackColor = true;
            this.ApplyXMLButton.Click += new System.EventHandler(this.ApplyXMLButton_Click);
            // 
            // XMLDescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 520);
            this.Controls.Add(this.ApplyXMLButton);
            this.Controls.Add(this.XmlDescriptionControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "XMLDescription";
            this.Text = "XML Description";
            this.ResumeLayout(false);

        }

        #endregion

        private XmlEditor XmlDescriptionControl;
        private System.Windows.Forms.Button ApplyXMLButton;

    }
}