﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace CGEditor_WinForms
{
    public partial class PropertiesForm : DockContent
    {
        public PropertiesForm()
        {
            InitializeComponent();
        }

        public ListBox ListBoxItems;

        public void SetControlledObject(ListBox source)
        {
            ListBoxItems = source;
            Controls.Add(ListBoxItems);
            ListBoxItems.Dock = DockStyle.Fill;
        }

    }
}
