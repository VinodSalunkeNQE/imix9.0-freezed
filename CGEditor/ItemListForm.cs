﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace CGEditor_WinForms
{
    public partial class ItemListForm : DockContent
    {
        public ItemListForm()
        {
            InitializeComponent();
        }

        public ListBox ListBoxTools;

        public void SetControlledObject(ListBox sourceListBox)
        {
            ListBoxTools = sourceListBox;
            Controls.Add(ListBoxTools);
            ListBoxTools.Dock = DockStyle.Fill;

        }
    }
}
