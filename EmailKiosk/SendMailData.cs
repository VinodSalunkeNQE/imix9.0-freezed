﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Management;
using System.Windows.Threading;
using System.Windows;

namespace EmailKiosk
{
    public static class SendMailData
    {
        public static bool flgClosed = false;
        public static string EmailRecipients;
        public static string EmailSenderName;
        public static string EmailMessage;
        public static string EmailSubject;
        public static string EmailOrdernumber;
        public static string EmailFolderPath;
        public static bool IsEmail;
        public static string mediaName;
        public static string MediaOrderNo;
        public static string EmailExtraMessage;
        public static int LanguageId;
        public static string LocationId;
        public static string EmailLangMessage;
        public static bool flgMsgbyLang = false;
        public static string HotFolderPath;

        public static DispatcherTimer timerobj = new DispatcherTimer();
        public static void CallTimer()
        {
            flgClosed = false;
            timerobj.Interval = TimeSpan.FromSeconds(120);

            timerobj.Tick += new EventHandler(timerobj_Tick);
            timerobj.Start();
        }
        static void timerobj_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!flgClosed)
                {
                    timerobj.Stop();
                    timerobj.IsEnabled = false;
                    CloseAllWindows();
                    timerobj.Tick -= new EventHandler(timerobj_Tick);
                } 
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            //finally
            //{
            //    timerobj.Start();
            //}
        }

        #region InstancesOfClasses
        private static Scan instancescan;

        public static Scan InstanceScan
        {
            get
            {
                if (instancescan == null)
                {
                    instancescan = new Scan();
                }
                return instancescan;
            }
        }
        private static DisplayImages instancedisplayimages;

        public static DisplayImages InstanceDisplayimages
        {
            get
            {
                if (instancedisplayimages == null)
                {
                    instancedisplayimages = new DisplayImages();
                }
                return instancedisplayimages;
            }
        }

        private static EnterOrderID instanceorderid;

        public static EnterOrderID InstanceOrderId
        {
            get
            {
                if (instanceorderid == null)
                {
                    instanceorderid = new EnterOrderID();
                }
                return instanceorderid;
            }
        }

        private static EnterEmailAddress instanceemailaddress;

        public static EnterEmailAddress InstanceEmailAddress
        {
            get
            {
                if (instanceemailaddress == null)
                {
                    instanceemailaddress = new EnterEmailAddress();
                }
                return instanceemailaddress;
            }
        }
        private static EnterSenderName instancesendername;

        public static EnterSenderName Instancesendername
        {
            get
            {
                if (instancesendername == null)
                {
                    instancesendername = new EnterSenderName();
                }
                return instancesendername;
            }
        }

        private static SelectAMessage instanceselectmessage;

        public static SelectAMessage InstanceSelectmessage
        {
            get
            {
                if (instanceselectmessage == null)
                {
                    instanceselectmessage = new SelectAMessage();
                }
                return instanceselectmessage;
            }
        }


        private static ReviewDeliveryInformation instancereview;

        public static ReviewDeliveryInformation InstanceReview
        {
            get
            {
                if (instancereview == null)
                {
                    instancereview = new ReviewDeliveryInformation();
                }
                return instancereview;
            }
        }
        private static Begin instancebegin;

        public static Begin InstanceBegin
        {
            get
            {
                if (instancebegin == null)
                {
                    instancebegin = new Begin();
                }
                return instancebegin;
            }
        }
        private static Language instanceLang;

        public static Language InstanceLang
        {
            get
            {
                if (instanceLang == null)
                {
                    instanceLang = new Language();
                }
                return instanceLang;
            }
        }

        #endregion
        public static void CloseAllWindows()
        {
            InstanceScan.Hide();
            InstanceOrderId.Hide();
            InstanceDisplayimages.Hide();
            InstanceReview.GrdMsg.Visibility = Visibility.Collapsed;
            InstanceReview.Hide();
            InstanceEmailAddress.Hide();
            InstanceSelectmessage.Hide();
            Instancesendername.Hide();
            InstanceLang.Hide();
            ClearResources();

            Window window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                //if (wnd.Title != "Begin" && wnd.Title != "EnterOrderID")
                if (wnd.Title != "Kiosk")
                {
                    wnd.Close();
                }
            }

            if (window != null)
            {
                window.Close();
            }
            //if (!InstanceBegin.IsActive) && wnd.Title != "EnterOrderID"
            //{
            //    InstanceBegin.Show();
            //}
            InstanceBegin.Show();
            InstanceBegin.Activate();
            timerobj.Stop();
            flgClosed = true;
        }

        public static ObservableCollection<MyImageClass> MyOrderedImages { get; set; }

        public static void ClearResources()
        {
            SendMailData.EmailMessage = null;
            SendMailData.EmailRecipients = null;
            SendMailData.EmailSenderName = null;
            SendMailData.EmailOrdernumber = null;
            SendMailData.EmailFolderPath = null;
            SendMailData.MyOrderedImages = null;
            SendMailData.LocationId = null;
           
            //new addidtions
            SendMailData.instancedisplayimages = null;
            SendMailData.instancescan = null;
            SendMailData.instanceorderid = null;
            SendMailData.instanceLang = null;
            //email related instances
            SendMailData.instanceemailaddress = null;
            SendMailData.instancesendername = null;
            SendMailData.instanceselectmessage = null;
            SendMailData.instancereview = null;
        }
        public static void Shutdown()
        {
            ManagementBaseObject mboShutdown = null;
            ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();          // You can't shutdown without security privileges         
            mcWin32.Scope.Options.EnablePrivileges = true;
            ManagementBaseObject mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");
            // Flag 1 means we want to shut down the system. Use "2" to reboot.         
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown", mboShutdownParams, null);
            }


        }

        public static void CloseProductType()
        {
            
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ProductType")
                {
                    window = (ProductType)wnd;
                    window.Close();
                    //break;
                }
            }
            //show new instance
            ProductType pdtype = new ProductType();
            pdtype.Show();
        }

        public static void CloseDisplayImages()
        {
            DisplayImages window1 = null;
            foreach (Window wnd1 in Application.Current.Windows)
            {
                if (wnd1.Title == "DisplayImages")
                {
                    window1 = (DisplayImages)wnd1;
                    window1.Close();
                    //break;
                }
            }
        }
    }
}
