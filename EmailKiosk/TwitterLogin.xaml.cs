﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mshtml;
using System.Windows.Navigation;
using System.Threading;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for TwitterLogin.xaml
    /// </summary>
    public partial class TwitterLogin : Window
    {
        TwitterAPIUtility utility = new TwitterAPIUtility();
        //public HTMLDocument htmlDoc {get; set;}
        string loginElement = string.Empty;
        public string txtLogin { get; set; }
        string RequestToken = string.Empty;
        string TokenSecret = string.Empty;
        string oAuth_token = string.Empty;
        string AccessToken = string.Empty;
        string AccessTokenSecret = string.Empty;
        string _verifier = string.Empty;
        //bool IsPosted = false;
        public string UserName = string.Empty;
        public string Password = string.Empty;
        //System.ComponentModel.BackgroundWorker TwitterPostworker = new System.ComponentModel.BackgroundWorker();
        //BusyWindow bs = new BusyWindow();
        public List<UploadImage> listImages = new List<UploadImage>();
        private bool IsCapsOn = false;
        ResourceManager res_man;
        CultureInfo cul;

        public TwitterLogin()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(TwitterLogin).Assembly);

            //TwitterPostworker.DoWork += new System.ComponentModel.DoWorkEventHandler(TwitterPostworker_DoWork);
            //TwitterPostworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(TwitterPostworker_RunWorkerCompleted);
        }



        /// <summary>
        /// 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateAuthorization()
        {
            try
            {
                string response = utility.RequestToken();
                string[] responseArray = response.Split('&');
                RequestToken = responseArray[0].Split('=')[1];
                TokenSecret = responseArray[1].Split('=')[1];
                oAuth_token = responseArray[0].Split('=')[1];
                _verifier = responseArray.Last();
                browser.Navigate(new Uri("https://api.twitter.com/oauth/authorize?oauth_token=" + RequestToken));
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }



        protected void GetAccessToken()
        {
            try
            {
                utility.oauthtoken = oAuth_token;
                utility.oauthtokensecret = TokenSecret;
                utility.oauthverifier = _verifier;
                string response = utility.RequestAccessToken();
                string[] responseArray = response.Split('&');
                AccessToken = responseArray[0].Split('=')[1];
                AccessTokenSecret = responseArray[1].Split('=')[1];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid Application key.","Digiphoto");
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        protected void SendStatusUpdate(string LocalImagePath, string Title)
        {
            try
            {
                utility.oauthAccessToken = AccessToken;
                utility.oauthAccessTokenSecret = AccessTokenSecret;
                string response = utility.UploadMedia(Title, LocalImagePath);
                //Please parse this response and use this to show status message.
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            HTMLDocument document = (HTMLDocument)browser.Document;
           
            IHTMLElement codeElement = document.getElementById("username_or_email");
            try
            {
                var txtLgn = document.activeElement;
                if (txtLogin != txtLgn.id)
                {
                    loginElement = string.Empty;
                }

                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            break;

                        }
                    case "SPACE":
                        {
                            loginElement = loginElement + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder.Visibility = Visibility.Visible;

                            break;
                        }
                    case "Back":
                        {

                            if (loginElement.Length > 0)
                                loginElement = loginElement.Remove(loginElement.Length - 1, 1);
                            else
                                loginElement = "";

                            break;
                        }
                    default:
                        {

                            //loginElement.innerText = loginElement.innerText + _objbtn.Content;
                            loginElement = loginElement + _objbtn.Content;

                        }
                        break;

                }

                txtLgn.innerText += loginElement;
                txtLogin = txtLgn.id;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                CloseThisWindow();
            }
        }

        private void show_key(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }



        private void browser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            try
            {             

                HTMLDocument document = (HTMLDocument)browser.Document;
                //htmlDoc = document;
               
                IHTMLElementCollection codeElements = document.getElementsByTagName("code");
                IHTMLElement userNameElement = document.getElementById("username_or_email");

                //Start Auto Login

                if (userNameElement != null)
                {
                    document.getElementById("username_or_email").setAttribute("value", UserName);
                    document.getElementById("password").setAttribute("value", Password);
                    document.getElementById("allow").click();
                }
                //End Auto Login

                
                int a = codeElements.length;
                if (codeElements != null && codeElements.length > 0)
                {
                    foreach (mshtml.IHTMLElement element in codeElements)
                    {
                        string verifier = element.innerText;
                        //txtPin.Text = verifier;
                        _verifier = verifier;
                        browser.Visibility = Visibility.Hidden;
                        //browser.Navigate("about:blank");
                        break;
                    }
                    if (_verifier.Length > 0)
                    {
                        GetAccessToken();
                        mdlFacebook.Visibility = Visibility.Visible;
                        mdlFacebook.BringIntoView();
                        browser.Visibility = Visibility.Hidden;
                        KeyBorder.Visibility = Visibility.Hidden;
                        PostImages();
                    }
                    else
                    {
                        CloseThisWindow();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                CloseThisWindow();
            }
        }



        private void PostImages()
        {
            //this.Hide();
            if (listImages != null)
            {
                foreach (UploadImage UpImg in listImages)
                {
                     SendStatusUpdate(UpImg.imgPath, UpImg.Title);
                    Thread.Sleep(20000);
                    //}
                }
                mdlFacebook.Visibility = Visibility.Hidden;               
                //browser.Visibility = Visibility.Visible;
                KeyBorder.Visibility = Visibility.Visible;

                string message = res_man.GetString("PhotoPostedMsg", cul);
                MessageBox.Show(message);
                DisplayImages displayImage = new DisplayImages();
                displayImage.SubmitMailDetails();
                CloseThisWindow();

            }
        }


        private void CloseThisWindow()
        {
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ProductType")
                {
                    window = (ProductType)wnd;
                    this.Close();
                    window.Show();
                    break;
                }
            }
        }



        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseThisWindow();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CreateAuthorization();
        }


        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }

      

    }
}
