﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Management;
using FrameworkHelper.Common;
using DigiPhoto;
using System.Globalization;
using System.Resources;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for Begin.xaml
    /// </summary>
    public partial class Begin : Window
    {
        private bool IsCapsOn = false;
        string curFocus = "";
        string Grdpopupvalue;
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        ResourceManager res_man;
        CultureInfo cul;
        public Begin()
        {
            InitializeComponent();
           
        }

        private void btnBegin_Click(object sender, RoutedEventArgs e)
        {
          //  Scan _objscan = new Scan();
            //_objscan.Show();
            try
            {
                SendMailData.InstanceLang.Show();
                //SendMailData.InstanceScan.Show();
                //SendMailData.InstanceScan.Activate();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #region Shutdowncode
        private void btn_shutdown_Click(object sender, RoutedEventArgs e)
        {
            string message = res_man.GetString("ShutdownUnlock", cul);
             string msg = res_man.GetString("Confirmshutdown", cul);
            string value = MessageBox.Show(message, msg, MessageBoxButton.YesNoCancel, MessageBoxImage.Question).ToString();
            if (value == "Yes")
            {
                GrdPopup.Visibility = Visibility.Visible;
                txtUsername.Text = "";
                txtPassword.Password = "";
                Grdpopupvalue = "Shutdown";
                txtUsername.Focusable = true;
                txtUsername.Focus();
                KeyBorder.Visibility = Visibility.Visible;
            }
            else if (value == "No")
            {
                GrdPopup.Visibility = Visibility.Visible;
                txtPassword.Password = "";
                txtUsername.Text = "";
                Grdpopupvalue = "Unklock";
                txtUsername.Focusable = true;
                txtUsername.Focus();
                KeyBorder.Visibility = Visibility.Visible;
                curFocus = "usr";
            }
        }
        private void btnsubmitpopup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValidPopup())
                {
                    if (Grdpopupvalue == "Unklock")
                    {
                        //------- By Saurabh (DIGIPHOTO-473) ----------------
                           // if (GetUserDetails(txtUsername.Text, txtPassword.Password) == "7" || GetUserDetails(txtUsername.Text, txtPassword.Password) == "17")
                        if (GetUserDetails(txtUsername.Text,CryptorEngine.Encrypt(txtPassword.Password, true)).Length > 0)
                        //------- By Saurabh (DIGIPHOTO-473) ----------------
                        {
                            GrdPopup.Visibility = Visibility.Collapsed;
                            Taskbar.Show();
                            AutoLock obj = new AutoLock();
                            obj.ReleaseKeyboardHook();
                            AutoLock.EnableDisableTaskManager(true);
                            KeyBorder.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        else
                        {
                            string message = res_man.GetString("AuthorizedUnlock", cul);
                            MessageBox.Show(message);
                        }
                    }
                    else if (Grdpopupvalue == "Shutdown")
                    {
                        if (GetUserDetails(txtUsername.Text, CryptorEngine.Encrypt(txtPassword.Password, true)) != null)
                        {
                           SendMailData.Shutdown();
                        }
                        else
                        {
                            string message = res_man.GetString("AuthorizedUnlockMsg", cul);
                            MessageBox.Show(message);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        
        private string GetUserDetails(string Username, string Password)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetUserDetailsForEmail", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", Username);
                cmd.Parameters.AddWithValue("@Password", Password);
                cn.Open();
                //------- By Saurabh (DIGIPHOTO-473) ----------------
                object objRole = cmd.ExecuteScalar();
                return objRole == null ? string.Empty : objRole.ToString();
                //return cmd.ExecuteScalar().ToString();
                //------------By Saurabh (DIGIPHOTO-473) -------------
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                cn.Close();
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPopup.Visibility = Visibility.Collapsed;
            KeyBorder.Visibility = Visibility.Collapsed;
        }
        private bool IsValidPopup()
        {

            if (txtUsername.Text == "")
            {
                string message = res_man.GetString("PleaseentertheusernameMsg", cul);
                MessageBox.Show(message);
                return false;
            }
            else if (txtPassword.Password == "")
            {
                string message = res_man.GetString("PleaseenterthepasswordMsg", cul);
                MessageBox.Show(message);
                return false;
            }

            else
            {
                return true;
            }
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(ReviewDeliveryInformation).Assembly);
                LoadContentsFromResource();
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message = res_man.GetString("CloseApplication", cul);
            string msg = res_man.GetString("Confirmshutdown", cul);
            string value = System.Windows.MessageBox.Show(message, msg, MessageBoxButton.YesNo, MessageBoxImage.Question).ToString();
            if (value == "Yes")
            {
                System.Windows.Application.Current.Shutdown();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtUsername_GotFocus(object sender, RoutedEventArgs e)
        {
            curFocus = "usr";
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string loginElement = ""; bool flgSkip = false;
                System.Windows.Controls.Button _objbtn = new System.Windows.Controls.Button();
                _objbtn = (System.Windows.Controls.Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            break;

                        }
                    case "SPACE":
                        {
                            loginElement = loginElement + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {
                            flgSkip = true;
                            if (curFocus == "usr")
                            {
                                loginElement = txtUsername.Text;
                            }
                            else if (curFocus == "pwd")
                            {
                                loginElement = txtPassword.Password;
                            }

                            if (loginElement.Length > 0)
                                loginElement = loginElement.Remove(loginElement.Length - 1, 1);
                            else
                                loginElement = "";
                            //update the text
                            if (curFocus == "usr")
                            {
                                txtUsername.Text = loginElement;
                            }
                            else if (curFocus == "pwd")
                            {
                                txtPassword.Password = loginElement;
                            }

                            break;
                        }
                    default:
                        {
                            loginElement = loginElement + _objbtn.Content;
                        }
                        break;

                }
                if (!flgSkip)
                {
                    if (curFocus == "usr")
                    {
                        txtUsername.Text += loginElement;
                    }
                    else if (curFocus == "pwd")
                    {
                        txtPassword.Password += loginElement;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //CloseThisWindow();
            }
        }

        private void txtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            curFocus = "pwd";
            KeyBorder.Visibility = Visibility.Visible;
        }
        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }

        }
    }
}
