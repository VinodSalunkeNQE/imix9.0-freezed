﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Collections;
using System.Resources;
using System.Globalization;


namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for Scan.xaml
    /// </summary>
    public partial class Scan : Window
    {
        //public static string emailfolderpath;
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        Hashtable htStat = new Hashtable();

        ResourceManager res_man;    
        CultureInfo cul;

        public Scan()
        {
            InitializeComponent();
            txtOrderNumber.Focus();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(Scan).Assembly);
        }




        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMailData.ClearResources();
                SendMailData.InstanceBegin.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void BtnorderID_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMailData.InstanceOrderId.Show();
                SendMailData.InstanceOrderId.Activate();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void txtOrderNumber_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (!string.IsNullOrEmpty(txtOrderNumber.Text.Trim()))
                    {
                        //emailfolderpath = GetHotFolderPath();
                        string orderno = txtOrderNumber.Text.ToUpperInvariant();

                        if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                        {
                            orderno = "DG-" + orderno;
                        }

                        SendMailData.MediaOrderNo = orderno;
                        ////
                        if (!GetStatusByOrderNo(SendMailData.MediaOrderNo))
                        {
                            string Msg = res_man.GetString("OrderExists", cul);
                            string headingMsg = res_man.GetString("iMIX", cul);
                            MessageBox.Show(Msg, headingMsg, MessageBoxButton.OK, MessageBoxImage.Stop);
                            return;
                        }
                        else
                        {
                            GetCurrentOrderStatus();
                            if (!htStat.ContainsValue("1"))
                            {
                                string Msg = res_man.GetString("OrderProcessed", cul);
                                string headingMsg = res_man.GetString("iMIX", cul);
                                MessageBox.Show(Msg, headingMsg, MessageBoxButton.OK, MessageBoxImage.Stop);
                                return;
                            }
                        }
                        SendMailData.EmailOrdernumber = SendMailData.MediaOrderNo;
                        ProductType productType = new ProductType();
                        productType.Show();
                        this.Hide();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        public void GetCurrentOrderStatus()
        {
            htStat = new Hashtable();
            string ordId = Convert.ToString(SendMailData.MediaOrderNo);
            using (SqlConnection con = new SqlConnection(DGconn))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.GetSocialMediaOrderStatus", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar).Value = ordId;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                string medType = rdr["DG_Orders_Details_ProductType_pkey"].ToString();
                                string medStat = rdr["Status"].ToString();
                                if (!htStat.ContainsKey(medType))
                                {
                                    htStat.Add(medType, medStat);
                                }
                            }
                        }
                    }
                }

            }
        }
        private bool GetStatusByOrderNo(string OrderId)
        {
            try
            {
                cn = new SqlConnection(DGconn);

                SqlCommand cmd = new SqlCommand("GetStatusByOrderNo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNo", OrderId);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        private string GetHotFolderPath()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetHotFolderPath", cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {
                LoadContentsFromResource();
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            txtOrderNumber.Text = "";
            txtOrderNumber.Focusable = true;
            txtOrderNumber.Focus();
            SendMailData.CallTimer();
        }

        private void LoadContentsFromResource()
        {
           
            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            scanOrderTxt.Text  = res_man.GetString("ScanOrder", cul);
            cancelContent.Text = res_man.GetString("Cancel", cul);
            orderContent.Text = res_man.GetString("EnterManually", cul);


        }
    }
}
