﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Windows.Media.Animation;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for EnterEmailAddress.xaml
    /// </summary>
    public partial class EnterEmailAddress : Window
    {
        private bool IsCapsOn = false;
        ResourceManager res_man;
        CultureInfo cul;
        public EnterEmailAddress()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(ProductType).Assembly);
            //LoadContentsFromResource();


        }

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            txtRecEmailAddress.Text = res_man.GetString("RecipientEmailMsg", cul);
            btnCancelContent.Text = res_man.GetString("Cancel", cul);
            btnContinueContent.Text = res_man.GetString("Continue", cul);


        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        break;

                    }
                case "SPACE":
                    {
                        txtRecipientEmail.Text = txtRecipientEmail.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;

                        break;
                    }
                case "Back":
                    {

                        txtRecipientEmail.Text = txtRecipientEmail.Text.Remove(txtRecipientEmail.Text.Length - 1, 1);

                        break;
                    }
                default:
                    {

                        txtRecipientEmail.Text = txtRecipientEmail.Text + _objbtn.Content;

                    }
                    break;
            }
        }

        private void show_key(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void btnContinue_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (IsValid())
            {
                try
                {
                    SendMailData.EmailRecipients = txtRecipientEmail.Text;
                    //EnterSenderName _objsendername = new EnterSenderName();
                    //_objsendername.Show();
                    SendMailData.Instancesendername.Show();
                    SendMailData.Instancesendername.Activate();
                    this.Hide();
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }

        }

        private void btnCancel_click(object sender, System.Windows.RoutedEventArgs e)
        {
            SendMailData.CloseDisplayImages();
            try
            {

                //ProductType product = new ProductType();
                //product.Show();

                SendMailData.CloseProductType();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        #region Common Methods
        private bool IsValid()
        {
          //Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");           
       //This is email validation for some complicated email addresses

            Regex regex=new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            Match match = regex.Match(txtRecipientEmail.Text);

            if (txtRecipientEmail.Text == "")
            {


                TbErr.Text = res_man.GetString("RecipientAddress", cul);  //"Enter Recipient Address";
                txtRecipientEmail.Focus();
                return false;
            }
            else if (!match.Success)
            {
                TbErr.Text = res_man.GetString("ErrorMsgEMail", cul); //"Invalid email address";
                txtRecipientEmail.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        private void txtRecipientEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (KeyBorder.Visibility == System.Windows.Visibility.Collapsed)
            {
                KeyBorder.Visibility = Visibility.Visible;
                Storyboard s;

                s = (Storyboard)this.FindResource("AppearKeyboard");
                this.BeginStoryboard(s);

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (SendMailData.EmailRecipients != null)
            //{
            //    txtRecipientEmail.Text = SendMailData.EmailRecipients;
            //}
            //txtRecipientEmail.Focusable = true;
            //txtRecipientEmail.Focus();
            //SendMailData.CallTimer();
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            LoadContentsFromResource();
            txtRecipientEmail.Text = "";
            if (SendMailData.EmailRecipients != null)
            {
                txtRecipientEmail.Text = SendMailData.EmailRecipients;
            }
            txtRecipientEmail.Focusable = true;
            txtRecipientEmail.Focus();
            SendMailData.CallTimer();

        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }
    }
}
