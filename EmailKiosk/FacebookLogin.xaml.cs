﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Facebook;
using System.Windows.Navigation;
using System.IO;
using System.Configuration;
using mshtml;
using System.Threading;
using System.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Application = System.Windows.Application;
using Path = System.IO.Path;
using WebBrowser = System.Windows.Forms.WebBrowser;


namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for FacebookLogin.xaml
    /// </summary>
    public partial class FacebookLogin : Window
    {
        private FacebookClient FBClient;
        public static string AppKey = ConfigurationManager.AppSettings["FacebookKey"].ToString();
        public HTMLDocument htmlDoc { get; set; }
        string loginElement = string.Empty;
        public string txtLogin { get; set; }
        public List<UploadImage> listImages = new List<UploadImage>();
        private bool IsCapsOn = false;
        ResourceManager res_man;
        CultureInfo cul;

        public string ApplicationId
        {
            get
            {
                return ConfigurationManager.AppSettings["FacebookKey"];
            }
        }

        public string ExtendedPermissions
        {
            get
            {
                return ConfigurationManager.AppSettings["ExtendedPermissions"];
            }
        }

        public string AppSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationSecret"];
            }
        }

        public string AccessToken { get; set; }



        //System.ComponentModel.BackgroundWorker FacebookPostworker = new System.ComponentModel.BackgroundWorker();
        //BusyWindow bs = new BusyWindow();
        public FacebookLogin()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(FacebookLogin).Assembly);

        }

            
            
        private void WebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {

            var url = e.Url.Fragment;
            if (url.Contains("access_token") && url.Contains("#"))
        {
                //this.Hide();
                url = (new Regex("#")).Replace(url, "?", 1);
                AccessToken = HttpUtility.ParseQueryString(url).Get("access_token");
            try
            {
                    //var fb = new FacebookClient(this.AccessToken);
                    //dynamic result1 = fb.Get("/me/permissions");
                    //dynamic result = fb.Post("me/feed", new { message = "My second wall post using Facebook C# SDK" });
                    PostImage(AccessToken);

                    //var newPostId = result.id;
                }
                catch (Exception exception)
                {
                    Console.Write(exception);
            }
            }
        }


        public bool PostImage(string token)
        {
            try
        {
                var fbClient = new FacebookClient(token);
            try
            {
                    foreach (var images in listImages)
                {
                        var filePath = images.imgPath;
                        var imgstream = File.OpenRead(filePath);
                        fbClient.Post("/me/photos", new
                        {
                            message = images.Title,
                            file = new FacebookMediaStream
                            {
                                ContentType = "image/jpg",
                                FileName = Path.GetFileName(filePath)
                            }.SetValue(imgstream)
                        });
                }
                    var logouturl = fbClient.GetLogoutUrl(new { access_token = token, next = "https://www.facebook.com/" });
                    WebBrowser wbrowse = (WebBrowser)((WindowsFormsHost)GrdBrowser.Children[0]).Child;
                    wbrowse.Navigate(logouturl);
                    string message = res_man.GetString("SuccessMsgFacebook", cul);
                    string messageheading = res_man.GetString("iMIX", cul);
                    System.Windows.MessageBox.Show(message, messageheading, MessageBoxButton.OK);
                    mdlFacebook.Visibility = Visibility.Hidden;
                    GrdBrowser.Visibility = Visibility.Visible;
                KeyBorder.Visibility = Visibility.Visible;
                DisplayImages displayImage = new DisplayImages();
                displayImage.SubmitMailDetails();
                    CloseThisWindow(true);

                    //string message = res_man.GetString("PhotoPostedMsg", cul);
                    //MessageBox.Show(message);
                    //mdlFacebook.Visibility = Visibility.Collapsed;
                    //WBrowser.Visibility = Visibility.Visible;
                    //KeyBorder.Visibility = Visibility.Visible;
                    //DisplayImages displayImage = new DisplayImages();
                    //displayImage.SubmitMailDetails();
                    return true;
            }
                catch
            {
                    return false;
        }

                //var imgstream = File.OpenRead(ImagePath);

                //dynamic res = fb.Post("/me/photos", new
                //{

                //    message = Status,
                //    file = new FacebookMediaStream
                //    {
                //        ContentType = "image/jpg",
                //        FileName = System.IO.Path.GetFileName(ImagePath)

                //    }.SetValue(imgstream)


                //});

                //return true;
            }
            catch
            {
                return false;
            }
        }

        private void CloseThisWindow(bool val)
        {
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ProductType")
                {
                    window = (ProductType)wnd;
                    if (val)
                    {
                        window.BtnFacebook.IsEnabled = false;
                        window.BtnFacebook.Opacity = 0.5;
                    }
                    this.Close();
                    window.Show();

                    break;
                }
            }

            DisplayImages window1 = null;
            foreach (Window wnd1 in Application.Current.Windows)
            {

                if (wnd1.Title == "DisplayImages")
                {
                    window1 = (DisplayImages)wnd1;
                    window1.Close();
                    break;
            }
        }
        }

        /// <summary>
        /// Added towards catching the asynch upload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FBClient_PostCompleted(object sender, FacebookApiEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                string errorMessage = res_man.GetString("FileSkippedMsg", cul); ;
                errorMessage += ErrorHandler.ErrorHandler.CreateErrorMessage(e.Error);
                
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return;
            }

            var result = e.GetResultData();
            string successMessage = res_man.GetString("SuccessMsg", cul); 
            successMessage += result.ToString();
            ErrorHandler.ErrorHandler.LogFileWrite(successMessage);
        }


        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WebBrowser wbrowse = (WebBrowser)((WindowsFormsHost)GrdBrowser.Children[0]).Child;
                System.Windows.Forms.HtmlDocument document = (System.Windows.Forms.HtmlDocument)wbrowse.Document;

                HtmlElement codeElement = document.GetElementById("username_or_email");

                var txtLgn = document.ActiveElement;
                if (txtLogin != txtLgn.Id)
                {
                    loginElement = string.Empty;
                }

                System.Windows.Controls.Button _objbtn = new System.Windows.Controls.Button();
                _objbtn = (System.Windows.Controls.Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            break;

                        }
                    case "SPACE":
                        {
                            loginElement = loginElement + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder.Visibility = Visibility.Visible;

                            break;
                        }
                    case "Back":
                        {

                            if (loginElement.Length > 0)
                                loginElement = loginElement.Remove(loginElement.Length - 1, 1);
                            else
                                loginElement = "";

                            break;
                        }
                    default:
                        {

                            //loginElement.innerText = loginElement.innerText + _objbtn.Content;
                            loginElement = loginElement + _objbtn.Content;

                        }
                        break;

                }

                txtLgn.InnerText += loginElement;
                txtLogin = txtLgn.Id;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                CloseThisWindow();
            }
        }

        private void show_key(object sender, RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }
        private void CloseThisWindow()
        {
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ProductType")
                {
                    window = (ProductType)wnd;
                    Close();
                    window.Show();

                    break;
                }
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseThisWindow();
            //SocialMediaLogin sml = new SocialMediaLogin();
            //sml.Show();
        }
        /*
        private void FacebookPostworker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            PostImages();
        }
        private void FacebookPostworker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            CloseThisWindow();
            DisplayImages displayImage = new DisplayImages();
            displayImage.SubmitMailDetails();
        }
        */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadContentsFromResource();

            var destinationURL = String.Format(
             @"https://www.facebook.com/dialog/oauth?client_id={0}&scope={1}&redirect_uri=http://www.facebook.com/connect/login_success.html&response_type=token",
             ApplicationId,
             ExtendedPermissions);

            // create the windows form host
            var host = new WindowsFormsHost();

            // create a new web browser
            var wbrowser = new WebBrowser { Height = 200, Width = 200 };
            wbrowser.ScriptErrorsSuppressed = true;
            // add it to winforms
            host.Child = wbrowser;

            // add it to wpf
            GrdBrowser.Children.Add(host);

            wbrowser.Navigated += WebBrowserNavigated;
            wbrowser.Navigate(destinationURL);
            //FacebookPostworker.DoWork += new System.ComponentModel.DoWorkEventHandler(FacebookPostworker_DoWork);
            //FacebookPostworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(FacebookPostworker_RunWorkerCompleted);
        }

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }
            
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }
    }
}
