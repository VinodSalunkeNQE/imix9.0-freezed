﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LinkedIn;
using System.Collections.Specialized;
using System.Web;
using System.Windows.Navigation;
using mshtml;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Resources;
using System.Globalization;


namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for LinkedInLogin.xaml
    /// </summary>
    public partial class LinkedInLogin : Window
    {
        int PostTry = 0;
        //string BaseUrl=string.Empty;
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        //private OAuthLinkedIn _oauth;   permissions no-description
        public HTMLDocument htmlDoc { get; set; }
        string loginElement = string.Empty;
        public string txtLogin { get; set; }
        private String _token;
        private String _verifier;
        private String _tokenSecret;
        private OAuthLinkedIn _oauth = new OAuthLinkedIn();
        public List<UploadImage> listImages = new List<UploadImage>();
        private bool IsCapsOn = false;
        ResourceManager res_man;
        CultureInfo cul;
        //System.ComponentModel.BackgroundWorker LinkedInPostworker = new System.ComponentModel.BackgroundWorker();
        //BusyWindow bs = new BusyWindow();
        public String Token
        {
            get
            {
                return _token;
            }
        }

        public String Verifier
        {
            get
            {
                return _verifier;
            }
        }

        public String TokenSecret
        {
            get
            {
                return _tokenSecret;
            }
        }

        public LinkedInLogin()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(LinkedInLogin).Assembly);
            LoadContentsFromResource();
            getImages();
            LinkedLogin();
            cancelContent.Text = res_man.GetString("Cancel", cul);
            //LinkedInPostworker.DoWork += new System.ComponentModel.DoWorkEventHandler(LinkedInPostworker_DoWork);
            //LinkedInPostworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(LinkedInPostworker_RunWorkerCompleted);

        }

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }
        }

        public LinkedInLogin(OAuthLinkedIn o)
        {

            try
            {
                _oauth = o;
                _token = null;
                InitializeComponent();
                //this.addressTextBox.Text = o.AuthorizationLink;
                _token = _oauth.Token;
                _tokenSecret = _oauth.TokenSecret;

                browser.Navigate(new Uri(_oauth.AuthorizationLink));

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
        }

        private void browser_Navigating(object sender, NavigatingCancelEventArgs e)
        {

            try
            {
                //this.addressTextBox.Text = e.Uri.ToString();
                if (e.Uri.Scheme == "liconnect")
                {
                    string queryParams = e.Uri.Query;
                    if (queryParams.Length > 0)
                    {
                        //Store the Token and Token Secret
                        NameValueCollection qs = HttpUtility.ParseQueryString(queryParams);
                        if (qs["oauth_token"] != null)
                        {
                            _token = qs["oauth_token"];
                        }
                        if (qs["oauth_verifier"] != null)
                        {
                            _verifier = qs["oauth_verifier"];
                        }
                    }

                    this.Close();

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
        }

        private void LinkedLogin()
        {
            try
            {
                String requestToken = _oauth.getRequestToken();
                //txtOutput.Text += "\n" + "Received request token: " + requestToken;
                //_oauth.Token = requestToken;
                this.Show();
                string verifier = _oauth.authorizeToken();

                if (string.IsNullOrEmpty(verifier))
                {
                    this.Hide();
                    CloseThisWindow(false);
                }
                else
                {
                    String accessToken = _oauth.getAccessToken();
                    PostImages();
                }

                DisplayImages window1 = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "DisplayImages")
                    {
                        window1 = (DisplayImages)wnd;
                        window1.Hide();
                        break;
                    }
                }

            }
            catch (Exception exp)
            {
            }
        }

        private void PostImages()
        {
            try
            {

                //string siteName = GetSubStoreName();
                OrderInfo orderDetails = GetSubStoreName();
                foreach (UploadImage upImg in listImages)
                {
                    PostTry = 0;
                    string[] path = upImg.imgPath.Split('\\');
                    if (path.Length > 1)
                    {
                        //PostImage(siteName + "/" + path[path.Length - 2] + "/LinkedIn/" + path.Last(), upImg.Title);
                        string folderStructurer = orderDetails.FTPURL + orderDetails.FtpFolder + "/" + orderDetails.Country + "_" + orderDetails.SubStoreName + "/" + orderDetails.OrderDate.ToString("ddMMyyyy") + "/" + path[path.Length - 2] + "/LinkedIn/" + path.Last();
                        PostImage(folderStructurer, upImg.Title);
                    }
                }
                string message = res_man.GetString("SuccessLinkdinMsg", cul);
                string messageheading = res_man.GetString("iMIX", cul);
                MessageBox.Show(message, messageheading, MessageBoxButton.OK);
                // mdlFacebook.Visibility = Visibility.Hidden;
                browser.Visibility = Visibility.Visible;
                KeyBorder.Visibility = Visibility.Visible;
                DisplayImages displayImage = new DisplayImages();
                displayImage.SubmitMailDetails();
                CloseThisWindow(true);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
        }



        private OrderInfo GetSubStoreName()
        {
            try
            {
                string sitename = string.Empty;
                cn = new SqlConnection(DGconn);

                SqlCommand cmd = new SqlCommand("GetSubStoreNameByOrderID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNo", SendMailData.MediaOrderNo);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                if (reader.HasRows)
                {
                    reader.Read();
                    OrderInfo order = new OrderInfo();
                    order.FTPURL = ConfigurationManager.AppSettings["FtpUrl"].ToString(); // reader["FtpIP"].ToString();
                    order.FtpFolder = reader["FtpFolder"].ToString();
                    order.SubStoreName = reader["DG_SubStore_Name"].ToString();
                    order.Country = reader["Country"].ToString();
                    order.OrderDate = Convert.ToDateTime(reader["DG_Orders_Date"]);
                    reader.Close();
                    return order;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                cn.Close();
            }
        }


        /// <summary>
        /// Post Image/ Text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PostImage(string FolderAndImageName, string ImageTitle)
        {
            try
            {
                PostTry++;


                string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xml += @"<share>
                  <content>
                    <title>" + ImageTitle + "</title>";
                xml += @"    <submitted-url>" + FolderAndImageName + @"</submitted-url>
                 <submitted-image-url>" + FolderAndImageName + "</submitted-image-url>";
                xml += @"
                  </content>
                  <visibility>
                    <code>anyone</code>
                  </visibility>
                </share>";

                string response = _oauth.APIWebRequest("POST", "http://api.linkedin.com/v1/people/~/shares", xml);




                /*               

               
               string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
               xml += @"<share>
                 <content>
                   <title>" + ImageTitle + @"</title>
                   <submitted-url>https://developer.linkedin.com/documents/share-api</submitted-url>
                   <submitted-image-url>http://119.82.77.188/iMixDigiPhoto/Web/" +
                                                                                 FolderAndImageName +
                   @"</submitted-image-url>
                 </content>
                 <visibility>
                   <code>anyone</code>
                 </visibility>
               </share>";

          
               */

                //string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                //xml += "<current-status>" + ImageTitle + " " +
                //                   FolderAndImageName + "</current-status>";

                //string response = _oauth.APIWebRequest("PUT", "http://api.linkedin.com/v1/people/~/current-status", xml);
                /*
                if (response == "")
                    txtApiResponse.Text = "Your new status updated";
                btnSendStatusUpdate.Focus();

                string response = _oauth.APIWebRequest("POST", "http://api.linkedin.com/v1/people/~/shares", xml);
                //if (response == "")
                //    txtOutput.Text += "\nYour new status updated.  view linkedin for status.";
                if (!string.IsNullOrEmpty(response))
                {
                    string strResult = "\n" + response;
                    strResult += "\nYour new share updated.  View linkedin for shared updates.";
                    //MessageBox.Show(strResult);
                }
                */
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                if (PostTry < 3)
                    PostImage(FolderAndImageName, ImageTitle);
            }
        }

        private void getImages()
        {
            DisplayImages window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "DisplayImages")
                {
                    window = (DisplayImages)wnd;
                    window.SetImageList(this);
                    break;
                }
            }
        }


        private void CloseThisWindow(bool val)
        {
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ProductType")
                {
                    window = (ProductType)wnd;
                    if (val)
                    {
                        window.BtnLinkedIn.IsEnabled = false;
                        window.BtnLinkedIn.Opacity = 0.5;
                    }
                    this.Close();
                    window.Show();

                    break;
                }
            }

            DisplayImages window1 = null;
            foreach (Window wnd1 in Application.Current.Windows)
            {

                if (wnd1.Title == "DisplayImages")
                {
                    window1 = (DisplayImages)wnd1;
                    window1.Close();
                    break;
                }
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseThisWindow(false);
        }
        /*
       private void LinkedInPostworker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
       {
           PostImages();
       }
       private void LinkedInPostworker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
       {
           bs.Hide();
           CloseThisWindow();
       }
       */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (browser.Document != null)
                {
                    HTMLDocument document = (HTMLDocument)browser.Document;

                    IHTMLElement codeElement = document.getElementById("username_or_email");

                    var txtLgn = document.activeElement;
                    if (txtLogin != txtLgn.id)
                    {
                        loginElement = string.Empty;
                    }

                    if (txtLgn.id == "session_key-oauthAuthorizeForm" || txtLgn.id == "session_password-oauthAuthorizeForm")
                    {
                        Button _objbtn = new Button();
                        _objbtn = (Button)sender;
                        switch (_objbtn.Content.ToString())
                        {
                            case "ENTER":
                                {
                                    break;

                                }
                            case "SPACE":
                                {
                                    loginElement = loginElement + " ";
                                    break;
                                }
                            case "CLOSE":
                                {
                                    //KeyBorder.Visibility = Visibility.Visible;

                                    break;
                                }
                            case "Back":
                                {

                                    if (loginElement.Length > 0)
                                        loginElement = loginElement.Remove(loginElement.Length - 1, 1);
                                    else
                                        loginElement = "";

                                    break;
                                }
                            default:
                                {

                                    //loginElement.innerText = loginElement.innerText + _objbtn.Content;
                                    loginElement = loginElement + _objbtn.Content;

                                }
                                break;

                        }

                        txtLgn.innerText += loginElement;
                        txtLogin = txtLgn.id;
                    }
                }
                //else
                //{ }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                CloseThisWindow(false);
            }

        }

        private void show_key(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }
        public class OrderInfo
        {
            public string SubStoreName { get; set; }
            public string FTPURL { get; set; }
            public string FTPIP { get; set; }
            public string FtpFolder { get; set; }
            public DateTime OrderDate { get; set; }
            public string Country { get; set; }
        }
    }
}
