﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for LAnguage.xaml
    /// </summary>
    public partial class Language : Window
    {
        public enum LanguageTypeold
        {
            English = 1,
            Arabic = 2,
            Swedish = 3,
            Spanish = 4,
            Danish = 5
        }

        public enum LanguageType
        {
            ENGLISH = 1,
            DEUTSCH = 2,
            ESPANOL = 3,
            ITALIANO = 4,
            FRANCAIS = 5
        }
        
        public Language()
        {
            InitializeComponent();
        }



        private void btnLang_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               // SendMailData.InstanceBegin.Show();
                string senderstr = ((Button)sender).Name.Substring(3).ToUpper();
                LanguageType value = (LanguageType)Enum.Parse(typeof(LanguageType), senderstr);
                SendMailData.LanguageId = (int)value;
                GlobalConfiguration.languageSelected = (int)value;   
                SendMailData.InstanceScan.Show();
                SendMailData.InstanceScan.Activate();
                            
                this.Hide();

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
    }
}
