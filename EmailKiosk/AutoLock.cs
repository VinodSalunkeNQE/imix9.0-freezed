﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows;

namespace EmailKiosk
{
    class AutoLock
    {
        #region Declaration
        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);
        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern IntPtr SetWindowsHookEx(
            int idHook,
            LowLevelKeyboardProcDelegate lpfn,
            IntPtr hMod,
            int dwThreadId);
        private IntPtr intLLKey;
        const int WH_KEYBOARD_LL = 13;
        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hHook);
        #endregion

        #region Common Methods For Locking
        public static void EnableDisableTaskManager(bool enable)
        {
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }

        private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }
            try
            {
                if (blnEat)
                    return 1;
                else
                {
                    try
                    {
                        int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
                        return Hook;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        return 1;
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        static LowLevelKeyboardProcDelegate mHookProc;
        public void KeyboardHook(object sender, EventArgs e)
        {
            // NOTE: ensure delegate can't be garbage collected
            mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
            // Get handle to main .exe
            IntPtr hModule = GetModuleHandle(IntPtr.Zero);
            // Hook
            intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
            if (intLLKey == IntPtr.Zero)
            {
                MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

            }

        }

        public void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
        #endregion
    }
}
