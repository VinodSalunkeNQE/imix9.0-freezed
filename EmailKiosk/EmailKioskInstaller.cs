﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Configuration;


namespace EmailKiosk
{
    [RunInstaller(true)]
    public partial class EmailKioskInstaller : System.Configuration.Install.Installer
    {
        public EmailKioskInstaller()
        {
            InitializeComponent();

        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {

            base.Install(stateSaver);
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();

            string targetDirectory = Context.Parameters["targetdir"];

            string servername = Context.Parameters["Servername"];

            string username = Context.Parameters["Username"];

            string password = Context.Parameters["Password"];
            string hotfolder = Context.Parameters["HotFolder"];

            string database = "Digiphoto";

            if (string.IsNullOrEmpty(targetDirectory) || string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hotfolder))
            {
                return;
            }
            string exePath = string.Format("{0}EmailKiosk.exe", targetDirectory);
            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
            String conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True";
           string connectionsection = config.ConnectionStrings.ConnectionStrings
   ["DigiConnectionString"].ConnectionString;


         ConnectionStringSettings   connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
            ConfigurationSection syncsection = config.GetSection("connectionStrings");
            //Ensures that the section is not already protected
            if (!syncsection.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                syncsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            //config.Save(ConfigurationSaveMode.Modified, true);
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");

        }
    }
}
