﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Facebook;
using System.Windows.Navigation;
using System.IO;
using System.Configuration;
using mshtml;
using System.Threading;
using System.Windows.Forms;
using System.Resources;
using System.Globalization;


namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for FacebookLogin.xaml
    /// </summary>
    public partial class SocialMediaLogin : Window
    {
        private bool IsCapsOn = false;
        string username = "";
        string pass = "";
        Facebook fb;
        string curFocus = "";
        int PostTry = 0;
        ResourceManager res_man;
        CultureInfo cul;
        public string txtLogin { get; set; }

        public List<UploadImage> listImages = new List<UploadImage>();
        System.ComponentModel.BackgroundWorker SocialPostworker = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();

        System.ComponentModel.BackgroundWorker SocialPostworkerTwitter = new System.ComponentModel.BackgroundWorker();

        #region Twitter member
        TwitterAPIUtility utility = new TwitterAPIUtility();
        string RequestToken = string.Empty;
        string TokenSecret = string.Empty;
        string oAuth_token = string.Empty;
        string AccessToken = string.Empty;
        string AccessTokenSecret = string.Empty;
        string _verifier = string.Empty;
        System.Windows.Forms.Timer WebTimer;
        int WebTimerTick;
        #endregion

        public SocialMediaLogin()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(SocialMediaLogin).Assembly);
            LoadContentsFromResource();

            txtUserName.Focus();
            SetMediaIcon();
            SocialPostworker.DoWork += SocialPostworker_DoWork;
            SocialPostworker.RunWorkerCompleted += SocialPostworker_RunWorkerCompleted;

            SocialPostworkerTwitter.DoWork += SocialPostworkerTwitter_DoWork;
            SocialPostworkerTwitter.RunWorkerCompleted += SocialPostworkerTwitter_RunWorkerCompleted;
        }
        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            userNameContent.Text = res_man.GetString("EnterUserName", cul);
            passwordContent.Text = res_man.GetString("EnterPassword", cul);
            btnSubmit.Content = res_man.GetString("Login", cul);
            cancelContent.Text = res_man.GetString("Cancel", cul);



        }

        #region Facebook functions

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (string.Compare(SendMailData.mediaName, "Facebook", true) == 0)
            {
                if (SocialLogin())
                {
                    bs.Show();
                    SocialPostworker.RunWorkerAsync();
                }
                else
                {
                    string Msg = res_man.GetString("ErrorMsgUsernamePassword", cul);
                    string iMIXMsg = res_man.GetString("iMIX", cul);
                    System.Windows.MessageBox.Show(Msg,
                        iMIXMsg, MessageBoxButton.OK);
                }
            }
            else if (string.Compare(SendMailData.mediaName, "Twitter", true) == 0)
            {

                username = txtUserName.Text.Trim();
                pass = txtPassword.Password.Trim();
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(pass))
                {
                    string Msg = res_man.GetString("TwitterCredentialsMsg", cul);
                    System.Windows.MessageBox.Show(Msg);
                }
                else
                {
                    btnSubmit.IsEnabled = false;
                    AuthorizeAccount(username, pass);

                }
            }
        }
        void SocialPostworker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            //Save into DB
            SaveDetail();
            string Msg = res_man.GetString("SuccessMsgFacebook", cul);
            string iMIXMsg = res_man.GetString("iMIX", cul);
            System.Windows.MessageBox.Show(Msg, iMIXMsg,
                MessageBoxButton.OK);

            bs.Hide();
            this.Hide();
            //foreach (Window wnd in System.Windows.Application.Current.Windows)
            //{
            //    ProductType window = null;
            //    if (wnd.Title == "ProductType")
            //    {
            //        window = (ProductType)wnd;
            //        window.BtnFacebook.IsEnabled = false;
            //        window.ImgFacebook.Opacity = 0.5;
            //        window.Show();
            //    }
            //}
            SendMailData.CloseDisplayImages();
            SendMailData.CloseProductType();
            this.Close();


        }

        void SocialPostworker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            foreach (UploadImage UpImg in listImages)
            {
                FileUpload(UpImg.imgPath, UpImg.Title);
                SendMailData.timerobj.Stop();
                SendMailData.CallTimer();
            }

        }

        private bool SocialLogin()
        {
            bool logSucc = false;
            string usr = txtUserName.Text.Trim();
            string pwd = txtPassword.Password.Trim();
            fb = Facebook.Login(usr, pwd);
            if (fb == null)
            {
                logSucc = false;
            }
            else
            {
                logSucc = true;
            }
            return logSucc;
        }


        private void FileUpload(string picFile, string picComment)
        {
            fb.UploadPhoto(picFile, picComment);
        }


        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string loginElement = ""; bool flgSkip = false;
                System.Windows.Controls.Button _objbtn = new System.Windows.Controls.Button();
                _objbtn = (System.Windows.Controls.Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            break;

                        }
                    case "SPACE":
                        {
                            loginElement = loginElement + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {
                            flgSkip = true;
                            if (curFocus == "usr")
                            {
                                loginElement = txtUserName.Text;
                            }
                            else if (curFocus == "pwd")
                            {
                                loginElement = txtPassword.Password;
                            }

                            if (loginElement.Length > 0)
                                loginElement = loginElement.Remove(loginElement.Length - 1, 1);
                            else
                                loginElement = "";
                            //update the text
                            if (curFocus == "usr")
                            {
                                txtUserName.Text = loginElement;
                            }
                            else if (curFocus == "pwd")
                            {
                                txtPassword.Password = loginElement;
                            }

                            break;
                        }
                    default:
                        {
                            loginElement = loginElement + _objbtn.Content;
                        }
                        break;

                }
                if (!flgSkip)
                {
                    if (curFocus == "usr")
                    {
                        txtUserName.Text += loginElement;
                    }
                    else if (curFocus == "pwd")
                    {
                        txtPassword.Password += loginElement;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //CloseThisWindow();
            }
        }

        private void show_key(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }

        /*
        private void CloseThisWindow()
        {
            SocialMediaLogin window = null;
            foreach (Window wnd in System.Windows.Application.Current.Windows)
            {
                if (wnd.Title == "SocialMediaLogin")
                {
                    window = (SocialMediaLogin)wnd;

                    window.Show();
                    this.Close();
                    break;
                }
            }
        }
        */
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            SendMailData.CloseDisplayImages();
            SendMailData.CloseProductType();
            this.Hide();
            this.Close();
            //foreach (Window wnd in System.Windows.Application.Current.Windows)
            //{
            //    ProductType window = null;
            //    if (wnd.Title == "ProductType")
            //    {
            //        window = (ProductType)wnd;
            //        window.Show();
            //    }
            //}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //FacebookPostworker.DoWork += new System.ComponentModel.DoWorkEventHandler(FacebookPostworker_DoWork);
            //FacebookPostworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(FacebookPostworker_RunWorkerCompleted);
        }

        private void txtUserName_GotFocus(object sender, RoutedEventArgs e)
        {
            curFocus = "usr";
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            curFocus = "pwd";
            KeyBorder.Visibility = Visibility.Visible;
        }
        #endregion




        #region Twitter Functions


        void SocialPostworkerTwitter_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            btnSubmit.IsEnabled = true;
            //Save into DB
            SaveDetail();
            bs.Hide();
            string Msg = res_man.GetString("SuccessMsgTwitter", cul);
            string iMIXMsg = res_man.GetString("iMIX", cul);
            System.Windows.MessageBox.Show(Msg, iMIXMsg,
                MessageBoxButton.OK);

            SendMailData.CloseDisplayImages();
            SendMailData.CloseProductType();
            this.Hide();
            this.Close();
            //this.Hide();
            //foreach (Window wnd in System.Windows.Application.Current.Windows)
            //{
            //    ProductType window = null;
            //    if (wnd.Title == "ProductType")
            //    {
            //        window = (ProductType)wnd;
            //        window.BtnTwitter.IsEnabled = false;
            //        window.ImgTwitter.Opacity = 0.5;
            //        window.Show();
            //    }
            //}
            //this.Close();
        }

        void SocialPostworkerTwitter_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                UploadAtTwitter();

            }
            catch
            { }
        }


        /*
        public MainWindow()
        {
            InitializeComponent();
           AuthorizeAccount("sanjay.singh@commdel.net","comm@123");
        }

       */

        /// <summary>
        /// 1st
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AuthorizeAccount(string UserName, string Password)
        {
            string response = utility.RequestToken();
            string[] responseArray = response.Split('&');
            RequestToken = responseArray[0].Replace("oauth_token=", "");//Split('=')[1];
            TokenSecret = responseArray[1].Replace("oauth_token_secret=", "");// Split('=')[1];
            oAuth_token = RequestToken;// responseArray[0]. Split('=')[1];
            _verifier = responseArray.Last();

            System.Windows.Forms.WebBrowser WB = new System.Windows.Forms.WebBrowser();
            WB.ScriptErrorsSuppressed = true;
            WB.Navigate("https://api.twitter.com/oauth/authorize?oauth_token=" + RequestToken);
            WBNavigatingWait(WB);

            if (WB.Document.GetElementById("username_or_email") != null)
            {
                WB.Document.GetElementById("username_or_email").SetAttribute("value", UserName);
                WB.Document.GetElementById("password").SetAttribute("value", Password);
               
                ////First step Authentication
                //WB.Document.GetElementById("allow").InvokeMember("click");
                //WBNavigatingWait(WB, 2);
                ////Second step verification (Added on 10-Feb-2015 towards changes done by twitter api)
                //WB.Document.GetElementById("allow").InvokeMember("click");
                //        WBNavigatingWait(WB, 2);

                foreach (HtmlElement Ele in WB.Document.GetElementsByTagName("input"))
                {
                    if ((!(Ele.GetAttribute("value") == null) && Ele.GetAttribute("value").Equals("Authorize app")))
                    {
                        Ele.InvokeMember("click");
                        WBNavigatingWait(WB, 2);
                        break;
                    }
                }

                string verifier = string.Empty;
                System.Windows.Forms.HtmlElementCollection codes = WB.Document.GetElementsByTagName("code");
                foreach (System.Windows.Forms.HtmlElement code in codes)
                {
                    verifier = code.InnerText;
                    //txtPin.Text = verifier;
                }

                if (PinVerify(verifier) == true)
                {
                    bs.Show();
                    SocialPostworkerTwitter.RunWorkerAsync();
                    //System.Windows.MessageBox.Show("Successfully Posted");
                    //BtnCancel_Click(new System.Windows.Controls.Button(), new RoutedEventArgs());
                }
                else
                {
                    btnSubmit.IsEnabled = true;
                    string Msg = res_man.GetString("ErrorMsgUsernamePassword", cul);
                    string iMIXMsg = res_man.GetString("iMIX", cul);
                    System.Windows.MessageBox.Show(Msg,
                       iMIXMsg, MessageBoxButton.OK);
                }
            }
            else
            {
                try
                {
                    foreach (HtmlElement Ele in WB.Document.GetElementsByTagName("input"))
                    {
                        if ((!(Ele.GetAttribute("value") == null) && Ele.GetAttribute("value").Equals("Sign out")))
                        {
                            Ele.InvokeMember("click");
                            WBNavigatingWait(WB, 1);
                            AuthorizeAccount(UserName, Password);
                            //break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                }
                catch
                {
                }
            }
        }

        private void UploadAtTwitter()
        {
            //Start Posting Images
            PostImages();

        }


        private bool PinVerify(string verifier)
        {
            try
            {
                utility.oauthtoken = oAuth_token;
                utility.oauthtokensecret = TokenSecret;
                utility.oauthverifier = verifier;
                string response = utility.RequestAccessToken();
                string[] responseArray = response.Split('&');
                AccessToken = responseArray[0].Replace("oauth_token=", "");// responseArray[0].Split('=')[1];
                AccessTokenSecret = responseArray[1].Replace("oauth_token_secret=", "");//responseArray[1].Split('=')[1];
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void WBNavigatingWait(System.Windows.Forms.WebBrowser WB, int Sec = 0)
        {
            WebTimer = new System.Windows.Forms.Timer();
            WebTimer.Interval = 1000;
            WebTimerTick = 0;
            WebTimer.Tick += WebTimer_Tick;
            WebTimer.Start();
            while (!((WB.ReadyState == WebBrowserReadyState.Complete && WebTimerTick > Sec)))
            {
                System.Windows.Forms.Application.DoEvents();
            }
            WebTimer.Stop();
            WebTimer.Dispose();
        }

        public void WebTimer_Tick(System.Object sender, System.EventArgs e)
        {
            WebTimerTick = WebTimerTick + 1;

        }


        private void PostImages()
        {
            //this.Hide();
            if (listImages != null)
            {
                foreach (UploadImage UpImg in listImages)
                {
                    PostTry = 0;
                    SendStatusUpdate(UpImg.imgPath, UpImg.Title);
                    SendMailData.timerobj.Stop();
                    SendMailData.CallTimer();
                }
            }
        }

        protected void SendStatusUpdate(string LocalImagePath, string Title)
        {
            try
            {
                PostTry++;
                utility.oauthAccessToken = AccessToken;
                utility.oauthAccessTokenSecret = AccessTokenSecret;
                string response = utility.UploadMedia(Title, LocalImagePath, AccessToken, AccessTokenSecret);
                //Please parse this response and use this to show status message.


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                if (PostTry < 3)
                    SendStatusUpdate(LocalImagePath, Title);
            }
        }

        #endregion

        private void SaveDetail()
        {
            DisplayImages displayImage = new DisplayImages();
            displayImage.SubmitMailDetails();
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }


        private void SetMediaIcon()
        {
            try
            {

                if (string.Compare(SendMailData.mediaName, "Facebook", true) == 0)
                {
                    //_objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    imgMedia.Source = new BitmapImage(new Uri(@"images/facebook.png", UriKind.Relative));
                }
                else if (string.Compare(SendMailData.mediaName, "Twitter", true) == 0)
                {
                    imgMedia.Source = new BitmapImage(new Uri(@"images/Twitter.png", UriKind.Relative));
                }
                else if (string.Compare(SendMailData.mediaName, "LinkedIn", true) == 0)
                {
                    imgMedia.Source = new BitmapImage(new Uri(@"images/LinkedIn.png", UriKind.Relative));
                }
                else if (string.Compare(SendMailData.mediaName, "Pinterest", true) == 0)
                {
                    imgMedia.Source = new BitmapImage(new Uri(@"images/Pinterest.png", UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }
    }
}
