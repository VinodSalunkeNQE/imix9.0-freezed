﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
using System.Threading;
using System.Diagnostics;

/// <summary>
/// Summary description for TwitterAPIUtility
/// </summary>
public class TwitterAPIUtility
{
    public string oauthconsumerkey = ConfigurationManager.AppSettings["TwitterKey"];// "F1TN81CjERefDqjDxcT1CBu7J";
    public string oauthconsumersecret = ConfigurationManager.AppSettings["TwitterSecret"];//"3DDjA2VZixHhPbfrUlKk8pgwqZ4rqR0WpSCduOkJPsRUD0euNT";
    public string oauthtokensecret = "";
    public string oauthtoken = "";
    public string oauthverifier = "";
    public string oauthAccessToken = "";
    public string oauthAccessTokenSecret = "";
    private SynchronizationContext sc = null;
    private string _tweet_api = "https://api.twitter.com/1.1/statuses/update_with_media.json";

    private class MyParam
    {
        public WebRequest wr;
        public string image_path;
        public string strBoundary;
        public string text;
    }
    public TwitterAPIUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string RequestToken()
    {
        string oauthcallback = "oob";
        string oauthtokensecret = string.Empty;
        string oauthtoken = string.Empty;
        string oauthsignaturemethod = "HMAC-SHA1";
        string oauthversion = "1.0";
        string oauthnonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        string oauthtimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        string url = "https://api.twitter.com/oauth/request_token?oauth_callback=" + oauthcallback;
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("oauth_version", oauthversion);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauthnonce);
        basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
        basestringParameters.Add("oauth_timestamp", oauthtimestamp);
        basestringParameters.Add("oauth_callback", Uri.EscapeDataString(oauthcallback));

        //Build the signature string
        string baseString = String.Empty;
        baseString += "POST" + "&";
        baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //Remove the trailing ambersand char last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthtokensecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string oauthsignature = Convert.ToBase64String(
          hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = false;
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);

        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauthnonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" + "\"" +
          Uri.EscapeDataString(oauthsignaturemethod) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauthtimestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(oauthsignature) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauthversion) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "POST";
        webRequest.ContentType = "application/x-www-form-urlencoded";

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;
        string responseFromServer = string.Empty;
        try
        {
            //webRequest.Proxy = new WebProxy();
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        
        {
        }
        return responseFromServer + "&" + oauthtimestamp;
    }

    public void Authorize()
    {
        string oauthsignaturemethod = "HMAC-SHA1";
        string oauthversion = "1.0";

        string url = "https://api.twitter.com/oauth/authorize?oauth_token=" + oauthtoken;
        string oauthnonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        string oauthtimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("oauth_version", oauthversion);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauthnonce);
        basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
        basestringParameters.Add("oauth_timestamp", oauthtimestamp);

        //Build the signature string
        string baseString = String.Empty;
        baseString += "GET" + "&";
        baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //Remove the trailing ambersand char last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthtokensecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string oauthsignature =
          Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));


        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = true;

        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);

        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauthnonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" + "\"" +
          Uri.EscapeDataString(oauthsignaturemethod) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauthtimestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(oauthsignature) + "\",";
        authorizationHeaderParams += "oauth_token=" + "\"" +
          Uri.EscapeDataString(oauthtoken) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauthversion) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "GET";
        webRequest.ContentType = "application/x-www-form-urlencoded";

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;
        try
        {
            //webRequest.Proxy = new WebProxy("enter proxy details/address");
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        {
        }
    }

    public string RequestAccessToken()
    {
        string oauthsignaturemethod = "HMAC-SHA1";
        string oauthversion = "1.0";
        string oauthnonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        string oauthtimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        string url = "https://api.twitter.com/oauth/access_token?oauth_verifier=" + oauthverifier;
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("oauth_version", oauthversion);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauthnonce);
        basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
        basestringParameters.Add("oauth_timestamp", oauthtimestamp);

        //Build the signature string
        string baseString = String.Empty;
        baseString += "POST" + "&";
        baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //Remove the trailing ambersand char last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthtokensecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string oauthsignature = Convert.ToBase64String(
          hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = false;
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);

        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauthnonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" + "\"" +
          Uri.EscapeDataString(oauthsignaturemethod) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauthtimestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(oauthsignature) + "\",";
        authorizationHeaderParams += "oauth_token=" + "\"" +
          Uri.EscapeDataString(oauthtoken) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauthversion) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "POST";
        webRequest.ContentType = "application/x-www-form-urlencoded";

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;
        string responseFromServer = string.Empty;
        try
        {
            //webRequest.Proxy = new WebProxy();
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        {
        }
        return responseFromServer;
    }

    public string SendReply(string statusMessage)
    {
        //If you want to reply to particular tweet then use @screenname along
        //with the status you need to update and status id of that particular
        //tweet to which you want to reply.
        //If you want to update status only then just post the message.
        string status = statusMessage;
        string postBody = "status=" + Uri.EscapeDataString(status);
        //string oauth_consumer_key = "your consumerkey";
        //string oauth_consumerSecret = "your consumer secret";
        string oauth_signature_method = "HMAC-SHA1";
        string oauth_version = "1.0";
        //string oauth_token = "your aouth token";
        //string oauth_token_secret = "your oauth token secret";
        string oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        string oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        //include the "in_reply_to_status_id" parameter if you need to reply to particular tweet.
        //basestringParameters.Add("in_reply_to_status_id", "status id of the post to which we are going to reply");
        basestringParameters.Add("status", Uri.EscapeDataString(status));
        basestringParameters.Add("oauth_version", oauth_version);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauth_nonce);
        basestringParameters.Add("oauth_signature_method", oauth_signature_method);
        basestringParameters.Add("oauth_timestamp", oauth_timestamp);
        basestringParameters.Add("oauth_token", oauthAccessToken);

        //Build the signature string
        string baseString = String.Empty;
        baseString += "POST" + "&";
        baseString += Uri.EscapeDataString("https://api.twitter.com/1.1/statuses/update.json") + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //GS - Remove the trailing ambersand char, remember 
        //it's been urlEncoded so you have to remove the 
        //last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key    
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthAccessTokenSecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string signatureString = Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = false;

        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://api.twitter.com/1.1/statuses/update.json");

        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauth_nonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" +
          "\"" + Uri.EscapeDataString(oauth_signature_method) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauth_timestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_token=" + "\"" +
          Uri.EscapeDataString(oauthAccessToken) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(signatureString) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauth_version) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "POST";
        webRequest.ContentType = "application/x-www-form-urlencoded";
        Stream stream = webRequest.GetRequestStream();
        byte[] bodyBytes = new ASCIIEncoding().GetBytes(postBody);
        stream.Write(bodyBytes, 0, bodyBytes.Length);
        stream.Flush();
        stream.Close();

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;

        string responseFromServer = string.Empty;
        try
        {
            //webRequest.Proxy = new WebProxy("enter proxy details/address");
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        {

        }
        return responseFromServer;
    }

    public string Search()
    {
        string url = "https://api.twitter.com/1.1/search/tweets.json?q=your search query";
        //string oauthconsumerkey = "your consumer key";
        //string oauthtoken = "your oauth token";
        //string oauthconsumersecret = "your consumer secret";
        //string oauthtokensecret = "your oauth token secret";
        string oauthsignaturemethod = "HMAC-SHA1";
        string oauthversion = "1.0";
        string oauthnonce = Convert.ToBase64String(
          new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        string oauthtimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("q", "News");
        basestringParameters.Add("oauth_version", oauthversion);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauthnonce);
        basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
        basestringParameters.Add("oauth_timestamp", oauthtimestamp);
        basestringParameters.Add("oauth_token", oauthAccessToken);
        //Build the signature string
        string baseString = String.Empty;
        baseString += "GET" + "&";
        baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //Remove the trailing ambersand char last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthAccessTokenSecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string oauthsignature = Convert.ToBase64String(
          hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = false;

        //authorization header
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);
        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauthnonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" + "\"" +
          Uri.EscapeDataString(oauthsignaturemethod) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauthtimestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_token=" + "\"" +
          Uri.EscapeDataString(oauthAccessToken) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(oauthsignature) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauthversion) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "GET";
        webRequest.ContentType = "application/x-www-form-urlencoded";

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;
        string responseFromServer = string.Empty;
        try
        {
            //Proxy settings
            //webRequest.Proxy = new WebProxy("enter proxy details/address");
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        {
        }
        return responseFromServer;
    }

    public string UploadMedia(string status, string imageFile)
    {
        string oauth_consumer_key = oauthconsumerkey;
        string oauth_consumerSecret = oauthconsumersecret;
        string oauth_signature_method = "HMAC-SHA1";
        string oauth_version = "1.0";
        string oauth_token = oauthAccessToken;
        string oauth_token_secret = oauthAccessTokenSecret;
        string oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        string oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("oauth_version", oauth_version);
        basestringParameters.Add("oauth_consumer_key", oauth_consumer_key);
        basestringParameters.Add("oauth_nonce", oauth_nonce);
        basestringParameters.Add("oauth_signature_method", oauth_signature_method);
        basestringParameters.Add("oauth_timestamp", oauth_timestamp);
        basestringParameters.Add("oauth_token", oauth_token);

        string baseString = String.Empty;

        baseString += "POST" + "&";

        baseString += Uri.EscapeDataString("https://api.twitter.com/1.1/statuses/update_with_media.json") + "&";

        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        baseString = baseString.Substring(0, baseString.Length - 3);

        string signingKey = Uri.EscapeDataString(oauth_consumerSecret) + "&" + Uri.EscapeDataString(oauth_token_secret);

        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));

        string signatureString = Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        ServicePointManager.Expect100Continue = false;

        HttpWebRequest tweetRequest = (HttpWebRequest)WebRequest.Create("https://api.twitter.com/1.1/statuses/update_with_media.json");

        tweetRequest.Method = "POST";

        tweetRequest.PreAuthenticate = true;

        tweetRequest.AllowWriteStreamBuffering = true;

        string authorizationHeaderParams = String.Empty;

        authorizationHeaderParams += "OAuth ";

        authorizationHeaderParams += "oauth_nonce=" + "\"" + Uri.EscapeDataString(oauth_nonce) + "\",";

        authorizationHeaderParams += "oauth_signature_method=" + "\"" + Uri.EscapeDataString(oauth_signature_method) + "\",";

        authorizationHeaderParams += "oauth_timestamp=" + "\"" + Uri.EscapeDataString(oauth_timestamp) + "\",";

        authorizationHeaderParams += "oauth_consumer_key=" + "\"" + Uri.EscapeDataString(oauth_consumer_key) + "\",";

        authorizationHeaderParams += "oauth_token=" + "\"" + Uri.EscapeDataString(oauth_token) + "\",";

        authorizationHeaderParams += "oauth_signature=" + "\"" + Uri.EscapeDataString(signatureString) + "\",";

        authorizationHeaderParams += "oauth_version=" + "\"" + Uri.EscapeDataString(oauth_version) + "\"";

        tweetRequest.Headers.Add("Authorization", authorizationHeaderParams);

        string boundary = "======" +
                          Guid.NewGuid().ToString().Substring(18).Replace("-", "") +
                          "======";

        var separator = "--" + boundary;
        var footer = "\r\n" + separator + "--\r\n";

        string shortFileName = Path.GetFileName(imageFile);
        string fileContentType = GetMimeType(shortFileName);
        string fileHeader = string.Format("Content-Disposition: file; " +
                                          "name=\"media\"; filename=\"{0}\"",
                                          shortFileName);
        var encoding = System.Text.Encoding.GetEncoding("iso-8859-1");

        var contents = new System.Text.StringBuilder();
        contents.AppendLine(separator);
        contents.AppendLine("Content-Disposition: form-data; name=\"status\"");
        contents.AppendLine();
        contents.AppendLine(status);
        contents.AppendLine(separator);
        contents.AppendLine(fileHeader);
        contents.AppendLine(string.Format("Content-Type: {0}", fileContentType));
        contents.AppendLine();

        using (var s = tweetRequest.GetRequestStream())
        {
            byte[] bytes = encoding.GetBytes(contents.ToString());
            s.Write(bytes, 0, bytes.Length);
            bytes = File.ReadAllBytes(imageFile);
            s.Write(bytes, 0, bytes.Length);
            bytes = encoding.GetBytes(footer);
            s.Write(bytes, 0, bytes.Length);
        }

        tweetRequest.ContentType = "multipart/form-data; boundary=\"" + boundary + "\"";

        tweetRequest.Timeout = 3 * 60 * 1000;

        string responseFromServer = string.Empty;
        try
        {
            HttpWebResponse webResponse = tweetRequest.GetResponse() as HttpWebResponse;

            Stream dataStream = webResponse.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            responseFromServer = reader.ReadToEnd();

            //TwitterPostResponse result = JsonConvert.DeserializeObject<TwitterPostResponse>(responseFromServer);

        }
        catch (WebException webex)
        {
          throw webex;
        }
        catch (Exception ex)
        {
        }
        return responseFromServer;
    }

    public string Test()
    {
        string oauthsignaturemethod = "HMAC-SHA1";
        string oauthversion = "1.0";
        string oauthnonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        string oauthtimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
        string url = "https://api.twitter.com/oauth/request_token?oauth_verifier=" + oauthverifier;
        SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
        basestringParameters.Add("oauth_version", oauthversion);
        basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
        basestringParameters.Add("oauth_nonce", oauthnonce);
        basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
        basestringParameters.Add("oauth_timestamp", oauthtimestamp);

        //Build the signature string
        string baseString = String.Empty;
        baseString += "POST" + "&";
        baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
        foreach (KeyValuePair<string, string> entry in basestringParameters)
        {
            baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
        }

        //Remove the trailing ambersand char last 3 chars - %26
        baseString = baseString.Substring(0, baseString.Length - 3);

        //Build the signing key
        string signingKey = Uri.EscapeDataString(oauthconsumersecret) +
          "&" + Uri.EscapeDataString(oauthtokensecret);

        //Sign the request
        HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
        string oauthsignature = Convert.ToBase64String(
          hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

        //Tell Twitter we don't do the 100 continue thing
        ServicePointManager.Expect100Continue = false;
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);

        string authorizationHeaderParams = String.Empty;
        authorizationHeaderParams += "OAuth ";
        authorizationHeaderParams += "oauth_nonce=" + "\"" +
          Uri.EscapeDataString(oauthnonce) + "\",";
        authorizationHeaderParams += "oauth_signature_method=" + "\"" +
          Uri.EscapeDataString(oauthsignaturemethod) + "\",";
        authorizationHeaderParams += "oauth_timestamp=" + "\"" +
          Uri.EscapeDataString(oauthtimestamp) + "\",";
        authorizationHeaderParams += "oauth_consumer_key=" + "\"" +
          Uri.EscapeDataString(oauthconsumerkey) + "\",";
        authorizationHeaderParams += "oauth_signature=" + "\"" +
          Uri.EscapeDataString(oauthsignature) + "\",";
        authorizationHeaderParams += "oauth_token=" + "\"" +
          Uri.EscapeDataString(oauthtoken) + "\",";
        authorizationHeaderParams += "oauth_version=" + "\"" +
          Uri.EscapeDataString(oauthversion) + "\"";
        webRequest.Headers.Add("Authorization", authorizationHeaderParams);

        webRequest.Method = "POST";
        webRequest.ContentType = "application/x-www-form-urlencoded";

        //Allow us a reasonable timeout in case Twitter's busy
        webRequest.Timeout = 3 * 60 * 1000;
        string responseFromServer = string.Empty;
        try
        {
            //webRequest.Proxy = new WebProxy();
            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            Stream dataStream = webResponse.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            responseFromServer = reader.ReadToEnd();
        }
        catch (Exception ex)
        {
        }
        return responseFromServer;
    }

    private static string GetMimeType(String filename)
    {
        var extension = System.IO.Path.GetExtension(filename).ToLower();
        var regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(extension);

        string result =
            ((regKey != null) && (regKey.GetValue("Content Type") != null))
            ? regKey.GetValue("Content Type").ToString()
            : "image/unknown";
        return result;
    }

    #region Twiiter Code Change
    public string UploadMedia(string status, string imageFile, string AccessToken, string AccessTokenSecret)
    {
        string _consumer_key = ConfigurationManager.AppSettings["TwitterKey"];
        string oauthconsumersecret = ConfigurationManager.AppSettings["TwitterSecret"];
        sc = SynchronizationContext.Current;

        SortedList<string, string> sl = new SortedList<string, string>();
        sl.Add("oauth_consumer_key", _consumer_key);
        sl.Add("oauth_nonce", Nonce());
        sl.Add("oauth_signature_method", "HMAC-SHA1");
        sl.Add("oauth_timestamp", TimeStamp());
        sl.Add("oauth_token", AccessToken);
        sl.Add("oauth_version", "1.0");
        string work = "";
        foreach (KeyValuePair<string, string> kvp in sl)
        {
            if (work != "")
            {
                work += "&";
            }
            work += kvp.Key + "=" + kvp.Value;
        }

        string work2 = "";
        work2 += "POST" + "&";
        // API URL
        work2 += Uri.EscapeDataString(_tweet_api) + "&";
        // Oauth 
        work2 += Uri.EscapeDataString(work);

        // OAuth tool
        Debug.WriteLine(work2);

        string oauth_signature = Signature(work2, oauthconsumersecret, AccessTokenSecret);

        work = "";
        foreach (KeyValuePair<string, string> kvp in sl)
        {
            // oauth_* 
            if (work != "")
            {
                if (kvp.Key.Substring(0, 6) == "oauth_")
                {
                    work += ", ";
                }
            }
            if (kvp.Key.Substring(0, 6) == "oauth_")
            {
                work += kvp.Key + "=" + Dd(kvp.Value);
            }
        }
        work += ", oauth_signature=" + Dd(Uri.EscapeDataString(oauth_signature));

        // OAuth tool 
        Debug.WriteLine(work);


        string strBoundary = DateTime.Now.Ticks.ToString("x");
        HttpWebRequest wr = (HttpWebRequest)HttpWebRequest.Create(_tweet_api);

        wr.Method = "POST";
        wr.ContentType = "multipart/form-data; boundary=---" + strBoundary;
        wr.Headers["Authorization"] = "OAuth " + work;


        AsyncCallback writeCallBack = new AsyncCallback(WriteCallBack);
        MyParam myParam = new MyParam() { wr = wr, image_path = imageFile, strBoundary = strBoundary, text = status };

        IAsyncResult iar1 = wr.BeginGetRequestStream(writeCallBack, myParam);
        string result;
        if (iar1.IsCompleted == true)
        {
            result = "true";
        }
        else
        {
            result = "false";
        }
        return result;
    }
    #region Change Code Upload Media
    public void WriteCallBack(IAsyncResult ar)
    {
        HttpWebRequest req = (HttpWebRequest)(ar.AsyncState as MyParam).wr;
        string path = (ar.AsyncState as MyParam).image_path;
        string strBoundary = (ar.AsyncState as MyParam).strBoundary;
        string text = (ar.AsyncState as MyParam).text;

        Stream sw = req.EndGetRequestStream(ar);

        Encoding encoding = Encoding.ASCII;

        Byte[] content = encoding.GetBytes("-----" + strBoundary + "\r\n");
        sw.Write(content, 0, content.Length);

        content = encoding.GetBytes("Content-Disposition: form-data; name=\"status\"\r\n\r\n");
        sw.Write(content, 0, content.Length);

        content = Encoding.GetEncoding("UTF-8").GetBytes(text + "\r\n");
        sw.Write(content, 0, content.Length);


        content = encoding.GetBytes("-----" + strBoundary + "\r\n");
        sw.Write(content, 0, content.Length);

        content = encoding.GetBytes("Content-Type: application/octet-stream\r\n");
        sw.Write(content, 0, content.Length);

        content = encoding.GetBytes("Content-Disposition: form-data; name=\"media[]\"; filename=\"media.jpg\"\r\n\r\n");
        sw.Write(content, 0, content.Length);


        FileStream fs = new FileStream(path, FileMode.Open);
        int buffer_len = 409600;
        long length = 0;
        int read_size = 0;
        byte[] file_data = new byte[buffer_len];
        while ((read_size = fs.Read(file_data, 0, buffer_len)) > 0)
        {
            length += read_size;
            Debug.WriteLine("writing...." + length);
            sw.Write(file_data, 0, read_size);
            if (read_size < buffer_len)
            {
                break;
            }
        }
        fs.Close();
        fs.Dispose();

        // multipart
        content = encoding.GetBytes("\r\n-----" + strBoundary + "--\r\n");
        sw.Write(content, 0, content.Length);

        sw.Close();
        sw.Dispose();


        AsyncCallback readCallBack = new AsyncCallback(this.ReadCallBack);
        IAsyncResult iar2 = req.BeginGetResponse(readCallBack, req);

    }

    public void ReadCallBack(IAsyncResult ar)
    {
        string result = "";

        HttpWebRequest req = (HttpWebRequest)ar.AsyncState;
        HttpWebResponse response = null;
        try
        {
            response = (HttpWebResponse)req.EndGetResponse(ar);

            Encoding enc = System.Text.Encoding.GetEncoding("UTF-8");
            StreamReader sr = new StreamReader(response.GetResponseStream(), enc);

            result = sr.ReadToEnd();
            Debug.WriteLine(result);
            sr.Close();
            sr.Dispose();
        }
        catch (Exception ex)
        {
            result = ex.Message;
            Debug.WriteLine(result);
        }

        if (sc != null)
            sc.Post((object state) => { OnTwitterImageUploadResult(new TwitterImageUploadArgs(result)); }, null);
       // sc.Post((object state) => { OnTwitterImageUploadResult(new TwitterImageUploadArgs(result)); }, null);

    }


    public class TwitterImageUploadArgs : EventArgs
    {

        private string message;

        public TwitterImageUploadArgs(string s)
        {
            message = s;
        }


        public string Message
        {
            get { return message; }
        }
    }

    public event EventHandler<TwitterImageUploadArgs> TwitterImageUploadResult;
    protected virtual void OnTwitterImageUploadResult(TwitterImageUploadArgs e)
    {
        EventHandler<TwitterImageUploadArgs> handler = TwitterImageUploadResult;

        if (handler != null)
        {
            handler(this, e);
        }
    }

    private string Dd(string base_string)
    {
        return "\"" + base_string + "\"";
    }

    private string Nonce()
    {
        Random rand = new Random();
        int nonce = rand.Next(1000000000);
        return nonce.ToString();
    }

    private string TimeStamp()
    {
        TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return Convert.ToInt64(ts.TotalSeconds).ToString();
    }

    private string Signature(string target, string oauthconsumersecret, string AccessTokenSecret)
    {
        string work = oauthconsumersecret + "&" + AccessTokenSecret;
        byte[] bin = Encoding.UTF8.GetBytes(target);

        HMACSHA1 hmacsha1 = new HMACSHA1();
        hmacsha1.Key = Encoding.UTF8.GetBytes(work);
        byte[] hash = hmacsha1.ComputeHash(bin);

        return Convert.ToBase64String(hash);
    }
    #endregion
    #endregion

}