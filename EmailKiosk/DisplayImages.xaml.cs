﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for DisplayImages.xaml
    /// </summary>
    public partial class DisplayImages : Window
    {

        #region Declaration
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        public string emailfolderpath;
        private bool IsCapsOn = false;
        string EmailFolderPath = string.Empty;
        ResourceManager res_man;
        CultureInfo cul;

        #endregion
        public DisplayImages()
        {
            InitializeComponent();
            EmailFolderPath = SendMailData.EmailFolderPath;
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(DisplayImages).Assembly);
        }


        private void Btncontinue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!SendMailData.IsEmail)
                {
                    if (SendMailData.mediaName == "Facebook")
                    {
                        this.Hide();
                        FacebookLogin objWin = new FacebookLogin();
                        //SocialMediaLogin objWin = new SocialMediaLogin();
                        //objWin.txbHeader.Text = "Facebook";
                        SetImageList(objWin);
                        objWin.Show();
                    }

                    if (SendMailData.mediaName == "LinkedIn")
                    {

                        LinkedInLogin objWin = new LinkedInLogin();
                        // this.Hide();

                    }
                    if (SendMailData.mediaName == "Twitter")
                    {

                        //TwitterLogin objWin = new TwitterLogin();
                        SocialMediaLogin objWin = new SocialMediaLogin();
                        //objWin.txbHeader.Text = "Twitter";
                        this.Hide();
                        SetImageList(objWin);
                        objWin.Show();

                    }
                    if (SendMailData.mediaName == "Pinwall")
                    {

                    }


                }
                else
                {

                    this.Hide();
                    SendMailData.EmailExtraMessage = SetMessage();
                    SendMailData.MyOrderedImages = MyImages;
                    SendMailData.InstanceEmailAddress.Show();
                    SendMailData.InstanceEmailAddress.Activate();

                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private string SetMessage()
        {
            string message = string.Empty;
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    //if (string.IsNullOrEmpty(message))
                    //    message = txtTitle.Text;
                    //else
                    string photoId = img.Source.ToString().Split('/').LastOrDefault().Split('.').FirstOrDefault();
                    message = message + "{" + photoId + "[" + txtTitle.Text;
                }
            }
            if (message.Length > 0)
                message = message.Remove(0, 1);
            return message;
        }

        /*
        private void SetImageList(TwitterLogin objWin)
        {
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    string Caption = txtTitle != null ? txtTitle.Text : string.Empty;
                    string path = ((System.Windows.Media.Imaging.BitmapImage)(img.Source)).UriSource.LocalPath;
                    if (img != null && path.Length > 1)
                        objWin.listImages.Add(new UploadImage(path, Caption));
                }
            }
        }
        */

        /*
        private void SetImageList(FacebookLogin objWin)
        {
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    string Caption = txtTitle != null ? txtTitle.Text : string.Empty;
                    string path = ((System.Windows.Media.Imaging.BitmapImage)(img.Source)).UriSource.LocalPath;
                    if (img != null && path.Length > 1)
                        objWin.listImages.Add(new UploadImage(img, Caption));
                }
            }
        }

        */
        private void SetImageList(SocialMediaLogin objWin)
        {
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    string Caption = txtTitle != null ? txtTitle.Text : string.Empty;
                    string path = ((System.Windows.Media.Imaging.BitmapImage)(img.Source)).UriSource.LocalPath;
                    if (img != null && path.Length > 1)
                        objWin.listImages.Add(new UploadImage(path, Caption));
                }
            }
        }
        private void SetImageList(FacebookLogin objWin)
        {
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    string Caption = txtTitle != null ? txtTitle.Text : string.Empty;
                    string path = ((System.Windows.Media.Imaging.BitmapImage)(img.Source)).UriSource.LocalPath;
                    if (img != null && path.Length > 1)
                        objWin.listImages.Add(new UploadImage(path, Caption));
                }
            }
        }

        public void SetImageList(LinkedInLogin objWin)
        {
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtTitle = FindVisualChild<TextBox>(obj);
                    Image img = FindVisualChild<Image>(obj);
                    string Caption = txtTitle != null ? txtTitle.Text : string.Empty;
                    string path = ((System.Windows.Media.Imaging.BitmapImage)(img.Source)).UriSource.LocalPath;
                    if (img != null && path.Length > 1)
                        objWin.listImages.Add(new UploadImage(path, Caption));
                }
            }
        }



        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        static public childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            if (string.Compare(_objbtn.Content.ToString(), "ClOSE", true) == 0)
            {
                KeyBorder.Visibility = Visibility.Collapsed;
                return;
            }
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    CheckBox chkB = FindVisualChild<CheckBox>(obj);

                    TextBox txtB = FindVisualChild<TextBox>(obj);
                    if (chkB.IsChecked == true)
                    {
                        switch (_objbtn.Content.ToString())
                        {
                            case "ENTER":
                                {
                                    break;

                                }
                            case "SPACE":
                                {
                                    txtB.Text = txtB.Text + " ";
                                    break;
                                }

                            case "Back":
                                {
                                    if (txtB.Text.Length > 0)
                                        txtB.Text = txtB.Text.Remove(txtB.Text.Length - 1, 1);
                                    else
                                        txtB.Text = "";
                                    break;
                                }
                            default:
                                {

                                    txtB.Text = txtB.Text + _objbtn.Content;

                                }
                                break;
                        }
                    }

                }
            }
        }

        private void show_key(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txt_Onfocus(object sender, RoutedEventArgs e)
        {
            if (KeyBorder.Visibility == System.Windows.Visibility.Collapsed)
            {
                KeyBorder.Visibility = Visibility.Visible;


            }
            //DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromItem(e);
            //TextBox txtz = FindVisualChild<TextBox>(obj);
            for (int index = 0; index < lstImages.Items.Count; index++)
            {
                DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                if (obj != null)
                {
                    TextBox txtLstInedx = FindVisualChild<TextBox>(obj);
                    if (txtLstInedx.IsFocused)
                    {
                        CheckBox chkLstInedx = FindVisualChild<CheckBox>(obj);
                        chkLstInedx.IsChecked = true;
                    }
                    else
                    {
                        CheckBox chkLstInedx = FindVisualChild<CheckBox>(obj);
                        chkLstInedx.IsChecked = false;
                    }

                }
            }

        }

        /*
        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
          
            SendMailData.CallTimer();
        }
        */
        public void SubmitMailDetails()
        {
            try
            {
                cn = new SqlConnection(DGconn);
                SqlCommand cmd = new SqlCommand("InsertEmailDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", SendMailData.MediaOrderNo);
                cmd.Parameters.AddWithValue("@DG_MediaName", (SendMailData.mediaName).Substring(0, 2));
                cn.Open();
                int i = cmd.ExecuteNonQuery();
                /*
                if (i > 0)
                {
                    MessageBox.Show("Images send successfully", "i-Mix", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                   
                }
                */
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objBtn = new Button();
                _objBtn = (Button)sender;
                if (_objBtn.Name == "btnnotmyorder")
                {
                    SendMailData.InstanceScan.Show();
                    SendMailData.InstanceScan.Activate();
                    this.Close();
                    SendMailData.ClearResources();
                    ProductType window = null;
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ProductType")
                        {
                            window = (ProductType)wnd;
                            window.Close();
                            break;
                        }
                    }
                }
                else
                {
                    ProductType window = null;
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ProductType")
                        {
                            window = (ProductType)wnd;
                            this.Hide();
                            break;
                        }
                    }

                    if (window != null)
                    {
                        window.Show();
                    }
                    else
                    {
                        ProductType enterOrderId = new ProductType();
                        enterOrderId.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (SendMailData.flgMsgbyLang)
            {
                for (int index = 0; index < lstImages.Items.Count; index++)
                {
                    DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(index);
                    if (obj != null)
                    {
                        TextBox txtTitle = FindVisualChild<TextBox>(obj);
                        txtTitle.Visibility = Visibility.Hidden;
                    }
                }
            }
            /*
            //start set focus
            DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(0);
            if (obj != null)
            {
                TextBox txtTitle = FindVisualChild<TextBox>(obj);
                txtTitle.Text = "ff";

            }
            */
            //End Set focus
            //emailfolderpath = SendMailData.EmailFolderPath;
            //myOrdernumber.Text = SendMailData.EmailOrdernumber;
            //SendMailData.CallTimer();

            //lstImages.Visibility = Visibility.Collapsed;
            ////Show processing
            //ShowLoader();

            //ThreadStart dataDownloadThread = delegate
            //{
            //    // Fill the collection Here....
            //    DisplayOrderedImages(emailfolderpath);
            //    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (EventHandler)
            //    delegate
            //    {
            //        //Hide the loader once  of data finishes...
            //        HideLoader();
            //    }, null, null);

            //};
            //dataDownloadThread.BeginInvoke(delegate(IAsyncResult aysncResult) { dataDownloadThread.EndInvoke(aysncResult); }, null);
        }

        #region Common Method
        public ObservableCollection<MyImageClass> MyImages { get; set; }
        private void DisplayOrderedImages(string FolderPath)
        {
            try
            {
                MyImages = new ObservableCollection<MyImageClass>();
                MyImages.Clear();
                string[] filePaths = Directory.GetFiles(FolderPath);
                foreach (var items in filePaths)
                {
                    string imgname = System.IO.Path.GetFileName(items);
                    if (imgname != "Thumbs.db")
                    {
                        MyImages.Add(new MyImageClass(GetImageFromResourceString(imgname)));
                    }
                }



            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private BitmapImage GetImageFromResourceString(string imageName)
        {

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 200;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(emailfolderpath + "\\" + imageName);
            image.EndInit();
            image.Freeze();
            return image;
        }
        private void ShowLoader()
        {
            loader.Visibility = Visibility.Visible;
            loader.StartStopLoader(true);
        }

        private void HideLoader()
        {
            loader.Visibility = Visibility.Collapsed;
            loader.StartStopLoader(false);
            lstImages.ItemsSource = MyImages;
            lstImages.Visibility = Visibility.Visible;
        }
        #endregion

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            myOrdernumber.Text = res_man.GetString("EnterOrderNo", cul);
            Caption.Text = res_man.GetString("LabelMsg", cul);
            btnnotmyorder.Content = res_man.GetString("NotMyOrder", cul);
            btnCancelContent.Text = res_man.GetString("Cancel", cul);
            btnContinueContent.Text = res_man.GetString("Continue", cul);


            if (SendMailData.mediaName == "EMail")
            {
                txtHeading.Text = res_man.GetString("OrderCompleted", cul); ;
            }
            else if (SendMailData.mediaName == "Facebook")
            {
                txtHeading.Text = "Facebook";
            }

            else if (SendMailData.mediaName == "Twitter")
            {
                txtHeading.Text = "Twitter";

            }
            else if (SendMailData.mediaName == "LinkedIn")
            {
                txtHeading.Text = "LinkedIn";

            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            
            LoadContentsFromResource();
            fillHeading();

            //SendMailData.EmailFolderPath = string.IsNullOrEmpty(SendMailData.EmailFolderPath) ? EmailFolderPath : SendMailData.EmailFolderPath;
            emailfolderpath = !string.IsNullOrEmpty(SendMailData.EmailFolderPath) ? SendMailData.EmailFolderPath : emailfolderpath; //SendMailData.EmailFolderPath;
            myOrdernumber.Text = SendMailData.EmailOrdernumber;
            SendMailData.CallTimer();

            lstImages.Visibility = Visibility.Collapsed;
            DisplayOrderedImages(emailfolderpath);

            ////Show processing
            //ShowLoader();

            //ThreadStart dataDownloadThread = delegate
            //{
            //    // Fill the collection Here....
            //    DisplayOrderedImages(emailfolderpath);
            //    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (EventHandler)
            //    delegate
            //    {
            //        //Hide the loader once  of data finishes...
            //        HideLoader();
            //    }, null, null);

            //};

            //dataDownloadThread.BeginInvoke(delegate(IAsyncResult aysncResult) { dataDownloadThread.EndInvoke(aysncResult); }, null);
            lstImages.ItemsSource = MyImages;
            lstImages.Visibility = Visibility.Visible;
            //fillHeading();
            //emailfolderpath = SendMailData.EmailFolderPath;
            //myOrdernumber.Text = SendMailData.EmailOrdernumber;
            //SendMailData.CallTimer();

            //lstImages.Visibility = Visibility.Collapsed;
            ////Show processing
            //ShowLoader();

            //ThreadStart dataDownloadThread = delegate
            //{
            //    // Fill the collection Here....
            //    DisplayOrderedImages(emailfolderpath);
            //    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (EventHandler)
            //    delegate
            //    {
            //        //Hide the loader once  of data finishes...
            //        HideLoader();
            //    }, null, null);

            //};
            //dataDownloadThread.BeginInvoke(delegate(IAsyncResult aysncResult) { dataDownloadThread.EndInvoke(aysncResult); }, null);

            ////start set focus
            //DependencyObject obj = lstImages.ItemContainerGenerator.ContainerFromIndex(0);
            //if (obj != null)
            //{                
            //    TextBox txtTitle = FindVisualChild<TextBox>(obj);
            //    txtTitle.Focus();

            //}
            ////End Set focus
        }


        /// <summary>
        /// For Fill Textblock
        /// </summary>
        private void fillHeading()
        {
            //if (SendMailData.mediaName == "EMail")
            //{
            //    txtHeading.Text = "Order To Be Completed";
            //}
            //else if (SendMailData.mediaName == "Facebook")
            //{
            //    txtHeading.Text = "Facebook";
            //}

            //else if (SendMailData.mediaName == "Twitter")
            //{
            //    txtHeading.Text = "Twitter";

            //}
            //else if (SendMailData.mediaName == "LinkedIn")
            //{
            //    txtHeading.Text = "LinkedIn";

            //}
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ChangeKey();
        }
        private void ChangeKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            SendMailData.timerobj.Stop();
        }

    }


    public class UploadImage
    {
        public Image img { get; set; }
        public string imgPath { get; set; }
        public string Title { get; set; }
        public UploadImage(string imgPath, string Title)
        {
            this.imgPath = imgPath;
            this.Title = Title;
        }
        public UploadImage(Image img, string Title)
        {
            this.img = img;
            this.Title = Title;
        }
    }

    #region Class For Email Images Details
    public class MyImageClass
    {
        public MyImageClass(ImageSource image)
        {

            this.Image = image;

        }
        public ImageSource Image { get; set; }


    }
    #endregion



}
