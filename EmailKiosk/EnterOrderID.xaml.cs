﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Collections;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for EnterOrderID.xaml
    /// </summary>
    public partial class EnterOrderID : Window
    {
        #region Declaration
        Hashtable htStat = new Hashtable();
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        public static string emailfolderpath;
        public string orderno;
        ResourceManager res_man;
        CultureInfo cul;
        #endregion
        public EnterOrderID()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(Scan).Assembly);
            //txtRFID.Text = OrderNumber;


        }
        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            enterOrderContent.Text = res_man.GetString("EnterOrderNo", cul);
            Ok.Content = res_man.GetString("Enter", cul); ;
            Cancel.Content = res_man.GetString("Cancel", cul);


        }
        private void bttnLogin_Enter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)


                btn_Click(Ok, new RoutedEventArgs());

        }

        public void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Name)
                {
                    case "btn1":
                        {
                            txtRFID.Text = txtRFID.Text + "1";
                            break;
                        }
                    case "btn2":
                        {
                            txtRFID.Text = txtRFID.Text + "2";
                            break;
                        }
                    case "btn3":
                        {
                            txtRFID.Text = txtRFID.Text + "3";
                            break;
                        }
                    case "btn4":
                        {
                            txtRFID.Text = txtRFID.Text + "4";
                            break;
                        }
                    case "btn5":
                        {
                            txtRFID.Text = txtRFID.Text + "5";
                            break;
                        }
                    case "btn6":
                        {
                            txtRFID.Text = txtRFID.Text + "6";
                            break;
                        }
                    case "btn7":
                        {
                            txtRFID.Text = txtRFID.Text + "7";
                            break;
                        }
                    case "btn8":
                        {
                            txtRFID.Text = txtRFID.Text + "8";
                            break;
                        }
                    case "btn9":
                        {
                            txtRFID.Text = txtRFID.Text + "9";
                            break;
                        }
                    case "btn0":
                        {
                            txtRFID.Text = txtRFID.Text + "0";
                            break;
                        }
                    case "Ok":
                        {
                            if (string.IsNullOrEmpty(txtRFID.Text.Trim()))
                            {
                                string message = res_man.GetString("EnterOrderNo", cul);//Please enter order no.
                                string msgHeading = res_man.GetString("iMIX", cul);
                                MessageBox.Show(message, msgHeading, MessageBoxButton.OK, MessageBoxImage.Stop);
                                break;
                            }
                            // productType.Owner = Application.Current.MainWindow;
                            if (!string.IsNullOrEmpty(txtRFID.Text.Trim()))
                            {

                                orderno = txtRFID.Text.ToUpperInvariant();

                                if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                                {
                                    orderno = "DG-" + orderno;


                                }

                                SendMailData.MediaOrderNo = orderno;
                                //var x = Directory.GetDirectories(GetHotFolderPath()+ "\\PrintImages\\"+ SendMailData.MediaOrderNo,"DG-",SearchOption.AllDirectories);
                                //if (x.Length<=0)
                                //{
                                //    MessageBox.Show("Order does not exists, please check order number.", "iMix", MessageBoxButton.OK, MessageBoxImage.Stop);
                                //    break;
                                //}
                                if (!GetStatusByOrderNo(SendMailData.MediaOrderNo))
                                {
                                    string message = res_man.GetString("OrderExists", cul);//Order does not exists, please check order number.
                                    string msgHeading = res_man.GetString("iMIX", cul);
                                    MessageBox.Show(message, msgHeading, MessageBoxButton.OK, MessageBoxImage.Stop);
                                    break;
                                }
                                else
                                {
                                    GetCurrentOrderStatus();
                                    if (!htStat.ContainsValue("1"))
                                    {
                                        string message = res_man.GetString("OrderProcessed", cul);
                                        string msgHeading = res_man.GetString("iMIX", cul);
                                        MessageBox.Show(message, msgHeading, MessageBoxButton.OK, MessageBoxImage.Stop);
                                        break;
                                    }
                                }
                                SendMailData.EmailOrdernumber = SendMailData.MediaOrderNo;
                                ProductType productType = new ProductType();
                                productType.Show();
                                this.Hide();
                                break;
                            }


                            break;
                        }
                    /*
                case "Btncontinu":
                    {

                        if (!string.IsNullOrEmpty(txtRFID.Text.Trim()))
                        {
                            emailfolderpath = GetHotFolderPath();
                             orderno = txtRFID.Text.ToUpperInvariant();

                            if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                            {
                                orderno = "DG-" + orderno;
                                  
                            }
                            if (GetEmailOrderStatus(orderno))
                            {
                                //emailfolderpath = System.IO.Path.Combine(emailfolderpath, "PrintImages\\Email\\") + orderno;
                                emailfolderpath = System.IO.Path.Combine(emailfolderpath, "PrintImages\\Twitter\\") + orderno;

                                if (Directory.Exists(emailfolderpath))
                                {
                                    SendMailData.EmailOrdernumber = orderno;
                                    SendMailData.EmailFolderPath = emailfolderpath;
                                    //DisplayImages _objdepImages = new DisplayImages();

                                    //_objdepImages.Show();
                                    SendMailData.InstanceDisplayimages.Show();
                                    SendMailData.InstanceDisplayimages.Activate();
                                    this.Hide();

                                }
                                else
                                {
                                    MessageBox.Show("Order does not exists, please check order number.", "i-Mix", MessageBoxButton.OK, MessageBoxImage.Stop);

                                }
                            }
                            else
                            {
                                MessageBox.Show("Order already submitted, please select a new order.", "i-Mix", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                            }
                        }
                        break;
                    }
                  */
                    case "Cancel":
                        {
                            SendMailData.ClearResources();
                            SendMailData.InstanceBegin.Show();
                            this.Hide();

                            break;
                        }
                    case "btnback":
                        {
                            if (txtRFID.Text.Length > 0)
                            {
                                txtRFID.Text = txtRFID.Text.Remove(txtRFID.Text.Length - 1, 1);
                            }
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #region Common Methods

        public void GetCurrentOrderStatus()
        {
            htStat = new Hashtable();
            string ordId = Convert.ToString(SendMailData.MediaOrderNo);
            using (SqlConnection con = new SqlConnection(DGconn))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.GetSocialMediaOrderStatus", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar).Value = ordId;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            string medType = rdr["DG_Orders_Details_ProductType_pkey"].ToString();
                            string medStat = rdr["Status"].ToString();
                            if (!htStat.ContainsKey(medType))
                            {
                                htStat.Add(medType, medStat);
                            }
                        }
                    }

                }

            }
        }
        private bool GetStatusByOrderNo(string OrderId)
        {
            try
            {
                cn = new SqlConnection(DGconn);

                SqlCommand cmd = new SqlCommand("GetStatusByOrderNo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNo", OrderId);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }
        private bool GetEmailOrderStatus(string OrderId, string MediaName)
        {
            try
            {
                cn = new SqlConnection(DGconn);

                SqlCommand cmd = new SqlCommand("GetEmailStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@DG_MediaName", MediaName);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    cn = new SqlConnection(DGconn);

            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
            //txtRFID.Focusable = true;
            //txtRFID.Focus();
            //SendMailData.CallTimer();
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {
                LoadContentsFromResource();
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            txtRFID.Text = "";
            txtRFID.Focusable = true;
            txtRFID.Focus();
            SendMailData.CallTimer();
        }

        public void SocialMedia(string mediaName, string orderno)
        {

            if (GetEmailOrderStatus(orderno, mediaName.Substring(0, 2)))
            {
                emailfolderpath = System.IO.Path.Combine(SendMailData.HotFolderPath, "PrintImages\\" + mediaName + "\\") + orderno;

                if (Directory.Exists(emailfolderpath))
                {
                    SendMailData.ClearResources();
                    SendMailData.EmailOrdernumber = orderno;
                    SendMailData.EmailFolderPath = emailfolderpath;
                    SendMailData.InstanceDisplayimages.Show();
                    SendMailData.InstanceDisplayimages.Activate();
                    SendMailData.mediaName = mediaName;
                    if (mediaName == "Email")
                        SendMailData.IsEmail = true;
                    else
                    {
                        // SendMailData.mediaName = mediaName;
                        SendMailData.IsEmail = false;
                    }

                    ProductType productType = null;
                    Window window = null;

                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ProductType")
                        {
                            wnd.Hide();
                            break;
                        }
                    }

                    //if (window == null)
                    //{
                    //    window = new ProductType();
                    //    //window.Show();
                    //    window.Close();
                    //}
                }
                else
                {
                    string message = res_man.GetString("OrderExists", cul);
                    string msgHeading = res_man.GetString("iMIX", cul);
                    MessageBox.Show(message, msgHeading, MessageBoxButton.OK, MessageBoxImage.Stop);
                }

            }
            else
            {
                string message = res_man.GetString("AlreadyOrderMsg", cul);
                string msgHeading = res_man.GetString("iMIX", cul);
                MessageBox.Show(message, msgHeading, MessageBoxButton.OK, MessageBoxImage.Stop);
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            SendMailData.timerobj.Stop();
        }
    }
}
