﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for SelectAMessage.xaml
    /// </summary>
    public partial class SelectAMessage : Window
    {
        #region Declaration
        public ObservableCollection<MyMessageClass> Mymessages { get; set; }
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        ResourceManager res_man;
        CultureInfo cul;
        #endregion
        public SelectAMessage()
        {
            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(ReviewDeliveryInformation).Assembly);
            LoadContentsFromResource();
            
            
        }
        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            selectMessageContent.Text = res_man.GetString("RequiredMsg", cul);
            btnCancelContent.Text = res_man.GetString("Cancel", cul);
            btnContinueContent.Text = res_man.GetString("Continue", cul);
            


        }

        private void Btncontinue_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            try
            {
                if (lstImages.SelectedItem != null)
                {

                    SendMailData.EmailMessage = ((EmailKiosk.MyMessageClass)(lstImages.SelectedItems[0])).Message;

                    //ReviewDeliveryInformation _objReviewDeliveryInformation = new ReviewDeliveryInformation();
                    //_objReviewDeliveryInformation.Show();
                    SendMailData.InstanceReview.Show();
                    SendMailData.InstanceReview.Activate();
                    this.Hide();
                }
                else
                {
                    TbErr.Text = res_man.GetString("RequiredMsg", cul); 
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void BtnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            try
            {
                SendMailData.CloseDisplayImages();
                //SendMailData.ClearResources();
                //SendMailData.InstanceBegin.Show();
                //SendMailData.ClearResources();
                //ProductType product = new ProductType();
                //product.Show();
                SendMailData.CloseProductType();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #region Common Methods
        private void GetEmailMessages()
        {
            try
            {

                Mymessages = new ObservableCollection<MyMessageClass>();
                Mymessages.Clear();
                SqlCommand cmd = new SqlCommand("GetEmailMessages", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();
                cmd.Parameters.Add("@LangID", SqlDbType.Int).Value = SendMailData.LanguageId;
                using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                    
                                    Mymessages.Add(new MyMessageClass(reader.GetString(0)));
                            }
                        }

                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }

            }
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {


            try
            {
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            GetEmailMessages();
            lstImages.ItemsSource = Mymessages;
            try
            {
                SendMailData.CallTimer();
                if (SendMailData.EmailMessage != null)
                {
                    int index = 0;
                    foreach (var item in Mymessages.Where(t => t.Message == SendMailData.EmailMessage))
                    {
                        index = Mymessages.IndexOf(item);
                    }

                    lstImages.SelectedItem = lstImages.Items[index];
                    ListBoxItem listBoxItem = lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem) as ListBoxItem;
                    listBoxItem.Focus();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            GetEmailMessages();
            lstImages.ItemsSource = Mymessages;
            try
            {
                SendMailData.CallTimer();
                if (SendMailData.EmailMessage != null)
                {
                    int index = 0;
                    foreach (var item in Mymessages.Where(t => t.Message == SendMailData.EmailMessage))
                    {
                        index = Mymessages.IndexOf(item);
                    }

                    lstImages.SelectedItem = lstImages.Items[index];
                    ListBoxItem listBoxItem = lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem) as ListBoxItem;
                    listBoxItem.Focus();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            //try
            //{
            //    cn = new SqlConnection(DGconn);

            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
            //GetEmailMessages();
            //lstImages.ItemsSource = Mymessages;
            //try
            //{
            //    SendMailData.CallTimer();
            //    if (SendMailData.EmailMessage != null)
            //    {
            //        int index = 0;
            //        foreach (var item in Mymessages.Where(t => t.Message == SendMailData.EmailMessage))
            //        {
            //            index = Mymessages.IndexOf(item);
            //        }

            //        lstImages.SelectedItem = lstImages.Items[index];
            //        ListBoxItem listBoxItem = lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem) as ListBoxItem;
            //        listBoxItem.Focus();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
        }

     
    }
}
