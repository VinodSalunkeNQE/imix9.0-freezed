﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for ucLoader.xaml
    /// </summary>
    public partial class ucLoader : UserControl
    {
        Storyboard LoaderAnimation = null;



        public string LoaderMessage
        {
            get { return (string)GetValue(LoaderMessageProperty); }
            set { SetValue(LoaderMessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LoaderMessage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoaderMessageProperty =
            DependencyProperty.Register("LoaderMessage", typeof(string), typeof(ucLoader), new UIPropertyMetadata(string.Empty));


        public ucLoader()
        {

            InitializeComponent();
            LoaderAnimation = this.Resources["loader_animation"] as Storyboard;

        }
        public void StartStopLoader(bool operationFlag)
        {
            if (LoaderAnimation != null)
            {
                if (operationFlag)
                    LoaderAnimation.Begin();
                else
                    LoaderAnimation.Stop();
            }
        }

    }
}
