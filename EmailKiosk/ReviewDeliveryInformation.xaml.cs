﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for ReviewDeliveryInformation.xaml
    /// </summary>
    public partial class ReviewDeliveryInformation : Window
    {
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        ResourceManager res_man;
        CultureInfo cul;
        public ReviewDeliveryInformation()
        {
            InitializeComponent();

            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(ReviewDeliveryInformation).Assembly);
            LoadContentsFromResource();


        }

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            reviewDeliveryInformation.Text = res_man.GetString("ReviewMsg", cul);

            photoGroupBx.Header = res_man.GetString("YourPhoto", cul); ;
            messageGroupBx.Header = res_man.GetString("messagedetails", cul); ;

            emailRecepientContent.Text = res_man.GetString("EmailRecepientMsg", cul)+" :";
            messageContent.Text = res_man.GetString("LabelMsg", cul) + " :";
            yournameContent.Text = res_man.GetString("LabelNameMsg", cul) + " :";
            btnCancelContent.Text = res_man.GetString("Cancel", cul);
            btnContinuecontent.Text = res_man.GetString("Continue", cul);
            txtmsg.Text = res_man.GetString("messageSent", cul);



        }


        private void Btncontinue_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            SubmitMailDetails(SendMailData.MediaOrderNo, tb_recepientName.Text, "", false, tb_sendername.Text, tb_emailmessage.Text);
        }

        private void BtnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            try
            {
                SendMailData.CloseDisplayImages();
                SendMailData.CloseProductType();
                this.Hide();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        #region Common Methods
        private void GetTheDataForPage()
        {
            if (SendMailData.flgMsgbyLang)
                tb_emailmessage.Text = SendMailData.EmailLangMessage;
            else
                tb_emailmessage.Text = SendMailData.EmailMessage;
            tb_recepientName.Text = SendMailData.EmailRecipients;
            tb_sendername.Text = SendMailData.EmailSenderName;
            // lstImages.Items.Clear();
            lstImages.ItemsSource = SendMailData.MyOrderedImages;
            //foreach (var item in SendMailData.MyOrderedImages)
            //{
            //    lstImages.Items.Add(item);
            //}
            //lstImages.InvalidateVisual();
            //lstImages.InvalidateArrange();

        }

        private void SubmitMailDetails(string OrderId, string EmailTo, string EmailBcc, bool EmailIsSent, string sendername, string message)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InsertEmailDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@Emailto", EmailTo);
                cmd.Parameters.AddWithValue("@EmailBcc", EmailBcc);
                cmd.Parameters.AddWithValue("@EmailIsSent", EmailIsSent);
                cmd.Parameters.AddWithValue("@Sendername", sendername);
                cmd.Parameters.AddWithValue("@EmailMessage", message);
                cmd.Parameters.AddWithValue("@DG_MediaName", "EM");
                cmd.Parameters.AddWithValue("@DG_OtherMessage", SendMailData.EmailExtraMessage);
                cmd.Parameters.AddWithValue("@DG_MailSubject", SendMailData.EmailSubject);
                cn.Open();
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    //MessageBox.Show("Email request submitted successfully", "iMix", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                   // SendMailData.ClearResources();
                    // SendMailData.InstanceBegin.Show();
                    //this.Hide();
                    GrdMsg.Visibility = Visibility.Visible;
                }

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                // MessageBox.Show(ex.Message);

            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        private void btneditemailaddress_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMailData.InstanceEmailAddress.Show();
                SendMailData.InstanceEmailAddress.Activate();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void btnMsgedit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMailData.InstanceSelectmessage.Show();
                SendMailData.InstanceSelectmessage.Activate();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSenderNameEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SendMailData.Instancesendername.Show();
                SendMailData.Instancesendername.Activate();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                SendMailData.timerobj.Stop();
                SendMailData.CallTimer();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                SendMailData.timerobj.Stop();
                SendMailData.CallTimer();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                GetTheDataForPage();
                cn = new SqlConnection(DGconn);
                SendMailData.CallTimer();
                if (SendMailData.flgMsgbyLang)
                    btnMsgedit.Visibility = Visibility.Hidden;
                else
                    btnMsgedit.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            try
            {

                GetTheDataForPage();
                cn = new SqlConnection(DGconn);
                SendMailData.CallTimer();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnokmsg_Click(object sender, RoutedEventArgs e)
        {
            GrdMsg.Visibility = Visibility.Collapsed;
            SendMailData.CloseDisplayImages();
            SendMailData.CloseProductType();
            this.Hide();
            this.Close();
            //SendMailData.InstanceBegin.Show();
            //this.Hide();
            //foreach (Window wnd in System.Windows.Application.Current.Windows)
            //{
            //    ProductType window = null;
            //    if (wnd.Title == "ProductType")
            //    {
            //        window = (ProductType)wnd;
            //        window.BtnMail.IsEnabled = false;
            //        window.ImgMail.Opacity = 0.5;
            //        window.Show();
            //    }
            //}
            //this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            SendMailData.timerobj.Stop();
        }

    }
}
