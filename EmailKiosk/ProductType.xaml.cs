﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for ProductType.xaml
    /// </summary>
    public partial class ProductType : Window
    {
        Hashtable htStat = new Hashtable();
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        ResourceManager res_man;
        CultureInfo cul;
        public ProductType()
        {
            InitializeComponent();
            if (SendMailData.EmailOrdernumber == null)
                return;
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(ProductType).Assembly);
            LoadContentsFromResource();
            this.WindowState = System.Windows.WindowState.Maximized;
            this.WindowStyle = System.Windows.WindowStyle.None;
            GetCurrentOrderStatus();

            SendMailData.HotFolderPath = GetHotFolderPath(SendMailData.EmailOrdernumber);
            if (SocialMedia("Facebook"))
            {
                BtnFacebook.Visibility = System.Windows.Visibility.Visible;
                BtnFacebook.IsEnabled = getEnabled("80");
                if (!BtnFacebook.IsEnabled)
                {
                    ImgFacebook.Opacity = 0.5;
                }
            }
            else
            {
                BtnFacebook.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (SocialMedia("LinkedIn"))
            {
                BtnLinkedIn.Visibility = System.Windows.Visibility.Visible;
                BtnLinkedIn.IsEnabled = getEnabled("81");
                if (!BtnLinkedIn.IsEnabled)
                {
                    ImgLinkedIn.Opacity = 0.5;
                }
            }
            else
            {
                BtnLinkedIn.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (SocialMedia("Twitter"))
            {
                BtnTwitter.Visibility = Visibility.Visible;
                BtnTwitter.IsEnabled = getEnabled("82");
                if (!BtnTwitter.IsEnabled)
                {
                    ImgTwitter.Opacity = 0.5;
                }
            }
            else
            {
                BtnTwitter.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (SocialMedia("EMail"))
            {
                BtnMail.Visibility = System.Windows.Visibility.Visible;
                BtnMail.IsEnabled = getEnabled("78");
                if (!BtnMail.IsEnabled)
                {
                    ImgMail.Opacity = 0.5;
                }
            }
            else
            {
                BtnMail.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            btnCancelContent.Text = res_man.GetString("Cancel", cul);



        }

        public void GetCurrentOrderStatus()
        {
            htStat = new Hashtable();
            string ordId = Convert.ToString(SendMailData.MediaOrderNo);
            using (SqlConnection con = new SqlConnection(DGconn))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.GetSocialMediaOrderStatus", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar).Value = ordId;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                string medType = rdr["DG_Orders_Details_ProductType_pkey"].ToString();
                                string medStat = rdr["Status"].ToString();
                                if (!htStat.ContainsKey(medType))
                                {
                                    htStat.Add(medType, medStat);
                                }
                            }
                        }
                    }
                }


            }


            // ----------------  By Saurabh ------------------------------
            using (SqlConnection con = new SqlConnection(DGconn))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.USp_getMessageByLanguage", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = ordId;
                    cmd.Parameters.Add("@LangID", SqlDbType.Int).Value = SendMailData.LanguageId;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                SendMailData.EmailLangMessage = rdr["DG_Message"].ToString();
                                SendMailData.EmailSubject = rdr["DG_Subject"].ToString();
                                SendMailData.flgMsgbyLang = true;
                            }
                        }
                        else
                            SendMailData.flgMsgbyLang = false;
                    }
                }
            }
            // ----------------  By Saurabh ------------------------------
        }
        private bool getEnabled(string mediaType)
        {
            if (htStat.ContainsKey(mediaType))
            {
                string stat = htStat[mediaType].ToString();
                if (stat == "0")
                    return false;
                else
                    return true;
            }
            return false;
        }

        public bool SocialMedia(string mediaName)
        {
            string emailfolderpath = SendMailData.HotFolderPath;
            emailfolderpath = System.IO.Path.Combine(emailfolderpath, "PrintImages\\" + mediaName + "\\") + SendMailData.MediaOrderNo;
            if (Directory.Exists(emailfolderpath))
            {
                SendMailData.ClearResources();
                return true;
            }
            else
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Defined path not found. Path is " + emailfolderpath);
                return false;
            }
        }

        private bool GetEmailOrderStatus(string OrderId, string MediaName)
        {
            try
            {
                cn = new SqlConnection(DGconn);

                SqlCommand cmd = new SqlCommand("GetEmailStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", SendMailData.MediaOrderNo);
                cmd.Parameters.AddWithValue("@DG_MediaName", MediaName);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }

        private string GetHotFolderPath(string OrderNumber)
        {
            try
            {
                cn = new SqlConnection(DGconn);
                SqlCommand cmd = new SqlCommand("GetHotFolderPathsOrderWise", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNumber", OrderNumber);
                cn.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }
        private void BtnFacebook_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Name)
                {

                    case "BtnFacebook":
                        {
                            SendMailData.mediaName = "Facebook";
                            EnterOrderID enterOrderId = new EnterOrderID();
                            enterOrderId.SocialMedia("Facebook", SendMailData.MediaOrderNo);

                            this.Hide();

                            break;
                        }
                    case "BtnLinkedIn":
                        {
                            SendMailData.mediaName = "LinkedIn";
                            EnterOrderID enterOrderId = new EnterOrderID();
                            enterOrderId.SocialMedia("LinkedIn", SendMailData.MediaOrderNo);
                            this.Hide();
                            break;
                        }
                    case "BtnTwitter":
                        {
                            SendMailData.mediaName = "Twitter";
                            EnterOrderID enterOrderId = new EnterOrderID();
                            enterOrderId.SocialMedia("Twitter", SendMailData.MediaOrderNo);
                            this.Hide();
                            break;
                        }
                    case "BtnPinWall":
                        {
                            SendMailData.mediaName = "Pinterest";
                            EnterOrderID enterOrderId = new EnterOrderID();
                            enterOrderId.SocialMedia("Pinwall", SendMailData.MediaOrderNo);
                            this.Hide();
                            break;
                        }
                    case "BtnMail":
                        {
                            SendMailData.mediaName = "EMail";
                            EnterOrderID enterOrderId = new EnterOrderID();
                            enterOrderId.SocialMedia("Email", SendMailData.MediaOrderNo);
                            this.Hide();
                            break;
                        }
                    case "BtnCancel":
                        {
                            Scan scan = new Scan();
                            //enterOrderId = new EnterOrderID();
                            SendMailData.ClearResources();
                            scan.Show();
                            this.Close();
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        private void BtnHome_Click(object sender, RoutedEventArgs e)
        {

            //SendMailData.ClearResources();
            //Begin enterorderID = new Begin();
            SendMailData.InstanceBegin.Show();
            SendMailData.InstanceBegin.Activate();
            //this.Close();
            SendMailData.ClearResources();
            ProductType window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (string.Compare(wnd.Title, "ProductType", true) == 0)
                {
                    window = (ProductType)wnd;
                    window.Close();
                    break;
                }
            }
            //enterorderID.Show();

        }

        private void Window_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            SendMailData.timerobj.Stop();
            SendMailData.CallTimer();
        }
    }
}
