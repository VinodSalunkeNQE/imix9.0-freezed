﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Management;
using System.Resources;
using System.Globalization;

namespace EmailKiosk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Declaration
        TextBox controlon;
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        string emailfolderpath;
        string Grdpopupvalue;
        ResourceManager res_man;
        CultureInfo cul;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {

            InitializeComponent();
            res_man = new ResourceManager("EmailKiosk.Resource.Res", typeof(MainWindow).Assembly);
            try
            {
                cn = new SqlConnection(DGconn);
                LoadContentsFromResource();

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void LoadContentsFromResource()
        {

            int value = GlobalConfiguration.languageSelected;
            switch (value)
            {
                case 1:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case 2:
                    cul = CultureInfo.CreateSpecificCulture("de");
                    break;
                case 3:
                    cul = CultureInfo.CreateSpecificCulture("es");
                    break;
                case 4:
                    cul = CultureInfo.CreateSpecificCulture("it");
                    break;
                case 5:
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
            }




            btnsubmit.Content = res_man.GetString("Submit", cul);
            btnshutdown.Content = res_man.GetString("Shutdown", cul);


        }
        #region Events
        /// <summary>
        /// Handles the Click event of the btnsubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValid())
                {

                    lstImages.Visibility = Visibility.Collapsed;
                    btnsend.Visibility = Visibility.Collapsed;
                    KeyBorder.Visibility = Visibility.Collapsed;
                    //emailfolderpath = GetHotFolderPath();
                    string orderno = txtOrderno.Text.ToUpperInvariant();

                    if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        orderno = "DG-" + orderno;
                    }
                    if (GetEmailOrderStatus(orderno))
                    {
                        emailfolderpath = System.IO.Path.Combine(SendMailData.HotFolderPath, "PrintImages\\Email\\") + orderno;

                        if (Directory.Exists(emailfolderpath))
                        {

                            //Show processing
                            ShowLoader();
                            ThreadStart dataDownloadThread = delegate
                            {
                                // Fill the collection Here....
                                DisplayImages(emailfolderpath);
                                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (EventHandler)
                                delegate
                                {
                                    //Hide the loader once  of data finishes...
                                    HideLoader();
                                }, null, null);

                            };
                            dataDownloadThread.BeginInvoke(delegate(IAsyncResult aysncResult) { dataDownloadThread.EndInvoke(aysncResult); }, null);
                        }
                        else
                        {
                            string messageheading = res_man.GetString("iMIX", cul);
                            string message = res_man.GetString("OrderExists", cul);
                            MessageBox.Show(message, messageheading, MessageBoxButton.OK, MessageBoxImage.Stop);

                        }

                    }
                    else
                    {
                        string messageheading = res_man.GetString("iMIX", cul);
                        string message = res_man.GetString("AlreadyOrderSubmitMsg", cul);
                        MessageBox.Show(message, messageheading, MessageBoxButton.OK, MessageBoxImage.Asterisk);

                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }

        }


        /// <summary>
        /// Handles the Click event of the btnsend control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnsend_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid())
            {
                string orderno = txtOrderno.Text.ToUpperInvariant();

                if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    orderno = "DG-" + orderno;
                }
                if (GetEmailOrderStatus(orderno))
                {
                    SubmitMailDetails(orderno, txtEmail.Text, "", false, "hi", "");
                }
                else
                {
                    string messageheading = res_man.GetString("iMIX", cul);
                    string message = res_man.GetString("AlreadyOrderSubmitMsg", cul);
                    MessageBox.Show(message, messageheading, MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }


            }
        }

        /// <summary>
        /// Handles the Click event of the btnshutdown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnshutdown_Click(object sender, RoutedEventArgs e)
        {
            string messageheading = res_man.GetString("Confirmshutdown", cul);
            string message = res_man.GetString("ShutdownUnlock", cul);
            string value = MessageBox.Show(message, messageheading, MessageBoxButton.YesNoCancel, MessageBoxImage.Question).ToString();
            if (value == "Yes")
            {
                GrdPopup.Visibility = Visibility.Visible;
                txtUsername.Text = "";
                txtPassword.Password = "";
                Grdpopupvalue = "Shutdown";

            }
            else if (value == "No")
            {
                GrdPopup.Visibility = Visibility.Visible;
                txtPassword.Password = "";
                txtUsername.Text = "";
                Grdpopupvalue = "Unklock";
            }
        }
        /// <summary>
        /// Handles the Click event of the btnsubmitpopup control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void btnsubmitpopup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValidPopup())
                {
                    if (Grdpopupvalue == "Unklock")
                    {
                        if (GetUserDetails(txtUsername.Text, txtPassword.Password) == "7" || GetUserDetails(txtUsername.Text, txtPassword.Password) == "17")
                        {
                            GrdPopup.Visibility = Visibility.Collapsed;
                            Taskbar.Show();
                            AutoLock obj = new AutoLock();
                            obj.ReleaseKeyboardHook();
                            AutoLock.EnableDisableTaskManager(true);
                        }
                        else
                        {
                            string message = res_man.GetString("AuthorizedUnlock", cul);
                            // GrdPopup.Visibility = Visibility.Collapsed;
                            MessageBox.Show(message);
                        }
                    }
                    else if (Grdpopupvalue == "Shutdown")
                    {
                        if (GetUserDetails(txtUsername.Text, txtPassword.Password) != null)
                        {
                            Shutdown();
                        }
                        else
                        {
                            string message = res_man.GetString("AuthorizedUnlockMsg", cul);
                            // GrdPopup.Visibility = Visibility.Collapsed;
                            MessageBox.Show(message);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPopup.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region Common Methods
        void Shutdown()
        {
            ManagementBaseObject mboShutdown = null;
            ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();          // You can't shutdown without security privileges         
            mcWin32.Scope.Options.EnablePrivileges = true;
            ManagementBaseObject mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");
            // Flag 1 means we want to shut down the system. Use "2" to reboot.         
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown", mboShutdownParams, null);
            }


        }

        private bool GetEmailOrderStatus(string OrderId)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetEmailStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }
        private string GetUserDetails(string Username, string Password)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetUserDetailsForEmail", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", Username);
                cmd.Parameters.AddWithValue("@Password", Password);
                cn.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                cn.Close();
            }
        }


        private void ShowLoader()
        {
            loader.Visibility = Visibility.Visible;
            loader.StartStopLoader(true);
        }

        private void HideLoader()
        {

            loader.Visibility = Visibility.Collapsed;
            loader.StartStopLoader(false);
            lstImages.ItemsSource = MyImages;
            lstImages.Visibility = Visibility.Visible;
            btnsend.Visibility = Visibility.Visible;

        }
        private bool IsValid()
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(txtEmail.Text);
            if (txtOrderno.Text == "")
            {
                string message = res_man.GetString("OrderMsg", cul);
                MessageBox.Show(message);
                txtOrderno.Focus();
                return false;
            }
            else if (txtEmail.Text == "")
            {
                string message = res_man.GetString("EnterEmailMsg", cul);
                MessageBox.Show(message);
                txtEmail.Focus();
                return false;
            }
            else if (!match.Success)
            {
                string message = res_man.GetString("ValidEmailMsg", cul);
                MessageBox.Show(message);
                txtEmail.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsValidPopup()
        {

            if (txtUsername.Text == "")
            {
                string message = res_man.GetString("PleaseentertheusernameMsg", cul);
                MessageBox.Show(message);
                return false;
            }
            else if (txtPassword.Password == "")
            {
                string message = res_man.GetString("PleaseenterthepasswordMsg", cul);
                MessageBox.Show(message);
                return false;
            }

            else
            {
                return true;
            }
        }

        private void SubmitMailDetails(string OrderId, string EmailTo, string EmailBcc, bool EmailIsSent, string sendername, string message)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InsertEmailDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@Emailto", EmailTo);
                cmd.Parameters.AddWithValue("@EmailBcc", EmailBcc);
                cmd.Parameters.AddWithValue("@EmailIsSent", EmailIsSent);
                cmd.Parameters.AddWithValue("@Sendername", sendername);
                cmd.Parameters.AddWithValue("@EmailMessage", message);
                cmd.Parameters.AddWithValue("@DG_MediaName", "EM");
                cn.Open();
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {

                    string msg = res_man.GetString("EmailRequestMsg", cul);
                    string messageHeading = res_man.GetString("iMIX", cul);
                    MessageBox.Show(msg, messageHeading, MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    txtEmail.Text = "";
                    txtOrderno.Text = "";

                    lstImages.Visibility = Visibility.Collapsed;
                    btnsend.Visibility = Visibility.Collapsed;
                }

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);

            }
            finally
            {
                cn.Close();
            }
        }
        private string GetHotFolderPath()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetHotFolderPath", cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        public ObservableCollection<MyImageClass> MyImages { get; set; }
        private BitmapImage GetImageFromResourceString(string imageName)
        {

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 200;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(emailfolderpath + "\\" + imageName);
            image.EndInit();
            image.Freeze();
            return image;
        }
        private void DisplayImages(string FolderPath)
        {
            try
            {
                MyImages = new ObservableCollection<MyImageClass>();
                MyImages.Clear();
                string[] filePaths = Directory.GetFiles(FolderPath);
                foreach (var items in filePaths)
                {
                    string imgname = System.IO.Path.GetFileName(items);
                    if (imgname != "Thumbs.db")
                    {
                        MyImages.Add(new MyImageClass(GetImageFromResourceString(imgname)));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #endregion

        #region EventsForKeyBoard

        #endregion

        /// <summary>
        /// Handles the GotFocus event of the txtOrderno control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtOrderno_GotFocus(object sender, RoutedEventArgs e)
        {
            if (KeyBorder.Visibility == Visibility.Collapsed)
            {
                KeyBorder.Visibility = Visibility.Visible;
            }
            controlon = (TextBox)(sender);
        }
        /// <summary>
        /// Handles the Click event of the btn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {

            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;

                        break;
                    }
                case "SPACE":
                    {

                        controlon.Text = controlon.Text + " ";


                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);

                        }
                        break;
                    }
                default:
                    {

                        controlon.Text = controlon.Text + _objbtn.Content;

                    }
                    break;
            }
        }




    }

}
