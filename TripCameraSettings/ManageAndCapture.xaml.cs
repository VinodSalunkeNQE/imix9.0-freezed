﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using AVT;
using AVT.VmbAPINET;
using DigiPhoto.DataLayer;
using System.Xml;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Drawing.Imaging;

namespace TripCameraSettings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ManageAndCapture : Window
    {
        #region Delaration
        Vimba system = new Vimba();
        CameraCollection cameras;
        string[] camDetails;
        Camera cam;
        TripCamBusiness tripCamBusiness = new TripCamBusiness();
        TripCamFeaturesInfo tripCamFeatureInfo = new TripCamFeaturesInfo();
        int cameraid = 0;
        double _aspectRatio = 0;
        BusyWindow busyWin = new BusyWindow();
        string password = string.Empty;
        #endregion

        #region Constructors
        public ManageAndCapture()
        {
            InitializeComponent();
            GetStoreConfigData();
            AddStrobeSourceItems();
            GetCameras();
            btnAdvanced.IsEnabled = false;
            btnColorCorrection.IsEnabled = false;
            ucAdvancedCtrl.ExecuteParentMethod += new EventHandler(SetAdvanceProperties);
            ucAuthenticateCtrl.ExecuteParentMethod += new EventHandler(Authenticate);
            ucProtectedPropertiesCtrl.ExecuteParentMethod += new EventHandler(SetProtectedProperties);

            //To disable tab key
            this.PreviewKeyDown += MainWindowPreviewKeyDown;
        }
        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();
            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.TripCameraPropertyPassword:
                        password = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToString() : string.Empty;
                        break;
                }
            }

        }
        private void SetProtectedProperties(object sender, EventArgs e)
        {
            TripCamFeaturesInfo _tripFeature=(TripCamFeaturesInfo)sender;
            tripCamFeatureInfo.Gain00 = _tripFeature.Gain00;
            tripCamFeatureInfo.Gain01 = _tripFeature.Gain01;
            tripCamFeatureInfo.Gain02 = _tripFeature.Gain02;
            tripCamFeatureInfo.Gain10 = _tripFeature.Gain10;
            tripCamFeatureInfo.Gain11 = _tripFeature.Gain11;
            tripCamFeatureInfo.Gain12 = _tripFeature.Gain12;
            tripCamFeatureInfo.Gain20 = _tripFeature.Gain20;
            tripCamFeatureInfo.Gain21 = _tripFeature.Gain21;
            tripCamFeatureInfo.Gain22 = _tripFeature.Gain22;
            tripCamFeatureInfo.BlackLevel = _tripFeature.BlackLevel;
            tripCamFeatureInfo.Gain = _tripFeature.Gain;
            tripCamFeatureInfo.Gamma = _tripFeature.Gamma;
            tripCamFeatureInfo.Hue = _tripFeature.Hue;
            tripCamFeatureInfo.Saturation = _tripFeature.Saturation;
            tripCamFeatureInfo.WhiteBalanceBlue = _tripFeature.WhiteBalanceBlue;
            tripCamFeatureInfo.WhiteBalanceRed = _tripFeature.WhiteBalanceRed;
            tripCamFeatureInfo.ColorTransformationMode = _tripFeature.ColorTransformationMode;

        }

        

        #endregion

        #region Events
        private void cmbCameraNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ImgPreviewC1.Source = null;
                if (cmbCameraNames.SelectedIndex > 0)
                {
                    string CamName = cmbCameraNames.SelectedValue.ToString();
                    camDetails = CamName.Split('/');
                    if (ValidateCameraAvailability(camDetails[1]))
                    {
                        if (ValidateCameraRunningStatus(camDetails[1]))
                        {
                            TripCamInfo tripCamInfo = tripCamBusiness.GetTripCameraInfoById(camDetails[0], camDetails[1]);
                            cameraid = tripCamInfo.Camera_pKey;
                            List<TripCamSettingInfo> lstTripCamInfo = tripCamBusiness.GetTripCamSettingsForCameraId(tripCamInfo.Camera_pKey, tripCamInfo.TripCamTypeId);
                            tripCamFeatureInfo = FillTripCamFeaturesFromDb(lstTripCamInfo);
                            cam = cameras[cmbCameraNames.SelectedIndex - 1];
                            GetConfigModeValuesFromCamera();
                            GetValuesFromCamera();
                            grdCameraDetails.DataContext = tripCamFeatureInfo;
                            btnAdvanced.IsEnabled = true;
                            btnColorCorrection.IsEnabled = true;

                        }
                        else
                        {
                            MessageBox.Show("Camera is already running", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                            cmbCameraNames.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Camera is not available. Please add camera in Manage Camera Section.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        cmbCameraNames.SelectedIndex = 0;
                    }
                }
                else
                {
                    btnAdvanced.IsEnabled = false;
                    btnColorCorrection.IsEnabled = false;
                    tripCamFeatureInfo = new TripCamFeaturesInfo();
                    grdCameraDetails.DataContext = tripCamFeatureInfo;
                    //ClearControls();
                }
            }
            catch (System.Data.SqlClient.SqlException sqlex)
            {
                MessageBox.Show("Unable to connect to database. Please contact IT Administrator.");
                ErrorHandler.ErrorHandler.LogError(sqlex);
                cmbCameraNames.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            system.Shutdown();
            Application.Current.Shutdown();
        }

        private void btnAdvanced_Click(object sender, RoutedEventArgs e)
        {
            ucAdvancedCtrl._Parent = this;
            grdParent.IsEnabled = false;
            //tripCamFeatureInfo.MaxStreamByteValue = 100;
            ucAdvancedCtrl.ShowHandlerDailog(tripCamFeatureInfo);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbCameraNames.SelectedIndex > 0)
                {
                    if (String.IsNullOrWhiteSpace(txtCameraName.Text))
                    {
                        MessageBox.Show("Please Enter Name of the Camera", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (rdbDHCP.IsChecked == false && !IsValidIP(tripCamFeatureInfo.Gateway))
                    {
                        MessageBox.Show("Please enter valid Gateway value", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (rdbDHCP.IsChecked == false && !IsValidIP(tripCamFeatureInfo.IPAddress))
                    {
                        MessageBox.Show("Please enter valid IPAddress", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (rdbDHCP.IsChecked == false && !IsValidIP(tripCamFeatureInfo.Subnet))
                    {
                        MessageBox.Show("Please enter valid Subnet value", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    busyWin.Show();
                    PushConfigModeSettingsToCamera();
                    bool res = SaveSettingsInDb();
                    if (res)
                    {
                        CreateSettingsXMLFile();
                        GetConfigModeValuesFromCamera();
                        System.Threading.Thread.Sleep(5000);
                    }
                }
                else
                {
                    MessageBox.Show("Please select camera.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void rdbDHCP_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbDHCP.IsChecked == true)
            {
                tripCamFeatureInfo.IPConfigurationMode = rdbDHCP.Content.ToString();
                //cam.Close();
                //GetConfigModeValuesFromCamera();
                if (txtGateway != null)
                {
                    txtGateway.IsEnabled = false;
                    txtGateway.Text = "";
                }
                if (txtIPAddress != null)
                {
                    txtIPAddress.IsEnabled = false;
                    txtIPAddress.Text = "";
                }
                if (txtSubnet != null)
                {
                    txtSubnet.IsEnabled = false;
                    txtSubnet.Text = "";
                }
            }
        }

        private void rdbManual_Checked(object sender, RoutedEventArgs e)
        {
            if (rdbManual.IsChecked == true)
            {
                tripCamFeatureInfo.IPConfigurationMode = rdbManual.Content.ToString();
                if (txtGateway != null)
                    txtGateway.IsEnabled = true;
                if (txtIPAddress != null)
                    txtIPAddress.IsEnabled = true;
                if (txtSubnet != null)
                    txtSubnet.IsEnabled = true;
            }
        }

        private void rdb_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rbn = (RadioButton)sender;
            tripCamFeatureInfo.CameraRotation = Convert.ToInt32(rbn.Content.ToString());
        }

        #region Textbox GotFocus Event
      
        bool result = false;
        private void txtHeight_GotFocus(object sender, RoutedEventArgs e)
        {
            
            double _aspectRatio = GetAspectRatio();
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.HeightMax - tripCamFeatureInfo.OffsetY,
                MinValue = 2,
                PropertyName = "Height",
            };
               //AuthenticateUserControl();
                SetSliderProperties(sender, obj);
        }

        private void AuthenticateUserControl()
        {
           ucAuthenticateCtrl.SetParent(this.grdCameraDetails);
           ucAuthenticateCtrl.ShowHandlerDialog(password);
        }
        private void Authenticate(object sender, EventArgs e)
        {
            result = (bool)sender;
            if (result)
            {
                ucProtectedPropertiesCtrl.SetParent(this.grdCameraDetails);
                ucProtectedPropertiesCtrl.ShowHandlerDialog(tripCamFeatureInfo);
                //ucProtectedPropertiesCtrl.Visibility = Visibility.Visible;
                //grdCameraDetails.IsEnabled = true;

                //grdCameraDetails.IsEnabled = true;
                //SetSliderProperties(_sender, obj);
            }
            else
                grdCameraDetails.IsEnabled = true;
        }


        private void txtOffsetX_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.WidthMax - tripCamFeatureInfo.Width,
                MinValue = 0,
                PropertyName = "OffsetX"
            };
            SetSliderProperties(sender, obj);
        }

        private void txtOffsetY_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.HeightMax - tripCamFeatureInfo.Height,
                MinValue = 0,
                PropertyName = "OffsetY"
            };
            SetSliderProperties(sender, obj);
        }

        private void txtWidth_GotFocus(object sender, RoutedEventArgs e)
        {
            GetAspectRatio();
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.WidthMax - tripCamFeatureInfo.OffsetX,
                MinValue = 2,
                PropertyName = "Width",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtHue_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 40,
                MinValue = -40,
                PropertyName = "Hue"
            };
            SetSliderProperties(sender, obj);
        }

        private void txtSaturation_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = 0,
                PropertyName = "Saturation",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }

        private void txtWBRed_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 3.0,
                MinValue = 0.8,
                PropertyName = "White Balance Red",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }

        private void txtWBBlue_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 3.0,
                MinValue = 0.8,
                PropertyName = "White Balance Blue",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }

        private void txtNoOfImages_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 5,
                MinValue = 1,
                PropertyName = "No of Images",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtTriggerDelay_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 13421772,
                MinValue = 0,
                PropertyName = "Trigger Delay",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtShutterSpeed_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 26843545,
                MinValue = 10,
                PropertyName = "Exposure Time",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtStrobeDelay_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 150,//13421772,
                MinValue = 0,
                PropertyName = "Strobe Delay",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtStrobeDuration_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 13421772,
                MinValue = 0,
                PropertyName = "Strobe Duration",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtPacketSize_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 9973,
                MinValue = 500,
                PropertyName = "Packet Size",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtLensFocus_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.LensFocusMax,
                MinValue = 0,
                PropertyName = "Lens Focus",
            };
            SetSliderProperties(sender, obj);
        }

        private void txtAperture_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = tripCamFeatureInfo.ApertureMax,
                MinValue = tripCamFeatureInfo.ApertureMin,
                PropertyName = "Aperture",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        #endregion
        private void SetSliderProperties(object sender, SliderInfo obj)
        {
            try
            {
                TextBox control = (TextBox)sender;
                if (String.IsNullOrWhiteSpace(control.Text))
                    control.Text = "0";
                ucSliderCtrl.sliderMinValue = obj.MinValue;
                ucSliderCtrl.sliderMaxValue = obj.MaxValue;
                ucSliderCtrl.sliderValue = Convert.ToDouble(control.Text);
                ucSliderCtrl.controlName = control.Name;
                ucSliderCtrl.TickFrequency = obj.tickFrequency == 0 ? 1 : obj.tickFrequency;
                ucSliderCtrl.PropertyName = obj.PropertyName;
                ucSliderCtrl.SetParent(this.grdCameraDetails);
                double _currentvalue = Convert.ToDouble(ucSliderCtrl.ShowHandlerDialog());

                if (obj.IsFloat)
                    control.Text = _currentvalue.ToString();
                else
                    control.Text = Convert.ToString(Convert.ToInt64(_currentvalue));
                if (control.Name == "txtHeight" && chkAspectRatio.IsChecked == true && _aspectRatio > 0)
                {
                    var Value = Convert.ToInt32(Convert.ToDouble(txtHeight.Text) / _aspectRatio).ToString();
                    txtWidth.Text = Value;
                }
                else if (control.Name == "txtWidth" && chkAspectRatio.IsChecked == true && _aspectRatio > 0)
                {
                    txtHeight.Text = Convert.ToInt32(Convert.ToDouble(txtWidth.Text) * _aspectRatio).ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnPreview_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbCameraNames.SelectedIndex > 0)
                {
                    try
                    {
                        cam = system.OpenCameraByID(cam.Id, VmbAccessModeType.VmbAccessModeFull);
                        if (null == cam)
                        {
                            MessageBox.Show("No camera retrieved.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    catch (VimbaException ex)
                    {
                        MessageBox.Show("Camera is already running. Please check the Capture Utility.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    finally
                    {
                        cam.Close();
                    }
                    AVT.VmbAPINET.Frame frame = null;
                    string fileName = cam.Id + (".xml");
                    string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    fileName = System.IO.Path.Combine(path, fileName);

                    if (System.IO.File.Exists(fileName))
                    {
                        busyWin.Show();

                        PushSettings();

                        cam = system.OpenCameraByID(cam.Id, VmbAccessModeType.VmbAccessModeFull);

                        if (null == cam)
                        {
                            MessageBox.Show("Unable to open camera.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                        try
                        {
                            cam.Features["TriggerSource"].EnumValue = "Freerun";
                            cam.AcquireSingleImage(ref frame, 5000);
                            cam.Features["TriggerSource"].EnumValue = tripCamFeatureInfo.TriggerSource;
                        }
                        catch (VimbaException ve)
                        {
                            ErrorHandler.ErrorHandler.LogError(ve);
                        }
                        finally
                        {
                            cam.Close();
                        }
                        PreviewFrame(frame);
                        busyWin.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Please save the settings for the select camera.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Please select camera.", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                busyWin.Hide();
            }
        }
        private void btnColorCorrection_Click(object sender, RoutedEventArgs e)
        {
            AuthenticateUserControl();
        }
        static void MainWindowPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Escape)
            {
                e.Handled = true;
            }
        }
        #endregion

        #region Common Functions
        private void AddStrobeSourceItems()
        {
            cmbStrobeSource.Items.Add("AquisitionTriggerReady");
            cmbStrobeSource.Items.Add("FrameTriggerReady");
            cmbStrobeSource.Items.Add("FrameTrigger");
            cmbStrobeSource.Items.Add("Exposing");
            cmbStrobeSource.Items.Add("FrameReadout");
            cmbStrobeSource.Items.Add("Acquiring");
            cmbStrobeSource.Items.Add("LineIn1");
            cmbStrobeSource.Items.Add("LineIn2");
            cmbStrobeSource.Items.Add("LineIn3");
            cmbStrobeSource.Items.Add("LineIn4");
            cmbStrobeSource.SelectedValue = "FrameTrigger";
           
        }
        private double GetAspectRatio()
        {
            double MaxHeight = tripCamFeatureInfo.HeightMax;
            double MaxWidth = tripCamFeatureInfo.WidthMax;
            if (chkAspectRatio.IsChecked == true)
            {
                _aspectRatio = MaxHeight / MaxWidth;
            }
            return _aspectRatio;
        }

        private void SetAdvanceProperties(object sender, EventArgs e)
        {
            string[] advSettings = (string[])sender;
            tripCamFeatureInfo.PixelFormat = advSettings[0];
            tripCamFeatureInfo.TriggerSource = advSettings[1];
            tripCamFeatureInfo.StreamBytesPerSec = Convert.ToInt32(advSettings[2]);
            grdParent.IsEnabled = true;
        }

        private void GetCameras()
        {
            try
            {
                cmbCameraNames.Items.Clear();
                cmbCameraNames.Items.Add("--Select Camera--");
                system.Startup();
                cameras = system.Cameras;
                int count = 0;
                while (count < cameras.Count && cameras.Count > 0)
                {
                    cmbCameraNames.Items.Add(Convert.ToString(cameras[count].Name + "/" + cameras[count].Id + "/" + cameras[count].SerialNumber));
                    count++;
                }
                //cmbCameraNames.Items.Add("GT3300C/SR-Test/Sr215673812");
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message, "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public bool ValidateCameraRunningStatus(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraRunningStatus(CamMake);
            return result;
        }

        public bool ValidateCameraAvailability(string CamMake)
        {
            bool result = false;
            result = tripCamBusiness.ValidateCameraAvailability(CamMake);
            return result;
        }

        public TripCamFeaturesInfo FillTripCamFeaturesFromDb(List<TripCamSettingInfo> lstTripCamInfo)
        {
            try
            {
                tripCamFeatureInfo = new TripCamFeaturesInfo();
                foreach (TripCamSettingInfo tripFeature in lstTripCamInfo)
                {
                    switch (tripFeature.TripCamSettingsMasterId)
                    {
                        case (int)TripCamFeatures.Height:
                            tripCamFeatureInfo.Height = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.HeightMax:
                            tripCamFeatureInfo.HeightMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetX:
                            tripCamFeatureInfo.OffsetX = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.OffsetY:
                            tripCamFeatureInfo.OffsetY = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Width:
                            tripCamFeatureInfo.Width = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WidthMax:
                            tripCamFeatureInfo.WidthMax = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Hue:
                            tripCamFeatureInfo.Hue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Saturation:
                            tripCamFeatureInfo.Saturation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceBlue:
                            tripCamFeatureInfo.WhiteBalanceBlue = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.WhiteBalanceRed:
                            tripCamFeatureInfo.WhiteBalanceRed = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.NoOfImages:
                            tripCamFeatureInfo.NoOfImages = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerDelay:
                            tripCamFeatureInfo.TriggerDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ExposureTime:
                            tripCamFeatureInfo.ExposureTime = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraRotation:
                            tripCamFeatureInfo.CameraRotation = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            switch (tripCamFeatureInfo.CameraRotation)
                            {
                                case 90:
                                    rdbNinety.IsChecked = true;
                                    break;
                                case 180:
                                    rdbOneEighty.IsChecked = true;
                                    break;
                                case 270:
                                    rdbTwoSeventy.IsChecked = true;
                                    break;
                                default:
                                    rdbZero.IsChecked = true;
                                    break;
                            }
                            break;
                        case (int)TripCamFeatures.StrobeDelay:
                            tripCamFeatureInfo.StrobeDelay = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDuration:
                            tripCamFeatureInfo.StrobeDuration = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeSource:
                            tripCamFeatureInfo.StrobeSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "FrameTrigger" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StrobeDurationMode:
                            tripCamFeatureInfo.StrobeDurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Controlled" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PacketSize:
                            tripCamFeatureInfo.PacketSize = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 1500 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.MACAddress:
                            tripCamFeatureInfo.MACAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPConfigurationMode:
                            tripCamFeatureInfo.IPConfigurationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Persistent" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.IPAddress:
                            tripCamFeatureInfo.IPAddress = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Subnet:
                            tripCamFeatureInfo.Subnet = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gateway:
                            tripCamFeatureInfo.Gateway = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "0.0.0.0" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.PixelFormat:
                            tripCamFeatureInfo.PixelFormat = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "BayerGR12" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.TriggerSource:
                            tripCamFeatureInfo.TriggerSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Line2" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.LensFocus:
                            tripCamFeatureInfo.LensFocus = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Aperture:
                            tripCamFeatureInfo.Aperture = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.CameraName:
                            tripCamFeatureInfo.CameraName = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.StreamBytesPerSec:
                            tripCamFeatureInfo.StreamBytesPerSec = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : Convert.ToInt32(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.BlackLevel:
                            tripCamFeatureInfo.BlackLevel = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain:
                            tripCamFeatureInfo.Gain = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gamma:
                            tripCamFeatureInfo.Gamma = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain00:
                            tripCamFeatureInfo.Gain00 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain01:
                            tripCamFeatureInfo.Gain01 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain02:
                            tripCamFeatureInfo.Gain02 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain10:
                            tripCamFeatureInfo.Gain10 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain11:
                            tripCamFeatureInfo.Gain11 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain12:
                            tripCamFeatureInfo.Gain12 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain20:
                            tripCamFeatureInfo.Gain20 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain21:
                            tripCamFeatureInfo.Gain21 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.Gain22:
                            tripCamFeatureInfo.Gain22 = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? 0 : (float)Convert.ToDouble(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.ColorTransformationMode:
                            tripCamFeatureInfo.ColorTransformationMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Manual" : Convert.ToString(tripFeature.SettingsValue);
                            break;

                        case (int)TripCamFeatures.ExposureMode:
                            tripCamFeatureInfo.ExposureMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Timed" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.AcquisitionMode:
                            tripCamFeatureInfo.AcquisitionMode = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Continuous" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.SyncOutPolarity:
                            tripCamFeatureInfo.SyncOutPolarity = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Invert" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.SyncOutSelector:
                            tripCamFeatureInfo.SyncOutSelector = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "SyncOut3" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                        case (int)TripCamFeatures.SyncOutSource:
                            tripCamFeatureInfo.SyncOutSource = String.IsNullOrWhiteSpace(tripFeature.SettingsValue) ? "Strobe1" : Convert.ToString(tripFeature.SettingsValue);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return tripCamFeatureInfo;
        }

        private void PushConfigModeSettingsToCamera()
        {
            try
            {
                bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeConfig);
                if (IsCameraopened)
                {
                    cam.Features["GevIPConfigurationMode"].EnumValue = tripCamFeatureInfo.IPConfigurationMode;

                    if (tripCamFeatureInfo.IPConfigurationMode.ToLower() == "persistent")
                    {
                        cam.Features["GevPersistentDefaultGateway"].IntValue = IP2Long(tripCamFeatureInfo.Gateway);

                        cam.Features["GevPersistentIPAddress"].IntValue = IP2Long(tripCamFeatureInfo.IPAddress);

                        cam.Features["GevPersistentSubnetMask"].IntValue = IP2Long(tripCamFeatureInfo.Subnet);
                    }

                    cam.Features["GevIPConfigurationApply"].RunCommand();
                }
                else
                {
                    MessageBox.Show("Camera cannot be opened. Please check the camera", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
            }
            finally
            {
                cam.Close();
            }
        }

        private bool OpenCamera(VmbAccessModeType OpenMode)
        {
            bool IsCamOpened = false;
            try
            {
                if (null == cam.Id)
                {
                    throw new ArgumentNullException("camera id");
                }

                //Open camera
                cam = system.OpenCameraByID(cam.Id, OpenMode);
                if (null == cam)
                {
                    throw new NullReferenceException("No camera retrieved.");
                }
                else
                {
                    IsCamOpened = true;
                }
            }
            catch (VimbaException ve)
            {
                MessageBox.Show(ve.Message, "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            return IsCamOpened;
        }

        private void GetConfigModeValuesFromCamera()
        {
            try
            {
                Feature pfeature;
                byte[] bytes;
                cam.Open(VmbAccessModeType.VmbAccessModeConfig);

                pfeature = cam.Features["GevIPConfigurationMode"];
                tripCamFeatureInfo.IPConfigurationMode = pfeature.StringValue;
                if (tripCamFeatureInfo.IPConfigurationMode.ToLower() == "dhcp")
                {
                    rdbDHCP.IsChecked = true;
                }
                else
                {
                    rdbManual.IsChecked = true;
                }

                pfeature = cam.Features["GevCurrentDefaultGateway"];
                bytes = (IPAddress.Parse(pfeature.IntValue.ToString())).GetAddressBytes();
                Array.Reverse(bytes);
                tripCamFeatureInfo.Gateway = new IPAddress(bytes).ToString();

                pfeature = cam.Features["GevCurrentIPAddress"];
                bytes = (IPAddress.Parse(pfeature.IntValue.ToString())).GetAddressBytes();
                Array.Reverse(bytes);
                tripCamFeatureInfo.IPAddress = new IPAddress(bytes).ToString();

                pfeature = cam.Features["GevCurrentSubnetMask"];
                bytes = (IPAddress.Parse(pfeature.IntValue.ToString())).GetAddressBytes();
                Array.Reverse(bytes);
                tripCamFeatureInfo.Subnet = new IPAddress(bytes).ToString();

            }
            catch (VimbaException vex)
            {
                ErrorHandler.ErrorHandler.LogError(vex);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                cam.Close();
            }
        }

        private void GetValuesFromCamera()
        {
            try
            {
                cam.Open(VmbAccessModeType.VmbAccessModeRead);
                FeatureCollection features = cam.Features;

                if (features.ContainsName("DeviceTemperature"))
                    tripCamFeatureInfo.DeviceTemperature = cam.Features["DeviceTemperature"].FloatValue;

                if (features.ContainsName("GevDeviceMACAddress"))
                    tripCamFeatureInfo.MACAddress = BitConverter.ToString((BitConverter.GetBytes(cam.Features["GevDeviceMACAddress"].IntValue)));

                if (features.ContainsName("StatFrameDelivered"))
                    tripCamFeatureInfo.ImagesCaptured = cam.Features["StatFrameDelivered"].IntValue;

                if (features.ContainsName("StatFrameDropped"))
                    tripCamFeatureInfo.ImagesDropped = cam.Features["StatFrameDropped"].IntValue;


                if (features.ContainsName("EFLensFocusCurrent"))
                    tripCamFeatureInfo.LensFocusMax = Convert.ToInt32(cam.Features["EFLensFocusMax"].IntValue);

                if (features.ContainsName("EFLensFStopCurrent"))
                {
                    tripCamFeatureInfo.ApertureMin = cam.Features["EFLensFStopMin"].FloatValue;
                    tripCamFeatureInfo.ApertureMax = cam.Features["EFLensFStopMax"].FloatValue;
                }

                //if (features.ContainsName("StreamBytesPerSecond"))
                //{
                //    tripCamFeatureInfo.MaxStreamByteValue = cam.Features["StreamBytesPerSecond"].FloatValue;
                //}
            }
            catch (VimbaException vex)
            {
                ErrorHandler.ErrorHandler.LogError(vex);
            }
            finally
            {
                cam.Close();
            }
        }

        private bool SaveSettingsInDb()
        {
            bool result = false;
            try
            {
                GetDefaultComboValue();
                result = tripCamBusiness.InsUpdTripCamSettings(tripCamFeatureInfo, cameraid);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return result;
        }

        private void GetDefaultComboValue()
        {
            tripCamFeatureInfo.StrobeSource = cmbStrobeSource.SelectedValue.ToString();
            tripCamFeatureInfo.StrobeDurationMode = cmbStrobeDurationMode.Text;
            tripCamFeatureInfo.SyncOutPolarity = cmbSyncOutPolarity.Text;
            tripCamFeatureInfo.SyncOutSelector=cmbSyncOutSelector.Text;
            tripCamFeatureInfo.SyncOutSource = cmbSyncOutSource.Text;
            tripCamFeatureInfo.ExposureMode=cmbExposureMode.Text;
                
        }

        private void CreateSettingsXMLFile()
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string xmlFilePath = System.IO.Path.Combine(path, (camDetails[0] + ".xml"));
                string CameraXMlFileName = System.IO.Path.Combine(path, (camDetails[1] + ".xml"));
                if (System.IO.File.Exists(xmlFilePath) && !System.IO.File.Exists(CameraXMlFileName))
                    System.IO.File.Copy(xmlFilePath, CameraXMlFileName);
                xmlDocument.Load((camDetails[1] + ".xml"));
                var element = xmlDocument.GetElementsByTagName("Settings");
                XmlNodeList nd = xmlDocument.GetElementsByTagName("Settings");
                if (nd != null)
                {
                    if (nd.Count > 0)
                        nd[0].Attributes["ID"].Value = camDetails[1];
                }


                XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("Settings");
                XmlNode settingsNode = xmlNodeList[0];

                foreach (XmlNode xmlNode in settingsNode.ChildNodes)
                {
                    string type = xmlNode.Name;
                    XmlAttribute nameAttribute = xmlNode.Attributes["Name"];
                    if (null == nameAttribute)
                    {
                        throw new Exception("Invalid camera settings xml file.");
                    }
                    string name = nameAttribute.Value;

                    switch (name)
                    {
                        case "Height": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Height); break; }

                        case "OffsetX": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.OffsetX); break; }

                        case "OffsetY": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.OffsetY); break; }

                        case "Width": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Width); break; }

                        case "Hue": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Hue); break; }

                        case "Saturation": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Saturation); break; }

                        case "BalanceWhiteAuto": { xmlNode.InnerText = "Off"; break; }

                        case "BalanceRatioAbs":
                            {
                                XmlAttribute enumAttribute = xmlNode.Attributes["Enumeration"];
                                string enume = enumAttribute.Value;
                                switch (enume)
                                {
                                    case "Blue": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.WhiteBalanceBlue); break; }

                                    case "Red": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.WhiteBalanceRed); break; }

                                }
                                break;
                            }

                        case "TriggerMode": { xmlNode.InnerText = "On"; break; }

                        case "TriggerDelayAbs": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.TriggerDelay); break; }

                        case "ExposureAuto": { xmlNode.InnerText = "Off"; break; }

                        case "ExposureTimeAbs": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.ExposureTime); break; }

                        case "StrobeDelay": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.StrobeDelay); break; }

                        case "StrobeDuration": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.StrobeDuration); break; }

                        case "StrobeSource": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.StrobeSource); break; }

                        case "GevSCPSPacketSize": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.PacketSize); break; }

                        case "TriggerSource": { xmlNode.InnerText = tripCamFeatureInfo.TriggerSource.ToString(); break; }

                        case "PixelFormat": { xmlNode.InnerText = tripCamFeatureInfo.PixelFormat.ToString(); break; }

                        case "EFLensFocusCurrent": { xmlNode.InnerText = tripCamFeatureInfo.LensFocus.ToString(); break; }

                        case "EFLensFStopCurrent": { xmlNode.InnerText = tripCamFeatureInfo.Aperture.ToString(); break; }

                        case "StreamBytesPerSecond": { xmlNode.InnerText = tripCamFeatureInfo.StreamBytesPerSec.ToString(); break; }

                        case "Gamma": { xmlNode.InnerText = tripCamFeatureInfo.Gamma.ToString(); break; }

                        case "Gain": { xmlNode.InnerText = tripCamFeatureInfo.Gain.ToString(); break; }

                        case "BlackLevel": { xmlNode.InnerText = tripCamFeatureInfo.BlackLevel.ToString(); break; }

                        case "ColorTransformationMode": { xmlNode.InnerText = tripCamFeatureInfo.ColorTransformationMode.ToString(); break; }

                        //case "ImageSize": { xmlNode.InnerText = txtImageSize.Text; break; }

                        case "ColorTransformationValue":
                            {
                                XmlAttribute enumAttribute = xmlNode.Attributes["Enumeration"];
                                string enume = enumAttribute.Value;
                                switch (enume)
                                {
                                    case "Gain00": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain00); break; }

                                    case "Gain01": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain01); break; }

                                    case "Gain02": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain02); break; }

                                    case "Gain10": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain10); break; }

                                    case "Gain11": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain11); break; }

                                    case "Gain12": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain12); break; }

                                    case "Gain20": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain20); break; }

                                    case "Gain21": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain21); break; }

                                    case "Gain22": { xmlNode.InnerText = Convert.ToString(tripCamFeatureInfo.Gain22); break; }
                                }
                                break;
                            }

                        case "AcquisitionMode": { xmlNode.InnerText = tripCamFeatureInfo.AcquisitionMode.ToString(); break; }
                        case "StrobeDurationMode": { xmlNode.InnerText = tripCamFeatureInfo.StrobeDurationMode.ToString(); break; }
                        case "SyncOutPolarity": { xmlNode.InnerText = tripCamFeatureInfo.SyncOutPolarity.ToString(); break; }
                        case "SyncOutSelector": { xmlNode.InnerText = tripCamFeatureInfo.SyncOutSelector.ToString(); break; }
                        case "SyncOutSource": { xmlNode.InnerText = tripCamFeatureInfo.SyncOutSource.ToString(); break; }
                       //case "ColorTransformationMode": { xmlNode.InnerText = cmbColorTransformationMode.SelectedValue.ToString(); break; }
                    }
                }
                xmlDocument.Save((camDetails[1] + ".xml"));

                System.Threading.Thread.Sleep(10000);
                busyWin.Hide();
                MessageBox.Show("Camera control features has been saved successfully", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show("Record not Saved due to some error", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private long IP2Long(string IP)
        {
            string[] IpBytes;
            double num = 0;
            if (!string.IsNullOrEmpty(IP))
            {
                IpBytes = IP.Split('.');
                Array.Reverse(IpBytes);
                for (int i = IpBytes.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(IpBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                }
            }
            return (long)num;
        }

        private bool IsValidIP(string IPAddr)
        {
            bool valid = false;
            string pattern = @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

            Regex check = new Regex(pattern);

            if (!string.IsNullOrWhiteSpace(IPAddr))
            {
                valid = check.IsMatch(IPAddr, 0);
            }
            return valid;
        }

        private void PreviewFrame(AVT.VmbAPINET.Frame frame)
        {
            try
            {
                if (null == frame)
                {
                    ErrorHandler.ErrorHandler.LogError(new ArgumentNullException("frame"));
                }
                if (VmbFrameStatusType.VmbFrameStatusComplete == frame.ReceiveStatus)
                {
                    Bitmap bitmap = null;
                    bitmap = new Bitmap((int)frame.Width, (int)frame.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    frame.Fill(ref bitmap);
                    System.Drawing.Image img = bitmap;
                    if (ImgPreviewC1.Source != null)
                        ImgPreviewC1.Source = null;
                    ImgPreviewC1.Source = ToBitmapImage(img, tripCamFeatureInfo.CameraRotation);
                    //img.Save(@"C:\Users\Public\Pictures\Sample Pictures\Preview.jpg");
                }
                else
                {
                    MessageBox.Show("Invalid frame received. Please check the value of StreamBytesPerSecond", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                    ErrorHandler.ErrorHandler.LogFileWrite(DateTime.Now.ToString() + ": Frame Receive Status : " + frame.ReceiveStatus.ToString());
                }
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void PushSettings()
        {
            try
            {
                VmbAccessModeType accessMode = cam.PermittedAccess;
                string fileName = cam.Id + (".xml");
                if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                {
                    //Now get the camera ID
                    string cameraID = cam.Id;

                    //Try to open the camera

                    bool IsCameraopened = OpenCamera(VmbAccessModeType.VmbAccessModeFull);

                    if (IsCameraopened)
                    {
                        //Load settings from file
                        StringCollection loadedFeatures = null;
                        StringCollection missingFeatures = null;
                        bool ignoreStreamable = false;
                        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        fileName = System.IO.Path.Combine(path, fileName);
                        Helper.LoadFromFile(cam, fileName, out loadedFeatures, out missingFeatures, ignoreStreamable);
                        //MessageBox.Show("Settings successfully loaded from file.");
                    }
                    else
                    {
                        MessageBox.Show("Camera cannot be opened. Please check the connection", "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (VimbaException ve)
            {
                ErrorHandler.ErrorHandler.LogError(ve);
                MessageBox.Show(ve.Message, "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                MessageBox.Show(ex.Message, "DEI", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                cam.Close();
            }
        }

        public static BitmapImage ToBitmapImage(System.Drawing.Image image, int rotationAngle)
        {
            var bitmapImage = new BitmapImage(); ;
            try
            {
                using (var memory = new MemoryStream())
                {
                    image.Save(memory, ImageFormat.Bmp);
                    memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    switch (rotationAngle)
                    {
                        case 90:
                            bitmapImage.Rotation = Rotation.Rotate90;
                            break;
                        case 180:
                            bitmapImage.Rotation = Rotation.Rotate180;
                            break;
                        case 270:
                            bitmapImage.Rotation = Rotation.Rotate270;
                            break;
                    }
                    bitmapImage.EndInit();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return bitmapImage;
        }

        private void ClearControls()
        {
            chkAspectRatio.IsChecked = false;
            txtHeight.Text = "";
            txtOffsetX.Text = "";
            txtOffsetY.Text = "";
            txtWidth.Text = "";
            txtHue.Text = "";
            txtSaturation.Text = "";
            txtWBRed.Text = "";
            txtWBBlue.Text = "";
            txtCameraName.Text = "";
            txtNoOfImages.Text = "";
            txtTriggerDelay.Text = "";
            txtShutterSpeed.Text = "";
            rdbZero.IsChecked = true;
            txtCamTemperature.Text = "";
            txtStrobeDelay.Text = "";
            txtStrobeDuration.Text = "";
            txtPacketSize.Text = "";
            txtMacAddress.Text = "";
            txtIPAddress.Text = "";
            txtGateway.Text = "";
            rdbManual.IsChecked = true;
            txtLensFocus.Text = "";
            txtAperture.Text = "";
        }
        #endregion
        
    }
}
