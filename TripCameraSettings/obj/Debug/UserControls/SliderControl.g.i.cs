﻿#pragma checksum "..\..\..\UserControls\SliderControl.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "5A5C9100049D55367BA47BDCC46D9CE5E92F123418DEF44A0FDA790F08B9870F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace TripCameraSettings {
    
    
    /// <summary>
    /// SliderControl
    /// </summary>
    public partial class SliderControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\UserControls\SliderControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdSliderCtrl;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\UserControls\SliderControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\UserControls\SliderControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblPropertyName;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\UserControls\SliderControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider slider;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\UserControls\SliderControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtValue;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TripCameraSettings;component/usercontrols/slidercontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\SliderControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\UserControls\SliderControl.xaml"
            ((TripCameraSettings.SliderControl)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.UserControl_LostFocus);
            
            #line default
            #line hidden
            
            #line 7 "..\..\..\UserControls\SliderControl.xaml"
            ((TripCameraSettings.SliderControl)(target)).GotFocus += new System.Windows.RoutedEventHandler(this.UserControl_GotFocus);
            
            #line default
            #line hidden
            return;
            case 2:
            this.grdSliderCtrl = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\UserControls\SliderControl.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lblPropertyName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.slider = ((System.Windows.Controls.Slider)(target));
            
            #line 25 "..\..\..\UserControls\SliderControl.xaml"
            this.slider.KeyUp += new System.Windows.Input.KeyEventHandler(this.txtValue_KeyUp);
            
            #line default
            #line hidden
            return;
            case 6:
            this.txtValue = ((System.Windows.Controls.TextBox)(target));
            
            #line 26 "..\..\..\UserControls\SliderControl.xaml"
            this.txtValue.KeyUp += new System.Windows.Input.KeyEventHandler(this.txtValue_KeyUp);
            
            #line default
            #line hidden
            
            #line 26 "..\..\..\UserControls\SliderControl.xaml"
            this.txtValue.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtnumber_TextChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

