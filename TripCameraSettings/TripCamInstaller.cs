﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;

namespace TripCameraSettings
{
    [RunInstaller(true)]
    public partial class TripCamInstaller : System.Configuration.Install.Installer
    {
        public TripCamInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {

            base.Install(stateSaver);
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();


            string targetDirectory = Context.Parameters["targetdir"];

            string servername = Context.Parameters["Servername"];

            string username = Context.Parameters["Username"];

            string password = Context.Parameters["Password"];
            string hotfolder = Context.Parameters["HotFolder"];

            string database = "Digiphoto";

            //System.Diagnostics.Debugger.Break();
            if (string.IsNullOrEmpty(targetDirectory) || string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hotfolder))
            {
                return;
            }
            string exePath = string.Format("{0}TripCameraSettings.exe", targetDirectory);

            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);

            string connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
           
            ConnectionStringSettings connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            String conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True";

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);

            ConfigurationSection section = config.GetSection("connectionStrings");

            //Ensures that the section is not already protected
            if (!section.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");

            //For RideCaptureUtility
            exePath = string.Format("{0}TripCaptureUtility.exe", targetDirectory);

            config = ConfigurationManager.OpenExeConfiguration(exePath);

            connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;

            connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);

            ConfigurationSection section2 = config.GetSection("connectionStrings");
            //Ensures that the section is not already protected
            if (!section2.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                section2.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");
        }
    }
}
