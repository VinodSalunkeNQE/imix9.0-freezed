﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TripCameraSettings
{
    /// <summary>
    /// Interaction logic for AuthenticateControl.xaml
    /// </summary>
    public partial class AuthenticateControl : UserControl
    {
        public AuthenticateControl()
        {
            InitializeComponent();
        }
        #region Declaration
        string password = string.Empty;
        UIElement _parent;
        private bool isUserAuthorised = false;
        public event EventHandler ExecuteParentMethod;
        #endregion

        #region Methods
        private void btnAuthenticateUser_Click(object sender, RoutedEventArgs e)
        {
            string validationMessage = Validations();

            if (validationMessage != null)
                txtErrorMessage.Text = validationMessage;
            else
            {
                isUserAuthorised = true;
                HideHandlerDialog();
            }

        }
        private string Validations()
        {
            if (txtPassword.Password == string.Empty)
                return "Please enter password *";
            if (password == string.Empty)
                return "Password is not set *";
            if (txtPassword.Password != password)
                return "Password is incorrect *";
            
            return null;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                btnAuthenticateUser_Click(null, null);
            }
        }
        #endregion

        #region HandlerDialouge
        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        public void ShowHandlerDialog(string _password)
        {
            password = _password;
            Visibility = Visibility.Visible;
            isUserAuthorised = false;
            txtPassword.Focus();
            _parent.IsEnabled = false;
        }
        private void HideHandlerDialog()
        {
            txtErrorMessage.Text = string.Empty;
            txtPassword.Clear();
            Visibility = Visibility.Collapsed;
            OnExecuteMethod();
        }

        public virtual void OnExecuteMethod()
        {
            if (ExecuteParentMethod != null)
                ExecuteParentMethod(isUserAuthorised, EventArgs.Empty);
        }
        #endregion

      
    }
}
