﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TripCameraSettings
{
    /// <summary>
    /// Interaction logic for SliderControl.xaml
    /// </summary>
    public partial class SliderControl : UserControl
    {
        public SliderControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result = string.Empty;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        public double sliderMinValue { get; set; }
        public double sliderMaxValue { get; set; }
        public double sliderValue { get; set; }
        public double TickFrequency { get; set; }
        public string controlName { get; set; }
        public string PropertyName { get; set; }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = slider.Value.ToString();
            HideHandlerDialog();
        }

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }
        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog()
        {
            setSliderValues();
            _result = string.Empty;

            Visibility = Visibility.Visible;
            txtValue.Focus();
            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        private void setSliderValues()
        {
            slider.Minimum = sliderMinValue;
            slider.Maximum = sliderMaxValue;
            slider.Value = sliderValue;
            slider.TickFrequency = TickFrequency;
            lblPropertyName.Text = PropertyName;


        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            //_result = txtValue.Text;
            //HideHandlerDialog();
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            //grdSliderCtrl.Focus();
            //slider.Focus();
        }

        private void txtValue_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                _result = slider.Value.ToString();
                HideHandlerDialog();
            }
        }
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ValidateInput(sender))
            {
                if (Convert.ToDouble(txtValue.Text) > slider.Maximum)
                    txtValue.Text = slider.Maximum.ToString();
            }
        }

        private static bool ValidateInput(object sender)
        {
            string strtemp = string.Empty;
            bool success = false;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                strtemp = "";
            }
            else
            {
                double num;
                success = double.TryParse(((TextBox)sender).Text, out num);
                if (success)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
            return success;
        }
    }
}
