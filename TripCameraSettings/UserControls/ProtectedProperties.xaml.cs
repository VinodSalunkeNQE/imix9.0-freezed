﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
namespace TripCameraSettings
{
    /// <summary>
    /// Interaction logic for ProtectedProperties.xaml
    /// </summary>
    public partial class ProtectedProperties : UserControl
    {
        public ProtectedProperties()
        {
            InitializeComponent();
        }

       
        #region Declaration
        UIElement _parent;
        public event EventHandler ExecuteParentMethod;
        #endregion
        
        #region Events
        private void txtBlackLevel_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 127.75,
                MinValue = 0,
                PropertyName = "BlackLevel",
                IsFloat = true,
                tickFrequency = 0.05
            };
            SetSliderProperties(sender, obj);
        }

        private void txtGain_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 40,
                MinValue = 0,
                PropertyName = "Gain",
                IsFloat = true,
                tickFrequency = 0.1
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGamma_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 4,
                MinValue = 0.25,
                PropertyName = "Gamma",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain00_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain00",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain01_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain01",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain02_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain02",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain10_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain10",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain22_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain22",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain21_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain21",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain20_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain20",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain12_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain12",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtGain11_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = -2,
                PropertyName = "Gain11",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtHue_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 40,
                MinValue = -40,
                PropertyName = "Hue"
            };
            SetSliderProperties(sender, obj);
        }
        private void txtSaturation_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 2,
                MinValue = 0,
                PropertyName = "Saturation",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtWBRed_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 3.0,
                MinValue = 0.8,
                PropertyName = "White Balance Red",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        private void txtWBBlue_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                MaxValue = 3.0,
                MinValue = 0.8,
                PropertyName = "White Balance Blue",
                IsFloat = true,
                tickFrequency = 0.01
            };
            SetSliderProperties(sender, obj);
        }
        #endregion

        #region Methods
        
        private void SetSliderProperties(object sender, SliderInfo obj)
        {
            try
            {
                TextBox control = (TextBox)sender;
                if (String.IsNullOrWhiteSpace(control.Text))
                    control.Text = "0";
                ucSliderCtrl.sliderMinValue = obj.MinValue;
                ucSliderCtrl.sliderMaxValue = obj.MaxValue;
                ucSliderCtrl.sliderValue = Convert.ToDouble(control.Text);
                ucSliderCtrl.controlName = control.Name;
                ucSliderCtrl.TickFrequency = obj.tickFrequency == 0 ? 1 : obj.tickFrequency;
                ucSliderCtrl.PropertyName = obj.PropertyName;
                ucSliderCtrl.SetParent(this.grdProtectedProperties);
                double _currentvalue = Convert.ToDouble(ucSliderCtrl.ShowHandlerDialog());

                if (obj.IsFloat)
                    control.Text = _currentvalue.ToString();
                else
                    control.Text = Convert.ToString(Convert.ToInt64(_currentvalue));
                
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

   
        #endregion

        #region HandlerDialog
        public void SetParent(UIElement parent)
        {
            _parent=parent;
        }
        public void ShowHandlerDialog(TripCamFeaturesInfo tripCameraFeatureInfo)
        {
            GetProperty(tripCameraFeatureInfo);
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
        }

        private void GetProperty(TripCamFeaturesInfo tripCameraFeatureInfo)
        {
            txtGain00.Text=tripCameraFeatureInfo.Gain00.ToString();
            txtGain01.Text=tripCameraFeatureInfo.Gain01.ToString();
            txtGain02.Text=tripCameraFeatureInfo.Gain02.ToString();
            txtGain10.Text=tripCameraFeatureInfo.Gain10.ToString();
            txtGain11.Text=tripCameraFeatureInfo.Gain11.ToString();
            txtGain12.Text=tripCameraFeatureInfo.Gain12.ToString();
            txtGain20.Text=tripCameraFeatureInfo.Gain20.ToString();
            txtGain21.Text=tripCameraFeatureInfo.Gain21.ToString();
            txtGain22.Text = tripCameraFeatureInfo.Gain22.ToString();
            txtBlackLevel.Text=tripCameraFeatureInfo.BlackLevel.ToString();
            txtGain.Text = tripCameraFeatureInfo.Gain.ToString();
            txtGamma.Text = tripCameraFeatureInfo.Gamma.ToString();
            txtHue.Text = tripCameraFeatureInfo.Hue.ToString();
            txtSaturation.Text = tripCameraFeatureInfo.Saturation.ToString();
            txtWBBlue.Text = tripCameraFeatureInfo.WhiteBalanceBlue.ToString();
            txtWBRed.Text = tripCameraFeatureInfo.WhiteBalanceRed.ToString();
            if (tripCameraFeatureInfo.ColorTransformationMode == null)
                cmbColorSelectionMode.SelectedIndex=0;
            else
                cmbColorSelectionMode.Text = tripCameraFeatureInfo.ColorTransformationMode.ToString();
            

        }
        public void HideHandlerDialog()
        {
            OnExecuteMethod();
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public virtual void OnExecuteMethod()
        {   
            TripCamFeaturesInfo tripCamera=SetProtectedProperties();
            if (ExecuteParentMethod != null)
                ExecuteParentMethod(tripCamera, EventArgs.Empty);
        }

        private TripCamFeaturesInfo SetProtectedProperties()
        {
            TripCamFeaturesInfo tripCamera = new TripCamFeaturesInfo();
            if (!String.IsNullOrEmpty(txtGain00.Text))
                tripCamera.Gain00 = float.Parse(txtGain00.Text);
            if (!String.IsNullOrEmpty(txtGain01.Text))
                tripCamera.Gain01 = float.Parse(txtGain01.Text);
            if (!String.IsNullOrEmpty(txtGain02.Text))
                tripCamera.Gain02 = float.Parse(txtGain02.Text);
            if (!String.IsNullOrEmpty(txtGain10.Text))
                tripCamera.Gain10 = float.Parse(txtGain10.Text);
            if (!String.IsNullOrEmpty(txtGain11.Text))
                tripCamera.Gain11 = float.Parse(txtGain11.Text);
            if (!String.IsNullOrEmpty(txtGain12.Text))
                tripCamera.Gain12 = float.Parse(txtGain12.Text);
            if (!String.IsNullOrEmpty(txtGain20.Text))
                tripCamera.Gain20 = float.Parse(txtGain20.Text);
            if (!String.IsNullOrEmpty(txtGain21.Text))
                tripCamera.Gain21 = float.Parse(txtGain21.Text);
            if (!String.IsNullOrEmpty(txtGain22.Text))
                tripCamera.Gain22 = float.Parse(txtGain22.Text);
            if (!String.IsNullOrEmpty(txtBlackLevel.Text))
                tripCamera.BlackLevel = float.Parse(txtBlackLevel.Text);
            if (!String.IsNullOrEmpty(txtGain.Text))
                tripCamera.Gain = float.Parse(txtGain.Text);
            if (!String.IsNullOrEmpty(txtGamma.Text))
                tripCamera.Gamma = float.Parse(txtGamma.Text);
            if (!String.IsNullOrEmpty(txtHue.Text))
                tripCamera.Hue = float.Parse(txtHue.Text);
            if (!String.IsNullOrEmpty(txtSaturation.Text))
                tripCamera.Saturation = float.Parse(txtSaturation.Text);
            if (!String.IsNullOrEmpty(txtWBBlue.Text))
                tripCamera.WhiteBalanceBlue = float.Parse(txtWBBlue.Text);
            if (!String.IsNullOrEmpty(txtWBRed.Text))
                tripCamera.WhiteBalanceRed = float.Parse(txtWBRed.Text);

            tripCamera.ColorTransformationMode = cmbColorSelectionMode.Text;
            return tripCamera;

        }
        #endregion
    }
}
