﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TripCameraSettings
{
    /// <summary>
    /// Interaction logic for AdvancedControls.xaml
    /// </summary>
    public partial class AdvancedControls : UserControl
    {
        #region Declaration
        public UIElement _Parent;
        public event EventHandler ExecuteParentMethod;

        #endregion
        public AdvancedControls()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //_result = txtValue.Text;
            HideHandlerDialog();
        }

        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            //tripCameraFeature.TriggerSource = 
            cmbtriggerSource.SelectedValue.ToString();
            OnExecuteMethod();
            //tripCameraFeature.PixelFormat = cmbImageType.SelectedValue.ToString();
        }

        double maxValue = 0;
        public void ShowHandlerDailog(TripCamFeaturesInfo tripCamFeatureInfo)
        {
            // _Parent.IsEnabled = false;
            Visibility = Visibility.Visible;
           // maxValue = tripCamFeatureInfo.MaxStreamByteValue;
            cmbImageType.Text = tripCamFeatureInfo.PixelFormat;
            cmbtriggerSource.Text = tripCamFeatureInfo.TriggerSource;
            txtStreamsBytes.Text = tripCamFeatureInfo.StreamBytesPerSec.ToString();;


        }
        protected virtual void OnExecuteMethod()
        {
            string[] advSettings = new string[5];
            EventArgs obj = new EventArgs();
            advSettings[0] = cmbImageType.Text;
            advSettings[1] = cmbtriggerSource.Text;
            advSettings[2] = txtStreamsBytes.Text;
            if (ExecuteParentMethod != null)
                ExecuteParentMethod(advSettings, EventArgs.Empty);
        }

        private void txtStreamsBytes_GotFocus(object sender, RoutedEventArgs e)
        {
            SliderInfo obj = new SliderInfo()
            {
                //MaxValue = maxValue,
                MaxValue =124000000,
                MinValue = 100000,
                PropertyName = "Stream Bytes/Sec",
            };
            SetSliderProperties(sender, obj);
        }
         private void SetSliderProperties(object sender, SliderInfo obj)
        {
            try
            {
                TextBox control = (TextBox)sender;
                if (String.IsNullOrWhiteSpace(control.Text))
                    control.Text = "0";
                ucSliderCtrl.sliderMinValue = obj.MinValue;
                ucSliderCtrl.sliderMaxValue = obj.MaxValue;
                ucSliderCtrl.sliderValue = Convert.ToDouble(control.Text);
                ucSliderCtrl.controlName = control.Name;
                ucSliderCtrl.TickFrequency = obj.tickFrequency == 0 ? 1 : obj.tickFrequency;
                ucSliderCtrl.PropertyName = obj.PropertyName;
                ucSliderCtrl.SetParent(this.grdAdvanceCntrl);
                double _currentvalue = Convert.ToDouble(ucSliderCtrl.ShowHandlerDialog());
                if (obj.IsFloat)
                    control.Text = _currentvalue.ToString();
                else
                    control.Text = Convert.ToString(Convert.ToInt64(_currentvalue));
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

       
    }

}
